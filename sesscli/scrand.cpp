//
// C++ Interface: Random Number Retriever
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "scrand.h"

#ifdef Q_OS_WIN32
#include <windows.h>
//#pragma comment(lib, "advapi32.lib")
#else
#include <QFile>
#endif

#include <QDebug>

namespace MagicSmokeRandom {

/// Get some crypto strength random bytes. May return less than requested.
QByteArray getRandomBytes(quint8 bytes)
{
	if(bytes==0)return QByteArray();
#if defined(Q_OS_UNIX)||defined(Q_OS_LINUX)||defined(Q_OS_DARWIN)
	//try urandom, then random
	QFile fd("/dev/urandom");
	if(!fd.open(QIODevice::ReadOnly)){
		fd.setFileName("/dev/random");
		if(!fd.open(QIODevice::ReadOnly)){
			qDebug()<<"Unable to open /dev/{u}random - sorry.";
			return QByteArray();
		}
	}
	QByteArray r=fd.read(bytes);
	fd.close();
	return r;
#elif defined(Q_OS_WIN32)
	BYTE data[256];
	HCRYPTPROV hCryptProv=0;
	if (!::CryptAcquireContextW(&hCryptProv, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)){
		qDebug()<<"Error: Unable to acquire crypto context.";
		return QByteArray();
	}
	QByteArray r;
	if(::CryptGenRandom(hCryptProv,bytes,data)){
		r=QByteArray((const char*)data,bytes);
	}else{
		qDebug()<<"Error: Unable to get random numbers from OS.";
	}
	if(!::CryptReleaseContext(hCryptProv, 0))
		qDebug()<<"Warning: unable to release crypto context!";
	return r;
#endif
}

}
