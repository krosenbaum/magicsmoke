#Project File for MagicSmoke Session Client Library
# (c) Konrad Rosenbaum, 2016

#basics
TEMPLATE = lib
VERSION = 
TARGET = magicsmoke-sesscli

include(../basics.pri)
include(scrand.pri)

#Localization
TRANSLATIONS = \
	smoke-sc_de.ts \
	smoke-sc_de_SAX.ts \
	smoke-sc_en.ts

#main source files
SOURCES += scli.cpp
HEADERS += scli.h
INCLUDEPATH += .

#make sure exports are ok
DEFINES += MAGICSMOKE_SESSCLI_LIB_BUILD=1

#make sure the correct Qt DLLs are used
QT += network
