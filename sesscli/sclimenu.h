//
// C++ Interface: Session Client
//
// Description: Session Client Menu Convenience Function.
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SC_SCLIMENU_H
#define MAGICSMOKE_SC_SCLIMENU_H

#include "scli.h"
#include <QMenu>
#include <QSignalMapper>
#include <QAction>


///creates a menu that allows (limited) control over the session manager
///returns nullptr if the menu could not be retrieved
///\param menu if not NULL: the menu to fill, if NULL: a new menu will be created, please do not forget to take ownership of the new menu
inline QMenu* MSessionClient_createMenuObject(QMenu*menu=nullptr)
{
	//retrieve if necessary
	auto inst=MSessionClient::instance();
	auto mmenu=inst->menuEntries();
	if(mmenu.isEmpty())return nullptr;
	//generate menu
	QMenu*m=menu?menu:new QMenu;
	QSignalMapper*sm=new QSignalMapper(m);
	for(auto entry:mmenu){
		QAction*a=m->addAction(entry.second,sm,SLOT(map()));
		sm->setMapping(a,entry.first);
	}
	QObject::connect(sm,SIGNAL(mapped(QString)),inst,SLOT(execServerCommand(QString)));
	return m;
}



#endif
