//
// C++ Interface: Session Client
//
// Description: Session Client Class - connects to a session manager (or even starts one)
//   and enables exchange of session data.
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SC_SCLI_H
#define MAGICSMOKE_SC_SCLI_H

#include <QObject>
#include <QList>
#include <QPair>
#include <QLocalSocket>

#ifdef MAGICSMOKE_SESSCLI_LIB_BUILD
# define MAGICSMOKE_SESSCLI_EXPORT Q_DECL_EXPORT
#else
# define MAGICSMOKE_SESSCLI_EXPORT Q_DECL_IMPORT
#endif

class MAGICSMOKE_SESSCLI_EXPORT MSessionClient:public QObject
{
	Q_OBJECT
public:
	///Initialize the session client, make sure it is connected to the Session Manager
	MSessionClient();
	///Disconnect the session client.
	virtual ~MSessionClient();

	///returns the current instance of the Session Client class
	///returns nullptr if there is no instance
	static MSessionClient*instance();

	///Wait until a session is available or the Session Manager quits.
	///\returns true if a session is available, false on error
	virtual bool waitForSessionAvailable();
	
	///Wait until the session manager is ready to receive commands.
	virtual bool waitForReady();

	///Returns true if there currently is a session available.
	virtual bool sessionIsAvailable()const;
	///Returns the session id.
	virtual QString currentSessionId()const;
	///Returns the profile id.
	virtual QString currentProfileId()const;
	///Returns the user name
	virtual QString currentUsername()const;
	///Returns true if the client is connected to a manager. If false there is something wrong.
	virtual bool isConnected()const;

	///retrieves and returns the menu entries of the session manager in the format command -> menu text
	/// \param force if true: force fresh retrieval of the menu even if it is already known
	virtual QList<QPair<QString,QString>> menuEntries(bool force=false)const;
	
	///retrieves and returns all configured profiles in the format profileId -> profile name
	/// \param force if true: force fresh retrieval of the menu profiles if it is already known
	virtual QList<QPair<QString,QString>> profiles(bool force=false)const;
	
	///gets the default profile ID
	virtual QString defaultProfileId()const;

public slots:
	void execServerCommand(QString);
	void setProfile(QString);
	void login(QString user,QString password,bool attemptLogin=true);

signals:
	void sessionIdChanged(QString sessionId);
	void sessionLost();
	void managerLost();
	void menuChanged();
	void profilesChanged();
	void readyReceived();

private slots:
	void socketLost();
	void readSocket();

private:
	QLocalSocket*msocket=nullptr;
	QString msid,mprofile,muser,mdefaultprofile;
	QList<QPair<QString,QString>> mmenu,mprofiles;
	bool misready=false;
};


#endif
