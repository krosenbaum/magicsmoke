//
// C++ Implementation: Session Client
//
// Description: Session Client Class - connects to a session manager (or even starts one)
//   and enables exchange of session data.
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "scli.h"
#include "scrand.h"

#include <QCoreApplication>
#include <QEventLoop>
#include <QLocalServer>
#include <QPointer>
#include <QProcess>
#include <QRegExp>
#include <QStringList>
#include <QTimer>

static QPointer<MSessionClient> sessionclientinstance;

MSessionClient::MSessionClient()
{
	sessionclientinstance=this;
	//check parameters for socket name
	QString sms;
	for(QString arg:qApp->arguments())
		if(arg.startsWith("-sms:")){
			sms=arg.mid(5);
			break;
		}
	//none found: start session manager and wait for socket name
	if(sms.isEmpty()){
		//generate reverse socket
		QString key=QString::fromLatin1(MagicSmokeRandom::getRandomBytes(32).toHex());
		QLocalServer serv;
		serv.setSocketOptions(QLocalServer::UserAccessOption);
		serv.setMaxPendingConnections(1);
		if(!serv.listen("magicsmoke-smr-"+key)){
			qDebug()<<"Ouch! Unable to create local socket server to communicate with Session Manager!";
			return;
		}
		//start process
		qDebug()<<"Starting Session Manager...";
		if(!QProcess::startDetached(qApp->applicationDirPath()+"/magicsmoke-sessman", QStringList()<<"-slave:"+key)){
			qDebug()<<"Ouch! Unable to start session manager!";
			return;
		}
		//wait for connection
		if(!serv.waitForNewConnection(10000)){
			qDebug()<<"Ouch! Session Manager did not respond!";
			return;
		}
		QLocalSocket *s=serv.nextPendingConnection();
		if(s==nullptr){
			qDebug()<<"Ouch! Session Manager did not connect back! Something is strange.";
			return;
		}
		//read Session ID data
		if(!s->waitForReadyRead(10000)){
			qDebug()<<"Ouch! Session Manager did not send a socket ID. Giving up.";
			s->deleteLater();
			return;
		}
		sms=QString::fromLatin1(s->readLine().trimmed());
		s->deleteLater();
		serv.close();
		if(sms.size()!=64 || !QRegExp("[a-f0-9]+",Qt::CaseInsensitive).exactMatch(sms)){
			qDebug()<<"Ouch! Session Manager did not send a valid socket ID."<<sms;
			return;
		}
		qDebug()<<"Got session socket ID from Session Manager.";
	}
	//open socket
	QLocalSocket *s=new QLocalSocket(this);
	s->connectToServer("magicsmoke-sms-"+sms);
	if(!s->waitForConnected(10000)){
		qDebug()<<"Ouch! Unable to connect to Session Manager!";
		s->deleteLater();
		return;
	}
	msocket=s;
	connect(s,SIGNAL(readyRead()),this,SLOT(readSocket()));
	connect(s,SIGNAL(disconnected()),this,SLOT(socketLost()));
	connect(s,SIGNAL(error(QLocalSocket::LocalSocketError)),this,SLOT(socketLost()));
	s->write("clientinit magicsmoke\n");
}

MSessionClient::~MSessionClient()
{
	if(msocket!=nullptr)
		msocket->close();
}

MSessionClient* MSessionClient::instance()
{
	return sessionclientinstance;
}


bool MSessionClient::isConnected() const
{
	return msocket!=nullptr && msocket->isOpen();
}

QString MSessionClient::currentSessionId() const
{
	return msid;
}

QString MSessionClient::currentProfileId() const
{
	return mprofile;
}

QString MSessionClient::currentUsername() const
{
	return muser;
}

bool MSessionClient::sessionIsAvailable() const
{
	return !msid.isEmpty();
}

bool MSessionClient::waitForSessionAvailable()
{
	if(!isConnected())return false;
	if(!msid.isEmpty())return true;
	//wait for signal
	QEventLoop loop;
	connect(this,SIGNAL(sessionIdChanged(QString)),&loop,SLOT(quit()));
	connect(this,SIGNAL(managerLost()),&loop,SLOT(quit()));
	loop.exec();
	return !msid.isEmpty();
}

bool MSessionClient::waitForReady()
{
	if(!isConnected())return false;
	if(misready)return true;
	//wait for signal
	QEventLoop loop;
	connect(this,SIGNAL(readyReceived()),&loop,SLOT(quit()));
	connect(this,SIGNAL(managerLost()),&loop,SLOT(quit()));
	loop.exec();
	return misready;
}

QList< QPair< QString, QString > > MSessionClient::menuEntries(bool force) const
{
	if(!isConnected())return QList<QPair<QString,QString>>();
	if(mmenu.isEmpty()||force){
		//get menu
		QEventLoop loop;
		connect(this,SIGNAL(menuChanged()),&loop,SLOT(quit()));
		connect(this,SIGNAL(managerLost()),&loop,SLOT(quit()));
		QTimer::singleShot(5000,&loop,SLOT(quit()));
		msocket->write("getmenu\n");
		msocket->waitForBytesWritten(1000);
		loop.exec();
	}
	return mmenu;
}

QList< QPair< QString, QString > > MSessionClient::profiles(bool force) const
{
	if(!isConnected())return QList<QPair<QString,QString>>();
	if(mprofiles.isEmpty()||force){
		//get menu
		QEventLoop loop;
		connect(this,SIGNAL(profilesChanged()),&loop,SLOT(quit()));
		connect(this,SIGNAL(managerLost()),&loop,SLOT(quit()));
		QTimer::singleShot(5000,&loop,SLOT(quit()));
		msocket->write("getprofiles\n");
		msocket->waitForBytesWritten(1000);
		loop.exec();
	}
	return mprofiles;
}

QString MSessionClient::defaultProfileId() const
{
	if(mprofiles.isEmpty())profiles();
	return mdefaultprofile;
}


void MSessionClient::socketLost()
{
	qDebug()<<"Warning: Session Manager Socket lost!";
	if(msocket)
		msocket->deleteLater();
	msocket=nullptr;
	msid.clear();
	emit managerLost();
	emit sessionLost();
}

void MSessionClient::readSocket()
{
	while(msocket && msocket->canReadLine()){
		const QString line=QString::fromUtf8(msocket->readLine()).trimmed();
		const int pos=line.indexOf(' ');
		const QString cmd=pos>0?line.left(pos):line;
		const QString par=pos>0?line.mid(pos+1).trimmed():QString();
		qDebug()<<"Received session manager command"<<cmd<<"with param"<<(cmd!="sid"?par:"(censored)");
		if(cmd=="sid"){
			msid=par;
			qDebug()<<"Received new Session ID.";
			emit sessionIdChanged(msid);
		}else if(cmd=="profile"){
			qDebug()<<"Using session profile"<<par;
			mprofile=par;
		}else if(cmd=="user"){
			qDebug()<<"Username"<<par;
			muser=par;
		}else if(cmd=="newmenu"){
			mmenu.clear();
		}else if(cmd=="menu"){
			const int pos=par.indexOf(' ');
			if(pos<2){
				qDebug()<<"Warning: received invalid menu command. Ignoring it.";
				continue;
			}
			mmenu.append(QPair<QString,QString>(par.left(pos),par.mid(pos+1)));
		}else if(cmd=="endmenu"){
			qDebug()<<"New Menu received from Session Manager!";
			emit menuChanged();
		}else if(cmd=="newprofiles"){
			mprofiles.clear();
			mdefaultprofile.clear();
		}else if(cmd=="haveprofile"){
			const int pos=par.indexOf(' ');
			if(pos<1){
				qDebug()<<"Warning: received invalid haveprofile command. Ignoring it. Info Parameter: "<<par;
				continue;
			}
			mprofiles.append(QPair<QString,QString>(par.left(pos),par.mid(pos+1)));
		}else if(cmd=="defaultprofile"){
			mdefaultprofile=par.trimmed();
		}else if(cmd=="endprofiles"){
			qDebug()<<"Received new list of profiles.";
			emit profilesChanged();
		}else if(cmd=="closed"){
			msid.clear();
			qDebug()<<"Warning: Session lost!";
			emit sessionLost();
		}else if(cmd=="quit"){
			socketLost();
		}else if(cmd=="ready"){
			misready=true;
			emit readyReceived();
		}else
			qDebug()<<"Warning: unknown session manager signal"<<cmd<<"encountered. Ignoring it.";
	}
}

void MSessionClient::execServerCommand ( QString cmd)
{
	if(msocket && msocket->isOpen())
		msocket->write(QString("exec "+cmd+"\n").toUtf8());
}

void MSessionClient::setProfile(QString pn)
{
	if(msocket && msocket->isOpen())
		msocket->write(QString("setprofile %1\n").arg(pn).toUtf8());
}

void MSessionClient::login(QString user,QString password,bool attemptLogin)
{
	if(msocket && msocket->isOpen())
		msocket->write(QString("setuser %1\nsetpasswd %2\n%3")
			.arg(user)
			.arg(QString::fromLatin1(password.toUtf8().toBase64()))
			.arg(attemptLogin?"login\n":"")
			.toUtf8());
}
