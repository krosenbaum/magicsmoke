//
// C++ Interface: Random Number Retriever
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SC_RAND_H
#define MAGICSMOKE_SC_RAND_H

#include <QByteArray>

namespace MagicSmokeRandom {

/// Get some crypto strength random bytes. May return less than requested.
QByteArray getRandomBytes(quint8 bytes);

}

#endif
