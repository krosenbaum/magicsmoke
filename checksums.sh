#!/bin/bash

# Helper Script to check on Archive integrity
#
# Once the distribution is complete this is used with the index file
# to check its integrity and the checksums of all archives.

if test $# -lt 1 ; then
        echo Usage: $0 archive-xmlfile [GPG-homedir]
        exit 1
fi

if test $# -ge 2 ; then
        gpg --batch --homedir $2 --verify $1.asc $1
        echo
fi

grep '<Archive' $1 |cut -d \< -f 2 |cut -d \> -f 1 | while read line ; do
 for i in $line ; do
   echo $i|grep -q ^sha256 && SHA=`echo $i|cut -d \" -f 2`
   echo $i|grep -q ^name && NAME=`echo $i|cut -d \" -f 2`
 done
 SHA2=`sha256sum $NAME|cut -d " " -f 1`
 if test $SHA = $SHA2 ; then
  echo GOOD $NAME $SHA
 else
  echo BAD $NAME
  echo "  file:" $SHA2
  echo "   sig:" $SHA
 fi
done