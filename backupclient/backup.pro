#Project File for MagicSmoke Backup Client
# (c) Konrad Rosenbaum, 2015
TEMPLATE = app

TARGET = msmokebackup

#main source files
SOURCES = main.cpp
#HEADERS = main.h
INCLUDEPATH += .


# Basic settings
include (../basics.pri)

# MagicSmoke Interface
include (../iface/iface.pri)

#make sure the correct Qt DLLs are used
QT += xml network
QT += widgets

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
