//
// C++ Implementation: Backup Client
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2015
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//
#include <QApplication>

#include "main.h"

#include "msinterface.h"
#include "MIncludeAll"

BLogin::BLogin()
{
	login();
}

void BLogin::login()
{
	accept();

	qDebug()<<req->login("backup.agent","backup");
}


int main(int ac,char**av)
{
	QApplication app(ac,av);
	//locate settings
	app.setOrganizationName("MagicSmoke");
	app.setApplicationName("MagicSmoke2");

	BLogin*l=new BLogin;
	l->show();

	return app.exec();
}
