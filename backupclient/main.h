//
// C++ Interface: Backup Client
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2015
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#ifndef MSBAK_MAIN_H
#define MSBAK_MAIN_H

#include <QDialog>

class BLogin:public QDialog
{
	Q_OBJECT
public:
	BLogin();

public slots:
	void login();
};

#endif
