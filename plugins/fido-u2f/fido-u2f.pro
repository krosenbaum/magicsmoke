TEMPLATE = lib
TARGET = msp-fido-u2f
CONFIG += plugin no_plugin_name_prefix

include (../../basics.pri)
include (../../src/smoke.pri)

#SOURCES += bcs-plugin.cpp configwidget.cpp bcskeyboard.cpp hidscanner.cpp
#HEADERS += bcs-plugin.h configwidget.h bcskeyboard.h hidscanner.h

CONFIG += link_pkgconfig

packagesExist(u2f-host) {
	PKGCONFIG += u2f-host
	message("Pkg-Config: U2F libs found")
} else {
	error("Pkg-Config: U2F libs not found!")
}
