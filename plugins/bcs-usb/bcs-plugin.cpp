//
// C++ Implementation: Barcode Plugin for USB
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "bcs-plugin.h"
#include "configwidget.h"
#include "hidscanner.h"

#include <hidapi.h>

#include <QDebug>
#include <QSettings>
#include <QTimer>

MHidBarcodePlugin::MHidBarcodePlugin(): QObject()
{
	qDebug()<<"Initializing USB Barcode Plugin";
	misopen = (hid_init()==0);
	if(misopen)
		qDebug()<<" ...success.";
	else{
		qDebug()<<" ...failed to initialize USB-HID.";
		return;
	}
	mdetecttmr=new QTimer(this);
	connect(mdetecttmr,SIGNAL(timeout()),this,SLOT(detectScanners()));
	mdetecttmr->setSingleShot(false);
	restartDetect();
}

MHidBarcodePlugin::~MHidBarcodePlugin()
{
	qDebug()<<"Unloading USB Barcode Plugin";
	if(mdetecttmr)mdetecttmr->stop();
	for(MHidBarcodeScanner*sc:mscanners){
		if(sc)delete sc;
	}
	mscanners.clear();
	if(misopen)hid_exit();
}


void MHidBarcodePlugin::configure(MBarcodeConfiguration* cfg)
{
	//suspend detection until dialog closed
	if(mdetecttmr)mdetecttmr->stop();
	//main tab
	new MUsbBarcodeConfig(cfg);
	QSettings set;set.beginGroup(MUsbBarcodeConfig::settingsGroup);
	for(const QString s:set.childGroups())
		new MUsbScannerConfig(cfg,s);
	//restart detection
	connect(cfg,SIGNAL(destroyed(QObject*)),this,SLOT(restartDetect()));
}

MHidBarcodePlugin::Config::Config(QSettings& set,QString grp)
{
	set.beginGroup(grp);
	isactive=set.value(MUsbBarcodeConfig::activeKey,false).toBool();
	vendor=set.value(MUsbBarcodeConfig::vendorKey,0).toInt();
	product=set.value(MUsbBarcodeConfig::productKey,0).toInt();
	iface=set.value(MUsbBarcodeConfig::interfaceKey,0).toInt();
	useserial=set.value(MUsbBarcodeConfig::useSerialKey,false).toBool();
	serial=set.value(MUsbBarcodeConfig::serialKey).toString();
	group=grp;
	set.endGroup();
}

void MHidBarcodePlugin::restartDetect()
{
	if(!misopen){
		if(mdetecttmr){
			delete mdetecttmr;
			mdetecttmr=0;
		}
		return;
	}
	const int interv=QSettings().value(MUsbBarcodeConfig::intervalKey,10).toInt();
	if(interv>0)
		mdetecttmr->start(interv*1000);
	else
		mdetecttmr->stop();
	//re-read config
	QSettings set;
	set.beginGroup(MUsbBarcodeConfig::settingsGroup);
	mconfig.clear();
	for(const QString grp:set.childGroups()){
		Config c(set,grp);
		if(!c.isactive){
			continue;
		}
		mconfig.append(c);
	}
	//TODO: go through running scanners and kill unknowns
}

void MHidBarcodePlugin::detectScanners()
{
	mscanners.removeAll(nullptr);
	if(!misopen)return;
	qDebug()<<"scanning for scanners...";
	hid_device_info *info=hid_enumerate(0,0);
	for(hid_device_info*dev=info;dev;dev=dev->next){
		//is it known?
		Config cfg=findCfgMatch(dev->vendor_id,dev->product_id,dev->interface_number,
					QString::fromWCharArray(dev->serial_number));
		if(cfg.vendor<=0)continue;
		//is it already open?
		MHidBarcodeScanner*scanner=findScanner(dev->path);
		if(scanner)continue;
		//open it
		qDebug()<<"instantiating scanner";
		scanner=new MHidBarcodeScanner(dev->path,cfg.group);
		mscanners.append(scanner);
		connect(scanner,SIGNAL(destroyed(QObject*)),this,SLOT(removeScanner(QObject*)));
		//register with parent
		registerScanner(scanner);
	}
	hid_free_enumeration(info);
}

MHidBarcodePlugin::Config MHidBarcodePlugin::findCfgMatch(int vendor, int product, int iface, QString serial)
{
	for(Config c:mconfig){
		if(c.vendor!=vendor || c.product!=product || c.iface!=iface)continue;
		if(c.useserial && c.serial!=serial)continue;
		return c;
	}
	return Config();
}

MHidBarcodeScanner* MHidBarcodePlugin::findScanner(QString path)
{
	for(MHidBarcodeScanner*scanner:mscanners)
		if(scanner->matchPath(path))
			return scanner;
	return nullptr;
}

void MHidBarcodePlugin::removeScanner(QObject* o)
{
	MHidBarcodeScanner*s=qobject_cast< MHidBarcodeScanner* >(o);
	if(s)
		mscanners.removeAll(s);
}

