//
// C++ Implementation: Barcode Plugin for USB: Scanner
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "hidscanner.h"
#include "configwidget.h"

#include <QDebug>
#include <QTimer>
#include <QSettings>

MHidBarcodeScanner::MHidBarcodeScanner(QString path,QString setGrp, QObject* parent)
	: MBarcodeScanner(parent),mpath(path)
{
	qDebug()<<"Trying to open device at"<<path;
	activate();
	//read settings
	QSettings set;
	set.beginGroup(MUsbBarcodeConfig::settingsGroup);
	set.beginGroup(setGrp);
	mtimeout=set.value(MUsbBarcodeConfig::timeoutKey,0).toInt();
	mterminator=MKey::fromCharName(set.value(MUsbBarcodeConfig::terminatorKey).toString());
	mname=set.value(MUsbBarcodeConfig::nameKey,mpath).toString();
	mlayout.loadLayout(set.value(MUsbBarcodeConfig::layoutKey,"qwerty").toString());
	qDebug()<<"Configured scanner"<<mpath<<"as device name"<<mname<<"with layout"<<mlayout.layoutName();
}

MHidBarcodeScanner::~MHidBarcodeScanner()
{
	deactivate();
	qDebug()<<"Deleting Scanner"<<mpath<<mname;
}

void MHidBarcodeScanner::activate()
{
	if(mdev!=nullptr && mrdtmr!=nullptr)return;
	mdev=hid_open_path(mpath.toLocal8Bit().data());
	if(!mdev){
		qDebug()<<"Unable to open device.";
		deleteLater();
		return;
	}
	hid_set_nonblocking(mdev,1);
	mrdtmr=new QTimer(this);
	mrdtmr->setSingleShot(false);
	mrdtmr->start(20);
	connect(mrdtmr,SIGNAL(timeout()),this,SLOT(readData()));
	qDebug()<<"Successfully opened scanner"<<mpath;
	emit activated();
}

void MHidBarcodeScanner::deactivate()
{
	qDebug()<<"Deactivating Scanner"<<mpath<<mname;
	if(mrdtmr)delete mrdtmr;
	mrdtmr=nullptr;
	if(mdev)hid_close(mdev);
	mdev=nullptr;
	emit deactivated();
}

QString MHidBarcodeScanner::readableName() const
{
	return mname;
}

void MHidBarcodeScanner::readData()
{
	if(mdev==nullptr){
		if(mseq.size()>0)processData();
		return;
	}
	//read data
	unsigned char buf[64];
	memset(buf,0,sizeof(buf));
	while(true){
		const int rs=hid_read(mdev,buf,sizeof(buf));
		if(rs<0){
			qDebug()<<"Error reading from USB device"<<mpath<<mname<<"Device will be closed.";
			deactivate();
			deleteLater();
			break;
		}
		if(rs==0)break;
		if(rs<3){
			qDebug()<<"Warning: invalid report from USB device"<<mpath<<"expected >=3 bytes, got"<<rs;
		}
		mseq.append(MHidKeyEvent(buf[0],QByteArray((char*)buf+2,rs-2)));
	}
	processData();
}

void MHidBarcodeScanner::processData()
{
	if(mseq.isEmpty())return;
	//is the timeout over?
	bool force=false;
	if(mtimeout>0 && mseq.msSinceLastKey()>=mtimeout)
		force=true;
	//can we see one of the termination codes?
	if(!force && !mterminator.isValid())return;
	MKeySequence kseq=mlayout.toKeys(mseq);
	if(!force && !kseq.contains(mterminator))return;
	//convert
	QString bc=kseq.toString();
	mseq.clear();
	//remove superfluous chars
	//verify checksum and remove it
	//emit barcode
	qDebug()<<"scanned barcode"<<bc;
	emit newBarcode(bc);
}
