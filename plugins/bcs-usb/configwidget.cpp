//
// C++ Implementation: Barcode Plugin for USB: Config
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "configwidget.h"

#include "barcode-plugin.h"
#include <QFormLayout>
#include <QComboBox>
#include <QPushButton>

const QString MUsbBarcodeConfig::settingsGroup="usbBarcodeScanner";
const QString MUsbBarcodeConfig::intervalKey="interval";
const QString MUsbBarcodeConfig::timeoutKey="timeoutms";
const QString MUsbBarcodeConfig::terminatorKey="terminatorkey";
const QString MUsbBarcodeConfig::layoutKey="kbdlayout";
const QString MUsbBarcodeConfig::nameKey="name";
const QString MUsbBarcodeConfig::activeKey="active";
const QString MUsbBarcodeConfig::vendorKey="vendorid";
const QString MUsbBarcodeConfig::productKey="productid";
const QString MUsbBarcodeConfig::serialKey="serial";
const QString MUsbBarcodeConfig::useSerialKey="useserial";
const QString MUsbBarcodeConfig::interfaceKey="interfaceid";

MUsbBarcodeConfig::MUsbBarcodeConfig(MBarcodeConfiguration* cfg)
{
	QFormLayout *fl;
	setLayout(fl=new QFormLayout);
	fl->addRow(tr("Detection Interval"),minterval=new QComboBox);
	minterval->setEditable(false);
	minterval->addItem(tr("Driver Inactive"),0);
	minterval->addItem(tr("5 seconds"),5);
	minterval->addItem(tr("10 seconds"),10);
	minterval->addItem(tr("20 seconds"),20);
	minterval->addItem(tr("30 seconds"),30);
	QPushButton*p;
	fl->addRow(tr("Add Scanner"),p=new QPushButton(tr("add...")));
	connect(p,SIGNAL(clicked(bool)),this,SLOT(addScanner()));

	cfg->addTab(this,tr("USB Settings"));
}

void MUsbBarcodeConfig::saveConfig()
{

}

void MUsbBarcodeConfig::addScanner()
{

}


MUsbScannerConfig::MUsbScannerConfig(MBarcodeConfiguration* cfg, QString id)
{
	//TODO: use a better name for the tab
	cfg->addTab(this,tr("USB Scanner: %1").arg(id));
}

void MUsbScannerConfig::saveConfig()
{

}
