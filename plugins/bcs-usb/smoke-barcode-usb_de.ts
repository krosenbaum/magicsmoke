<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MUsbBarcodeConfig</name>
    <message>
        <location filename="configwidget.cpp" line="35"/>
        <source>Detection Interval</source>
        <translation>Suchintervall</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="37"/>
        <source>Driver Inactive</source>
        <translation>Treiber inaktiv</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="38"/>
        <source>5 seconds</source>
        <translation>5 Sekunden</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="39"/>
        <source>10 seconds</source>
        <translation>10 Sekunden</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="40"/>
        <source>20 seconds</source>
        <translation>20 Sekunden</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="41"/>
        <source>30 seconds</source>
        <translation>30 Sekunden</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="43"/>
        <source>Add Scanner</source>
        <translation>Barcodeleser hinzufügen</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="43"/>
        <source>add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="configwidget.cpp" line="46"/>
        <source>USB Settings</source>
        <translation>USB Einstellungen</translation>
    </message>
</context>
<context>
    <name>MUsbScannerConfig</name>
    <message>
        <location filename="configwidget.cpp" line="63"/>
        <source>USB Scanner: %1</source>
        <translation>USB Leser: %1</translation>
    </message>
</context>
</TS>
