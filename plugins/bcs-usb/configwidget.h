//
// C++ Interface: plugin for USB barcode scanners
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BCSUSB_CONFIG_H
#define MAGICSMOKE_BCSUSB_CONFIG_H

#include <QWidget>

class QComboBox;
class MBarcodeConfiguration;
class MUsbBarcodeConfig:public QWidget
{
	Q_OBJECT
	public:
		MUsbBarcodeConfig(MBarcodeConfiguration*);

		static const QString settingsGroup;
		static const QString intervalKey;
		static const QString timeoutKey;
		static const QString terminatorKey;
		static const QString layoutKey;
		static const QString nameKey;
		static const QString activeKey;
		static const QString vendorKey;
		static const QString productKey;
		static const QString serialKey;
		static const QString useSerialKey;
		static const QString interfaceKey;

	public slots:
		void saveConfig();
		void addScanner();
	private:
		QComboBox*minterval;
};

class MUsbScannerConfig:public QWidget
{
	Q_OBJECT
	public:
		MUsbScannerConfig(MBarcodeConfiguration*,QString);
	public slots:
		void saveConfig();
};

#endif
