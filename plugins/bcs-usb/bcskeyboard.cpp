//
// C++ Implementation: Barcode Plugin for USB: Keyboard Stuff
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "bcskeyboard.h"
#include <QFileInfo>
#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QDir>

MHidKeyLayout::MHidKeyLayout()
{
	loadLayout("base");
}

MHidKeyLayout::MHidKeyLayout(QString name)
{
	loadLayout(name);
}

QString MHidKeyLayout::layoutCodeName() const
{
	if(mfilename.isEmpty())return QString();
	return QFileInfo(mfilename).baseName();
}

void MHidKeyLayout::clearKeyMapping()
{
	mkeymap.clear();
}

void MHidKeyLayout::resetLayout()
{
	clearKeyMapping();
	loadLayout("base");
}

void MHidKeyLayout::setKeyMapping(unsigned char keycode, const QList<MKey> &c)
{
	if(!c.isEmpty())mkeymap.insert(keycode,c);
	else mkeymap.remove(keycode);
}

QString MHidKeyLayout::toLayoutDoc() const
{
	QString ret="#This is a generated keyboard layout\n";
	if(!mlname.isEmpty())ret+="title "+mlname+"\n";
	for(unsigned char kc:mkeymap.keys()){
		if(mkeymap[kc].isEmpty())continue;
		ret+=QString("0x%1").arg((int)kc,0,16);
		for(const MKey&k:mkeymap[kc])
			ret+=QString(" %1").arg(k.toCharName());
		ret+="\n";
	}
	return ret;
}

bool MHidKeyLayout::loadLayout(QString filename)
{
	//no slash: try to find a match
	if(!filename.contains('/')){
		if(!filename.endsWith(".kbl"))filename+=".kbl";
		//search for file in app dir
		if(QFileInfo(QCoreApplication::applicationDirPath()+"/kbdlayouts/"+filename).exists())
			filename=QCoreApplication::applicationDirPath()+"/kbdlayouts/"+filename;
		//search in resources
		else if(QFileInfo(":/kbdlayouts/"+filename).exists())
			filename=":/kbdlayouts/"+filename;
		else{
			qDebug()<<"Unable to find a match for keyboard layout"<<filename;
			return false;
		}
	}
	//load file
	QFile lf(filename);
	if(!lf.open(QIODevice::ReadOnly)){
		qDebug()<<"unable to open keyboard layout"<<filename<<"for USB HID scanners";
		return false;
	}
	return parseLayout(QString::fromUtf8(lf.readAll()));
}

bool MHidKeyLayout::parseLayout(QString content,QString filename)
{
	mfilename.clear();
	QString ntitle;
	const QStringList clst=content.split('\n',QString::KeepEmptyParts);
	for(int i=0;i<clst.size();i++){
		QString line=clst[i].trimmed();
		if(line.isEmpty())continue;
		if(line.startsWith('#'))continue;
		const QStringList toks=line.split(' ',QString::SkipEmptyParts);
		if(toks.size()<2){
			qDebug()<<"Error: incomplete line"<<(i+1)<<"in layout file"<<filename;
			return false;
		}
		if(toks[0]=="title"){
			ntitle=line.mid(line.indexOf(' ')).trimmed();
			continue;
		}
		if(toks[0]=="include"){
			if(!loadLayout(toks[1]))return false;
			else continue;
		}
		bool ok;
		const int kc=toks[0].toInt(&ok,0);
		if(!ok || kc<0 || kc>255){
			qDebug()<<"Error: illegal key code"<<toks[0]<<"on line"<<(i+1)<<"in layout file"<<filename;
			return false;
		}
		QList<MKey>keys;
		for(int i=1;i<toks.size();i++)
			keys.append(MKey::fromCharName(toks[i]));
		if(keys.size()==1){
			keys[0]=keys[0].toLower();
			keys.append(keys[0].toUpper());
		}
		mkeymap.insert(kc,keys);
	}
	mfilename=filename;
	mlname=ntitle;
	return true;
}

MKeySequence MHidKeyLayout::toKeys(const MHidKeySequence& hsq) const
{
	MKeySequence ret;
	for(MHidKeyEvent ev:hsq){
		//get modifiers
		MKey::Modifiers mod=ev.modifiers();
		//get key
		const int key=ev.lastkeycode();
		//layer?
		const int layer=mod.testFlag(MKey::AltGr)?2:(mod.testFlag(MKey::Shift)?1:0);
		//generate key
		ret.append(MKey(mkeymap.value(key).value(layer),mod));
	}
	return ret;
}

QString MHidKeyLayout::toString(const MHidKeySequence& hsq) const
{
	return toKeys(hsq).toString();
}

QMap< QString, QString > MHidKeyLayout::findLayouts()
{
	static QMap<QString,QString>laymap;
	if(laymap.size()>0)return laymap;
	//search all files
	for(QString d:QStringList()<<":/kbdlayouts"<<QCoreApplication::applicationDirPath()+"/kbdlayouts")
		for(QString fn:QDir(d).entryList(QStringList()<<"*.kbl",QDir::Files)){
			MHidKeyLayout kl(d+"/"+fn);
			if(!kl.layoutFileName().isEmpty())
				laymap.insert(kl.layoutCodeName(),kl.layoutName());
		}
	//done
	return laymap;
}

MKey::MKey(QChar c, Modifiers m)
	:mmod(m),mtype(CharKey),mchar(c)
{
	//is it space, return, tab?
	if(c==' ')mtype=Space;else
	if(c=='\n')mtype=Return;else
	if(c=='\t')mtype=Tabulator;
	//sort out shift modifier if this is a normal char
	else if(m.testFlag(Shift)){
		mmod^=Shift;
		mchar=mchar.toUpper();
	}else{
		mchar=mchar.toLower();
	}
}

MKey::MKey(const MKey& k, Modifiers m)
	:mmod(k.mmod|m),mtype(k.mtype),mchar(k.mchar)
{
	//shift correction
	if(mtype==CharKey){
		if(mmod.testFlag(Shift)){
			mmod^=Shift;
			mchar=mchar.toUpper();
		}else{
			mchar=mchar.toLower();
		}
	}
}

bool MKey::operator==(const MKey& k)
{
	//trivial: type and modifier check
	if(mtype!=k.mtype || mmod!=k.mmod)return false;
	//char compare?
	if(mtype==CharKey)return mchar==k.mchar;
	else return true;
}

QString MKey::toCharName() const
{
	QString ret;
	//release?
	if(mtype==AllKeyRelease)return "none";
	//modifiers
	if(mmod&Shift)ret+="shift-";
	if(mmod&Ctrl)ret+="ctrl-";
	if(mmod&Alt)ret+="alt-";
	if(mmod&AltGr)ret+="altgr-";
	//key
	switch(mtype){
		case Escape:return ret+"esc";
		case CapsLock:return ret+"caps";
		case NumLock:return ret+"num";
		case Backspace:return ret+"backspace";
		case Return:return ret+"return";
		case Tabulator:return ret+"tab";
		case Space:return ret+"space";
		case CharKey:return ret+mchar;
		default:return ret+"none";
	}
}

MKey MKey::fromCharName(QString n)
{
	//separate modifiers
	Modifiers mods=None;
	if(n.contains('-')){
		QStringList sl=n.split('-',QString::SkipEmptyParts);
		n=sl.takeLast();
		for(QString m:sl){
			m=m.toLower();
			if(m=="shift")mods|=Shift;
			else if(m=="ctrl")mods|=Ctrl;
			else if(m=="alt")mods|=Alt;
			else if(m=="altgr")mods|=AltGr;
			else qDebug()<<"Warning: illegal modifier"<<m;
		}
	}
	//normal key?
	if(n.size()==1)return MKey(n[0],mods);
	//special key or alias...
	n=n.toLower();
	if(n=="minus")return MKey('-',mods);
	if(n=="plus")return MKey('+',mods);
	if(n=="tab")return MKey(Tabulator,mods);
	if(n=="return")return MKey(Return,mods);
	if(n=="esc")return MKey(Escape,mods);
	if(n=="caps")return MKey(CapsLock,mods);
	if(n=="backspace")return MKey(Backspace,mods);
	if(n=="space")return MKey(Space,mods);
	if(n=="num")return MKey(NumLock,mods);
	//unknown sequence or no key
	if(n=="nokey" || n=="error" || n=="none")return MKey();
	qDebug()<<"Warning: unknown key type"<<n;
	return MKey();
}

bool MKey::isPrintable(bool ign) const
{
	//ctrl-* and alt-* cannot be printed
	if(!ign && (mmod&(Ctrl|Alt)))return false;
	//exclude some special keys
	switch(mtype){
		case CharKey:return mchar.isPrint();
		case Space:return true;
		default:return false;
	}
}

QChar MKey::toChar(bool ign) const
{
	if(!isPrintable(ign))return QChar();
	if(mtype==Space)return ' ';
	if(mtype==CharKey)return mchar;
	//hmm?
	return QChar();
}


QString MKeySequence::toString() const
{
	QString ret;
	//go through sequence and convert
	bool altmode=false;
	int altseq=0;
	for(MKey key:*this){
//                 qDebug()<<"conv"<<key.toCharName();
		//enable alt mode?
		if(key.modifiers()==MKey::Alt){
			altmode=true;
			const int d=key.toChar(true).digitValue();
			if(d>=0){
				altseq*=10;
				altseq+=d;
			}
//                         qDebug()<<"Alt: "<<d<<altseq<<key.toChar(true);
			continue;
		}
		if(altmode && !key.modifiers().testFlag(MKey::Alt)){
			altmode=false;
			if(altseq>0)
				ret+=QChar::fromLatin1(altseq);
//                         qDebug()<<"Alt compl:"<<altseq<<QChar::fromLatin1(altseq);
			altseq=0;
			//fall through and process remainder
		}
		//printable?
		if(key.isPrintable())ret+=key.toChar();
	}
	//residual alt mode?
	if(altmode && altseq>0)ret+=QChar::fromLatin1(altseq);
	//done
	return ret;
}

void MHidKeyEvent::reduce()
{
	QByteArray r;
	for(unsigned char c:mkeys)
		if(c>1)r.append(c);
	mkeys=r;
}

qint64 MHidKeySequence::msSinceLastKey() const
{
	if(isEmpty())return 0;
	return at(size()-1).timestamp().msecsTo(QDateTime::currentDateTime());
}
