TEMPLATE = lib
TARGET = msp-bcs-usb
CONFIG += plugin no_plugin_name_prefix

include (../../basics.pri)
include (../../src/smoke.pri)
include (hidapi.pri)

SOURCES += bcs-plugin.cpp configwidget.cpp bcskeyboard.cpp hidscanner.cpp
HEADERS += bcs-plugin.h configwidget.h bcskeyboard.h hidscanner.h

RESOURCES += layouts.qrc

TRANSLATIONS += smoke-barcode-usb_de.ts
