//
// C++ Interface: plugin for USB barcode scanners: Scanner Implementation
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BCSUSB_SCANNER_H
#define MAGICSMOKE_BCSUSB_SCANNER_H

#include <barcode-plugin.h>

#include <hidapi.h>

#include "bcskeyboard.h"

class QTimer;
class MHidBarcodeScanner:public MBarcodeScanner
{
	Q_OBJECT
	public:
		explicit MHidBarcodeScanner(QString path,QString setGrp,QObject* parent = 0);
		virtual ~MHidBarcodeScanner();

		QString readableName()const override;
		bool isActive()const override{return mdev!=nullptr;}

		void activate() override;
		void deactivate() override;

		bool matchPath(const QString &p){return mpath==p;}

	private slots:
		void readData();
		void processData();
	private:
		hid_device*mdev=nullptr;
		QTimer*mrdtmr=nullptr;
		QString mpath,mname;
		MHidKeySequence mseq;
		MHidKeyLayout mlayout;
		int mtimeout=0;
		MKey mterminator;
};

#endif
