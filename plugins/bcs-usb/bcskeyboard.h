//
// C++ Interface: plugin for USB barcode scanners: Scanner Implementation
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BCSUSB_KBD_H
#define MAGICSMOKE_BCSUSB_KBD_H

#include <QList>
#include <QObject>
#include <QFlag>
#include <QDateTime>
#include <QMap>

class MKey
{
	public:
		enum Modifier {
			None = 0,
			Ctrl = 1,
			Shift = 2,
			Alt = 4,
			AltGr = 8,
			GUI = 16,
		};
		Q_DECLARE_FLAGS(Modifiers,Modifier);

		enum KeyType{
			AllKeyRelease = 0,
			CharKey = 1,
			CapsLock = 2,
			NumLock = 3,
			Backspace = 4,
			Return = 5,
			Tabulator = 6,
			Space = 7,
			ModifierOnly =8,
			Escape = 9,
		};

		MKey(){}
		MKey(const MKey&k,Modifiers m);
		MKey(const MKey&)=default;
		MKey(MKey&&)=default;

		MKey(QChar,Modifiers m=None);
		MKey(KeyType t,Modifiers m=None):mmod(m),mtype(t){}

		MKey& operator=(const MKey&)=default;
		MKey& operator=(MKey&&)=default;

		bool operator==(const MKey&);
		bool operator!=(const MKey&k){return !operator==(k);}

		bool isValid()const{return mtype!=AllKeyRelease;}

		Modifiers modifiers()const{return mmod;}
		QChar toChar(bool ignoremodifier=false)const;
		bool isPrintable(bool ignoremodifier=false)const;

		///returns the encoded character name as used in layout files
		QString toCharName()const;
		///returns a key object matching the encoded character name from a layout file
		static MKey fromCharName(QString);

		MKey toLower()const{MKey r(*this);r.mchar=r.mchar.toLower();return r;}
		MKey toUpper()const{MKey r(*this);r.mchar=r.mchar.toUpper();return r;}
	private:
		Modifiers mmod=None;
		KeyType mtype=AllKeyRelease;
		QChar mchar;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(MKey::Modifiers)

class MKeySequence:public QList<MKey>
{
	public:
		MKeySequence(){}
		MKeySequence(const QList< MKey >& l):QList< MKey >(l){}
		MKeySequence(const MKeySequence& other)=default;
		MKeySequence(MKeySequence&&)=default;

		MKeySequence& operator=(const QList<MKey>&l){QList<MKey>::operator=(l);return *this;}
		MKeySequence& operator=(const MKeySequence&)=default;
		MKeySequence& operator=(MKeySequence&&)=default;

		QString toString()const;
};

class MHidKeyEvent
{
	inline static MKey::Modifiers modconv(unsigned char m){
		MKey::Modifiers r=MKey::None;
		if(m&0x11)r=MKey::Ctrl;
		if(m&0x22)r|=MKey::Shift;
		if(m&0x04)r|=MKey::Alt;
		if(m&0x40)r|=MKey::AltGr;
		if(m&0x88)r|=MKey::GUI;
		return r;
	}
	public:

		MHidKeyEvent(){}
		MHidKeyEvent(unsigned char mod,QByteArray keys):mmod(modconv(mod)),mkeys(keys){reduce();}
		MHidKeyEvent(const MHidKeyEvent&)=default;
		MHidKeyEvent(MHidKeyEvent&&)=default;

		MHidKeyEvent& operator=(const MHidKeyEvent&)=default;
		MHidKeyEvent& operator=(MHidKeyEvent&&)=default;

		MKey::Modifiers modifiers()const{return mmod;}
		QByteArray keycodes()const{return mkeys;}
		QDateTime timestamp()const{return mtime;}
		int lastkeycode()const{if(mkeys.size()>0)return mkeys[mkeys.size()-1];else return 0;}

	private:
		QDateTime mtime=QDateTime::currentDateTime();
		MKey::Modifiers mmod=MKey::None;
		QByteArray mkeys;
		void reduce();
};

class MHidKeySequence:public QList<MHidKeyEvent>
{
	public:
		MHidKeySequence(){}
		MHidKeySequence(QList< MHidKeyEvent >& other):QList< MHidKeyEvent >(other){}
		MHidKeySequence(const MHidKeySequence&)=default;
		MHidKeySequence(MHidKeySequence&&)=default;

		MHidKeySequence& operator=(const MHidKeySequence&)=default;
		MHidKeySequence& operator=(MHidKeySequence&&)=default;

		qint64 msSinceLastKey()const;
};

class MHidKeyLayout
{
	public:
		///loads the base layout only
		MHidKeyLayout();
		///loads the specified layout
		explicit MHidKeyLayout(QString name);
		MHidKeyLayout(const MHidKeyLayout&)=default;
		MHidKeyLayout(MHidKeyLayout&&)=default;

		MHidKeyLayout& operator=(const MHidKeyLayout&)=default;
		MHidKeyLayout& operator=(MHidKeyLayout&&)=default;

		MKeySequence toKeys(const MHidKeySequence&)const;
		QString toString(const MHidKeySequence&)const;

		///attempts to load a layout file, normally the name mentioned
		///should not contain a complete path or the suffix .kbl - the
		///implementation will automatically search for a matching file;
		///if it does contain a slash it is assumed to be a complete file name
		bool loadLayout(QString filename);
		bool parseLayout(QString content,QString filename=QString());
		void resetLayout();

		QString toLayoutDoc()const;

		void setKeyMapping(unsigned char keycode,const QList<MKey> &c);
		void clearKeyMapping();
		
		///returns the human readable name of the layout
		QString layoutName()const{return mlname;}
		///returns the iso code of the layout (base of the file name)
		///or an empty string if this is a custom layout
		QString layoutCodeName()const;
		///returns the full file name of the layout
		QString layoutFileName()const{return mfilename;}

		void setLayoutName(QString l){mlname=l;}

		///returns true if this is a populated layout
		bool isValid()const{return mkeymap.size()>0;}

		static QMap<QString,QString> findLayouts();
	private:
		QString mfilename,mlname;
		QMap<unsigned char,QList<MKey>>mkeymap;
};

#endif
