//
// C++ Interface: plugin for USB barcode scanners
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BARCODE_PLUGIN_HID_H
#define MAGICSMOKE_BARCODE_PLUGIN_HID_H

#include "barcode-plugin.h"

#include <QPointer>

class QTimer;
class MHidBarcodeScanner;
class QSettings;
class MHidBarcodePlugin:public QObject,public MBarcodePlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID MBarcodePlugin_IID)
	Q_INTERFACES(MBarcodePlugin)
	public:
		MHidBarcodePlugin();
		virtual ~MHidBarcodePlugin();
		virtual void configure(MBarcodeConfiguration*)override;
	private slots:
		void detectScanners();
		void restartDetect();
		void removeScanner(QObject*);
	private:
		bool misopen;
		QTimer*mdetecttmr=nullptr;
		struct Config{
			int vendor=0,product=0,iface=0;
			bool useserial=false,isactive=false;
			QString serial,group;
			Config(){}
			Config(const Config&)=default;
			Config(Config&&)=default;
			Config(QSettings&,QString);
			Config& operator=(const Config&)=default;
			Config& operator=(Config&&)=default;
		};
		QList<Config>mconfig;
		Config findCfgMatch(int vendor,int product,int iface,QString serial);
		QList<QPointer<MHidBarcodeScanner>>mscanners;
		MHidBarcodeScanner*findScanner(QString path);
};

#endif
