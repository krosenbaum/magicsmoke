#HIDAPI settings

HIDAPIDIR = $$PWD/../../hidapi

INCLUDEPATH += $$HIDAPIDIR/hidapi

CONFIG += link_pkgconfig

packagesExist(hidapi-hidraw) {
	PKGCONFIG += hidapi-hidraw
	message("Pkg-Config: using HIDAPI with HIDraw interface")
} else {
  packagesExist(hidapi) {
	PKGCONFIG += hidapi
	message("Pkg-Config: using HIDAPI with default interface")
  } else {
    packagesExist(hidapi-libusb) {
	PKGCONFIG += hidapi-libusb
	message("Pkg-Config: using HIDAPI with libusb interface")
    } else {
      linux {
        SOURCES += $$HIDAPIDIR/linux/hid.c
        LIBS += -ludev
	message("Using built-in HIDAPI for Linux")
      }
      win32 {
        SOURCES += $$HIDAPIDIR/windows/hid.c
        LIBS += -lsetupapi
	message("Using built-in HIDAPI for Windows")
      }
      mac {
        SOURCES += $$HIDAPIDIR/mac
        #from mac/Makefile-manual, not sure how to use this:
        #LIBS += -framework IOKit -framework CoreFoundation
	message("Using built-in HIDAPI for MacOS/X")
      }
    }
  }
}
