//
// C++ Implementation: code39
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "code39.h"

#include <QMap>
#include <QPainter>

//order of characters for checksum calculation and list of allowed chars
static const QString c39mod="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%";

//init pixel map
static QMap<char,QString> initmap()
{
	QMap<char,QString>map;
	// 0=black 1=white pixel
	map.insert('*',"0111010001000101");
	map.insert('-',"0111010100010001");
	map.insert('$',"0111011101110101");
	map.insert('%',"0101110111011101");
	map.insert(' ',"0111000101000101");
	map.insert('.',"0001110101000101");
	map.insert('/',"0111011101011101");
	map.insert('+',"0111010111011101");
	map.insert('0',"0101110001000101");
	map.insert('1',"0001011101010001");
	map.insert('2',"0100011101010001");
	map.insert('3',"0001000111010101");
	map.insert('4',"0101110001010001");
	map.insert('5',"0001011100010101");
	map.insert('6',"0100011100010101");
	map.insert('7',"0101110100010001");
	map.insert('8',"0001011101000101");
	map.insert('9',"0100011101000101");
	map.insert('A',"0001010111010001");
	map.insert('B',"0100010111010001");
	map.insert('C',"0001000101110101");
	map.insert('D',"0101000111010001");
	map.insert('E',"0001010001110101");
	map.insert('F',"0100010001110101");
	map.insert('G',"0101011100010001");
	map.insert('H',"0001010111000101");
	map.insert('I',"0100010111000101");
	map.insert('J',"0101000111000101");
	map.insert('K',"0001010101110001");
	map.insert('L',"0100010101110001");
	map.insert('M',"0001000101011101");
	map.insert('N',"0101000101110001");
	map.insert('O',"0001010001011101");
	map.insert('P',"0100010001011101");
	map.insert('Q',"0101010001110001");
	map.insert('R',"0001010100011101");
	map.insert('S',"0100010100011101");
	map.insert('T',"0101000100011101");
	map.insert('U',"0001110101010001");
	map.insert('V',"0111000101010001");
	map.insert('W',"0001110001010101");
	map.insert('X',"0111010001010001");
	map.insert('Y',"0001110100010101");
	map.insert('Z',"0111000100010101");
	return map;
}
//map characters on pixel codes
static const QMap<char,QString>c39map=initmap();

//calculate checksum
static char code39mod(QString str)
{
	int sum=0;
	for(int i=0;i<str.size();i++)
		sum+=c39mod.indexOf(str[i]);
	return c39mod[sum%43].toLatin1();
}

QImage code39(QString str)
{
	//check string, return empty pixmap if invalid
	str=str.toUpper();
	for(int i=0;i<str.size();i++){
		if(!c39mod.contains(str[i])){
			qDebug("Warning: invalid barcode requested: %s.",str.toLatin1().data());
			return QImage();
		}
	}
	//create 01-list
	QString rstr=c39map['*'];
	for(int i=0;i<str.size();i++)
		rstr+=c39map[str[i].toLatin1()];
	rstr+=c39map[code39mod(str)];
	rstr+=c39map['*'];
// 	qDebug("Code-39, encoding: %s -> %c",str.toLatin1().data(),code39mod(str));
	//define xpm
	const char *xpm[4];
    QByteArray xpmsz=QString("%1 1 2 1").arg(rstr.size()).toLatin1();
	xpm[0]=xpmsz.data();
	xpm[1]="0\tg #000000";
	xpm[2]="1\tg #FFFFFF";
	QByteArray xpmc=rstr.toLatin1();
	xpm[3]=xpmc.data();
	//create pixmap
	return QImage(xpm);
}
