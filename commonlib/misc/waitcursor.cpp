//
// C++ Implementation: waitcursor
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2013
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "waitcursor.h"
#include <MTransaction>

#include <QApplication>
#include <QCursor>

WaitCursor::WaitCursor()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
}

WaitCursor::~WaitCursor()
{
	QApplication::restoreOverrideCursor();
}

static int transactionCursor()
{
        MTransaction::setStartStopActions(
                []{QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));},
                []{QApplication::restoreOverrideCursor();}
        );
        return 0;
}

static int tcdummy=transactionCursor();