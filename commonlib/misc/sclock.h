//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SCLOCK_H
#define MAGICSMOKE_SCLOCK_H

#include <QLabel>

#include "commonexport.h"

class QMenu;
class QAction;


class MAGICSMOKE_COMMON_EXPORT MServerClock:public QLabel
{
	Q_OBJECT
	public:
		MServerClock();
	private slots:
		void updateClock();
		void contextMenu(const QPoint&);
		void showInfo();
	protected:
		virtual void mouseDoubleClickEvent ( QMouseEvent * event );
	private:
		QMenu*contextmenu;
		QAction*showfull,*showtime,*showdate,*showiso;
};

#endif
