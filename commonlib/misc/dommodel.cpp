//
// C++ Implementation: DOM model
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2012-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "dommodel.h"

#include <QDomDocument>
#include <QDomElement>
#include <QMap>
#include <QSet>

#include <QDebug>

//switch this to en-/dis-able debug messages
static inline
// QDebug mDebug(){return QDebug(QtDebugMsg);}
QNoDebug mDebug(){return QNoDebug();}

#include <DPtr>

class DPTR_CLASS_NAME(MDomItemModel)
{
	public:
		//nextid: counter for internal DOM node ids
		//docid: id of the document node
		int nextidx,docidx;
		//cache structure that holds one node each
		struct DomCache{
			QDomNode node;
			QList<int>children;
			int parent;
			DomCache():parent(-1){}
			DomCache(const DomCache&)=default;
			DomCache(const QDomNode&node_,int parent_=-1)
			:node(node_),parent(parent_){}
		};
		//cache of all DOM nodes, index is the internal ID
		QMap<int,DomCache>domCache;
		//set of node types that are shown
		QSet<QDomNode::NodeType>showType;
		//functors for generation of the display
		QMap<int,MDomModelFunctor> contentFromNode;
		//config for element display
		int elemAttrLen,elemTextLen;
		bool showtypeLabel;
		//header data
		QMap<QPair<int,int>,QVariant>horizHeader,vertHeader;
		
		Private();
		
		int buildCache(const QDomNode&,int p=-1);
		void removeNode(int nodeid,bool insideRecursion=false);
};

DEFINE_DPTR(MDomItemModel);

MDomItemModel::MDomItemModel(QObject* parent)
	: QAbstractItemModel(parent)
{
}

MDomItemModel::Private::Private()
	:nextidx(1),docidx(-1)
{
	showType<<QDomNode::ElementNode;
	elemAttrLen=20;elemTextLen=30;
	showtypeLabel=true;
}


MDomItemModel::MDomItemModel(const QDomDocument& doc, QObject* parent)
	: QAbstractItemModel(parent)
{
	setDomDocument(doc);
}

void MDomItemModel::setDomDocument(const QDomDocument& doc)
{
	beginResetModel();
	d->domCache.clear();
	d->docidx=d->buildCache(doc);
	mDebug()<<"DOM model setting new document - found"<<d->domCache.size()<<"DOM nodes, root node is"<<d->docidx;
	endResetModel();
}

int MDomItemModel::Private::buildCache(const QDomNode& node,int parent)
{
	if(node.isNull())return -1;
	//create this node
	int idx=nextidx++;
	DomCache dc(node,parent);
	//recursively add children
	QDomNodeList nl=node.childNodes();
	mDebug()<<"node"<<idx<<"type"<<node.nodeType()<<"name"<<node.nodeName()<<"has"<<nl.size()<<"children";
	for(int i=0;i<nl.size();i++)
		dc.children.append(buildCache(nl.at(i),idx));
	//insert this node
	domCache.insert(idx,dc);
	//return index
	return idx;
}

void MDomItemModel::showNodeTypes(const QSet< QDomNode::NodeType >& nt)
{
	if(nt==d->showType)return;
	beginResetModel();
	d->showType=nt;
	if(!d->showType.contains(QDomNode::ElementNode))
		d->showType.insert(QDomNode::ElementNode);
	mDebug()<<"DOM Model - showing these types"<<d->showType;
	endResetModel();
}

void MDomItemModel::addShowNodeType(QDomNode::NodeType nt)
{
	if(d->showType.contains(nt))return;
	beginResetModel();
	d->showType.insert(nt);
	mDebug()<<"DOM Model - showing these types"<<d->showType;
	endResetModel();
}

void MDomItemModel::addShowNodeTypes(const QSet< QDomNode::NodeType >& nt)
{
	if(nt.isEmpty())return;
	beginResetModel();
	foreach(QDomNode::NodeType n,nt)
		if(!d->showType.contains(n))
			d->showType.insert(n);
	mDebug()<<"DOM Model - showing these types"<<d->showType;
	endResetModel();
}

void MDomItemModel::showAllNodeTypes()
{
	showNodeTypes(QSet<QDomNode::NodeType>()
		<<QDomNode::ElementNode
		<<QDomNode::AttributeNode
		<<QDomNode::TextNode
		<<QDomNode::CDATASectionNode
		<<QDomNode::EntityReferenceNode
		<<QDomNode::EntityNode
		<<QDomNode::ProcessingInstructionNode
		<<QDomNode::CommentNode
		<<QDomNode::DocumentNode
		<<QDomNode::DocumentTypeNode
		<<QDomNode::DocumentFragmentNode
		<<QDomNode::NotationNode
		<<QDomNode::BaseNode
	);
}

QSet< QDomNode::NodeType > MDomItemModel::shownNodeTypes()const
{
	return d->showType;
}

void MDomItemModel::showElementProperties(int ma,int mt)
{
	//ignore if there is no change
	if(d->elemAttrLen == ma && d->elemTextLen == mt)return;
	//send a model reset if this has an actual effect, ie. if we do the rendering
	bool b=!d->contentFromNode.contains(Qt::DisplayRole);
	if(b)beginResetModel();
	//change params
	d->elemAttrLen=ma;
	d->elemTextLen=mt;
	//tell the view
	if(b)endResetModel();
}

void MDomItemModel::showTypeLabel(bool show)
{
	//ignore if there is no change
	if(d->showtypeLabel == show)return;
	//send a model reset if this has an actual effect, ie. if we do the rendering
	bool b=!d->contentFromNode.contains(Qt::DisplayRole);
	if(b)beginResetModel();
	//change params
	d->showtypeLabel=show;
	//tell the view
	if(b)endResetModel();
}

QDomNode MDomItemModel::node(const QModelIndex& index) const
{
	if(!index.isValid()){
		if(!d->domCache.contains(d->docidx)) return QDomNode();
		else return d->domCache[d->docidx].node;
	}
	if(!d->domCache.contains(index.internalId()))return QDomNode();
	QDomNode node=d->domCache[index.internalId()].node;
	qDebug()<<"returning node"<<index.internalId()<<"type"<<node.nodeType()<<node.nodeName()<<node.nodeValue();
	return node;
}

void MDomItemModel::setNode(const QModelIndex& index, const QDomNode& node)
{
	//get the node id
	if(!index.isValid())return;
	int nid=index.internalId();
	if(nid<=0 || !d->domCache.contains(nid))return;
	int pid=d->domCache[nid].parent;
	if(pid<=0 || !d->domCache.contains(pid))return;
	//invalidate children
	int csize=d->domCache[nid].children.size()-1;
        if(csize<0)csize=0;
	beginRemoveRows(index,0,csize);
	for(int n:d->domCache[nid].children)d->removeNode(n);
	d->domCache[nid].children.clear();
	endRemoveRows();
	//replace the node in DOM
	qDebug()<<"replacing node"<<nid<<"at"<<index<<"old"<<d->domCache[nid].node.nodeValue()<<"new"<<node.nodeValue()<<node.nodeType();
	QDomNode nnode=node.cloneNode();
	d->domCache[pid].node.replaceChild(nnode,d->domCache[nid].node);
	d->domCache[nid].node=nnode;
	qDebug()<<"   new nodes type is"<<nnode.nodeType()<<"value"<<nnode.nodeValue();
	//add the new child-nodes
	QDomNodeList nl=nnode.childNodes();
	beginInsertRows(index,0,nl.size()?nl.size()-1:0);
	for(int i=0;i<nl.size();i++)
		d->domCache[nid].children.append(d->buildCache(nl.at(i),nid));
	endInsertRows();
	//tell the view we are finished
	dataChanged(index,index);
}

void MDomItemModel::insertNodes(const QDomNodeList& newnodes, const QModelIndex& parent, int row)
{
	QList<QDomNode>nnl;
	for(int i=0;i<newnodes.size();i++)nnl<<newnodes.at(i);
	insertNodes(nnl,parent,row);
}

void MDomItemModel::insertNode(const QDomNode& newnode, const QModelIndex& parent, int row)
{
	insertNodes(QList<QDomNode>()<<newnode,parent,row);
}

void MDomItemModel::insertNodes(const QList< QDomNode >& newnodes, const QModelIndex& parent, int row)
{
	if(newnodes.size()<1)return;
	//check parent validity
	if(!parent.isValid())return;
	int pid=parent.internalId();
	if(!d->domCache.contains(pid))return;
	//only add to tags (TODO: there are a few other node types that can have children)
	if(!d->domCache[pid].node.isElement())return;
	//get the parent and a clone of the new node
	QDomElement pn=d->domCache[pid].node.toElement();
	const int rcnt=rowCount(parent);
	//handle appending
	if(row<0 || row>=rcnt){
		const int rmax=rcnt+newnodes.size()-1;
		beginInsertRows(parent,rcnt,rmax);
		for(QDomNode nd:newnodes){
			QDomNode nn=nd.cloneNode();
			pn.appendChild(nn);
			d->domCache[pid].children.append(d->buildCache(nn,pid));
		}
		endInsertRows();
		return;
	}
	//handle inserting
	//get current occupant of this row
	QDomNode sibl=node(index(row,0,parent));
	const int rmax=row+newnodes.size()-1;
	beginInsertRows(parent,row,rmax);
	//insert
	for(QDomNode nd:newnodes){
		QDomNode nn=nd.cloneNode();
                qDebug()<<"Attempting insert with"<<nn.nodeName()<<nn.nodeValue();
		nn=pn.insertBefore(nn,sibl);
                if(nn.isNull())
                        qDebug()<<"!!!!ARG insert of DOM node failed! into"<<pn.nodeName()<<"before"<<(int)sibl.nodeType()<<sibl.nodeName()<<sibl.nodeValue()<<"which is child of"<<sibl.parentNode().nodeName();
		d->domCache[pid].children.insert(row,d->buildCache(nn,pid));
	}
	endInsertRows();
}

int MDomItemModel::columnCount(const QModelIndex&) const
{
	return 1;
}

QDomDocument MDomItemModel::domDocument() const
{
	if(d->docidx>=0 && d->domCache.contains(d->docidx))
		return d->domCache[d->docidx].node.cloneNode().toDocument();
	else
		return QDomDocument();
}

QVariant MDomItemModel::data(const QModelIndex& index, int role) const
{
	if(role==DomNodeRole)return QVariant::fromValue(node(index));
	//root index
	QDomNode node;
	if(!index.isValid()){
		if(d->domCache.contains(d->docidx))
			node=d->domCache[d->docidx].node;
	}else{
		if(d->domCache.contains(index.internalId()))
			node=d->domCache[index.internalId()].node;
	}
	//check node
	if(node.isNull())return QVariant();
	//try to find callback
	if(d->contentFromNode.contains(role))return d->contentFromNode[role](node);
	//no callback: display role
	if(role==Qt::DisplayRole)return displayFromNode(node);
	//fall back
	return QVariant();
}

QVariant MDomItemModel::displayFromNode(const QDomNode& node) const
{
	if(node.isNull())return "NULL node";
	switch(node.nodeType()){
		case QDomNode::ElementNode:{
			QString r;
			if(d->showtypeLabel)r+="tag ";
			r+="<"+node.nodeName();
			QString a,t;
			//append attribs
			if(d->elemAttrLen>=0 && node.hasAttributes()){
				auto attrs=node.attributes();
				QString a;
				for(int i=0;i<attrs.size();i++){
					if(i)a+=" ";
					QDomAttr att=attrs.item(i).toAttr();
					a+=att.nodeName()+"=\""+att.nodeValue()+"\"";
				}
				if(a.size()>d->elemAttrLen){
					a.truncate(d->elemAttrLen);
					a+="...";
				}
				r+=" "+a.trimmed();
			}
			r+=">";
			//append text
			if(d->elemTextLen>0){
				QString t;
				QDomNodeList nl=node.childNodes();
				for(int i=0;i<nl.size();i++){
					QDomNode nd=nl.at(i);
					if(nd.isCharacterData()){
						if(t.size()>0)t+=" ";
						t+=QString(nd.nodeValue()).replace('\n',' ');
					}
				}
				if(t.size()>d->elemTextLen){
					t.truncate(d->elemTextLen);
					t+="...";
				}
				r+=t.trimmed();
			}
			return r;
		}
		case QDomNode::AttributeNode:
			return (d->showtypeLabel?"attribute ":"")+node.nodeName()+"="+node.nodeValue();
		case QDomNode::TextNode:
		case QDomNode::CDATASectionNode:
			return QString(d->showtypeLabel?"text ":"")+"\""+QString(node.nodeValue()).replace('\n',' ')+"\"";
		case QDomNode::CommentNode:
			return QString(d->showtypeLabel?"comment ":"")+"<!-- "+node.nodeValue()+" -->";
		default:
			return (d->showtypeLabel?("node ("+QString::number(node.nodeType())+"): "):QString())+node.nodeName()+" - \""+node.nodeValue()+"\"";
	}
}

int MDomItemModel::rowCount(const QModelIndex& parent) const
{
	int pid=-1;
	if(!parent.isValid())pid=d->docidx;
	else pid=parent.internalId();
	if(!d->domCache.contains(pid)){
		mDebug()<<"DOM Model - index"<<parent.row()<<parent.column()<<parent.internalId()<<"does not exist";
		return 0;
	}
	int ret=0;
	foreach(int cid,d->domCache[pid].children)
		if(d->domCache.contains(cid) && d->showType.contains(d->domCache[cid].node.nodeType()))
			ret++;
	mDebug()<<"DOM Model - index"<<parent.row()<<parent.column()<<pid<<"has"<<ret<<"rows out of"<<d->domCache[pid].children.size()<<"items";
	return ret;
}

QModelIndex MDomItemModel::index(int row, int column, const QModelIndex& parent) const
{
	//only col 0 is valid for now
	if(column!=0){
		mDebug()<<"DOM Model - index for wrong column requested"<<row<<column;
		return QModelIndex();
	}
	//discard invalid rows
	if(row<0){
		mDebug()<<"DOM Model - index for negative row requested"<<row<<column;
		return QModelIndex();
	}
	//validate/find parent
	int pid=-1;
	if(!parent.isValid())pid=d->docidx;
	else pid=parent.internalId();
	if(pid<=0 || !d->domCache.contains(pid)){
		mDebug()<<"DOM Model - index for invalid node requested"<<row<<column<<pid<<"parent is"<<parent.row()<<parent.column()<<parent.internalId();
		return QModelIndex();
	}
	//try to find element
	if(row>=d->domCache[pid].children.size()){
		mDebug()<<"DOM Model - index of empty parent requested, parent:"<<parent.row()<<parent.column()<<parent.internalId();
		return QModelIndex();
	}
	int rc=0;
	foreach(int cid,d->domCache[pid].children){
		if(!d->showType.contains(d->domCache[cid].node.nodeType()))
			continue;
		if(row==rc++){
			mDebug()<<"DOM Model - creating index"<<row<<0<<cid<<"of parent"<<parent.row()<<parent.column()<<parent.internalId();
			return createIndex(row,0,cid);
		}
	}
	//failed
	mDebug()<<"DOM Model - requested index out of range"<<row<<column<<"of parent"<<parent.row()<<parent.column()<<parent.internalId();
	return QModelIndex();
}

QModelIndex MDomItemModel::parent(const QModelIndex& index) const
{
	if(!index.isValid() || !d->domCache.contains(index.internalId())){
		mDebug()<<"DOM Model - requesting parent of"<<index.row()<<index.column()<<index.internalId()<<"- invalid item";
		return QModelIndex();
	}
	//find parent
	int pid=d->domCache[index.internalId()].parent;
	if(pid<=0 || !d->domCache.contains(pid)){
		mDebug()<<"DOM Model - requesting parent of"<<index.row()<<index.column()<<index.internalId()<<"- invalid parent";
		return QModelIndex();
	}
	//find grand-parent
	int gpid=d->domCache[pid].parent;
	//is grand-parent invalid? (parent must be the root node)
	if(gpid<=0 || !d->domCache.contains(gpid)){
		mDebug()<<"DOM Model - requesting parent of"<<index.row()<<index.column()<<index.internalId()<<"- invalid grand-parent, cannot search";
		return QModelIndex();
	}
	//find row of the parent inside grand-parent
	int row=0;
	foreach(int nid,d->domCache[gpid].children){
		if(nid==pid)break;
		if(d->showType.contains(d->domCache[nid].node.nodeType()))row++;
	}
	mDebug()<<"DOM Model - requesting parent of"<<index.row()<<index.column()<<index.internalId()<<"- parent is"<<row<<0<<pid;
	return createIndex(row,0,pid);
}

void MDomItemModel::removeNode(const QModelIndex& index)
{
	if(!index.isValid())return;
	int nid=index.internalId();
	if(!d->domCache.contains(nid))return;
	//find parent index and warn the view
	QModelIndex pidx=parent(index);
	if(pidx.isValid())beginRemoveRows(pidx,index.row(),index.row());
	else beginResetModel();
	//remove the node
	d->removeNode(nid);
	//tell the view we are done
	if(pidx.isValid())endRemoveRows();
	else endResetModel();
}

void MDomItemModel::Private::removeNode(int nid,bool inrecurs)
{
	//sanity check
	if(!domCache.contains(nid))return;
	//remove main node from model
	if(!inrecurs){
		int pid=domCache[nid].parent;
		if(domCache.contains(pid)){
			domCache[pid].node.removeChild(domCache[nid].node);
			domCache[pid].children.removeAll(nid);
		}
	}
	//remove children
	foreach(int cid,domCache[nid].children)
		removeNode(cid,true);
	//remove self
	domCache.remove(nid);
}


bool MDomItemModel::removeRows(int row, int count, const QModelIndex& parent)
{
	//sanity check
	if(row<0 || count<=0)return false;
	//find the parent
	int pid=-1;
	if(parent.isValid())pid=parent.internalId();
	else pid=d->docidx;
	if(!d->domCache.contains(pid))
		return false;
	//check the parent has those children
	if(rowCount(parent)<(row+count))
		return false;
	//warn the view
	beginRemoveRows(parent,row,row+count-1);
	//find the children's IDs
	QList<int>cids;
	int cnt=0;
	foreach(int cid,d->domCache[pid].children){
		if(cnt>=(row+count))break;
		if(!d->showType.contains(d->domCache[cid].node.nodeType()))
			continue;
		if(cnt++>=row)cids<<cid;
	}
	//remove them
	foreach(int cid,cids)d->removeNode(cid);
	//update view
	endRemoveRows();
	return true;
}

void MDomItemModel::setContentFromNodeCall(int role,MDomModelFunctor f)
{
	if(role==DomNodeRole)return;
	beginResetModel();
	if(f)
		d->contentFromNode.insert(role,f);
	else
		d->contentFromNode.remove(role);
	endResetModel();
}

QVariant MDomItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(orientation==Qt::Horizontal)
		return d->horizHeader.value(QPair<int,int>(section,role));
	else
		return d->vertHeader.value(QPair<int,int>(section,role));
}

bool MDomItemModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role)
{
	if(orientation==Qt::Horizontal)
		d->horizHeader.insert(QPair<int,int>(section,role),value);
	else
		d->vertHeader.insert(QPair<int,int>(section,role),value);
	return true;
}
