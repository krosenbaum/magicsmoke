//
// C++ Interface: code39
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include <QImage>
#include <QString>

#include "commonexport.h"

/**Takes a string and converts it into a code-39 bar code.
Code-39 allows letters (case-insensitive), digits, spaces and the special chars "-.$/+%".
The bar code pixmap will be 1 pixel high and 16 pixels wide for each character (plus start/stop character and checksum character) - it needs to be scaled up to fit the intended size.*/
MAGICSMOKE_COMMON_EXPORT QImage code39(QString);
