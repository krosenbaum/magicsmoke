//
// C++ Interface: debug
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_DEBUG_H
#define MAGICSMOKE_DEBUG_H

#include "commonexport.h"

MAGICSMOKE_COMMON_EXPORT void initDebug(const QString&dataDir);

#endif
