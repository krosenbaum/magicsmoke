HEADERS += \
	$$PWD/debug.h \
	$$PWD/waitcursor.h \
	$$PWD/sclock.h \
	$$PWD/dommodel.h \
	$$PWD/barcode-plugin.h

SOURCES += \
	$$PWD/code39.cpp \
	$$PWD/debug.cpp \
	$$PWD/waitcursor.cpp \
	$$PWD/sclock.cpp \
	$$PWD/dommodel.cpp \
	$$PWD/barcode-plugin.cpp

INCLUDEPATH += $$PWD
