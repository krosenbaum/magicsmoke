//
// C++ Implementation: debug
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2012
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "debug.h"
// #include "main.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>

static QFile*mylogFile=0;


static void mymsghandler(QtMsgType,const QMessageLogContext&, const QString&msg)
{
        if(mylogFile){
		QByteArray data;
		//QDateTime::toString requires an active app object, otherwise it crashes
		if(qApp)
			data=QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz").toLatin1() +" ";
		data+=msg.toLatin1()+"\n";
                mylogFile->write(data);
                mylogFile->flush();
        }
#ifdef Q_OS_UNIX
        fprintf(stderr,"%s\n",msg.toLatin1().data());
#endif
}

void initDebug(const QString&dataDir)
{
    if(QCoreApplication::instance()->arguments().contains("-nolog")){
        qDebug()<<"-nolog argument was given, will not create a log file";
	return;
    }
	//create new log file
	QFileInfo appfi(qApp->applicationFilePath());
	QDir(dataDir).mkpath("debuglog");
        mylogFile=new QFile(dataDir+"/debuglog/"+appfi.baseName()+"-"+QDateTime::currentDateTime().toString("yyyy-MM-dd_hh.mm.ss.zzz")+".log");
        //...open it
        if(mylogFile->open(QIODevice::WriteOnly|QIODevice::Append|QIODevice::Text)){
        	//install as default log
        	qDebug("Installing debuglog in %s",mylogFile->fileName().toLatin1().data());
                qInstallMessageHandler(mymsghandler);
        }else{
        	//hmm, failed to open, well hope that stderr is working...
                delete mylogFile;
                mylogFile=0;
                qDebug("Failed to install debuglog.");
        }
        qDebug()<<"Log file opened by"<<appfi.absoluteFilePath();
        //delete old logs (older than 30 days)
	QStringList fll=QDir(dataDir+"/debuglog").entryList(QDir::Files);
	QDateTime old=QDateTime::currentDateTime().addDays(-30);
	for(int i=0;i<fll.size();i++){
		QFile f(dataDir+"/debuglog/"+fll[i]);
		if(QFileInfo(f).lastModified()<=old)
			f.remove();
	}
}
