//
// C++ Implementation: Barcode Plugin Basics
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "barcode-plugin.h"

#include <QBoxLayout>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QPluginLoader>
#include <QPointer>
#include <QPushButton>
#include <QTabWidget>

static QList<QPointer<MBarcodeScanner> >allscanners;
static QList<MBarcodePlugin* >allplugins;

MBarcodeConfiguration::MBarcodeConfiguration(QWidget* parent): QDialog(parent)
{
        setWindowTitle(tr("Configure Barcode Plugins"));
        
        QVBoxLayout*vl;
        QHBoxLayout*hl;
        QPushButton*p;
        setLayout(vl=new QVBoxLayout);
        vl->addWidget(mtab=new QTabWidget,1);
        vl->addLayout(hl=new QHBoxLayout);
        hl->addStretch(1);
        hl->addWidget(p=new QPushButton(tr("&Save")));
        connect(p,SIGNAL(clicked(bool)),this,SLOT(accept()));
        hl->addWidget(p=new QPushButton(tr("&Cancel")));
        connect(p,SIGNAL(clicked(bool)),this,SLOT(reject()));
        
        setSizeGripEnabled(true);
        
        for(MBarcodePlugin*plug:allplugins)
                if(plug)plug->configure(this);
}

void MBarcodeConfiguration::addTab(QWidget* w, QString t)
{
        mtab->addTab(w,t);
}



MBarcodeHub* MBarcodeHub::instance()
{
        static MBarcodeHub hub;
        return &hub;
}

MBarcodeHub::MBarcodeHub(): QObject()
{
        qDebug()<<"instantiating barcode hub";
}

MBarcodeHub::~MBarcodeHub()
{
        qDebug()<<"deleting barcode hub";
}

MBarcodeScanner::MBarcodeScanner(QObject* parent): QObject(parent)
{
}

void MBarcodePlugin::registerScanner(MBarcodeScanner* scanner)
{
        if(!scanner)return;
        allscanners.append(scanner);
        QObject::connect(scanner,SIGNAL(newBarcode(QString)), MBarcodeHub::instance(),SIGNAL(newBarcode(QString)));
}

MBarcodePlugin::~MBarcodePlugin()
{}

QList< MBarcodeScanner* > MBarcodeHub::allScanners()
{
        QList<MBarcodeScanner*>ret;
        for(MBarcodeScanner*s:allscanners)
                if(s)ret<<s;
        //TODO: prune the allscanners list
        return ret;
}

void MBarcodeHub::initialize()
{
        //get install dir
        const QString dir=QCoreApplication::applicationDirPath();
        //go through all potential barcode plugins (msp-bcs-*.so or ...*.dll)
        for(const QString file:QDir(dir).entryList(QStringList()<<"msp-bcs-*",QDir::Files)){
                //attempt to load it
                qDebug()<<"Attempting to load potential barcode plugin"<<file;
                QPluginLoader pl(dir+"/"+file);
                const bool r=pl.load();
                qDebug()<<"   ...."<<file<<(r?"successfully loaded.":"not a valid plugin.");
                if(r){
                        QObject*oplug=pl.instance();
                        if(!oplug)continue;
                        MBarcodePlugin*plug=qobject_cast< MBarcodePlugin* >(oplug);
                        if(!plug)continue;
                        allplugins<<plug;
                        connect(oplug,SIGNAL(destroyed(QObject*)), this,SLOT(removePlugin(QObject*)));
                }
        }
}

void MBarcodeHub::removePlugin(QObject* obj)
{
        if(!obj)return;
        allplugins.removeAll(qobject_cast< MBarcodePlugin* >(obj));
}
