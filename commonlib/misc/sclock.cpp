//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "sclock.h"

#include <QActionGroup>
#include <QDebug>
#include <QDialog>
#include <QFormLayout>
#include <QMenu>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPushButton>
#include <QTimer>
#include <QBoxLayout>

#include <TimeStamp>
#include "misc.h"

MServerClock::MServerClock()
{
	QTimer *tm=new QTimer(this);
	tm->setSingleShot(false);
	tm->start(1000);
	connect(tm,SIGNAL(timeout()),this,SLOT(updateClock()));
	
	connect(this,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(contextMenu(QPoint)));
	setContextMenuPolicy(Qt::CustomContextMenu);
	setFrameStyle(QFrame::Panel|QFrame::Sunken);
	
	contextmenu=new QMenu(this);
	contextmenu->addAction(tr("Show Format Info"),this,SLOT(showInfo()));
	contextmenu->addSeparator();
	QActionGroup*ag=new QActionGroup(this);
	QAction*ac=ag->addAction(tr("Show Full Time"));
	ac->setCheckable(true);ac->setChecked(true);
	ac->setData(tr("%W, %D %N %Y %a:%I %p %t","full time format"));
	contextmenu->addAction(ac);showfull=ac;
	ac=ag->addAction(tr("Show Date Only"));ac->setCheckable(true);
	ac->setData(tr("%w, %M/%D/%Y","date only format"));
	contextmenu->addAction(ac);showdate=ac;
	ac=ag->addAction(tr("Show Time Only"));ac->setCheckable(true);
	ac->setData(tr("%a:%I %p %t","time only format"));
	contextmenu->addAction(ac);showtime=ac;
	ac=ag->addAction(tr("Show Short Date/Time"));ac->setCheckable(true);
	ac->setData(tr("%w %Y-%M-%D %h:%I %t","ISO like short time format"));
	contextmenu->addAction(ac);showiso=ac;
	connect(ag,SIGNAL(triggered(QAction*)),this,SLOT(updateClock()));
	
	updateClock();
}

void MServerClock::updateClock()
{
	//get time and format
	TimeStamp ts=TimeStamp::now();
	QString fm=showfull->data().toString();
	if(showdate->isChecked())fm=showdate->data().toString();
	if(showtime->isChecked())fm=showtime->data().toString();
	if(showiso->isChecked())fm=showiso->data().toString();
	//set it
	setText(tr("Current Server Time:\n%1").arg(MLocalFormat().formatDateTime(ts,fm)));
	//make sure tool tip is up to date
	int off=ts.offsetFromUTC()/60;
	QString tz=tr("Server Time Zone: %1\nOffset from UTC: %2 minutes %3")
		.arg(MLocalFormat().timeZone())
		.arg(off<0?-off:off)
		.arg(off>=0?tr("east","positive time zone offset"):tr("west","negative time zone offset"));
	if(toolTip()!=tz)setToolTip(tz);
}

void MServerClock::mouseDoubleClickEvent(QMouseEvent* event)
{
	QWidget::mouseDoubleClickEvent(event);
	if(event->button()!=Qt::LeftButton)return;
	showInfo();
}

static inline QString list2str(const QStringList&l)
{
	QString ret,r;
	for(int i=0;i<l.size();i++){
		if(i)r+=", ";
		r+="'"+l[i]+"'";
		if(r.size()>=30){
			ret+=r+"\n";
			r="";
		}
	}
	ret+=r;
	return ret.trimmed();
}

void MServerClock::showInfo()
{
	QDialog d(this);
	d.setWindowTitle(tr("Server Format Info"));
	QVBoxLayout*vl;
	QFormLayout*fl;
	QHBoxLayout*hl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addLayout(fl=new QFormLayout,10);
	fl->setLabelAlignment(Qt::AlignRight|Qt::AlignTop);
	MLocalFormat lf;
	fl->addRow(new QLabel(tr("Number Format:")));
	fl->addRow(tr("Decimal Dot:"),new QLabel(lf.decimalDot()));
	fl->addRow(tr("Separator:"),new QLabel(tr("'%1' every %2 digits").arg(lf.thousandSeparator()).arg(lf.thousandDigits())));
	
	fl->addRow(new QLabel);
	fl->addRow(new QLabel(tr("Currency Settings:")));
	fl->addRow(tr("Currency Symbol:"),new QLabel(lf.currency()));
	fl->addRow(tr("Division Digits:"),new QLabel(QString::number(lf.moneyDecimals())));
	fl->addRow(tr("Negative Sign:"),new QLabel(lf.moneyNegativeSign()));
	fl->addRow(tr("Positive Sign:"),new QLabel(lf.moneyPositiveSign()));
	fl->addRow(tr("Example:"),new QLabel(lf.formatMoney(-1234567890)));
	
	fl->addRow(new QLabel);
	fl->addRow(new QLabel(tr("Date and Time Settings:")));
	fl->addRow(tr("Day of the Week:"),new QLabel(list2str(lf.weekDayNames())));
	fl->addRow(tr("Day of the Week Abbreviated:"),new QLabel(list2str(lf.shortWeekDayNames())));
	fl->addRow(tr("Month Names:"),new QLabel(list2str(lf.monthNames())));
	fl->addRow(tr("Month Names Abbreviated:"),new QLabel(list2str(lf.shortMonthNames())));
	fl->addRow(tr("Date Format:"),new QLabel(lf.dateFormat()));
	fl->addRow(tr("Time Format:"),new QLabel(lf.timeFormat()));
	fl->addRow(tr("Date and Time Format:"),new QLabel(lf.dateTimeFormat()));
	MLocalFormat lfs;lfs.setTimeZone(TimeStamp::systemLocalZone());
	TimeStamp now=TimeStamp::now();
	fl->addRow(tr("System Time Zone:"),new QLabel(lfs.timeZone()));
	fl->addRow(tr("Current Local Time:"),new QLabel(lfs.formatDateTime(now)));
	fl->addRow(tr("Theater/Server Time Zone:"),new QLabel(lf.timeZone()));
	fl->addRow(tr("Current Theater/Server Time:"),new QLabel(lf.formatDateTime(now)));
	
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Close")));
	p->setDefault(true);
	connect(p,SIGNAL(clicked()),&d,SLOT(close()));
	
	d.exec();
}

void MServerClock::contextMenu(const QPoint& p)
{
	contextmenu->popup(mapToGlobal(p));
}
