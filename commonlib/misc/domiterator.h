//
// C++ Implementation: DOM node iterator for range based for loops
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
// originally introduced at http://silmor.de/qtstuff.domfor.php

#ifndef SMOKE_DOM_ITERATOR_H
#define SMOKE_DOM_ITERATOR_H

#include "commonexport.h"

class MAGICSMOKE_COMMON_EXPORT QDomNodeIterator
{
        int pos;
        const QDomNodeList&container;
public:
        //constructors and assignments
        QDomNodeIterator(const QDomNodeList&l,int p):pos(p),container(l){}
        QDomNodeIterator(const QDomNodeIterator&)=default;
        QDomNodeIterator(QDomNodeIterator&&)=default;
        QDomNodeIterator& operator=(const QDomNodeIterator&)=default;
        QDomNodeIterator& operator=(QDomNodeIterator&&)=default;
        
        //increment
        QDomNodeIterator& operator++(){pos++;return *this;}
        
        //comparison
        bool operator==(const QDomNodeIterator&o){return pos==o.pos && container==o.container;}
        bool operator!=(const QDomNodeIterator&o){return pos!=o.pos || container!=o.container;}
        
        //indirection
        QDomNode operator*(){return container.at(pos);}
};

//begin and end
inline QDomNodeIterator begin(const QDomNodeList&l){return QDomNodeIterator(l,0);}
inline QDomNodeIterator end(const QDomNodeList&l){return QDomNodeIterator(l,l.size());}


#endif
