//
// C++ Interface: plugin base for barcode scanners
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BARCODE_PLUGIN_H
#define MAGICSMOKE_BARCODE_PLUGIN_H

#include "commonexport.h"

#include <QtPlugin>
#include <QStringList>
#include <QDialog>
#include <QList>

class QTabWidget;

///Scanner Configuration Dialog
class MAGICSMOKE_COMMON_EXPORT MBarcodeConfiguration:public QDialog
{
        Q_OBJECT
        public:
                ///instantiates a new config dialog and asks plugins to display their tabs
                MBarcodeConfiguration(QWidget*parent);
                
        public slots:
                ///used by plugins to add configuration tabs,
                ///tabs should connect to the accepted() and rejected() signals to store data
                void addTab(QWidget*,QString);
        private:
                QTabWidget*mtab;
};

///base class of actual scanner implementations
class MAGICSMOKE_COMMON_EXPORT MBarcodeScanner:public QObject
{
        Q_OBJECT
        public:
                explicit MBarcodeScanner(QObject* parent = 0);
                
                ///returns a human readable name for the scanner
                virtual QString readableName()const=0;
                
                ///returns whether the scanner is currently active
                virtual bool isActive()const=0;
                
        public slots:
                ///attempts to activate the scanner
                virtual void activate()=0;
                ///attempts to de-activate the scanner
                virtual void deactivate()=0;
        signals:
                ///emitted when the scanner becomes active
                void activated();
                ///emitted when the scanner becomes inactive
                void deactivated();
                ///emitted when a new barcode is available
                void newBarcode(QString);
};

///base class of barcode scanner plugins
class MAGICSMOKE_COMMON_EXPORT MBarcodePlugin
{
        public:
                virtual ~MBarcodePlugin();

                ///called whenever the user opens a configuration dialog
                virtual void configure(MBarcodeConfiguration*)=0;
        protected:
                ///called by plugins to register a new scanner that has come online
                static void registerScanner(MBarcodeScanner*);
};

///central barcode scanner hub, this is used by widgets that require barcodes,
///plugins report (indirectly) to the hub
class MAGICSMOKE_COMMON_EXPORT MBarcodeHub:public QObject
{
        Q_OBJECT
        public:
                ///returns the hub object
                static MBarcodeHub*instance();
                
                ///initializes plugins (unless already initialized)
                void initialize();
                
                ///returns all currently active scanners
                QList<MBarcodeScanner*>allScanners();
                
        signals:
                ///this is emitted whenever any of the barcode scanners find a new barcode
                void newBarcode(QString);
                
        private slots:
                ///automatically called by when a plugin destructs
                void removePlugin(QObject*);
        private:
                MBarcodeHub();
                virtual ~MBarcodeHub();
};

#define MBarcodePlugin_IID "de.silmor.MagicSmoke.BarcodePlugin/1.0"

Q_DECLARE_INTERFACE(MBarcodePlugin, MBarcodePlugin_IID);


#endif
