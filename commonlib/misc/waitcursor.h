//
// C++ Interface: waitcursor
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_WAITCURSOR_H
#define MAGICSMOKE_WAITCURSOR_H

#include "commonexport.h"

/**simple class to show a busy-cursor while an instance of it exists*/
class MAGICSMOKE_COMMON_EXPORT WaitCursor
{
	public:
		/**displays a wait cursor (Qt::WaitCursor)*/
		WaitCursor();
		/**restores the last normal cursor*/
		~WaitCursor();
};


#endif
