//
// C++ Interface: odtrender
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_DOMMODEL_H
#define MAGICSMOKE_DOMMODEL_H

#include <QAbstractItemModel>
#include <DPtrBase>
#include <QDomNode>

#include <functional>

#include "commonexport.h"

class QDomNode;
class QDomDocument;

/** \brief Special functor that can be used to customize MDomItemModel.
Any static function or lambda that takes a QDomNode as argument and returns a QVariant can be used to specialize MDomItemModel. The type of data returned through the QVariant depends on the role the function is supposed to serve. Only one function/lambda can be registered for each role in any given model instance.*/
typedef std::function<QVariant(const QDomNode&)> MDomModelFunctor;

/** \brief This is a specialized model type that shows and allows to manipulate a DOM tree.
The model holds a copy of the DOM tree, so the original is not changed even if the model changes.
You can retrieve a current copy of the model's DOM tree by calling domDocument().
This model can not carry application specific user data. It is only editable through its specialized node methods.*/
class MAGICSMOKE_COMMON_EXPORT MDomItemModel:public QAbstractItemModel
{
	Q_OBJECT
	DECLARE_DPTR(d);
	public:
		///instantiate an empty DOM model
		MDomItemModel(QObject* parent = 0);
		///instantiate a DOM model with an initial DOM tree
		///\param xml the model will be fed with a copy of this DOM tree
		MDomItemModel(const QDomDocument&xml, QObject* parent = 0);
		
		/**returns a copy of the DOM tree represented by this model, 
		you can safely manipulate this copy without influencing the model. If you want
		to change the model's content use the node methods provided here.*/
		virtual QDomDocument domDocument()const;
		
		///alias for Qt::UserRole that is used to retrieve the DOM node of an index.
		static const int DomNodeRole=Qt::UserRole;
	
		///always returns 1 - the DOM tree has only one column corresponding to the node
		virtual int columnCount(const QModelIndex&parent=QModelIndex())const;
		///returns data corresponding to the node represented by index;
		///this method can be customized through setContentFromNodeCall(...)
		virtual QVariant data(const QModelIndex&index, int role=Qt::DisplayRole)const;
		
		///returns an index corresponding to the row, column, and parent node
		/// \param row the row beneith the parent node - the row number depends on which node types are shown, only visible node types are counted
		/// \param column must be 0, otherwise an invalid index is returned
		/// \param parent an invalid node for the root node (QDomDocument) or any valid index returned through index(...)
		virtual QModelIndex index(int row, int column, const QModelIndex&parent=QModelIndex())const;
		///returns the parent index of a node - if it has a parent
		virtual QModelIndex parent(const QModelIndex&index)const;
		///returns the amount of visible nodes beneith parent
		virtual int rowCount(const QModelIndex&parent=QModelIndex())const;
		
		///returns a set of node types that are configured to be visible in this model
		virtual QSet<QDomNode::NodeType> shownNodeTypes()const;
		
		///returns the QDomNode corresponding to index, for the invalid index the QDomDocument is returned
		virtual QDomNode node(const QModelIndex&index)const;
		
		///sets header data - it is currently possible to set any header for any role, although only few are actually shown in views
		bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole);
		///returns the header data for a specific header position and role
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole)const;
		
	public slots:
		/** sets the document represented by this model;
		the model makes a deep copy of the document, so that the original stays intact even if the model changes. Use domDocument() to retrieve a fresh copy of the models version of the document.*/
		virtual void setDomDocument(const QDomDocument&);
		
		///changes the settings of what kinds of nodes are shown in the model,
		///element nodes cannot be switched off,
		///all model indexes become invalid after a call to this method
		void showNodeTypes(const QSet<QDomNode::NodeType>&);
		///changes the settings of what kinds of nodes are shown in the model,
		///all model indexes become invalid after a call to this method
		void addShowNodeType(QDomNode::NodeType);
		///changes the settings of what kinds of nodes are shown in the model,
		///all model indexes become invalid after a call to this method
		void addShowNodeTypes(const QSet<QDomNode::NodeType>&);
		///changes the settings of what kinds of nodes are shown in the model,
		///all model indexes become invalid after a call to this method
		void showAllNodeTypes();
		
		/**changes the way element nodes are displayed, 
		this method has no effect if you customize Qt::DisplayRole through setContentFromNodeCall(...)
		\param maxattr the amount of characters shown for attributes, if this is 0 the model will just show "..." if the element has attributes, if this is <0 the model will ignore attributes
		\param maxtext the amount of characters shown for character data (QDomText and QDomCDATASection), if this is 0 the model will just show "..." if the element has text, if this is <0 the model will ignore any text
		*/
		void showElementProperties(int maxattr,int maxtext);
		
		///if true the default display routine will show a node type label
		void showTypeLabel(bool show=true);
		
		///replaces the node at the index, you cannot replace the entire document with this function - use setDomDocument for this
		virtual void setNode(const QModelIndex&index,const QDomNode&);
		///insert a node into the parent at given position (if no position is given: append)
		virtual void insertNode(const QDomNode&newnode,const QModelIndex&parent,int row=-1);
		///insert a list of nodes into the parent at given position (if no position is given: append)
		virtual void insertNodes(const QDomNodeList&newnodes,const QModelIndex&parent,int row=-1);
		///insert a list of nodes into the parent at given position (if no position is given: append)
		virtual void insertNodes(const QList<QDomNode>&newnodes,const QModelIndex&parent,int row=-1);
		///removes the nodes at the given position and their child nodes
		virtual bool removeRows(int row, int count, const QModelIndex&parent=QModelIndex());
		///removes the node at the given position and its child nodes
		virtual void removeNode(const QModelIndex&);
		
		/**Customize the way the model returns data to the view, per default the model only has code to generate a text representation for Qt::DisplayRole. You can override any role, including Qt::DisplayRole, except for DomNodeRole. The callback takes a const reference to QDomNode as argument and should convert it to a QVariant appropriate for the role.
		\param role the role to be customized
		\param callback a static function, functor or lambda that takes a node as argument and returns a variant as result, use nullptr to delete a customization
		*/
		void setContentFromNodeCall(int role,MDomModelFunctor callback);
		
		///resets the model to an invalid/empty QDomDocument
		virtual void clear(){setDomDocument(QDomDocument());}
		
	private:
		///default implementation for Display-Role
		QVariant displayFromNode(const QDomNode&)const;
};

Q_DECLARE_METATYPE(QDomNode);
Q_DECLARE_METATYPE(MDomModelFunctor);

#endif
