#Project File for MagicSmoke Config Classes
# (c) Konrad Rosenbaum, 2016

#basics
TEMPLATE = lib
VERSION = 
TARGET = magicsmoke-common

include(../basics.pri)
include(../iface/iface.pri)
include(../taurus/zip.pri)
include(../taurus/elam.pri)
include(../taurus/aurora.pri)

#Localization
TRANSLATIONS = \
	smoke-common_de.ts \
	smoke-common_en.ts

#main source files
HEADERS += configdialog.h mapplication.h
SOURCES += configdialog.cpp mapplication.cpp
INCLUDEPATH += .
DEPENDPATH += $$INCLUDEPATH
RESOURCES += files.qrc

include(crypto/crypto.pri)
include(widgets/widgets.pri)
include(misc/misc.pri)
include(templates/templates.pri)
include(stick/stick.pri)


#make sure exports are ok
DEFINES += MAGICSMOKE_COMMON_LIB_BUILD=1

#make sure the correct Qt DLLs are used
QT += gui widgets network printsupport
