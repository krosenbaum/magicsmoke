//
// C++ Implementation: config dialog
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "keygen.h"
#include "mapplication.h"
#include "configdialog.h"
#include "office.h"
#include "listview.h"
#include "sslexception.h"
#include "misc.h"

#include <QByteArray>
#include <QCheckBox>
#include <QComboBox>
#include <QCryptographicHash>
#include <QFile>
#include <QFileDialog>
#include <QFontDatabase>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkReply>
#include <QPushButton>
#include <QRegExp>
#include <QSettings>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QStyleFactory>
#include <QTableView>
#include <QTimer>
#include <QUrl>

MConfigDialog::MConfigDialog()
{
	setWindowTitle(tr("Magic Smoke Configuration"));

	oldrow=-1;
	sslexcept=0;
	//main layout
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	setLayout(hl=new QHBoxLayout);
	//create Menu Bar
	QMenuBar*mb;
	hl->setMenuBar(mb=new QMenuBar);
	QMenu*m=mb->addMenu(tr("&Profile"));
	m->addAction(tr("&New Profile..."),this,SLOT(newProfile()));
	m->addAction(tr("&Delete Profile"),this,SLOT(deleteProfile()));
	m->addAction(tr("&Rename Profile"),this,SLOT(renameProfile()));
	m->addAction(tr("C&lone Profile"),this,SLOT(cloneProfile()));
	m->addAction(tr("&Make Default Profile"),this,SLOT(defaultProfile()));
	m->addSeparator();
	m->addAction(tr("&Export Host Key..."),this,SLOT(exportKey()));
	m->addAction(tr("&Import Host Key..."),this,SLOT(importKey()));
	m->addAction(tr("&Generate Host Key..."),this,SLOT(generateKey()));
	m->addSeparator();
	m->addAction(tr("&Close Window"),this,SLOT(close()));
	m=mb->addMenu(tr("&Settings"));
	m->addAction(tr("&Language..."),this,SLOT(changeLang()));
	m->addAction(tr("&OpenOffice.org Settings..."),this,SLOT(openOfficeCfg()));
	m->addAction(tr("Set &Default Label Font..."),this,SLOT(setDefaultFont()));
	m->addAction(tr("Set &Application Style..."),this,SLOT(setAppStyle()));
	mb->addMenu(MApplication::helpMenu());

	//create list view
	hl->addWidget(profiles=new MListView);
	profilemodel=new QStandardItemModel(this);
	profiles->setModel(profilemodel);
	profiles->setEditTriggers(QListView::NoEditTriggers);
	connect(profiles,SIGNAL(clicked(const QModelIndex&)),this,SLOT(loadProfile()));
	connect(profiles,SIGNAL(activated(const QModelIndex&)),this,SLOT(loadProfile()));
	//create tabs
	QTabWidget*tab;
	QWidget*w;
	hl->addWidget(tab=new QTabWidget,10);
	tab->addTab(w=new QWidget,tr("Connection"));
	QGridLayout*gl;
	w->setLayout(gl=new QGridLayout);
	QLabel*lab;
	int lctr=0;
	gl->addWidget(lab=new QLabel(tr("Server URL:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addLayout(hl=new QHBoxLayout,lctr,1);
	hl->addWidget(new QLabel("https://"),0);
	hl->addWidget(serverurl=new QLineEdit,1);
	gl->addWidget(useproxy=new QCheckBox(tr("Proxy:")),++lctr,0);
	gl->addLayout(hl=new QHBoxLayout,lctr,1);
	hl->addWidget(proxyname=new QLineEdit,1);
	hl->addWidget(new QLabel(":"),0);
	hl->addWidget(proxyport=new QSpinBox,0);
	proxyport->setRange(1,65535);
	connect(useproxy,SIGNAL(toggled(bool)),proxyname,SLOT(setEnabled(bool)));
	connect(useproxy,SIGNAL(toggled(bool)),proxyport,SLOT(setEnabled(bool)));
	gl->addWidget(lab=new QLabel(tr("Proxy Username:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(proxyuser=new QLineEdit,lctr,1);
	gl->addWidget(lab=new QLabel(tr("Proxy Password:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(proxypass=new QLineEdit,lctr,1);
	proxypass->setEchoMode(QLineEdit::Password);
	connect(useproxy,SIGNAL(toggled(bool)),proxyuser,SLOT(setEnabled(bool)));
	connect(useproxy,SIGNAL(toggled(bool)),proxypass,SLOT(setEnabled(bool)));
	gl->setRowStretch(++lctr,10);

	tab->addTab(w=new QWidget,tr("Authentication"));
	w->setLayout(gl=new QGridLayout);
	lctr=0;
	gl->addWidget(lab=new QLabel(tr("Hostname:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(hostname=new QLineEdit,lctr,1);
	gl->addWidget(lab=new QLabel(tr("Hostkey:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(hostkey=new QLineEdit,lctr,1);
        gl->addWidget(lab=new QLabel(tr("Default Username:")),++lctr,0);
        lab->setAlignment(Qt::AlignRight);
        gl->addWidget(username=new QLineEdit,lctr,1);
        gl->addWidget(lab=new QLabel(tr("Hint for Username:")),++lctr,0);
        lab->setAlignment(Qt::AlignRight);
        gl->addWidget(userhint=new QLineEdit,lctr,1);
	gl->setRowStretch(++lctr,10);
	gl->addLayout(hl=new QHBoxLayout,++lctr,0,1,2);

	tab->addTab(w=new QWidget,tr("SSL Exceptions"));
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("List of non-fatal SSL exceptions:")),0);
	vl->addWidget(ssltable=new QTableView,10);
	ssltable->setModel(sslmodel=new QStandardItemModel(this));
	ssltable->setEditTriggers(QListView::NoEditTriggers);
	vl->addSpacing(10);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*pb;
	hl->addWidget(pb=new QPushButton(tr("Clear")));
	connect(pb,SIGNAL(clicked()),this,SLOT(clearSslExceptions()));
	hl->addWidget(pb=new QPushButton(tr("Probe Server")));
	connect(pb,SIGNAL(clicked()),this,SLOT(serverProbe()));

	tab->addTab(w=new QWidget,tr("Scripting"));
	w->setLayout(gl=new QGridLayout);
	gl->setColumnStretch(4,1);
	lctr=0;
	gl->addWidget(lab=new QLabel(tr("You can set scripting preferences here. You have the following options:\nAllow: if active scripts from this source are allowed to run.\nPriority: locations with the lowest value are searched first, when a script it found the other locations are ignored.")),lctr,0,1,5);
	lab->setWordWrap(true);
	gl->addWidget(lab=new QLabel(tr("Server side scripts:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(asrscript=new QCheckBox(tr("allow")),lctr,1);
	gl->addWidget(new QLabel(tr("Prio:")),lctr,2);
	gl->addWidget(psrscript=new QSpinBox,lctr,3);
	psrscript->setRange(0,100);
	gl->addWidget(lab=new QLabel(tr("Built in scripts:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(abiscript=new QCheckBox(tr("allow")),lctr,1);
	gl->addWidget(new QLabel(tr("Prio:")),lctr,2);
	gl->addWidget(pbiscript=new QSpinBox,lctr,3);
	pbiscript->setRange(0,100);
	gl->addWidget(lab=new QLabel(tr("User local scripts:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(ausscript=new QCheckBox(tr("allow")),lctr,1);
	gl->addWidget(new QLabel(tr("Prio:")),lctr,2);
	gl->addWidget(pusscript=new QSpinBox,lctr,3);
	pusscript->setRange(0,100);
	gl->addWidget(lab=new QLabel(tr("User script path:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(usrscript=new QLineEdit,lctr,1,1,4);
	gl->setRowStretch(++lctr,1);

	setSizeGripEnabled(true);

	initProfiles();
	loadProfile();
}

MConfigDialog::~MConfigDialog()
{
	saveProfile();
}

void MConfigDialog::initProfiles(QString selIdx)
{
	oldrow=-1;
	QSettings set;
	int defpro=set.value("defaultprofile",0).toInt();
	set.beginGroup("profiles");
	QStringList prf=set.childGroups();
	profilemodel->clear();
	if(profilemodel->columnCount()<1)
		profilemodel->insertColumn(0);
	profilemodel->insertRows(0,prf.size());
	int newrow=0;
	for(int i=0;i<prf.size();i++){
		QModelIndex idx=profilemodel->index(i,0);
		profilemodel->setData(idx,set.value(prf[i]+"/name").toString());
		profilemodel->setData(idx,prf[i],Qt::UserRole);
		if(i==defpro){
			QFont f=profiles->font();
			f.setBold(true);
			profilemodel->setData(idx,f,Qt::FontRole);
		}
		if(prf[i]==selIdx)newrow=i;
	}
	profiles->setCurrentIndex(profilemodel->index(newrow,0));
}

void MConfigDialog::loadProfile()
{
	//save old
	saveProfile();
	if(sslexcept)delete sslexcept;
	sslexcept=0;
	//get new
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	oldrow=idx.row();
	QString key=profilemodel->data(idx,Qt::UserRole).toString();
        const QString&dataDir=MApplication::dataDir();
	QSettings set;
	set.beginGroup("profiles/"+key);
	hostname->setText(set.value("hostname").toString());
	hostkey->setText(set.value("hostkey").toString());
	serverurl->setText(set.value("serverurl","my.host.com/path/machine.php").toString());
	useproxy->setChecked(set.value("useproxy",false).toBool());
	proxyname->setText(set.value("proxyname","proxy").toString());
	proxyport->setValue(set.value("proxyport",74).toInt());
	proxyuser->setText(set.value("proxyuser").toString());
	proxypass->setText(set.value("proxypass").toString());
	proxyname->setEnabled(useproxy->isChecked());
	proxyport->setEnabled(useproxy->isChecked());
	proxyuser->setEnabled(useproxy->isChecked());
	proxypass->setEnabled(useproxy->isChecked());
        username->setText(set.value("username").toString());
        userhint->setText(set.value("usernamehint").toString());
	sslexcept=new MSslExceptions(dataDir+"/profile."+key+"/sslexceptions.xml");
	sslFillModel();
	abiscript->setChecked(set.value("script/allowbuiltin",true).toBool());
	ausscript->setChecked(set.value("script/allowuser",false).toBool());
	asrscript->setChecked(set.value("script/allowserver",false).toBool());
	pbiscript->setValue(set.value("script/priobuiltin",10).toInt());
	pusscript->setValue(set.value("script/priouser",0).toInt());
	psrscript->setValue(set.value("script/prioserver",20).toInt());
	usrscript->setText(set.value("script/userpath",dataDir+"/userscripts").toString());
}

void MConfigDialog::saveProfile()
{
	if(oldrow<0 || oldrow>=profilemodel->rowCount())return;
	QString key=profilemodel->data(profilemodel->index(oldrow,0),Qt::UserRole).toString();
	QSettings set;
	set.beginGroup("profiles/"+key);
	set.setValue("hostname",hostname->text());
	set.setValue("hostkey",hostkey->text());
	set.setValue("serverurl",serverurl->text());
	set.setValue("useproxy",useproxy->isChecked());
	set.setValue("proxyname",proxyname->text());
	set.setValue("proxyport",proxyport->value());
	set.setValue("proxyuser",proxyuser->text());
	set.setValue("proxypass",proxypass->text());
        set.setValue("username",username->text());
        set.setValue("usernamehint",userhint->text());
	QDir(MApplication::dataDir()).mkdir("profile."+key);
	if(sslexcept)sslexcept->savesslexcept();
	set.setValue("script/allowbuiltin",abiscript->isChecked());
	set.setValue("script/allowuser",ausscript->isChecked());
	set.setValue("script/allowserver",asrscript->isChecked());
	set.setValue("script/priobuiltin",pbiscript->value());
	set.setValue("script/priouser",pusscript->value());
	set.setValue("script/prioserver",psrscript->value());
	set.setValue("script/userpath",usrscript->text());
}

void MConfigDialog::newProfile()
{
	saveProfile();
	//scan for existing ones...
	QSettings set;
	set.beginGroup("profiles");
	QStringList prf=set.childGroups();
	QStringList prn;
	for(int i=0;i<prf.size();i++){
		prn<<set.value(prf[i]+"/name").toString();
	}
	//create new index
	QString pidx;
	for(int i=0;;i++){
		pidx=QString::number(i);
		if(!prf.contains(pidx))break;
	}
	//request name
	QString pname;
	while(1){
		bool ok;
		pname=QInputDialog::getText(this,tr("New Profile"),tr("Please enter a profile name. It must be non-empty and must not be used yet:"),QLineEdit::Normal,pname,&ok);
		if(!ok)return;
		if(pname.isEmpty())continue;
		if(!prn.contains(pname))break;
	}
	//create
	set.setValue(pidx+"/name",pname);
	set.sync();
	initProfiles(pidx);
	loadProfile();
}

void MConfigDialog::deleteProfile()
{
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString key=profilemodel->data(idx,Qt::UserRole).toString();
	QSettings set;
	set.beginGroup("profiles");
	set.remove(key);
	set.sync();
	oldrow=-1;
	initProfiles();
	loadProfile();
}

void MConfigDialog::renameProfile()
{
	//get old name
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString key=profilemodel->data(idx,Qt::UserRole).toString();
	QString pname=profilemodel->data(idx).toString();
	//query
	bool ok;
	QString npname=QInputDialog::getText(this,tr("Rename Profile"),tr("Please enter a profile name. It must be non-empty and must not be used yet:"),QLineEdit::Normal,pname,&ok);
	if(!ok)return;
	//check
	if(npname==pname)return;
	for(int i=0;i<profilemodel->rowCount();i++){
		QString pn=profilemodel->data(profilemodel->index(i,0)).toString();
		if(pn==npname){
			QMessageBox::warning(this,tr("Warning"),tr("This profile name is already in use."));
			return;
		}
	}
	//set
	QSettings().setValue("profiles/"+key+"/name",npname);
	profilemodel->setData(idx,npname);
}

void MConfigDialog::cloneProfile()
{
	saveProfile();
	//get current
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString key=profilemodel->data(idx,Qt::UserRole).toString();
	//scan for existing ones...
	QSettings set;
	set.beginGroup("profiles");
	QStringList prf=set.childGroups();
	QStringList prn;
	for(int i=0;i<prf.size();i++){
		prn<<set.value(prf[i]+"/name").toString();
	}
	//create new index
	QString pidx;
	for(int i=0;;i++){
		pidx=QString::number(i);
		if(!prf.contains(pidx))break;
	}
	//request name
	QString pname;
	while(1){
		bool ok;
		pname=QInputDialog::getText(this,tr("New Profile"),tr("Please enter a profile name. It must be non-empty and must not be used yet:"),QLineEdit::Normal,pname,&ok);
		if(!ok)return;
		if(pname.isEmpty())continue;
		if(!prn.contains(pname))break;
	}
	//clone
	set.beginGroup(key);
	QStringList keys=set.allKeys();
	set.endGroup();
	for(int i=0;i<keys.size();i++)
		set.setValue(pidx+"/"+keys[i],set.value(key+"/"+keys[i]));
	//create
	set.setValue(pidx+"/name",pname);
	set.sync();
	initProfiles(pidx);
	loadProfile();
}
void MConfigDialog::defaultProfile()
{
	saveProfile();
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString key=profilemodel->data(idx,Qt::UserRole).toString();
	QSettings().setValue("defaultprofile",key);
	initProfiles(key);
	loadProfile();
}


void MConfigDialog::changeLang()
{
	MApplication::choseLanguage();
}

void MConfigDialog::exportKey()
{
	QSettings st;
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString pkey=profilemodel->data(idx,Qt::UserRole).toString();
	st.beginGroup("profiles/"+pkey);
	QString host=hostname->text().trimmed();
	QString key=hostkey->text().trimmed();
	saveKey(host,key);
}

void MConfigDialog::generateKey()
{
	//ask nicely
	if(hostkey->text()!="")
	if(QMessageBox::question(this,tr("Generate Hostkey"), tr("Do you really want to generate a new host key for this profile? This may disable all accounts from this host."), QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
		return;
	//actually create new key
	QString name;
	MKeyGen mkg(this);
	QString k=mkg.getKey();
	if(k=="")
		if(mkg.exec()!=QDialog::Accepted)
			return;
	hostkey->setText(mkg.getKey());
}

void MConfigDialog::saveKey(QString host,QString key)
{
	QStringList fn;
	QFileDialog fdlg(this,tr("Export Key to File"),QString(),"Magic Smoke Host Key (*.mshk)");
	fdlg.setDefaultSuffix("mshk");
	fdlg.setAcceptMode(QFileDialog::AcceptSave);
	fdlg.setFileMode(QFileDialog::AnyFile);
	fdlg.setDirectory(currentDir());
	if(!fdlg.exec())return;
	fn=fdlg.selectedFiles();
	if(fn.size()!=1)return;
	setCurrentDir(fn[0]);
	QFile fd(fn[0]);
	if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to open file %1 for writing: %2").arg(fn[0]).arg(fd.errorString()));
		return;
	}
	QString chk=QCryptographicHash::hash(key.toLatin1(),QCryptographicHash::Md5).toHex();
	QString out="MagicSmokeHostKey\n"+host+"\n"+key+"\n"+chk;
	fd.write(out.toLatin1());
	fd.close();
}

void MConfigDialog::importKey()
{
	QModelIndex idx=profiles->currentIndex();
	if(!idx.isValid())return;
	QString profkey=profilemodel->data(idx,Qt::UserRole).toString();

	if(QMessageBox::warning(this,tr("Warning"),tr("Importing a key overwrites the host key that is currently used by this profile. This may disable your accounts. Do you still want to continue?"),QMessageBox::Yes|QMessageBox::Abort,QMessageBox::Abort)!=QMessageBox::Yes)
		return;
	QStringList fn;
	QFileDialog fdlg(this,tr("Import Key from File"),QString(),"Magic Smoke Host Key (*.mshk)");
	fdlg.setDefaultSuffix("mshk");
	fdlg.setAcceptMode(QFileDialog::AcceptOpen);
	fdlg.setFileMode(QFileDialog::ExistingFile);
	fdlg.setDirectory(currentDir());
	if(!fdlg.exec())return;
	fn=fdlg.selectedFiles();
	if(fn.size()!=1)return;
	setCurrentDir(fn[0]);
	QFile fd(fn[0]);
	if(!fd.open(QIODevice::ReadOnly)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to open file %1 for reading: %2").arg(fn[0]).arg(fd.errorString()));
		return;
	}
	//read content (max: 10k to limit potential damage)
	QStringList fc=QString::fromLatin1(fd.read(10240)).split("\n",Qt::SkipEmptyParts);
	fd.close();
	//check content
	if(fc.size()<3){
		QMessageBox::warning(this,tr("Warning"),tr("This is not a host key file."));
		return;
	}
	if(fc[0].trimmed()!="MagicSmokeHostKey"){
		QMessageBox::warning(this,tr("Warning"),tr("This is not a host key file."));
		return;
	}
	QString hname=fc[1].trimmed();
	if(!QRegExp("[A-Za-z][A-Za-z0-9_]*").exactMatch(hname)){
		QMessageBox::warning(this,tr("Warning"),tr("This host key file does not contain a valid host name."));
		return;
	}
	QString key=fc[2].trimmed();
	if(!QRegExp("[0-9a-fA-F]+").exactMatch(key) || key.size()<40){
		QMessageBox::warning(this,tr("Warning"),tr("This host key file does not contain a valid key."));
		return;
	}
	QString chk=QCryptographicHash::hash(key.toLatin1(),QCryptographicHash::Md5).toHex();
	if(chk!=fc[3].trimmed()){
		QMessageBox::warning(this,tr("Warning"),tr("The key check sum did not match. Please get a clean copy of the host key file."));
		return;
	}
	//save
	hostname->setText(hname);
	hostkey->setText(key);
	QSettings set;
	set.beginGroup("profiles/"+profkey);
	set.setValue("hostkey",key);
	set.setValue("hostname",hname);
}

void MConfigDialog::openOfficeCfg()
{
	MOfficeConfig c(this);
	c.exec();
}

void MConfigDialog::setDefaultFont()
{
	QStringList fonts=QFontDatabase().families();
	QString df=QInputDialog::getItem(this,tr("Chose Default Font"),tr("Please chose a default font:"),fonts,fonts.indexOf(QSettings().value("defaultfont","").toString()),false);
	if(df.isEmpty())return;
	QSettings().setValue("defaultfont",df);
}

void MConfigDialog::setAppStyle()
{
	MAppStyleDialog d(this);
	d.exec();
}

void MConfigDialog::serverProbe()
{
	//sanity check
	if(!sslexcept)return;
	sslexcept->clearRecorded();
	//set up mini-environment
	QEventLoop loop;
	QNetworkAccessManager man;
	connect(&man,SIGNAL(finished(QNetworkReply*)),&loop,SLOT(quit()));
	QTimer::singleShot(30000,&loop,SLOT(quit()));
	connect(&man,SIGNAL(sslErrors(QNetworkReply*,const QList<QSslError>&)), this,SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
	if(useproxy->isChecked())
		man.setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,proxyname->text(),proxyport->value(),proxyuser->text(),proxypass->text()));
	//query server
	QNetworkRequest rq(QUrl("https://"+serverurl->text()));
	QNetworkReply *rp=man.get(rq);
	//wait for result
	setEnabled(false);
	loop.exec();
	setEnabled(true);
	//probe result
	if(rp->error()==QNetworkReply::NoError){
		QMessageBox::information(this,tr("Server Probe"),tr("The request finished without errors."));
	}else{
		QMessageBox::warning(this,tr("Server Probe"),tr("The request finished with an error: %1").arg(rp->errorString()));
	}
	delete rp;
	//save
	sslexcept->savesslexcept();
}

void MConfigDialog::sslErrors(QNetworkReply*rp,QList<QSslError>errs)
{
	if(!sslexcept)return;
	//check/record/clear
    if(sslexcept->checksslexcept(errs)){
        rp->ignoreSslErrors();
        return;
    }
	//convert to string
	QString err=tr("SSL Errors encountered:\n");
	for(int i=0;i<errs.size();i++){
        qDebug()<<"err type"<<(int)errs[i].error();
        qDebug()<<"err stri"<<errs[i].errorString();
		QSslCertificate c=errs[i].certificate();
		err+=tr("Certificate \"%1\"\n  Fingerprint (sha1): %2\n  Error: %3\n")
            .arg(c.subjectInfo(QSslCertificate::CommonName)
#if QT_VERSION >= 0x50000
                 .value(0)
#endif
                 )
			.arg(QString::fromLatin1(c.digest(QCryptographicHash::Sha1).toHex()))
			.arg(errs[i].errorString());
	}
	err+=tr("Accept connection anyway?");
	if(QMessageBox::warning(this,tr("SSL Warning"),err,QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes){
		rp->ignoreSslErrors();
		sslexcept->acceptRecorded();
	}
	sslFillModel();
}

void MConfigDialog::clearSslExceptions()
{
	if(sslexcept)sslexcept->clear();
	sslmodel->clear();
}

void MConfigDialog::sslFillModel()
{
	sslmodel->clear();
	if(!sslexcept)return;
	sslmodel->insertColumns(0,3);
	sslmodel->setHorizontalHeaderLabels(QStringList()<<tr("Common Name")<<tr("SHA-1 Digest")<<tr("Error Type"));
	QList<QPair<QSslCertificate,int> >lst=sslexcept->nonFatalExceptions();
	sslmodel->insertRows(0,lst.size());
	for(int i=0;i<lst.size();i++){
		sslmodel->setData(sslmodel->index(i,0),lst[i].first.subjectInfo(QSslCertificate::CommonName));
		sslmodel->setData(sslmodel->index(i,1),QString::fromLatin1(lst[i].first.digest(QCryptographicHash::Sha1).toHex()));
		sslmodel->setData(sslmodel->index(i,2),QSslError((QSslError::SslError)lst[i].second).errorString());
	}
}

MAppStyleDialog::MAppStyleDialog(QWidget*par)
	:QDialog(par)
{
	setWindowTitle(tr("Application Style"));
	QGridLayout *gl;
	setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("GUI Style:")),0,0);
	gl->addWidget(style=new QComboBox,0,1,1,2);
	style->addItem(tr("System Default"));
	style->setEditable(false);
	QStringList k=QStyleFactory::keys();
	QString cs=QSettings().value("appstyle").toString();
	style->addItems(k);
	if(k.contains(cs))
		style->setCurrentIndex(k.indexOf(cs)+1);

	gl->addWidget(new QLabel(tr("Stylesheet:")),1,0);
	gl->addWidget(css=new QLineEdit,1,1);
	css->setText(QSettings().value("appstylesheet").toString());
	QPushButton*p;
	gl->addWidget(p=new QPushButton("..."),1,2);
	connect(p,SIGNAL(clicked()),this,SLOT(selectCSS()));

	gl->setRowMinimumHeight(2,15);
	QHBoxLayout*hl;
	gl->addLayout(hl=new QHBoxLayout,3,0,1,3);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()),Qt::QueuedConnection);
	connect(p,SIGNAL(clicked()),this,SLOT(save()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

void MAppStyleDialog::selectCSS()
{
	QString s=QFileDialog::getOpenFileName(this,tr("Select Stylesheet"),currentDir());
	if(s!=""){
		css->setText(s);
		setCurrentDir(s);
	}
}

void MAppStyleDialog::save()
{
	if(style->currentIndex()==0)
		QSettings().remove("appstyle");
	else
		QSettings().setValue("appstyle",style->currentText());
	QSettings().setValue("appstylesheet",css->text());
}

