<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MAppStyleDialog</name>
    <message>
        <location filename="configdialog.cpp" line="632"/>
        <source>Application Style</source>
        <translation>Applikationsstil</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="635"/>
        <source>GUI Style:</source>
        <translation>Grafikstil:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="637"/>
        <source>System Default</source>
        <translation>Systemstandard</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="645"/>
        <source>Stylesheet:</source>
        <translation>Stylesheet:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="656"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="659"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="665"/>
        <source>Select Stylesheet</source>
        <translation>Stylesheet auswählen</translation>
    </message>
</context>
<context>
    <name>MApplication</name>
    <message>
        <source>&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-2013&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;protected under the GNU GPL v.3 or at your option any newer&lt;p&gt;See also the &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;This version was compiled from repository &apos;%3&apos; revision &apos;%4&apos;.&lt;p&gt;The current application data base path is &apos;%5&apos;.</source>
        <oldsource>&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-2011&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;protected under the GNU GPL v.3 or at your option any newer&lt;p&gt;See also the &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;This version was compiled from repository &apos;%3&apos; revision &apos;%4&apos;.&lt;p&gt;The current application data base path is &apos;%5&apos;.</oldsource>
        <translation type="vanished">&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-2013&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;geschkützt unter der GNU GPL v.3 oder neueren Versionen&lt;p&gt;Siehe auch &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;Diese version wurde aus Repository &apos;%3&apos; revision &apos;%4&apos; gebaut.&lt;p&gt;Der Datenpfad ist &apos;%5&apos;.</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="130"/>
        <source>&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-%6&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;protected under the GNU GPL v.3 or at your option any newer&lt;p&gt;See also the &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;This version was compiled at %8&lt;br/&gt;from repository &apos;%3&apos;&lt;br/&gt;revision &apos;%4&apos;&lt;br/&gt;last changed %7.&lt;p&gt;The current application data base path is &apos;%5&apos;.</source>
        <translation>&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-2013&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;geschützt unter der GNU GPL v.3 oder neueren Versionen&lt;p&gt;Siehe auch &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;Diese version wurde am %8&lt;br/&gt;aus Repository &apos;%3&apos;&lt;br/&gt;revision &apos;%4&apos;&lt;br/&gt;zuletzt geändert am %7 gebaut.&lt;p&gt;Der Datenpfad ist &apos;%5&apos;.</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="154"/>
        <source>MagicSmoke Version Information</source>
        <translation>MagicSmoke Versionsinformationen</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="157"/>
        <source>&lt;h3&gt;MagicSmoke Version %3&lt;/h3&gt;&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;Repository:&lt;td&gt;%1&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Revision:&lt;td&gt;%2&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Modification&amp;nbsp;State:&lt;td&gt;%4&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Commit Time:&lt;td&gt;%5&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Committer:&lt;td&gt;%6&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;
&lt;h3&gt;Libraries&lt;/h3&gt;
&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;WOC:&lt;td&gt;%7&lt;br/&gt;%8&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;PACK&amp;nbsp;Library:&lt;td&gt;%9&lt;br/&gt;%10&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Qt:&lt;td&gt;%11&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ELAM:&lt;td&gt;%12&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Default:&lt;td&gt;%13 in directory %14&lt;br/&gt;version %15&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Built-In:&lt;td&gt;%16 (library: %17)&lt;/tr&gt;&lt;/table&gt;</source>
        <translation>&lt;h3&gt;MagicSmoke Version %3&lt;/h3&gt;&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;Repository:&lt;td&gt;%1&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Revision:&lt;td&gt;%2&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Zustand:&lt;td&gt;%4&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Zeit:&lt;td&gt;%5&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Committer:&lt;td&gt;%6&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;
&lt;h3&gt;Bibliotheken&lt;/h3&gt;
&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;WOC:&lt;td&gt;%7&lt;br/&gt;%8&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;PACK&amp;nbsp;Library:&lt;td&gt;%9&lt;br/&gt;%10&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Qt:&lt;td&gt;%11&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ELAM:&lt;td&gt;%12&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Default:&lt;td&gt;%13 im Verzeichnis %14&lt;br/&gt;Version %15&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Built-In:&lt;td&gt;%16 (library: %17)&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="235"/>
        <source>default:/index.html</source>
        <comment>default help URL, if you translate the index.html file, then change this as well</comment>
        <translation>default:/index.html</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="450"/>
        <location filename="mapplication.cpp" line="457"/>
        <source>New Update</source>
        <translation>Neues Update</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="450"/>
        <source>A new version of MagicSmoke is available. Do you want to download the new version %1?</source>
        <translation>Eine neue Version von MagicSmoke ist verfügbar. Möchten sie die Version &apos;%1&apos; jetzt herunterladen?</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="457"/>
        <source>A new version of MagicSmoke is ready for installation. Do you want to install?</source>
        <translation>Eine neue Version von MagicSmoke ist bereit für die Installation. Möchten Sie jetzt installieren?</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="464"/>
        <source>Update Complete</source>
        <translation>Update abgeschlossen</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="464"/>
        <source>A new version of MagicSmoke has been installed. It will be used the next time you start MagicSmoke.</source>
        <translation>Eine neue Version von MagicSmoke wurde installiert. Nach einem Neustart der Applikation wird diese aktiv werden.</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="469"/>
        <source>Update Failed</source>
        <translation>Update fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="469"/>
        <source>An update of MagicSmoke failed.</source>
        <translation>Das Update von MagicSmoke ist leider fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="235"/>
        <location filename="mapplication.cpp" line="240"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="128"/>
        <source>About MagicSmoke</source>
        <translation>Über MagicSmoke</translation>
    </message>
</context>
<context>
    <name>MBarcodeConfiguration</name>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="27"/>
        <source>Configure Barcode Plugins</source>
        <translation>Barcode-Plugins konfigurieren</translation>
    </message>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="36"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="38"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>MBarcodeLine</name>
    <message>
        <location filename="widgets/barcodeline.cpp" line="23"/>
        <source>Type a barcode into this line or scan it with a barcode scanner.</source>
        <translation>Bitte geben Sie den Barcode in diese Zeile ein oder scannen Sie ihn.</translation>
    </message>
    <message>
        <location filename="widgets/barcodeline.cpp" line="24"/>
        <source>Type or scan a barcode.</source>
        <translation>Barcode eingeben oder scannen.</translation>
    </message>
</context>
<context>
    <name>MCentDialog</name>
    <message>
        <location filename="widgets/centbox.cpp" line="105"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="widgets/centbox.cpp" line="108"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MConfigDialog</name>
    <message>
        <location filename="configdialog.cpp" line="50"/>
        <source>Magic Smoke Configuration</source>
        <translation>Magic Smoke Konfiguration</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="61"/>
        <source>&amp;Profile</source>
        <translation>&amp;Profil</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="62"/>
        <source>&amp;New Profile...</source>
        <translation>&amp;Neues Profil</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="63"/>
        <source>&amp;Delete Profile</source>
        <translation>Profil &amp;Löschen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="64"/>
        <source>&amp;Rename Profile</source>
        <translation>Profil &amp;Umbenennen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="65"/>
        <source>C&amp;lone Profile</source>
        <translation>Profil &amp;kopieren</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="66"/>
        <source>&amp;Make Default Profile</source>
        <translation>Zum Standardprofil machen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="68"/>
        <source>&amp;Export Host Key...</source>
        <translation>Hostkey &amp;exportieren...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="69"/>
        <source>&amp;Import Host Key...</source>
        <translation>Hostkey &amp;importieren...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="70"/>
        <source>&amp;Generate Host Key...</source>
        <translation>Hostkey &amp;generieren...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="72"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Fenster schließen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="73"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="74"/>
        <source>&amp;Language...</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="75"/>
        <source>&amp;OpenOffice.org Settings...</source>
        <translation>&amp;OpenOffice Einstellungen...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="76"/>
        <source>Set &amp;Default Label Font...</source>
        <translation>Standardschrift für Aufkleber setzen...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="77"/>
        <source>Set &amp;Application Style...</source>
        <translation>&amp;Applikationsstil setzen...</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="91"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="96"/>
        <source>Server URL:</source>
        <translation>Server-URL:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="101"/>
        <source>Proxy:</source>
        <translation>Proxy:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="109"/>
        <source>Proxy Username:</source>
        <translation>Nutzername Proxy:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="112"/>
        <source>Proxy Password:</source>
        <translation>Passwort Proxy:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="120"/>
        <source>Authentication</source>
        <translation>Authentifizierung</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="123"/>
        <source>Hostname:</source>
        <translation>Hostname:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="126"/>
        <source>Hostkey:</source>
        <translation>Hostkey:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="129"/>
        <source>Default Username:</source>
        <translation>Standardnutzername:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="132"/>
        <source>Hint for Username:</source>
        <translation>Hinweis für Nutzername:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="138"/>
        <source>SSL Exceptions</source>
        <translation>SSL Ausnahmen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="140"/>
        <source>List of non-fatal SSL exceptions:</source>
        <translation>Liste der unkritischen SSL Ausnahmen:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="148"/>
        <source>Clear</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="150"/>
        <source>Probe Server</source>
        <translation>Server prüfen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="153"/>
        <source>Scripting</source>
        <translation>Scripte</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="157"/>
        <source>You can set scripting preferences here. You have the following options:
Allow: if active scripts from this source are allowed to run.
Priority: locations with the lowest value are searched first, when a script it found the other locations are ignored.</source>
        <translation>Hier können Sie Scripteinstellungen machen. Sie haben folgende Optionen:
Erlauben: wenn ausgewählt dann sind Scripte von dieser Quelle erlaubt.
Priorität: Quellen mit dem niedrigsten Wert werden zuerst durchsucht.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="159"/>
        <source>Server side scripts:</source>
        <translation>Serverseitige Scripte:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="161"/>
        <location filename="configdialog.cpp" line="167"/>
        <location filename="configdialog.cpp" line="173"/>
        <source>allow</source>
        <translation>erlauben</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="162"/>
        <location filename="configdialog.cpp" line="168"/>
        <location filename="configdialog.cpp" line="174"/>
        <source>Prio:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="165"/>
        <source>Built in scripts:</source>
        <translation>Eingebaute Scripte:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="171"/>
        <source>User local scripts:</source>
        <translation>Nutzerscripte:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="177"/>
        <source>User script path:</source>
        <translation>Nutzerscriptpfad:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="306"/>
        <location filename="configdialog.cpp" line="382"/>
        <source>New Profile</source>
        <translation>Neues Profil</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="306"/>
        <location filename="configdialog.cpp" line="341"/>
        <location filename="configdialog.cpp" line="382"/>
        <source>Please enter a profile name. It must be non-empty and must not be used yet:</source>
        <translation>Bitte geben Sie einen Profilnamen ein (mind. 1 Zeichen):</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="341"/>
        <source>Rename Profile</source>
        <translation>Profil Umbenennen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="348"/>
        <location filename="configdialog.cpp" line="458"/>
        <location filename="configdialog.cpp" line="473"/>
        <location filename="configdialog.cpp" line="487"/>
        <location filename="configdialog.cpp" line="495"/>
        <location filename="configdialog.cpp" line="499"/>
        <location filename="configdialog.cpp" line="504"/>
        <location filename="configdialog.cpp" line="509"/>
        <location filename="configdialog.cpp" line="514"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="348"/>
        <source>This profile name is already in use.</source>
        <translation>Dieser Profilname ist bereits in Benutzung.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="432"/>
        <source>Generate Hostkey</source>
        <translation>Hostkey generieren</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="432"/>
        <source>Do you really want to generate a new host key for this profile? This may disable all accounts from this host.</source>
        <translation>Wollen Sie wirklich einen neuen Hostkey für dieses Profil generieren? Es ist möglich dass Sie sich danach nicht mehr einloggen können.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="447"/>
        <source>Export Key to File</source>
        <translation>Hostkey als Datei speichern</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="458"/>
        <source>Unable to open file %1 for writing: %2</source>
        <translation>Datei %1 kann nicht zum Schreiben geöffnet werden: %2</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="473"/>
        <source>Importing a key overwrites the host key that is currently used by this profile. This may disable your accounts. Do you still want to continue?</source>
        <translation>Der Import eines Keys überschreibt den aktuellen Key des Profils. Dies könnte Ihre Accounts unbenutzbar machen. Trotzdem fortfahren?</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="476"/>
        <source>Import Key from File</source>
        <translation>Key aus Datei importieren</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="487"/>
        <source>Unable to open file %1 for reading: %2</source>
        <translation>Datei %1 kann nicht zum Lesen geöffnet werden: %2</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="495"/>
        <location filename="configdialog.cpp" line="499"/>
        <source>This is not a host key file.</source>
        <translation>Dies ist keine Hostkeydatei.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="504"/>
        <source>This host key file does not contain a valid host name.</source>
        <translation>Die Hostkeydatei enthält keinen gültigen Hostnamen.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="509"/>
        <source>This host key file does not contain a valid key.</source>
        <translation>Diese Datei enthält keinen gültigen Hostkey.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="514"/>
        <source>The key check sum did not match. Please get a clean copy of the host key file.</source>
        <translation>Die Checksumme dieser Datei ist fehlgeschlagen. Bitte besorgen Sie eine neue Kopie der Datei.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="535"/>
        <source>Chose Default Font</source>
        <translation>Standardschriftart auswählen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="535"/>
        <source>Please chose a default font:</source>
        <translation>Bitte wählen Sie eine Standardschrift:</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="568"/>
        <location filename="configdialog.cpp" line="570"/>
        <source>Server Probe</source>
        <translation>Server Prüfen</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="568"/>
        <source>The request finished without errors.</source>
        <translation>Die Anfrage war erfolgreich.</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="570"/>
        <source>The request finished with an error: %1</source>
        <translation>Die Anfrage hatte Fehler: %1</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="586"/>
        <source>SSL Errors encountered:
</source>
        <translation>SSL-Fehler wurden gefunden:
</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="591"/>
        <source>Certificate &quot;%1&quot;
  Fingerprint (sha1): %2
  Error: %3
</source>
        <translation>Zertifikat: %1
  Fingerabdruck (SHA1): %2
  Fehler: %3
</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="600"/>
        <source>Accept connection anyway?</source>
        <translation>Verbindung trotzdem nutzen?</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="601"/>
        <source>SSL Warning</source>
        <translation>SSL Warnung</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>Common Name</source>
        <translation>Common Name</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>SHA-1 Digest</source>
        <translation>SHA-1 Hash</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>Error Type</source>
        <translation>Fehlertyp</translation>
    </message>
</context>
<context>
    <name>MEventView</name>
    <message>
        <source>Artist: %1</source>
        <translation type="vanished">Künstler: %1</translation>
    </message>
    <message>
        <source>Starttime: %1</source>
        <translation type="vanished">Beginn: %1</translation>
    </message>
    <message>
        <source>Endtime: %1</source>
        <translation type="vanished">Ende: %1</translation>
    </message>
    <message>
        <source>Room: %1</source>
        <translation type="vanished">Spielstätte: %1</translation>
    </message>
    <message>
        <source>Seats: %1 max. (%2 sold out; %3 reserved; %4 free)</source>
        <translation type="vanished">Plätze: %1 maximal (%2 verkauft; %3 reserviert; %4 frei)</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;background-color:#ff8080&quot;&gt;Sold Out!&lt;/div&gt;</source>
        <comment>Colored display for sold out tickets</comment>
        <translation type="vanished">&lt;div style=&quot;background-color:#ff8080&quot;&gt;Nicht verfügbar!&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;table border=1 style=&quot;border-style:groove; background-color:#00ff00&quot;&gt;&lt;tr&gt;&lt;td&gt;&lt;div style=&quot;background-color:#80ff80&quot;&gt;&lt;a href=&quot;%1&quot;&gt;Order Tickets...&lt;/a&gt;&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <comment>Colored display for ordering tickets</comment>
        <translation type="vanished">&lt;table border=1 style=&quot;border-style:groove; background-color:#00ff00&quot;&gt;&lt;tr&gt;&lt;td&gt;&lt;div style=&quot;background-color:#80ff80&quot;&gt;&lt;a href=&quot;%1&quot;&gt;Bestellen...&lt;/a&gt;&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;background-color:#c0c0c0&quot;&gt;Unable to sell.&lt;/div&gt;</source>
        <comment>Colored display for tickets unavailable to this user</comment>
        <translation type="vanished">&lt;div style=&quot;background-color:#c0c0c0&quot;&gt;Kann nicht verkauft werden.&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Prices</source>
        <translation type="vanished">Preise</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Kategorie</translation>
    </message>
    <message>
        <source>Price</source>
        <translation type="vanished">Preis</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Verfügbar</translation>
    </message>
    <message>
        <source>Max. Seats</source>
        <translation type="vanished">Max. Plätze</translation>
    </message>
    <message>
        <source>Order</source>
        <translation type="vanished">Bestellen</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Beschreibung</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="vanished">Kommentar</translation>
    </message>
    <message>
        <location filename="widgets/eventview.cpp" line="171"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="widgets/eventview.cpp" line="171"/>
        <source>Unable to open URL %1</source>
        <translation>Kann URL &apos;%1&apos; nicht öffnen.</translation>
    </message>
</context>
<context>
    <name>MKeyGen</name>
    <message numerus="yes">
        <location filename="crypto/keygen.cpp" line="59"/>
        <location filename="crypto/keygen.cpp" line="89"/>
        <source>Current random buffer: %n Bits</source>
        <translation>
            <numerusform>Aktueller Zufallspuffer: %n Bit</numerusform>
            <numerusform>Aktueller Zufallspuffer: %n Bits</numerusform>
        </translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="43"/>
        <source>Magic Smoke Key Generator</source>
        <translation>Magic Smoke Schlüsselgenerator</translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="54"/>
        <source>&lt;html&gt;&lt;h1&gt;Key Generation&lt;/h1&gt;
I am currently collecting random bits in order to generate a host key for this installation. Please use mouse and keyboard to generate more random. Alternatively you can load a key from an external medium.&lt;p&gt;
At least %1 Bits of random are required.</source>
        <translation>&lt;html&gt;&lt;h1&gt;Schlüsselgenerierung&lt;/h1&gt;Das Programm sammelt gerade Zufallsbits für diese Installation. Bitte benutzen Sie Maus und Tastatur, um mehr Zufall zu generieren. Alternativ können Sie auch einen fertigen Schlüssel von einem externen Medium laden.&lt;p&gt;Mindestens %1 Zufallsbits werden gebraucht.</translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="70"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="72"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>MLabelConfig</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="380"/>
        <source>Label Configuration</source>
        <translation>Etikettenkonfiguration</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="383"/>
        <source>Ticket Labels:</source>
        <translation>Eintrittskarten:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="386"/>
        <location filename="templates/labeldlg.cpp" line="425"/>
        <source>Print Dialog:</source>
        <translation>Druckdialog:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="387"/>
        <location filename="templates/labeldlg.cpp" line="426"/>
        <source>Always ask for printer</source>
        <translation>Immer nach Drucker fragen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="388"/>
        <location filename="templates/labeldlg.cpp" line="427"/>
        <source>Ask if unknown or not present</source>
        <translation>Fragen wenn unbekannt oder nicht angeschlossen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="389"/>
        <location filename="templates/labeldlg.cpp" line="428"/>
        <source>Never ask for printer</source>
        <translation>Niemals nach Drucker fragen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="391"/>
        <location filename="templates/labeldlg.cpp" line="430"/>
        <source>Page Dialog:</source>
        <translation>Seitendialog:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="392"/>
        <location filename="templates/labeldlg.cpp" line="431"/>
        <source>Always ask for page layout</source>
        <translation>Immer nach Seiteneinteilung fragen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="393"/>
        <location filename="templates/labeldlg.cpp" line="432"/>
        <source>Ask if more than one label per page</source>
        <translation>Nur fragen wenn mehr als 1 Etikett zu bedrucken ist</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="394"/>
        <location filename="templates/labeldlg.cpp" line="433"/>
        <source>Never ask for page layout</source>
        <translation>Niemals fragen (ganze Seite nutzen)</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="401"/>
        <location filename="templates/labeldlg.cpp" line="438"/>
        <source>Printer:</source>
        <translation>Drucker:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="407"/>
        <source>Chose Printer for Tickets</source>
        <translation>Drucker für Eintrittskarten</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="408"/>
        <location filename="templates/labeldlg.cpp" line="445"/>
        <source>Set Printer</source>
        <translation>Drucker setzen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="414"/>
        <location filename="templates/labeldlg.cpp" line="451"/>
        <source>Print Mode:</source>
        <translation>Druckmodus:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="415"/>
        <location filename="templates/labeldlg.cpp" line="452"/>
        <source>Direct Print</source>
        <translation>Direktdruck</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="416"/>
        <location filename="templates/labeldlg.cpp" line="453"/>
        <source>Use Buffer Pixmap</source>
        <translation>Puffer-Bitmap benutzen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="422"/>
        <source>Voucher Labels:</source>
        <translation>Gutschein-Etiketten:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="444"/>
        <source>Chose Printer for Vouchers</source>
        <translation>Drucker für Gutscheine</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="460"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="462"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MLabelDialog</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="48"/>
        <source>Label Printing Setup</source>
        <translation>Etikettendruck einrichten</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="57"/>
        <source>mm</source>
        <comment>defaultmetric: mm, in, cm</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="83"/>
        <source>Label offset:</source>
        <translation>Seitenrand:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="89"/>
        <source>Label size:</source>
        <translation>Etikettengröße:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="95"/>
        <source>Unit:</source>
        <translation>Einheit:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="97"/>
        <source>Millimeter</source>
        <translation>Millimeter</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="98"/>
        <source>Centimeter</source>
        <translation>Zentimeter</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="99"/>
        <source>Inch</source>
        <translation>Zoll</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="107"/>
        <source>Page usage:</source>
        <translation>Seitennutzung:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="110"/>
        <source>Page %1</source>
        <translation>Seite %1</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="154"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="158"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="357"/>
        <source>Warning: the label may not fit on the page!</source>
        <translation>Warnung: der Aufkleber könnte größer als die eingestellte Seite sein!</translation>
    </message>
</context>
<context>
    <name>MLabelPrintDialog</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="660"/>
        <source>Custom Size</source>
        <comment>paper type</comment>
        <translation>Benutzerdefinierte Größe</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="661"/>
        <source>A0</source>
        <comment>paper type</comment>
        <translation>A0</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="662"/>
        <source>A1</source>
        <comment>paper type</comment>
        <translation>A1</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="663"/>
        <source>A2</source>
        <comment>paper type</comment>
        <translation>A2</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="664"/>
        <source>A3</source>
        <comment>paper type</comment>
        <translation>A3</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="665"/>
        <source>A4</source>
        <comment>paper type</comment>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="666"/>
        <source>A5</source>
        <comment>paper type</comment>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="667"/>
        <source>A6</source>
        <comment>paper type</comment>
        <translation>A6</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="668"/>
        <source>A7</source>
        <comment>paper type</comment>
        <translation>A7</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="669"/>
        <source>A8</source>
        <comment>paper type</comment>
        <translation>A8</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="670"/>
        <source>A9</source>
        <comment>paper type</comment>
        <translation>A9</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="671"/>
        <source>B0</source>
        <comment>paper type</comment>
        <translation>B0</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="672"/>
        <source>B1</source>
        <comment>paper type</comment>
        <translation>B1</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="673"/>
        <source>B2</source>
        <comment>paper type</comment>
        <translation>B2</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="674"/>
        <source>B3</source>
        <comment>paper type</comment>
        <translation>B3</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="675"/>
        <source>B4</source>
        <comment>paper type</comment>
        <translation>B4</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="676"/>
        <source>B5</source>
        <comment>paper type</comment>
        <translation>B5</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="677"/>
        <source>B6</source>
        <comment>paper type</comment>
        <translation>B6</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="678"/>
        <source>B7</source>
        <comment>paper type</comment>
        <translation>B7</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="679"/>
        <source>B8</source>
        <comment>paper type</comment>
        <translation>B8</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="680"/>
        <source>B9</source>
        <comment>paper type</comment>
        <translation>B9</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="681"/>
        <source>B10</source>
        <comment>paper type</comment>
        <translation>B10</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="682"/>
        <source>C5E</source>
        <comment>paper type</comment>
        <translation>C5E</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="683"/>
        <source>U.S. Common 10 Envelope</source>
        <comment>paper type</comment>
        <translation>U.S. Common 10 Envelope</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="684"/>
        <source>DLE</source>
        <comment>paper type</comment>
        <translation>DLE</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="685"/>
        <source>Executive</source>
        <comment>paper type</comment>
        <translation>Executive</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="686"/>
        <source>Folio</source>
        <comment>paper type</comment>
        <translation>Folio</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="687"/>
        <source>Ledger</source>
        <comment>paper type</comment>
        <translation>Ledger</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="688"/>
        <source>Legal</source>
        <comment>paper type</comment>
        <translation>Legal</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="689"/>
        <source>Letter</source>
        <comment>paper type</comment>
        <translation>Letter</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="690"/>
        <source>Tabloid</source>
        <comment>paper type</comment>
        <translation>Tabloid</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="700"/>
        <source>Chose Printer</source>
        <translation>Drucker auswählen</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="703"/>
        <source>Printer:</source>
        <translation>Drucker:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="715"/>
        <source>Make &amp; Model:</source>
        <translation>Typ und Modell:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="716"/>
        <source>Location:</source>
        <translation>Standort:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="717"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="718"/>
        <source>Unit of Measure:</source>
        <translation>Maßeinheit:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="719"/>
        <source>Millimeter</source>
        <translation>Millimeter</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="720"/>
        <source>Centimeter</source>
        <translation>Zentimeter</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="721"/>
        <source>Inch</source>
        <translation>Zoll</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="723"/>
        <source>Paper Type:</source>
        <translation>Papiertyp:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="733"/>
        <source>Paper Size:</source>
        <translation>Papiergröße:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="736"/>
        <source>wide x</source>
        <comment>paper size: width</comment>
        <translation>breit x</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="739"/>
        <source>high</source>
        <comment>paper size: height</comment>
        <translation>hoch</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="741"/>
        <source>Margins:</source>
        <translation>Ränder:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="745"/>
        <source>Use Full Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="755"/>
        <source>Resolution:</source>
        <translation>Auflösung:</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="759"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="762"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MOfficeConfig</name>
    <message>
        <source>Configure OpenOffice.org Access</source>
        <translation type="vanished">Zugriff auf OpenOffice Konfigurieren</translation>
    </message>
    <message>
        <source>OpenOffice.org</source>
        <translation type="vanished">OpenOffice.org</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="136"/>
        <source>Path to Executable:</source>
        <translation>Pfad zum Programm:</translation>
    </message>
    <message>
        <source>...</source>
        <comment>select OpenOffice path button</comment>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="124"/>
        <source>Configure LibreOffice Access</source>
        <translation>Zugriff auf LibreOffice Konfigurieren</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="134"/>
        <source>LibreOffice</source>
        <translation>LibreOffice</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="138"/>
        <source>...</source>
        <comment>select LibreOffice path button</comment>
        <translation>...</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="141"/>
        <source>Printing ODF</source>
        <translation>ODF Drucken</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="144"/>
        <source>Printer:</source>
        <translation>Drucker:</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="149"/>
        <source>(Default Printer)</source>
        <translation>(Standarddrucker)</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="159"/>
        <source>Always confirm printer when printing ODF</source>
        <translation>Drucker bestägen, wenn ODF gedruckt wird.</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="161"/>
        <source>Save printed files</source>
        <translation>Gedruckte Dateien auch speichern</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="164"/>
        <source>Opening ODF</source>
        <translation>ODF Öffnen</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="166"/>
        <source>Always open as Read-Only</source>
        <translation>Immer im Nur-Lese-Modus öffnen</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="168"/>
        <source>Automatically open all newly created files</source>
        <translation>Alle neuen Dateien automatisch öffnen</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="174"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="177"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="197"/>
        <source>Select LibreOffice executable</source>
        <translation>LibreOffice.org Programm wählen</translation>
    </message>
    <message>
        <source>Select OpenOffice.org executable</source>
        <translation type="vanished">OpenOffice.org Programm wählen</translation>
    </message>
</context>
<context>
    <name>MServerClock</name>
    <message>
        <location filename="misc/sclock.cpp" line="41"/>
        <source>Show Format Info</source>
        <translation>Zeige Formatinformationen</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="44"/>
        <source>Show Full Time</source>
        <translation>Langes Zeitformat</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="46"/>
        <source>%W, %D %N %Y %a:%I %p %t</source>
        <comment>full time format</comment>
        <translation>%W, %D. %N %Y %h:%I %t</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="48"/>
        <source>Show Date Only</source>
        <translation>nur Datum zeigen</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="49"/>
        <source>%w, %M/%D/%Y</source>
        <comment>date only format</comment>
        <translation>%w, %D.%M.%Y</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="51"/>
        <source>Show Time Only</source>
        <translation>nur Uhrzeit zeigen</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="52"/>
        <source>%a:%I %p %t</source>
        <comment>time only format</comment>
        <translation>%h:%I %t</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="54"/>
        <source>Show Short Date/Time</source>
        <translation>Kurzform Datum und Zeit</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="55"/>
        <source>%w %Y-%M-%D %h:%I %t</source>
        <comment>ISO like short time format</comment>
        <translation>%w %Y-%M-%D %h:%I %t</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="71"/>
        <source>Current Server Time:
%1</source>
        <translation>Aktuelle Theater/Server-Zeit:
%1</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="74"/>
        <source>Server Time Zone: %1
Offset from UTC: %2 minutes %3</source>
        <translation>Server-Zeitzone: %1
Offset von UTC: %2 Minuten %3</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="77"/>
        <source>east</source>
        <comment>positive time zone offset</comment>
        <translation>östlich</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="77"/>
        <source>west</source>
        <comment>negative time zone offset</comment>
        <translation>westlich</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="106"/>
        <source>Server Format Info</source>
        <translation>Sever Formatinformationen</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="114"/>
        <source>Number Format:</source>
        <translation>Zahlenformat:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="115"/>
        <source>Decimal Dot:</source>
        <translation>Dezimaltrenner:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="116"/>
        <source>Separator:</source>
        <translation>Füllzeichen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="116"/>
        <source>&apos;%1&apos; every %2 digits</source>
        <translation>&apos;%1&apos; aller %2 Ziffern</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="119"/>
        <source>Currency Settings:</source>
        <translation>Währungseinstellungen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="120"/>
        <source>Currency Symbol:</source>
        <translation>Währungssymbol:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="121"/>
        <source>Division Digits:</source>
        <translation>Dezimalstellen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="122"/>
        <source>Negative Sign:</source>
        <translation>Negationszeichen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="123"/>
        <source>Positive Sign:</source>
        <translation>Pluszeichen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="124"/>
        <source>Example:</source>
        <translation>Beispiel:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="127"/>
        <source>Date and Time Settings:</source>
        <translation>Datums- und Zeit-Einstellungen:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="128"/>
        <source>Day of the Week:</source>
        <translation>Wochentag:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="129"/>
        <source>Day of the Week Abbreviated:</source>
        <translation>Abgekürzter Wochentag:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="130"/>
        <source>Month Names:</source>
        <translation>Monatsname:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="131"/>
        <source>Month Names Abbreviated:</source>
        <translation>Abgekürzter Monatsname:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="132"/>
        <source>Date Format:</source>
        <translation>Datumsformat:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="133"/>
        <source>Time Format:</source>
        <translation>Zeitformat:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="134"/>
        <source>Date and Time Format:</source>
        <translation>Datums- und Zeitformat:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="137"/>
        <source>System Time Zone:</source>
        <translation>System-Zeitzone:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="138"/>
        <source>Current Local Time:</source>
        <translation>Aktuelle lokale Zeit:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="139"/>
        <source>Theater/Server Time Zone:</source>
        <translation>Theater/Server Zeitzone:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="140"/>
        <source>Current Theater/Server Time:</source>
        <translation>Aktuelle Theater/Server-Zeit:</translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="145"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>initprofile</name>
    <message>
        <location filename="mapplication.cpp" line="481"/>
        <source>Initial Profile Warning</source>
        <translation>Warnung: initiales Profil</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="481"/>
        <source>You need a profile to work with Magic Smoke. Please create one now.</source>
        <translation>Sie brauchen ein Profil, um mit Magic Smoke arbeiten zu können. Bitte legen Sie eines an.</translation>
    </message>
</context>
<context>
    <name>lang</name>
    <message>
        <location filename="mapplication.cpp" line="71"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="71"/>
        <source>The changed language setting will only be active after restarting the application.</source>
        <translation>Die Änderung der Sprachkonfiguration wird es nach dem nächsten Neustart des Programms wirksam.</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="66"/>
        <source>Chose Language</source>
        <translation>Sprache auswählen</translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="66"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
</context>
<context>
    <name>office</name>
    <message>
        <location filename="templates/office.cpp" line="50"/>
        <source>Chose Printer</source>
        <translation>Drucker auswählen</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="53"/>
        <source>Please chose a printer:</source>
        <translation>Bitte wählen Sie einen Drucker:</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="57"/>
        <source>(Default Printer)</source>
        <translation>(Standarddrucker)</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="73"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="96"/>
        <source>Save current document as...</source>
        <translation>Aktuelles Dokuement speichern unter...</translation>
    </message>
</context>
</TS>
