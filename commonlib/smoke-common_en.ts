<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MAppStyleDialog</name>
    <message>
        <location filename="configdialog.cpp" line="632"/>
        <source>Application Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="635"/>
        <source>GUI Style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="637"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="645"/>
        <source>Stylesheet:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="656"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="659"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="665"/>
        <source>Select Stylesheet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MApplication</name>
    <message>
        <location filename="mapplication.cpp" line="130"/>
        <source>&lt;h3&gt;MagicSmoke v. %1&lt;/h3&gt;&amp;copy; Konrad Rosenbaum, 2007-%6&lt;br&gt;&amp;copy; Peter Keller, 2007-2008&lt;br&gt;protected under the GNU GPL v.3 or at your option any newer&lt;p&gt;See also the &lt;a href=&quot;%2&quot;&gt;MagicSmoke Homepage&lt;/a&gt;.&lt;p&gt;This version was compiled at %8&lt;br/&gt;from repository &apos;%3&apos;&lt;br/&gt;revision &apos;%4&apos;&lt;br/&gt;last changed %7.&lt;p&gt;The current application data base path is &apos;%5&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="154"/>
        <source>MagicSmoke Version Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="157"/>
        <source>&lt;h3&gt;MagicSmoke Version %3&lt;/h3&gt;&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;Repository:&lt;td&gt;%1&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Revision:&lt;td&gt;%2&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Modification&amp;nbsp;State:&lt;td&gt;%4&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Commit Time:&lt;td&gt;%5&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Committer:&lt;td&gt;%6&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;
&lt;h3&gt;Libraries&lt;/h3&gt;
&lt;table border=&apos;1&apos; style=&apos;border-style: solid; border-color: #888;&apos;&gt;&lt;tr&gt;&lt;td&gt;WOC:&lt;td&gt;%7&lt;br/&gt;%8&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;PACK&amp;nbsp;Library:&lt;td&gt;%9&lt;br/&gt;%10&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Qt:&lt;td&gt;%11&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ELAM:&lt;td&gt;%12&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Default:&lt;td&gt;%13 in directory %14&lt;br/&gt;version %15&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Time&amp;nbsp;Zone&amp;nbsp;Built-In:&lt;td&gt;%16 (library: %17)&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="235"/>
        <source>default:/index.html</source>
        <comment>default help URL, if you translate the index.html file, then change this as well</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="450"/>
        <location filename="mapplication.cpp" line="457"/>
        <source>New Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="450"/>
        <source>A new version of MagicSmoke is available. Do you want to download the new version %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="457"/>
        <source>A new version of MagicSmoke is ready for installation. Do you want to install?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="464"/>
        <source>Update Complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="464"/>
        <source>A new version of MagicSmoke has been installed. It will be used the next time you start MagicSmoke.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="469"/>
        <source>Update Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="469"/>
        <source>An update of MagicSmoke failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="235"/>
        <location filename="mapplication.cpp" line="240"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="128"/>
        <source>About MagicSmoke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MBarcodeConfiguration</name>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="27"/>
        <source>Configure Barcode Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="36"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/barcode-plugin.cpp" line="38"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MBarcodeLine</name>
    <message>
        <location filename="widgets/barcodeline.cpp" line="23"/>
        <source>Type a barcode into this line or scan it with a barcode scanner.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/barcodeline.cpp" line="24"/>
        <source>Type or scan a barcode.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCentDialog</name>
    <message>
        <location filename="widgets/centbox.cpp" line="105"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/centbox.cpp" line="108"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MConfigDialog</name>
    <message>
        <location filename="configdialog.cpp" line="50"/>
        <source>Magic Smoke Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="61"/>
        <source>&amp;Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="62"/>
        <source>&amp;New Profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="63"/>
        <source>&amp;Delete Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="64"/>
        <source>&amp;Rename Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="65"/>
        <source>C&amp;lone Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="66"/>
        <source>&amp;Make Default Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="68"/>
        <source>&amp;Export Host Key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="69"/>
        <source>&amp;Import Host Key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="70"/>
        <source>&amp;Generate Host Key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="72"/>
        <source>&amp;Close Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="73"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="74"/>
        <source>&amp;Language...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="75"/>
        <source>&amp;OpenOffice.org Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="76"/>
        <source>Set &amp;Default Label Font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="77"/>
        <source>Set &amp;Application Style...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="91"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="96"/>
        <source>Server URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="101"/>
        <source>Proxy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="109"/>
        <source>Proxy Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="112"/>
        <source>Proxy Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="120"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="123"/>
        <source>Hostname:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="126"/>
        <source>Hostkey:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="129"/>
        <source>Default Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="132"/>
        <source>Hint for Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="138"/>
        <source>SSL Exceptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="140"/>
        <source>List of non-fatal SSL exceptions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="148"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="150"/>
        <source>Probe Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="153"/>
        <source>Scripting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="157"/>
        <source>You can set scripting preferences here. You have the following options:
Allow: if active scripts from this source are allowed to run.
Priority: locations with the lowest value are searched first, when a script it found the other locations are ignored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="159"/>
        <source>Server side scripts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="161"/>
        <location filename="configdialog.cpp" line="167"/>
        <location filename="configdialog.cpp" line="173"/>
        <source>allow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="162"/>
        <location filename="configdialog.cpp" line="168"/>
        <location filename="configdialog.cpp" line="174"/>
        <source>Prio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="165"/>
        <source>Built in scripts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="171"/>
        <source>User local scripts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="177"/>
        <source>User script path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="306"/>
        <location filename="configdialog.cpp" line="382"/>
        <source>New Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="306"/>
        <location filename="configdialog.cpp" line="341"/>
        <location filename="configdialog.cpp" line="382"/>
        <source>Please enter a profile name. It must be non-empty and must not be used yet:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="341"/>
        <source>Rename Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="348"/>
        <location filename="configdialog.cpp" line="458"/>
        <location filename="configdialog.cpp" line="473"/>
        <location filename="configdialog.cpp" line="487"/>
        <location filename="configdialog.cpp" line="495"/>
        <location filename="configdialog.cpp" line="499"/>
        <location filename="configdialog.cpp" line="504"/>
        <location filename="configdialog.cpp" line="509"/>
        <location filename="configdialog.cpp" line="514"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="348"/>
        <source>This profile name is already in use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="432"/>
        <source>Generate Hostkey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="432"/>
        <source>Do you really want to generate a new host key for this profile? This may disable all accounts from this host.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="447"/>
        <source>Export Key to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="458"/>
        <source>Unable to open file %1 for writing: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="473"/>
        <source>Importing a key overwrites the host key that is currently used by this profile. This may disable your accounts. Do you still want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="476"/>
        <source>Import Key from File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="487"/>
        <source>Unable to open file %1 for reading: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="495"/>
        <location filename="configdialog.cpp" line="499"/>
        <source>This is not a host key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="504"/>
        <source>This host key file does not contain a valid host name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="509"/>
        <source>This host key file does not contain a valid key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="514"/>
        <source>The key check sum did not match. Please get a clean copy of the host key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="535"/>
        <source>Chose Default Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="535"/>
        <source>Please chose a default font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="568"/>
        <location filename="configdialog.cpp" line="570"/>
        <source>Server Probe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="568"/>
        <source>The request finished without errors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="570"/>
        <source>The request finished with an error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="586"/>
        <source>SSL Errors encountered:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="591"/>
        <source>Certificate &quot;%1&quot;
  Fingerprint (sha1): %2
  Error: %3
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="600"/>
        <source>Accept connection anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="601"/>
        <source>SSL Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>Common Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>SHA-1 Digest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="619"/>
        <source>Error Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEventView</name>
    <message>
        <location filename="widgets/eventview.cpp" line="171"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/eventview.cpp" line="171"/>
        <source>Unable to open URL %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MKeyGen</name>
    <message>
        <location filename="crypto/keygen.cpp" line="43"/>
        <source>Magic Smoke Key Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="54"/>
        <source>&lt;html&gt;&lt;h1&gt;Key Generation&lt;/h1&gt;
I am currently collecting random bits in order to generate a host key for this installation. Please use mouse and keyboard to generate more random. Alternatively you can load a key from an external medium.&lt;p&gt;
At least %1 Bits of random are required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="crypto/keygen.cpp" line="59"/>
        <location filename="crypto/keygen.cpp" line="89"/>
        <source>Current random buffer: %n Bits</source>
        <translation>
            <numerusform>Current random buffer: %n Bit</numerusform>
            <numerusform>Current random buffer: %n Bits</numerusform>
        </translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="70"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="crypto/keygen.cpp" line="72"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MLabelConfig</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="380"/>
        <source>Label Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="383"/>
        <source>Ticket Labels:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="386"/>
        <location filename="templates/labeldlg.cpp" line="425"/>
        <source>Print Dialog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="387"/>
        <location filename="templates/labeldlg.cpp" line="426"/>
        <source>Always ask for printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="388"/>
        <location filename="templates/labeldlg.cpp" line="427"/>
        <source>Ask if unknown or not present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="389"/>
        <location filename="templates/labeldlg.cpp" line="428"/>
        <source>Never ask for printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="391"/>
        <location filename="templates/labeldlg.cpp" line="430"/>
        <source>Page Dialog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="392"/>
        <location filename="templates/labeldlg.cpp" line="431"/>
        <source>Always ask for page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="393"/>
        <location filename="templates/labeldlg.cpp" line="432"/>
        <source>Ask if more than one label per page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="394"/>
        <location filename="templates/labeldlg.cpp" line="433"/>
        <source>Never ask for page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="401"/>
        <location filename="templates/labeldlg.cpp" line="438"/>
        <source>Printer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="407"/>
        <source>Chose Printer for Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="408"/>
        <location filename="templates/labeldlg.cpp" line="445"/>
        <source>Set Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="414"/>
        <location filename="templates/labeldlg.cpp" line="451"/>
        <source>Print Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="415"/>
        <location filename="templates/labeldlg.cpp" line="452"/>
        <source>Direct Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="416"/>
        <location filename="templates/labeldlg.cpp" line="453"/>
        <source>Use Buffer Pixmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="422"/>
        <source>Voucher Labels:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="444"/>
        <source>Chose Printer for Vouchers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="460"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="462"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MLabelDialog</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="48"/>
        <source>Label Printing Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="57"/>
        <source>mm</source>
        <comment>defaultmetric: mm, in, cm</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="83"/>
        <source>Label offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="89"/>
        <source>Label size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="95"/>
        <source>Unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="97"/>
        <source>Millimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="98"/>
        <source>Centimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="99"/>
        <source>Inch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="107"/>
        <source>Page usage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="110"/>
        <source>Page %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="154"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="158"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="357"/>
        <source>Warning: the label may not fit on the page!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MLabelPrintDialog</name>
    <message>
        <location filename="templates/labeldlg.cpp" line="660"/>
        <source>Custom Size</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="661"/>
        <source>A0</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="662"/>
        <source>A1</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="663"/>
        <source>A2</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="664"/>
        <source>A3</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="665"/>
        <source>A4</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="666"/>
        <source>A5</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="667"/>
        <source>A6</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="668"/>
        <source>A7</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="669"/>
        <source>A8</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="670"/>
        <source>A9</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="671"/>
        <source>B0</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="672"/>
        <source>B1</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="673"/>
        <source>B2</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="674"/>
        <source>B3</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="675"/>
        <source>B4</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="676"/>
        <source>B5</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="677"/>
        <source>B6</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="678"/>
        <source>B7</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="679"/>
        <source>B8</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="680"/>
        <source>B9</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="681"/>
        <source>B10</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="682"/>
        <source>C5E</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="683"/>
        <source>U.S. Common 10 Envelope</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="684"/>
        <source>DLE</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="685"/>
        <source>Executive</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="686"/>
        <source>Folio</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="687"/>
        <source>Ledger</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="688"/>
        <source>Legal</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="689"/>
        <source>Letter</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="690"/>
        <source>Tabloid</source>
        <comment>paper type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="700"/>
        <source>Chose Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="703"/>
        <source>Printer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="715"/>
        <source>Make &amp; Model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="716"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="717"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="718"/>
        <source>Unit of Measure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="719"/>
        <source>Millimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="720"/>
        <source>Centimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="721"/>
        <source>Inch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="723"/>
        <source>Paper Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="733"/>
        <source>Paper Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="736"/>
        <source>wide x</source>
        <comment>paper size: width</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="739"/>
        <source>high</source>
        <comment>paper size: height</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="741"/>
        <source>Margins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="745"/>
        <source>Use Full Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="755"/>
        <source>Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="759"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/labeldlg.cpp" line="762"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOfficeConfig</name>
    <message>
        <location filename="templates/office.cpp" line="136"/>
        <source>Path to Executable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="124"/>
        <source>Configure LibreOffice Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="134"/>
        <source>LibreOffice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="138"/>
        <source>...</source>
        <comment>select LibreOffice path button</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="141"/>
        <source>Printing ODF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="144"/>
        <source>Printer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="149"/>
        <source>(Default Printer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="159"/>
        <source>Always confirm printer when printing ODF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="161"/>
        <source>Save printed files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="164"/>
        <source>Opening ODF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="166"/>
        <source>Always open as Read-Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="168"/>
        <source>Automatically open all newly created files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="174"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="177"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="197"/>
        <source>Select LibreOffice executable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MServerClock</name>
    <message>
        <location filename="misc/sclock.cpp" line="41"/>
        <source>Show Format Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="44"/>
        <source>Show Full Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="46"/>
        <source>%W, %D %N %Y %a:%I %p %t</source>
        <comment>full time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="48"/>
        <source>Show Date Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="49"/>
        <source>%w, %M/%D/%Y</source>
        <comment>date only format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="51"/>
        <source>Show Time Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="52"/>
        <source>%a:%I %p %t</source>
        <comment>time only format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="54"/>
        <source>Show Short Date/Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="55"/>
        <source>%w %Y-%M-%D %h:%I %t</source>
        <comment>ISO like short time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="71"/>
        <source>Current Server Time:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="74"/>
        <source>Server Time Zone: %1
Offset from UTC: %2 minutes %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="77"/>
        <source>east</source>
        <comment>positive time zone offset</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="77"/>
        <source>west</source>
        <comment>negative time zone offset</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="106"/>
        <source>Server Format Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="114"/>
        <source>Number Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="115"/>
        <source>Decimal Dot:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="116"/>
        <source>Separator:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="116"/>
        <source>&apos;%1&apos; every %2 digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="119"/>
        <source>Currency Settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="120"/>
        <source>Currency Symbol:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="121"/>
        <source>Division Digits:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="122"/>
        <source>Negative Sign:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="123"/>
        <source>Positive Sign:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="124"/>
        <source>Example:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="127"/>
        <source>Date and Time Settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="128"/>
        <source>Day of the Week:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="129"/>
        <source>Day of the Week Abbreviated:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="130"/>
        <source>Month Names:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="131"/>
        <source>Month Names Abbreviated:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="132"/>
        <source>Date Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="133"/>
        <source>Time Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="134"/>
        <source>Date and Time Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="137"/>
        <source>System Time Zone:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="138"/>
        <source>Current Local Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="139"/>
        <source>Theater/Server Time Zone:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="140"/>
        <source>Current Theater/Server Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/sclock.cpp" line="145"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>initprofile</name>
    <message>
        <location filename="mapplication.cpp" line="481"/>
        <source>Initial Profile Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="481"/>
        <source>You need a profile to work with Magic Smoke. Please create one now.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>lang</name>
    <message>
        <location filename="mapplication.cpp" line="66"/>
        <source>Chose Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="66"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="71"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapplication.cpp" line="71"/>
        <source>The changed language setting will only be active after restarting the application.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>office</name>
    <message>
        <location filename="templates/office.cpp" line="50"/>
        <source>Chose Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="53"/>
        <source>Please chose a printer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="57"/>
        <source>(Default Printer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="73"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/office.cpp" line="96"/>
        <source>Save current document as...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
