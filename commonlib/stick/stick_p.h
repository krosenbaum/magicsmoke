//
// C++ Interface: Stick Renderer Private
//
// Description: Stick Renderer for Qt, implements a twig-like syntax
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_STICK_PH
#define MAGICSMOKE_STICK_PH

#include <QObject>
#include <QVariant>
#include <QMap>
#include <QStringList>

class MStickRenderer;
namespace ELAM {class Engine;}

class MStickToken
{
public:
// 	MStickToken(QString);
	MStickToken()=default;
	MStickToken(const MStickToken&)=default;
	MStickToken(MStickToken&&)=default;

	MStickToken& operator=(const MStickToken&)=default;
	MStickToken& operator=(MStickToken&&)=default;

	QString render(MStickRenderer&,ELAM::Engine&);

	bool hasError()const{return !merror.isEmpty();}

	enum class Type{
		CompoundToken,
		ForLoop,
		PrintExpression,
		CalcExpression,
		IfElse,
		Else,
		Comment,
		Text,
		EndIf,
		EndFor
	};
private:
	friend class MStickTokenizer;
	MStickToken(QStringList&);
	void parseCommand(QStringList&);

	Type mtype=Type::CompoundToken;
	QList<MStickToken>msubtokens;
	QString mcontent,mextra,merror;
};

class MStickTokenizer
{
public:
	MStickTokenizer(QString);
	MStickToken* createTokens();
private:
	QStringList mtokens;
};

#endif
