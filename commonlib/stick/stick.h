//
// C++ Interface: Stick Renderer
//
// Description: Stick Renderer for Qt, implements a twig-like syntax
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_STICK_H
#define MAGICSMOKE_STICK_H

#include <QObject>
#include <QVariant>
#include <QMap>
#include <QStringList>

#include "commonexport.h"

class MStickToken;
namespace ELAM {class Engine;}

///A Twig-like renderer for HTML pages or other text based documents.
class MAGICSMOKE_COMMON_EXPORT MStickRenderer
{
public:
	///instantiate the renderer
	MStickRenderer();
	///clean up
	virtual ~MStickRenderer();

	///sets the template text file
	virtual void setTemplateFile(QString filename);
	///sets the template as plain text
	virtual void setTemplate(QString text);

	///makes a property available as a variable in the renderer
	virtual void setProperty(QString name,QVariant value);
	///makes several properties available as variables in the renderer
	virtual void setProperties(QMap<QString,QVariant>properties);
	///deletes a property/variable
	virtual void removeProperty(QString name);

	///returns a specific property or a NULL variant if not found
	virtual QVariant property(QString name)const;
	///returns the names of all known properties
	virtual QStringList propertyNames()const;

	///executes one rendering run on the template and returns the result as string
	virtual QString render();

private:
	QMap<QString,QVariant>mproperties;
	MStickToken*mtemplate=nullptr;

	///initializes the internal ELAM engine
	void initEngine(ELAM::Engine&);
};

#endif
