//
// C++ Implementation: Stick Renderer
//
// Description: Stick Renderer - Twig like renderer for Qt
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "stick.h"
#include "stick_p.h"

#include <QFile>
#include <QStringList>
#include <QDebug>

#include <ELAM/StringEngine>
#include <ELAM/BoolEngine>
#include <ELAM/IntEngine>
#include <ELAM/FloatEngine>

#include "misc.h"
#include "msinterface.h"


MStickRenderer::MStickRenderer()
{
}

MStickRenderer::~MStickRenderer()
{
	if(mtemplate){
		delete mtemplate;
		mtemplate=nullptr;
	}
}

QVariant MStickRenderer::property ( QString name ) const
{
	return mproperties[name];
}

QStringList MStickRenderer::propertyNames() const
{
	return mproperties.keys();
}

void MStickRenderer::setProperties ( QMap< QString, QVariant > properties )
{
	mproperties=properties;
}

void MStickRenderer::setProperty ( QString name, QVariant value )
{
	mproperties.insert(name,value);
}

void MStickRenderer::removeProperty ( QString name )
{
	mproperties.remove(name);
}

void MStickRenderer::setTemplate ( QString text )
{
	if(mtemplate)delete mtemplate;
	mtemplate=MStickTokenizer(text).createTokens();
}

void MStickRenderer::setTemplateFile ( QString filename )
{
	QFile fd(filename);
	fd.open(QIODevice::ReadOnly);
	setTemplate(QString::fromUtf8(fd.readAll()));
}

void MStickRenderer::initEngine ( ELAM::Engine&engine )
{
	//add int, float, bool, string
	ELAM::Engine::configureReflection(engine);
	ELAM::BoolEngine::configureBoolEngine(engine);
	ELAM::BoolEngine::configureLogicEngine(engine);
	ELAM::IntEngine::configureIntEngine(engine);
	ELAM::FloatEngine::configureFloatEngine(engine);
	ELAM::StringEngine::configureStringEngine(engine);

	//add pipe and other properties
	engine.setPipeOperator("|");
	engine.characterClasses().setDotChar('.');
	engine.setNamedOperatorsAllowed(true);

	//add MSmoke functions
	//datetime
	engine.setFunction("asDate",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<qint64>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 int argument");
		return unix2date(args[0].value<qint64>());
	});
	engine.setFunction("asDateTime",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<qint64>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 int argument");
		return unix2dateTime(args[0].value<qint64>());
	});
	//money
	engine.setFunction("asMoney",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<qint64>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 int argument");
		return cent2str(args[0].value<qint64>());
	});
	//html
	auto html=[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<QString>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string argument");
		return htmlize(args[0].value<QString>());
	};
	engine.setFunction("html",html);
	engine.setFunction("escape",html);
	engine.setFunction("e",html);
	engine.setFunction("xml",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<QString>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string argument");
		return xmlize(args[0].value<QString>());
	});
	engine.setFunction("raw",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<QString>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string argument");
		return args[0];
	});
	//MIME
	engine.setFunction("base64",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1||!args[0].canConvert<QByteArray>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 blob/byteArray argument");
		const auto base=args[0].toByteArray().toBase64();
		QString r;
		for(int i=0;i<base.size();i+=76)
			r+=QString::fromLatin1(base.mid(i,76))+"\r\n";
		return r;
	});
	engine.setFunction("implode",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=2||!args[0].canConvert<QStringList>()||!args[1].canConvert<QString>())
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 2 arguments: (stringlist elements, string separator)");
		return args[0].toStringList().join(args[1].toString());
	});
	//Access Rights
	engine.setFunction("checkflags",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=1)
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string (list) argument");
		if(args[0].canConvert<QStringList>())
			return req->checkFlags(args[0].toStringList());
		else if(args[0].canConvert<QString>())
			return req->checkFlags(args[0].toString().split(' ',QString::SkipEmptyParts));
		else
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string (list) argument");
	});
	//string functions
	engine.setFunction("startsWith",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()!=2)
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 2 string arguments");
		return args[0].toString().startsWith(args[1].toString());
	});
	engine.setFunction("substr",[](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
		if(args.size()<2&&args.size()>3)
			return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 string and 1-2 int arguments: substr(string,start,count)");
		if(args.size()==2)
			return args[0].toString().mid(args[1].toInt());
		else
			return args[0].toString().mid(args[1].toInt(),args[2].toInt());
	});
}

QString MStickRenderer::render()
{
	if(!mtemplate)return QString();
	//init expression engine
	ELAM::Engine engine;
	initEngine(engine);
	//add properties as constants
	for(QString prop:mproperties.keys())
		engine.setConstant(prop,mproperties[prop]);
	//render
	return mtemplate->render(*this,engine);
}

static inline QString toError(QString text)
{
	return QString("<p><font color=\"red\"><b>"+htmlize(text)+"</b></font></p>");
}

QString MStickToken::render(MStickRenderer&renderer,ELAM::Engine&engine)
{
	if(hasError()){
		return toError("Parser Error: "+merror);
	}
	switch(mtype){
		case Type::Text:{
			return mcontent;
		}
		case Type::CompoundToken:{
			QString r;
			for(MStickToken&t:msubtokens)
				r+=t.render(renderer,engine);
			return r;
		}
		case Type::ForLoop:{
			if(msubtokens.size()<1)
				return toError("Empty For-Loop");
			//resolve list expression
			QVariant loop=engine.evaluate(mcontent);
			//sanity check on type
			if(loop.isNull())
				return toError("Void Loop Expression");
			if(loop.canConvert<ELAM::Exception>())
				return toError("Error in Loop Expression: "+loop.toString()+"; "+mcontent);
			//convert to list
			QVariantList looplist;
			if(loop.canConvert<QVariantList>())looplist=loop.toList();
			else looplist.append(loop);
// 			qDebug()<<"Loop has"<<looplist.size()<<"elements"<<loop;
// 			qDebug()<<"Loop:"<<mextra<<mcontent;
			//loop over it
			QString res;
			if(!looplist.isEmpty()){
				for(QVariant v:looplist){
					engine.setConstant(mextra,v);
					res+=msubtokens[0].render(renderer,engine);
				}
			}else if(msubtokens.size()>1) //optional loop-else
				return msubtokens[1].render(renderer,engine);
			//done
			return res;
		}
		case Type::IfElse:{
			if(msubtokens.size()<1)
				return toError("Empty If-Clause");
			//calculate
			QVariant res=engine.evaluate(mcontent);
			if(res.canConvert<ELAM::Exception>())
				return toError("Error in If-Expression: "+res.toString()+"; "+mcontent);
			if(!res.canConvert<bool>())
				return toError("If-Expression must be boolean: "+mcontent);
			//execute
			if(res.toBool())
				return msubtokens[0].render(renderer,engine);
			else
				if(msubtokens.size()>1)
					return msubtokens[1].render(renderer,engine);
				else
					return QString();
		}
		case Type::PrintExpression:{
			QVariant res=engine.evaluate(mcontent);
			return res.canConvert<ELAM::Exception>()?toError("Calculation Error: "+res.toString()+" in "+mcontent):res.toString();
		}
		case Type::CalcExpression:{
			QVariant res=engine.evaluate(mcontent);
			return res.canConvert<ELAM::Exception>()?toError("Calculation Error: "+res.toString()+" in "+mcontent):QString();
		}
		default:
			return QString();
	};
}

MStickTokenizer::MStickTokenizer(QString text)
{
	int par=0;
	QString s;QChar quote='\0';bool bslash=false;
	for(QChar c:text){
		if(par==0){//normal text - no special handling whats'o'ever
			//start of special token?
			if(c=='{'){
				if(!s.isEmpty()){mtokens<<s;s.clear();}
				par++;
			}
			s+=c;
		}else{//special token
			s+=c;
			//inside quotes? don't count braces there
			if(!quote.isNull()){
				//reset backslash on char after it
				if(bslash)bslash=false;
				else{
					//detect backslash or end-quote
					if(c=='\\')bslash=true;else
					if(c==quote)quote='\0';
				}
			}else
			//count braces
			if(c=='{')par++;else
			if(c=='}'){
				par--;
				if(par==0){
					if(!s.isEmpty()){mtokens<<s;s.clear();}
				}
			}else
			//detect quotes (string constants - those are handled differently
			//inside quotes we do not count braces and handle backslash as escape char
			if(c=='\'' || c=='\"')quote=c;
		}
	}
	if(!s.isEmpty())mtokens<<s;
}

MStickToken* MStickTokenizer::createTokens()
{
	QStringList copy(mtokens);
	MStickToken t;
	while(copy.size()>0)t.msubtokens.append(MStickToken(copy));
	return new MStickToken(t);
}

MStickToken::MStickToken(QStringList&tokens)
{
	//get content
	if(tokens.size()==0)return;
	mcontent=tokens.takeFirst();
	//classify
	if(mcontent.startsWith("{{")){
		mtype=Type::PrintExpression;
		if(mcontent.endsWith("}}"))
			mcontent=mcontent.mid(2,mcontent.size()-4).trimmed();
		else
			merror="Invalid Expression in "+mcontent;
	}else if(mcontent.startsWith("{#")){
		mtype=Type::Comment;
		if(!mcontent.endsWith("#}"))
			merror="Invalid comment in "+mcontent;
	}else if(mcontent.startsWith("{%")){
		if(!mcontent.endsWith("%}")){
			mtype=Type::Comment;
			merror="Invalid Command Expression in "+mcontent;
			return;
		}
		mcontent=mcontent.mid(2,mcontent.size()-4).trimmed();
		parseCommand(tokens);
	}else if(mcontent.startsWith("{")){
		mtype=Type::Comment;
		merror="Invalid use of braces in "+mcontent;
	}else{
		mtype=Type::Text;
	}
}

void MStickToken::parseCommand(QStringList&tokens)
{
	QStringList expr=mcontent.split(' ',QString::SkipEmptyParts);
	if(expr.isEmpty()){
		mtype=Type::Comment;
		merror="Empty command";
		return;
	}
	if(expr[0]=="set"){
		mtype=Type::CalcExpression;
		mcontent=mcontent.mid(3).trimmed();
		return;
	}
	if(expr[0]=="for"){
		if(expr.size()<4){
			mtype=Type::Comment;
			merror="Incomplete For Loop";
			return;
		}
		//syntax: {% for varname in expression... %}
		mextra=expr[1];
		if(expr[2]!="in"){
			mtype=Type::Comment;
			merror="Syntax error in For Loop, expecting 'for varname in expression...' in "+mcontent;
			return;
		}
		mcontent=mcontent.mid(mcontent.indexOf(" in ")+4).trimmed();
		mtype=Type::ForLoop;
		//look for else and endfor, coll
		MStickToken sub;
		while(true){
			if(tokens.isEmpty()){
				qDebug()<<"premature end of loop in "+mcontent;
				msubtokens.append(sub);
				return;
			}
			MStickToken detail(tokens);
			if(detail.mtype==Type::Else){
				if(msubtokens.size()>0){
					mtype=Type::Comment;
					merror="Syntax Error: Double Else in For Loop detected.";
					return;
				}
				msubtokens.append(sub);
				sub=MStickToken();
			}else if(detail.mtype==Type::EndFor){
				msubtokens.append(sub);
				return;
			}else
				sub.msubtokens.append(detail);
		}
		//unreachable!
		qDebug()<<"Reached unreachable code in MStickToken::parseCommand!";
		return;
	}
	if(expr[0]=="if"){
		//syntax if expr...
		mcontent=mcontent.mid(2).trimmed();
		mtype=Type::IfElse;
		//look for else and endif
		MStickToken sub;
		while(true){
			if(tokens.isEmpty()){
				qDebug()<<"premature end of conditional in "+mcontent;
				msubtokens.append(sub);
				return;
			}
			MStickToken detail(tokens);
			if(detail.mtype==Type::Else){
				if(msubtokens.size()>0){
					mtype=Type::Comment;
					merror="Syntax Error: Double Else in Conditional detected.";
					return;
				}
				msubtokens.append(sub);
				sub=MStickToken();
			}else if(detail.mtype==Type::EndIf){
				msubtokens.append(sub);
				return;
			}else
				sub.msubtokens.append(detail);
		}
		//unreachable!
		qDebug()<<"Reached unreachable code in MStickToken::parseCommand!";
		return;
	}
	if(expr[0]=="else"){
		mtype=Type::Else;
		return;
	}
	if(expr[0]=="endif"){
		mtype=Type::EndIf;
		return;
	}
	if(expr[0]=="endfor"){
		mtype=Type::EndFor;
		return;
	}
	//unknown command
	mtype=Type::Comment;
	merror=QString("Unknown Command '%1' encountered, expecting one of set, if, for.").arg(expr[0]);
}
