//
// C++ Interface: main
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MAPP_H
#define MAGICSMOKE_MAPP_H

#include <QString>
#include <QApplication>

#include "commonexport.h"

class QMenu;
class EFilter;
namespace AuroraUpdater{class Aurora;};
///Main application object of MagicSmoke
class MAGICSMOKE_COMMON_EXPORT MApplication:public QApplication
{
	Q_OBJECT
	public:
                ///create MagicSmoke application
		MApplication(int&ac,char**av);
		
                ///return the standard help menu
		static QMenu*helpMenu();
                
                ///shortcut: the main function
                static int realmain(int,char**);
                /**contains the directory that is used for external data storage*/
                static QString dataDir();
		/**show a dialog to change the language*/
		static QString choseLanguage(bool warn=true);

                ///override the data directory
                ///- call it before any of the init functions
                ///- never call this while a session is active!
                static void setDataDir(QString);
                ///override the configuration directory
                ///- call this before initialize
                ///- on Windows and MacOS, this also switches the settings mode to ini-files
                static void setConfigDir(QString);
	public slots:
                ///complete initialization: style, language, data dir
                void initialize();
                ///set the configured application style
                void setMyStyle();
                ///initialize the localization
                void initLanguage();
                ///initialize the profiles
                void initProfile();
                
		///show about dialog
		void aboutMS();
                ///show version info
		void versionDlg();
                ///start a browser with the help page
		void help();

		///initialize the updater
                void initUpdater();

        private slots:
                ///jump to a specific help page
		void help(int);
                
                ///initialize the help menu from config files
                void initHelpUrl();
                
                ///updater is ready for download
                void updateReadyDownload(const QString&);
                ///updater is ready for install
                void updaterReadyInstall();
                ///update complete
                void updaterCompleted();
                ///something failed for the updater
                void updaterFailed();
                
        private:
                EFilter *ef=nullptr;
                QTranslator*qttrans=nullptr,*mstrans=nullptr;
                AuroraUpdater::Aurora*updater=nullptr;
};

///return the instance of the MApplication 
#define mApp (qobject_cast<MApplication*>(qApp))

#endif
