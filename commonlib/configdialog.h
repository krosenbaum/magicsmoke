//
// C++ Interface: mainwindow
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_CONFIGDIALOG_H
#define MAGICSMOKE_CONFIGDIALOG_H

#include <QDialog>
#include <QList>
#include <QSslError>

class QCheckBox;
class QLineEdit;
class QListView;
class QNetworkReply;
class QSpinBox;
class QStandardItemModel;
class QTableView;

class MSslExceptions;

#include "commonexport.h"

/**login and profile configuration window*/
class MAGICSMOKE_COMMON_EXPORT MConfigDialog:public QDialog
{
	Q_OBJECT
	public:
		MConfigDialog();
		~MConfigDialog();
		
	private:
		QCheckBox*useproxy,*abiscript,*ausscript,*asrscript;
		QLineEdit*hostname,*hostkey,*serverurl,*proxyname,*username,
			*userhint,*proxyuser,*proxypass,*usrscript;
		QSpinBox*proxyport,*pbiscript,*pusscript,*psrscript;
		QListView*profiles;
		QStandardItemModel*profilemodel,*sslmodel;
		QTableView*ssltable;
		
		MSslExceptions*sslexcept;
		
		int oldrow;
		
		//helper for exportKey and generateKey
		void saveKey(QString hostname,QString hostkey);
		
	private slots:
		//handling of login/profile screen
		void initProfiles(QString idx=QString());
		void loadProfile();
		void saveProfile();
		void newProfile();
		void deleteProfile();
		void renameProfile();
		void cloneProfile();
		void defaultProfile();
		//settings
		void changeLang();
		void exportKey();
		void importKey();
		void generateKey();
		void openOfficeCfg();
		void setDefaultFont();
		void setAppStyle();
		//ssl server probe
		void serverProbe();
        void sslErrors(QNetworkReply*,QList<QSslError>);
		void clearSslExceptions();
		void sslFillModel();
};

class QComboBox;
class QLineEdit;

class MAGICSMOKE_COMMON_EXPORT MAppStyleDialog:public QDialog
{
	Q_OBJECT
	public:
		MAppStyleDialog(QWidget*);
	private slots:
		void selectCSS();
		void save();
	private:
		QComboBox*style;
		QLineEdit*css;
};

#endif
