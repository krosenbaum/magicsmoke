//
// C++ Interface: labeldlg
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_LABELDLG_H
#define MAGICSMOKE_LABELDLG_H

#include "commonexport.h"

#include <QDialog>
#include <QPointF>
#include <QList>

class QDoubleSpinBox;
class QPaintDevice;
class QCheckBox;
class QComboBox;
class QLineEdit;
class QPrinter;
class QLabel;

///access to the label specific configuration
class MAGICSMOKE_COMMON_EXPORT MLabelConfig
{
	public:
		///shows the configuration dialog
		static void configDialog(QWidget*parent=0);

		///the type of label whose configuration is being queried
		enum LabelType {
                        ///invalid config object
                        Invalid=0,
			///ticket label
			Ticket=1,
			///voucher label
			Voucher=2
		};
                ///instantiate an empty configuration object
                MLabelConfig(){initMeta();}
		///instantiates a configuration object for the given label type
		MLabelConfig(LabelType lt):m_type(lt){initMeta();}
		///copies the config object
		MLabelConfig(const MLabelConfig&lc):m_type(lc.m_type){initMeta();}
		///copies the config object
		MLabelConfig& operator=(const MLabelConfig&lc){m_type=lc.m_type;return *this;}

		///returns the type represented by this config object
		LabelType labelType()const{return m_type;}

		///returns the configured printer name (even if not available on the local system)
		QString printerName()const;

		///configures the QPrinter object to the printer set for this label type;
		///use selectPrinter if you want to fall back to a selection dialog
		/// \returns true if the printer was successfully configured
		bool configurePrinter(QPrinter&)const;

		///configures the printer for this particular label type;
		///whether the dialog is shown depends on settings and printer availability;
		///if the dialog is shown the function remembers the new printer settings
		/// \param printer pointer to the QPrinter object to be configured
		/// \param force if true: force showing the printer dialog
		/// \returns true if the selection was successful
		bool selectPrinter(QPrinter&printer,bool force=false);

		///whether to ask for a printer when printing labels
		enum PrintAskMode{
			//do not change the order of these entries!
			//their numerical value is used as index in configDialog()
			///always ask for a printer
			PrintAskAlways=0,
			///only ask if the currently set printer is not known/connected
			PrintAskIfUnknown=1,
			///never ask for a printer, just fail if the printer is unavailable
			PrintAskNever=2
		};
		///whether to ask for the page layout when printing labels
		enum PageAskMode{
			//do not change the order of these entries!
			//their numerical value is used as index in configDialog()
			///always ask
			PageAskAlways=0,
			///only ask if there are more than 1 labels on the page
			PageAskIfNeeded=1,
			///never ask, just assume the whole page is available
			PageAskNever=2,
		};

		///returns the policy for asking for a printer
		PrintAskMode printAskMode()const;

		///returns the policy for asking for page layout
		PageAskMode pageAskMode()const;

		///how to send data to the printer
		enum PrintMode{
			//do not change the order of these entries!
			//their numerical value is used as index in configDialog()
			///print directly through the printer driver
			PrintDirect=0,
			///use a buffer pixmap to render fonts/graphics in memory first
			PrintPixmap=1
		};

		///returns how to send data to the printer
		PrintMode printMode()const;

	private:
		LabelType m_type;
		static QString tr(const char*,const char*c=0);

		/// \internal helper to store printer settings for next time
		void storePrinter(QPrinter&);
		/// \internal helper to store ask modes
		void storeAskModes(PrintAskMode,PageAskMode);
		/// \internal helper to store print mode
		void storePrintMode(PrintMode);
		/// \internal return the config group
		QString cgroup()const;

                /// \internal initialize meta type info
                void initMeta();

                friend QDataStream&operator<<(QDataStream&,const MLabelConfig&);
                friend QDataStream&operator>>(QDataStream&,MLabelConfig&);
};
MAGICSMOKE_COMMON_EXPORT QDataStream &operator<<(QDataStream &out, const MLabelConfig &myObj);
MAGICSMOKE_COMMON_EXPORT QDataStream &operator>>(QDataStream &in, MLabelConfig &myObj);

Q_DECLARE_METATYPE(MLabelConfig);

///dialog that allows to select which labels on a sheet of paper are used
class MAGICSMOKE_COMMON_EXPORT MLabelDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates a label dialog*/
		MLabelDialog(QWidget*parent,QPrinter*printer,int numlabels,QSizeF labelsize);
		/**deletes the label dialog and stores its current settings*/
		~MLabelDialog();

		/**returns the offset of label number n; relative to the coordinate system of the given paint device; starts at the bottom left of the page*/
		QPointF labelOffset(int n)const;
		/**returns whether this label is on a new page*/
		bool labelNeedsPageTurn(int n)const;
		///returns whether there is only one label per page
		bool oneLabelPerPage()const;

		///configures the printer for this particular label type
		///whether the dialog is shown depends on settings and printer availability
		/// \param type type of label
		/// \param printer pointer to the QPrinter object to be configured
		/// \param force if true: force showing the printer dialog
		/// \returns true if the selection was successful
		static inline bool selectPrinter(MLabelConfig::LabelType type, QPrinter&printer, bool force=false)
		{return MLabelConfig(type).selectPrinter(printer,force);}

	public slots:
		///shows the dialog if necessary - this depends on the local configuration for
		///this label type
		///if not shown it returns true (presuming the user would have wanted to print)
		///if shown it returns true if the user clicked "Ok" and false otherwise
		bool conditionalExec(MLabelConfig::LabelType);

	private slots:
		/**internal: toggle Row button clicked*/
		void toggleRow(int);
		/**internal: toggle Column button clicked*/
		void toggleColumn(int);
		/**internal: toggle all button clicked*/
		void toggleAll();

		/**internal: display correct page*/
		void updatePage();
		/**helper: save current/old page*/
		void savePage();
		/**internal: save settings for next time*/
		void saveSettings();

	private:
		QLineEdit *offx,*offy,*sizex,*sizey;
		QLabel *warning;
		QSizeF labelsize;
		QComboBox *metric,*page;
		QList<QCheckBox*>checks;
		QList<QList<bool> >checked;
		QPrinter* printer;
		int numlabels,oldpage,maxrows,maxcols;

		/**internal helper: find coordinates of label n; returns page id; returns -1 on failure*/
		int findLabel(int n,int&row,int&col)const;
};


///Dialog that allows to configure details on how to print a label.
class MAGICSMOKE_COMMON_EXPORT MLabelPrintDialog:public QDialog
{
        Q_OBJECT
        public:
                explicit MLabelPrintDialog(QPrinter*,QWidget* parent = 0, Qt::WindowFlags f = Qt::WindowFlags());
        public slots:
                void setButtonLabel(const QString&);
        private slots:
                void printerChanged(int);
                void paperChanged(int);
                void unitChanged(int);

                void okBtnPressed();
        private:
                QPrinter*mprinter;
                QComboBox*mprnselect,*mresolution,*mpaper,*munit;
                QDoubleSpinBox*mmargleft,*mmargright,*mmargtop,*mmargbottom;
                QDoubleSpinBox*msizex,*msizey;
                QLabel*mdescript,*mlocation,*mmakemodel;
                QPushButton*mokbtn;
		QCheckBox*mfullpage;
                double moldunit=1.;

                //internal: initialize paper types
                void initPaper();
};

#endif
