//
// C++ Interface: order bill renderer
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BILLRENDER_H
#define MAGICSMOKE_BILLRENDER_H

#include <QMap>

#include "odtrender.h"

#include "MOOrder"
#include "MOShipping"

/**displays an order and allows the user to execute several commands on it*/
class MAGICSMOKE_COMMON_EXPORT MBillRenderer:public MOdtRenderer
{
	public:
		/**creates the order renderer*/
		MBillRenderer(const MOOrder&,const MTemplate&);

	protected:
		/**callback for bill generator: variables; see MOdtSignalRenderer for details*/
		QVariant getVariable(QString)override;
		/**callback for bill generator: loops; see MOdtSignalRenderer for details*/
		int getLoopIterations(QString loopname)override;
		/**callback to set values of a specific loop iteration*/
		void setLoopIteration(QString loopname,int iteration)override;
		/**callback for bill generator: loop variables; see MOdtSignalRenderer for details*/
		QVariant getLoopVariable(QString,int,QString);

	private:
		MOOrder m_order;
		QMap<QString,int>m_loopiter;

		//printing buffer
		struct TickInfo{
			TickInfo(const MOTicket&t):proto(t){amount=1;}
			TickInfo(const TickInfo&t):proto(t.proto){amount=t.amount;}
			TickInfo(){amount=0;}
			MOTicket proto;
			int amount;
		};
		struct PrintBuffer{
			QList<MOTicket> tickets;
			QList<MOVoucher> vouchers;
			QList<TickInfo> tickinfo;
		}printBuffer;
};

#endif
