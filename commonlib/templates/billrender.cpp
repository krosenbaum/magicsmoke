//
// C++ Implementation: bill odtrender
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "billrender.h"
#include <QVariant>
#include <QDebug>

static inline bool compare(const MOTicket&a,const MOTicket&b)
{
	if(a.eventid()!=b.eventid())return false;
	if(a.price()!=b.price())return false;
	return true;
}

MBillRenderer::MBillRenderer ( const MOOrder&o, const MTemplate& tmp)
	:MOdtRenderer(tmp),m_order(o)
{
	//get tickets (only valid ones, only those that are to be paid)
	QList<MOTicket>tlst=m_order.tickets();
	for(int i=0;i<tlst.size();i++)
		if(!tlst[i].ticketid().isNull() && (tlst[i].status()&MOTicket::MaskPay)!=0){
			printBuffer.tickets.append(tlst[i]);
                }
	//accumulated view on tickets
	for(int i=0;i<printBuffer.tickets.size();i++){
		MOTicket t=printBuffer.tickets[i];
		bool found=false;
		for(int j=0;j<printBuffer.tickinfo.size();j++){
			if(compare(printBuffer.tickinfo[j].proto,t)){
				found=true;
				printBuffer.tickinfo[j].amount++;
				break;
			}
		}
		if(!found)printBuffer.tickinfo.append(t);
	}
	//get valid vouchers
	QList<MOVoucher>vlst=m_order.vouchers();
	for(int i=0;i<vlst.size();i++)
		if(vlst[i].price()>0||vlst[i].value()>0)
			printBuffer.vouchers.append(vlst[i]);
}

QVariant MBillRenderer::getVariable(QString vn)
{
	if(vn=="ORDERDATE"){
		return m_order.ordertime().value();
	}else
	if(vn=="ORDERDATETIME"){
		return m_order.ordertime().value();
	}else
	if(vn=="SENTDATE"){
		return m_order.senttime().value();
	}else
	if(vn=="SENTDATETIME"){
		return m_order.senttime().value();
	}else
	if(vn=="CUSTOMERID")return QString::number(m_order.customerid());else
	if(vn=="ORDERID")return QString::number(m_order.orderid());else
	if(vn=="ADDRESS")return m_order.fullInvoiceAddress();else
	if(vn=="FULLADDRESS")return m_order.fullInvoiceAddress();else
	if(vn=="NAME")return m_order.customer().value().fullName();else
	if(vn=="DELIVERYADDRESS")return m_order.fullDeliveryAddress();else
	if(vn=="FINALADDRESS")return m_order.fullDeliveryAddress();else
	if(vn=="TOTALPRICE"){
		return m_order.totalprice().value();
	}else
	if(vn=="AMOUNTPAID"){
		return m_order.amountpaid().value();
	}else
	if(vn=="SELLER")return m_order.soldby().value();else
	if(vn=="COMMENT")return m_order.comments().value();else
	if(vn=="AMOUNTTOPAY"){
		return m_order.amountToPay();
	}else
	if(vn=="AMOUNTTOREFUND"){
		return m_order.amountToRefund();
	}else
	if(vn=="TICKETS"){
		return printBuffer.tickets.size();
	}else
	if(vn=="ACCTICKETS"){
		return printBuffer.tickinfo.size();
	}else
	if(vn=="VOUCHERS"){
		return printBuffer.vouchers.size();
	}else
	if(vn=="ADDRESSLINES"){
		return m_order.fullInvoiceAddress().split("\n").size();
	}else
	if(vn=="SHIPPING")return m_order.shippingtype().value().description().value();else
	if(vn=="SHIPPINGPRICE"){
		return m_order.shippingtype().value().cost().value();
	}else
	if(vn=="SHIPPINGCOST"){
		return m_order.shippingcosts().value();
	}else{
		if(vn.contains(':')){
			QStringList sl=vn.split(':');
			int it=-1;
			if(m_loopiter.contains(sl[0]))it=m_loopiter[sl[0]];
			return getLoopVariable(sl[0],it,sl[1]);
		}
	}
// 	qDebug()<<"got variable"<<vn<<"value"<<value;
	qDebug()<<"Warning: requested unknown variable"<<vn;
	return QVariant();
}

int MBillRenderer::getLoopIterations(QString loopname)
{
	if(loopname=="TICKETS")return printBuffer.tickets.size();
	if(loopname=="ACCTICKETS")return printBuffer.tickinfo.size();
	if(loopname=="VOUCHERS")return printBuffer.vouchers.size();
	if(loopname=="ADDRESSLINES")return m_order.fullInvoiceAddress().split("\n").size();
// 	qDebug()<<"loop"<<loopname<<"has"<<iterations<<"iterations";
	//fall back
	qDebug()<<"Warning: requested unknown loop"<<loopname;
	return -1;
}

void MBillRenderer::setLoopIteration(QString loopname, int iteration)
{
// 	qDebug()<<"setting loop iter"<<loopname<<iteration;
	if(iteration<0)return;
	int max=getLoopIterations(loopname);
	if(iteration>=max)return;
	m_loopiter.insert(loopname,iteration);
}


QVariant MBillRenderer::getLoopVariable(QString loopname,int it,QString vn)
{
	if(loopname=="TICKETS"){
		QList<MOTicket> &tickets=printBuffer.tickets;
		if(it<0 || it>=tickets.size())return QVariant();

		if(vn=="PRICE"){
			return tickets[it].price().value();
		}else
		if(vn=="ID")return tickets[it].ticketid().value();else
		if(vn=="TITLE")return tickets[it].event().title().value();else
		if(vn=="ARTIST")return tickets[it].event().artist().value().name().value();else
		if(vn=="DATE"){
			return tickets[it].event().start().value();
		}else
		if(vn=="STARTTIME"){
			return tickets[it].event().start().value();
		}else
		if(vn=="ENDTIME"){
			return tickets[it].event().end().value();
		}else
		if(vn=="ROOM")return tickets[it].event().room().value();
	}else if(loopname=="ACCTICKETS"){
		QList<TickInfo> &tickets=printBuffer.tickinfo;
		if(it<0 || it>=tickets.size())return QVariant();

		if(vn=="PRICE"){
			return tickets[it].proto.price().value();
		}else
		if(vn=="FULLPRICE"){
			return tickets[it].proto.price().value()*tickets[it].amount;
		}else
		if(vn=="TITLE")return tickets[it].proto.event().title().value();else
		if(vn=="ARTIST")return tickets[it].proto.event().artist().value().name().value();else
		if(vn=="DATE"){
			return tickets[it].proto.event().start().value();
		}else
		if(vn=="STARTTIME"){
			return tickets[it].proto.event().start().value();
		}else
		if(vn=="ENDTIME"){
			return tickets[it].proto.event().end().value();
		}else
		if(vn=="ROOM")return tickets[it].proto.event().room().value();else
		if(vn=="AMOUNT"){
			return tickets[it].amount;
		}
	}else if(loopname=="VOUCHERS"){
		if(it<0 || it>=printBuffer.vouchers.size())return QVariant();

		if(vn=="PRICE"){
			return printBuffer.vouchers[it].price().value();
		}else
		if(vn=="VALUE"){
			return printBuffer.vouchers[it].value().value();
		}else
		if(vn=="ID")return printBuffer.vouchers[it].voucherid().value();
	}else if(loopname=="ADDRESSLINES"){
		QStringList lst=m_order.fullInvoiceAddress().split("\n");
		if(it<0 || it>=lst.size())return QVariant();
		return lst[it];
	}else{
		qDebug()<<"Warning: requested variable"<<vn<<"from unknown loop"<<loopname;
		return QVariant();
	}
	qDebug()<<"Warning: requested unknown variable"<<vn<<"from loop"<<loopname;
	return QVariant();
}

