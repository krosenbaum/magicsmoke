//
// C++ Interface: office
//
// Description: Wrapper and Config Dialog around OpenOffice.org
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_OFFICE_H
#define MAGICSMOKE_OFFICE_H

#include <QString>

/**calls OpenOffice.org to open an ODF file*/
void openOfficeFile(QString fname);

/**calls OpenOffice.org to print an ODF file*/
void printOfficeFile(QString fname);

///calls OpenOffice.org to convert an ODF file to PDF;
///returns the PDF file name on success, empty string on failure
QString convertOfficeFilePdf(QString fname);

#include <QDialog>

#include "commonexport.h"

class QComboBox;
class QCheckBox;
class QLineEdit;

/**configuration dialog for OpenOffice access*/
class MAGICSMOKE_COMMON_EXPORT MOfficeConfig:public QDialog
{
	Q_OBJECT
	public:
		MOfficeConfig(QWidget*);
		
	private slots:
		void savedata();
		void selectpath();
	private:
		QComboBox*printer;
		QCheckBox*viewonly,*doopen,*askprint,*saveprint;
		QLineEdit*oopath;
};


#endif
