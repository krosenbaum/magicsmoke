//
// C++ Implementation: office
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "office.h"
#include "misc.h"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPrinterInfo>
#include <QProcess>
#include <QPushButton>
#include <QSettings>

static QString getofficepath()
{
	return QSettings().value("officePath","soffice").toString();
}

void openOfficeFile(QString fname)
{
	//calculate parameters
	QStringList r;
	if(QSettings().value("officeViewOnly",false).toBool())r<<"-view";
	r<<fname;
	//open
	QProcess::startDetached(getofficepath(),r);
}

static void confirmPrinter(QString&prn)
{
	QDialog d;
	d.setWindowTitle(QCoreApplication::translate("office","Chose Printer"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(QCoreApplication::translate("office","Please chose a printer:")),0);
	QComboBox*printer;
	vl->addWidget(printer=new QComboBox,0);
	printer->setEditable(false);
	printer->addItem(QCoreApplication::translate("office","(Default Printer)"));
	QList<QPrinterInfo>aprn=QPrinterInfo::availablePrinters();
	int cprn=0;
	for(int i=0;i<aprn.size();i++){
		QString s=aprn[i].printerName();
		printer->addItem(s);
		if(s==prn)cprn=i+1;
	}
	printer->setCurrentIndex(cprn);
// 	qDebug()<<"printer selection"<<prn<<cprn;
	vl->addSpacing(10);
	vl->addStretch(1);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(QCoreApplication::translate("office","Ok")));
	QObject::connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	d.exec();
	if(printer->currentIndex()==0)prn="";
	else prn=printer->currentText();
}

void printOfficeFile(QString fname)
{
	//calculate parameters
	QStringList r;
	r<<"--headless";
	QString p=QSettings().value("officePrinter","").toString();
	if(QSettings().value("officePrinterConfirm",false).toBool())confirmPrinter(p);
	if(p=="")r<<"-p";
	else r<<"--pt"<<p;
	r<<fname;
	//print and wait for finish
	QProcess proc;
	proc.start(getofficepath(),r);
	proc.waitForFinished();
	//copy file
	if(QSettings().value("officePrinterSave",false).toBool()){
		QString fn=QFileDialog::getSaveFileName(0,QCoreApplication::translate("office","Save current document as..."),currentDir());
		if(fn=="")return;
		QFile(fn).remove();
		QFile(fname).copy(fn);
		setCurrentDir(fn);
	}
}

QString convertOfficeFilePdf(QString fname)
{
	QFileInfo finf(fname);
	//calculate parameters
	QStringList r;
	r<<"--headless"<<"--convert-to"<<"pdf"<<"--outdir"<<finf.dir().absolutePath();
	r<<fname;
	//print and wait for finish
	QProcess proc;
	proc.start(getofficepath(),r);
	proc.waitForFinished();
	//calculate new name
	QString pname=finf.dir().absoluteFilePath(finf.completeBaseName()+".pdf");
	if(QFile::exists(pname))return pname;
	else return QString();
}

MOfficeConfig::MOfficeConfig(QWidget*par)
	:QDialog(par)
{
	setWindowTitle(tr("Configure LibreOffice Access"));
	
	QVBoxLayout*vl,*vl2;
	setLayout(vl=new QVBoxLayout);
	QSettings set;
	
	QGroupBox*gb;
	QHBoxLayout*hl;
	QPushButton*p;
	
	vl->addWidget(gb=new QGroupBox(tr("LibreOffice")));
	gb->setLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Path to Executable:")));
	hl->addWidget(oopath=new QLineEdit(set.value("officePath","soffice").toString()),1);
	hl->addWidget(p=new QPushButton(tr("...","select LibreOffice path button")));
	connect(p,SIGNAL(clicked()),this,SLOT(selectpath()));
	
	vl->addWidget(gb=new QGroupBox(tr("Printing ODF")));
	gb->setLayout(vl2=new QVBoxLayout);
	vl2->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Printer:")));
	hl->addWidget(printer=new QComboBox,1);
	printer->setEditable(false);
	printer->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	printer->setMinimumContentsLength(10);
	printer->addItem(tr("(Default Printer)"));
	QList<QPrinterInfo>aprn=QPrinterInfo::availablePrinters();
	int cprn=0;
	QString cp=set.value("officePrinter","").toString();
	for(int i=0;i<aprn.size();i++){
		QString s=aprn[i].printerName();
		printer->addItem(s);
		if(cp==s)cprn=i+1;
	}
	printer->setCurrentIndex(cprn);
	vl2->addWidget(askprint=new QCheckBox(tr("Always confirm printer when printing ODF")));
	askprint->setChecked(set.value("officePrinterConfirm",false).toBool());
	vl2->addWidget(saveprint=new QCheckBox(tr("Save printed files")));
	saveprint->setChecked(set.value("officePrinterSave",false).toBool());
	
	vl->addWidget(gb=new QGroupBox(tr("Opening ODF")));
	gb->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(viewonly=new QCheckBox(tr("Always open as Read-Only")));
	viewonly->setChecked(set.value("officeViewOnly",false).toBool());
	vl2->addWidget(doopen=new QCheckBox(tr("Automatically open all newly created files")));
	doopen->setChecked(set.value("doOpenODFs",false).toBool());
	
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	hl->addWidget(p=new QPushButton(tr("OK")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	connect(p,SIGNAL(clicked()),this,SLOT(savedata()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

void MOfficeConfig::savedata()
{
	QSettings set;
	int cprn=printer->currentIndex();
	if(cprn==0)set.setValue("officePrinter","");
	else set.setValue("officePrinter",printer->currentText());
	
	set.setValue("officePath",oopath->text());
	set.setValue("officeViewOnly",viewonly->isChecked());
	set.setValue("doOpenODFs",doopen->isChecked());
	set.setValue("officePrinterConfirm",askprint->isChecked());
	set.setValue("officePrinterSave",saveprint->isChecked());
}

void MOfficeConfig::selectpath()
{
	QString np=QFileDialog::getOpenFileName(this,tr("Select LibreOffice executable"),QFileInfo(oopath->text()).dir().absolutePath());
	if(np!=""){
		oopath->setText(np);
		setCurrentDir(np);
	}
}
