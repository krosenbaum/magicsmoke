HEADERS += \
	$$PWD/odtrender.h \
	$$PWD/ticketrender.h \
	$$PWD/office.h \
	$$PWD/labeldlg.h \
	$$PWD/billrender.h

SOURCES += \
	$$PWD/odtrender.cpp \
	$$PWD/ticketrender.cpp \
	$$PWD/office.cpp \
	$$PWD/labeldlg.cpp \
	$$PWD/billrender.cpp

INCLUDEPATH += $$PWD
