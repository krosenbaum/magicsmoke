//
// C++ Implementation: labeldlg
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "labeldlg.h"

#include "msinterface.h"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QCoreApplication>
#include <QDebug>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
// #include <QPrintDialog>
#include <QPageLayout>
#include <QPrinter>
#include <QPrinterInfo>
#include <QPushButton>
#include <QScrollArea>
#include <QSettings>
#include <QSignalMapper>
#include <QToolButton>

#define ROWS 20
#define COLS 20

MLabelDialog::MLabelDialog(QWidget*par,QPrinter* pn,int nl,QSizeF ls)
    :QDialog(par)
{
    printer=pn;
    numlabels=nl;
    labelsize=ls;
    maxrows=ROWS;
    maxcols=COLS;

    setWindowTitle(tr("Label Printing Setup"));

    //get settings
    QSettings set;
    set.beginGroup("printer/"+printer->printerName());
    double ox=set.value("offsetx",0.0).toDouble();
    double oy=set.value("offsety",0.0).toDouble();
    double lsx=set.value("sizex",0.0).toDouble();
    double lsy=set.value("sizey",0.0).toDouble();
    QString mtrc=set.value("metric",tr("mm","defaultmetric: mm, in, cm")).toString();
    if(lsx==0.0 || lsy==0.0){
        //convert ticket template size
        double dx=printer->logicalDpiX();
        double dy=printer->logicalDpiY();
        if(mtrc=="in"){
            lsx=ls.width()/dx;
            lsy=ls.height()/dy;
        }else
        if(mtrc=="cm"){
            lsx=ls.width()/dx*2.54;
            lsy=ls.height()/dy*2.54;
        }else{//mm
            lsx=ls.width()/dx*25.4;
            lsy=ls.height()/dy*25.4;
        }
    }

    //display

    QVBoxLayout*vl;
    QGridLayout*gl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(gl=new QGridLayout,0);

    int ln=0;
    gl->addWidget(new QLabel(tr("Label offset:")),ln,0);
    gl->addWidget(offx=new QLineEdit(QString::number(ox)),ln,1);
    offx->setValidator(new QDoubleValidator(0.0,1000.0,2,this));
    gl->addWidget(new QLabel("x"),ln,2);
    gl->addWidget(offy=new QLineEdit(QString::number(oy)),ln,3);
    offy->setValidator(new QDoubleValidator(0.0,1000.0,2,this));
    gl->addWidget(new QLabel(tr("Label size:")),++ln,0);
    gl->addWidget(sizex=new QLineEdit(QString::number(lsx)),ln,1);
    sizex->setValidator(new QDoubleValidator(0.0,1000.0,2,this));
    gl->addWidget(new QLabel("x"),ln,2);
    gl->addWidget(sizey=new QLineEdit(QString::number(lsy)),ln,3);
    sizey->setValidator(new QDoubleValidator(0.0,1000.0,2,this));
    gl->addWidget(new QLabel(tr("Unit:")),++ln,0);
    gl->addWidget(metric=new QComboBox,ln,1,1,3);
    metric->addItem(tr("Millimeter"),"mm");
    metric->addItem(tr("Centimeter"),"cm");
    metric->addItem(tr("Inch"),"in");
    gl->addWidget(warning=new QLabel(""),++ln,0,1,4);
    QPalette pal=warning->palette();
    pal.setColor(QPalette::WindowText,Qt::red);
    warning->setPalette(pal);
    gl->setColumnStretch(++ln,10);

    vl->addSpacing(10);
    vl->addWidget(new QLabel(tr("Page usage:")),0);
    vl->addWidget(page=new QComboBox,0);
    for(int i=0;i<nl;i++)
        page->addItem(tr("Page %1").arg(i+1),i);
    oldpage=0;

    QScrollArea *sa;
    QWidget*w=new QWidget;
    vl->addWidget(sa=new QScrollArea,10);
    w->setLayout(gl=new QGridLayout);
    QToolButton*t;
    gl->addWidget(t=new QToolButton,0,0);
    t->setIcon(QIcon(":/arrowdiag.png"));
    connect(t,SIGNAL(clicked()),this,SLOT(toggleAll()));
    QSignalMapper *cmap=new QSignalMapper(this);
    connect(cmap,SIGNAL(mapped(int)),this,SLOT(toggleColumn(int)));
    for(int i=1;i<=COLS;i++){
        gl->addWidget(t=new QToolButton,0,i);
        t->setIcon(QIcon(":/arrowdown.png"));
        connect(t,SIGNAL(clicked()),cmap,SLOT(map()));
        cmap->setMapping(t,i-1);
    }
    QSignalMapper *rmap=new QSignalMapper(this);
    connect(rmap,SIGNAL(mapped(int)),this,SLOT(toggleRow(int)));
    for(int i=1;i<=ROWS;i++){
        gl->addWidget(t=new QToolButton,i,0);
        t->setIcon(QIcon(":/arrowright.png"));
        connect(t,SIGNAL(clicked()),rmap,SLOT(map()));
        rmap->setMapping(t,i-1);
        for(int j=1;j<=COLS;j++){
            QCheckBox*b;
            gl->addWidget(b=new QCheckBox,i,j);
            checks.append(b);
            connect(b,SIGNAL(clicked()),this,SLOT(savePage()));
        }
    }
    sa->setWidget(w);
    connect(page,SIGNAL(currentIndexChanged(int)),this,SLOT(updatePage()));
    connect(sizex,SIGNAL(textChanged(const QString&)),this,SLOT(updatePage()));
    connect(sizey,SIGNAL(textChanged(const QString&)),this,SLOT(updatePage()));
    connect(metric,SIGNAL(currentIndexChanged(int)),this,SLOT(updatePage()));

    vl->addSpacing(15);
    QHBoxLayout*hl;
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(10);
    QPushButton*p;
    hl->addWidget(p=new QPushButton(tr("Ok")),0);
    connect(p,SIGNAL(clicked()),this,SLOT(accept()));
    connect(p,SIGNAL(clicked()),this,SLOT(saveSettings()));
    p->setDefault(true);
    hl->addWidget(p=new QPushButton(tr("Cancel")),0);
    connect(p,SIGNAL(clicked()),this,SLOT(reject()));

    //initialize
    QList<bool>tpl;
    for(int i=0;i<(ROWS*COLS);i++)tpl.append(true);
    for(int i=0;i<nl;i++)checked.append(tpl);
    updatePage();
}

QDataStream &operator<<(QDataStream &out, const MLabelConfig &myObj)
{
        char t=myObj.m_type;
        out.writeBytes(&t,1);
        return out;
}
QDataStream &operator>>(QDataStream &in, MLabelConfig &myObj)
{
        char *t;
        uint l;
        in.readBytes(t,l);
        if(t && l>0)
                myObj.m_type=(MLabelConfig::LabelType)(*t);
        if(t)delete t;
        return in;
}

void MLabelConfig::initMeta()
{
        static int mt=-1;
        if(mt>=0)return;
        mt=qRegisterMetaType<MLabelConfig>();
#if QT_VERSION < 0x060000
        qRegisterMetaTypeStreamOperators<MLabelConfig>();
#endif
}

MLabelDialog::~MLabelDialog(){}

int MLabelDialog::findLabel(int n,int&row,int&col)const
{
    //search allowed labels
    int ctr=-1;
    for(int p=0;p<numlabels;p++)
        for(int r=maxrows-1;r>=0;r--)
            for(int c=0;c<maxcols;c++){
                if(checked[p][r*COLS+c])ctr++;
                if(ctr==n){
                    row=r;
                    col=c;
                    return p;
                }
            }
    //fail
    return -1;
}

QPointF MLabelDialog::labelOffset(int n)const
{
    //find it
    int row=0,col=0;
    findLabel(n,row,col);
    //get offset
    double px=offx->text().toDouble();
    double py=offy->text().toDouble();
    //add rows/cols
    px+=col*sizex->text().toDouble();
    py+=row*sizey->text().toDouble();
    //correct to DPI
    QString mtrc=metric->itemData(metric->currentIndex()).toString();
    if(mtrc=="in"){
        px*=printer->logicalDpiX();
        py*=printer->logicalDpiY();
    }else
    if(mtrc=="cm"){
        px*=printer->logicalDpiX()/2.54;
        py*=printer->logicalDpiY()/2.54;
    }else{
        px*=printer->logicalDpiX()/25.4;
        py*=printer->logicalDpiY()/25.4;
    }
    //return
    return QPointF(px,py);
}

bool MLabelDialog::labelNeedsPageTurn(int n)const
{
    //find it
    int pg,row=0,col=0;
    pg=findLabel(n,row,col);
    //page 0 needs no turn
    if(pg<=0)return false;
    //scan that page (all rows below label)
    for(int r=maxrows-1;r>row;r--)
        for(int c=0;c<maxcols;c++)
            if(checked[pg][r*COLS+c])return false;
    //scan that page (all cols left of label)
    for(int c=0;c<col;c++)
        if(checked[pg][row*COLS+c])return false;
    //none found, this is the first one: turn page
    return true;
}

bool MLabelDialog::oneLabelPerPage() const
{
    return maxrows<=1 && maxcols<=1;
}

bool MLabelDialog::conditionalExec(MLabelConfig::LabelType lt)
{
    //check config
    bool doexec=false;
    switch(MLabelConfig(lt).pageAskMode()){
        case MLabelConfig::PageAskAlways:doexec=true;break;
        case MLabelConfig::PageAskIfNeeded:doexec=!oneLabelPerPage();break;
        case MLabelConfig::PageAskNever:return true;
    }
    //execute, or not
    if(doexec)
        return exec()==Accepted;
    else
        return true;
}

void MLabelDialog::toggleRow(int r)
{
    for(int i=r*COLS;i<(r*COLS+COLS);i++){
        checked[oldpage][i]^=true;
        checks[i]->setChecked(checked[oldpage][i]);
    }
}

void MLabelDialog::toggleColumn(int c)
{
    for(int r=0;r<ROWS;r++){
        int i=r*COLS+c;
        checked[oldpage][i]^=true;
        checks[i]->setChecked(checked[oldpage][i]);
    }
}

void MLabelDialog::toggleAll()
{
    for(int i=0;i<(ROWS*COLS);i++){
        checked[oldpage][i]^=true;
        checks[i]->setChecked(checked[oldpage][i]);
    }
}

void MLabelDialog::savePage()
{
    for(int i=0;i<(ROWS*COLS);i++)
        checked[oldpage][i]=checks[i]->isChecked();
}

void MLabelDialog::updatePage()
{
    //find how many rows/cols fit on the page
    double lx=sizex->text().toDouble();
    double ly=sizey->text().toDouble();
    QRectF pr=printer->pageRect(QPrinter::DevicePixel);
    QString mtrc=metric->itemData(metric->currentIndex()).toString();
    if(mtrc=="in"){
        lx*=printer->logicalDpiX();
        ly*=printer->logicalDpiY();
    }else
    if(mtrc=="cm"){
        lx*=printer->logicalDpiX()/2.54;
        ly*=printer->logicalDpiY()/2.54;
    }else{
        lx*=printer->logicalDpiX()/25.4;
        ly*=printer->logicalDpiY()/25.4;
    }

    bool dowarn=false;
    if(ly>0.0){
        maxrows=pr.height()/ly;
        if(maxrows>ROWS)maxrows=ROWS;
        if(maxrows<1){
            maxrows=1;
            dowarn=true;
        }
    }else maxrows=ROWS;
    if(lx>0.0){
        maxcols=pr.width()/lx;
        if(maxcols>COLS)maxcols=COLS;
        if(maxcols<1){
            maxcols=1;
            dowarn=true;
        }
    }else maxcols=COLS;
    //update
    oldpage=page->itemData(page->currentIndex()).toInt();
    for(int r=0;r<ROWS;r++)
        for(int c=0;c<COLS;c++){
            int i=r*COLS+c;
            bool b=r<maxrows && c<maxcols;
            checks[i]->setChecked(b && checked[oldpage][i]);
            checks[i]->setEnabled(b);
        }
    if(dowarn)
        warning->setText(tr("Warning: the label may not fit on the page!"));
    else
        warning->setText("");
}

void MLabelDialog::saveSettings()
{
    QSettings set;
    set.beginGroup("printer/"+printer->printerName());
    set.setValue("offsetx",offx->text().toDouble());
    set.setValue("offsety",offy->text().toDouble());
    set.setValue("sizex",sizex->text().toDouble());
    set.setValue("sizey",sizey->text().toDouble());
    set.setValue("metric",metric->itemData(metric->currentIndex()).toString());
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void MLabelConfig::configDialog(QWidget* parent)
{
    QDialog d(parent);
    MLabelConfig ticket(MLabelConfig::Ticket),voucher(MLabelConfig::Voucher);
    d.setWindowTitle(tr("Label Configuration"));
    QFormLayout*fl;
    d.setLayout(fl=new QFormLayout);
    fl->addRow(new QLabel(tr("Ticket Labels:")));
    //each one: ask printer mode, ask label-page mode, printer/settings
    QComboBox*t_prnmode,*t_pagmode,*t_prnbufmode;
    fl->addRow(tr("Print Dialog:"),t_prnmode=new QComboBox);
    t_prnmode->addItem(tr("Always ask for printer"),PrintAskAlways);
    t_prnmode->addItem(tr("Ask if unknown or not present"),PrintAskIfUnknown);
    t_prnmode->addItem(tr("Never ask for printer"),PrintAskNever);
    t_prnmode->setCurrentIndex(ticket.printAskMode());
    fl->addRow(tr("Page Dialog:"),t_pagmode=new QComboBox);
    t_pagmode->addItem(tr("Always ask for page layout"),PageAskAlways);
    t_pagmode->addItem(tr("Ask if more than one label per page"),PageAskIfNeeded);
    t_pagmode->addItem(tr("Never ask for page layout"),PageAskNever);
    t_pagmode->setCurrentIndex(ticket.pageAskMode());
    QHBoxLayout*hl;
    QLabel*t_prn;QMap<QString,QVariant>t_prnset;
    QToolButton*p;
    QPrinter t_printer;
    ticket.configurePrinter(t_printer);
    fl->addRow(tr("Printer:"),hl=new QHBoxLayout);
    hl->addWidget(t_prn=new QLabel(ticket.printerName()),1);
    hl->addWidget(p=new QToolButton);
    p->setText("...");
    auto settprn=[&]{
        MLabelPrintDialog pd(&t_printer,&d);
                pd.setWindowTitle(tr("Chose Printer for Tickets"));
                pd.setButtonLabel(tr("Set Printer"));
//                 pd.setOptions(QAbstractPrintDialog::PrintShowPageSize);
        if(pd.exec()!=QDialog::Accepted)return;
        t_prn->setText(t_printer.printerName());
    };
    d.connect(p,&QToolButton::clicked,settprn);
    fl->addRow(tr("Print Mode:"),t_prnbufmode=new QComboBox);
    t_prnbufmode->addItem(tr("Direct Print"));
    t_prnbufmode->addItem(tr("Use Buffer Pixmap"));
    t_prnbufmode->setCurrentIndex((int)ticket.printMode());
    QFrame*frm;
    fl->addRow(frm=new QFrame);
    frm->setFrameStyle(QFrame::HLine|QFrame::Sunken);
    //Vouchers...
    fl->addRow(new QLabel(tr("Voucher Labels:")));
    //each one: ask printer mode, ask label-page mode, printer/settings
    QComboBox*v_prnmode,*v_pagmode,*v_prnbufmode;
    fl->addRow(tr("Print Dialog:"),v_prnmode=new QComboBox);
    v_prnmode->addItem(tr("Always ask for printer"),PrintAskAlways);
    v_prnmode->addItem(tr("Ask if unknown or not present"),PrintAskIfUnknown);
    v_prnmode->addItem(tr("Never ask for printer"),PrintAskNever);
    v_prnmode->setCurrentIndex(voucher.printAskMode());
    fl->addRow(tr("Page Dialog:"),v_pagmode=new QComboBox);
    v_pagmode->addItem(tr("Always ask for page layout"),PageAskAlways);
    v_pagmode->addItem(tr("Ask if more than one label per page"),PageAskIfNeeded);
    v_pagmode->addItem(tr("Never ask for page layout"),PageAskNever);
    v_pagmode->setCurrentIndex(voucher.pageAskMode());
    QLabel*v_prn;QMap<QString,QVariant>v_prnset;
    QPrinter v_printer;
    voucher.configurePrinter(v_printer);
    fl->addRow(tr("Printer:"),hl=new QHBoxLayout);
    hl->addWidget(v_prn=new QLabel(voucher.printerName()),1);
    hl->addWidget(p=new QToolButton);
    p->setText("...");
    auto setvprn=[&]{
        MLabelPrintDialog pd(&v_printer,&d);
                pd.setWindowTitle(tr("Chose Printer for Vouchers"));
                pd.setButtonLabel(tr("Set Printer"));
//                 pd.setOptions(QAbstractPrintDialog::PrintShowPageSize);
        if(pd.exec()!=QDialog::Accepted)return;
        v_prn->setText(v_printer.printerName());
    };
    d.connect(p,&QToolButton::clicked,setvprn);
    fl->addRow(tr("Print Mode:"),v_prnbufmode=new QComboBox);
    v_prnbufmode->addItem(tr("Direct Print"));
    v_prnbufmode->addItem(tr("Use Buffer Pixmap"));
    v_prnbufmode->setCurrentIndex((int)voucher.printMode());
    //button...
    fl->addRow(new QLabel(" "));
    fl->addRow(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*b;
    hl->addWidget(b=new QPushButton(tr("Ok")));
    d.connect(b,SIGNAL(clicked()),&d,SLOT(accept()));
    hl->addWidget(b=new QPushButton(tr("Cancel")));
    d.connect(b,SIGNAL(clicked()),&d,SLOT(reject()));
    //show dialog
    if(d.exec()!=QDialog::Accepted)return;
    //save settings
    ticket.storePrinter(t_printer);
    ticket.storeAskModes(
        (MLabelConfig::PrintAskMode)t_prnmode->currentIndex(),
        (MLabelConfig::PageAskMode)t_pagmode->currentIndex()
    );
    ticket.storePrintMode((MLabelConfig::PrintMode)t_prnbufmode->currentIndex());
    voucher.storePrinter(v_printer);
    voucher.storeAskModes(
        (MLabelConfig::PrintAskMode)v_prnmode->currentIndex(),
        (MLabelConfig::PageAskMode)v_pagmode->currentIndex()
    );
    voucher.storePrintMode((MLabelConfig::PrintMode)v_prnbufmode->currentIndex());
}

QString MLabelConfig::tr(const char* s, const char* c)
{
    return QCoreApplication::translate("MLabelConfig",s,c);
}

QString MLabelConfig::printerName() const
{
    QString key=m_type==Ticket?"ticket":"voucher";
    QSettings set;
    set.beginGroup(req->settingsGroup()+"/"+key);
    if(!set.contains("name"))return QString();
    return set.value("name").toString();
}

bool MLabelConfig::selectPrinter(QPrinter& printer, bool force)
{
    bool gotit=configurePrinter(printer);
    if(!force)
    switch(printAskMode()){
        case PrintAskAlways:force=true;break;
        case PrintAskIfUnknown:if(!gotit)force=true;break;
        case PrintAskNever:/*do not override the force*/ break;
    }
    if(force){
        MLabelPrintDialog pd(&printer);
        return pd.exec()==QDialog::Accepted;
    }else
        return gotit;
}

QString MLabelConfig::cgroup() const
{
    const QString key=m_type==Ticket?"ticket":"voucher";
    return req->settingsGroup()+"/"+key;
}

static inline void dumpPrn(QPrinter&p,QString s)
{
        qDebug()<<"printer"<<p.printerName()<<s<<"valid?"<<p.isValid();
        qDebug()<<"  page mm"<<p.pageLayout().pageSize().rect(QPageSize::Millimeter); //pageSizeMM();
        //qreal ml,mr,mt,mb;
        auto marg=p.pageLayout().margins(QPageLayout::Millimeter);
        //p.getPageMargins(&ml,&mr,&mb,&mt,QPrinter::Millimeter);
        qDebug()<<"  margin"<<marg;//<<ml<<mr<<mb<<mt;
        qDebug()<<"  duplex"<<p.duplex()
        <<"orient"<<(p.pageLayout().orientation()==QPageLayout::Portrait?"portrait":"landscape");
        qDebug()<<"  resolution"<<p.resolution();
}

bool MLabelConfig::configurePrinter(QPrinter&prn ) const
{
    QSettings set;
    set.beginGroup(cgroup());
    if(!set.contains("name"))return false;
    QString pname=set.value("name").toString();
    bool found=false;
    for(const QPrinterInfo &info : QPrinterInfo::availablePrinters()){
        if(info.printerName()==pname){
            found=true;
            break;
        }
    }
    if(!found){
                qDebug()<<"MLabelConfig::configurePrinter could not find printer"<<pname<<"falling back to system default printer";
                return false;
        }
    prn.setPrinterName(pname);
        dumpPrn(prn,"before setting printer object");
#if !defined(Q_OS_WIN32) && !defined(Q_OS_WINCE)
    prn.setPrinterSelectionOption(set.value("soption","").toString());
#endif
    QPageSize::PageSizeId pgsz=QPageSize::PageSizeId(set.value("paper",QPageSize::Custom).toInt());
    auto pglayout=prn.pageLayout();
    pglayout.setPageSize(QPageSize(pgsz));
    prn.setPageLayout(pglayout);
    if(pgsz==QPageSize::Custom)
        prn.setPageSize(QPageSize(set.value("papersizemm").toSizeF(),QPageSize::Millimeter));
    qreal ml,mr,mt,mb;
    ml=set.value("marginLeft").toDouble();
    mr=set.value("marginRight").toDouble();
    mb=set.value("marginBottom").toDouble();
    mt=set.value("marginTop").toDouble();
    prn.setFullPage(set.value("fullPage",true).toBool());
    prn.setPageMargins(QMarginsF(ml,mt,mr,mb),QPageLayout::Millimeter);
    prn.setDuplex(QPrinter::DuplexMode(set.value("duplex",QPrinter::DuplexAuto).toInt()));
    prn.setPageOrientation((set.value("orientation","portrait").toString()=="portrait")?QPageLayout::Portrait:QPageLayout::Landscape);
    if(set.contains("resolution"))
        prn.setResolution(set.value("resolution").toInt());
    if(set.contains("papersource"))
        prn.setPaperSource(QPrinter::PaperSource(set.value("papersource").toInt()));
    dumpPrn(prn,"after setting printer object");
    return true;
}

void MLabelConfig::storePrinter(QPrinter& prn)
{
    QSettings set;
    set.beginGroup(cgroup());
    set.setValue("name",prn.printerName());
#if !defined(Q_OS_WIN32) && !defined(Q_OS_WINCE)
    set.setValue("soption",prn.printerSelectionOption());
#endif
    //qreal ml,mr,mt,mb;
    //prn.getPageMargins(&ml,&mr,&mb,&mt,QPrinter::Millimeter);
    QMarginsF marg=prn.pageLayout().margins(QPageLayout::Millimeter);
    set.setValue("marginLeft",marg.left());
    set.setValue("marginRight",marg.right());
    set.setValue("marginBottom",marg.bottom());
    set.setValue("marginTop",marg.top());
    set.setValue("fullPage",prn.fullPage());
    set.setValue("duplex",(int)prn.duplex());
    set.setValue("orientation",prn.pageLayout().orientation()==QPageLayout::Portrait?"portrait":"landscape");
    set.setValue("paper",(int)prn.paperSize());
//         set.setValue("papersize",prn.paperSize(QPrinter::Point));
    set.setValue("papersizemm",prn.pageSizeMM());
    set.setValue("resolution",prn.resolution());
    set.setValue("papersource",prn.paperSource());
    set.setValue("settings",QVariant::fromValue(*this));
    qDebug()<<"storing printer settings"<<cgroup()<<prn.printerName()<<prn.paperSize();
}


MLabelConfig::PageAskMode MLabelConfig::pageAskMode() const
{
    QSettings set;
    set.beginGroup(cgroup());
    return PageAskMode(set.value("pageAskMode",PageAskIfNeeded).toInt());
}

MLabelConfig::PrintAskMode MLabelConfig::printAskMode() const
{
    QSettings set;
    set.beginGroup(cgroup());
    return PrintAskMode(set.value("printAskMode",PrintAskIfUnknown).toInt());
}

void MLabelConfig::storeAskModes(MLabelConfig::PrintAskMode prm, MLabelConfig::PageAskMode pgm)
{
    QSettings set;
    set.beginGroup(cgroup());
    set.setValue("printAskMode",(int)prm);
    set.setValue("pageAskMode",(int)pgm);
}

MLabelConfig::PrintMode MLabelConfig::printMode() const
{
    QSettings set;
    set.beginGroup(cgroup());
    return PrintMode(set.value("printBufMode",PrintDirect).toInt());
}

void MLabelConfig::storePrintMode ( MLabelConfig::PrintMode pm )
{
    QSettings set;
    set.beginGroup(cgroup());
    set.setValue("printBufMode",(int)pm);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

struct Paper_s {
        int ptype=0;
        QSizeF psize;
        QString pname;

        Paper_s()=default;
        Paper_s(int t):ptype(t){}
        Paper_s(const Paper_s&)=default;
        Paper_s(const QString&n,int t,double x,double y):ptype(t),psize(x,y),pname(n){}

        Paper_s&operator=(const Paper_s&)=default;

        bool operator==(const Paper_s&t)const{return ptype==t.ptype;}
        bool operator==(int t)const{return ptype==t;}
};

static QList<Paper_s> paperTypes;

void MLabelPrintDialog::initPaper()
{
        if(paperTypes.size())return;
        paperTypes=QList<Paper_s>()
        << Paper_s(tr("Custom Size","paper type"),QPageSize::Custom,0,0)
        << Paper_s(tr("A0","paper type"),QPageSize::A0,841,1189)
        << Paper_s(tr("A1","paper type"),QPageSize::A1,594,841)
        << Paper_s(tr("A2","paper type"),QPageSize::A2,420,594)
        << Paper_s(tr("A3","paper type"),QPageSize::A3,297,420)
        << Paper_s(tr("A4","paper type"),QPageSize::A4,210,297)
        << Paper_s(tr("A5","paper type"),QPageSize::A5,148,210)
        << Paper_s(tr("A6","paper type"),QPageSize::A6,105,148)
        << Paper_s(tr("A7","paper type"),QPageSize::A7,74,105)
        << Paper_s(tr("A8","paper type"),QPageSize::A8,52,74)
        << Paper_s(tr("A9","paper type"),QPageSize::A9,37,52)
        << Paper_s(tr("B0","paper type"),QPageSize::B0,1000,1414)
        << Paper_s(tr("B1","paper type"),QPageSize::B1,707,1000)
        << Paper_s(tr("B2","paper type"),QPageSize::B2,500,707)
        << Paper_s(tr("B3","paper type"),QPageSize::B3,353,500)
        << Paper_s(tr("B4","paper type"),QPageSize::B4,250,353)
        << Paper_s(tr("B5","paper type"),QPageSize::B5,176,250)
        << Paper_s(tr("B6","paper type"),QPageSize::B6,125,176)
        << Paper_s(tr("B7","paper type"),QPageSize::B7,88,125)
        << Paper_s(tr("B8","paper type"),QPageSize::B8,62,88)
        << Paper_s(tr("B9","paper type"),QPageSize::B9,33,62)
        << Paper_s(tr("B10","paper type"),QPageSize::B10,31,44)
        << Paper_s(tr("C5E","paper type"),QPageSize::C5E,163,229)
        << Paper_s(tr("U.S. Common 10 Envelope","paper type"),QPageSize::Comm10E,105,241)
        << Paper_s(tr("DLE","paper type"),QPageSize::DLE,110,220)
        << Paper_s(tr("Executive","paper type"),QPageSize::Executive,190.5,254)
        << Paper_s(tr("Folio","paper type"),QPageSize::Folio,210,330)
        << Paper_s(tr("Ledger","paper type"),QPageSize::Ledger,431.8,279.4)
        << Paper_s(tr("Legal","paper type"),QPageSize::Legal,215.9,355.6)
        << Paper_s(tr("Letter","paper type"),QPageSize::Letter,215.9,279.4)
        << Paper_s(tr("Tabloid","paper type"),QPageSize::Tabloid,279.4,431.8)
        ;
}



MLabelPrintDialog::MLabelPrintDialog(QPrinter* prn, QWidget* parent, Qt::WindowFlags f)
        : QDialog(parent, f)
        , mprinter(prn)
{
        setWindowTitle(tr("Chose Printer"));
        QFormLayout*fl;
        setLayout(fl=new QFormLayout);
        fl->addRow(tr("Printer:"),mprnselect=new QComboBox);
    mprnselect->setMinimumContentsLength(10);
    mprnselect->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
        int cur=-1,def=-1;
        for(const QPrinterInfo&info:QPrinterInfo::availablePrinters()){
                if(info.printerName()==prn->printerName())cur=mprnselect->count();
                if(info.isDefault())def=mprnselect->count();
                mprnselect->addItem(info.printerName());
        }
        if(cur>=0)mprnselect->setCurrentIndex(cur);
        else if(def>=0)mprnselect->setCurrentIndex(def);
        connect(mprnselect,SIGNAL(currentIndexChanged(int)), this,SLOT(printerChanged(int)));
        fl->addRow(tr("Make & Model:"),mmakemodel=new QLabel);
        fl->addRow(tr("Location:"),mlocation=new QLabel);
        fl->addRow(tr("Description:"),mdescript=new QLabel);
        fl->addRow(tr("Unit of Measure:"),munit=new QComboBox);
        munit->addItem(tr("Millimeter"),1.);
        munit->addItem(tr("Centimeter"),10.);
        munit->addItem(tr("Inch"),25.4);
        connect(munit,SIGNAL(currentIndexChanged(int)),this,SLOT(unitChanged(int)));
        fl->addRow(tr("Paper Type:"),mpaper=new QComboBox);
        initPaper();
        cur=0;
        for(const auto&paper:paperTypes){
                if(mprinter->paperSize()==paper.ptype)cur=mpaper->count();
                mpaper->addItem(paper.pname,paper.ptype);
        }
        mpaper->setCurrentIndex(cur);
        connect(mpaper,SIGNAL(currentIndexChanged(int)), this,SLOT(paperChanged(int)));
        QHBoxLayout*hl;
        fl->addRow(tr("Paper Size:"),hl=new QHBoxLayout);
        hl->addWidget(msizex=new QDoubleSpinBox,1);
        msizex->setRange(0,10000);
        hl->addWidget(new QLabel(tr("wide x","paper size: width")));
        hl->addWidget(msizey=new QDoubleSpinBox);
        msizey->setRange(0,10000);
        hl->addWidget(new QLabel(tr("high","paper size: height")));
        QGridLayout*gl;
        fl->addRow(tr("Margins:"),gl=new QGridLayout);
        //qreal ml,mr,mt,mb;
    QMarginsF marg=mprinter->pageLayout().margins(QPageLayout::Millimeter);
        //mprinter->getPageMargins(&ml,&mt,&mr,&mb,QPrinter::Millimeter);
    gl->addWidget(mfullpage=new QCheckBox(tr("Use Full Page")),0,0,1,3);
    mfullpage->setChecked(mprinter->fullPage());
        gl->addWidget(mmargtop=new QDoubleSpinBox,1,1);
        mmargtop->setValue(marg.top()/moldunit);
        gl->addWidget(mmargleft=new QDoubleSpinBox,2,0);
        mmargleft->setValue(marg.left()/moldunit);
        gl->addWidget(mmargright=new QDoubleSpinBox,2,2);
        mmargright->setValue(marg.right()/moldunit);
        gl->addWidget(mmargbottom=new QDoubleSpinBox,3,1);
        mmargbottom->setValue(marg.bottom()/moldunit);
        fl->addRow(tr("Resolution:"),mresolution=new QComboBox);
        fl->addRow(new QLabel(" "));
        fl->addRow(hl=new QHBoxLayout);
        hl->addStretch(1);
        hl->addWidget(mokbtn=new QPushButton(tr("Print")));
        connect(mokbtn,SIGNAL(clicked(bool)),this,SLOT(okBtnPressed()));
        QPushButton *cc;
        hl->addWidget(cc=new QPushButton(tr("Cancel")));
        connect(cc,SIGNAL(clicked(bool)),this,SLOT(reject()));

        //make sure it is selected
        printerChanged(mprnselect->currentIndex());

        //reset selected paper type and size from printer
        paperChanged(mpaper->currentIndex());
        //reset margins from printer
}

void MLabelPrintDialog::setButtonLabel(const QString& l)
{
        mokbtn->setText(l);
}

// QString MLabelPrintDialog::paperSourceStr(int p)
// {
//         switch(p){
//                 case QPrinter::Auto:return tr("Automatic","paper source");
//                 case QPrinter::Cassette:return tr("Cassette","paper source");
//                 case QPrinter::Envelope:return tr("Envelope","paper source");
//                 case QPrinter::EnvelopeManual:return tr("Manual Envelope","paper source");
//                 case QPrinter::FormSource:return tr("Forms","paper source");
//                 case QPrinter::LargeCapacity:return tr("Large Capacity","paper source");
//                 case QPrinter::LargeFormat:return tr("Large Format","paper source");
//                 case QPrinter::Lower:return tr("Lower Tray","paper source");
//                 case QPrinter::MaxPageSource:return tr("Maximum Page Source","paper source");
//                 case QPrinter::Middle:return tr("Middle Tray","paper source");
//                 case QPrinter::Manual:return tr("Manual Feed","paper source");
//                 case QPrinter::OnlyOne:return tr("Only One Source","paper source");
//                 case QPrinter::Tractor:return tr("Tractor","paper source");
//                 case QPrinter::SmallFormat:return tr("Small Format","paper source");
//                 default:return tr("Unknown","paper source");
//         }
// }

void MLabelPrintDialog::printerChanged(int idx)
{
        //get printer
        const QString &pname=mprnselect->itemText(idx);
        if(pname.isEmpty())return;
        //set location and description
        const QPrinterInfo info=QPrinterInfo::printerInfo(pname);
        if(info.isNull())return;
        mmakemodel->setText(info.makeAndModel());
        mlocation->setText(info.location());
        mdescript->setText(info.description());
        //set resolution
        QPrinter prn(info);
        mresolution->clear();
        const int cur=mresolution->itemData(mresolution->currentIndex()).toInt();
        int curidx=-1;
        const int def=prn.resolution();
        int defidx=-1;
        for(auto r:prn.supportedResolutions()){
                if(r==cur)curidx=mresolution->count();
                if(r==def)defidx=mresolution->count();
                mresolution->addItem(QString("%1 dpi").arg(r),r);
        }
        if(curidx>=0)defidx=curidx;
        if(defidx>=0)mresolution->setCurrentIndex(defidx);
}

void MLabelPrintDialog::paperChanged(int idx)
{
        const int ptype=mpaper->itemData(idx).toInt();
        for(const auto&paper:paperTypes){
                if(paper==ptype){
                        //set paper size
                        if(ptype == QPrinter::Custom){
                                msizex->setValue(mprinter->pageSizeMM().width()/moldunit);
                                msizey->setValue(mprinter->pageSizeMM().height()/moldunit);
                        }else{
                                msizex->setValue(paper.psize.width()/moldunit);
                                msizey->setValue(paper.psize.height()/moldunit);
                        }
                        break;
                }
        }
        msizex->setEnabled(ptype==QPrinter::Custom);
        msizey->setEnabled(ptype==QPrinter::Custom);
}

void MLabelPrintDialog::unitChanged(int idx)
{
        const double fac=munit->itemData(idx).toDouble();
        //recalculate everything
        msizex->setValue(msizex->value()*moldunit/fac);
        msizey->setValue(msizey->value()*moldunit/fac);
        mmargbottom->setValue(mmargbottom->value()*moldunit/fac);
        mmargleft->setValue(mmargleft->value()*moldunit/fac);
        mmargright->setValue(mmargright->value()*moldunit/fac);
        mmargtop->setValue(mmargtop->value()*moldunit/fac);
        //remember
        moldunit=fac;
}

void MLabelPrintDialog::okBtnPressed()
{
        //save data to printer
        mprinter->setPrinterName(mprnselect->itemText(mprnselect->currentIndex()));
        mprinter->setPaperSize(QPrinter::PaperSize(mpaper->itemData(mpaper->currentIndex()).toInt()));
        mprinter->setPageSizeMM(QSizeF(msizex->value()*moldunit,msizey->value()*moldunit));
        //mprinter->setPageMargins(mmargleft->value()*moldunit, mmargtop->value()*moldunit, mmargright->value()*moldunit, mmargbottom->value()*moldunit, QPrinter::Millimeter);
    mprinter->setFullPage(mfullpage->isChecked());
        mprinter->setPageMargins(QMarginsF(mmargleft->value()*moldunit, mmargtop->value()*moldunit, mmargright->value()*moldunit, mmargbottom->value()*moldunit), QPageLayout::Millimeter);
        mprinter->setResolution(mresolution->itemData(mresolution->currentIndex()).toInt());

        //close the dialog
        accept();
}
