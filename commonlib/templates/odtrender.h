//
// C++ Interface: odtrender
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ODTRENDER_H
#define MAGICSMOKE_ODTRENDER_H

#include <QObject>
#include <QString>

#include "templates.h"
#include "commonexport.h"

class MOdtRendererPrivate;
class QFile;

MAGICSMOKE_COMMON_EXPORT extern const QString OdfTemplateNS;
MAGICSMOKE_COMMON_EXPORT extern const QString OdfTemplatePrefix;

/**abstract base class for all ODT rendering classes*/
class MAGICSMOKE_COMMON_EXPORT MOdtRenderer
{
	public:
		/**instantiates a renderer loaded from template file*/
		MOdtRenderer(MTemplate file);
		/**deletes the renderer*/
		virtual ~MOdtRenderer();
		
		/**starts the internal rendering routine and outputs to file*/
		virtual void renderToFile(QString file);
		
		/**starts the internal rendering routine and outputs to file*/
		virtual void renderToFile(QFile& file,bool preventOpen=false);
		
		/**starts the internal rendering routine and outputs to printer (calls OpenOffice to print)*/
		virtual void renderToPrinter();

		///like renderToPrinter, but renders to PDF, returns the PDF file name
		virtual QString renderToPdf();
		
		///helper routine: converts a V1 template to V2
		///you have to make sure to not do a double conversion on an already converted document
		static QByteArray convertV1toV2(const QByteArray&);
		
		///format of the file represented by this renderer
		enum class FileFormat{
			///no file loaded or loading was not successful
			None,
			///file was a valid v2 format
			Version2,
			///file was probably v1 or plain ODF, it has been converted
			ConvertedV1,
		};
		///returns the format of the file
		FileFormat fileFormat()const;
		
	protected:
		friend class MOdtRendererPrivate;
		friend class MOdfEngine;
		/**implement this to return the value of a variable during rendering; should return empty string if the variable does not exist*/
		virtual QVariant getVariable(QString varname)=0;
		
		/**implement this to return the amount of iterations for a defined loop; should return 0 if the loop does not exist*/
		virtual int getLoopIterations(QString loopname)=0;
		
		/**implement this to populate the variables used in a specific iteration of the loop*/
		virtual void setLoopIteration(QString loopname, int interation)=0;
	private:
		MOdtRendererPrivate*d;
};

/**generic class that implements MOdtRenderer by calling signals; the signals must be connected using a direct or blocking connection; if any of them is not used it will behave as if the variable/loop did not exist*/
class MAGICSMOKE_COMMON_EXPORT MOdtSignalRenderer:public QObject,public MOdtRenderer
{
	Q_OBJECT
	public:
		/**instantiates a renderer loaded from template file*/
		MOdtSignalRenderer(MTemplate file);
		/**deletes the renderer*/
		~MOdtSignalRenderer();
	
	signals:
		/**connect this to return the value of a variable during rendering; should return empty string if the variable does not exist*/
		void getVariable(QString varname,QVariant&value);
		
		/**connect this to return the amount of iterations for a defined loop; should return 0 if the loop does not exist*/
		void getLoopIterations(QString loopname,int&iterations);
		
		void setLoopIteration(QString loopname, int interation);
	
	protected:
		QVariant getVariable(QString varname);
		int getLoopIterations(QString loopname);
};

#endif
