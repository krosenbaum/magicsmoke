//
// C++ Interface: odtrender
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TICKETRENDER_H
#define MAGICSMOKE_TICKETRENDER_H

#include <QString>
#include <QPointF>
#include <QSizeF>

#include "templates.h"

class MLabelRendererPrivate;
class MOTicket;
class MOVoucher;
class QPaintDevice;
class QPainter;

#include "commonexport.h"

/**base class that describes labels*/
class MAGICSMOKE_COMMON_EXPORT MLabel
{
	public:
		/**constructs the label*/
		MLabel();
		/**deconstructs the label*/
		virtual ~MLabel();
		
		/**abstract: overwrite this to return data for a label*/
		virtual QString getVariable(QString)const=0;
};

/**base class for all label rendering classes*/
class MAGICSMOKE_COMMON_EXPORT MLabelRenderer
{
	public:
		/**instantiates a renderer loaded from template file*/
		MLabelRenderer(MTemplate file);
		/**deletes the renderer*/
		virtual ~MLabelRenderer();
		
		/**renders the ticket; returns whether the rendering was successful*/
		virtual bool render(const MLabel&label,QPaintDevice&pdev,QPainter*painter=0,QPointF offset=QPointF(),bool usePixmap=false);
		
		/**returns the size of the ticket in the coordinates of the specified paint device (used for preview)*/
		virtual QSizeF labelSize(const QPaintDevice&);

		///returns the size of the label in Millimeters
		virtual QSizeF labelSizeMM();
		
	protected:
		friend class MLabelRendererPrivate;
		
	private:
		MLabelRendererPrivate*d;
};

/**convenience class: renders vouchers directly*/
class MAGICSMOKE_COMMON_EXPORT MVoucherRenderer:public MLabelRenderer
{
	public:
		MVoucherRenderer(MTemplate f);
		bool render(const MOVoucher&label,QPaintDevice&pdev,QPainter*painter=0,QPointF offset=QPointF(),bool usePixmap=false);
};

/**convenience class: renders vouchers directly*/
class MAGICSMOKE_COMMON_EXPORT MTicketRenderer:public MLabelRenderer
{
	public:
		MTicketRenderer(MTemplate f);
		bool render(const MOTicket&label,QPaintDevice&pdev,QPainter*painter=0,QPointF offset=QPointF(),bool usePixmap=false);
};


#endif
