//
// C++ Implementation: odtrender
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "ticketrender.h"

#include <QBuffer>
#include <QCryptographicHash>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFontDatabase>
#include <QImage>
#include <QList>
#include <QMap>
#include <QPaintDevice>
#include <QPainter>
#include <QSettings>
#include <QStringList>

#include <Unzip>
#include <Zip>
#include "code39.h"

#include "MOTicket"
#include "MOVoucher"


class MLabelRendererPrivate
{
	protected:
		friend class MLabelRenderer;
		
		MLabelRendererPrivate(QString file,MLabelRenderer*p);
		~MLabelRendererPrivate();
		
		//part of the constructor
		void prepare(Unzip&);
		
		//called by MLabelRenderer
		bool render(const MLabel&,QPaintDevice&,QPainter*,QPointF);
		
		//called by MLabelRenderer
		QSizeF labelSize(const QPaintDevice&);
		QSizeF labelSizeMM();
		
	private:
		MLabelRenderer*parent;
		//internal font-IDs
		QList<int> fdb;
		QMap<QString,QStringList>fdbl;
		QMap<QString,QString>getfontdb;
		//image cache:
		QMap<QString,QImage> idb;
		//XML template cache (template.xml)
		QDomDocument txml;
		//contains the unit of measurement
		QString unit;
		//contains the size of the Label in "unit"
		QSizeF tsize;
		//after constructor: contains whether this object is correctly initialized
		bool canrender;
		
		//does the actual rendering work
		void render(QDomElement&,const MLabel&,QPaintDevice&,QPainter*,QPointF);
		
		//renders a single line
		QString renderLine(const MLabel&,QString);
		
		//parses element to extract offset
		QPointF getoffset(QDomElement&,bool);
		
		//parses element to extract size
		QSizeF getsize(QDomElement&,bool);
		
		//converts a point to "natural" coordinates
		QPointF tonatural(const QPaintDevice&,QPointF);
		
		//converts a size to "natural" coordinates
		QSizeF tonatural(const QPaintDevice&,QSizeF);

		//converts a size to "natural" coordinates
		QSizeF tonatural(int dpi,QSizeF);
		
		//returns a system font name
		QString getfont(QString);
};

MLabelRenderer::MLabelRenderer(MTemplate file)
{
	d=new MLabelRendererPrivate(file.cacheFileName(),this);
}

MLabelRendererPrivate::MLabelRendererPrivate(QString file,MLabelRenderer*p)
{
	parent=p;
	canrender=false;
	//open ZIP
	Unzip temp;
	QFile tfile(file);
	if(tfile.open(QIODevice::ReadOnly))temp.open(&tfile);
	else return;
	prepare(temp);
	temp.close();
	tfile.close();
	qDebug("Label Renderer initialized: %s",canrender?"ready!":"error, can't render.");
}

void MLabelRendererPrivate::prepare(Unzip&temp)
{
	//make sure this is a valid ZIP file
	if(!temp.locateFile("template.xml")){
		qDebug("Label renderer: can't find template.xml");
		canrender=false;
		return;
	}
	//load XML
	QBuffer buffer;
	buffer.open(QBuffer::ReadWrite);
	temp.currentFile(buffer);
	if(!txml.setContent(buffer.data())){
		qDebug("Label renderer: can't parse template.xml - XML fault.");
		canrender=false;
		return;
	}
	//assume we can render now
	canrender=true;
	//parse document element
	QDomElement doc=txml.documentElement();
	unit=doc.attribute("unit","mm");
	if(unit!="mm"&&unit!="in"){
		qDebug("Label renderer: illegal unit in template.xml.");
		canrender=false;
		return;
	}
	tsize=getsize(doc,true);
	if(!canrender)return;
	//check for fonts
	QDomNodeList nl=txml.elementsByTagName("LoadFont");
	QStringList fndb;
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString fn=el.attribute("file");
		if(fn==""){
			qDebug("Label renderer warning: Font element without file attribute. Line %i Column %i.",el.lineNumber(),el.columnNumber());
			continue;
		}
		if(fndb.contains(fn)){
			qDebug("Label renderer warning: Font file %s was loaded more than once. Line %i Column %i.",fn.toLatin1().data(),el.lineNumber(),el.columnNumber());
			continue;
		}
		fndb.append(fn);
		if(!temp.locateFile(fn)){
			qDebug("Label renderer warning: Font element references unknown file. Line %i Column %i.",el.lineNumber(),el.columnNumber());
			continue;
		}
		QBuffer buffer;
		buffer.open(QBuffer::ReadWrite);
		temp.currentFile(buffer);
		QByteArray fdata=buffer.data();
		int fid=QFontDatabase::addApplicationFontFromData(fdata);
		if(fid<0){
			qDebug("Label renderer warning: Font could not be loaded: %s (size: %i, md5: %s)",fn.toLatin1().data(),fdata.size(),QCryptographicHash::hash(fdata,QCryptographicHash::Md5).toHex().data());
			continue;
		}
		fdb.append(fid);
		fdbl.insert(fn,QFontDatabase::applicationFontFamilies(fid));
	}
	//check for pictures
	nl=txml.elementsByTagName("Picture");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString fn=el.attribute("file");
		if(fn==""){
			qDebug("Label renderer error: Picture element without file attribute. Line %i Column %i.",el.lineNumber(),el.columnNumber());
			//failed pictures are fatal
			canrender=false;
			return;
		}
		if(idb.contains(fn))continue;
		if(!temp.locateFile(fn)){
			qDebug("Label renderer error: Picture element references unknown file. Line %i Column %i.",el.lineNumber(),el.columnNumber());
			//failed pictures are fatal
			canrender=false;
			return;
		}
		QBuffer buffer;
		buffer.open(QBuffer::ReadWrite);
		temp.currentFile(buffer);
		QImage img;
		if(!img.loadFromData(buffer.data())){
			qDebug("Label renderer error: Picture file %s could not be interpreted.",fn.toLatin1().data());
			//failed pictures are fatal
			canrender=false;
			return;
		}
		idb.insert(fn,img);
	}
}

MLabelRenderer::~MLabelRenderer()
{
	delete d;
	d=0;
}
MLabelRendererPrivate::~MLabelRendererPrivate()
{
	//delete fonts
	for(int i=0;i<fdb.size();i++)
		QFontDatabase::removeApplicationFont(fdb[i]);
}

bool MLabelRenderer::render(const MLabel&label,QPaintDevice&pdev,QPainter*painter,QPointF offset,bool usePixmap)
{
	bool success=false;
	if(usePixmap){
		// create pixmap (assume 600 DPI)
		QSizeF sz=d->tonatural(600,d->tsize);
		QImage map(sz.toSize(),QImage::Format_RGB32);
		map.setDotsPerMeterX(23622);
		map.setDotsPerMeterY(23622);
		map.fill(Qt::white);
		// render to pixmap
		success=d->render(label,map,nullptr,QPointF());
		// render to pdev
		if(success){
			QPainter*paint=painter;
			if(!painter)paint=new QPainter(&pdev);
			QSizeF nsize=d->tonatural(pdev,d->tsize);
			paint->setClipRect(QRectF(offset,nsize));
			paint->drawImage(QRectF(offset,nsize).toRect(),map,QRectF(QPointF(),map.size()));
			if(!painter)delete paint;
		}
	}else
		success=d->render(label,pdev,painter,offset);
	return success;
}

bool MLabelRendererPrivate::render(const MLabel&label,QPaintDevice&pdev,QPainter*painter,QPointF offset)
{
	//sanity check
	if(!canrender){
		qDebug("Label Renderer: render called, but can't render.");
		return false;
	}
	//actually render
	QDomElement el=txml.documentElement();
	render(el,label,pdev,painter,offset);
	//return result (canrender may be changed by renderer
	return canrender;
}

QPointF MLabelRendererPrivate::getoffset(QDomElement&el,bool isfatal)
{
	QStringList off=el.attribute("offset").split(" ");
	if(off.size()!=2){
		if(isfatal){
			qDebug("Label renderer error: Illegal offset in %s at line %i column %i.",el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QPointF();
	}
	QPointF ret;
	bool b;
	ret.setX(off[0].toDouble(&b));
	if(!b){
		if(isfatal){
			qDebug("Label renderer error: Illegal offset in %s at line %i column %i.",el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QPointF();
	}
	ret.setY(off[1].toDouble(&b));
	if(!b){
		if(isfatal){
			qDebug("Label renderer error: Illegal offset in %s at line %i column %i.",el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QPointF();
	}
	return ret;
}

QSizeF MLabelRendererPrivate::getsize(QDomElement&el,bool isfatal)
{
	QStringList lst=el.attribute("size").split(" ");
	if(lst.size()!=2){
		if(isfatal){
			qDebug("Label renderer error: Illegal size (%i items) in %s at line %i column %i.",lst.size(),el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QSizeF();
	}
	QSizeF ret;
	bool b;
	ret.setWidth(lst[0].toDouble(&b));
	if(!b){
		if(isfatal){
			qDebug("Label renderer error: Illegal size (invalid width) in %s at line %i column %i.",el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QSizeF();
	}
	ret.setHeight(lst[1].toDouble(&b));
	if(!b){
		if(isfatal){
			qDebug("Label renderer error: Illegal size (invalid height) in %s at line %i column %i.",el.tagName().toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
		}
		return QSizeF();
	}
	return ret;
}

QPointF MLabelRendererPrivate::tonatural(const QPaintDevice&dev,QPointF p)
{
	double fac;
	if(unit=="mm")fac=25.4;
	else fac=1.0;
	p.setX(p.x()*dev.logicalDpiX()/fac);
	p.setY(p.y()*dev.logicalDpiY()/fac);
	return p;
}

QSizeF MLabelRendererPrivate::tonatural(const QPaintDevice&dev,QSizeF s)
{
	double fac;
	if(unit=="mm")fac=25.4;
	else fac=1.0;
	s.setWidth(s.width()*dev.logicalDpiX()/fac);
	s.setHeight(s.height()*dev.logicalDpiY()/fac);
	return s;
}

QSizeF MLabelRendererPrivate::tonatural(int dpi,QSizeF s)
{
	double fac;
	if(unit=="mm")fac=25.4;
	else fac=1.0;
	s.setWidth(s.width()*dpi/fac);
	s.setHeight(s.height()*dpi/fac);
	return s;
}

void MLabelRendererPrivate::render(QDomElement&sup, const MLabel&tick, QPaintDevice&pdev, QPainter*painter, QPointF noff)
{
	//initialize painter
	QPainter *paint;
	if(painter)
		paint=painter;
	else
		paint=new QPainter(&pdev);
	QSizeF nsize=tonatural(pdev,tsize);
	paint->setClipRect(QRectF(noff,nsize));
	//parse file
	QDomNodeList nl=sup.childNodes();
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		if(el.isNull())continue;
		QString enm=el.tagName();
		if(enm=="LoadFont"){
			//ignore, already done in constructor
		}else
		if(enm=="Picture"){
			//get attributes
			QPointF off=getoffset(el,true);
			if(!canrender){
				return;
			}
			QSizeF sz=getsize(el,true);
			if(!canrender){
				return;
			}
			//make use of smooth
			int smooth=el.attribute("smooth","1").toInt();
			QString fname=el.attribute("file");
			//paint
			QImage img=idb[fname].scaled(tonatural(pdev,sz).toSize(), Qt::IgnoreAspectRatio, smooth!=0?Qt::SmoothTransformation:Qt::FastTransformation);
			paint->drawImage(QRectF(tonatural(pdev,off)+noff,tonatural(pdev,sz)),img);
		}else
		if(enm=="Text"){
			//get attributes
			QPointF off=getoffset(el,true);
			if(!canrender){
				return;
			}
			QSizeF sz=getsize(el,true);
			if(!canrender){
				return;
			}
			//get text
			QString text=renderLine(tick,el.text().trimmed());
			//get font; TODO: introduce weight and italic settings
			QString font=getfont(el.attribute("font","%"));
			int size=el.attribute("fontsize","10").toInt();
			if(size<1)size=10;
			paint->setFont(QFont(font,size));
			//get alignment
			int flag=0;
			QString al=el.attribute("align","left");
			if(al=="left")flag=Qt::AlignLeft;else
			if(al=="right")flag=Qt::AlignRight;
			else flag=Qt::AlignHCenter;
			al=el.attribute("valign","top");
			if(al=="top")flag|=Qt::AlignTop;else
			if(al=="bottom")flag|=Qt::AlignBottom;
			else flag|=Qt::AlignVCenter;
			//render
			paint->drawText(QRectF(tonatural(pdev,off)+noff,tonatural(pdev,sz)),flag,text);
			qDebug("test %s",text.toLatin1().data());
		}else
		if(enm=="Barcode"){
			//get attributes
			QPointF off=getoffset(el,true);
			if(!canrender){
				return;
			}
			QSizeF sz=getsize(el,true);
			if(!canrender){
				return;
			}
			QString cd=tick.getVariable("BARCODE");
			//paint
			//TODO: find a way to switch off antialiasing
			QRectF rect(tonatural(pdev,off)+noff,tonatural(pdev,sz));
			paint->drawImage(rect,code39(cd));
		}else{
			qDebug("Label renderer error: unknown element %s in label template at line %i column %i.",enm.toLatin1().data(),el.lineNumber(),el.columnNumber());
			canrender=false;
			return;
		}
	}
	if(!painter)
		delete paint;
}

QString MLabelRendererPrivate::getfont(QString fnt)
{
	QString dfont=QSettings().value("defaultfont","Helvetica").toString();
	//did we already answer this?
	if(getfontdb.contains(fnt))return getfontdb[fnt];
	if(fnt=="%")return dfont;
	//is it a file name?
	if(fnt.size())if(fnt[0]=='@'){
		QString font;
		QString fnt2=fnt.mid(1);
		if(fdbl.contains(fnt2))if(fdbl[fnt2].size())
			font=fdbl[fnt2][0];
		if(font.isEmpty()){
			qDebug("Label renderer warning: font file %s did not contain font families, using %s instead.",fnt.toLatin1().data(),dfont.toLatin1().data());
			font=dfont;
		}
		getfontdb.insert(fnt,font);
	}
	if(!getfontdb.contains(fnt)){
		QStringList fonts=QFontDatabase().families();
		if(!fonts.contains(fnt)){
			qDebug("Label renderer warning: font %s not found, searching a close match...",fnt.toLatin1().data());
			QString font;
			for(int i=0;i<fonts.size();i++){
				if(i)font+=", ";
				font+=fonts[i];
			}
			qDebug("Label renderer hint - found the following fonts: %s",font.toLatin1().data());
			font=dfont;
			for(int i=0;i<fonts.size();i++)
				if(fonts[i].startsWith(fnt)){
					font=fonts[i];
					break;
				}
			qDebug("Label renderer hint: replacing font %s with %s",fnt.toLatin1().data(),font.toLatin1().data());
			getfontdb.insert(fnt,font);
		}else
			getfontdb.insert(fnt,fnt);
	}
	return getfontdb[fnt];
}

QString MLabelRendererPrivate::renderLine(const MLabel&tick,QString line)
{
	QString ret,vname;
	bool isvar=false;
	static QString vc="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//scan characters
	for(int i=0;i<line.size();i++){
		//are we currently inside a variable name?
		if(isvar){
			//end of var?
			if(line[i]=='@'){
				if(vname=="")ret+="@";
				else{
					//this is a variable, get value
					ret+=tick.getVariable(vname);
				}
				//reset mode
				isvar=false;
				vname="";
			}else
			//continuation of var?
			if(vc.contains(line[i])){
				//valid var-name-letter, add
				vname+=line[i];
			}else{//not a valid var-name-letter
				//reset
				isvar=false;
				ret+="@";
				ret+=vname;
				vname="";
			}
		}else{//not inside variable name
			//is this the start of a var?
			if(line[i]=='@'){
				isvar=true;
				vname="";
			}else{
				ret+=line[i];
			}
		}
	}
	//anything left over?
	if(isvar){
		//reset
		ret+="@"+vname;
	}
	//return transformed line
	return ret;
}

QSizeF MLabelRenderer::labelSize(const QPaintDevice&dev)
{
	return d->labelSize(dev);
}

QSizeF MLabelRendererPrivate::labelSize(const QPaintDevice&dev)
{
	return tonatural(dev,tsize);
}

QSizeF MLabelRenderer::labelSizeMM()
{
	return d->labelSizeMM();
}

QSizeF MLabelRendererPrivate::labelSizeMM()
{
	qreal fac;
	if(unit=="mm")fac=1.0;else fac=25.4;
	return QSizeF(tsize.width()*fac, tsize.height()*fac);
}


MLabel::MLabel(){}
MLabel::~MLabel(){}

class MTicketLabel:public MLabel
{
	public:
		MTicketLabel(const MOTicket&t):tick(t){}
		QString getVariable(QString var)const;
	private:
		MOTicket tick;
};


//WARNING: if you add anything here, make sure you simulate it in ticketedit.cpp
QString MTicketLabel::getVariable(QString var)const
{
	if(var=="TICKETID"||var=="BARCODE")return tick.ticketid();
	if(var=="PRICE")return tick.priceString();
	if(var=="DATETIME")return tick.event().startTimeString();
	if(var=="ROOM")return tick.event().room();
	if(var=="TITLE")return tick.event().title();
	if(var=="ARTIST")return tick.event().artist().value().name();
	if(var=="PRICECATEGORY")return tick.priceCategoryName();
	if(var=="PRICECATEGORYABBR")return tick.priceCategoryShort();
	return "";
}

MTicketRenderer::MTicketRenderer(MTemplate f):MLabelRenderer(f){}
bool MTicketRenderer::render(const MOTicket&label,QPaintDevice&pdev,QPainter*painter,QPointF offset,bool usePixmap)
{
	return MLabelRenderer::render(MTicketLabel(label),pdev,painter,offset,usePixmap);
}

class MVoucherLabel:public MLabel
{
	public:
		MVoucherLabel(const MOVoucher&t):vouc(t){}
		QString getVariable(QString var)const;
	private:
		MOVoucher vouc;
};

//WARNING: if you add anything here, make sure you simulate it in ticketedit.cpp
QString MVoucherLabel::getVariable(QString var)const
{
	if(var=="VOUCHERID"||var=="BARCODE")return vouc.voucherid();
	if(var=="PRICE")return vouc.priceString();
	if(var=="VALUE")return vouc.valueString();
	if(var=="VALIDDATE")return vouc.validDate();
	return "";
}

MVoucherRenderer::MVoucherRenderer(MTemplate f):MLabelRenderer(f){}
bool MVoucherRenderer::render(const MOVoucher&label,QPaintDevice&pdev,QPainter*painter,QPointF offset,bool usePixmap)
{
	return MLabelRenderer::render(MVoucherLabel(label),pdev,painter,offset,usePixmap);
}
