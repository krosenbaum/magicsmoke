//
// C++ Implementation: odtrender
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "misc.h"
#include "odtrender.h"
#include "office.h"

#include <QBuffer>
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QProcess>
#include <QSettings>
#include <QTemporaryFile>

#include <Unzip>
#include <Zip>

#include "formula.h"
using namespace ELAM;

class MOdfEngine:public MElamEngine
{
	MOdtRenderer*parent;
	public:
		MOdfEngine(MOdtRenderer* pa):parent(pa)
		{
			CharacterClassSettings cs=characterClasses();
			cs.setAssignmentChars('\0','=');
			cs.setOperatorClass(cs.operatorClass().remove(':'));
			QPair<QString,QString>nc=cs.nameClass();
			nc.second+=':';
			cs.setNameClass(nc);
		}
		bool hasConstant(QString v)const
		{
			if(parent->getVariable(v).isValid())
				return true;
			else
				return MElamEngine::hasConstant(v);
		}
		QVariant getConstant(QString v)const
		{
			QVariant vv=parent->getVariable(v);
			if(vv.isValid())
				return vv;
			else
				return MElamEngine::getConstant(v);
		}
};

class MOdtRendererPrivate
{
	protected:
		friend class MOdtRenderer;

		//methods that the parent uses
		MOdtRendererPrivate(QString file,MOdtRenderer*p);
		~MOdtRendererPrivate();

		bool renderToFile(QFile&);

		//data the parent uses
		QString extension;
		MOdtRenderer::FileFormat fileformat;

	private:
		//methods that the parent does not call

		///overall rendering: copies the templates content, calls render()
		void render(QIODevice*);

		///content.xml rendering
		QList<QDomNode> render(QDomNode node);

		///scan string and replace variable values
		QString renderString(QString);

		///gets a scalar variable
		QString getVariable(QString varname);

		//data the parent does not access
		//parent
		MOdtRenderer*parent;
		//the template file
		Unzip temp;
		QFile tfile;
		//xml representation of the content.xml file in the template
		QDomDocument cdoc;

		//local variables and calculation engine
		Engine *calc;
};

MOdtRenderer::MOdtRenderer(MTemplate file)
{
	d=new MOdtRendererPrivate(file.cacheFileName(),this);
	d->extension=file.targetExtension();
	d->calc=new MOdfEngine(this);
}

const QString OdfTemplateNS("http://smoke.silmor.de/odftemplate/namespace");
const QString OdfTemplatePrefix("msmoketpl");
MOdtRendererPrivate::MOdtRendererPrivate(QString file,MOdtRenderer*p)
	:fileformat(MOdtRenderer::FileFormat::None),tfile(file)
{
	parent=p;
	//open ZIP
	if(tfile.open(QIODevice::ReadOnly))temp.open(&tfile);
	else {
		qDebug()<<"Error opening ODF template file"<<file;
		return;
	}
	//TODO: make sure this is a valid ZIP file, preferably with some ODT content
	//check what version it is, convert if necessary
	if(!temp.locateFile("content.xml")){
		qDebug()<<file<<"is not an ODF template. aborting.";
		return;
	}
	//get content
	QBuffer buffer;
	buffer.open(QBuffer::ReadWrite);
	temp.currentFile(buffer);
	QString err;int errln=-1,errcl=-1;
	bool dov1=false;
	const QString tpename=OdfTemplatePrefix+":template";
	buffer.seek(0);
	if(!cdoc.setContent(&buffer,false,&err,&errln,&errcl)){
		qDebug()<<"Hmm, not XML, trying version 1 converter...";
		qDebug()<<" Info: line ="<<errln<<"column ="<<errcl<<"error ="<<err;
		dov1=true;
	}else{
		//check for template element
		QDomElement de=cdoc.documentElement();
		if(de.isNull()||de.tagName()!=tpename){
			qDebug()<<"Template is not v2, trying to convert...";
			dov1=true;
			cdoc.clear();
		}
	}
	//conversion process
	if(!dov1){
		fileformat=MOdtRenderer::FileFormat::Version2;
		return;
	}
	//try again
	if(!cdoc.setContent(MOdtRenderer::convertV1toV2(buffer.data()),false,&err,&errln,&errcl)){
		qDebug()<<"Hmm, definitely not XML - even after conversion, aborting...";
		qDebug()<<" Info: line ="<<errln<<"column ="<<errcl<<"error ="<<err;
		return;
	}else{
		fileformat=MOdtRenderer::FileFormat::ConvertedV1;
		qDebug()<<"Successfully converted the template.";
	}
}

MOdtRenderer::~MOdtRenderer()
{
	if(d==nullptr)return;
	if(d->calc!=nullptr)delete d->calc;
	d->calc=nullptr;
	delete d;
	d=nullptr;
}
MOdtRendererPrivate::~MOdtRendererPrivate()
{
	temp.close();
	tfile.close();
}

MOdtRenderer::FileFormat MOdtRenderer::fileFormat()const
{
	return d->fileformat;
}

void MOdtRenderer::renderToFile(QString file)
{
	QFile f(file);
	if(f.open(QIODevice::ReadWrite)){
		renderToFile(f);
		f.close();
	}
}

void MOdtRenderer::renderToFile(QFile &file,bool preventopen)
{
	if(d->renderToFile(file))
		if(!preventopen && QSettings().value("doOpenODFs",false).toBool())
			openOfficeFile(file.fileName());
}
bool MOdtRendererPrivate::renderToFile(QFile &file)
{
	if(!file.isWritable())
		if(!file.open(QIODevice::ReadWrite))return false;
	file.seek(0);
	file.resize(0);
	render(&file);
	return true;
}

void MOdtRenderer::renderToPrinter()
{
	//generate temporary file
	QTemporaryFile tfile;
	tfile.setAutoRemove(false);//we don't want it to be auto-deleted on close()
	tfile.setFileTemplate(QDir::tempPath()+"/msmoke_XXXXXX."+d->extension);
	tfile.open();
	QString tname=tfile.fileName();
	//render
	d->renderToFile(tfile);
	tfile.close();
	//call ooffice and print
	printOfficeFile(tname);
	//remove temporary file
	QFile(tname).remove();
}

QString MOdtRenderer::renderToPdf()
{
	//generate temporary file
	QTemporaryFile tfile;
	tfile.setAutoRemove(false);//we don't want it to be auto-deleted on close()
	tfile.setFileTemplate(QDir::tempPath()+"/msmoke_XXXXXX."+d->extension);
	tfile.open();
	QString tname=tfile.fileName();
	//render
	d->renderToFile(tfile);
	tfile.close();
	//call ooffice and print
	QString pdf=convertOfficeFilePdf(tname);
	//remove temporary file
	QFile(tname).remove();

	return pdf;
}

void MOdtRendererPrivate::render(QIODevice*out)
{
	//sanity check
	if(!temp.isOpen()){
		qDebug()<<"ODF Renderer: I don't have a valid template, cannot render.";
		return;
	}
	if(!out->isWritable()){
		qDebug()<<"ODF Renderer Ooops: output device not writeable when trying to render.";
		return;
	}
	//rewind input
	if(!temp.firstFile())return;
	//create output
	Zip ozip;
	ozip.open(out);
	//copy contents
	do{
		ZipFileInfo info= temp.currentFile();
		if(info.fileName()=="content.xml"){
			//render
			QByteArray cont=render(cdoc.cloneNode()).value(0).toDocument().toByteArray();
			//write
			QBuffer buffer;
			buffer.setData(cont);
			buffer.open(QBuffer::ReadWrite);
			buffer.seek(0);
			ozip.storeFile(info.fileName(),buffer,info.createTime());
		}else{
			//copy unchanged
			temp.copyCurrentFile(ozip);
		}
	}while(temp.nextFile());
	ozip.close();
}

static inline QList<QDomNode> n2q(const QDomNodeList&nl){
	QList<QDomNode>ret;
	for(int i=0;i<nl.size();i++)ret<<nl.at(i);
	return ret;
}

QList<QDomNode> MOdtRendererPrivate::render(QDomNode node)
{
	//trivial cases
	if(node.isComment()||node.isDocumentType()||node.isNotation()||node.isProcessingInstruction()){
		qDebug()<<"trivial type"<<(int)node.nodeType()<<node.nodeName();
		return QList<QDomNode>()<<node.cloneNode();
	}
	//buffer for sub-nodes
	QDomNodeList nl=node.childNodes();
	//handle the document itself
	if(node.isDocument()){
		QDomDocument doc=node.cloneNode(false).toDocument();
		foreach(QDomNode nd,n2q(nl))
			foreach(QDomNode rnd,render(nd)){
				if(doc.appendChild(rnd).isNull())
					qDebug()<<"!!!!!!!! Error: document appending node type"<<(int)rnd.nodeType()<<rnd.nodeName()<<(rnd.isNull()?"is null":"not null")<<" while doc"<<(doc.isNull()?"is null":"not null");
			}
		return QList<QDomNode>()<<doc;
	}
	//handle elements - with special routines
	if(node.isElement()){
		QDomElement el=node.toElement();
		//handle special commands
		QStringList tnl=el.tagName().split(":");
		if(tnl.value(0)==OdfTemplatePrefix){
			QList<QDomNode>ret;
			if(tnl.value(1)=="if"){
// 				qDebug()<<"????????it's a bit iffy"<<el.attribute("select");
				QVariant res=calc->evaluate(el.attribute("select"));
				if(!res.canConvert<bool>()){
					qDebug()<<"!!!!!!!! ODF Engine error: if has been called with instructions that do not convert to bool"<<el.attribute("select");
					qDebug()<<"         Skipping this statement altogether.";
					return ret;
				}
				bool istrue=res.toBool();
				foreach(QDomNode nd,n2q(nl)){
					if(!nd.isElement())continue;
					QDomElement el2=nd.toElement();
					if(el2.tagName()==(OdfTemplatePrefix+":else"))istrue=!istrue;
					if(istrue)ret<<render(el2);
				}
			}else if(tnl.value(1)=="else"){
				qDebug()<<"!!!!!!!! Unexpected <else/> tag found, ignoring it.";
			}else if(tnl.value(1)=="calculate"){
// 				qDebug()<<"????????calculatin'"<<el.attribute("exec");
				QString form=el.attribute("exec");
				QVariant res=calc->evaluate(form);
				qDebug()<<"!!!!!!!! ODF Engine Calculation"<<form<<"yields"<<res;
			}else if(tnl.value(1)=="loop"){
				QString lvn=el.attribute("variable").trimmed();
				int max=parent->getLoopIterations(lvn);
				qDebug()<<"!!!!!!!! entering loop"<<lvn<<"with"<<max<<"iterations";
				//iterate
				for(int i=0;i<max;i++){
					parent->setLoopIteration(lvn,i);
					foreach(QDomNode nd,n2q(nl))
						if(nd.isElement())
							ret<<render(nd);
				}
				//end of loop
				qDebug()<<"!!!!!!!! leaving loop"<<lvn;
			}else if(tnl.value(1)=="template"){
// 				qDebug()<<"template found... processing...";
				for(int i=0;i<nl.size();i++){
					if(!nl.at(i).isElement())continue;
					ret<<render(nl.at(i));
				}
			}else
				qDebug()<<"!!!!!!!!! Unknown special tag found, ignoring it:"<<el.tagName();
			return ret;
		}
		//handle all others
		QDomElement ret=el.cloneNode(false).toElement();
// 		qDebug()<<"normal tag"<<el.tagName()<<ret.isNull();
		for(int i=0;i<nl.size();i++)
			foreach(QDomNode nd,render(nl.at(i))){
// 				qDebug()<<"appending to normal tag tyope"<<(int)nd.nodeType()<<nd.nodeName()<<"is null?"<<ret.isNull();
				if(ret.appendChild(nd).isNull())
					qDebug()<<"!!!!!!!! Error appending normal tag"<<nd.nodeName()<<(nd.isNull()?"is null":"not null")<<" to normal tag"<<ret.nodeName()<<(ret.isNull()?"is null":"not null");
			}
		return QList<QDomNode>()<<ret;
	}
	//text
	if(node.isText()||node.isCDATASection()){
		QString line=renderString(node.nodeValue());
		QList<QDomNode>ret;
		if(node.isText())
			ret<<node.ownerDocument().createTextNode(line);
		else if(node.isCDATASection())
			ret<<node.ownerDocument().createCDATASection(line);
		return ret;
	}
	//fallback
	qDebug()<<"!! ODF renderer, hmm why didn't I catch node type"<<(int)node.nodeType()<<"with name"<<node.nodeName();
	return QList<QDomNode>()<<node.cloneNode();
}

QString MOdtRendererPrivate::renderString(QString line)
{
	//scan characters
	bool isvar=false;
	QString vname,ret;
	foreach(QChar c,line){
		//are we currently inside a variable name?
		if(isvar){
			//end of var?
			if(c=='@'){
				if(vname.size()>0){
					QVariant v=calc->evaluate(vname);
					qDebug()<<"!!!!!!!! Calculation"<<vname<<"yields"<<v.toString();
					if(v.canConvert<Exception>()){
						Exception e=v.value<Exception>();
						qDebug()<<"  ?????? Exception detail: at"<<e.errorPos()<<"error"<<e.errorTypeName()<<":"<<e.errorText();
					}
					ret+=v.toString();
				}else
					ret+="@";
				//reset mode
				isvar=false;
				vname="";
			}else
			//continuation of var
			vname+=c;
		}else{//not inside variable name
			//is this the start of a var?
			if(c=='@'){
				isvar=true;
				vname="";
			}else{
				ret+=c;
			}
		}
	}
	//anything left over?
	if(isvar){
		//reset
		ret+="@"+vname;
	}
	//return transformed line
	return ret;
}

QByteArray MOdtRenderer::convertV1toV2(const QByteArray& old)
{
	QByteArray nba;
	QList<QByteArray>olst=old.split('\n');
	bool hstarted=false;
	foreach(QByteArray line,olst){
		//is this the start of the file?
		if(!hstarted){
			QByteArray st=line.trimmed().left(2);
			//find the first real tag and insert the new start tag there
			if(st!="<?" && st!="<!" && st[0]=='<'){
				nba+="<msmoketpl:template xmlns:msmoketpl=\"";
				nba+=OdfTemplateNS.toLatin1();
				nba+="\">\n";
				hstarted=true;
			}
			//add the line just scanned...
			nba+=line;
			nba+='\n';
			continue;
		}
		//is this a special line?
		QByteArray lnt=line.trimmed();
		if(lnt.size()>1 && lnt.at(0)=='#'){
			//extract command
			int cnt=lnt.indexOf(':');
			QByteArray parm;
			if(cnt>0){
				parm=lnt.mid(cnt+1);
				lnt=lnt.left(cnt);
// 				qDebug()<<"found command"<<lnt<<"param"<<parm;
			}
			//check command and replace
			if(lnt=="#IF"){
				nba+="<msmoketpl:if select=\"";
				nba+=xmlize(parm);
				nba+="\">";
			}else if(lnt=="#ELSE")
				nba+="<msmoketpl:else/>";
			else if(lnt=="#ENDIF")
				nba+="</msmoketpl:if>";
			else if(lnt=="#SETNEWLINE"){
				nba+="<!-- Warning: the obsolete #SETNEWLINE command has been used here! Please consider using a loop instead:\n";
				nba+=xmlize(line);
				nba+="\n-->";
			}else if(lnt=="#CALC"){
				nba+="<msmoketpl:calculate exec=\"";
				nba+=xmlize(parm);
				nba+="\"/>";
			}else if(lnt=="#LOOP"){
				nba+="<msmoketpl:loop variable=\"";
				nba+=xmlize(parm);
				nba+="\">";
			}else if(lnt=="#ENDLOOP"){
				nba+="</msmoketpl:loop>";
			}else{
				nba+="<!-- An unknown command was used here:\n";
				nba+=xmlize(line);
				nba+="\n-->";
			}
		}else //no, not special: just store it
			nba+=line;
		//add a newline anyway
		nba+='\n';
	}
	//close the template
	nba+="</msmoketpl:template>";
// 	qDebug()<<"conversion test"<<nba;
	return nba;
}


/********************************************************************/

MOdtSignalRenderer::MOdtSignalRenderer(MTemplate file)
	:MOdtRenderer(file)
{
}

MOdtSignalRenderer::~MOdtSignalRenderer(){}

QVariant MOdtSignalRenderer::getVariable(QString varname)
{
	QVariant ret;
	emit getVariable(varname,ret);
	return ret;
}

int MOdtSignalRenderer::getLoopIterations(QString loopname)
{
	int ret=-1;
	emit getLoopIterations(loopname,ret);
	return ret;
}
