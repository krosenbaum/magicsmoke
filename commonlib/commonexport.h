
#include <QtCore/QtGlobal>

#ifdef MAGICSMOKE_COMMON_LIB_BUILD
#define MAGICSMOKE_COMMON_EXPORT Q_DECL_EXPORT
#else
#define MAGICSMOKE_COMMON_EXPORT Q_DECL_IMPORT
#endif
