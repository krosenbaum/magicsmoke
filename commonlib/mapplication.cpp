//
// C++ Implementation: main
//
// Description: Main Program
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include <QDesktopServices>
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QHostInfo>
#include <QIcon>
#include <QInputDialog>
#include <QLibraryInfo>
#include <QLocale>
#include <QMenu>
#include <QMessageBox>
#include <QProgressDialog>
#include <QSettings>
#include <QSignalMapper>
#include <QStringList>
#include <QTranslator>
#include <QUrl>
#include <QtGlobal>

#include <TimeStamp>
#include <TZFile>

#include "configdialog.h"
#include "debug.h"
#include "barcode-plugin.h"
#include "hmac.h"
#include "keygen.h"
#include "mapplication.h"
#include "msinterface.h"
#include <ELAM/Engine>
#include "misc.h"
#include <Aurora>
#include "boxwrapper.h"

QString MApplication::choseLanguage(bool warn)
{
	QString lang=QSettings().value("lang","--").toString();
	if(lang=="--"){
		lang=QLocale::system().name();
	}
	int cur=0;
	QStringList langs;
	langs<<"C - default";
	QStringList files=QDir(QApplication::applicationDirPath()).entryList(QStringList()<<"smoke_*.qm", QDir::Files, QDir::Name);
	for(int i=0;i<files.size();i++){
		QString l=files[i].mid(6);
		l.chop(3);
		langs<<l;
		if(l==lang)cur=langs.size()-1;
	}
	bool ok;
	QString lc;
	lc=QInputDialog::getItem(0,QApplication::translate("lang","Chose Language"),QApplication::translate("lang","Language:"),langs,cur,false,&ok);
	if(ok){
		lang=lc.split(' ').at(0);
		QSettings().setValue("lang",lang);
		if(warn)
			QMessageBox::information(0,QApplication::translate("lang","Information"), QApplication::translate("lang","The changed language setting will only be active after restarting the application."));
	}
	return lang;
}

class MProgressWrapperGui:public MProgressWrapper
{
        QProgressDialog*pd;
        public:
                MProgressWrapperGui(QString label,QString button)
                {
                        pd=new QProgressDialog(label,button,0,0);
                        pd->setMinimumDuration(500);
                        //pd->show();
                }
                ~MProgressWrapperGui()
                {
                        if(pd)pd->deleteLater();
                        pd=0;
                }
                virtual void setLabelText(QString l)
                {
                        pd->setLabelText(l);
                }
                virtual void setCancelButtonText(QString t)
                {
                        pd->setCancelButtonText(t);
                }
                virtual void cancel()
                {
                        pd->cancel();
                }
                virtual void hide()
		{
			pd->hide();
		}
                virtual void setRange(int mi,int ma)
                {
                        pd->setRange(mi,ma);
                }
                virtual void setValue(int v)
                {
                        pd->setValue(v);
                }
};

#ifndef HOMEPAGE_BASEURL
#define HOMEPAGE_BASEURL "http://smoke.silmor.de"
#endif

#define GIT_DATE_FORMAT "yyyy-MM-dd HH:mm:ss"
#define BUILD_DATE_FORMAT Qt::ISODate

void MApplication::aboutMS()
{
	QMessageBox mb;
	mb.setIconPixmap(QPixmap(":/icon.png"));
	mb.setWindowTitle(tr("About MagicSmoke"));
	MSInterface*ifc=MSInterface::instance();
	mb.setText(tr(	"<h3>MagicSmoke v. %1</h3>"
			"&copy; Konrad Rosenbaum, 2007-%6<br>" //I'm lazy, but assuming I'm still the principal author...
			"&copy; Peter Keller, 2007-2008<br>"
			"protected under the GNU GPL v.3 or at your option any newer<p>"
			"See also the <a href=\"%2\">MagicSmoke Homepage</a>.<p>"
			"This version was compiled at %8<br/>from repository '%3'<br/>revision '%4'<br/>last changed %7.<p>"
			"The current application data base path is '%5'.")
			.arg(MSInterface::staticVersionInfo(WOb::VersionHR)) //%1 - human readable version
			.arg(HOMEPAGE_BASEURL) //%2 - Homepage
			.arg(MSInterface::staticVersionInfo(WOb::VersionRootURL)) //%3 - main (remotes/origin) GIT repo
			.arg(MSInterface::staticVersionInfo(WOb::VersionNumber)) //%4 - rev ID
			.arg(ifc?ifc->dataDir(): dataDir())//%5 - $HOME/.magicSmoke2
			.arg(MSInterface::staticVersionInfo(WOb::VersionTime).left(4))//%6 - year of the last modification
			.arg(MSInterface::staticVersionInfo(WOb::VersionTime)) //%7 - last modification in GIT
			.arg(MSInterface::staticVersionInfo(WOb::VersionGenTime)) //%8 - compile time (of iface.so)
		);
	mb.setStandardButtons(QMessageBox::Ok);
	mb.exec();
}

void MApplication::versionDlg()
{
	QMessageBox mb;
	mb.setIconPixmap(QPixmap(":/icon.png"));
	mb.setWindowTitle(tr("MagicSmoke Version Information"));
	TimeZoneLib::TZFile tzf=TimeStamp::defaultZoneFile();
	auto unknown=[](const QString&s)->QString{if(s.isEmpty())return "(unknown)";else return "'"+s+"'";};
	mb.setText(tr(	"<h3>MagicSmoke Version %3</h3>"
			"<table border='1' style='border-style: solid; border-color: #888;'>"
			"<tr><td>Repository:<td>%1</tr>"
			"<tr><td>Revision:<td>%2</tr>"
			"<tr><td>Modification&nbsp;State:<td>%4</tr>"
			"<tr><td>Commit Time:<td>%5</tr>"
			"<tr><td>Committer:<td>%6</tr>"
			"</table><p>\n"
			"<h3>Libraries</h3>\n<table border='1' style='border-style: solid; border-color: #888;'>"
			"<tr><td>WOC:<td>%7<br/>%8</tr>"
			"<tr><td>PACK&nbsp;Library:<td>%9<br/>%10</tr>"
			"<tr><td>Qt:<td>%11</tr>"
			"<tr><td>ELAM:<td>%12</tr>"
			"<tr><td>Time&nbsp;Zone&nbsp;Default:<td>%13 in directory %14<br/>version %15</tr>"
			"<tr><td>Time&nbsp;Zone&nbsp;Built-In:<td>%16 (library: %17)</tr>"
			"</table>"
			)
			.arg(htmlize(MSInterface::staticVersionInfo(WOb::VersionRootURL))) // %1
			.arg(htmlize(MSInterface::staticVersionInfo(WOb::VersionNumber))) //%2
			.arg(htmlize(MSInterface::staticVersionInfo(WOb::VersionHR))) //%3
			.arg(MSInterface::staticVersionInfo(WOb::VersionLocallyModified)) //%4
			.arg(htmlize(MSInterface::staticVersionInfo(WOb::VersionTime))) //%5
			.arg(htmlize(MSInterface::staticVersionInfo(WOb::VersionAuthor))) //&6
			.arg(htmlize(MSInterface::staticWocVersionInfo(WOb::VersionRootURL))) //%7
			.arg(htmlize(MSInterface::staticWocVersionInfo(WOb::VersionNumber)))
			.arg(htmlize(WInterface::staticLibraryVersionInfo(WOb::VersionRootURL))) //%9
			.arg(htmlize(WInterface::staticLibraryVersionInfo(WOb::VersionNumber)))
			.arg(htmlize(QT_VERSION_STR)) //%11
			.arg(htmlize(ELAM::versionInfo())) //%12
			.arg(unknown(tzf.name())) //%13
			.arg(unknown(tzf.dirName()))
			.arg(unknown(tzf.version()))
			.arg(TimeZoneLib::TZFile::builtinVersion()) //%16
			.arg(TimeZoneLib::TZFile::libraryVersion()) //%17
		);
	mb.setStandardButtons(QMessageBox::Ok);
	mb.exec();
}


struct HelpUrl {
        enum Type {None, Item, Separator} type=None;
        QString label,url;
        HelpUrl(Type t,QString l,QString u=QString()):type(t),label(l),url(u){}
};
static QList<HelpUrl> helpUrl;

static void loadHelpUrlFile(const QString&fname)
{
        QFile fd(fname);
        QList<HelpUrl > urls;
        //try to open
        if(fd.open(QIODevice::ReadOnly)){
                //read config
                QDomDocument doc;
                doc.setContent(&fd);
                QDomNodeList nl=doc.documentElement().childNodes();
                for(int i=0;i<nl.size();i++){
                        QDomElement el=nl.at(i).toElement();
                        if(!el.isElement())continue;
                        if(el.tagName()=="Item")
                                urls.append(HelpUrl(HelpUrl::Item, el.attribute("label").trimmed(), el.text().trimmed()));
                        else if(el.tagName()=="Separator")
                                urls.append(HelpUrl(HelpUrl::Separator, el.attribute("label").trimmed()));
                }
        }
        //append to url list
        if(helpUrl.size()>0 && urls.size()>0)helpUrl.append(HelpUrl(HelpUrl::Separator,""));
        helpUrl.append(urls);
}

void MApplication::initHelpUrl()
{
	//check file exists, if not initialize it
	loadHelpUrlFile(dataDir()+"/helpMenu.xml");
        loadHelpUrlFile(applicationDirPath()+"/helpMenu.xml");
	//fallback
	if(helpUrl.size()<1)
		helpUrl<<HelpUrl(HelpUrl::Item,tr("&Help"),tr("default:/index.html","default help URL, if you translate the index.html file, then change this as well"));
}

QMenu* MApplication::helpMenu()
{
	QMenu*m=new QMenu(tr("&Help"));
	QSignalMapper *map=new QSignalMapper(m);
	connect(map,SIGNAL(mapped(int)),qApp,SLOT(help(int)));
	for(int i=0;i<helpUrl.size();i++){
                if(helpUrl[i].type==HelpUrl::Separator){
                        m->addSeparator()->setText(helpUrl[i].label);
                }else{
                        QAction*a=m->addAction(helpUrl[i].label,map,SLOT(map()));
                        map->setMapping(a,i);
                }
	}
	m->addSeparator();
	m->addAction("About &MagicSmoke",qApp,SLOT(aboutMS()));
	m->addAction("About &Qt",qApp,SLOT(aboutQt()));
	m->addAction("&Version Info",qApp,SLOT(versionDlg()));
	return m;
}

static inline QUrl convHelpUrl(QUrl s)
{
        if(s.scheme()=="default"){
                static const QString ad=QApplication::applicationDirPath()+"/doc/";
                if(QFile(ad+"index.html").exists())
                        return QUrl::fromLocalFile(ad+s.path());
                else
                        //TODO: allow different versions?
                        return HOMEPAGE_BASEURL "/doc/" + s.path();
        }
        else return s;
}



void MApplication::help()
{
        qDebug()<<"Opening default help...";
	QDesktopServices::openUrl(convHelpUrl(QUrl("default:/index.html")));
}

void MApplication::help(int idx)
{
	if(idx<0 || idx>=helpUrl.size()){
		help();
		return;
	}
	QUrl u=convHelpUrl(helpUrl[idx].url);
        qDebug()<<"Opening help:"<<u;
	QDesktopServices::openUrl(u);
}

#define ENV_DATADIR "MAGICSMOKE_DATADIR"
#define ENV_CONFIGDIR "MAGICSMOKE_CONFIGDIR"

MApplication::MApplication(int&ac,char**av)
:QApplication(ac,av)
{
        //locate settings
        setOrganizationName("MagicSmoke");
        setApplicationName("MagicSmoke2");

        //set icon
        setWindowIcon(QIcon(":/icon.png"));

        //install event filter for random generator
        installEventFilter(ef=new EFilter);

        //init GUI wrappers
        MBoxWrapper::setWarning([](QString title,QString text){QMessageBox::warning(nullptr,title,text);});
        MProgressWrapper::setFactory([](QString label,QString button)->MProgressWrapper*{
                return new MProgressWrapperGui(label,button);
        });

        //check environment
	if(qEnvironmentVariableIsSet(ENV_DATADIR))
		setDataDir(QString::fromLocal8Bit(qgetenv(ENV_DATADIR)));
	if(qEnvironmentVariableIsSet(ENV_CONFIGDIR))
		setConfigDir(QString::fromLocal8Bit(qgetenv(ENV_CONFIGDIR)));

        //check parameters
//         qDebug()<<"arguments"<<arguments();
        for(const QString& param:arguments()){
                if(param.startsWith("-confdir=")){
                        setConfigDir(param.mid(9));
                }else if(param.startsWith("-datadir=")){
                        setDataDir(param.mid(9));
                }
        }

        //data directory fallback
        if(dataDir().isEmpty())
                setDataDir("$BASE/.magicSmoke2");
}

void MApplication::setConfigDir(QString d)
{
        QSettings::setDefaultFormat(QSettings::IniFormat);
        const QString cdir=MSInterface::resolveDir(d);
        QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,cdir);
        qDebug()<<"Setting the config directory to"<<cdir<<"from"<<d;
	qputenv(ENV_CONFIGDIR,d.toLocal8Bit());
}

void MApplication::setDataDir(QString d)
{
        MSInterface::setAppDataDir(d);
	qDebug()<<"Setting Data Directory to"<<d;
	qputenv(ENV_DATADIR,d.toLocal8Bit());
}


void MApplication::setMyStyle()
{
	QSettings set;
	if(set.contains("appstyle"))
		setStyle(set.value("appstyle").toString());
	if(set.contains("appstylesheet")){
		QFile fd(set.value("appstylesheet").toString());
		if(fd.open(QIODevice::ReadOnly)){
			setStyleSheet(QString::fromLocal8Bit(fd.readAll()));
			fd.close();
		}
	}
}

static inline void loadTranslation(QTranslator*trans,const QString&lang,const QString&prog)
{
	QStringList lpath;
	lpath <<QApplication::applicationDirPath() <<QLibraryInfo::location(QLibraryInfo::TranslationsPath);
	for(int i=0;i<lpath.size();i++){
		bool ok=trans->load(prog+"_"+lang,lpath[i]);
		QString msg=QString("...loading %1_%2 from %3 ...%4")
			 .arg(prog).arg(lang)
			 .arg(lpath[i])
			 .arg(ok?"ok":"failed");
		qDebug("%s",msg.toLatin1().data());
		if(ok)return;
	}
}

void MApplication::initLanguage()
{
	//try to find appropriate locale
	QString lang=QSettings().value("lang","--").toString();
	if(lang=="--"){
		lang=choseLanguage(false);
	}
	qDebug("Loading language %s",lang.toLatin1().data());
	//load Qt translation
	loadTranslation(qttrans=new QTranslator(this),lang,"qt");
	installTranslator(qttrans);
	//load magicSmoke translation
	loadTranslation(mstrans=new QTranslator(this),lang,"smoke");
	installTranslator(mstrans);
	//load Pack translation
	QTranslator*xtrans;
	loadTranslation(xtrans=new QTranslator(this),lang,"qwbase");
	installTranslator(xtrans);
	//detect and load remaining modules
	QStringList files=QDir(applicationDirPath()).entryList(QStringList()<<"smoke-*.qm",QDir::Files|QDir::Readable,QDir::Name);
	QStringList loaded, modules;
	// --> stage 1: find modules
	for(auto file:files){
		QString m=file.split('_').value(0);
		if(!m.isEmpty() && !modules.contains(m))
			modules.append(m);
	}
	// --> stage 2: go through modules and load
	for(auto mod:modules){
		loadTranslation(xtrans=new QTranslator(this),lang,mod);
		installTranslator(xtrans);
	}
	//defaults
	if(lang!="--"&&lang!=""){
		QLocale::setDefault(QLocale(lang));
#ifndef Q_OS_WIN
		setenv("LANG",lang.toLatin1().data(),1);
	}else{
		setenv("LANG","C",1);
#endif
	}
}

void MApplication::initialize()
{
        //load the correct style
        setMyStyle();
        //initialize log file
        initDebug(dataDir());
        //init localization and profiles
        initLanguage();
        initProfile();
        //init help menu layout
        initHelpUrl();
}

void MApplication::initUpdater()
{
        updater=new Aurora(applicationDirPath()+"/magicsmoke.xml",this);
        connect(updater,SIGNAL(newVersionAvailable(QString)),this,SLOT(updateReadyDownload(QString)));
        connect(updater,SIGNAL(readyForInstallation()),this,SLOT(updaterReadyInstall()));
        connect(updater,SIGNAL(installationFinished()),this,SLOT(updaterCompleted()));
        connect(updater,SIGNAL(downloadFailed()),this,SLOT(updaterFailed()));
        connect(updater,SIGNAL(installationFailed()),this,SLOT(updaterFailed()));
        //we ignore all errors that happen prior to successful verification, since they
        //would create too much noise
}

void MApplication::updateReadyDownload(const QString& v)
{
        qDebug()<<"Asking for Download permission";
        QMessageBox::StandardButton r=QMessageBox::question(0,tr("New Update"),tr("A new version of MagicSmoke is available. Do you want to download the new version %1?").arg(v),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
        if(r==QMessageBox::Yes)
                updater->startDownload();
}

void MApplication::updaterReadyInstall()
{
        QMessageBox::Button r=QMessageBox::question(0,tr("New Update"),tr("A new version of MagicSmoke is ready for installation. Do you want to install?"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
        if(r==QMessageBox::Yes)
                updater->startInstallation();
}

void MApplication::updaterCompleted()
{
        QMessageBox::information(0,tr("Update Complete"),tr("A new version of MagicSmoke has been installed. It will be used the next time you start MagicSmoke."));
}

void MApplication::updaterFailed()
{
        QMessageBox::warning(0,tr("Update Failed"),tr("An update of MagicSmoke failed."));
}

QString MApplication::dataDir()
{
        return MSInterface::appDataDir();
}

void MApplication::initProfile()
{
        //check/generate profile
        if(!QSettings().childGroups().contains("profiles")){
                QMessageBox::warning(0,translate("initprofile","Initial Profile Warning"), translate("initprofile","You need a profile to work with Magic Smoke. Please create one now."));
                MConfigDialog().exec();
        }
}
