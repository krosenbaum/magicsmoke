//
// C++ Interface: hmac
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef SMOKE_HMAC_H
#define SMOKE_HMAC_H

#include <QCryptographicHash>

#include "commonexport.h"

/**Calculate a cryptographic HMAC (used by authentication algorithm)*/
class MAGICSMOKE_COMMON_EXPORT SMHmac
{
	public:
		/**constructs an object that calculates HMACs*/
		SMHmac(QCryptographicHash::Algorithm algo,const QByteArray&key);
		/**adds length bytes of data to the hash-stream*/
		void addData(const char * data, int length );
		/**adds data to the hash-stream*/
		void addData(const QByteArray & data );
		/**reset the hash to the state immediately after construction*/
		void reset();
		/**finalize the hash and return the result*/
		QByteArray result();
		
		/**returns the length of the results of given algorithm in bytes; this should have been implemented by QCryptographicHash! performs a test calculation if the algo is unknown*/
		static int resultWidth(QCryptographicHash::Algorithm);
		/**returns the length of the blocks used internally in the given algorithm in bytes; this should have been implemented by QCryptographicHash! returns -1 if in doubt*/
		static int blockWidth(QCryptographicHash::Algorithm);
		/**complete HMAC calculation in a can*/
		static QByteArray hmac(const QByteArray&data,const QByteArray&key,QCryptographicHash::Algorithm method);
	private:
		//the two key schedules
		QByteArray keyA,keyB;
		//remember where we are
		enum State {Collecting,Finished};
		mutable State state;
		//inner hash function
		QCryptographicHash subhashin,subhashout;
};


#endif
