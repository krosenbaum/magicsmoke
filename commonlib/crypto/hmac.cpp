//
// C++ Implementation: hmac
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "hmac.h"
#include <QByteArray>

SMHmac::SMHmac(QCryptographicHash::Algorithm algo,const QByteArray&key)
	:subhashin(algo),subhashout(algo),state(Collecting)
{
	//calculate key schedules
	QByteArray k2=key;
	int hlen=blockWidth(algo);
	if(k2.size()>hlen){
		//hash the key if it is too long
		k2=QCryptographicHash::hash(k2,algo);
	}else
	if(k2.size()<hlen){
		//pad the key if it is too short
		for(int i=k2.size();i<hlen;i++)
			k2.append((char)0);
	}
	//mix with ipad/opad
	for(int i=0;i<hlen;i++){
		keyA.append(k2[i]^0x5c);
		keyB.append(k2[i]^0x36);
	}
	//init hashes
	subhashin.addData(keyB);
	subhashout.addData(keyA);
}
void SMHmac::addData ( const char * data, int length ){addData(QByteArray(data,length));}
void SMHmac::addData ( const QByteArray & data )
{
	//don't continue if already finished
	if(state!=Collecting)return;
	//add to inner hash
	subhashin.addData(data);
}

void SMHmac::reset ()
{
	//reset hashes
	subhashin.reset();
	subhashout.reset();
	//initialize
	subhashin.addData(keyB);
	subhashout.addData(keyA);
	//reset state
	state=Collecting;
}

QByteArray SMHmac::result()
{
	if(state==Finished){
		return subhashout.result();
	}else{
		state=Finished;
		subhashout.addData(subhashin.result());
		return subhashout.result();
	}
}

//static 
int SMHmac::resultWidth(QCryptographicHash::Algorithm a)
{
	switch(a){
		case QCryptographicHash::Md4:return 128/8;
		case QCryptographicHash::Md5:return 128/8;
		case QCryptographicHash::Sha1:return 160/8;
		//this is rather expensive, but Qt leaves me no choice
		default: return QCryptographicHash::hash(QByteArray("a"),a).size();
	}
}

//static 
int SMHmac::blockWidth(QCryptographicHash::Algorithm a)
{
	switch(a){
		case QCryptographicHash::Md4:
		case QCryptographicHash::Md5:
		case QCryptographicHash::Sha1:return 64;
		//FIXME: this should be in QCryptographicHash, since I can't know all its hashes!
		default: return -1;
	}
}

//static 
QByteArray SMHmac::hmac(const QByteArray&data,const QByteArray&key,QCryptographicHash::Algorithm method)
{
	SMHmac hm(method,key);
	hm.addData(data);
	return hm.result();
}
