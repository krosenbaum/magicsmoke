//
// C++ Interface: keygen
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_KEYGEN_H
#define MAGICSMOKE_KEYGEN_H

#include <QDialog>
#include <QByteArray>

class QLabel;
class QPushButton;

#include "commonexport.h"

class MAGICSMOKE_COMMON_EXPORT MKeyGen:public QDialog
{
	Q_OBJECT
	public:
		MKeyGen(QWidget*parent=0);
		~MKeyGen();
		
		QString getKey(int len=40);
		
	private:
		QLabel*randlab;
		int randstat;
		
		QPushButton *okbtn,*ccbtn;
		
	private slots:
		void updateProps();
};

class MAGICSMOKE_COMMON_EXPORT EFilter:public QObject
{
	Q_OBJECT
	public:
		EFilter();
		~EFilter();
		
		static EFilter*instance();
		
		int entropy();
		QByteArray getRandom(int);
	signals:
		void moreBits();
	protected:
		bool eventFilter(QObject*,QEvent*);
	private:
		QByteArray randbuf,randlast;
		int randent,randctr;
		void addBit(int);
};

//shortcut:
MAGICSMOKE_COMMON_EXPORT QByteArray getRandom(int);
MAGICSMOKE_COMMON_EXPORT int getEntropy();

#endif
