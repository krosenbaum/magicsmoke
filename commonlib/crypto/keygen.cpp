//
// C++ Implementation: keygen
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "keygen.h"

#include <QBoxLayout>
#include <QColor>
#include <QCryptographicHash>
#include <QKeyEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QPalette>
#include <QPointer>
#include <QPushButton>
#include <QSettings>
#include <QTime>

#include "hmac.h"

//enable linux to read /dev/random
#ifdef __linux__
#include <unistd.h>
#include <fcntl.h>
#endif

//amount of random bits needed:
#define RANDNEED 160

static QPointer<EFilter>efilter;

MKeyGen::MKeyGen(QWidget*parentw)
	:QDialog(parentw)
{
	setWindowTitle(tr("Magic Smoke Key Generator"));
	
	//pre-init random buffer
	randstat=-1;
	connect(efilter,SIGNAL(moreBits()),this,SLOT(updateProps()));
	//make sure all mouse events arrive
	setMouseTracking(true);
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QLabel*lab;
	//main explanation
	vl->addWidget(lab=new QLabel(tr("<html><h1>Key Generation</h1>\nI am currently collecting random bits in order to generate a host key for this installation. Please use mouse and keyboard to generate more random. Alternatively you can load a key from an external medium.<p>\nAt least %1 Bits of random are required.").arg(RANDNEED)),0);
	lab->setWordWrap(true);
	lab->setMouseTracking(true);
	//buffer display
	int e=efilter->entropy();
	vl->addWidget(randlab=new QLabel(tr("Current random buffer: %n Bits","",e)),0);
	randlab->setMouseTracking(true);
	randlab->setFrameShape(QFrame::Box);
	randlab->setAutoFillBackground(true);
	
	vl->addStretch(1);
	
	//buttons
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(1);
	hl->addWidget(okbtn=new QPushButton(tr("&OK")),0);
	connect(okbtn,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(ccbtn=new QPushButton(tr("&Cancel")),0);
	connect(ccbtn,SIGNAL(clicked()),this,SLOT(reject()));
	
	//make sure widgets look right
	updateProps();
	
	//make window really big
	showMaximized();
}

MKeyGen::~MKeyGen()
{
}

void MKeyGen::updateProps()
{
	int e=efilter->entropy();
	randlab->setText(tr("Current random buffer: %n Bits","",e));
	if(efilter->entropy()<RANDNEED){
		//check whether we need to set it
		if(randstat==0)return;
		randstat=0;
		//set label color
		QPalette pal=randlab->palette();
		pal.setColor(QPalette::Window,QColor("#ff8080"));
		randlab->setPalette(pal);
		//set button
		okbtn->setEnabled(false);
	}else{
		//check whether we need to set it
		if(randstat==1)return;
		randstat=1;
		//set label color
		QPalette pal=randlab->palette();
		pal.setColor(QPalette::Window,QColor("#80ff80"));
		randlab->setPalette(pal);
		//set button
		okbtn->setEnabled(true);
	}
}

QString MKeyGen::getKey(int len)
{
	if(efilter->entropy()<RANDNEED)return "";
	return efilter->getRandom(len).toHex();
}


EFilter::EFilter()
{
	efilter=this;
	randctr=0;
	//preload seed
	QSettings set;
	randbuf.append(set.value("randomseed").toByteArray());
	randent=randbuf.size();
	if(randent>=RANDNEED)set.setValue("randomseed",getRandom(RANDNEED));
	//if Linux: try to pre-load some random
#ifdef __linux__
	int fd=::open("/dev/random",O_RDONLY|O_NONBLOCK);
	if(fd>=0){
		char buf[RANDNEED/8];
		int rd=::read(fd,buf,sizeof(buf));
		if(rd>0){
			randbuf.append(QByteArray(buf,rd));
			randent+=rd*8;
		}
		::close(fd);
	}
#endif
}

EFilter::~EFilter()
{
	QSettings set;
	set.setValue("randomseed",getRandom(RANDNEED));
}

EFilter* EFilter::instance()
{
	return efilter;
}

QByteArray EFilter::getRandom(int bytes)
{
	QByteArray ret;
	while(ret.size()<bytes){
		randlast.append(QByteArray::number(randctr));
		randlast=SMHmac::hmac(randlast,randbuf,QCryptographicHash::Sha1);
		randctr++;
		randent--;
		ret.append(randlast);
	}
	if(randent<0)randent=0;
	return ret.left(bytes);
}

int EFilter::entropy()
{
	return randent;
}

bool EFilter::eventFilter(QObject*,QEvent*e)
{
	//add to random buffer for key and mouse events
	switch(e->type()){
		case QEvent::KeyPress:case QEvent::KeyRelease:
		{
			QKeyEvent*ke=(QKeyEvent*)e;
			addBit(ke->key()+(int)ke->modifiers());
			break;
		}
		case QEvent::MouseButtonPress:case QEvent::MouseButtonRelease:
		case QEvent::MouseMove:
		{
			QMouseEvent*me=(QMouseEvent*)e;
			addBit((int)me->buttons()+me->x()+me->y());
			break;
		}
		default:
			//ignore
			break;
	}
	return false;
}

void EFilter::addBit(int b)
{
	//add bit to buffer
	randbuf.append((b>>24)&0xff);
	randbuf.append((b>>16)&0xff);
	randbuf.append((b>>8)&0xff);
	randbuf.append(b&0xff);
	randent++;
	//add time info as another bit
	QTime ct=QTime::currentTime();
	if(ct.msec()!=0){ //don't use it if platform has no msecs
		int t=ct.msec()+ct.second()+ct.minute();
		randbuf.append((t>>24)&0xff);
		randbuf.append((t>>16)&0xff);
		randbuf.append((t>>8)&0xff);
		randbuf.append(t&0xff);
		randent++;
	}
	//pack buffer if necessary (>10kB)
	if(randbuf.size()>=10240){
		randbuf=getRandom(80);
		if(randent>320)randent=320;
	}
	//update display
	emit moreBits();
}

QByteArray getRandom(int num)
{
	if(efilter.isNull())return QByteArray();
	return efilter->getRandom(num);
}

int getEntropy()
{
	if(efilter.isNull())return 0;
	return efilter->entropy();
}
