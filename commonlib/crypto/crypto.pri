HEADERS += \
	crypto/keygen.h \
	crypto/hmac.h

SOURCES += \
	crypto/keygen.cpp \
	crypto/hmac.cpp

INCLUDEPATH += ./crypto