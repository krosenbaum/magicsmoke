//
// C++ Implementation: centbox
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "centbox.h"
#include "misc.h"

#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

MCentSpinBox::MCentSpinBox(QWidget*parent,int value,int maxValue)
	:QAbstractSpinBox(parent),mvalid(priceRegExp(),this)
{
	mmin=0;
	mmax=maxValue;
	if(mmin>mmax)mmin=mmax;
	mval=value;
	if(mval<mmin)mval=mmin;
	if(mval>mmax)mval=mmax;
	
	setButtonSymbols(UpDownArrows);
	lineEdit()->setText(cent2str(mval));
	connect(this,SIGNAL(editingFinished()),this,SLOT(fixup()));
}

int MCentSpinBox::value()const
{
// 	qDebug("current value: %i",mval);
	return mval;
}

void MCentSpinBox::setValue(int m)
{
	if(m>mmax)m=mmax;
	if(m<mmin)m=mmin;
	if(mval==m)return;
// 	qDebug("setval %i",m);
	mval=m;
	lineEdit()->setText(cent2str(m));
	emit valueChanged(m);
}

MCentSpinBox::StepEnabled MCentSpinBox::stepEnabled () const
{
	return (mval>mmin?StepDownEnabled:StepNone)|(mval<mmax?StepUpEnabled:StepNone);
}

void MCentSpinBox::setRange(int mi,int ma)
{
	if(mi>ma)return;
	mmin=mi;
	mmax=ma;
	if(mval>mmax){
		setValue(mmax);
	}else
	if(mval<mmin){
		setValue(mmin);
	}
}

int MCentSpinBox::minimumValue()const{return mmin;}
int MCentSpinBox::maximumValue()const{return mmax;}

void MCentSpinBox::stepBy(int x)
{
	int m=mval+x;
	setValue(m);
}

QValidator::State MCentSpinBox::validate(QString &input, int &pos) const
{
	return mvalid.validate(input,pos);
}

void MCentSpinBox::fixup()
{
	int m=str2cent(text());
	setValue(m);
}

/*****************************************************/

MCentDialog::MCentDialog(QWidget*parent,QString title,QString label,int val,int maxValue)
	:QDialog(parent)
{
	setWindowTitle(title);
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(label));
	vl->addWidget(box=new MCentSpinBox(this,val,maxValue));
	QHBoxLayout *hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("OK")));
	p->setDefault(true);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

int MCentDialog::value()const
{
	return box->value();
}

int MCentDialog::getCents(QWidget*parent,QString title,QString label,int val,int maxValue,bool*ok)
{
	MCentDialog d(parent,title,label,val,maxValue);
	bool b=d.exec()==QDialog::Accepted;
	if(ok)*ok=b;
	if(b)
		return d.value();
	else
		return 0;
}
