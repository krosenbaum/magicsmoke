//
// C++ Interface: treeview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TREEVIEW_H
#define MAGICSMOKE_TREEVIEW_H

#include <QTreeView>

#include "commonexport.h"

/**enhances QTreeView to react a bit friendlier on clicks*/
class MAGICSMOKE_COMMON_EXPORT MTreeView:public QTreeView
{
        public:
                MTreeView(QWidget*w=0):QTreeView(w){}
        protected:
                void keyPressEvent(QKeyEvent *event);

};

#endif
