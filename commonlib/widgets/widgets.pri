HEADERS += \
	widgets/centbox.h \
	widgets/listview.h \
	widgets/treeview.h \
	widgets/barcodeline.h \
	widgets/eventview.h \
	widgets/seatplanview.h

SOURCES += \
	widgets/centbox.cpp \
	widgets/listview.cpp \
	widgets/treeview.cpp \
	widgets/barcodeline.cpp \
	widgets/eventview.cpp \
	widgets/seatplanview.cpp

INCLUDEPATH += ./widgets
