//
// C++ Implementation: treeview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "listview.h"

void MListView::keyPressEvent(QKeyEvent *event)
{
        QModelIndex idx=currentIndex();
        QListView::keyPressEvent(event);
        if(idx!=currentIndex())
                emit activated(currentIndex());
}