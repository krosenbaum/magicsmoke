//
// C++ Implementation: event list tab
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "eventview.h"
#include "misc.h"
#include "msinterface.h"
#include "templates.h"
#include "stick.h"

#include <MOEvent>
#include <MOEventPrice>

#include <QEventLoop>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>
#include <QUrlQuery>
#include <QDir>
#include <QMessageBox>
#include <QDesktopServices>


MEventView::MEventView ( QWidget* parent ) : QTextBrowser ( parent )
{
	setReadOnly(true);
	connect(this,SIGNAL(anchorClicked(QUrl)),this,SLOT(handleClick(QUrl)));
	setOpenExternalLinks(false);
	setOpenLinks(false);
	qDebug()<<"Setting path"<<req->parentUrl()<<"for URL"<<req->url();
	setSearchPaths(QStringList()<<req->parentUrl().toString());
	resetPattern();
	mNam=new QNetworkAccessManager(this);
	connect(mNam,SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), req,SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
	initCacheDir();
}

void MEventView::initCacheDir()
{
	mCacheDir=req->dataDir()+"/eventcache";
	QDir d(req->dataDir());
	if(!d.exists("eventcache")){
		if(!d.mkdir("eventcache"))
			qDebug()<<"Warning: cannot create event data cache"<<mCacheDir;
	}
	d.setPath(mCacheDir);
	QDateTime old=QDateTime::currentDateTime().addDays(-2);
	for(auto entry:d.entryInfoList(QDir::Files)){
		if(entry.lastModified()<old)
			d.remove(entry.fileName());
	}
}

static inline QString url2base64(QUrl url)
{
	return url.toEncoded().toBase64(QByteArray::OmitTrailingEquals|QByteArray::Base64UrlEncoding);
}

QVariant MEventView::loadResource ( int type, const QUrl& name )
{
	Q_UNUSED(type);
	qDebug()<<"Loading Resource"<<name;
	if(name.isRelative()||name.scheme()=="http"||name.scheme()=="https"){
		QUrl url(name);
		if(name.isRelative())url=req->url().resolved(name);
		QString encoded=url2base64(url);
		//find in cache
		if(QDir(mCacheDir).exists(encoded)){
			qDebug()<<"Found resource"<<url.toString()<<"in cache as"<<encoded;
			QFile fd(mCacheDir+"/"+encoded);
			fd.open(QIODevice::ReadOnly);
			return fd.readAll();
		}
		//schedule retrieval
		QNetworkReply*rpl= mNam->get(QNetworkRequest(url));
		qDebug()<<"Trying to GET"<<url;
		connect(rpl,&QNetworkReply::finished, this,[=]{saveReply(rpl,encoded);});
		//return dummy
		return QByteArray();
	}else
		return QTextBrowser::loadResource ( type, name );
}

void MEventView::saveReply ( QNetworkReply*rpl, QString fname)
{
	QFile fd(mCacheDir+"/"+fname);
	if(fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		fd.write(rpl->readAll());
		qDebug()<<"...got"<<fd.size()<<"bytes for cache file"<<fname;
		fd.close();
		rpl->deleteLater();
		renderEvent();
	}

}


void MEventView::clearEvent()
{
	clearHistory();
	clear();
}

void MEventView::setEvent ( MOEvent event )
{
	clearEvent();
	mCurrentEvent=event;
	renderEvent();
}

void MEventView::renderEvent()
{
	MStickRenderer renderer;
	renderer.setTemplate(mPattern);
// 	renderer.setTemplateFile(qApp->applicationDirPath()+"/../commonlib/widgets/eventview.html");
	renderer.setProperty("event",QVariant::fromValue<MOEvent>(mCurrentEvent));
	QString detail=renderer.render();
	setHtml(detail);
}

void MEventView::handleClick(const QUrl &url)
{
	//check whether it is order URL
	if(url.scheme()=="order"){
		QUrlQuery query(url);
		if(query.hasQueryItem("ev")){
			bool b=false;
			qint64 evid=query.queryItemValue("ev").toLongLong(&b);
			if(!b || evid<0){
				qDebug()<<"ordering tickets for current event (invalid EvID)...";
				emit eventOrderTicket();
				return;
			}
			qint64 prid=-1;
			if(query.hasQueryItem("pr")){
				prid=query.queryItemValue("pr").toLongLong(&b);
				if(!b)prid=-1;
			}
			qDebug()<<"ordering tickets for event"<<evid<<"price ID"<<prid;
			emit eventOrderTicket(evid,prid);
		}else{
			qDebug()<<"ordering tickets for current event (no EvID)...";
			emit eventOrderTicket();
		}
		return;
	}
	QUrl target=url;
	//Other URLs happen only in the comment or description sections
	if(url.isRelative()){
		//a relative link can only occur in comment or description, so it is relative to the
		//server URL (the index.php used for displaying this on the web page is parallel to machine.php,
		//so we can safely use it as base for resolving the relative URL)
		target=req->url().resolved(url);
		qDebug()<<"Relative URL"<<url<<"aligned to"<<req->url();
		qDebug()<<" ...opening"<<target;
	}
	if(QDesktopServices::openUrl(target))
		qDebug()<<"Opened URL"<<target;
	else{
		qDebug()<<"WARNING: unknown URL type in"<<url;
		QMessageBox::warning(this,tr("Warning"),tr("Unable to open URL %1").arg(url.toString()));
	}
}

void MEventView::setPattern ( QString p )
{
	mPattern=p;
}

void MEventView::resetPattern()
{
	MTemplate tmp=req->templateStore()->getTemplate("eventview",true);
	QString fname;
	if(!tmp.isValid()||!tmp.isHtml()){
		qDebug()<<"Unable to find valid eventview template";
		fname=":/eventview-pattern.html";
	}else
		fname=tmp.cacheFileName();
	QFile fd(fname);
	fd.open(QIODevice::ReadOnly);
	mPattern=QString::fromUtf8(fd.readAll());
}
