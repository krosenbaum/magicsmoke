//
// C++ Implementation: barcode line edit
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#include "barcodeline.h"
#include "barcode-plugin.h"

MBarcodeLine::MBarcodeLine(QWidget* parent): MBarcodeLine(QString(),parent)
{
}

MBarcodeLine::MBarcodeLine(const QString& cont, QWidget* parent): QLineEdit(cont,parent)
{
	setToolTip(tr("Type a barcode into this line or scan it with a barcode scanner."));
	setPlaceholderText(tr("Type or scan a barcode."));
	connect(MBarcodeHub::instance(),SIGNAL(newBarcode(QString)), this,SLOT(setBarcode(QString)));
}

void MBarcodeLine::setBarcode(QString bc)
{
	if(bc.isEmpty())return;
	//do we actually have focus?
	//TODO: maybe this should be visibility?
	if(scanMode()==ScanMode::InFocus && !hasFocus())return;
	if(scanMode()==ScanMode::ForceFocus){
		setFocus();
		emit askForFocus();
	}
	//enter barcode and emit ready signal
	setText(bc);
	emit returnPressed();
}
