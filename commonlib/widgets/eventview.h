//
// C++ Interface: event view`
//
// Description: widget for displaying event details
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_EVENTVIEW_H
#define MAGICSMOKE_EVENTVIEW_H

#include <QTextBrowser>

#include <MOEvent>

#include "commonexport.h"

class QNetworkAccessManager;
class QNetworkReply;

///Event Viewer
class MAGICSMOKE_COMMON_EXPORT MEventView:public QTextBrowser
{
	Q_OBJECT
public:
	explicit MEventView ( QWidget* parent = 0 );
	QVariant loadResource(int type, const QUrl & name)override;

public slots:
	void setEvent(MOEvent);
	void clearEvent();
	void setPattern(QString);
	void resetPattern();
private slots:
	void handleClick(const QUrl&);
signals:
	/**order ticket from event tab*/
	void eventOrderTicket();
	/**order ticket from event tab*/
	void eventOrderTicket(qint64,qint64);
private:
	QString mPattern,mCacheDir;
	QNetworkAccessManager*mNam;
	MOEvent mCurrentEvent;

	void initCacheDir();
	void saveReply(QNetworkReply*,QString);
	void renderEvent();
};


#endif
