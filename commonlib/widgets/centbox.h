//
// C++ Interface: centbox
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_CENTBOX_H
#define MAGICSMOKE_CENTBOX_H

#include <QAbstractSpinBox>
#include <QDialog>
#include <QRegularExpressionValidator>

#include "commonexport.h"

class MAGICSMOKE_COMMON_EXPORT MCentSpinBox:public QAbstractSpinBox
{
	Q_OBJECT
	public:
		MCentSpinBox(QWidget*parent=0,int value=0,int maxValue=2147483647);

		virtual int value()const;
		virtual void setValue(int);

		virtual void setRange(int,int);

		virtual int minimumValue()const;
		virtual int maximumValue()const;

		void stepBy(int);
	protected:
		virtual QValidator::State validate(QString &input, int &pos) const;
		virtual StepEnabled stepEnabled () const;
	signals:
		void valueChanged(int);
	private slots:
		void fixup();
	private:
		int mval,mmax,mmin;
        QRegularExpressionValidator mvalid;
};

class MAGICSMOKE_COMMON_EXPORT MCentDialog:public QDialog
{
	Q_OBJECT
	public:
		MCentDialog(QWidget*parent=0,QString title=QString(),QString label=QString(),int value=0,int maxValue=2147483647);

		virtual int value()const;

		static int getCents(QWidget*parent=0,QString title=QString(),QString label=QString(),int value=0,int maxValue=2147483647,bool *ok=0);
	private:
		MCentSpinBox*box;
};

#endif
