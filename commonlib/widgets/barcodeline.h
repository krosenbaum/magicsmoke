//
// C++ Interface: barcode line edit
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2014
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BARCODELINE_H
#define MAGICSMOKE_BARCODELINE_H

#include <QLineEdit>

#include "commonexport.h"

class MAGICSMOKE_COMMON_EXPORT MBarcodeLine:public QLineEdit
{
	Q_OBJECT
	public:
		explicit MBarcodeLine(QWidget* parent = 0);
		explicit MBarcodeLine(const QString& , QWidget* parent = 0);

		enum class ScanMode{
			InFocus,
			InBackground,
			ForceFocus,
		};

		ScanMode scanMode()const{return mscanmode;}

	private slots:
		void setBarcode(QString);

	public slots:
		void setScanMode(ScanMode mode){mscanmode=mode;}

	signals:
		void askForFocus();

	private:
		ScanMode mscanmode=ScanMode::InFocus;
};

#endif
