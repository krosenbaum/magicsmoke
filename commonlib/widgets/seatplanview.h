//
// C++ Interface: seatplan viewer
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2017
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef SMOKE_SEATPLANVIEW_H
#define SMOKE_SEATPLANVIEW_H

#include <QWidget>
#include <QList>

#include <QtGui/QTransform>

#include "MOSeatPlan"

#include "commonexport.h"

class MAGICSMOKE_COMMON_EXPORT MSeatPlanView:public QWidget
{
    Q_OBJECT
public:
        MSeatPlanView(const MOSeatPlan&);
        
        void resetPlan(const MOSeatPlan&);
        void setBlocked(const QStringList&);
        void setWanted(const QStringList&);
        QStringList wanted()const;
        
        ///used by test gui only: if true then right-click blocks or unblocks instead of "wanting", middle click sets to Unavailable
        void setRightIsBlock(bool b){mrightIsBlock=b;}
        ///if true it is not possible to select seats
        void setReadOnly(bool b){mreadOnly=b;}
        
        MOSeatPlan plan()const{return mplan;}
        
        class SelectedSeat{
                QString mgroupId,mrowId,mgroupName,mrowName;
                int mseatId=0;
                friend class MSeatPlanView;
                SelectedSeat(QString gid,QString gn,QString rid,QString rn,int sid):mgroupId(gid),mrowId(rid),mgroupName(gn),mrowName(rn),mseatId(sid){}
        public:
                SelectedSeat()=default;
                SelectedSeat(const SelectedSeat&)=default;
                
                SelectedSeat& operator=(const SelectedSeat&)=default;
                bool operator==(const SelectedSeat&s)const{return mgroupId==s.mgroupId && mrowId==s.mrowId && mgroupName==s.mgroupName && mrowName==s.mrowName && mseatId==s.mseatId;}
                
                QString groupId()const{return mgroupId;}
                QString groupName()const{return mgroupName;}
                QString rowId()const{return mrowId;}
                QString rowName()const{return mrowName;}
                int seatId()const{return mseatId;}
                
                QString fullId()const{return QString("%1:%2:%3").arg(mgroupId).arg(mrowId).arg(mseatId);}
        };
        QList<SelectedSeat>selection()const;
signals:
        void seatSelected(SelectedSeat);
        void seatDeselected(SelectedSeat);
        void selectionChanged();
        
private slots:
        void checkSelection(SelectedSeat);
        
protected:
        void paintEvent(QPaintEvent*)override;
        void mousePressEvent(QMouseEvent *event)override;
        void mouseReleaseEvent(QMouseEvent *event)override;
        void resizeEvent(QResizeEvent *event)override;


private:
        MOSeatPlan mplan;
        double mscale=0.0;
        QRect mclip;
        bool mrightIsBlock=false,mreadOnly=false;
        enum SeatState {Free,Blocked,Wanted,Unavailable};
        struct Seat {
                Seat(){}
                Seat(int i,QTransform f,QSize s):id(i),position(f),size(s){}
                Seat(const Seat&)=default;
                Seat& operator=(const Seat&)=default;
                
                bool contains(QPoint);
                
                int id=0;
                SeatState state=Free;
                QTransform position;
                QSize size;
        };
        struct Row {
                Row(QString i,QString n,QString gi,QString gn):id(i),name(n),gid(gi),gname(gn){if(name.isEmpty())name=id;if(gname.isEmpty())gname=gid;}
                Row(const Row&)=default;
                Row& operator=(const Row&)=default;
                QString id,name,gid,gname;
                QList<Seat> seats;
                QVector<QColor> colors;
        };
        QList<Row> mrows;
};

#endif
