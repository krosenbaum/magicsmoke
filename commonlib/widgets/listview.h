//
// C++ Interface: treeview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_LISTVIEW_H
#define MAGICSMOKE_LISTVIEW_H

#include <QListView>

#include "commonexport.h"

/**enhances QTreeView to react a bit friendlier on clicks*/
class MAGICSMOKE_COMMON_EXPORT MListView:public QListView
{
        public:
                MListView(QWidget*w=0):QListView(w){}
        protected:
                void keyPressEvent(QKeyEvent *event);

};

#endif
