//
// C++ Implementation: seatplan viewer
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2017
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "seatplanview.h"

#include <QMouseEvent>
#include <QResizeEvent>
#include <QPainter>
#include <QDebug>

MSeatPlanView::MSeatPlanView(const MOSeatPlan&p){resetPlan(p);}


static inline QRect parseGeo(const QString s)
{
        QStringList g=s.trimmed().split(' ');
        if(g.size()!=4)return QRect();
        QRect r(g[0].toInt(), g[1].toInt(), g[2].toInt(), g[3].toInt());
        return r;
}

enum ColorRole {Background=0,TkBackground,WBackground,UBackground,Foreground,TkForeground,WForeground,UForeground,MaxColor=UForeground};

static inline QVector<QColor> getColors(const MOSeatPlan&plan,const MOSeatPlanGroup&group,const MOSeatPlanRow&row)
{
        auto getcol=[](Nullable<QString>ps,Nullable<QString>gs,Nullable<QString>rs,QColor fb)->QColor{
                if(!rs.isNull() && !rs.data().trimmed().isEmpty())return QColor(rs);
                if(!gs.isNull() && !gs.data().trimmed().isEmpty())return QColor(gs);
                if(!ps.isNull() && !ps.data().trimmed().isEmpty())return QColor(ps);
                return fb;
        };
        QVector<QColor> pcolor((int)MaxColor+1);
        pcolor[Foreground]=getcol(plan.fgcolor(),group.fgcolor(),row.fgcolor(),QColor(Qt::black));
        pcolor[Background]=getcol(plan.bgcolor(),group.bgcolor(),row.bgcolor(),QColor(Qt::white));
        pcolor[TkForeground]=getcol(plan.tkfgcolor(),group.tkfgcolor(),row.tkfgcolor(),pcolor[Foreground]);
        pcolor[TkBackground]=getcol(plan.tkcolor(),group.tkcolor(),row.tkcolor(),QColor(Qt::darkGray));
        pcolor[WForeground]=getcol(plan.wfgcolor(),group.wfgcolor(),row.wfgcolor(),pcolor[Foreground]);
        pcolor[WBackground]=getcol(plan.wcolor(),group.wcolor(),row.wcolor(),QColor(Qt::darkGreen));
        pcolor[UForeground]=getcol(plan.ufgcolor(),group.ufgcolor(),row.ufgcolor(),pcolor[Foreground]);
        pcolor[UBackground]=getcol(plan.ucolor(),group.ucolor(),row.ucolor(),QColor(Qt::gray));
        return pcolor;
}

void MSeatPlanView::resetPlan(const MOSeatPlan&p)
{
        mplan=p;

        //parse plan for seats
        mrows.clear();
        const QRect mr=QRect(QPoint(0,0),mplan.qSize());
        for(const auto&grp:mplan.Group()){
                QRect gr=parseGeo(grp.geo());
                if(!gr.isValid())gr=mr;
                qDebug()<<"scanning group"<<grp.id().data()<<gr;
                if(grp.id()->contains(':'))
                        qDebug()<<"WARNING: group"<<grp.id()<<"contains a colon. This is not permitted. Please fix the plan!";
                QTransform gt;
                gt.translate(gr.x(),gr.y());
                gt.rotate(grp.angle());
                for(const auto&row:grp.Row()){
                        QRect rr=parseGeo(row.geo());
                        if(!rr.isValid())rr=QRect(QPoint(0,0),gr.size());
                        qDebug()<<"scanning row"<<row.id()<<rr;
                        if(row.id()->contains(':'))
                                qDebug()<<"WARNING: row"<<row.id()<<"contains a colon. This is not permitted. Please fix the plan!";
                        QTransform rt(gt);
                        rt.translate(rr.x(),rr.y());
                        rt.rotate(row.angle());
                        //create row object
                        Row nrow(row.id(),row.name(),grp.id(),grp.name());
                        nrow.colors=getColors(mplan,grp,row);
                        //auto-generate or explicit seating?
                        if(!row.capacity().isNull()){
                                //assume auto generation
                                if(row.Seat().size()>0)
                                        qDebug()<<"WARNING: row"<<row.id().data()<<"has a capacity for auto-generation, but also contains <Seat> elements! Ignoring  explicit seats. Please fix the plan!";
                                const int cap=row.capacity().data(0);
                                const int first=row.first().data(1);
                                QSize ssz=QSize(rr.width()/cap-1,rr.height());
                                if(ssz.width()>ssz.height())ssz.setWidth(ssz.height());
                                qDebug()<<"generating"<<cap<<"seats";
                                int tx=(rr.width()-cap*ssz.width())/(cap>1?cap-1:1) + ssz.width();
                                for(int i=0;i<cap;i++){
                                        QTransform st(rt);
                                        st.translate(i*tx,0);
                                        nrow.seats<<Seat(i+first,st,ssz);
                                }
                        }else{
                                //assume explicit seating
                                for(const auto&seat:row.Seat()){
                                        QTransform st(rt);
                                        QRect sr=parseGeo(seat.geo());
                                        qDebug()<<"scanning seat"<<seat.id();
                                        st.translate(sr.x(),sr.y());
                                        st.rotate(seat.angle());
                                        nrow.seats<<Seat(seat.id(),st,sr.size());
                                }
                        }
                        //store row
                        if(nrow.seats.size()>0)
                                mrows<<nrow;
                        else
                                qDebug()<<"WARNING: row"<<row.id().data()<<"is empty, ignoring it.";
                }
        }

        //repaint
        resizeEvent(nullptr);
        update();
}

void MSeatPlanView::setBlocked(const QStringList&){}
void MSeatPlanView::setWanted(const QStringList&){}
QStringList MSeatPlanView::wanted()const{return QStringList();}

enum class BgType {Unknown,Line,Rect,Text,Circle,Image};
static inline BgType bgType(const QString s)
{
        if(s=="line")return BgType::Line;
        if(s=="rect")return BgType::Rect;
        if(s=="text")return BgType::Text;
        if(s=="circle")return BgType::Circle;
        if(s=="image")return BgType::Image;

        return BgType::Unknown;
}

static inline QRect toGeo(const QString s,double scale,QRect clip)
{
        QStringList g=s.trimmed().split(' ');
        if(g.size()!=4)return QRect();
        return QRect(clip.x()+scale*g[0].toInt(), clip.y()+scale*g[1].toInt(),
                     scale*g[2].toInt(), scale*g[3].toInt());
}

static inline QImage findImage(const MOSeatPlan &plan,QString imgName)
{
        for(auto&img:plan.Image()){
                if(img.name().data().trimmed()==imgName){
//                         qDebug()<<"image found"<<img.Data().data().size();
                        return QImage::fromData(img.Data());
                }
        }
        //not found
        qDebug()<<"image"<<imgName<<"not found";
        return QImage();
}

void MSeatPlanView::paintEvent(QPaintEvent*e)
{
        QWidget::paintEvent(e);
        if(!mplan.isGraphical())return;

        // painter and background color
        QPainter p(this);
        p.setClipRect(mclip);
        p.fillRect(mclip,QColor(mplan.bgcolor()));

        // first paint background
        for(auto bg:mplan.Background()){
                QRect geo=toGeo(bg.geo(),mscale,mclip);
                p.setPen(QColor(bg.fgcolor()));
                p.setBrush(QColor(bg.bgcolor()));
                switch(bgType(bg.type())){
                        case BgType::Line:
                                p.drawLine(geo.x(),geo.y(),geo.x()+geo.width(),geo.y()+geo.height());
                                break;
                        case BgType::Rect:
                                p.drawRect(geo);
                                break;
                        case BgType::Text:{
                                QFont ft(bg.font().data("Sans"));
                                ft.setPixelSize(bg.fontsize().data(10)*mscale);
                                p.setFont(ft);
                                p.drawText(geo,0,bg.content());
                                break;
                        }
                        case BgType::Circle:
                                p.drawEllipse(geo);
                                break;
                        case BgType::Image:
                                //find image & paint
                                p.drawImage(geo,findImage(mplan,bg.content().data().trimmed()));
                                break;
                        default:
                                qDebug()<<"Warning: unknown background element of type"<<bg.type();
                                break;
                }
        }

        // paint groups -> rows -> seats
        p.setPen(QColor(Qt::blue));
        p.setBrush(QColor(Qt::yellow));
        for(const auto&row:mrows)
                for(const auto&seat:row.seats){
//                         qDebug()<<"painting seat"<<row.id<<seat.id;
                        p.resetTransform();
                        QTransform sp(seat.position);
                        QTransform t;
                        t.translate(mclip.x(),mclip.y());
                        t.scale(mscale,mscale);
                        p.setTransform(sp*t);
                        int fg=Foreground,bg=Background;
                        switch(seat.state){
                                case Free:fg=Foreground;bg=Background;break;
                                case Blocked:fg=TkForeground;bg=TkBackground;break;
                                case Wanted:fg=WForeground;bg=WBackground;break;
                                case Unavailable:fg=UForeground;bg=UBackground;break;
                        }
                        p.setPen(row.colors[fg]);
                        p.setBrush(row.colors[bg]);
                        QFont ft("Helvetica");
                        ft.setPixelSize(qMin(seat.size.width(),seat.size.height())-4);
                        p.setFont(ft);
                        p.drawRect(QRect(QPoint(0,0),seat.size));
                        p.drawText(QRect(QPoint(0,0),seat.size),Qt::AlignCenter,QString::number(seat.id));
                }
}

bool MSeatPlanView::Seat::contains(QPoint p)
{
        QPoint r=p*position.inverted();
        if(r.x()<0||r.y()<0)return false;
        return r.x()<=size.width() && r.y()<=size.height();
}

void MSeatPlanView::mousePressEvent(QMouseEvent *event)
{
        //QWidget::mousePressEvent(event);
        //convert mouse coordinates to plan coordinates
        int x=event->pos().x();
        int y=event->pos().y();
        x-=mclip.x();
        y-=mclip.y();
        x/=mscale;
        y/=mscale;
        //find matching seat
        for(auto&row:mrows)
                for(auto&seat:row.seats)
                        if(seat.contains(QPoint(x,y))){
                                qDebug()<<"found seat"<<row.id<<seat.id<<"at"<<x<<y<<"mouse"<<event->pos();
                                event->setAccepted(true);
                                //button press...
                                if((event->button()&Qt::LeftButton)==Qt::LeftButton){
                                        if(seat.state==Free){
                                                seat.state=Wanted;
                                                emit seatSelected(SelectedSeat(row.gid,row.gname,row.id,row.name,seat.id));
                                                emit selectionChanged();
                                        }else if(seat.state==Wanted){
                                                seat.state=Free;
                                                emit seatDeselected(SelectedSeat(row.gid,row.gname,row.id,row.name,seat.id));
                                                emit selectionChanged();
                                        }
                                        //TODO: handle multiple available price categories?
                                }
                                if(mrightIsBlock && (event->button()&Qt::RightButton)==Qt::RightButton){if(seat.state==Blocked)seat.state=Free;else seat.state=Blocked;}
                                if(mrightIsBlock && (event->button()&Qt::MiddleButton)==Qt::MiddleButton){if(seat.state==Unavailable)seat.state=Free;else seat.state=Unavailable;}
                                //done
                                update();
                                return;
                        }
}
void MSeatPlanView::mouseReleaseEvent(QMouseEvent *event)
{
        QWidget::mouseReleaseEvent(event);
}


void MSeatPlanView::resizeEvent(QResizeEvent *event)
{
        //calculate scaled plan size
        QSize ps=mplan.qSize();
        if(ps.width()>0 && ps.height()>0){
                QSize ws=event?event->size():size();
                double f=(double)ws.width()/ps.width();
                if((f*ps.height())>ws.height())f=(double)ws.height()/ps.height();
                mscale=f;
                mclip.setX((ws.width()-f*ps.width())/2);
                mclip.setY((ws.height()-f*ps.height())/2);
                mclip.setWidth(f*ps.width());
                mclip.setHeight(f*ps.height());
//                 qDebug()<<"widget size"<<ws<<size()<<"plan size"<<ps<<"scale"<<f<<"clip"<<mclip;
        }
        if(event){
                QWidget::resizeEvent(event);
                update();
        }
}

QList<MSeatPlanView::SelectedSeat> MSeatPlanView::selection()const
{
        QList<SelectedSeat>result;
        for(const auto&row:mrows)
                for(const auto&seat:row.seats)
                        if(seat.state==Blocked)
                                result<<SelectedSeat(row.gid,row.gname,row.id,row.name,seat.id);
        return result;
}

void MSeatPlanView::checkSelection(MSeatPlanView::SelectedSeat)
{
    //TODO
}
