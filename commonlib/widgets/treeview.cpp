//
// C++ Implementation: treeview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "treeview.h"

void MTreeView::keyPressEvent(QKeyEvent *event)
{
        QModelIndex idx=currentIndex();
        QTreeView::keyPressEvent(event);
        if(idx!=currentIndex())
                emit activated(currentIndex());
}