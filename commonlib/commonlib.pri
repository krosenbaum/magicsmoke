#Include PRI for the config library

LIBS += -lmagicsmoke-common
INCLUDEPATH += $$PWD/../commonlib $$PWD/../commonlib/crypto $$PWD/../commonlib/widgets $$PWD/../commonlib/misc $$PWD/../commonlib/templates $$PWD/../commonlib/stick
QT += gui widgets

include($$PWD/../iface/iface.pri)
include($$PWD/../taurus/zip.pri)
include($$PWD/../taurus/elam.pri)
include($$PWD/../taurus/aurora.pri)
