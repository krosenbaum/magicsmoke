#basic compile settings

#put everything here:
DESTDIR = $$PWD/bin

#hide classes that are not explicitly exported
# this is default on Windows, we have it here to have all systems behave the same way
CONFIG += hide_symbols
#use PRL files (link info)
CONFIG += create_prl link_prl
#put debug symbols in separate file (on linux)
CONFIG += separate_debug_info
#enable C++-11 features
CONFIG += c++11

#FIXME: compat
equals(QT_MAJOR_VERSION, 6): QT += core5compat

#compilation output:
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

#make sure libs are found
LIBS += -L$$PWD/bin

#protect the stack
QMAKE_CFLAGS += -fstack-protector-all -Wstack-protector
QMAKE_CXXFLAGS += -fstack-protector-all -Wstack-protector

#ASLR for windows
win32 {
  QMAKE_LFLAGS +=  -Wl,--nxcompat -Wl,--dynamicbase
  LIBS += -lssp
}

linux-g++* {
  #make sure we find our libs
  QMAKE_LFLAGS += -Wl,-rpath,\'\$$ORIGIN\'
}
