# Libraries for Magic Smoke by (c) Konrad Rosenbaum, 2007-2013

# ZIP library
include($$PWD/../taurus/zip.pri)

# MagicSmoke Interface
include ($$PWD/../iface/iface.pri)

# Aurora Updater library
include($$PWD/../taurus/aurora.pri)

# Session Client library
include($$PWD/../sesscli/sesscli.pri)

# Common classes
include($$PWD/../commonlib/commonlib.pri)

#make sure the correct Qt DLLs are used
QT += xml network
QT += widgets printsupport
