#Project File for MagicSmoke
# (c) Konrad Rosenbaum, 2007-2013

TEMPLATE = lib
TARGET = magicsmoke
VERSION = 

#Localization
TRANSLATIONS = \
	smoke_de.ts \
	smoke_en.ts

#main source files
SOURCES = main.cpp
HEADERS = main.h
INCLUDEPATH += .

#make sure exports are ok
DEFINES += MAGICSMOKE_LIB_BUILD=1

#Base URL of MagicSmoke, 
# this URL points to the general homepage with downloads, links etc.
# BASEURL/doc/index.html points to the default documentation
# you need the triple-backslash to make it a string constant
DEFINES += HOMEPAGE_BASEURL=\\\"http://smoke.silmor.de\\\"

#images
RESOURCES += images/files.qrc

#libraries, modules, stuff...
include(templates/templates.pri)
include(dialogs/dialogs.pri)
include(mwin/mwin.pri)

include (../basics.pri)
include (libs.pri)

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
