//
// C++ Interface: customer
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_CUSTOMERDLG_H
#define MAGICSMOKE_CUSTOMERDLG_H

#include <QString>
#include <QDialog>
#include <QFrame>
#include <QItemDelegate>
#include <QList>
#include <QScrollArea>

#include "MOCustomerInfo"
#include "MOCustomer"

class QComboBox;
class QHBoxLayout;
class QLineEdit;
class QListView;
class QSortFilterProxyModel;
class QStandardItemModel;
class QTableView;

/**shows a list of customers, lets the user select and offers to alter/create customers*/
class MCustomerListDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates a new customer list dialog; expects a usable webrequest object and a parent, if isselect is set to true it offers a select button, if selected is set to a matching customerID it will be pre-selected*/
		MCustomerListDialog(QWidget*,bool isselect=false,qint64 selected=-1);
		
		/**returns the selected customer*/
		MOCustomerInfo getCustomer();
		
	private slots:
		/**internal: new customer*/
		void newCustomer();
		/**internal: edit selected customer*/
		void editCustomer();
		/**internal: delete selected customer*/
		void deleteCustomer();

	private:
		QList<MOCustomerInfo> m_list;
		QListView*m_listview;
		QStandardItemModel*m_listmodel;
		QSortFilterProxyModel*m_proxymodel;
		QLineEdit*m_filter;
		
		/**updates internal list*/
		void updateList(int id=-1);
};

class QLineEdit;
class QTextEdit;
class QLabel;

/**displays exactly one address; even though it offers to edit or delete addresses, it does not actually do any change on the database*/
class MAddressWidget:public QFrame
{
	Q_OBJECT
	public:
		/**creates the widget, if isselect==true a "select" button is shown*/
		MAddressWidget(QWidget*parent=0,bool isselect=false);
		
		/**returns the currently shown address*/
		MOAddress address()const;
		
	public slots:
		/**sets the address to be shown, enables the buttons if it is valid*/
		void setAddress(const MOAddress&);
		
		/**convenience method: sets the address to be shown, retrieves it from the database*/
		void setAddress(qint64);
		
		/**clears the display, disables the buttons*/
		void clearAddress();
		
	signals:
		/**emitted if the address has changed, the parameter contains the current content;
		this also emits if the address has changed or cleared programmatically, use addressEdited if you want only manual changes*/
		void addressChanged(const MOAddress&);
		
		/**emitted if the address was changed by the user, contains the current address content*/
		void addressEdited(const MOAddress&);
		
		/**emitted if the address was deleted by the user, addressEdited is also emitted*/
		void addressDeleted(const MOAddress&);
		
		/**emitted if the user presses the "select" button, contains the address*/
		void addressSelected(const MOAddress&);
		
		/**emitted if the user presses the "select" button, contains the address ID*/
		void addressSelected(qint64);
		
		/**internal: used to activate or de-activate buttons*/
		void addrValid(bool);
		
	private slots:
		/**internal: the user clicked edit*/
		void editClick();
		/**internal: the user clicked delete*/
		void delClick();
		/**internal: the user clicked select*/
		void selectClick();
		/**internal: re-display the address*/
		void redisplay();
		
	private:
		MOAddress m_addr;
		QLabel*m_lab;
};

class QSignalMapper;
class QGridLayout;

/**widget that displays a list of addresses, it uses MAddressWidget to display every single address*/
class MAddressListWidget:public QScrollArea
{
	Q_OBJECT
	public:
		/**creates the widget*/
		MAddressListWidget(QWidget*parent,const QList<MOAddress>&,bool isselect=false);
		
		/**returns the current list*/
		QList<MOAddress> addressList()const{return m_addr;}
		
		/**returns the selected address*/
		MOAddress selection()const{return m_sel;}
		
	signals:
		/**emitted if the address has changed, the parameter contains the current content;
		this also emits if the address has changed programmatically, use addressEdited if you want only manual changes*/
		void addressChanged(const MOAddress&);
		
		/**emitted if the address was changed by the user, contains the current address content*/
		void addressEdited(const MOAddress&);
		
		/**emitted if the address was deleted by the user, addressEdited is also emitted*/
		void addressDeleted(const MOAddress&);
		
		/**emitted if the user presses the "select" button, contains the address*/
		void addressSelected(const MOAddress&);
		
		/**emitted if the user presses the "select" button, contains the address ID*/
		void addressSelected(qint64);
		
		/**emitted if the user creates a new address via createAddress()*/
		void addressCreated(const MOAddress&);
		
	public slots:
		/**asks the user for a new address*/
		void createAddress();
		
		/**pre-selects an address by its position in the current address list, this method should only be called once!*/
		void preselect(int);
		
		///alias: pre-selects an address if it is present in the list of displayed addresses
		void preselect(const MOAddress&);
		
	private slots:
		/**internal: used to change an address after editing*/
		void addrEdit(int);
		
		/**internal: used to remember the selected address*/
		void addrSel(const MOAddress&);
		
		/**internal: add an address widget, 
		\param vis if true the routine ensures the widget is in the visible part of the scrollarea
		\param mark if true marks the address with a color
		*/
		void addAddr(const MOAddress&,bool vis=false);
		
	private:
		QList<MOAddress>m_addr;
		MOAddress m_sel;
		QSignalMapper *map;
		bool m_select;
		QGridLayout*m_layout;
};

/**wrapper dialog to chose an address; updates the customer is there is a change*/
class MAddressChoiceDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates the dialog*/
		MAddressChoiceDialog(QWidget*,const MOCustomer&);
		
		/**returns the chosen address or an invalid address if none was chosen*/
		MOAddress address()const;
		
		/**returns the ID of the chosen address or -1 if the unselect button was used*/
		qint64 addressId()const;
		
		/**returns the (modified) customer object*/
		MOCustomer customer()const{return m_cust;}
	public slots:
		///pre-select an address
		void preselect(const MOAddress&);
		///add an unselect button
		void addUnselectButton(const QString&);
	private slots:
		/**internal: notified if there is a change to the address list*/
		void changed();
		/**internal: updates the customer if necessary*/
		void updateCustomer();
		///unselect button pressed
		void unselect();
	private:
		MOCustomer m_cust;
		bool m_needupdate,m_unsel;
		MAddressListWidget*m_lwidget;
		QHBoxLayout*m_btnlayout;
};

/**dialog for editing exactly one address*/
class MAddressDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates the dialog, if no valid address is given it displays as "create address" otherwise as "edit address"*/
		MAddressDialog(QWidget*parent=0,MOAddress addr=MOAddress());
		
		/**returns the address*/
		MOAddress address();
		
		///helper function to create a new country, used by MAddressDialog and MNewCustomerWizard
		///returns a valid country on success
		static MOCountry createNewCountry(QWidget*parent=0);
	private slots:
		void selectCountry();
	private:
		QLineEdit *m_name,*m_addr1,*m_addr2,*m_city,*m_state,*m_zip,*m_country;
		MOAddress m_addr;
};

/**edit a specific customer*/
class MCustomerDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates a new customer dialog*/
		MCustomerDialog(MOCustomer,QWidget*);
		
		/**returns the customer as currently entered*/
		MOCustomer getCustomer();
		
	private slots:
		/**internal: save data*/
		void save();
		/**internal: add contact*/
		void addContact();
		/**internal: remove contact*/
		void removeContact();
		///internal: change mail address
		void changeMail();
		///internal: send a reset mail
		void resetLogin();
		
	private:
		MOCustomer m_cust;
		QLineEdit*m_name,*m_fname,*m_title;
		QTextEdit*m_comm;
		MAddressListWidget*m_addr;
		QLabel*m_mail;
		QTableView*m_contact;
		QStandardItemModel*m_contactmodel;
};

/**Helper class for customer contact table*/
class MContactTableDelegate:public QItemDelegate
{
	Q_OBJECT
	public:
		MContactTableDelegate(QObject *parent = 0);
		
		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
				      const QModelIndex &index) const;
		
		void setEditorData(QWidget *editor, const QModelIndex &index) const;
		void setModelData(QWidget *editor, QAbstractItemModel *model,
				  const QModelIndex &index) const;
	private:
		//cache of contact types
		//why ever the interesting delegate methods are const, this has to be mutable then
		mutable QList<MOContactType>m_typelist;
};


/**Wizard for creating a new customer.*/
class MNewCustomerWizard:public QDialog
{
	Q_OBJECT
	public:
		MNewCustomerWizard(QWidget* parent = 0, Qt::WindowFlags f = 0);
		
		MOCustomer customer()const{return m_cust;}
		
		static MOCustomer getNewCustomer(QWidget*parent=0);
	private slots:
		void save();
		void newcountry();
		void updateCountry(QString id=QString());
	private:
		MOCustomer m_cust;
		QComboBox*m_title,*m_city,*m_state,*m_country;
		QLineEdit*m_name,*m_fname,*m_addr1,*m_zipcode;
		QList<MOContactType>m_typelist;
		QList<MOCountry>m_countrylist;
		QList<QLineEdit*>m_contact;
};

#endif
