//
// C++ Implementation: pricecatdlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "pricecatdlg.h"
#include "msinterface.h"
#include "flagedit.h"

#include "MTGetAllPriceCategories"
#include "MTCreatePriceCategory"
#include "MTChangePriceCategory"

#include <QBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>

MPriceCategoryDialog::MPriceCategoryDialog(QWidget*pw,bool showselect)
	:QDialog(pw)
{
	m_cat=req->queryGetAllPriceCategories().getpricecategories();
	setWindowTitle(tr("Select a Price Category"));
	setSizeGripEnabled(true);
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(m_list=new QListWidget,10);
	for(int i=0;i<m_cat.size();i++){
		QListWidgetItem*it=new QListWidgetItem(m_cat[i].name());
		it->setData(Qt::UserRole,i);
		m_list->addItem(it);
	}
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("New...","new price category")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newCat()));
	p->setEnabled(req->hasRight(req->RCreatePriceCategory));
	hl->addWidget(p=new QPushButton(tr("Edit...","edit price category")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editCat()));
	p->setEnabled(req->hasRight(req->RChangePriceCategory));
	if(showselect){
		hl->addWidget(p=new QPushButton(tr("Select","select price category")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(accept()));	
		hl->addWidget(p=new QPushButton(tr("Cancel")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	}else{
		hl->addWidget(p=new QPushButton(tr("Close")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	}
}

MOPriceCategory MPriceCategoryDialog::selection()const
{
	//get selection
	QList<QListWidgetItem*>wlst=m_list->selectedItems();
	if(wlst.size()<1)return MOPriceCategory();
	return m_cat[wlst[0]->data(Qt::UserRole).toInt()];
}

void MPriceCategoryDialog::newCat()
{
	MPCDEdit d(this,MOPriceCategory());
	//ask
	if(d.exec()!=QDialog::Accepted)return;
	//get props, create cat
	MOPriceCategory cat=d.result();
	MTCreatePriceCategory cpc=req->queryCreatePriceCategory(cat);
	if(cpc.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while creating new price category: %1").arg(cpc.errorString()));
		return;
	}
	//add and select
	cat=cpc.getpricecategory();
	m_cat<<cat;
	QListWidgetItem*it=new QListWidgetItem(cat.name());
	it->setData(Qt::UserRole,m_cat.size()-1);
	m_list->addItem(it);
	m_list->setCurrentItem(it);
}

void MPriceCategoryDialog::editCat()
{
	//get current
	QList<QListWidgetItem*>wlst=m_list->selectedItems();
	if(wlst.size()<1)return;
	//ask
	int pcidx=wlst[0]->data(Qt::UserRole).toInt();
	MPCDEdit d(this,m_cat[pcidx]);
	if(d.exec()!=QDialog::Accepted)return;
	//get props, create cat
	MOPriceCategory cat=d.result();
	MTChangePriceCategory cpc=req->queryChangePriceCategory(cat);
	if(cpc.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while creating new price category: %1").arg(cpc.errorString()));
		return;
	}
	//add and select
	cat=cpc.getpricecategory();
	m_cat[pcidx]=cat;
	wlst[0]->setText(cat.name());
}



MPCDEdit::MPCDEdit(QWidget*par,const MOPriceCategory&c)
	:QDialog(par),cat(c)
{
	//TODO: remaining properties
	//dialog
	bool b=cat.pricecategoryid().isNull();
	setWindowTitle(b?tr("New Price Category"):tr("Change Price Category"));
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QPushButton*p;
	setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout,1);
	fl->addRow(tr("Category Name:"),name=new QLineEdit(cat.name()));
	fl->addRow(tr("Category Abbreviation:"),abbr=new QLineEdit(cat.abbreviation()));
    abbr->setMaxLength(10);//limited in DB
	fl->addRow(tr("Formula:"),form=new QLineEdit(cat.formula()));
	fl->addRow(tr("Flags:"),hl=new QHBoxLayout);
	hl->addWidget(flags=new QLabel(cat.flags()),1);
	hl->addWidget(p=new QPushButton("..."));
	connect(p,SIGNAL(clicked(bool)),this,SLOT(editFlags()));
	vl->addSpacing(10);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(b?tr("Create"):tr("Save")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	setSizeGripEnabled(true);
}

void MPCDEdit::editFlags()
{
	flags->setText(MFlagEditor::edit(this, flags->text(), tr("Flags of price category '%1':").arg(name->text())));
}

MOPriceCategory MPCDEdit::result()const
{
	MOPriceCategory rcat(cat);
	rcat.setname(name->text());
	rcat.setabbreviation(abbr->text());
	rcat.setformula(form->text());
	rcat.setflags(flags->text());
	return rcat;
}
