//
// C++ Interface: eventedit
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef SMOKE_EVENT_EDIT_H
#define SMOKE_EVENT_EDIT_H

#include <QDialog>

#include "MOEvent"

class MFlagWidget;
class QCheckBox;
class QDateTime;
class QDateTimeEdit;
class QLabel;
class QLineEdit;
class QSpinBox;
class QStandardItemModel;
class QTableView;
class QTextEdit;

/**create and edit events*/
class MEventEditor:public QDialog
{
	Q_OBJECT
	public:
		///Mode for opening the editor window
		enum class OpenMode{
			/// Auto-Detect by Event-ID
			Auto,
			/// Edit the existing event
			Edit,
			/// Create a new event, if an ID is given use it as template only
			Create
		};
		/**opens the editor, retrieves the event automatically if given*/
		MEventEditor(QWidget*w,qint64 id=-1):MEventEditor(w,OpenMode::Auto,id){}
		/**opens the editor*/
		MEventEditor(QWidget*,OpenMode,qint64 id=-1);
	private slots:
		/**sends the event back to the database*/
		void writeBack();
		
		/**room button has been clicked: open a list of rooms, select one*/
		void selectRoom();
		/**create a new room*/
		void newRoom();
		/**seatplan button has been clicked: open a list of plans, select one*/
		void selectSeatPlan();
		///create a new seat plan
		void newSeatPlan();

		/**artist button has been clicked: open a list of artists, select one*/
		void selectArtist();
		/**create a new artist*/
		void newArtist();
		
		/**called when the start time of the event changes: validates the end time*/
		void startTimeChanged(const QDateTime&);
		/**called when the end time of the event changes: validates the start time*/
		void endTimeChanged(const QDateTime&);
		
		/**updates the price table*/
		void updatePrice();
		/**changes a price*/
		void changePrice();
		/**adds a price*/
		void addPrice();
		/**removes a price*/
		void removePrice();
		
	private:
		MOEvent event;
		QDateTimeEdit*starttime,*endtime;
		QLineEdit*title,*artist,*room,*cancelreason,*seatplan;
		QTextEdit*description,*comment;
		QCheckBox*cancelcheck;
		QSpinBox*capacity;
		QLabel*eventid;
		MFlagWidget*flags;
		QTableView*pricetable;
		QStandardItemModel*pricemodel;
		int seatplanid=-1;
};

class MCentSpinBox;
class QSpinBox;

/**helper class for event editor: edits a price */
class MEEPriceEdit:public QDialog
{
	Q_OBJECT
	protected:
		friend class MEventEditor;
		MCentSpinBox*price;
		QSpinBox*cap,*ord;
		QLabel*flg,*cname;
		MEEPriceEdit(QWidget*,MOEventPrice,int);
	private slots:
		void setFlags();
};



#endif
