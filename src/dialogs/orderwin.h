//
// C++ Interface: orderwin
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ORDERWIN_H
#define MAGICSMOKE_ORDERWIN_H

#include <QDialog>
#include <QMainWindow>
#include <QMap>

#include "odtrender.h"

#include "MOOrder"
#include "MOShipping"

class QAction;
class QLabel;
class QTableView;
class QStandardItemModel;
class QPrinter;

/**displays an order and allows the user to execute several commands on it*/
class MOrderWindow:public QMainWindow
{
	Q_OBJECT
	public:
		/**creates the order window*/
		MOrderWindow(QWidget*,const MOOrder&);
		
		/**used by the ODF editor: sets the template file to use for testing*/
		void setTestTemplateFile(QString);
	
	private slots:
		/**internal: updates the labels and the ticket table*/
		void updateData();
		
		/**internal: show the tickets as graphics*/
		void itemView();
		/**internal: print the currently selected ticket*/
		void printCurrentItem();
		/**internal: print all tickets*/
		void printTickets();
		/**internal helper: print list of tickets*/
		void printTickets(QList<MOTicket>);
		/**internal: print all vouchers*/
		void printVouchers();
		/**internal helper: print list of vouchers*/
		void printVouchers(QList<MOVoucher>);
		
		/**print a bill*/
		void printBill();
		/**save the bill as file*/
		void saveBill();

		/**received payment*/
		void payment();
		/**generate a refund*/
		void refund();
		/**pay with a voucher*/
		void payvoucher();
		
		/**change a ticket/voucher price*/
		void changeItem();
		/**change a ticket price category*/
		void changeItemCat();
		///change the validity of a voucher
		void changeVoucherValid();
		/**return a ticket/voucher*/
		void itemReturn();
		
		/**change the comment on the order*/
		void addComment();
		/**change the comment on the order*/
		void changeComments();
		
		/**change the shipping option*/
		void changeShipping();
		
		///change the invoice address
		void changeInvAddr();
		///change the delivery address
		void changeDelAddr();
		///view/change customer data
		void changeCustData();
		
		/**cancel the order*/
		void cancelOrder();
		/**mark as shipped*/
		void shipOrder();
		
		/**create a new order from a reservation*/
		void createOrder();
		
		///retrieve audit data for a voucher
		void voucherAudit();
		///retrieve audit data for a ticket
		void ticketAudit();
		///retrieve order audit
		void orderAudit();
		
	private:
		MOOrder m_order;
		bool m_changed;
		QLabel *m_orderid,*m_orderdate,*m_sentdate,*m_state,*m_paid,*m_total,*m_comment,
		 *m_shipmeth,*m_shipprice,*m_daddr,*m_iaddr,*m_soldby,*m_custname,*m_coupon;
		QTableView *m_table;
		QStandardItemModel *m_model;
		QAction*m_res2order,*m_cancel,*m_ship,*m_pay,*m_payv,*m_refund;
		QMap<QString,int>m_loopiter;
		QString m_testTemplate;
};

class MTicketRenderer;
class MVoucherRenderer;

/**helper class: displays tickets and vouchers*/
class MOrderItemView:public QDialog
{
	Q_OBJECT
	public:
		MOrderItemView(QWidget*,QList<MOTicket>,QList<MOVoucher>);
		~MOrderItemView();
		
	private slots:
		void changeItem(int);
	private:
		QList<MOTicket> tickets;
		QList<MOVoucher> vouchers;
		QLabel*disp;
		MTicketRenderer*trender;
		MVoucherRenderer*vrender;
};

class MCentSpinBox;
class QComboBox;

/**helper class: allows to change the shipping option*/
class MShippingChange:public QDialog
{
	Q_OBJECT
	public:
		/**creates the dialog*/
		MShippingChange(QWidget*,MOShipping,int);
		
		/**returns the selected shipping option, including corrected price*/
		MOShipping selection()const;
		/**returns the entered price in cent*/
		int price()const;
		
	private slots:
		/**internal: updates price when new option is selected*/
		void changeItem(int);
	private:
		QList<MOShipping> all;
		QComboBox*opt;
		MCentSpinBox*prc;
};

#endif
