//
// C++ Interface: orderaudit dialog
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ORDERAUDITDLG_P_H
#define MAGICSMOKE_ORDERAUDITDLG_P_H

#include "orderauditdlg.h"

#include <MTGetOrderAudit>
#include <QMetaMethod>
#include <TimeStamp>

template<class T>
static bool hasMetaValue(T*obj,QString n)
{
	Q_UNUSED(obj);
	const QMetaObject mobj=T::staticMetaObject;
	//look for property
	int id=mobj.indexOfProperty(n.toLatin1().data());
	if(id>=0)return true;
	//look for method
	id=mobj.indexOfMethod(n.toLatin1().data());
	if(id<0)return false;
	QMetaMethod mm=mobj.method(id);
	if(mm.parameterNames().size()!=0)return false;
	if(QString(mm.typeName()).isEmpty())return false;
	return true;
}

template<class T>
static QVariant getMetaValue(T*obj,QString n)
{
	const QMetaObject mobj=T::staticMetaObject;
	//look for property
	int id=mobj.indexOfProperty(n.toLatin1().data());
	if(id>=0)return mobj.property(id).readOnGadget(obj);
	//look for method
	id=mobj.indexOfMethod(n.toLatin1().data());
	if(id<0)return QVariant();
	QMetaMethod mm=mobj.method(id);
	if(mm.parameterNames().size()!=0)return QVariant();
	if(QString(mm.typeName()).isEmpty())return QVariant();
	//try to invoke
	int dataid=QMetaType::type(mm.typeName());
	void *data=QMetaType::create(dataid);
	QVariant ret;
	if(mm.invokeOnGadget(obj, QGenericReturnArgument(mm.typeName(),data)))
		ret=QVariant(dataid,data);
	QMetaType::destroy(dataid,data);
	return ret;
}

class MOADynamicObject:public QObject
{
	public:
		virtual bool hasValue(QString n)//{return hasMetaValue(this,n);}
		{
			const QMetaObject*mobj=metaObject();
			//look for property
			int id=mobj->indexOfProperty(n.toLatin1().data());
			if(id>=0)return true;
			//look for method
			id=mobj->indexOfMethod(n.toLatin1().data());
			if(id<0)return false;
			QMetaMethod mm=mobj->method(id);
			if(mm.parameterNames().size()!=0)return false;
			if(QString(mm.typeName()).isEmpty())return false;
			return true;
		}
		virtual QVariant getValue(QString n)//{return getMetaValue(this,n);}
		{
			const QMetaObject*mobj=metaObject();
			//look for property
			int id=mobj->indexOfProperty(n.toLatin1().data());
			if(id>=0)return mobj->property(id).read(this);
			//look for method
			id=mobj->indexOfMethod(n.toLatin1().data());
			if(id<0)return QVariant();
			QMetaMethod mm=mobj->method(id);
			if(mm.parameterNames().size()!=0)return QVariant();
			if(QString(mm.typeName()).isEmpty())return QVariant();
			//try to invoke
			int dataid=QMetaType::type(mm.typeName());
			void *data=QMetaType::create(dataid);
			QVariant ret;
			if(mm.invoke(this, Qt::DirectConnection, QGenericReturnArgument(mm.typeName(),data)))
				ret=QVariant(dataid,data);
			QMetaType::destroy(dataid,data);
			return ret;
		}
};

class MOAItem:public MOADynamicObject
{
	Q_OBJECT
	Q_PROPERTY(qint64 auditTime READ auditTime)
	Q_PROPERTY(QString auditUser READ auditUser)
	Q_PROPERTY(QString auditTransaction READ auditTransaction)
	Q_PROPERTY(QString itemID READ itemID)
	Q_PROPERTY(QString itemTypeString READ itemTypeString)
	public:
		enum ItemType{None,Order,Ticket,Voucher};
		ItemType itemType()const{return mtype;}
		
		MOAItem(){mtype=None;}
		MOAItem(const MOOrderAudit&a){morder=a;mtype=Order;}
		MOAItem(const MOTicketAudit&a){mticket=a;mtype=Ticket;}
		MOAItem(const MOVoucherAudit&a){mvoucher=a;mtype=Voucher;}
		MOAItem(const MOAItem&i){operator=(i);}
		MOAItem&operator=(const MOAItem&i)
		{
			mtype=i.mtype;
			morder=i.morder;mticket=i.mticket;mvoucher=i.mvoucher;
			return *this;
		}

		virtual bool hasValue(QString n){
			if(hasMetaValue(this,n))return true;
			switch(mtype){
				case Order:return hasMetaValue(&morder,n);
				case Ticket:return hasMetaValue(&mticket,n);
				case Voucher:return hasMetaValue(&mvoucher,n);
				default: return false;
			}
		}
		virtual QVariant getValue(QString n){
			if(hasMetaValue(this,n))return getMetaValue(this,n);
			switch(mtype){
				case Order:return getMetaValue(&morder,n);
				case Ticket:return getMetaValue(&mticket,n);
				case Voucher:return getMetaValue(&mvoucher,n);
				default: return false;
			}
		}
	public slots:
		MOOrderAudit order()const{return morder;}
		MOTicketAudit ticket()const{return mticket;}
		MOVoucherAudit voucher()const{return mvoucher;}
		
		qint64 auditTime()const{switch(mtype){
			case Order:return (qint64)morder.audittime();
			case Ticket:return (qint64)mticket.audittime();
			case Voucher:return (qint64)mvoucher.audittime();
			default: return 0;
		}}
		QString auditUser()const{switch(mtype){
			case Order:return morder.audituname();
			case Ticket:return mticket.audituname();
			case Voucher:return mvoucher.audituname();
			default: return QString();
		}}
		QString auditTransaction()const{switch(mtype){
			case Order:return morder.audittransaction();
			case Ticket:return mticket.audittransaction();
			case Voucher:return mvoucher.audittransaction();
			default: return QString();
		}}
		
		bool isSameTransaction(const MOAItem&i)const{
			if(mtype==None || i.mtype==None)return false;
			return auditTime() == i.auditTime() &&
			       auditUser() == i.auditUser() &&
			       auditTransaction() == i.auditTransaction();
		}
		
		QString itemID()const{switch(mtype){
			case Order:return QString::number(morder.orderid());
			case Ticket:return mticket.ticketid();
			case Voucher:return mvoucher.voucherid();
			default:return QString();
		}}
		
		QString itemTypeString()const{switch(mtype){
			case Order:return QCoreApplication::translate("MOAItem","Order");
			case Ticket:return QCoreApplication::translate("MOAItem","Ticket");
			case Voucher:return QCoreApplication::translate("MOAItem","Voucher");
			default:return QString();
		}}
		
	private:
		ItemType mtype;
		MOOrderAudit morder;
		MOTicketAudit mticket;
		MOVoucherAudit mvoucher;
};

class MOAParcel:public MOADynamicObject
{
	Q_OBJECT
	Q_PROPERTY(qint64 auditTime READ auditTime)
	Q_PROPERTY(QString auditUser READ auditUser)
	Q_PROPERTY(QString auditTransaction READ auditTransaction)
	Q_PROPERTY(qint64 size READ size)
	Q_PROPERTY(qint64 totalPrice READ totalPrice)
	public:
		MOAParcel(){mprice=0;}
		MOAParcel(const MOAParcel&p){mitems=p.mitems;mprice=p.mprice;}
		MOAParcel(const MOAItem&i){mitems<<i;mprice=0;}
		MOAParcel& operator=(const MOAParcel&p){
			mitems=p.mitems;mprice=p.mprice;
			return *this;}
		
	public slots:
		qint64 auditTime()const{
			if(mitems.size()>0)return mitems[0].auditTime();
			else return 0;
		}
		QString auditUser()const{
			if(mitems.size()>0)return mitems[0].auditUser();
			else return QString();
		}
		QString auditTransaction()const{
			if(mitems.size()>0)return mitems[0].auditTransaction();
			else return QString();
		}
		
		bool isSameTransaction(const MOAItem&i)const{
			if(mitems.size()!=0)
				return mitems[0].isSameTransaction(i);
			else
				return true;
		}
		bool addItem(const MOAItem&i){
			if(isSameTransaction(i)){
				mitems<<i;
				return true;
			}else return false;
		}
		
		qint64 size()const{return mitems.size();}
		
		qint64 totalPrice()const{return mprice;}
	public:
		MOAItem item(int i)const{return mitems.value(i);}
		MOAItem operator[](int i)const{return mitems[i];}
		MOAItem& operator[](int i){return mitems[i];}
		MOAItem& item(int i){return mitems[i];}
		
		void setPrice(qint64 p){mprice=p;}
		
		//used for sorting
		bool operator<(const MOAParcel&p)const{return auditTime()<p.auditTime();}
		
	private:
		QList<MOAItem>mitems;
		qint64 mprice;
};

#endif
