//
// C++ Implementation: eventsummary
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "eventsummary.h"
#include "misc.h"
#include "odtrender.h"
#include "msinterface.h"

#include "MTGetEventSummary"

#include <QBoxLayout>
#include <QDomDocument>
#include <QDomElement>
#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>

MEventSummary::MEventSummary(QWidget*par,qint64 eid)
	:QDialog(par),eventid(eid)
{
	nreserved=ncancelled=ntotaltickets=ntotalmoney=0;
	//get event data
	getSummaryData();
	//layout/tabs
	setWindowTitle(tr("Summary for Event %1").arg(event.title()));
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QTabWidget*tab;
	vl->addWidget(tab=new QTabWidget);
	QWidget*w;
	tab->addTab(w=new QWidget,tr("Summary"));
	QGridLayout*gl;
	w->setLayout(gl=new QGridLayout);
	int rc=0;
	gl->addWidget(new QLabel(tr("Title:")),rc,0);
	gl->addWidget(new QLabel(event.title()),rc,1);
	gl->addWidget(new QLabel(tr("Artist:")),++rc,0);
	gl->addWidget(new QLabel(event.artist().value().name()),rc,1);
	gl->addWidget(new QLabel(tr("Start:")),++rc,0);
	gl->addWidget(new QLabel(event.startTimeString()),rc,1);
	gl->addWidget(new QLabel(tr("Capacity:")),++rc,0);
	gl->addWidget(new QLabel(QString::number(event.capacity())),rc,1);
	gl->addWidget(new QLabel(tr("Tickets currently reserved:")),++rc,0);
	gl->addWidget(new QLabel(QString::number(nreserved)),rc,1);
	gl->addWidget(new QLabel(tr("Tickets currently cancelled:")),++rc,0);
	gl->addWidget(new QLabel(QString::number(ncancelled)),rc,1);
	gl->addWidget(new QLabel(tr("Tickets currently usable:")),++rc,0);
	gl->addWidget(new QLabel(QString::number(ntotaltickets)),rc,1);
	gl->addWidget(new QLabel(tr("Total Income:")),++rc,0);
	gl->addWidget(new QLabel(cent2str(ntotalmoney)),rc,1);
	
	QTableView*table;
	QStandardItemModel*model;
	tab->addTab(table=new QTableView,tr("Tickets"));
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	table->setModel(model=new QStandardItemModel(this));
	model->insertColumns(0,5);
	model->insertRows(0,tickets.size());
	model->setHorizontalHeaderLabels(QStringList() <<tr("Price") <<tr("Category") <<tr("Bought") <<tr("Used") <<tr("Unused"));
	for(int i=0;i<tickets.size();i++){
		Tickets tc=tickets[i];
		model->setData(model->index(i,0),cent2str(tc.price));
                model->setData(model->index(i,1),tc.category);
                model->setData(model->index(i,2),tc.bought);
		model->setData(model->index(i,3),tc.used);
		model->setData(model->index(i,4),tc.unused);
	}
	table->resizeColumnsToContents();
	
	QTextBrowser *tb;
	tab->addTab(tb=new QTextBrowser,tr("Comments"));
	QString cmt;
	for(int i=0;i<comments.size();i++){
		Comment c=comments[i];
		if(cmt=="")cmt="<html><body>";
		else cmt+="<hr/>";
		cmt+="<b>"+tr("Order: ");
		cmt+=QString::number(c.orderid);
		cmt+="</b><br/><b>"+tr("Customer: ")+htmlize(c.custname);
		cmt+="</b><p/>"+htmlize(c.comment);
	}
	tb->setHtml(cmt);
	
	QHBoxLayout*hl;
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Print")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(print()));
	hl->addWidget(p=new QPushButton(tr("Save as...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(saveas()));
	hl->addSpacing(15);
	hl->addWidget(p=new QPushButton(tr("Close")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
}

MEventSummary::~MEventSummary(){}

void MEventSummary::setTestTemplateFile(const QString& t)
{
	testTemplate=t;
}

void MEventSummary::getSummaryData()
{
	MTGetEventSummary ges=req->queryGetEventSummary(eventid);
	if(ges.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while retrieving data: %1").arg(ges.errorString()));
		return;
	}
	event=ges.getevent();
	QList<MOOrder>ord=ges.getorders();
	
	nreserved=ncancelled=ntotaltickets=ntotalmoney=0;
	QMap<QPair<int,QString>,Tickets>maptickets;

	//parse orders
	for(int i=0;i<ord.size();i++){
		//remember the order
		orders.insert(ord[i].orderid().value(),ord[i]);
		orderids<<ord[i].orderid();
		//parse tickets
		QList<MOTicket>ticks=ord[i].tickets();
		for(int j=0;j<ticks.size();j++){
			//ignore foreign tickets
			if(ticks[j].eventid()!=eventid)
				continue;
			//count accumulated prices
			const int p=ticks[j].price();
                        const QString c=ticks[j].priceCategoryName();
                        const QPair<int,QString>pkey(p,c);
			if(!maptickets.contains(pkey))
				maptickets.insert(pkey,Tickets(p,c));
			//count up
			if(ticks[j].status().value()&MOTicket::MaskBlock){
				ntotaltickets++;
				ntotalmoney+=ticks[j].price();
				maptickets[pkey].bought++;
				if(ticks[j].status()==MOTicket::Used)
					maptickets[pkey].used++;
				else
					maptickets[pkey].unused++;
			}
			if(ticks[j].status()==MOTicket::Reserved)
				nreserved++;
			if(ticks[j].status()==MOTicket::Cancelled)
				ncancelled++;
		}
		//register comments
		QString cs=ord[i].comments().value().trimmed();
		if(cs!=""){
			Comment c;
			c.custid=ord[i].customerid();
			c.custname=ord[i].customer().value().fullName();
			c.orderid=ord[i].orderid();
			c.comment=cs;
			comments.append(c);
		}
	}
	tickets=maptickets.values();
	qSort(orderids);
}

void MEventSummary::print()
{
	MTemplate tf;
	if(testTemplate.isEmpty())
		tf=req->templateStore()->getTemplate("eventsummary");
	else
		tf.setFile(testTemplate);
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (eventsummary). Giving up."));
		return;
	}
	MOdtSignalRenderer rend(tf);
	connect(&rend,SIGNAL(getVariable(QString,QVariant&)), this,SLOT(getVariable(QString,QVariant&)));
	connect(&rend,SIGNAL(getLoopIterations(QString,int&)), this,SLOT(getLoopIterations(QString,int&)));
	connect(&rend,SIGNAL(setLoopIteration(QString,int)), this,SLOT(setLoopIteration(QString,int)));
	rend.renderToPrinter();
}

void MEventSummary::saveas()
{
	MTemplate tf;
	if(testTemplate.isEmpty())
		tf=req->templateStore()->getTemplate("eventsummary");
	else
		tf.setFile(testTemplate);
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (eventsummary). Giving up."));
		return;
	}
	QFileDialog fd(this);
	fd.setAcceptMode(QFileDialog::AcceptSave);
	fd.setFileMode(QFileDialog::AnyFile);
	fd.setConfirmOverwrite(true);
	fd.setNameFilter(tr("Open Document File (*.%1)").arg(tf.targetExtension()));
	fd.setDefaultSuffix(tf.targetExtension());
	fd.setDirectory(currentDir());
	QString fname;
	if(fd.exec()){
		QStringList fn=fd.selectedFiles();
		if(fn.size()<1)return;
		fname=fn[0];
		setCurrentDir(fname);
	}
	MOdtSignalRenderer rend(tf);
	connect(&rend,SIGNAL(getVariable(QString,QVariant&)), this,SLOT(getVariable(QString,QVariant&)));
	connect(&rend,SIGNAL(getLoopIterations(QString,int&)), this,SLOT(getLoopIterations(QString,int&)));
	connect(&rend,SIGNAL(setLoopIteration(QString,int)), this,SLOT(setLoopIteration(QString,int)));
	rend.renderToFile(fname);
}

void MEventSummary::getVariable(QString varname,QVariant&value)
{
	if(varname=="TITLE")
		value=event.title().value();
	else
	if(varname=="ARTIST")
		value=event.artist().value().name().value();
	else
	if(varname=="ROOM")
		value=event.room().value();
	else
	if(varname=="START"){
		value=event.start().value();
	}else
	if(varname=="CAPACITY"){
		value=event.capacity().value();
	}else
	if(varname=="RESERVED"){
		value=nreserved;
	}else
	if(varname=="BOUGHT"){
		value=ntotaltickets;
	}else
	if(varname=="USED"){
		int nused=0;
		for(int i=0;i<tickets.size();i++)nused+=tickets[i].used;
		value=nused;
	}else
	if(varname=="UNUSED"){
		int nunused=0;
		for(int i=0;i<tickets.size();i++)nunused+=tickets[i].unused;
		value=nunused;
	}else
	if(varname=="CANCELLED"){
		value=ncancelled;
	}else
	if(varname=="TOTALMONEY"){
		value=ntotalmoney;
	}else
	if(varname=="TICKETS"){
		value=tickets.size();
	}else
	if(varname=="COMMENTS"){
		value=comments.size();
	}else
	if(varname=="ORDERS"){
		value=orderids.size();
	}else
	if(varname=="EVENTPRICE"){
		value=/*event.defaultprice().value()*/(int)1;
	}else{
		if(varname.contains(':')){
			QStringList sl=varname.split(':');
			if(!loopiter.contains(sl[0]))return;
			getLoopVariable(sl[0],loopiter[sl[0]],sl[1],value);
		}
	}
}

void MEventSummary::getLoopIterations(QString loopname,int&iterations)
{
	if(loopname=="TICKETS")iterations=tickets.size();else
	if(loopname=="COMMENTS")iterations=comments.size();else
	if(loopname=="ORDERS")iterations=orderids.size();
}

void MEventSummary::setLoopIteration(QString lv, int it)
{
	if(it<0)return;
	int max=-1;
	getLoopIterations(lv,max);
	if(it>=max)return;
	loopiter.insert(lv,it);
}

void MEventSummary::getLoopVariable(QString loopname,int iteration,QString varname,QVariant&value)
{
	if(loopname=="TICKETS"){
		if(iteration<0 || iteration>=tickets.size())return;
		
		if(varname=="PRICE"){
			value=tickets[iteration].price;
		}else
		if(varname=="BOUGHT"){
			value=tickets[iteration].bought;
		}else
		if(varname=="USED"){
			value=tickets[iteration].used;
		}else
		if(varname=="UNUSED"){
			value=tickets[iteration].unused;
		}
	}else
	if(loopname=="COMMENTS"){
		if(iteration<0 || iteration>=comments.size())return;
	
		if(varname=="ORDERID")value=QString::number(comments[iteration].orderid);else
		if(varname=="CUSTOMERID")value=QString::number(comments[iteration].custid);else
		if(varname=="CUSTOMER")value=comments[iteration].custname;else
		if(varname=="TEXT")value=comments[iteration].comment;
	}else
	if(loopname=="ORDERS"){
		if(iteration<0 || iteration>=orderids.size())return;
		//get order id
		int oid=orderids[iteration];
		
		if(varname=="ORDERID"){
			value=QString::number(oid);
			return;
		}
		
		//paranoia check
		if(!orders.contains(oid))return;
		
		if(varname=="CUSTOMERID")value=QString::number(orders[oid].orderid());else
		if(varname=="CUSTOMER")value=orders[oid].customer().value().fullName();else
		if(varname=="FULLPRICE"){
			value=orders[oid].totalprice().value();
		}else
		if(varname=="SHIPPING")value=orders[oid].shippingtype().value().description().value();else
		if(varname=="SHIPPINGCOST"){
			value=orders[oid].shippingcosts().value();
		}else{
			QList<MOTicket>ticks=orders[oid].tickets();
			QList<MOTicket>eticks;
			int first,last;
			first=last=event.start();
			int eid=event.eventid();
			for(int i=0;i<ticks.size();i++)
				if(eid==ticks[i].eventid() && ticks[i].status()&MOTicket::MaskPay)
					eticks.append(ticks[i]);
			if(varname=="SHIPPINGCOSTWEIGHTED"){
				//get amount of usable tickets
				int ua=0;
				for(int i=0;i<ticks.size();i++)
					if(ticks[i].status()&MOTicket::MaskPay)
						ua++;
				//calculate weighted amount
				value=orders[oid].shippingcosts()*eticks.size()/ua;
			}else
			if(varname=="SHIPPINGCOSTIFFIRST"){
				if(first==event.start())
					value=orders[oid].shippingcosts().value();
				else
					value=0;
			}else
			if(varname=="SHIPPINGCOSTIFLAST"){
				if(last==event.start())
					value=orders[oid].shippingcosts().value();
				else
					value=0;
			}else
			if(varname=="NUMTICKETS"){
				value=eticks.size();
			}else
			if(varname=="TICKETPRICE"){
				int prc=0;
				for(int i=0;i<eticks.size();i++)
					prc+=eticks[i].price();
				value=prc;
			}
		}
	}
}
