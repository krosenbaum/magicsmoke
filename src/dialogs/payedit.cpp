//
// C++ Implementation: payment types
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2011-2012
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "misc.h"
#include "payedit.h"
#include "centbox.h"
#include "flagedit.h"

#include "msinterface.h"
#include "MTSetPaymentType"
#include "MTDeletePaymentType"
#include "MTSetDefaultPaymentType"
#include "MTGetPaymentTypes"

#include <QApplication>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QStandardItemModel>
#include <QTableView>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QBoxLayout>
#include <QComboBox>

static QList<MOPaymentType> getPayTypeCache(bool force=false);
static QString getDefaultPayType(bool force=false);
static void invalidatePayTypeCache();

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// payment type editor

MPaymentEditor::MPaymentEditor(QWidget*par)
	:QDialog(par)
{
	all=getPayTypeCache(true);
	setWindowTitle(tr("Edit Payment Options"));
	setSizeGripEnabled(true);
	
	QHBoxLayout*hl,*hl2;
	QVBoxLayout*vl,*vl2;
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout,1);
	hl->addLayout(vl2=new QVBoxLayout);
	vl2->addWidget(table=new QTableView,1);
	table->setModel(model=new QStandardItemModel(this));
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	table->verticalHeader()->hide();
	updateTable();
	vl2->addLayout(hl2=new QHBoxLayout);
	hl2->addWidget(new QLabel(tr("Default Payment Type:")));
	hl2->addWidget(deftype=new QLabel(getDefaultPayType()),1);
	hl->addLayout(vl2=new QVBoxLayout,0);
	QPushButton*p;
	bool b=req->hasRight(req->RSetPaymentType);
	vl2->addWidget(p=new QPushButton(tr("Change Description")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeDescription()));
	p->setEnabled(b);
	vl2->addWidget(p=new QPushButton(tr("Change Data")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeData()));
	p->setEnabled(b);
	vl2->addWidget(p=new QPushButton(tr("Change Flags")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeAvail()));
	p->setEnabled(b);
	vl2->addWidget(p=new QPushButton(tr("Change Default")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeDefault()));
	p->setEnabled(req->hasRight(req->RSetDefaultPaymentType));
	vl2->addSpacing(20);
	vl2->addWidget(p=new QPushButton(tr("Add Option")));
	connect(p,SIGNAL(clicked()),this,SLOT(addNew()));
	p->setEnabled(b);
	vl2->addWidget(p=new QPushButton(tr("Delete Option")));
	connect(p,SIGNAL(clicked()),this,SLOT(deletePay()));
	p->setEnabled(req->hasRight(req->RDeletePaymentType));
	vl2->addStretch(1);
	
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Close")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
}
void MPaymentEditor::updateTable()
{
	model->clear();
	model->insertColumns(0,5);
	model->insertRows(0,all.size());
	model->setHorizontalHeaderLabels(QStringList()<<tr("ID")<<tr("Description")<<tr("Data Name")<<tr("Data Default")<<tr("Flags"));
	for(int i=0;i<all.size();i++){
		model->setData(model->index(i,0),all[i].name().value());
		model->setData(model->index(i,1),all[i].description().value());
		model->setData(model->index(i,2),all[i].dataname().value());
		model->setData(model->index(i,3),all[i].datapreset().value());
		model->setData(model->index(i,4),all[i].flags().value());
	}
	table->resizeColumnsToContents();
}

void MPaymentEditor::changeDescription()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get paytype
	MOPaymentType s=all[idx.row()];
	//get new value
	QString r=QInputDialog::getText(this,tr("Payment Option Description"),tr("Please select a new description for this payment option:"),QLineEdit::Normal,s.description());
	if(r=="")return;
	s.setdescription(r);
	MTSetPaymentType cs=MTSetPaymentType::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes: %1").arg(cs.errorString()));
		return;
	}
	all[idx.row()]=cs.getpaytype();
	updateTable();
	invalidatePayTypeCache();
}

void MPaymentEditor::changeData()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get paytype
	MOPaymentType s=all[idx.row()];
	//get new values
	QDialog d(this);
	d.setWindowTitle(tr("Change Payment Data"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);
	QLineEdit*dname,*dpat;
	fl->addRow(tr("Data Name (human readable):"),dname=new QLineEdit);
	dname->setText(s.dataname());
	fl->addRow(tr("Data Default (pattern):"),dpat=new QLineEdit);
	dpat->setText(s.datapreset());
	vl->addWidget(new QLabel(tr("Hint: %Y=year, %M=month, %D=day, %%=%-sign, %O=order ID, %U=user")));
	vl->addStretch(1);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//wait for user
	if(d.exec()!=QDialog::Accepted)return;
	s.setdataname(dname->text());
	s.setdatapreset(dpat->text());
	//query server
	MTSetPaymentType cs=MTSetPaymentType::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes: %1").arg(cs.errorString()));
		return;
	}
	all[idx.row()]=cs.getpaytype();
	updateTable();
	invalidatePayTypeCache();
}
void MPaymentEditor::changeAvail()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get paytype
	MOPaymentType s=all[idx.row()];
	//get new value
	s.setflags(MFlagEditor::edit(this,s.flags(),tr("Edit Flags of payment option '%1'.").arg(s.description())));
	//save
	MTSetPaymentType cs=MTSetPaymentType::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes."));
		return;
	}
	all[idx.row()]=s;
	updateTable();
	invalidatePayTypeCache();
}

void MPaymentEditor::changeDefault()
{
	//dialog
	QDialog d(this);
	d.setWindowTitle(tr("Set Default Payment Type"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel("Please chose a default payment type:"));
	QComboBox*box;
	vl->addWidget(box=new QComboBox);
	box->setModel(model);
	QString cdef=deftype->text();
	for(int i=0;i<model->rowCount();i++){
		if(model->data(model->index(i,0)).toString()==cdef)
			box->setCurrentIndex(i);
	}
	vl->addSpacing(15);
	vl->addStretch(1);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	//get the new default
	QString ndef=box->currentText();
	if(ndef==cdef)return;
	//check for flags
	if(model->data(model->index(box->currentIndex(),4)).toString().trimmed()!="")
		QMessageBox::warning(this, tr("Warning"), tr("The payment type '%1' has flags set, it may not be usable for every user, please consider removing those flags.").arg(ndef));
	//actually set it
	MTSetDefaultPaymentType dpt=req->querySetDefaultPaymentType(ndef);
	if(dpt.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Unable to set the new default: %1").arg(dpt.errorString()));
	else{
		deftype->setText(ndef);
		invalidatePayTypeCache();
	}
}

void MPaymentEditor::addNew()
{
	//get new values
	QDialog d(this);
	d.setWindowTitle(tr("Create new Payment Option"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);
	QLineEdit*dname,*dpat,*name,*descr;
	fl->addRow(tr("Payment Type Name:"),name=new QLineEdit);
	name->setValidator(new QRegExpValidator(QRegExp(tr("[a-zA-Z-/_\\(\\),\\.]{1,63}","payment type pattern - allow national chars!")),&d));
	fl->addRow(tr("Description:"),descr=new QLineEdit);
	fl->addRow(tr("Data Name (human readable):"),dname=new QLineEdit);
	fl->addRow(tr("Data Default (pattern):"),dpat=new QLineEdit);
	fl->addRow(new QLabel(tr("Hint: %Y=year, %M=month, %D=day, %H=hour(0..23), %h=hour(1..12), %m=minute, %a=AM/PM, %%=%-sign")));
	MFlagWidget*flgw;
	fl->addRow(tr("Flags:"),flgw=new MFlagWidget);
	flgw->setEditorLabel(tr("Edit flags of the new payment option:"));
	vl->addStretch(1);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//wait for user
	if(d.exec()!=QDialog::Accepted)return;
	MOPaymentType s;
	s.setname(name->text());
	s.setdescription(descr->text());
	s.setdataname(dname->text());
	s.setdatapreset(dpat->text());
	s.setflags(flgw->flags());
	//query server
	MTSetPaymentType cs=MTSetPaymentType::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes: %1").arg(cs.errorString()));
		return;
	}
	all.append(cs.getpaytype());
	updateTable();
	invalidatePayTypeCache();
}

void MPaymentEditor::deletePay()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//ask
	MOPaymentType s=all[idx.row()];
	if(QMessageBox::question(this,tr("Really Delete?"),tr("Really delete payment option '%1'?").arg(s.description()),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)
		return;
	//get payment type
	MTDeletePaymentType ds=MTDeletePaymentType::query(s.name());
	if(ds.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to delete this option: %1").arg(ds.errorString()));
		return;
	}
	all.removeAt(idx.row());
	updateTable();
	invalidatePayTypeCache();
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MPaymentDialog::MPaymentDialog(QWidget* parent, Qt::WindowFlags f): QDialog(parent, f)
{
	setWindowTitle(tr("Payment"));
	QFormLayout*fl;
	setLayout(fl=new QFormLayout);
	fl->addRow(new QLabel(tr("Please enter the payment data below.")));
	fl->addRow(tr("Amount paid:"),m_cent=new MCentSpinBox);
	fl->addRow(tr("Payment Type:"),m_ptype=new QComboBox);
	connect(m_ptype,SIGNAL(currentIndexChanged(int)),this,SLOT(setPType()));
	fl->addRow(m_dlabel=new QLabel(tr("Data?:")),m_data=new QLineEdit);
	fl->addRow(new QLabel("  "));
	QHBoxLayout*hl;
	fl->addRow(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
	setSizeGripEnabled(true);
	
	//preset ptype
	QList<MOPaymentType>pt=getPayTypeCache();
	QString dp=getDefaultPayType();
	int idx=0;
	foreach(MOPaymentType p,pt){
		if(req->checkFlags(p.flags())){
			m_ptype->addItem(p.description(),QVariant::fromValue<MOPaymentType>(p));
			if(p.name()==dp)idx=m_ptype->count()-1;
		}
	}
	m_ptype->setCurrentIndex(idx);
	
	//update
	setPType();
}

void MPaymentDialog::setMaximum(qint64 m)
{
	m_cent->setRange(0,m);
}

void MPaymentDialog::setValue(qint64 v)
{
	m_cent->setValue(v);
}

qint64 MPaymentDialog::value() const
{
	return m_cent->value();
}

QString MPaymentDialog::paytype() const
{
	return m_ptype->itemData(m_ptype->currentIndex()).value<MOPaymentType>().name();
}

QString MPaymentDialog::paydata() const
{
	if(m_data->isEnabled())return m_data->text();
	else return QString();
}

void MPaymentDialog::setPType()
{
	//get the data
	MOPaymentType pt=m_ptype->itemData(m_ptype->currentIndex()).value<MOPaymentType>();
	//do we have a data name?
	if(pt.dataname().value().trimmed().isEmpty()){
		m_dlabel->setText(tr("(not required)"));
		m_data->setText("");
		m_data->setEnabled(false);
                m_data->setPlaceholderText(tr("(not required)"));
		return;
	}
	//normal setting
	m_dlabel->setText(pt.dataname()+":");
	m_data->setEnabled(true);
	//calculate and set pattern
	QString dtxt,dpat=pt.datapreset();
	bool isperc=false;
	foreach(QChar c,dpat){
		if(isperc){
			switch(c.toLatin1()){
				case '%':dtxt+='%';break;
				case 'Y':dtxt+=QString::number(QDate::currentDate().year());break;
				case 'M':dtxt+=QDate::currentDate().toString("MM");break;
				case 'D':dtxt+=QDate::currentDate().toString("dd");break;
				case 'H':dtxt+=QTime::currentTime().toString("HH");break;
				case 'h':dtxt+=QTime::currentTime().toString("hh ap").split(" ").at(0);break;
				case 'm':dtxt+=QTime::currentTime().toString("mm");break;
				case 'a':dtxt+=QTime::currentTime().toString("ap");break;
				default:dtxt+='%';dtxt+=c;break;
			}
			isperc=false;
		}else{
			if(c=='%')isperc=true;
			else dtxt+=c;
		}
	}
	if(isperc)dtxt+='%';
	m_data->setText(dtxt);
        m_data->setPlaceholderText(dtxt);
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// cache of payment types, used by payment dialog

static QList<MOPaymentType> c_paytypes;
static QString c_defpay;
static QDateTime c_nextupdate;

static QList<MOPaymentType> getPayTypeCache(bool force)
{
	if(!force){
		if(!c_nextupdate.isValid() || c_nextupdate <= QDateTime::currentDateTime())
			force=true;
	}
	if(!force)return c_paytypes;
	//get data
	MTGetPaymentTypes gpt=req->queryGetPaymentTypes();
	if(gpt.hasError())
		qDebug()<<"Warning: unable to fetch payment type data:"<<gpt.errorString();
	else{
		c_paytypes=gpt.getpaytypes();
		c_defpay=gpt.getdefaultpaytype();
		c_nextupdate=QDateTime::currentDateTime().addSecs(300);
	}
	//return it
	return c_paytypes;
}
static QString getDefaultPayType(bool force)
{
	//use above for getting data
	getPayTypeCache(force);
	//return data
	return c_defpay;
}
static void invalidatePayTypeCache()
{
	c_nextupdate=QDateTime();
}
