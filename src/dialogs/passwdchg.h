//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_PASSWDCHG_H
#define MAGICSMOKE_PASSWDCHG_H

#include <QDialog>

class QLabel;
class QLineEdit;
class QPushButton;

/**Helper dialog for changing passwords*/
class MPasswordChange:public QDialog
{
	Q_OBJECT
	public:
		/**creates a password change dialog, if user is empty it is presumed to change own password*/
		MPasswordChange(QWidget*parent,QString user=QString());
		
		/**if own password: return old password*/
		QString oldPassword();
		/**returns new password, or empty if the two lines don't match*/
		QString newPassword();
	private:
		QLineEdit *oldpwd,*newpwd1,*newpwd2;
};

#endif
