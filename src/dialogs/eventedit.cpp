//
// C++ Implementation: eventedit
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2017
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "eventedit.h"

#include <TimeStamp>

#include <QBoxLayout>
#include <QCheckBox>
#include <QDateTimeEdit>
#include <QDebug>
#include <QFileDialog>
#include <QFormLayout>
#include <QGridLayout>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextEdit>
#include <QTimer>

#include "msinterface.h"
#include "MTGetEvent"
#include "MTCreateEvent"
#include "MTChangeEvent"
#include "MTGetAllRooms"
#include "MTCreateRoom"
#include "MTGetAllArtists"
#include "MTCreateArtist"
#include "MTGetAllSeatPlans"
#include "MTCreateSeatPlan"
#include "MTUpdateSeatPlan"

#include "centbox.h"
#include "pricecatdlg.h"
#include "formula.h"
#include "flagedit.h"
#include "misc.h"

MEventEditor::MEventEditor(QWidget*pw, OpenMode mode, qint64 id)
	:QDialog(pw)
{
	if(id>=0){
		MTGetEvent ge=MTGetEvent::query(id);
		if(ge.stage()!=ge.Success){
			QMessageBox::warning(this,tr("Warning"),tr("Unable to load event from server."));
			//make myself disappear immediately
			QTimer::singleShot(1,this,SLOT(reject()));
			//no point in setting up widgets
			return;
		}
		event=ge.getevent().value();
	}
	switch(mode){
		case OpenMode::Auto:
			if(id>=0)break;
			//else fallthru
		case OpenMode::Create:{
			event.setid(Nullable<qint64>());
			auto prices=event.price();
			for(auto&price:prices){
				price.setamounttickets(0);
				price.setamountticketsblock(0);
			}
			event.setprice(prices);
			break;
		}
		default:break;
	}

	setWindowTitle(tr("Event Editor"));
	
	QGridLayout*gl;
	QVBoxLayout*vl,*vl2;
	QHBoxLayout*hl;
	QPushButton*p;
	int lctr=0;
	QLabel*lab;
	QTabWidget*tab;
	QWidget*w;
	
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(tab=new QTabWidget,10);
	
	tab->addTab(w=new QWidget,tr("Event"));
	w->setLayout(gl=new QGridLayout);
	
	gl->addWidget(lab=new QLabel(tr("ID:")),lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(eventid=new QLabel,lctr,1);
	eventid->setFrameStyle(QFrame::StyledPanel|QFrame::Sunken);
	eventid->setText(event.id().isNull()?"new":QString::number(event.id()));
	
	gl->addWidget(lab=new QLabel(tr("Title:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(title=new QLineEdit,lctr,1);
	title->setText(event.title());
	
	gl->addWidget(lab=new QLabel(tr("Artist:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addLayout(hl=new QHBoxLayout,lctr,1);
	hl->addWidget(artist=new QLineEdit,10);
	artist->setReadOnly(true);
	artist->setText(event.artist().value().name());
	hl->addWidget(p=new QPushButton("..."),0);
	connect(p,SIGNAL(clicked()),this,SLOT(selectArtist()));
	
	gl->addWidget(lab=new QLabel(tr("Start Time:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(starttime=new QDateTimeEdit,lctr,1);
	starttime->setDisplayFormat(tr("ddd MMMM d yyyy, h:mm ap","time format"));
	starttime->setCalendarPopup(true);
	connect(starttime,SIGNAL(dateTimeChanged(const QDateTime&)),this,SLOT(startTimeChanged(const QDateTime&)));
	
	gl->addWidget(lab=new QLabel(tr("End Time:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(endtime=new QDateTimeEdit,lctr,1);
	endtime->setDisplayFormat(tr("ddd MMMM d yyyy, h:mm ap","time format"));
	endtime->setCalendarPopup(true);
	connect(endtime,SIGNAL(dateTimeChanged(const QDateTime&)),this,SLOT(endTimeChanged(const QDateTime&)));
	if(event.id().isNull() && event.start().isNull()){
		QDateTime tt(QDate::currentDate().addDays(1),QTime(20,0));//default: tomorrow 8pm
		starttime->setDateTime(tt);
		endtime->setDateTime(tt.addSecs(3600));
	}else{//valid event: use the date and time from event
		starttime->setDateTime(TimeStamp(event.start()));
		endtime->setDateTime(TimeStamp(event.end()));
	}
	
	gl->addWidget(lab=new QLabel(tr("Room/Place:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addLayout(hl=new QHBoxLayout,lctr,1);
	hl->addWidget(room=new QLineEdit,10);
	room->setReadOnly(true);
	room->setText(event.room());
	hl->addWidget(p=new QPushButton("..."),0);
	connect(p,SIGNAL(clicked()),this,SLOT(selectRoom()));
	
	gl->addWidget(lab=new QLabel(tr("Seat Plan:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addLayout(hl=new QHBoxLayout,lctr,1);
	hl->addWidget(seatplan=new QLineEdit,10);
	seatplan->setReadOnly(true);
	seatplan->setText(event.seatplan().data().name());
	hl->addWidget(p=new QPushButton("..."),0);
	connect(p,SIGNAL(clicked()),this,SLOT(selectSeatPlan()));

	gl->addWidget(lab=new QLabel(tr("Capacity:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(capacity=new QSpinBox,lctr,1);
	capacity->setRange(0,1000000000);//it is unlikely that any show will attract more people!
	capacity->setValue(event.capacity());
	
	gl->addWidget(cancelcheck=new QCheckBox(tr("Event Cancelled:")),++lctr,0);
	cancelcheck->setChecked(event.iscancelled());
	cancelcheck->setEnabled(!event.id().isNull() && req->hasRight(req->PChangeEvent_CancelEvent));
	gl->addWidget(cancelreason=new QLineEdit,lctr,1);
	cancelreason->setEnabled(event.iscancelled());
	cancelreason->setText(event.cancelreason());
	connect(cancelcheck,SIGNAL(toggled(bool)),cancelreason,SLOT(setEnabled(bool)));
	
	gl->addWidget(lab=new QLabel(tr("Flags:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
	gl->addWidget(flags=new MFlagWidget(event.flags()),lctr,1);
	flags->setEditorLabel(tr("Edit Flags of Event '%1'").arg(event.title()));
	
	tab->addTab(w=new QWidget,tr("Description"));
	w->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(new QLabel(tr("The description will be displayed on the web site, please use HTML syntax.")),0);
	vl2->addWidget(description=new QTextEdit,10);
	description->setPlainText(event.description());
	
	tab->addTab(w=new QWidget,tr("Comment"));
	w->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(new QLabel(tr("The comment is for internal use only, please add any hints relevant for your collegues.")),0);
	vl2->addWidget(comment=new QTextEdit,10);
	comment->setPlainText(event.comment());
	
	tab->addTab(w=new QWidget,tr("Prices"));
	w->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(pricetable=new QTableView,10);
	vl2->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Change Price")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(changePrice()));
	hl->addWidget(p=new QPushButton(tr("Add Price")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(addPrice()));
	hl->addWidget(p=new QPushButton(tr("Remove Price")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(removePrice()));
	pricetable->setModel(pricemodel=new QStandardItemModel(this));
	pricetable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        pricetable->setSelectionMode(QAbstractItemView::SingleSelection);
        pricetable->setSelectionBehavior(QAbstractItemView::SelectRows);
        vl2->addWidget(new QLabel(tr("Hint: Prices with the lowest Ordering number are shown first when selecting a category for tickets.")));
	updatePrice();
	
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch();
	hl->addWidget(p=new QPushButton(tr("Save")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	connect(p,SIGNAL(clicked()),this,SLOT(writeBack()));
	hl->addWidget(p=new QPushButton(tr("Close")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
	setSizeGripEnabled(true);
}

void MEventEditor::writeBack()
{
	//copy contents to event
	event.settitle(title->text());
	event.setstart(TimeStamp(starttime->dateTime(),true).toUnix());
	event.setend(TimeStamp(endtime->dateTime(),true).toUnix());
	event.setiscancelled(cancelcheck->isChecked());
	event.setcancelreason(cancelreason->text());
	event.setdescription(description->toPlainText());
	event.setcomment(comment->toPlainText());
	event.setcapacity(capacity->value());
	event.setroom(room->text());
	event.setflags(flags->text());
	if(seatplanid>=0)
		event.setseatplanid(seatplanid);
	else
		event.setseatplanid(NullValue());
	//artist is set in other methods already
	//prices are changed by the methods reacting to the table
	//send to server
	if(event.id().isNull()){
		MTCreateEvent ce=req->queryCreateEvent(event);
		if(ce.hasError())
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating event: %1").arg(ce.errorString()));
	}else{
		MTChangeEvent ce=req->queryChangeEvent(event);
		if(ce.hasError())
			QMessageBox::warning(this,tr("Warning"),tr("Error while changing event: %1").arg(ce.errorString()));
	}
}

void MEventEditor::updatePrice()
{
	pricemodel->clear();
	pricemodel->insertColumns(0,5);
	pricemodel->setHorizontalHeaderLabels(QStringList()<<tr("Price Category")<<tr("Price")<<tr("Ordering")<<tr("Ticket Capacity")<<tr("Tickets")<<tr("Seats Blocked")<<tr("Flags"));
	QList<MOEventPrice> prc=event.price();
	pricemodel->insertRows(0,prc.size());
	for(int i=0;i<prc.size();i++){
		pricemodel->setData(pricemodel->index(i,0), prc[i].pricecategory().value().name().value());
		pricemodel->setData(pricemodel->index(i,0), prc[i].pricecategoryid().value(), Qt::UserRole);
		pricemodel->setData(pricemodel->index(i,1), cent2str(prc[i].price().value()));
		pricemodel->setData(pricemodel->index(i,2), prc[i].prio().value());
		pricemodel->setData(pricemodel->index(i,3), prc[i].maxavailable().value());
		pricemodel->setData(pricemodel->index(i,4), prc[i].amounttickets().value());
		pricemodel->setData(pricemodel->index(i,5), prc[i].amountticketsblock().value());
		pricemodel->setData(pricemodel->index(i,6), prc[i].flags().value());
	}
	pricetable->resizeColumnsToContents();
}

MEEPriceEdit::MEEPriceEdit(QWidget*pw,MOEventPrice prc,int maxseats)
	:QDialog(pw)
{
	QFormLayout*gl;
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QPushButton*p;
	QDialog d(this);
	setWindowTitle(tr("Change Price"));
	setSizeGripEnabled(true);
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(gl=new QFormLayout,10);
	gl->addRow(tr("Price category:"),cname=new QLabel(prc.pricecategory().value().name()));
	cname->setFrameStyle(QFrame::StyledPanel|QFrame::Sunken);
	gl->addRow(tr("Price:"),price=new MCentSpinBox(&d,prc.price()));
	price->setMinimumWidth(200);
	gl->addRow(tr("Maximum Seats:"),cap=new QSpinBox);
	gl->addRow(tr("Ordering:"),ord=new QSpinBox);
	gl->addRow(tr("Flags:"),hl=new QHBoxLayout);
	hl->addWidget(flg=new QLabel(prc.flags()),1);
	flg->setFrameStyle(QFrame::StyledPanel|QFrame::Sunken);
	hl->addWidget(p=new QPushButton("..."),0);
	connect(p,SIGNAL(clicked(bool)),this,SLOT(setFlags()));
	cap->setRange(prc.amountticketsblock(),maxseats);
	cap->setValue(prc.maxavailable());
	ord->setRange(0,0x7fffffff);
	ord->setValue(prc.prio());
	vl->addSpacing(10);
	vl->addStretch(1);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

void MEEPriceEdit::setFlags()
{
	flg->setText(MFlagEditor::edit(this, flg->text(), tr("Edit flags of price %1:").arg(cname->text())));
}


void MEventEditor::changePrice()
{
	//get selection
	QModelIndex idx=pricetable->currentIndex();
	if(!idx.isValid())return;
	int pid=pricemodel->data(pricemodel->index(idx.row(),0),Qt::UserRole).toInt();
	int lid=-1;
	QList<MOEventPrice> prc=event.price();
	for(int i=0;i<prc.size();i++)
		if(prc[i].pricecategoryid().value()==pid){
			lid=i;
			break;
		}
	if(lid<0)return;
	//display dialog
	MEEPriceEdit d(this,prc[lid],capacity->value());
	if(d.exec()!=QDialog::Accepted)return;
	//use data
	prc[lid].setprice(d.price->value());
	prc[lid].setmaxavailable(d.cap->value());
	prc[lid].setflags(d.flg->text());
	prc[lid].setprio(d.ord->value());
	event.setprice(prc);
	updatePrice();
}

void MEventEditor::addPrice()
{
	//get category
	MPriceCategoryDialog pcd(this);
	if(pcd.exec()!=QDialog::Accepted)return;
	MOPriceCategory cat=pcd.selection();
	//sanity check on selectopm
	if(cat.pricecategoryid().isNull())return;
	for(int i=0;i<event.price().size();i++)
		if(event.price().at(i).pricecategoryid()==cat.pricecategoryid()){
			QMessageBox::warning(this,tr("Warning"),tr("Price category already exists in this event."));
			return;
		}
	//convert category into price
	MOEventPrice ep;
	ep.setpricecategory(cat);
	ep.setpricecategoryid(cat.pricecategoryid());
	ep.setflags(cat.flags());
	//execute formula
	MElamEngine ee;
	QList<MOEventPrice> prc=event.price();
	for(int i=0;i<prc.size();i++){
		ee.setVariable(prc[i].pricecategory().value().abbreviation(), prc[i].price().value());
	}
	ep.setprice(ee.evaluate(cat.formula()).toInt());
	//get local properties
	MEEPriceEdit d(this,ep,capacity->value());
	if(d.exec()!=QDialog::Accepted)return;
	//use data
	ep.setprice(d.price->value());
	ep.setmaxavailable(d.cap->value());
	ep.setprio(d.ord->value());
	ep.setflags(d.flg->text());
	event.setprice(event.price()<<ep);
	updatePrice();
}

void MEventEditor::removePrice()
{
	//get selection
	QModelIndex idx=pricetable->currentIndex();
	if(!idx.isValid())return;
	int pid=pricemodel->data(pricemodel->index(idx.row(),0),Qt::UserRole).toInt();
	int lid=-1;
	QList<MOEventPrice> prc=event.price();
	for(int i=0;i<prc.size();i++)
		if(prc[i].pricecategoryid().value()==pid){
			lid=i;
			break;
		}
	if(lid<0)return;
	//check
	if(prc[lid].amounttickets().value()>0){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot remove price '%1' - it has tickets in the database.").arg(prc[lid].pricecategory().value().name().value()));
		return;
	}
	//remove
	prc.removeAt(lid);
	event.setprice(prc);
	updatePrice();
}

void MEventEditor::selectRoom()
{
	QList<MORoom>rlst=req->queryGetAllRooms().getrooms();
	QDialog d;
	d.setWindowTitle(tr("Select a Room"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QListWidget*rlstw;
	vl->addWidget(rlstw=new QListWidget,10);
	for(int i=0;i<rlst.size();i++)
		rlstw->addItem(rlst[i].id());
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("New...","new room")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newRoom()));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	p->setEnabled(req->hasRight(req->RCreateRoom));
	hl->addWidget(p=new QPushButton(tr("Select","select room")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()==QDialog::Rejected)return;
	//get selection
	QList<QListWidgetItem*>wlst=rlstw->selectedItems();
	if(wlst.size()<1)return;
	room->setText(wlst[0]->text());
}

void MEventEditor::selectSeatPlan()
{
	QList<MOSeatPlanInfo>rlst=req->queryGetAllSeatPlans().getplans();
	QDialog d(this);
	d.setWindowTitle(tr("Select a Seat Plan"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QListView*rlstw;
	QStandardItemModel model;
	vl->addWidget(rlstw=new QListView,10);
	rlstw->setModel(&model);
	model.insertColumns(0,1);
	model.insertRows(0,rlst.size());
	qDebug()<<"plans:"<<rlst.size();
	for(int i=0;i<rlst.size();i++){
		qDebug()<<"plan"<<rlst[i].name().data();
		const QModelIndex idx=model.index(i,0);
		model.setData(idx,rlst[i].name().data());
		model.setData(idx,rlst[i].seatplanid().data(),Qt::UserRole);
	}
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("New...","new seatplan")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newSeatPlan()));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	p->setEnabled(req->hasRight(req->RCreateSeatPlan));
	hl->addWidget(p=new QPushButton(tr("Select","select seatplan")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Remove Seat Plan","remove seatplan from event")),0);
	connect(p,&QPushButton::clicked,&d,[&](){
		seatplanid=-1;
		seatplan->setText(QString());
		d.reject();
	});
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()==QDialog::Rejected)return;
	//get selection
	const QModelIndex cidx=rlstw->currentIndex();
	if(!cidx.isValid())return;
	bool b=false;
	seatplanid=model.data(cidx,Qt::UserRole).toInt(&b);
	if(b)
		seatplan->setText(model.data(cidx).toString());
	else{
		seatplan->setText(QString());
		seatplanid=-1;
	}
}

void MEventEditor::newRoom()
{
	//TODO: do more intelligent input for new room
	QString rid=QInputDialog::getText(this,tr("New Room"),tr("Name of new room:"));
	if(rid!=""){
		MTCreateRoom cr=req->queryCreateRoom(rid,0,rid);
		if(cr.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating new room: %1").arg(cr.errorString()));
			return;
		}
		room->setText(rid);
	}
}

void MEventEditor::newSeatPlan()
{
	if(room->text().isEmpty()){
		QMessageBox::warning(this,tr("Warning"),tr("Please select a room first, since a seat plan must have a room."));
		return;
	}
	QString fname=QFileDialog::getOpenFileName(this,tr("Please select a seat plan file"));
	if(fname.isEmpty())return;
	QString plan;
	QFile fd(fname);
	if(!fd.open(QIODevice::ReadOnly)){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot open seat plan file '%1'.").arg(fname));
		return;
	}
	//TODO: fix detection of encoding
	plan=QString::fromUtf8(fd.readAll());
	fd.close();
	QString name=QInputDialog::getText(this,tr("Seat Plan Name"),tr("Please enter a name for your seat plan:"));
	if(name.isEmpty())return;
	MOSeatPlanInfo sp;
	sp.setname(name);
	sp.setroomid(room->text());
	sp.setplan(plan);
	MTCreateSeatPlan csp=req->queryCreateSeatPlan(sp);
	if(csp.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while creating seat plan: %1").arg(csp.errorString()));
		return;
	}
	seatplanid=csp.getplan().value().seatplanid();
	seatplan->setText(csp.getplan().value().name());
}

void MEventEditor::selectArtist()
{
	QList<MOArtist>rlst=req->queryGetAllArtists().getartists();
	QDialog d;
	d.setWindowTitle(tr("Select an Artist"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QListWidget*rlstw;
	vl->addWidget(rlstw=new QListWidget,10);
	for(int i=0;i<rlst.size();i++){
		QListWidgetItem*it=new QListWidgetItem(rlst[i].name());
		it->setData(Qt::UserRole,i);
		rlstw->addItem(it);
	}
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("New...","new artist")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newArtist()));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	p->setEnabled(req->hasRight(req->RCreateArtist));
	hl->addWidget(p=new QPushButton(tr("Select","select artist")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()==QDialog::Rejected)return;
	//get selection
	QList<QListWidgetItem*>wlst=rlstw->selectedItems();
	if(wlst.size()<1)return;
	artist->setText(wlst[0]->text());
	event.setartist(rlst[wlst[0]->data(Qt::UserRole).toInt()]);
	event.setartistid(rlst[wlst[0]->data(Qt::UserRole).toInt()].id());
}

void MEventEditor::newArtist()
{
	//TODO: do more intelligent input for new artist
	QString rid=QInputDialog::getText(this,tr("New Artist"),tr("Name of new artist:"));
	if(rid!=""){
		MTCreateArtist cr=req->queryCreateArtist(rid,"","");
		if(cr.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating new artist: %1").arg(cr.errorString()));
			return;
		}
		artist->setText(rid);
		event.setartist(cr.getartist());
		event.setartistid(cr.getartist().value().id());
	}
}

void MEventEditor::startTimeChanged(const QDateTime&st)
{
	QDateTime et=endtime->dateTime();
	if(et<st)endtime->setDateTime(st);
}

void MEventEditor::endTimeChanged(const QDateTime&et)
{
	QDateTime st=starttime->dateTime();
	if(st>et)starttime->setDateTime(et);
}
