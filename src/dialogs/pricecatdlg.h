//
// C++ Interface: pricecatdlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_PRICECATDLG_H
#define MAGICSMOKE_PRICECATDLG_H

#include <QDialog>

#include "MOPriceCategory"

class QListWidget;

/**shows a list of price categories, allows to edit them, returns the one selected*/
class MPriceCategoryDialog:public QDialog
{
	Q_OBJECT
	public:
		MPriceCategoryDialog(QWidget*p=0,bool showselect=true);
		
		/**returns the current selection*/
		MOPriceCategory selection()const;
	private:
		QListWidget*m_list;
		QList<MOPriceCategory>m_cat;
	private slots:
		/**internal: called to create a new category*/
		void newCat();
		/**internal: called to change a category*/
		void editCat();
};

class QLineEdit;
class QLabel;

/**helper class: edit a price category*/
class MPCDEdit:public QDialog
{
	Q_OBJECT
	public:
		MPCDEdit(QWidget*,const MOPriceCategory&);
		MOPriceCategory result()const;
	private slots:
		void editFlags();
	private:
		MOPriceCategory cat;
		QLineEdit*name,*abbr,*form;
		QLabel*flags;
};

#endif
