//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "passwdchg.h"

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>


MPasswordChange::MPasswordChange(QWidget*par,QString user)
	:QDialog(par)
{
	oldpwd=newpwd1=newpwd2=0;
	if(user=="")
		setWindowTitle(tr("Change my password"));
	else
		setWindowTitle(tr("Reset password of user \"%1\"").arg(user));
	QGridLayout*gl;
	setLayout(gl=new QGridLayout);
	if(user==""){
		gl->addWidget(new QLabel(tr("Old Password:")),0,0);
		gl->addWidget(oldpwd=new QLineEdit,0,1);
		oldpwd->setEchoMode(QLineEdit::Password);
	}
	gl->addWidget(new QLabel(tr("New Password:")),1,0);
	gl->addWidget(newpwd1=new QLineEdit,1,1);
	newpwd1->setEchoMode(QLineEdit::Password);
	gl->addWidget(new QLabel(tr("Repeat Password:")),2,0);
	gl->addWidget(newpwd2=new QLineEdit,2,1);
	newpwd2->setEchoMode(QLineEdit::Password);
	
	QHBoxLayout*hl;
	gl->addLayout(hl=new QHBoxLayout,3,0,1,2);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Set Password")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

QString MPasswordChange::oldPassword()
{
	if(oldpwd)return oldpwd->text();
	return "";
}

QString MPasswordChange::newPassword()
{
	if(newpwd1==0 || newpwd2==0)return "";
	if(newpwd1->text()!=newpwd2->text())return "";
	return newpwd1->text();
}
