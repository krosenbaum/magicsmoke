//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "backupdlg.h"

#include "centbox.h"
#include "msinterface.h"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>


MBackupDialog::MBackupDialog(QWidget*par,QString pk)
	:QDialog(par),profilekey(pk)
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	QString path=set.value("backupfile",req->dataDir()+"/backup").toString();
	int gens=set.value("backupgenerations",3).toInt();
	int tm=set.value("backupinterval",0).toInt();
	//dialog
	setWindowTitle(tr("Backup Settings"));
	QGridLayout*gl;
	QHBoxLayout*hl;
	setLayout(gl=new QGridLayout);
	QPushButton*p;
	gl->addWidget(new QLabel(tr("Backup File:")),0,0);
	gl->addWidget(lpath=new QLineEdit(path),0,1);
	gl->addWidget(p=new QPushButton(tr("...")),0,2);
	connect(p,SIGNAL(clicked()),this,SLOT(getpath()));
	
	gl->addWidget(new QLabel(tr("Generations to keep:")),1,0);
	gl->addWidget(gener=new QSpinBox,1,1,1,2);
	gener->setRange(1,16);
	gener->setValue(gens);
	
	gl->addWidget(new QLabel(tr("Automatic Backup:")),2,0);
	gl->addWidget(autob=new QCheckBox,2,1,1,2);
	autob->setChecked(tm>0);
	
	gl->addWidget(new QLabel(tr("Interval in days:")),3,0);
	gl->addWidget(interv=new QSpinBox,3,1,1,2);
	interv->setRange(1,24);
	interv->setValue(tm>0?tm:1);
	
	gl->setRowMinimumHeight(4,15);
	gl->addLayout(hl=new QHBoxLayout,5,0,1,3);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("&OK")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	connect(p,SIGNAL(clicked()),this,SLOT(save()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

void MBackupDialog::getpath()
{
	QString path=QFileDialog::getSaveFileName(this,tr("Backup File"),lpath->text());
	if(path!="")lpath->setText(path);
}

void MBackupDialog::save()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	set.setValue("backupfile",lpath->text());
	set.setValue("backupgenerations",gener->value());
	set.setValue("backupinterval",autob->isChecked()?interv->value():0);
}
