//
// C++ Interface: orderaudit dialog
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ORDERAUDITDLG_H
#define MAGICSMOKE_ORDERAUDITDLG_H

#include <QDialog>
#include <DPtrBase>

class MTGetOrderAudit;
class MOrderAuditDialog:public QDialog
{
	Q_OBJECT
	DECLARE_DPTR(d);
	public:
		MOrderAuditDialog(MTGetOrderAudit&audit,QWidget*parent=0);
		virtual ~MOrderAuditDialog();
		
		static void openOrderAuditDialog(qint64,QWidget*parent=0);
		
		static void openVoucherAuditDialog(QString,QWidget*parent=0);
		static void openTicketAuditDialog(QString,QWidget*parent=0);
		
		static void openUserAuditDialog(QString,qint64,QWidget*parent=0);
	private slots:
		void getProfiles();
		void calcPrices();
		void drawdata();
};

class MTGetUserAudit;
class MUserAuditDialog:public QDialog
{
	Q_OBJECT
	DECLARE_DPTR(d);
	public:
		MUserAuditDialog(const MTGetUserAudit&,QWidget* parent = 0, Qt::WindowFlags f = 0);
	private slots:
		void showorder(qint64 oid=-1);
		void showorderbyticket();
		void showorderbyvoucher();
		void auditorder(qint64 oid=-1);
		void auditorderbyticket();
		void auditorderbyvoucher();
		void auditvoucher();
		void auditticket();
};

#endif
