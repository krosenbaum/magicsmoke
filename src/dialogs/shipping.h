//
// C++ Interface: shipping
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SHIPPING_H
#define MAGICSMOKE_SHIPPING_H

#include <QString>
#include <QDialog>

class QStandardItemModel;
class QTableView;

#include "MOShipping"

class MShippingEditor:public QDialog
{
	Q_OBJECT
	public:
		MShippingEditor(QWidget*);
	
	private slots:
		void changeDescription();
		void changePrice();
		void changeAvail();
		void addNew();
		void deleteShip();
		void updateTable();
		
	private:
		QList<MOShipping>all;
		QStandardItemModel*model;
		QTableView*table;
};

#endif
