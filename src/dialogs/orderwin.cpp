//
// C++ Implementation: orderwin
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "centbox.h"
#include "customerdlg.h"
#include "labeldlg.h"
#include "misc.h"
#include "msinterface.h"
#include "odtrender.h"
#include "orderauditdlg.h"
#include "orderwin.h"
#include "payedit.h"
#include "templates.h"
#include "ticketrender.h"
#include "billrender.h"

#include "MOEvent"
#include "MTCancelOrder"
#include "MTChangeOrderAddress"
#include "MTChangeTicketPrice"
#include "MTChangeTicketPriceCategory"
#include "MTChangeVoucherValidity"
#include "MTGetAllShipping"
#include "MTGetCustomer"
#include "MTGetEvent"
#include "MTGetEventList"
#include "MTOrderAddComment"
#include "MTOrderChangeComments"
#include "MTOrderChangeShipping"
#include "MTOrderMarkShipped"
#include "MTOrderPay"
#include "MTOrderRefund"
#include "MTReservationToOrder"
#include "MTReturnTicketVoucher"
#include "MTUseVoucher"

#include <QApplication>
#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QDebug>
#include <QDesktopWidget>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QInputDialog>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPainter>
#include <QPixmap>
#include <QPrintDialog>
#include <QPrinter>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextEdit>

#include <math.h>

/**helper class that has a more sensible sizeHint than the original QTableView - it tries to make sure the full table is visible*/
class MOWTableView:public QTableView
{
	public:
		MOWTableView()
		{
			//set a sensible policy
			QSizePolicy sp=sizePolicy();
			sp.setVerticalPolicy  (QSizePolicy::Expanding);
			sp.setHorizontalPolicy(QSizePolicy::Expanding);
			setSizePolicy(sp);
			//remember absolute maximum
			absmax=QDesktopWidget().availableGeometry(this).size()-QSize(50,50);
		}
		QSize sizeHint()const{
			QSize psz=QTableView::minimumSizeHint();
			QSize vsz=maximumViewportSize()+QSize(50,50);
			//get maximum of parents view and viewports view of things
			if(psz.width()>vsz.width())vsz.setWidth(psz.width());
			if(psz.height()>vsz.height())vsz.setHeight(psz.height());
			//constrain to absolute maximum
			if(absmax.width()<vsz.width())vsz.setWidth(absmax.width());
			if(absmax.height()<vsz.height())vsz.setHeight(absmax.height());
// 			qDebug()<<"ret min"<<vsz;
			return vsz;
		}
	private:
		QSize absmax;
};

MOrderWindow::MOrderWindow(QWidget*par,const MOOrder&o)
	:QMainWindow(par),m_order(o)
{
	setWindowTitle(tr("Order Details"));
	setAttribute(Qt::WA_DeleteOnClose);
	m_changed=false;
	
	QMenuBar*mb=menuBar();
	QMenu *m=mb->addMenu(tr("&Order"));
	m_res2order=m->addAction(tr("&Order..."),this,SLOT(createOrder()));
	m_cancel=m->addAction(tr("C&ancel Order..."),this,SLOT(cancelOrder()));
 	m_ship=m->addAction(tr("&Mark Order as Shipped..."),this,SLOT(shipOrder()));
	m->addSeparator();
	m->addAction(tr("Change Item &Price..."),this,SLOT(changeItem()))
	 ->setEnabled(req->hasRight(req->RChangeTicketPrice));
	m->addAction(tr("C&hange Ticket Price Category..."),this,SLOT(changeItemCat()))
	 ->setEnabled(req->hasRight(req->RChangeTicketPriceCategory));
	m->addAction(tr("Change Voucher Validity..."),this,SLOT(changeVoucherValid()))
	 ->setEnabled(req->hasRight(req->MInterface::RChangeVoucherValidity));
 	m->addAction(tr("&Return Item..."),this,SLOT(itemReturn()))
 	 ->setEnabled(req->hasRight(req->RReturnTicketVoucher));
	m->addSeparator();
 	m->addAction(tr("Add Commen&t..."),this,SLOT(addComment()))
 	 ->setEnabled(req->hasRight(req->ROrderAddComment));
	if(req->hasRight(req->ROrderChangeComments))
	 m->addAction(tr("Change C&omments..."),this,SLOT(changeComments()));
 	m->addAction(tr("Change Sh&ipping Method..."),this,SLOT(changeShipping()))
 	 ->setEnabled(req->hasRight(req->ROrderChangeShipping));
	m->addAction(tr("Change Invoice Address..."),this,SLOT(changeInvAddr()))
	 ->setEnabled(req->hasRight(req->RChangeOrderAddress));
	m->addAction(tr("Change Delivery Address..."),this,SLOT(changeDelAddr()))
	 ->setEnabled(req->hasRight(req->RChangeOrderAddress));
	m->addAction(tr("Customer Data..."),this,SLOT(changeCustData()))
	 ->setEnabled(req->hasRight(MInterface::RGetCustomer));
	m->addSeparator();
	m->addAction(tr("&Close"),this,SLOT(close()));
	
	m=mb->addMenu(tr("&Payment"));
	m_pay=m->addAction(tr("Receive &Payment..."),this,SLOT(payment()));
	m_refund=m->addAction(tr("&Refund..."),this,SLOT(refund()));
 	m_payv=m->addAction(tr("Pay with &Voucher..."),this,SLOT(payvoucher()));

	m=mb->addMenu(tr("P&rinting"));
	m->addAction(tr("Print &Bill..."),this,SLOT(printBill()));
	m->addAction(tr("Save Bill &as file..."),this,SLOT(saveBill()));
	m->addSeparator();
	m->addAction(tr("Print &Tickets..."),this,SLOT(printTickets()));
	m->addAction(tr("Print V&ouchers..."),this,SLOT(printVouchers()));
	m->addAction(tr("Print &Current Item..."),this,SLOT(printCurrentItem()));
	m->addAction(tr("&View Items..."),this,SLOT(itemView()));
	
	m=mb->addMenu(tr("&Audit"));
	m->addAction(tr("Voucher History..."),this,SLOT(voucherAudit()))
	 ->setEnabled(req->hasRight(req->RGetVoucherAudit));
	m->addAction(tr("Ticket History..."),this,SLOT(ticketAudit()))
	 ->setEnabled(req->hasRight(req->RGetTicketAudit));
	m->addAction(tr("Order History..."),this,SLOT(orderAudit()))
	 ->setEnabled(req->hasRight(req->RGetOrderAudit));
	
	QWidget*w;
	setCentralWidget(w=new QWidget);
	QVBoxLayout *vl;
	w->setLayout(vl=new QVBoxLayout);
	
	QGridLayout*gl;
	vl->addLayout(gl=new QGridLayout,0);
	int rw=0;
	QLabel*lab;
	gl->addWidget(new QLabel(tr("Order ID:")),rw,0);
	gl->addWidget(m_orderid=new QLabel(QString::number(m_order.orderid())),rw,1);
	gl->addWidget(new QLabel(tr("Order State:")),++rw,0);
	gl->addWidget(m_state=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Order Date:")),++rw,0);
	gl->addWidget(m_orderdate=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Shipping Date:")),++rw,0);
	gl->addWidget(m_sentdate=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Customer:")),++rw,0);
	gl->addWidget(m_custname=new QLabel(),rw,1);
	gl->addWidget(lab=new QLabel(tr("Invoice Address:")),++rw,0);
	lab->setAlignment(Qt::AlignTop);
	gl->addWidget(m_iaddr=lab=new QLabel(),rw,1);
	lab->setWordWrap(true);lab->setFrameShape(lab->Box);lab->setFrameShadow(lab->Raised);
	gl->addWidget(lab=new QLabel(tr("Delivery Address:")),++rw,0);
	lab->setAlignment(Qt::AlignTop);
	gl->addWidget(m_daddr=lab=new QLabel(),rw,1);
	lab->setWordWrap(true);lab->setFrameShape(lab->Box);lab->setFrameShadow(lab->Raised);
	gl->addWidget(new QLabel(tr("Shipping Method:")),++rw,0);
	gl->addWidget(m_shipmeth=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Shipping Costs:")),++rw,0);
	gl->addWidget(m_shipprice=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Total Price:")),++rw,0);
	gl->addWidget(m_total=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Already Paid:")),++rw,0);
	gl->addWidget(m_paid=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Coupon:")),++rw,0);
	gl->addWidget(m_coupon=new QLabel(),rw,1);
	gl->addWidget(new QLabel(tr("Sold by:")),++rw,0);
	gl->addWidget(m_soldby=new QLabel(),rw,1);
	gl->addWidget(lab=new QLabel(tr("Order Comments:")),++rw,0);
	lab->setAlignment(Qt::AlignTop);
	gl->addWidget(m_comment=lab=new QLabel(),rw,1);
	lab->setWordWrap(true);lab->setFrameShape(lab->Box);lab->setFrameShadow(lab->Raised);
	gl->setColumnStretch(0,0);
	gl->setColumnStretch(1,10);
	
	
	vl->addSpacing(10);
	vl->update();
// 	QSize sz=size();//qDebug()<<"size"<<sz;
	
	vl->addWidget(m_table=new MOWTableView,10);
	m_table->setModel(m_model=new QStandardItemModel(this));
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_table->setSortingEnabled(true);
	updateData();
}

void MOrderWindow::setTestTemplateFile(QString tf)
{
	m_testTemplate=tf;
}

static const int ITEM_TICKET=1;
static const int ITEM_VOUCHER=2;
static const int ITEM_ITEM=3;

void MOrderWindow::updateData()
{
	//label data
	m_orderid->setText(QString::number(m_order.orderid()));
	m_state->setText(m_order.orderStatusString());
	m_orderdate->setText(m_order.orderDateTimeStr());
	m_sentdate->setText(m_order.sentDateTimeStr());
	m_custname->setText(m_order.customer().value().fullName());
	m_daddr->setText(m_order.fullDeliveryAddress(false));
	m_iaddr->setText(m_order.fullInvoiceAddress(false));
	m_shipmeth->setText(m_order.shippingtype().value().description());
	m_shipprice->setText(cent2str(m_order.shippingcosts()));
	m_total->setText(m_order.totalPriceString());
	m_paid->setText(m_order.amountPaidString());
	m_soldby->setText(m_order.soldby());
	m_comment->setText(m_order.comments());
	if(m_order.couponid().isNull())m_coupon->setText(tr("(none)","no coupon"));
	else m_coupon->setText(QString("%1 (%2)").arg(m_order.couponid().data()).arg(m_order.coupondescription().data()));
	//get detail data
	QList<MOTicket> tickets=m_order.tickets();
	QList<MOEvent> events;
	if(tickets.size()>0){
		QList<qint64>evid;
		for(int i=0;i<tickets.size();i++)
			if(!evid.contains(tickets[i].eventid()))
				evid.append(tickets[i].eventid());
		MTGetEventList gel=req->queryGetEventList(evid);
		events=gel.getevents();
	}
	QList<MOVoucher> vouchers=m_order.vouchers();
	QList<MOItemInfo> items=m_order.items();
	//set up model
	m_model->clear();
	m_model->setHorizontalHeaderLabels(QStringList()<<tr("Item ID")<<tr("Description")<<tr("Date/Time")<<tr("Status")<<tr("Price"));
	m_model->insertRows(0,tickets.size()+vouchers.size()+items.size());
	//insert tickets
	for(int i=0;i<tickets.size();i++){
		m_model->setData(m_model->index(i,0),tickets[i].ticketid().value());
		m_model->setData(m_model->index(i,0),ITEM_TICKET,Qt::UserRole);
		m_model->setData(m_model->index(i,3),tickets[i].statusString());
		m_model->setData(m_model->index(i,4),tickets[i].priceString()+" ("+tickets[i].priceCategoryName()+")");
		//find event
		MOEvent ev;int eid=tickets[i].eventid();
		for(int j=0;j<events.size();j++)
			if(events[j].eventid().value()==eid)
				ev=events[j];
		//render remainder
		m_model->setData(m_model->index(i,1),ev.title().value());
		m_model->setData(m_model->index(i,2),tr("Begin: %1").arg(ev.startTimeString()));
	}
	//insert vouchers
	int off=tickets.size();
	for(int i=0;i<vouchers.size();i++){
		m_model->setData(m_model->index(i+off,0),ITEM_VOUCHER,Qt::UserRole);
		m_model->setData(m_model->index(i+off,0),vouchers[i].voucherid().value());
		m_model->setData(m_model->index(i+off,1),tr("Voucher (current value: %1)").arg(vouchers[i].valueString()));
		m_model->setData(m_model->index(i+off,2),tr("Valid till: %1").arg(vouchers[i].validDate()));
		m_model->setData(m_model->index(i+off,3),vouchers[i].statusString());
		m_model->setData(m_model->index(i+off,4),vouchers[i].priceString());
	}
	//insert items
	off+=vouchers.size();
	for(int i=0;i<items.size();i++){
		m_model->setData(m_model->index(i+off,0),ITEM_ITEM,Qt::UserRole);
		m_model->setData(m_model->index(i+off,0),items[i].itemid().value());
		m_model->setData(m_model->index(i+off,1),tr("%1x %2").arg(items[i].amount().value()).arg(items[i].productname().value()));
		m_model->setData(m_model->index(i+off,3),"");
		m_model->setData(m_model->index(i+off,4),cent2str(items[i].totalprice()));
	}
	//refresh
	m_table->resizeColumnsToContents();
	m_res2order->setEnabled(req->hasRight(req->RReservationToOrder)&&m_order.isReservation());
	m_cancel->setEnabled(req->hasRight(req->RCancelOrder));
	m_ship->setEnabled(req->hasRight(req->ROrderMarkShipped)&&m_order.status()==m_order.Placed);
	m_pay->setEnabled(req->hasRight(req->ROrderPay)&&m_order.needsPayment());
	m_payv->setEnabled(req->hasRight(req->RUseVoucher)&&m_order.needsPayment());
	m_refund->setEnabled(req->hasRight(req->ROrderRefund)&&m_order.needsRefund());
}

void MOrderWindow::itemView()
{
	QList<MOTicket>tickets=m_order.tickets();
	QList<MOVoucher>vouchers=m_order.vouchers();
	if(tickets.size()<1 && vouchers.size()<1)return;
	MOrderItemView tv(this,tickets,vouchers);
	tv.exec();
}

void MOrderWindow::printCurrentItem()
{
	//get current ticketid/voucherid
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	//find ticket/voucher
	if(type==ITEM_TICKET){
		QList<MOTicket>ticks;
		QList<MOTicket>tickets=m_order.tickets();
		for(int i=0;i<tickets.size();i++)
			if(tickets[i].ticketid().value()==id /*&& MOTicket(tickets[i]).isValid()*/)
				ticks<<tickets[i];
		printTickets(ticks);
	}else
	if(type==ITEM_VOUCHER){
		QList<MOVoucher>vouchs;
		QList<MOVoucher>vouchers=m_order.vouchers();
		for(int i=0;i<vouchers.size();i++)
			if(vouchers[i].voucherid().value()==id /*&& MOVoucher(vouchers[i]).isValid()*/)
				vouchs<<vouchers[i];
		printVouchers(vouchs);
	}
	
}

void MOrderWindow::printTickets()
{
	printTickets(m_order.tickets());
}

void MOrderWindow::printVouchers()
{
	printVouchers(m_order.vouchers());
}

void MOrderWindow::printTickets(QList<MOTicket> ticketsin)
{
	//reduce ticket list to usable ones
	QList<MOTicket> tickets;
	for(int i=0;i<ticketsin.size();i++){
		if(ticketsin[i].status()&MOTicket::MaskPay)
			tickets.append(ticketsin[i]);
	}
	//sanity check
	if(tickets.size()<1){
		QMessageBox::warning(this,tr("Warning"),tr("There are no tickets left to print."));
		return;
	}
	//get template
	MTemplate tf=req->templateStore()->getTemplate("ticket");
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (ticket.xtt). Giving up."));
		return;
	}
	//get printer settings
	QPrinter printer;
	MLabelConfig lconf(MLabelConfig::Ticket);
	lconf.selectPrinter(printer);
	//label arrangement
	MTicketRenderer render(tf);
	MLabelDialog ld(this,&printer,tickets.size(),render.labelSize(printer));
	if(!ld.conditionalExec(MLabelConfig::Ticket))
		return;
	//print
	QPainter painter(&printer);
	for(int i=0;i<tickets.size();i++){
		QPointF p=ld.labelOffset(i);
		if(ld.labelNeedsPageTurn(i)){
			printer.newPage();
		}
		render.render(tickets[i],printer,&painter,p,lconf.printMode()==MLabelConfig::PrintPixmap);
	}
}

void MOrderWindow::printVouchers(QList<MOVoucher> vouchersin)
{
	//reduce voucher list to usable ones
	QList<MOVoucher>vouchers;
	for(int i=0;i<vouchersin.size();i++){
		if(vouchersin[i].isValid() && vouchersin[i].value()>0)
			vouchers.append(vouchersin[i]);
	}
	//sanity check
	if(vouchers.size()<1){
		QMessageBox::warning(this,tr("Warning"),tr("There are no vouchers left to print."));
		return;
	}
	//get template
	MTemplate tf=req->templateStore()->getTemplate("voucher");
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (voucher.xtt). Giving up."));
		return;
	}
	//get printer settings
	QPrinter printer;
	MLabelConfig lconf(MLabelConfig::Voucher);
	lconf.selectPrinter(printer);
	//label arrangement
	MVoucherRenderer render(tf);
	MLabelDialog ld(this,&printer,vouchers.size(),render.labelSize(printer));
	if(!ld.conditionalExec(MLabelConfig::Voucher))
		return;
	//print
	QPainter painter(&printer);
	for(int i=0;i<vouchers.size();i++){
		QPointF p=ld.labelOffset(i);
		if(ld.labelNeedsPageTurn(i)){
			printer.newPage();
		}
		render.render(vouchers[i],printer,&painter,p,lconf.printMode()==MLabelConfig::PrintPixmap);
	}
}

void MOrderWindow::printBill()
{
	//get template
	MTemplate tf;
	if(m_testTemplate.isEmpty()){
		tf=req->templateStore()->getTemplate("bill");
	}else
		tf.setFile(m_testTemplate);
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (bill). Giving up."));
		return;
	}
	qDebug()<<"printing bill using template"<<tf.cacheFileName();
	//mark order as shipped?
	if(m_order.status()==MOOrder::Placed)
	if(QMessageBox::question(this,tr("Mark as shipped?"),tr("Mark this order as shipped now?"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
		MTOrderMarkShipped ms=req->queryOrderMarkShipped(m_order.orderid(),-1);
		if(!ms.hasError())m_order=ms.getorder();
		updateData();
	}
	//print bill
	MBillRenderer rend(m_order, tf);
	rend.renderToPrinter();
}

void MOrderWindow::saveBill()
{
	//get template
	MTemplate tf;
	if(m_testTemplate.isEmpty())
		tf=req->templateStore()->getTemplate("bill");
	else
		tf.setFile(m_testTemplate);
	if(!tf.isValid()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to get template file (bill). Giving up."));
		return;
	}
	qDebug()<<"saving bill using template"<<tf.cacheFileName();
	//get target file name
	QFileDialog fd(this);
	fd.setAcceptMode(QFileDialog::AcceptSave);
	fd.setFileMode(QFileDialog::AnyFile);
	fd.setConfirmOverwrite(true);
	fd.setNameFilter(tr("Open Document File (*.%1)").arg(tf.targetExtension()));
	fd.setDefaultSuffix(tf.targetExtension());
	fd.setDirectory(currentDir());
	QString fname;
	if(fd.exec()){
		QStringList fn=fd.selectedFiles();
		if(fn.size()<1)return;
		fname=fn[0];
		setCurrentDir(fname);
	}
	//mark order as shipped?
	if(m_order.status()==MOOrder::Placed)
	if(QMessageBox::question(this,tr("Mark as shipped?"),tr("Mark this order as shipped now?"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
		MTOrderMarkShipped ms=req->queryOrderMarkShipped(m_order.orderid(),-1);
		if(!ms.hasError())m_order=ms.getorder();
		updateData();
	}
	//render bill
	MBillRenderer rend(m_order, tf);
	rend.renderToFile(fname);
}

void MOrderWindow::payment()
{
	//get value
	MPaymentDialog pd(this);
	pd.setMaximum(m_order.amountToPay());
	pd.setValue(m_order.amountToPay());
	if(pd.exec()!=QDialog::Accepted)return;
	int pay=pd.value();
	if(pay<=0)return;
	//submit
	MTOrderPay op=req->queryOrderPay(m_order.orderid(),pay,pd.paytype(),pd.paydata());
	if(op.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while trying to pay: %1").arg(op.errorString()));
		return;
	}
	m_order=op.getorder();
	updateData();
	//tell user if less money was required
	if(op.getamount()<pay)
		QMessageBox::information(this,tr("Payment Info"),
		   tr("Payment successful, but only %1 was required, please hand back the remaining %2.")
		   .arg(cent2str(op.getamount()))
		   .arg(cent2str(pay-op.getamount()))
		);
}

void MOrderWindow::payvoucher()
{
	if(!m_order.isValid())return;
	//get voucher
	bool ok;
	QString vid=QInputDialog::getText(this,tr("Enter Voucher"),tr("Please enter the ID of the voucher you want to use:"),QLineEdit::Normal,"",&ok);
	if(!ok)return;
	if(vid=="")return;
	//submit
	MTUseVoucher uv=req->queryUseVoucher(m_order.orderid(),vid);
	if(uv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while trying to pay with voucher '%1': %2").arg(vid).arg(uv.errorString()));
		return;
	}
	m_order=uv.getorder();
	QMessageBox::information(this,tr("Voucher Info"),
		tr("Successfully paid order %1 with voucher '%2'.\nAmount deducted: %3\nRemaining value of this voucher: %4")
		.arg(m_order.orderid())
		.arg(vid)
		.arg(cent2str(uv.getamount()))
		.arg(cent2str(uv.getvoucher().value().value()))
	);
	updateData();
}

void MOrderWindow::refund()
{
	if(!m_order.isValid())return;
	//get value
	bool ok;
	int pay=MCentDialog::getCents(this,tr("Enter Refund"),tr("Please enter the amount that will be refunded:"),m_order.amountToRefund(),m_order.amountToRefund(),&ok);
	if(!ok)return;
	if(pay<=0)return;
	//submit
	MTOrderRefund op=req->queryOrderRefund(m_order.orderid(),pay);
	if(op.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while trying to refund: %1").arg(op.errorString()));
		return;
	}
	m_order=op.getorder();
	updateData();
}

void MOrderWindow::changeItem()
{
	//get ticket selection
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type==ITEM_TICKET){
		//find ticket
		QList<MOTicket>tickets=m_order.tickets();
		MOTicket tick;
		for(int i=0;i<tickets.size();i++)
			if(tickets[i].ticketid()==id)
				tick=tickets[i];
		//get value
		bool ok;
		int pay=MCentDialog::getCents(this,tr("Enter Price"),tr("Please enter the new price for the ticket:"),tick.price(),100000000,&ok);
		if(!ok)return;
		if(pay<0)return;
		//submit
		MTChangeTicketPrice ctp=req->queryChangeTicketPrice(id,pay);
		if(ctp.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while attempting to change ticket price: %1").arg(ctp.errorString()));
			return;
		}
		m_order=ctp.getorder();
		updateData();
	}else{
		QMessageBox::warning(this,tr("Warning"),tr("Cannot change this item type."));
		return;
	}
}

void MOrderWindow::changeItemCat()
{
	//get ticket selection
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type==ITEM_TICKET){
		//find ticket
		QList<MOTicket>tickets=m_order.tickets();
		MOTicket tick;
		for(int i=0;i<tickets.size();i++)
			if(tickets[i].ticketid()==id)
				tick=tickets[i];
		//get event
		MTGetEvent ge=MTGetEvent::query(tick.eventid());
		if(ge.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error getting event, please try again."));
			return;
		}
		MOEvent ev=ge.getevent();
		//get Price Category
		QList<MOEventPrice> ep=ev.price();
		if(ep.size()<=1){
			QMessageBox::warning(this,tr("Warning"),tr("Cannot select another price category - there are none left."));
			return;
		}
		int pcidx=0;
		int pcid=tick.pricecategoryid();
		QDialog d(this);
		d.setWindowTitle(tr("Select Price Category"));
		QVBoxLayout*vl;
		QHBoxLayout*hl;
		QComboBox*box;
		QPushButton*p;
		d.setLayout(vl=new QVBoxLayout);
		vl->addWidget(new QLabel(tr("Please chose a price category:")));
		vl->addWidget(box=new QComboBox);
		for(int i=0;i<ep.size();i++){
			box->addItem(ep[i].pricecategory().value().name().value()+": "+cent2str(ep[i].price()));
			if(ep[i].pricecategoryid()==pcid)pcidx=i;
		}
		box->setCurrentIndex(pcidx);
		box->setEditable(false);
		vl->addSpacing(10);
		vl->addStretch(10);
		vl->addLayout(hl=new QHBoxLayout);
		hl->addStretch(10);
		hl->addWidget(p=new QPushButton(tr("Ok")));
		connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
		hl->addWidget(p=new QPushButton(tr("Cancel")));
		connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
		if(d.exec()!=QDialog::Accepted)return;
		pcidx=box->currentIndex();
		//submit
		MTChangeTicketPriceCategory ctp=req->queryChangeTicketPriceCategory(id,ep[pcidx].pricecategoryid());
		if(ctp.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while attempting to change ticket price: %1").arg(ctp.errorString()));
			return;
		}
		m_order=ctp.getorder();
		updateData();
	}else{
		QMessageBox::warning(this,tr("Warning"),tr("Cannot change this item type."));
		return;
	}
}

void MOrderWindow::itemReturn()
{
	//get ticket selection
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type!=ITEM_TICKET && type!=ITEM_VOUCHER){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot return this item type."));
		return;
	}
	//ask nicely
	if(QMessageBox::question(this,tr("Return Ticket or Voucher"),tr("Do you really want to return this ticket or voucher?"),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//submit
	MTReturnTicketVoucher rtv=req->queryReturnTicketVoucher(id);
	if(rtv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error whily trying to return item: %1").arg(rtv.errorString()));
		return;
	}
	m_order=rtv.getorder();
	updateData();
}

void MOrderWindow::changeVoucherValid()
{
	//get voucher selection
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type!=ITEM_VOUCHER){
		QMessageBox::warning(this,tr("Warning"),tr("This is not a voucher."));
		return;
	}
	//find object
	MOVoucher voucher;
	for(MOVoucher v:m_order.vouchers())
		if(v.voucherid()==id){
			voucher=v;
			break;
		}
	//ask nicely
	QDialog d(this);
	d.setWindowTitle(tr("Change Validity Date"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("Changing validity date of voucher '%1':").arg(id)));
	vl->addWidget(new QLabel(tr("Current validity: '%1'").arg(voucher.validDate())));
	QCheckBox*cb;
	vl->addWidget(cb=new QCheckBox(tr("Indefinitely valid")));
	cb->setChecked(voucher.validtime().isNull());
	QDateEdit*de;
	vl->addWidget(de=new QDateEdit(QDateTime::fromTime_t(voucher.validtime()).date()));
	de->setCalendarPopup(true);
	de->setDisplayFormat(tr("yyyy-MM-dd","Date format for editing voucher validity date"));
	de->setDisabled(cb->isChecked());
	connect(cb,SIGNAL(toggled(bool)),de,SLOT(setDisabled(bool)));
	QLineEdit*le;
	vl->addWidget(le=new QLineEdit);
	le->setPlaceholderText(tr("Comment..."));
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&Set Validity")));
	connect(p,SIGNAL(clicked(bool)),&d,SLOT(accept()));
	p->setDefault(true);
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked(bool)),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	//submit
	MTChangeVoucherValidity cvv=req->queryChangeVoucherValidity(id, cb->isChecked(), Nullable<qint64>(de->dateTime().toTime_t()), le->text());
	if(cvv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error whily trying to change voucher validity: %1").arg(cvv.errorString()));
		return;
	}
	m_order=cvv.getorder();
	updateData();
}

void MOrderWindow::cancelOrder()
{
	if(QMessageBox::question(this,tr("Cancel Order?"),tr("Cancel this order now?"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)!=QMessageBox::Yes)
		return;
	//query
	MTCancelOrder co=req->queryCancelOrder(m_order.orderid());
	if(co.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while cancelling order: %1").arg(co.errorString()));
		return;
	}
	m_order=co.getorder();
	updateData();
}

void MOrderWindow::createOrder()
{
	//sanity check
	if(m_order.status()!=MOOrder::Reserved)return;
	//call server
	MTReservationToOrder ro=req->queryReservationToOrder(m_order.orderid());
	if(ro.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while changing order status: %1").arg(ro.errorString()));
		return;
	}
	m_order=ro.getorder();
	updateData();
}

void MOrderWindow::shipOrder()
{
	if(QMessageBox::question(this,tr("Mark as shipped?"),tr("Mark this order as shipped now?"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
		QDateTime tm=QDateTime::currentDateTime();
		if(req->hasRight(req->POrderMarkShipped_SetTime)){
			QDialog d;
			d.setWindowTitle(tr("Set shipping time"));
			QHBoxLayout*hl;
			QVBoxLayout*vl;
			QPushButton*p;
			QDateTimeEdit*dte;
			d.setLayout(vl=new QVBoxLayout);
			vl->addWidget(new QLabel(tr("Enter the shipping time:")));
			vl->addWidget(dte=new QDateTimeEdit(tm));
			vl->addSpacing(10);
			vl->addStretch(10);
			vl->addLayout(hl=new QHBoxLayout);
			hl->addStretch(10);
			hl->addWidget(p=new QPushButton(tr("OK")));
			connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
			hl->addWidget(p=new QPushButton(tr("Cancel")));
			connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
			if(d.exec()!=QDialog::Accepted)return;
			tm=dte->dateTime();
		}
		MTOrderMarkShipped ms=req->queryOrderMarkShipped(m_order.orderid(),tm.toTime_t());
		if(ms.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while marking order as shipped: %1").arg(ms.errorString()));
			return;
		}
		m_order=ms.getorder();
		updateData();
	}
}

void MOrderWindow::changeComments()
{
	//create editor dialog
	QString cmt=m_order.comments();
	QDialog d;
	d.setWindowTitle(tr("Change comments: order %1").arg(m_order.orderid()));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QTextEdit*te;
	vl->addWidget(te=new QTextEdit,1);
	te->setPlainText(cmt);
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&Save")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//get status
	if(d.exec()!=QDialog::Accepted)return;
	//send to server
	MTOrderChangeComments ac=req->queryOrderChangeComments(m_order.orderid(),te->toPlainText());
	if(ac.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("There was a problem uploading the new comment: %1").arg(ac.errorString()));
		return;
	}
	m_order=ac.getorder();
	//reset display
	updateData();
}
void MOrderWindow::addComment()
{
	//create editor dialog
	QString cmt=m_order.comments();
	QDialog d;
	d.setWindowTitle(tr("Add comment: order %1").arg(m_order.orderid()));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QTextEdit*te;
	QLabel*lab;
	vl->addWidget(lab=new QLabel("Current comment:\n"+cmt));
	lab->setWordWrap(true);
	vl->addWidget(te=new QTextEdit,1);
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&Save")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//get status
	if(d.exec()!=QDialog::Accepted)return;
	//send to server
	MTOrderAddComment ac=req->queryOrderAddComment(m_order.orderid(),te->toPlainText());
	if(ac.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("There was a problem uploading the new comment: %1").arg(ac.errorString()));
		return;
	}
	m_order=ac.getorder();
	//reset display
	updateData();
}

void MOrderWindow::changeShipping()
{
	//create editor dialog
	MShippingChange d(this,m_order.shippingtype(),m_order.shippingcosts());
	//get status
	if(d.exec()!=QDialog::Accepted)return;
	//send to server
	int shipid=-1;
	if(!d.selection().shipid().isNull())shipid=d.selection().shipid();
	MTOrderChangeShipping ocs=req->queryOrderChangeShipping(m_order.orderid(),shipid,d.price());
	if(ocs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while changing shipping: %1").arg(ocs.errorString()));
		return;
	}
	m_order=ocs.getorder();
	//reset display
	updateData();
}

void MOrderWindow::changeDelAddr()
{
	//get customer
	MAddressChoiceDialog d(this,m_order.customer().value());
	d.preselect(m_order.deliveryaddress());
	d.addUnselectButton(tr("No Delivery Address"));
	if(d.exec()!=QDialog::Accepted)return;
	//send data
	MTChangeOrderAddress oa=req->queryChangeOrderAddress(m_order.orderid(), false,-1, true,d.addressId());
	if(oa.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to set address, server error: %1").arg(oa.errorString()));
		return;
	}
	m_daddr->setText(d.address().fullAddress());
}

void MOrderWindow::changeCustData()
{
    //get object
    MTGetCustomer gc=req->queryGetCustomer(m_order.customerid());
    if(gc.hasError()){
        QMessageBox::warning(this,tr("Warning"),tr("Unable to get customer data: %1").arg(gc.errorString()));
        return;
    }
    //open dialog
    MCustomerDialog cd(gc.getcustomer().value(),this);
    if(cd.exec()==QDialog::Accepted)
        updateData();
}

void MOrderWindow::changeInvAddr()
{
	//get customer
	MAddressChoiceDialog d(this,m_order.customer().value());
	d.preselect(m_order.invoiceaddress());
	d.addUnselectButton(tr("No Invoice Address"));
	if(d.exec()!=QDialog::Accepted)return;
	//send data
	MTChangeOrderAddress oa=req->queryChangeOrderAddress(m_order.orderid(), true,d.addressId(), false,-1);
	if(oa.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to set address, server error: %1").arg(oa.errorString()));
		return;
	}
	m_iaddr->setText(d.address().fullAddress());
}

void MOrderWindow::voucherAudit()
{
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type!=ITEM_VOUCHER){
		QMessageBox::warning(this,tr("Warning"),tr("The selected item is not a voucher."));
		return;
	}
	MOrderAuditDialog::openVoucherAuditDialog(id);
}

void MOrderWindow::ticketAudit()
{
	QModelIndexList lst=m_table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=m_model->index(lst[0].row(),0);
	QString id=m_model->data(idx).toString();
	if(id=="")return;
	int type=m_model->data(idx,Qt::UserRole).toInt();
	if(type!=ITEM_TICKET){
		QMessageBox::warning(this,tr("Warning"),tr("The selected item is not a ticket."));
		return;
	}
	MOrderAuditDialog::openTicketAuditDialog(id);
}

void MOrderWindow::orderAudit()
{
	MOrderAuditDialog::openOrderAuditDialog(m_order.orderid());
}


/*************************************************************************************/

MOrderItemView::MOrderItemView(QWidget*w,QList<MOTicket>t,QList<MOVoucher>v)
	:QDialog(w),tickets(t),vouchers(v)
{
	setWindowTitle(tr("Preview Tickets"));
	
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QComboBox *cb;
	vl->addWidget(cb=new QComboBox,0);
	cb->setEditable(false);
	for(int i=0;i<tickets.size();i++)
		cb->addItem(tr("Ticket: ")+tickets[i].ticketid());
	for(int i=0;i<vouchers.size();i++)
		cb->addItem(tr("Voucher: ")+vouchers[i].voucherid());
	vl->addWidget(disp=new QLabel,10);
	//get the templates
	trender=new MTicketRenderer(req->templateStore()->getTemplate("ticket"));
	vrender=new MVoucherRenderer(req->templateStore()->getTemplate("voucher"));
	changeItem(0);
	connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(changeItem(int)));
}

MOrderItemView::~MOrderItemView()
{
	delete trender;
	delete vrender;
}

void MOrderItemView::changeItem(int idx)
{
	//ticket or voucher?
	if(idx<tickets.size()){
		QSizeF sz=trender->labelSize(*disp);
		QPixmap tick(sz.toSize());
		tick.fill();
		if(!trender->render(tickets[idx],tick))
			qDebug("unable to render");
		disp->setPixmap(tick);
	}else{
		QSizeF sz=vrender->labelSize(*disp);
		QPixmap tick(sz.toSize());
		tick.fill();
		if(!vrender->render(vouchers[idx-tickets.size()],tick))
			qDebug("unable to render");
		disp->setPixmap(tick);
	}
}


/*************************************************************************************/

MShippingChange::MShippingChange(QWidget*pa,MOShipping s,int costs)
	:QDialog(pa)
{
	QList<MOShipping>ships=req->queryGetAllShipping().getshipping();
	all.clear();
	MSInterface*ifc=MSInterface::instance();
	foreach(MOShipping ship,ships){
		if(ifc){
			if(ifc->checkFlags(ship.flags()))all<<ship;
		}else all<<ship;
	}
	setWindowTitle(tr("Change Shipping Method"));
	
	int cid=-1;
	if(!s.shipid().isNull())cid=s.shipid();
	
	QGridLayout*gl;
	setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("Method:")),0,0);
	gl->addWidget(opt=new QComboBox,0,1);
	gl->addWidget(new QLabel(tr("Price:")),1,0);
	gl->addWidget(prc=new MCentSpinBox,1,1);
	gl->setRowMinimumHeight(2,15);
	gl->setRowStretch(2,1);
	QHBoxLayout*hl;
	gl->addLayout(hl=new QHBoxLayout,3,0,1,2);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
	prc->setRange(0,1000000000);//hmm, even in Yen this should be big enough
	prc->setValue(costs);
	prc->setEnabled(req->hasRight(req->POrderChangeShipping_ChangePrice));
	
	opt->addItem(tr("(None)","shipping method"));
	int scid=0;
	for(int i=0;i<all.size();i++){
		opt->addItem(all[i].description());
		if(all[i].shipid().value()==cid)
			scid=i+1;
	}
	opt->setCurrentIndex(scid);
	connect(opt,SIGNAL(currentIndexChanged(int)),this,SLOT(changeItem(int)));
}

MOShipping MShippingChange::selection()const
{
	int cid=opt->currentIndex();
	if(cid==0)return MOShipping();
	MOShipping cp=all[cid-1];
	cp.setcost(price());
	return cp;
}

int MShippingChange::price()const
{
	return prc->value();
}

void MShippingChange::changeItem(int cid)
{
	if(cid==0)prc->setValue(0);
	else prc->setValue(all[cid-1].cost());
}
