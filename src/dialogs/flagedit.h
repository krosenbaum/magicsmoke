//
// C++ Interface: flagedit
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See COPYING.GPL file that comes with this distribution
//
//

#ifndef MAGICSMOKE_FLAGEDIT_H
#define MAGICSMOKE_FLAGEDIT_H

#include <QDialog>
#include <QStringList>

#include "MOFlag"

class QLabel;
class QStandardItemModel;
class QTableView;
class QLineEdit;
class QAbstractButton;

/**enables the user to edit flags attached to an entity as defined by MagicSmoke*/
class MFlagEditor:public QDialog
{
	Q_OBJECT
	public:
		/**convenience function: opens a flag editor and returns the result or the original string if unchanged*/
		static QString edit(QWidget*parent,QString flags,QString label=QString());
		
		/**creates an editor window*/
		MFlagEditor(QWidget*parent,QString flags,QString label=QString());
		
		/**returns the flag string corresponding to the current state of the editor*/
		QString currentFlags()const;
		/**returns the original string with which the editor was initialized*/
		QString originalFlags()const;
		
	private:
		friend class MFlagEditorDelegate;
		QList<MOFlag>allflags;
		QStringList flags;
		QStandardItemModel*model;
		QTableView*table;
	private slots:
		/**reset to original state*/
		void reset();
};

///simple widget that displays flags and allows to edit them
class MFlagWidget:public QWidget
{
	Q_OBJECT
	public:
		///creates a flag widget with no flags set, per default it is editable
		MFlagWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
		///creates a flag widget with flags set, per default it is editable
		MFlagWidget(QString flags,QWidget* parent = 0, Qt::WindowFlags f = 0);
		///creates a flag widget with flags set, per default it is editable
		MFlagWidget(QStringList flags,QWidget* parent = 0, Qt::WindowFlags f = 0);
		
		///returns the currently displayed flags as string
		QString flags()const{return m_flags;}
		///alias for flags() (for compatibility with other widgets)
		QString text()const{return m_flags;}
		
		///returns the label that will be used for the flag editor
		QString editorLabel()const{return m_label;}
	public slots:
		///sets the flags displayed in this widget
		void setFlags(QString);
		///sets the flags displayed in this widget
		void setFlags(QStringList);
		///sets the flags displayed in this widget (alias for compatibility with other widgets)
		void setText(QString);
		
		///sets the label for the flag editor when the user changes flags
		void setEditorLabel(QString e);
		
		///makes the widget editable (this is the default)
		void setEditable(bool e=true);
		///makes the widget readonly (per default it is editable)
		void setReadonly(bool ro=true);
	
	private slots:
		/// \internal call the editor
		void doedit();
	private:
		QString m_flags,m_label;
		QLabel*m_line;
		QAbstractButton*m_btn;
		
		/// \internal helper for the constructors
		void init();
};

#endif
