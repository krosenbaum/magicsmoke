//
// C++ Interface: checkdlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_CHECKDLG_H
#define MAGICSMOKE_CHECKDLG_H

#include <QDialog>
#include <QList>

class QCheckBox;

/**class for items that can be displayed in a MCheckDialog*/
class MCheckItem
{
	public:
		/**constructs a check item*/
		MCheckItem(QString key=QString(),bool isset=false,QString label=QString())
		{m_key=key;m_set=isset;if(label!="")m_label=label;else m_label=m_key;}
		
		virtual ~MCheckItem(){}
		
		/**overwrite this to return a label that can be displayed*/
		virtual QString label()const{return m_label;}
		/**overwrite this to return a key string that identifies the item (default implementation returns the label)*/
		virtual QString key()const{return m_key;}
		/**overwrite this to return whether the item is checked*/
		virtual bool isSet()const{return m_set;}
		/**overwrite this to change the checking status of the item*/
		virtual void set(bool b){m_set=b;}
		
	protected:
		QString m_key,m_label;
		bool m_set;
};

/**implements a list of checkable items*/
typedef QList<MCheckItem> MCheckList;

/**a dialog that consists of check boxes in a QScrollArea */
class MCheckDialog:public QDialog
{
	Q_OBJECT
	public:
		/**creates the dialog*/
		MCheckDialog(QWidget*parent,const MCheckList&checks,QString title);
		
		/**returns the current state of all check boxes, the return value is an exact copy of the check list from the constructor with updated check settings*/
		MCheckList getCheckList()const;
	private:
		QList<QCheckBox*>m_boxes;
		mutable MCheckList m_list;
};

#endif
