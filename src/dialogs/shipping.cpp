//
// C++ Implementation: shipping
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "misc.h"
#include "shipping.h"
#include "centbox.h"
#include "flagedit.h"

#include "MOShipping"
#include "MTGetAllShipping"
#include "MTChangeShipping"
#include "MTCreateShipping"
#include "MTDeleteShipping"

#include "msinterface.h"

#include <QApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QTableView>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QBoxLayout>

MShippingEditor::MShippingEditor(QWidget*par)
	:QDialog(par)
{
	all=req->queryGetAllShipping().getshipping();
	setWindowTitle(tr("Edit Shipping Options"));
	setSizeGripEnabled(true);
	
	QHBoxLayout*hl;
	QVBoxLayout*vl,*vl2;
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout,1);
	hl->addWidget(table=new QTableView,1);
	table->setModel(model=new QStandardItemModel(this));
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	table->verticalHeader()->hide();
	updateTable();
	hl->addLayout(vl2=new QVBoxLayout,0);
	QPushButton*p;
	vl2->addWidget(p=new QPushButton(tr("Change Description")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeDescription()));
	vl2->addWidget(p=new QPushButton(tr("Change Price")));
	connect(p,SIGNAL(clicked()),this,SLOT(changePrice()));
	vl2->addWidget(p=new QPushButton(tr("Change Flags")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeAvail()));
	vl2->addSpacing(20);
	vl2->addWidget(p=new QPushButton(tr("Add Option")));
	connect(p,SIGNAL(clicked()),this,SLOT(addNew()));
	vl2->addWidget(p=new QPushButton(tr("Delete Option")));
	connect(p,SIGNAL(clicked()),this,SLOT(deleteShip()));
	vl2->addStretch(1);
	
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}
void MShippingEditor::updateTable()
{
	model->clear();
	model->insertColumns(0,4);
	model->insertRows(0,all.size());
	model->setHorizontalHeaderLabels(QStringList()<<tr("ID")<<tr("Description")<<tr("Price")<<tr("Flags"));
	for(int i=0;i<all.size();i++){
		model->setData(model->index(i,0),all[i].shipid().value());
		model->setData(model->index(i,1),all[i].description().value());
		model->setData(model->index(i,2),cent2str(all[i].cost()));
		model->setData(model->index(i,3),all[i].flags().value());
	}
	table->resizeColumnsToContents();
}

void MShippingEditor::changeDescription()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get shipping
	MOShipping s=all[idx.row()];
	//get new value
	QString r=QInputDialog::getText(this,tr("Shipping Option Description"),tr("Please select a new description for this shipping option:"),QLineEdit::Normal,s.description());
	if(r=="")return;
	s.setdescription(r);
	MTChangeShipping cs=MTChangeShipping::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes: %1").arg(cs.errorString()));
		return;
	}
	all[idx.row()]=cs.getshipping();
	updateTable();
}

void MShippingEditor::changePrice()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get shipping
	MOShipping s=all[idx.row()];
	//get new value
	bool b;
	int r=MCentDialog::getCents(this,tr("Shipping Option Price"),tr("Please select a new price for this shipping option:"),s.cost(),1000000000,&b);
	if(!b)return;
	s.setcost(r);
	MTChangeShipping cs=MTChangeShipping::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes: %1").arg(cs.errorString()));
		return;
	}
	all[idx.row()]=cs.getshipping();
	updateTable();
}
void MShippingEditor::changeAvail()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//get shipping
	MOShipping s=all[idx.row()];
	//get new value
	s.setflags(MFlagEditor::edit(this,s.flags(),tr("Edit Flags of shipping option '%1'.").arg(s.description())));
	//save
	MTChangeShipping cs=MTChangeShipping::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the changes."));
		return;
	}
	all[idx.row()]=s;
	updateTable();
}
void MShippingEditor::addNew()
{
	//get data
	//TODO: use a single dialog
	QString dsc=QInputDialog::getText(this,tr("Shipping Option Description"),tr("Please select a new description for this new shipping option:"));
	if(dsc=="")return;
	bool b;
	int prc=MCentDialog::getCents(this,tr("Shipping Option Price"),tr("Please select a new price for this new shipping option:"),0,1000000000,&b);
	if(!b)return;
	//create the option
	MOShipping s;
	s.setdescription(dsc);
	s.setcost(prc);
	MTCreateShipping cs=MTCreateShipping::query(s);
	if(cs.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Could not store the data: %1").arg(cs.errorString()));
		return;
	}
	all.append(cs.getshipping());
	updateTable();
}

void MShippingEditor::deleteShip()
{
	//find item
	QModelIndexList lst=table->selectionModel()->selectedIndexes();
	if(lst.size()<1)return;
	QModelIndex idx=lst[0];
	//ask
	MOShipping s=all[idx.row()];
	if(QMessageBox::question(this,tr("Really Delete?"),tr("Really delete shipping option '%1'?").arg(s.description()),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)
		return;
	//get shipping
	int id=s.shipid();
	MTDeleteShipping ds=MTDeleteShipping::query(id);
	if(ds.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to delete this option: %1").arg(ds.errorString()));
		return;
	}
	all.removeAt(idx.row());
	updateTable();
}
