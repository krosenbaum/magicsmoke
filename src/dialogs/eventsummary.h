//
// C++ Interface: eventsummary
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_EVENTSUMMARY_H
#define MAGICSMOKE_EVENTSUMMARY_H

#include <QDialog>
#include <QMap>

#include "MOOrder"
#include "MOEvent"
#include "odtrender.h"

class QTableView;
class QStandardItemModel;
class QLabel;

/**shows a summary for the event*/
class MEventSummary:public QDialog
{
	Q_OBJECT
	public:
		/**creates a new summary dialog, requests data from server*/
		MEventSummary(QWidget*parent,qint64 eventid);
		/**deletes MEventSummary*/
		~MEventSummary();
		
		///set a test template
		void setTestTemplateFile(const QString&);
		
	private slots:
		/**internal: print summary*/
		void print();
		/**internal: save summary to file*/
		void saveas();
		
		//used for ODT rendering:
		void getVariable(QString,QVariant&);
		void getLoopIterations(QString,int&);
		void getLoopVariable(QString,int,QString,QVariant&);
		void setLoopIteration(QString,int);
		
	private:
		qint64 eventid;
		MOEvent event;
		int nreserved,ncancelled,ntotaltickets,ntotalmoney;
		struct Tickets{
                        Tickets(){}
			Tickets(int p,QString c=QString()):price(p),category(c){}
			int price=0,bought=0,used=0,unused=0;
                        QString category;
		};
		QList<Tickets>tickets;
		struct Comment{
			int custid,orderid;
			QString custname,comment;
		};
		QList<Comment>comments;
		QList<int>orderids;
		QMap<int,MOOrder>orders;
		QMap<int,MOCustomerInfo>customers;
		QMap<QString,int>loopiter;
		QString testTemplate;
		
		//get summary
		void getSummaryData();
};

#endif
