//
// C++ Implementation: customer
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "customerdlg.h"
#include "msinterface.h"
#include "misc.h"

#include "MTChangeCustomer"
#include "MTChangeCustomerMail"
#include "MTCreateContactType"
#include "MTCreateCountry"
#include "MTCreateCustomer"
#include "MTDeleteCustomer"
#include "MTGetAddress"
#include "MTGetAllContactTypes"
#include "MTGetAllCountries"
#include "MTGetAllCustomerNames"
#include "MTGetCreateCustomerHints"
#include "MTGetCustomer"
#include "MTResetCustomerPassword"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QApplication>
#include <QDomElement>
#include <QFormLayout>
#include <QGridLayout>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSignalMapper>
#include <QSortFilterProxyModel>
#include <QStackedLayout>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextEdit>

MCustomerListDialog::MCustomerListDialog(QWidget*par,bool isselect,qint64 presel)
	:QDialog(par)
{
	if(isselect)
		setWindowTitle(tr("Select a Customer"));
	else
		setWindowTitle(tr("Customers"));
	
	//layout
	QVBoxLayout*vl,*vl2;
	QHBoxLayout*hl;
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout,10);
	hl->addLayout(vl2=new QVBoxLayout,10);
	vl2->addWidget(m_filter=new QLineEdit,0);
	vl2->addWidget(m_listview=new QListView,10);
	m_listmodel=new QStandardItemModel(this);
	m_proxymodel=new QSortFilterProxyModel(this);
	m_proxymodel->setSourceModel(m_listmodel);
	m_proxymodel->setDynamicSortFilter(true);
	m_proxymodel->setSortCaseSensitivity(Qt::CaseInsensitive);
	connect(m_filter,SIGNAL(textChanged(const QString&)),m_proxymodel,SLOT(setFilterFixedString(const QString&)));
	m_proxymodel->setFilterCaseSensitivity(Qt::CaseInsensitive);
	m_listview->setModel(m_proxymodel);
	m_listview->setEditTriggers(QAbstractItemView::NoEditTriggers);
	hl->addLayout(vl2=new QVBoxLayout,0);
	QPushButton*p;
	vl2->addWidget(p=new QPushButton(tr("Details...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editCustomer()));
	vl2->addWidget(p=new QPushButton(tr("Create new...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newCustomer()));
	p->setEnabled(req->hasRight(req->RCreateCustomer));
	vl2->addWidget(p=new QPushButton(tr("Delete...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(deleteCustomer()));
	p->setEnabled(req->hasRight(req->RDeleteCustomer));
	vl2->addStretch(2);
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	if(isselect){
		hl->addWidget(p=new QPushButton(tr("Select")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(accept()));
		connect(m_listview,SIGNAL(doubleClicked(const QModelIndex&)),this,SLOT(accept()));
		hl->addWidget(p=new QPushButton(tr("Cancel")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	}else{
		hl->addWidget(p=new QPushButton(tr("Close")),0);
		connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	}
	
	//update data
	updateList(presel);
}

void MCustomerListDialog::updateList(int nid)
{
	//check for current selection
	if(nid<0){
		QModelIndex idx=m_listview->currentIndex();
		if(idx.isValid()){
			int i=m_proxymodel->data(idx,Qt::UserRole).toInt();
			if(i>=0&&i<m_list.size())
				nid=m_list[i].customerid();
		}
	}
	//go to server
	MTGetAllCustomerNames gac=req->queryGetAllCustomerNames();
	if(gac.hasError())return;
	m_list.clear();
	m_list=gac.getcustomers();
	//update widget
	m_listmodel->clear();
	m_listmodel->insertRows(0,m_list.size());
	m_listmodel->insertColumn(0);
	for(int i=0;i<m_list.size();i++){
		QModelIndex idx=m_listmodel->index(i,0);
		m_listmodel->setData(idx,m_list[i].fullName());
		m_listmodel->setData(idx,i,Qt::UserRole);
	}
	m_listmodel->sort(0);
	//reset current
	if(nid>=0)
	for(int i=0;i<m_proxymodel->rowCount();i++){
		QModelIndex idx=m_proxymodel->index(i,0);
		int j=m_proxymodel->data(idx,Qt::UserRole).toInt();
		if(j<0 || j>=m_list.size())continue;
		if(nid==m_list[j].customerid()){
			m_listview->setCurrentIndex(idx);
			break;
		}
	}
}

MOCustomerInfo MCustomerListDialog::getCustomer()
{
	//get selection
	QModelIndex idx=m_listview->currentIndex();
	if(!idx.isValid())return MOCustomerInfo();
	//return object
	int i=m_proxymodel->data(idx,Qt::UserRole).toInt();
	if(i<0||i>=m_list.size())return MOCustomerInfo();
	return m_list[i];
}

void MCustomerListDialog::newCustomer()
{
	MOCustomer c=MNewCustomerWizard::getNewCustomer(this);
	if(c.isValid())
		updateList(c.id());
// 	MCustomerDialog cd(MOCustomer(),this);
// 	if(cd.exec()==QDialog::Accepted)
// 		updateList(cd.getCustomer().id());
}
void MCustomerListDialog::editCustomer()
{
	//get selection
	QModelIndex idx=m_listview->currentIndex();
	if(!idx.isValid())return;
	//get object
	int i=m_proxymodel->data(idx,Qt::UserRole).toInt();
	if(i<0||i>=m_list.size())return;
	MTGetCustomer gc=req->queryGetCustomer(m_list[i].customerid());
	if(gc.hasError())return;
	//open dialog
	MCustomerDialog cd(gc.getcustomer().value(),this);
	if(cd.exec()==QDialog::Accepted)
		updateList(m_list[i].customerid());
}
void MCustomerListDialog::deleteCustomer()
{
	//get selection
	QModelIndex idx=m_listview->currentIndex();
	if(!idx.isValid())return;
	//get pointer to customer
	int dusr=m_proxymodel->data(idx,Qt::UserRole).toInt();
	if(dusr<0||dusr>=m_list.size())return;
	//show delete dialog
	QDialog d;
	d.setWindowTitle(tr("Delete Customer"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("Really delete this customer (%1)?").arg(m_proxymodel->data(idx).toString())));
	vl->addSpacing(20);
	QCheckBox*cb;
	vl->addWidget(cb=new QCheckBox(tr("merge with other entry:")));
	QComboBox*cm;
	vl->addWidget(cm=new QComboBox);
	cm->setModel(m_listmodel);
	cm->setEnabled(cb->isEnabled());
	connect(cb,SIGNAL(toggled(bool)),cm,SLOT(setEnabled(bool)));
	vl->addSpacing(20);
	vl->addStretch(10);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&Yes")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("&No")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	//delete it
	int musr=m_listmodel->data(m_listmodel->index(cm->currentIndex(),0),Qt::UserRole).toInt();
	MTDeleteCustomer dc=req->queryDeleteCustomer(m_list[dusr].customerid(),cb->isChecked()?m_list[musr].customerid().value():-1);
	if(dc.hasError()){
		QMessageBox::warning(this,tr("Error"),tr("Failed to delete customer: %1").arg(dc.errorString()));
		return;
	}
	//update view
	updateList();
}


/****************************************************************/

static const int ContactIdRole = Qt::UserRole;
static const int ContactTypeIdRole = Qt::UserRole+1;

MCustomerDialog::MCustomerDialog(MOCustomer c,QWidget*par)
	:QDialog(par),m_cust(c)
{
	if(m_cust.isValid())
		setWindowTitle(tr("Customer %1").arg(m_cust.name()));
	else
		setWindowTitle(tr("New Customer"));
	
	//layout
	QGridLayout *gl;
	QVBoxLayout*vl,*vl2;
	QHBoxLayout*hl;
	QTabWidget*tab;
	QPushButton*p;
	QWidget*w;
	
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(tab=new QTabWidget);
	tab->addTab(w=new QWidget,tr("Customer"));
	
	QMenu*menu=new QMenu(this);
	menu->addAction(tr("Change Mail Address"),this,SLOT(changeMail()));
	menu->addAction(tr("Reset Password"),this,SLOT(resetLogin()));
	
	w->setLayout(gl=new QGridLayout);
	int lc=0;
	gl->addWidget(new QLabel(tr("Name:")),lc,0);
	gl->addWidget(m_name=new QLineEdit(m_cust.name()),lc,1);
	gl->addWidget(new QLabel(tr("First Name:")),++lc,0);
	gl->addWidget(m_fname=new QLineEdit(m_cust.firstname()),lc,1);
	gl->addWidget(new QLabel(tr("Title:")),++lc,0);
	gl->addWidget(m_title=new QLineEdit(m_cust.title()),lc,1);
	gl->setRowMinimumHeight(++lc,10);
	gl->addWidget(new QLabel(tr("Web-Login/eMail:")),++lc,0);
	gl->addWidget(m_mail=new QLabel(m_cust.email()),lc,1);
	gl->addWidget(p=new QPushButton(tr("Edit Login")),++lc,1);
	p->setMenu(menu);
	gl->setRowMinimumHeight(++lc,10);
	gl->addWidget(new QLabel(tr("Comment:")),++lc,0,1,2);
	gl->addWidget(m_comm=new QTextEdit,++lc,0,1,2);
	m_comm->setPlainText(m_cust.comments());
	gl->setRowStretch(lc,10);
	gl->setRowMinimumHeight(++lc,15);
	
	tab->addTab(w=new QWidget,tr("Addresses"));
	w->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(m_addr=new MAddressListWidget(0,m_cust.addresses()),10);
	vl2->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Add Address")),0);
	connect(p,SIGNAL(clicked()),m_addr,SLOT(createAddress()));
	
	tab->addTab(w=new QWidget,tr("Contact Information"));
	w->setLayout(vl2=new QVBoxLayout);
	vl2->addWidget(m_contact=new QTableView,10);
	m_contact->setModel(m_contactmodel=new QStandardItemModel(this));
	m_contact->setItemDelegate(new MContactTableDelegate(this));
	vl2->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Add")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(addContact()));
	hl->addWidget(p=new QPushButton(tr("Remove")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(removeContact()));
	QList<MOContact>cts=m_cust.contacts();
	m_contactmodel->insertColumns(0,2);
	m_contactmodel->insertRows(0,cts.size());
	m_contactmodel->setHorizontalHeaderLabels(QStringList()<<tr("Type","table: contact type")<<tr("Contact","table: contact info"));
	for(int i=0;i<cts.size();i++){
		m_contactmodel->setData(m_contactmodel->index(i,0),
			cts[i].contacttype().value().contacttype().value());
		m_contactmodel->setData(m_contactmodel->index(i,0),
			cts[i].contactid().value(), ContactIdRole);
		m_contactmodel->setData(m_contactmodel->index(i,0),
			cts[i].contacttypeid().value(), ContactTypeIdRole);
		m_contactmodel->setData(m_contactmodel->index(i,1),
			cts[i].contact().value());
	}
	m_contact->resizeColumnsToContents();
	
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Save")),0);
	//make sure saving is faster than closing the dialog
	connect(p,SIGNAL(clicked()),this,SLOT(accept()),Qt::QueuedConnection);
	connect(p,SIGNAL(clicked()),this,SLOT(save()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
	setSizeGripEnabled(true);
}

void MCustomerDialog::addContact()
{
	//create new row
	int rn=m_contactmodel->rowCount();
	m_contactmodel->insertRows(rn,1);
	//set basic data (guaranteed invalid IDs)
	QModelIndex idx=m_contactmodel->index(rn,0);
	m_contactmodel->setData(idx,-1,ContactIdRole);
	m_contactmodel->setData(idx,-1,ContactTypeIdRole);
	//scroll to it, edit it
	m_contact->scrollTo(idx);
	m_contact->edit(idx);
}

void MCustomerDialog::removeContact()
{
	QModelIndex idx=m_contact->currentIndex();
	if(!idx.isValid())return;
	m_contactmodel->removeRows(idx.row(),1);
}

MOCustomer MCustomerDialog::getCustomer()
{
	//copy data from input fields
	m_cust.setname(m_name->text());
	m_cust.setfirstname(m_fname->text());
	m_cust.settitle(m_title->text());
	m_cust.setaddresses(m_addr->addressList());
	m_cust.setcomments(m_comm->toPlainText());
	QList<MOContact>clst;
	for(int i=0;i<m_contactmodel->rowCount();i++){
		MOContact c;
		QModelIndex idx0=m_contactmodel->index(i,0);
		c.setcontactid(m_contactmodel->data(idx0,ContactIdRole).toInt());
		c.setcontacttypeid(m_contactmodel->data(idx0,ContactTypeIdRole).toInt());
		c.setcustomerid(m_cust.customerid());
		c.setcontact(m_contactmodel->data(m_contactmodel->index(i,1)).toString());
		clst<<c;
	}
	m_cust.setcontacts(clst);
	return m_cust;
}

void MCustomerDialog::save()
{
	getCustomer();
	if(m_cust.isValid()){
		MTChangeCustomer cc=req->queryChangeCustomer(m_cust);
		if(cc.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while changing customer data: %1").arg(cc.errorString()));
			return;
		}
		m_cust=cc.getcustomer();
	}else{
		MTCreateCustomer cc=req->queryCreateCustomer(m_cust);
		if(cc.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating customer data: %1").arg(cc.errorString()));
			return;
		}
		m_cust=cc.getcustomer();
	}
}

void MCustomerDialog::changeMail()
{
	QString mail=QInputDialog::getText(this, tr("Change Mail Address"), tr("Please enter the mail address for this customer to log into the web portal:"),QLineEdit::Normal, m_mail->text());
	if(mail!=""){
		MTChangeCustomerMail ccm=req->queryChangeCustomerMail(m_cust.customerid(),mail);
		if(ccm.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Unable to set new email: %1").arg(ccm.errorString()));
			return;
		}
		m_mail->setText(mail);
		m_cust.setemail(mail);
	}
}

void MCustomerDialog::resetLogin()
{
	MTResetCustomerPassword rcp=req->queryResetCustomerPassword(m_cust.customerid());
	if(rcp.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while resetting password: %1").arg(rcp.errorString()));
	else
		QMessageBox::information(this,tr("Password Reset"),tr("A password reset mail has been sent to the customer."));
}


/********************************************************************************/

MContactTableDelegate::MContactTableDelegate(QObject*p)
	:QItemDelegate(p)
{
}

QWidget *MContactTableDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &/* option */,
        const QModelIndex & index ) const
{
	if(index.column()==0){//contact type: dropdown
		QComboBox *editor = new QComboBox(parent);
		editor->installEventFilter(const_cast<MContactTableDelegate*>(this));
		return editor;
	}
	if(index.column()==1){//contact string: normal delegate, line edit
		QLineEdit*editor=new QLineEdit(parent);
		editor->installEventFilter(const_cast<MContactTableDelegate*>(this));
		return editor;
	}
	
	return 0;
}

void MContactTableDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	int c=index.column();
	if(c==0){//type
		if(m_typelist.size()==0)m_typelist=MTGetAllContactTypes::query().gettypes();
		QComboBox*box=(QComboBox*)editor;
		int curidx=0,cur=index.model()->data(index,ContactTypeIdRole).toInt();
		for(int i=0;i<m_typelist.size();i++){
			box->addItem(m_typelist[i].contacttype(),m_typelist[i].contacttypeid().value());
			if(m_typelist[i].contacttypeid().value()==cur)
				curidx=i;
		}
		if(req->hasRight(req->RCreateContactType))
			box->addItem(tr("(New Contact Type)"),-1);
		box->setCurrentIndex(curidx);
	}
	if(c==1){//content
		((QLineEdit*)editor)->setText(index.model()->data(index).toString());
	}
}

void MContactTableDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
	const QModelIndex &index) const
{
	int c=index.column();
	if(c==0){//type
		//get content of box
		QComboBox*box=(QComboBox*)editor;
		QString tn=box->itemText(box->currentIndex());
		int tid=box->itemData(box->currentIndex()).toInt();
		if(tid<0){//create new one
			QDialog d(editor->window());
			d.setWindowTitle(tr("Create new Contact Type"));
			QFormLayout*fl;
			QVBoxLayout*vl;
			QHBoxLayout*hl;
			QLineEdit*nm,*up;
			QPushButton*p;
			d.setLayout(vl=new QVBoxLayout);
			vl->addLayout(fl=new QFormLayout);
			fl->addRow(tr("Contact Type Name:"),nm=new QLineEdit);
			fl->addRow(tr("Contact Type URI Prefix:"),up=new QLineEdit);
			vl->addStretch(1);
			vl->addLayout(hl=new QHBoxLayout);
			hl->addWidget(p=new QPushButton(tr("Ok")));
			connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
			hl->addWidget(p=new QPushButton(tr("Cancel")));
			connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
			if(d.exec()!=QDialog::Accepted)return;
			MTCreateContactType ct=MTCreateContactType::query(nm->text(),up->text());
			if(ct.hasError()){
				QMessageBox::warning(editor->window(),tr("Warning"),tr("Error while creating contact type: %1").arg(ct.errorString()));
				return;
			}
			//get data
			MOContactType cty=ct.getcontacttype();
			tn=cty.contacttype();
			tid=cty.contacttypeid();
			//clear cache, so it is retrieved again next time
			m_typelist.clear();
		}
		//set new contact type
		model->setData(index,tn);
		model->setData(index,tid,ContactTypeIdRole);
	}
	if(c==1){//content
		model->setData(index,((QLineEdit*)editor)->text());
	}
}

/*****************************************************************************/

MAddressWidget::MAddressWidget(QWidget*parent,bool isselect)
	:QFrame(parent)
{
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QPushButton*p;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(m_lab=new QLabel(" "),10);
	vl->addLayout(hl=new QHBoxLayout,0);
	if(isselect){
		hl->addWidget(p=new QPushButton(tr("Select")));
		connect(p,SIGNAL(clicked()),this,SLOT(selectClick()));
		connect(this,SIGNAL(addrValid(bool)),p,SLOT(setEnabled(bool)));
	}
	hl->addWidget(p=new QPushButton(tr("Edit")));
	connect(p,SIGNAL(clicked()),this,SLOT(editClick()));
	connect(this,SIGNAL(addrValid(bool)),p,SLOT(setEnabled(bool)));
	hl->addWidget(p=new QPushButton(tr("Delete")));
	connect(p,SIGNAL(clicked()),this,SLOT(delClick()));
	connect(this,SIGNAL(addrValid(bool)),p,SLOT(setEnabled(bool)));
}

MOAddress MAddressWidget::address()const{return m_addr;}

void MAddressWidget::setAddress(const MOAddress&a)
{
	m_addr=a;
	emit addrValid(!a.addressid().isNull());
	setEnabled(!a.isdeleted().value());
	redisplay();
	emit addressChanged(m_addr);
}

void MAddressWidget::redisplay()
{
	m_lab->setText(m_addr.fullAddress());
	m_lab->setMinimumSize(m_lab->sizeHint());
	m_lab->adjustSize();
	adjustSize();
}

void MAddressWidget::setAddress(qint64 id)
{
	setAddress(MTGetAddress::query(id).getaddress());
}

void MAddressWidget::clearAddress()
{
	m_addr=MOAddress();
	emit addrValid(false);
	setEnabled(false);
	redisplay();
	emit addressChanged(m_addr);
}

void MAddressWidget::editClick()
{
	MAddressDialog d(this,m_addr);
	if(d.exec()==QDialog::Accepted){
		m_addr=d.address();
		emit addrValid(!m_addr.addressid().isNull());
		setEnabled(!m_addr.isdeleted().value());
		redisplay();
		emit addressEdited(m_addr);
		emit addressChanged(m_addr);
	}
}

void MAddressWidget::delClick()
{
	if(QMessageBox::question(this,tr("Delete Address"),tr("Really delete this address?\n%1").arg(m_addr.fullAddress()),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)
		return;
	m_addr.setisdeleted(true);
	setEnabled(false);
	emit addressEdited(m_addr);
	emit addressDeleted(m_addr);
	emit addressChanged(m_addr);
}

void MAddressWidget::selectClick()
{
	emit addressSelected(m_addr);
	emit addressSelected(m_addr.addressid());
}

/*****************************************************************************/

//number of columns in the address list widget
#define ADDRCOLS 3

MAddressListWidget::MAddressListWidget(QWidget*parent,const QList<MOAddress>&addr,bool isselect)
	:QScrollArea(parent),m_select(isselect)
{
	map=new QSignalMapper(this);
	connect(map,SIGNAL(mapped(int)),this,SLOT(addrEdit(int)),Qt::DirectConnection);
	QWidget*w;
	setWidget(w=new QWidget);
	setWidgetResizable(true);
	QVBoxLayout*vl;
	w->setLayout(vl=new QVBoxLayout);
	vl->addLayout(m_layout=new QGridLayout,0);
	vl->addStretch(10);
	for(int i=0;i<addr.size();i++)
		addAddr(addr[i]);
}

void MAddressListWidget::addrEdit(int id)
{
	qDebug("edited addr %i",id);
	MAddressWidget*aw=qobject_cast<MAddressWidget*>(map->mapping(id));
	if(aw==0)return;
	qDebug("handling edit");
	m_addr[id]=aw->address();
	aw->adjustSize();
	widget()->adjustSize();
}

void MAddressListWidget::addrSel(const MOAddress&a)
{
	m_sel=a;
}

void MAddressListWidget::addAddr(const MOAddress&addr,bool vis)
{
	//create
	int id=m_addr.size();
	m_addr<<addr;
	MAddressWidget *aw=new MAddressWidget(this,m_select);
	aw->setAddress(addr);
	aw->setFrameStyle(QFrame::Box|QFrame::Raised);
	map->setMapping(aw,id);
	//display
	m_layout->addWidget(aw,id/ADDRCOLS,id%ADDRCOLS);
	aw->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::MinimumExpanding);
	aw->adjustSize();
	widget()->adjustSize();
	if(vis)ensureWidgetVisible(aw);
	//connections to myself
	connect(aw,SIGNAL(addressEdited(const MOAddress&)),map,SLOT(map()),Qt::DirectConnection);
	connect(aw,SIGNAL(addressSelected(const MOAddress&)),this,SLOT(addrSel(const MOAddress&)),Qt::DirectConnection);
	//proxy connections
	connect(aw,SIGNAL(addressEdited(const MOAddress&)),this,SIGNAL(addressEdited(const MOAddress&)),Qt::QueuedConnection);
	connect(aw,SIGNAL(addressChanged(const MOAddress&)),this,SIGNAL(addressChanged(const MOAddress&)),Qt::QueuedConnection);
	connect(aw,SIGNAL(addressDeleted(const MOAddress&)),this,SIGNAL(addressDeleted(const MOAddress&)),Qt::QueuedConnection);
	connect(aw,SIGNAL(addressSelected(const MOAddress&)),this,SIGNAL(addressSelected(const MOAddress&)),Qt::QueuedConnection);
	connect(aw,SIGNAL(addressSelected(qint64)),this,SIGNAL(addressSelected(qint64)),Qt::QueuedConnection);
}

void MAddressListWidget::preselect(int i)
{
	if(i<0 || i>=m_addr.size())return;
	MAddressWidget*aw=qobject_cast<MAddressWidget*>(map->mapping(i));
	if(!aw)return;
	//set new selection
	aw->setFrameStyle(QFrame::Box | QFrame::Plain);
	aw->setLineWidth(2);
	m_sel=m_addr[i];
	//make sure it is visible
	ensureWidgetVisible(aw);
	aw->adjustSize();
	widget()->adjustSize();
}

void MAddressListWidget::preselect(const MOAddress& addr)
{
	for(int i=0;i<m_addr.size();i++){
		if(m_addr[i].addressid()==addr.addressid()){
			preselect(i);
			break;
		}
	}
}

void MAddressListWidget::createAddress()
{
	MAddressDialog d(this);
	if(d.exec()!=QDialog::Accepted)return;
	addAddr(d.address());
	emit addressCreated(m_addr[m_addr.size()-1]);
}

/*****************************************************************************/

MAddressDialog::MAddressDialog(QWidget*parent,MOAddress addr)
	:QDialog(parent),m_addr(addr)
{
	if(!m_addr.addressid().isNull())
		setWindowTitle(tr("Edit Address"));
	else
		setWindowTitle(tr("Create Address"));
	
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QFormLayout*fl;
	QPushButton*p;
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(fl=new QFormLayout,0);
	if(m_addr.isValid())
		fl->addRow(tr("Last used:"),new QLabel(MLocalFormat().formatDateTime(m_addr.lastused())));
	fl->addRow(tr("Name:"),m_name=new QLineEdit(m_addr.name()));
	fl->addRow(tr("Address:"),m_addr1=new QLineEdit(m_addr.addr1()));
	fl->addRow(tr("Address:"),m_addr2=new QLineEdit(m_addr.addr2()));
	fl->addRow(tr("City:"),m_city=new QLineEdit(m_addr.city()));
	fl->addRow(tr("State:"),m_state=new QLineEdit(m_addr.state()));
	fl->addRow(tr("ZIP Code:"),m_zip=new QLineEdit(m_addr.zipcode()));
	fl->addRow(tr("Country:"),hl=new QHBoxLayout);
	hl->addWidget(m_country=new QLineEdit(m_addr.country().value().name()),10);
	m_country->setReadOnly(true);
	hl->addWidget(p=new QPushButton("..."),0);
	connect(p,SIGNAL(clicked()),this,SLOT(selectCountry()));
	
	vl->addStretch(10);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

MOAddress MAddressDialog::address()
{
	//set pseudo-ID to make it appear valid to other widgets
	if(m_addr.addressid().isNull())m_addr.setaddressid(-1);
	//set properties
	m_addr.setname(m_name->text());
	m_addr.setaddr1(m_addr1->text());
	m_addr.setaddr2(m_addr2->text());
	m_addr.setcity(m_city->text());
	m_addr.setstate(m_state->text());
	m_addr.setzipcode(m_zip->text());
	//country and countryid is set by selectCountry
	
	return m_addr;
}

//static
MOCountry MAddressDialog::createNewCountry(QWidget* parent)
{
	QDialog d(parent);
	d.setWindowTitle(tr("Create New Country"));
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QFormLayout*fl;
	QLineEdit*name,*abbr;
	QPushButton*p;
	d.setLayout(vl=new QVBoxLayout);
	vl->addLayout(fl=new QFormLayout);
	fl->addRow(tr("Country Name:"),name=new QLineEdit);
	fl->addRow(tr("Abbreviation:"),abbr=new QLineEdit);
	vl->addStretch(10);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//wait and check
	if(d.exec()!=QDialog::Accepted)return MOCountry();
	if(name->text().trimmed()=="" || abbr->text().trimmed()==""){
		QMessageBox::warning(parent,tr("Warning"),tr("The country name and abbreviation must contain something!"));
		return MOCountry();
	}
	//create
	MTCreateCountry cc=MTCreateCountry::query(abbr->text().trimmed(),name->text().trimmed());
	if(cc.hasError()){
		QMessageBox::warning(parent,tr("Warning"),tr("Error while creating country: %1").arg(cc.errorString()));
		return MOCountry();
	}
	return cc.getcountry();
}

void MAddressDialog::selectCountry()
{
	//get countries
	QList<MOCountry>cntry=MTGetAllCountries::query().getcountries();
	QStringList clst;
	int cur=0;
	QString curstr=m_country->text();
	for(int i=0;i<cntry.size();i++){
		clst<<cntry[i].name().value().trimmed();
		if(cntry[i].name().value() == curstr)cur=i;
	}
	if(req->hasRight(req->RCreateCountry))
		clst<<tr(" Create New Country...","this pseudo-entry must contain leading space to distinguish it from genuine countries");
	//display selection
	bool ok;
	QString it=QInputDialog::getItem(this,tr("Select Country"),tr("Please select a country:"),clst,cur,false,&ok);
	if(!ok || it=="")return;
	//get selection
	for(int i=0;i<cntry.size();i++){
		if(it == cntry[i].name().value()){
			m_addr.setcountry(cntry[i]);
			m_addr.setcountryid(cntry[i].id());
			m_country->setText(cntry[i].name());
			return;
		}
	}
	//not found: must be the create new entry
	MOCountry cc=createNewCountry(this);
	if(cc.id().isNull())return;
	m_addr.setcountry(cc);
	m_addr.setcountryid(m_addr.country().value().id());
	m_country->setText(m_addr.country().value().name());
}

/*****************************************************************************/

MAddressChoiceDialog::MAddressChoiceDialog(QWidget*par,const MOCustomer&c)
	:QDialog(par),m_cust(c)
{
	m_needupdate=m_unsel=false;
	setWindowTitle(tr("Chose an Address"));
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(m_lwidget=new MAddressListWidget(this,m_cust.addresses(),true),10);
	connect(m_lwidget,SIGNAL(addressChanged(const MOAddress&)),this,SLOT(changed()));
	connect(m_lwidget,SIGNAL(addressDeleted(const MOAddress&)),this,SLOT(changed()));
	connect(m_lwidget,SIGNAL(addressCreated(const MOAddress&)),this,SLOT(changed()));
	connect(m_lwidget,SIGNAL(addressEdited(const MOAddress&)),this,SLOT(changed()));
	//on selection: first update customer, then close window
	connect(m_lwidget,SIGNAL(addressSelected(qint64)), this,SLOT(updateCustomer()), Qt::DirectConnection);
	
	vl->addLayout(m_btnlayout=hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Add Address")),0);
	connect(p,SIGNAL(clicked()),m_lwidget,SLOT(createAddress()));
	hl->addSpacing(20);
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

void MAddressChoiceDialog::addUnselectButton(const QString& t)
{
	m_btnlayout->insertSpacing(2,20);
	QPushButton*p;
	m_btnlayout->insertWidget(3,p=new QPushButton(t));
	connect(p,SIGNAL(clicked()),this,SLOT(unselect()));
}

MOAddress MAddressChoiceDialog::address()const
{
	if(m_unsel)return MOAddress();
	else return m_lwidget->selection();
}

qint64 MAddressChoiceDialog::addressId()const
{
	if(m_unsel)return -1;
	else return address().addressid();
}
void MAddressChoiceDialog::changed(){m_needupdate=true;}

void MAddressChoiceDialog::updateCustomer()
{
	setEnabled(false);
	if(m_needupdate){
		//do update
		m_cust.setaddresses(m_lwidget->addressList());
		MTChangeCustomer cc=MTChangeCustomer::query(m_cust);
		if(cc.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Unable to save changes made to addresses: %1").arg(cc.errorString()));
			return;
		}
		m_cust=cc.getcustomer();
	}
	//close dialog successfully
	accept();
}

void MAddressChoiceDialog::preselect(const MOAddress& addr)
{
	m_lwidget->preselect(addr);
}

void MAddressChoiceDialog::unselect()
{
	m_unsel=true;
	accept();
}

/*****************************************************************************/

MNewCustomerWizard::MNewCustomerWizard(QWidget* parent, Qt::WindowFlags f): QDialog(parent, f)
{
	QWidget*w;
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	QGridLayout*gl;
	QStackedLayout *sl;
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(sl=new QStackedLayout);
	//button bar
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p,*pprev,*pnext,*pdone;
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	p->setIcon(QIcon(":/cancel.png"));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	hl->addWidget(pprev=p=new QPushButton(tr("Back")));
	p->setIcon(QIcon(":/prev.png"));
	connect(p,&QPushButton::clicked,[=](){sl->setCurrentIndex(sl->currentIndex()-1);});
	hl->addWidget(pnext=p=new QPushButton(tr("Next")));
	p->setIcon(QIcon(":/next.png"));
	connect(p,&QPushButton::clicked,[=](){sl->setCurrentIndex(sl->currentIndex()+1);});
	hl->addWidget(pdone=p=new QPushButton(tr("Finish")));
	p->setIcon(QIcon(":/done.png"));
	connect(p,SIGNAL(clicked()),this,SLOT(save()));
	auto pagechange=[=](int idx){
		pprev->setEnabled(idx>0);
		int max=sl->count()-1;
		pnext->setEnabled(idx<max);
		pdone->setEnabled(idx>=max);
	};
	connect(sl,&QStackedLayout::currentChanged,pagechange);
	
	//get helper data
	MTGetCreateCustomerHints cch=req->queryGetCreateCustomerHints();
	QStringList titles,cities,states;
	if(!cch.hasError()){
		m_countrylist=cch.getcountries();
		m_typelist=cch.getcontacttypes();
		titles=cch.gettitles();
		cities=cch.getcities();
		states=cch.getstates();
	}else{
		//try something else
		m_countrylist=req->queryGetAllCountries().getcountries();
		m_typelist=req->queryGetAllContactTypes().gettypes();
	}
	
	//address page
	sl->addWidget(w=new QWidget);
	int row=0;
	w->setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("Please enter name and address information.\nPlease enter it also if it is not needed immediately.")),row,0,1,2);
	gl->addWidget(new QLabel(tr("Name:")),++row,0);
	gl->addLayout(hl=new QHBoxLayout,row,1);
	hl->addWidget(m_title=new QComboBox,1);
	m_title->setEditable(true);
	m_title->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	m_title->setMinimumContentsLength(8);
	m_title->lineEdit()->setPlaceholderText(tr("Title"));
	for(const QString&tt:titles)m_title->addItem(tt);
	hl->addWidget(m_name=new QLineEdit,2);
	m_name->setPlaceholderText(tr("Family Name"));
	hl->addWidget(m_fname=new QLineEdit,2);
	m_fname->setPlaceholderText(tr("Given Name"));
	gl->addWidget(new QLabel(tr("Address:")),++row,0);
	gl->addWidget(m_addr1=new QLineEdit,row,1);
	m_addr1->setPlaceholderText(tr("123 Example Street"));
	gl->addWidget(new QLabel(tr("City:")),++row,0);
	gl->addLayout(hl=new QHBoxLayout,row,1);
	hl->addWidget(m_zipcode=new QLineEdit,0);
	m_zipcode->setPlaceholderText(tr("Zip Code"));
	hl->addWidget(m_city=new QComboBox,1);
	m_city->setEditable(true);
	m_city->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	m_city->setMinimumContentsLength(5);
	m_city->lineEdit()->setPlaceholderText(tr("Chose City"));
	for(const QString&ct:cities)m_city->addItem(ct);
	gl->addWidget(new QLabel(tr("State:")),++row,0);
	gl->addWidget(m_state=new QComboBox,row,1);
	m_state->setEditable(true);
	m_state->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	m_state->setMinimumContentsLength(8);
	m_state->lineEdit()->setPlaceholderText(tr("State (optional)"));
	for(const QString&st:states)m_state->addItem(st);
	gl->addWidget(new QLabel(tr("Country:")),++row,0);
	gl->addLayout(hl=new QHBoxLayout,row,1);
	hl->addWidget(m_country=new QComboBox,1);
	m_country->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	m_country->setMinimumContentsLength(10);
	updateCountry();
	if(req->hasRight(req->RCreateCountry)){
		hl->addWidget(p=new QPushButton(tr("New...")));
		connect(p,SIGNAL(clicked()),this,SLOT(newcountry()));
	}
	// ...spacing
	gl->setColumnStretch(1,1);
	gl->addWidget(new QFrame,++row,0,1,2);
	gl->setRowStretch(row,1);
	
	//contacts page
	QScrollArea*sa;
	sl->addWidget(sa=new QScrollArea);
	sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	sa->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	sa->setWidgetResizable(true);
	sa->setWidget(w=new QWidget);
	w->setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("Please enter at least one way of contacting the customer.")),row=0,0,1,2);
	for(const MOContactType&ct:m_typelist){
		gl->addWidget(new QLabel(ct.contacttype()),++row,0);
		QLineEdit*le;
		gl->addWidget(le=new QLineEdit,row,1);
		le->setPlaceholderText(tr("Please enter %1","%1=contact type name").arg(ct.contacttype()));
		m_contact.append(le);
	}
	
	// ...spacing
	gl->setColumnStretch(1,1);
	gl->addWidget(new QFrame,++row,0,1,2);
	gl->setRowStretch(row,1);

	//make sure buttons are initialized
	pagechange(sl->currentIndex());
	//make size changeable
	setSizeGripEnabled(true);
}

void MNewCustomerWizard::save()
{
	//create customer
	MOCustomer inp;
	inp.setname(m_name->text());
	inp.setfirstname(m_fname->text());
	inp.settitle(m_title->currentText());
	MOAddress adr;
	adr.setaddr1(m_addr1->text());
	adr.setcity(m_city->currentText());
	if(!m_state->currentText().trimmed().isEmpty())
		adr.setstate(m_state->currentText());
	adr.setzipcode(m_zipcode->text());
	adr.setcountryid(m_country->itemData(m_country->currentIndex()).toString());
	//complete addr
	inp.addaddresses(adr);
	//contacts
	for(int i=0;i<m_typelist.size() && i<m_contact.size();i++){
		const MOContactType&ct=m_typelist[i];
		if(ct.contacttypeid().isNull())continue;
		QLineEdit*le=m_contact[i];
		if(le==nullptr)continue;
		if(le->text().trimmed().isEmpty())continue;
		MOContact con;
		con.setcontacttypeid(ct.contacttypeid());
		con.setcontact(le->text());
		inp.addcontacts(con);
	}
	//create customer
	MTCreateCustomer cc=req->queryCreateCustomer(inp);
	if(!cc.hasError()){
		m_cust=cc.getcustomer();
		accept();
		return;
	}
	if(QMessageBox::warning(this,tr("Warning"),tr("There was an error while creating the customer: %1").arg(cc.errorString()),QMessageBox::Abort|QMessageBox::Retry)==QMessageBox::Retry)return;
	//close dialog
	accept();
}

void MNewCustomerWizard::updateCountry(QString id)
{
	//sort data
	qSort(m_countrylist.begin(),m_countrylist.end(),
	      [](const MOCountry&a,const MOCountry&b){
		      return a.name().value()<b.name().value();
	      });
	//remember which one to pre-select
	if(id.isEmpty())id=m_country->itemData(m_country->currentIndex()).toString();
	int idx=-1;
	//clear
	m_country->clear();
	//add
	for(MOCountry ct:m_countrylist){
		if(ct.id()==id)idx=m_country->count();
		m_country->addItem(ct.name(),ct.id().value());
	}
	//pre-select (last one shown or new one created)
	if(idx>=0)m_country->setCurrentIndex(idx);
}

void MNewCustomerWizard::newcountry()
{
	//dialog
	MOCountry co=MAddressDialog::createNewCountry(this);
	if(co.id().isNull())return;
	//add to list
	m_countrylist.append(co);
	updateCountry(co.id());
}

MOCustomer MNewCustomerWizard::getNewCustomer(QWidget*parent)
{
	//TODO: check which dialog to use
	MNewCustomerWizard ncw(parent);
	ncw.exec();
	return ncw.customer();
}
