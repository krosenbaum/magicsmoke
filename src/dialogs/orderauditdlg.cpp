//
// C++ Implementation: orderaudit dialog
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "orderauditdlg_p.h"
#include "orderwin.h"

#include "formula.h"
#include "mapplication.h"

#include "msinterface.h"
#include "MTGetTicketAudit"
#include "MTGetVoucherAudit"
#include "MTGetOrderAudit"
#include "MTGetUserAudit"
#include "MTGetOrder"

#include <DPtr>
#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTableView>
#include <QTreeView>
#include <QHeaderView>


class MOAAuditList:public QList<MOAParcel>
{
	public:
		void addOrders(const QList<MOOrderAudit>&o)
		{for(int i=0;i<o.size();i++)addItem(o[i]);}
		void addTickets(const QList<MOTicketAudit>&o)
		{for(int i=0;i<o.size();i++)addItem(o[i]);}
		void addVouchers(const QList<MOVoucherAudit>&o)
		{for(int i=0;i<o.size();i++)addItem(o[i]);}
		
		void addItem(const MOAItem&it){
			for(int i=0;i<size();i++)
				if(operator[](i).addItem(it))
					return;
			append(MOAParcel(it));
		}
};

class MOAFormula:public MElamEngine
{
	MOrderAuditDialog*mdialog;
	MOADynamicObject*mcontext;
	public:
		MOAFormula()
		{mdialog=0;mcontext=0;}
		MOAFormula(const MOAFormula&f):MElamEngine(){operator=(f);}
		MOAFormula&operator=(const MOAFormula&f)
		{mdialog=f.mdialog;mcontext=f.mcontext;return *this;}
		void setParent(MOrderAuditDialog*p){MElamEngine::setParent(p);mdialog=p;}
		void setContext(MOADynamicObject*ctx){mcontext=ctx;}
		bool hasConstant(QString c)const{
			if(MElamEngine::hasConstant(c))return true;
// 			qDebug()<<"looking for const"<<c;
			if(mcontext){
// 				qDebug()<<"have const"<<c<<mcontext->hasValue(c);
				return mcontext->hasValue(c);
			}
			return false;
		}
		QVariant getConstant(QString c)const{
// 			qDebug()<<"getting const"<<c;
			if(mcontext){
				if(mcontext->hasValue(c))
					return mcontext->getValue(c);
			}
			return MElamEngine::getConstant(c);
		}
};

class DPTR_CLASS_NAME(MOrderAuditDialog)
{
protected:
	friend class MOrderAuditDialog;
	QTreeView*mdata;
	QStandardItemModel*mmodel;
	QComboBox*mprofile;
	QDomDocument mprofiledoc;
	MOAAuditList maudit;
	MOAFormula formula;
};
DEFINE_DPTR(MOrderAuditDialog);

void MOrderAuditDialog::openOrderAuditDialog(qint64 oid,QWidget*parent)
{
	MTGetOrderAudit oa=req->queryGetOrderAudit(oid);
// 	qDebug()<<"order lines"<<oa.getorder().size()<<"ticket lines"<<oa.gettickets().size()<<"voucher lines"<<oa.getvouchers().size();
	if(oa.hasError()){
		QMessageBox::warning(parent,tr("Warning"),tr("Error while retrieving audit data: %1").arg(oa.errorString()));
		return;
	}
	if(oa.getorder().size()==0)
		QMessageBox::warning(parent,tr("Warning"),tr("Sorry, no audit data available."));
	else
		MOrderAuditDialog(oa,parent).exec();
}

MOrderAuditDialog::MOrderAuditDialog(MTGetOrderAudit& audit, QWidget* parent)
	: QDialog(parent)
{
	//init
	QList<MOOrderAudit>orders=audit.getorder();
	setWindowTitle(tr("Order Audit [%1]").arg(orders.value(0).orderid()));
	setSizeGripEnabled(true);
	d->formula.setParent(this);
	resize(800,600);
	//init list
	d->maudit.addOrders(orders);
	d->maudit.addTickets(audit.gettickets());
	d->maudit.addVouchers(audit.getvouchers());
	qSort(d->maudit);
	qDebug()<<"# audit items:"<<d->maudit.size();
	//accumulate prices
	calcPrices();
	//get profiles
	getProfiles();
	//display
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(d->mprofile=new QComboBox,0);
	QDomNodeList nl=d->mprofiledoc.elementsByTagName("Report");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		d->mprofile->addItem(el.attribute("name"),i);
	}
	vl->addWidget(d->mdata=new QTreeView,1);
	d->mdata->setModel(d->mmodel=new QStandardItemModel);
	d->mdata->setEditTriggers(QAbstractItemView::NoEditTriggers);
	connect(d->mprofile,SIGNAL(currentIndexChanged(int)),this,SLOT(drawdata()));
	drawdata();
	//button bar
	QPushButton*p;
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addWidget(p=new QPushButton("Expand"),0);
	connect(p,SIGNAL(clicked()),d->mdata,SLOT(expandAll()));
	hl->addWidget(p=new QPushButton("Collapse"),0);
	connect(p,SIGNAL(clicked()),d->mdata,SLOT(collapseAll()));
	hl->addStretch(1);
	hl->addWidget(p=new QPushButton("Close"),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
}

MOrderAuditDialog::~MOrderAuditDialog()
{
}

void MOrderAuditDialog::drawdata()
{
	//clear
	d->mmodel->clear();
	//get profile
	QDomNodeList nl=d->mprofiledoc.elementsByTagName("Report");
	if(nl.size()<1)return;
	int pid=d->mprofile->itemData(d->mprofile->currentIndex()).toInt();
	if(pid<0||pid>=nl.size())return;
	QDomElement profile=nl.at(pid).toElement();
	//set size
	d->mmodel->insertRows(0,d->maudit.size());
	int numcols=profile.attribute("columns").toInt();
	d->mmodel->insertColumns(0,numcols);
	//set header
	nl=profile.elementsByTagName("Header");
	for(int i=0;i<nl.size();i++){
		QDomElement el=nl.at(i).toElement();
		d->mmodel->setHeaderData(el.attribute("col").toInt(),Qt::Horizontal,el.text());
	}
	//get profile formulae
	QDomNodeList suml=profile.elementsByTagName("Summary");
	QDomNodeList ordl=profile.elementsByTagName("Order");
	QDomNodeList tckl=profile.elementsByTagName("Ticket");
	QDomNodeList voul=profile.elementsByTagName("Voucher");
	//set data
	for(int i=0;i<d->maudit.size();i++){
		MOAParcel pc=d->maudit.value(i);
		d->formula.setContext(&pc);
		for(int k=0;k<suml.size();k++){
			QDomElement sum=suml.at(k).toElement();
			QModelIndex idx=d->mmodel->index(i,sum.attribute("col").toInt());
			QVariant v=d->formula.evaluate(sum.text());
			if(v.userType()==ELAM::Exception::metaTypeId())
				qDebug()<<"Exception in summary formula. Formula:"<<sum.text() <<"Column:"<<sum.attribute("col").toInt() <<"Exception:"<<v.value<ELAM::Exception>();
			d->mmodel->setData(idx,v);
		}
		QModelIndex pidx=d->mmodel->index(i,0);
		d->mmodel->insertRows(0,pc.size(),pidx);
		d->mmodel->insertColumns(0,numcols,pidx);
		for(int j=0;j<pc.size();j++){
			MOAItem itm=pc[j];
			d->formula.setContext(&itm);
			switch(itm.itemType()){
				case MOAItem::Order:nl=ordl;break;
				case MOAItem::Ticket:nl=tckl;break;
				case MOAItem::Voucher:nl=voul;break;
				default:continue;
			}
			for(int k=0;k<nl.size();k++){
				QDomElement el=nl.at(k).toElement();
				QModelIndex idx=d->mmodel->index(j,el.attribute("col").toInt(),pidx);
				QVariant v=d->formula.evaluate(el.text());
				if(v.userType()==ELAM::Exception::metaTypeId())
					qDebug()<<"Exception in formula. Formula:"<<el.text() <<"ItemType:"<<itm.itemTypeString() <<"Column:"<<el.attribute("col").toInt() <<"Exception:"<<v.value<ELAM::Exception>();
				d->mmodel->setData(idx,v);
			}
		}
		d->formula.setContext(0);
	}
	d->mdata->expandAll();
	for(int i=0;i<d->mmodel->columnCount();i++)
		d->mdata->resizeColumnToContents(i);
}

void MOrderAuditDialog::calcPrices()
{
	QMap<QString,qint64>prclist;
	for(int i=0;i<d->maudit.size();i++){
		//accumulate items
		for(int j=0;j<d->maudit[i].size();j++){
			MOAItem itm=d->maudit[i][j];
			QString id=itm.itemTypeString()+itm.itemID();
			switch(itm.itemType()){
				case MOAItem::Order:
					prclist.insert(id,itm.order().shippingcosts());
					break;
				case MOAItem::Voucher:
					prclist.insert(id,itm.voucher().price());
					break;
				case MOAItem::Ticket:
					prclist.insert(id,itm.ticket().amountToPay());
					break;
				default:break;
			}
		}
		//calculate current price
		qint64 prc=0;
		QStringList k=prclist.keys();
		for(int j=0;j<k.size();j++)
			prc+=prclist[k[j]];
		//set price to parcel
		d->maudit[i].setPrice(prc);
	}
}

void MOrderAuditDialog::getProfiles()
{
	QFile fd(MApplication::dataDir()+"/orderaudit.xml");
	if(fd.exists() && fd.open(QIODevice::ReadOnly)){
		qDebug()<<"loading main config"<<fd.fileName();
		if(d->mprofiledoc.setContent(&fd))
			return;
	}
	fd.setFileName(":/orderaudit.xml");
	if(fd.exists() && fd.open(QIODevice::ReadOnly)){
		d->mprofiledoc.setContent(&fd);
		qDebug()<<"loading backup config";
	}
}

void MOrderAuditDialog::openTicketAuditDialog(QString id,QWidget*parent)
{
	//get data
	MTGetTicketAudit va=req->queryGetTicketAudit(id);
	if(va.getticket().size()==0){
		QMessageBox::warning(parent,tr("No Data"),tr("No audit data found for this ticket."));
		return;
	}
	//display construction
	QList<MOTicketAudit>tlist=va.getticket();
	QList<MOEvent>elist=va.getevent();
	qDebug()<<"got # tickets:"<<tlist.size();
	QDialog d(parent);
	d.setWindowTitle(tr("Ticket Audit: %1").arg(id));
	d.setSizeGripEnabled(true);
	d.resize(800,600);
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QTableView*tab;
	QStandardItemModel*model;
	vl->addWidget(tab=new QTableView,1);
	tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tab->setModel(model=new QStandardItemModel(tab));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Close")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	//enter data
	model->insertRows(0,tlist.size());
// 	model->insertColumns(0,5);
	model->setHorizontalHeaderLabels(QStringList()
		<<tr("Date/Time")
		<<tr("User")
		<<tr("Transaction")
		<<tr("Price")
		<<tr("Event Date")
		<<tr("Event")
		);
	for(int i=0;i<tlist.size();i++){
		int eid=tlist[i].eventid();
		MOEvent ev;
		foreach(MOEvent e,elist)
			if(e.eventid()==eid)ev=e;
		model->setData(model->index(i,0),unix2dateTime(tlist[i].audittime()));
		model->setData(model->index(i,1),tlist[i].audituname().value());
		model->setData(model->index(i,2),tlist[i].audittransaction().value());
		model->setData(model->index(i,3),tlist[i].priceString());
		model->setData(model->index(i,4),unix2dateTime(ev.start()));
		model->setData(model->index(i,5),ev.title().value());
	}
	tab->resizeColumnsToContents();
	//show
	d.exec();
}

void MOrderAuditDialog::openVoucherAuditDialog(QString id,QWidget*parent)
{
	//get data
	MTGetVoucherAudit va=req->queryGetVoucherAudit(id);
	if(va.getvoucher().size()==0){
		QMessageBox::warning(parent,tr("No Data"),tr("No audit data found for this voucher."));
		return;
	}
	//display construction
	QList<MOVoucherAudit>vlist=va.getvoucher();
	qDebug()<<"got # vouchers:"<<vlist.size();
	QDialog d(parent);
	d.setWindowTitle(tr("Voucher Audit: %1").arg(id));
	d.setSizeGripEnabled(true);
	d.resize(800,600);
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QTableView*tab;
	QStandardItemModel*model;
	vl->addWidget(tab=new QTableView,1);
	tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tab->setModel(model=new QStandardItemModel(tab));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Close")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	//enter data
	model->insertRows(0,vlist.size());
// 	model->insertColumns(0,5);
	model->setHorizontalHeaderLabels(QStringList()
		<<tr("Date/Time")
		<<tr("User")
		<<tr("Transaction")
		<<tr("Value")
		<<tr("Price")
		<<tr("Validity")
		<<tr("Comment")
		);
	for(int i=0;i<vlist.size();i++){
		model->setData(model->index(i,0),unix2dateTime(vlist[i].audittime()));
		model->setData(model->index(i,1),vlist[i].audituname().value());
		model->setData(model->index(i,2),vlist[i].audittransaction().value());
		model->setData(model->index(i,3),vlist[i].valueString());
		model->setData(model->index(i,4),vlist[i].priceString());
		model->setData(model->index(i,5),vlist[i].validDate());
		model->setData(model->index(i,6),vlist[i].comment().value());
	}
	tab->resizeColumnsToContents();
	//show
	d.exec();
}

void MOrderAuditDialog::openUserAuditDialog(QString uid, qint64 oldest, QWidget* parent)
{
	MTGetUserAudit gua=req->queryGetUserAudit(uid,oldest);
	if(gua.hasError()){
		QMessageBox::warning(parent,tr("Warning"),tr("Unable to get user audit data: %1").arg(gua.errorString()));
		return;
	}
	MUserAuditDialog(gua,parent).exec();
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class DPTR_CLASS_NAME(MUserAuditDialog)
{
protected:
	friend class MUserAuditDialog;
	QTableView*morder,*mtick,*mvouch;
	QStandardItemModel*mmorder,*mmtick,*mmvouch;
};
DEFINE_DPTR(MUserAuditDialog);


MUserAuditDialog::MUserAuditDialog(const MTGetUserAudit& gua, QWidget* parent, Qt::WindowFlags f)
	: QDialog(parent, f)
{
	setWindowTitle(tr("User Audit: %1").arg(gua.getuname()));
	setSizeGripEnabled(true);
	resize(800,600);
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QTabWidget*tab;
	vl->addWidget(tab=new QTabWidget,1);
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Close")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	
	//order tab
	QWidget*w;
	tab->addTab(w=new QWidget,tr("Orders"));
	w->setLayout(hl=new QHBoxLayout);
	hl->addWidget(d->morder=new QTableView);
	d->morder->setModel(d->mmorder=new QStandardItemModel(this));
	d->morder->setEditTriggers(QAbstractItemView::NoEditTriggers);
	QList<MOOrderAudit> ords=gua.getorders();
	d->mmorder->insertColumns(0,3);
	d->mmorder->setHorizontalHeaderLabels(QStringList()<<tr("Order ID")<<tr("Action")<<tr("Time"));
	d->mmorder->insertRows(0,ords.size());
	for(int i=0;i<ords.size();i++){
		d->mmorder->setData(d->mmorder->index(i,0),ords[i].orderid().value());
		d->mmorder->setData(d->mmorder->index(i,1),ords[i].audittransaction().value());
		d->mmorder->setData(d->mmorder->index(i,2),unix2dateTime(ords[i].audittime()));
	}
	d->morder->resizeColumnsToContents();
	d->morder->verticalHeader()->hide();
	hl->addLayout(vl=new QVBoxLayout);
	vl->addWidget(p=new QPushButton(tr("Show Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(showorder()));
	vl->addWidget(p=new QPushButton(tr("Audit Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(auditorder()));
	vl->addStretch(1);
	
	//ticket tab
	tab->addTab(w=new QWidget,tr("Tickets"));
	w->setLayout(hl=new QHBoxLayout);
	hl->addWidget(d->mtick=new QTableView);
	d->mtick->setModel(d->mmtick=new QStandardItemModel(this));
	d->mtick->setEditTriggers(QAbstractItemView::NoEditTriggers);
	QList<MOTicketAudit> ticks=gua.gettickets();
	d->mmtick->insertColumns(0,4);
	d->mmtick->setHorizontalHeaderLabels(QStringList()<<tr("Order ID")<<tr("Ticket")<<tr("Action")<<tr("Time"));
	d->mmtick->insertRows(0,ticks.size());
	for(int i=0;i<ticks.size();i++){
		d->mmtick->setData(d->mmtick->index(i,0),ticks[i].orderid().value());
		d->mmtick->setData(d->mmtick->index(i,1),ticks[i].ticketid().value());
		d->mmtick->setData(d->mmtick->index(i,2),ticks[i].audittransaction().value());
		d->mmtick->setData(d->mmtick->index(i,3),unix2dateTime(ticks[i].audittime()));
	}
	d->mtick->resizeColumnsToContents();
	d->mtick->verticalHeader()->hide();
	hl->addLayout(vl=new QVBoxLayout);
	vl->addWidget(p=new QPushButton(tr("Show Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(showorderbyticket()));
	vl->addWidget(p=new QPushButton(tr("Audit Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(auditorderbyticket()));
	vl->addSpacing(10);
	vl->addWidget(p=new QPushButton(tr("Audit Ticket")));
	connect(p,SIGNAL(clicked()),this,SLOT(auditticket()));
	vl->addStretch(1);

	//voucher tab
	tab->addTab(w=new QWidget,tr("Vouchers"));
	w->setLayout(hl=new QHBoxLayout);
	hl->addWidget(d->mvouch=new QTableView);
	d->mvouch->setModel(d->mmvouch=new QStandardItemModel(this));
	d->mvouch->setEditTriggers(QAbstractItemView::NoEditTriggers);
	QList<MOVoucherAudit> vous=gua.getvouchers();
	d->mmvouch->insertColumns(0,4);
	d->mmvouch->setHorizontalHeaderLabels(QStringList()<<tr("Order ID")<<tr("Voucher")<<tr("Action")<<tr("Time"));
	d->mmvouch->insertRows(0,vous.size());
	for(int i=0;i<vous.size();i++){
		d->mmvouch->setData(d->mmvouch->index(i,0),vous[i].orderid().value());
		d->mmvouch->setData(d->mmvouch->index(i,1),vous[i].voucherid().value());
		d->mmvouch->setData(d->mmvouch->index(i,2),vous[i].audittransaction().value());
		d->mmvouch->setData(d->mmvouch->index(i,3),unix2dateTime(vous[i].audittime()));
	}
	d->mvouch->resizeColumnsToContents();
	d->mvouch->verticalHeader()->hide();
	hl->addLayout(vl=new QVBoxLayout);
	vl->addWidget(p=new QPushButton(tr("Show Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(showorderbyvoucher()));
	vl->addWidget(p=new QPushButton(tr("Audit Order")));
	connect(p,SIGNAL(clicked()),this,SLOT(auditorderbyvoucher()));
	vl->addSpacing(10);
	vl->addWidget(p=new QPushButton(tr("Audit Voucher")));
	connect(p,SIGNAL(clicked()),this,SLOT(auditvoucher()));
	vl->addStretch(1);
	
	//TODO: item tab
}

void MUserAuditDialog::showorder(qint64 oid)
{
	if(oid<0){
		QModelIndex idx=d->morder->currentIndex();
		if(!idx.isValid())return;
		oid=d->mmorder->data(d->mmorder->index(idx.row(),0)).toLongLong();
	}
	//get it
	MTGetOrder go=req->queryGetOrder(oid);
	if(go.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to retrieve order: %1").arg(go.errorString()));
		return;
	}
	//show it
	MOrderWindow *ow=new MOrderWindow(this,go.getorder());
	ow->show();
}

void MUserAuditDialog::auditorder(qint64 oid)
{
	if(oid<0){
		QModelIndex idx=d->morder->currentIndex();
		if(!idx.isValid())return;
		oid=d->mmorder->data(d->mmorder->index(idx.row(),0)).toLongLong();
	}
	//get it
	MOrderAuditDialog::openOrderAuditDialog(oid,this);
}

void MUserAuditDialog::showorderbyticket()
{
	QModelIndex idx=d->mtick->currentIndex();
	if(!idx.isValid())return;
	showorder(d->mmtick->data(d->mmtick->index(idx.row(),0)).toLongLong());
}

void MUserAuditDialog::auditorderbyticket()
{
	QModelIndex idx=d->mtick->currentIndex();
	if(!idx.isValid())return;
	auditorder(d->mmtick->data(d->mmtick->index(idx.row(),0)).toLongLong());
}

void MUserAuditDialog::showorderbyvoucher()
{
	QModelIndex idx=d->mvouch->currentIndex();
	if(!idx.isValid())return;
	showorder(d->mmvouch->data(d->mmvouch->index(idx.row(),0)).toLongLong());
}

void MUserAuditDialog::auditorderbyvoucher()
{
	QModelIndex idx=d->mvouch->currentIndex();
	if(!idx.isValid())return;
	auditorder(d->mmvouch->data(d->mmvouch->index(idx.row(),0)).toLongLong());
}

void MUserAuditDialog::auditvoucher()
{
	QModelIndex idx=d->mvouch->currentIndex();
	if(!idx.isValid())return;
	MOrderAuditDialog::openVoucherAuditDialog(d->mmvouch->data(d->mmvouch->index(idx.row(),1)).toString(),this);
}

void MUserAuditDialog::auditticket()
{
	QModelIndex idx=d->mtick->currentIndex();
	if(!idx.isValid())return;
	MOrderAuditDialog::openTicketAuditDialog(d->mmtick->data(d->mmtick->index(idx.row(),1)).toString(),this);
}
