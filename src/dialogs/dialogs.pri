HEADERS += \
	dialogs/eventedit.h \
	dialogs/eventsummary.h \
	dialogs/orderwin.h \
	dialogs/shipping.h \
	dialogs/customerdlg.h \
	dialogs/checkdlg.h \
	dialogs/passwdchg.h \
	dialogs/pricecatdlg.h \
	dialogs/flagedit.h \
	dialogs/orderauditdlg.h \
	dialogs/orderauditdlg_p.h \
	dialogs/backupdlg.h \
	dialogs/payedit.h

SOURCES += \
	dialogs/eventedit.cpp \
	dialogs/eventsummary.cpp \
	dialogs/orderwin.cpp \
	dialogs/shipping.cpp \
	dialogs/customerdlg.cpp \
	dialogs/checkdlg.cpp \
	dialogs/passwdchg.cpp \
	dialogs/pricecatdlg.cpp \
	dialogs/flagedit.cpp \
	dialogs/orderauditdlg.cpp \
	dialogs/backupdlg.cpp \
	dialogs/payedit.cpp

RESOURCES += dialogs/dialogfiles.qrc

INCLUDEPATH += ./dialogs
