//
// C++ Implementation: flagedit
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See COPYING.GPL file that comes with this distribution
//
//

#include "flagedit.h"
#include "MTGetValidFlags"

#include <QBoxLayout>
#include <QComboBox>
#include <QCoreApplication>
#include <QDebug>
#include <QGridLayout>
#include <QHeaderView>
#include <QItemDelegate>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTableView>
#include <QToolButton>

static const int MIGNORE=0;
static const int MMUST=1;
static const int MNOT=2;
#define MMAX MNOT
static const char *modes[]={
	QT_TRANSLATE_NOOP("MFlagEditor","ignore"),
	QT_TRANSLATE_NOOP("MFlagEditor","must have"),
	QT_TRANSLATE_NOOP("MFlagEditor","must not have")
};

class MFlagEditorDelegate:public QItemDelegate
{
	public:
		MFlagEditorDelegate(MFlagEditor*p):QItemDelegate(p){m_parent=p;}
		
		virtual QWidget * createEditor(QWidget*p,const QStyleOptionViewItem&,const QModelIndex& index) const{
			if(index.column()!=0)return 0;
			//create widget
			QComboBox *cb=new QComboBox(p);
			cb->setEditable(false);
			for(int i=0;i<=MMAX;i++)
				cb->addItem(QCoreApplication::translate("MFlagEditor",modes[i]));
			//set current
			cb->setCurrentIndex(m_parent->model->data(index,Qt::UserRole).toInt());
			//finish up
			return cb;
		}
		virtual void setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const{
			if(index.column()!=0)return;
			QComboBox*cb=qobject_cast<QComboBox*>(editor);
			int c=cb->currentIndex();if(c<0)c=0;if(c>MMAX)c=MMAX;
			model->setData(index,QCoreApplication::translate("MFlagEditor",modes[c]));
			model->setData(index,c,Qt::UserRole);
		}
	private:
		MFlagEditor*m_parent;
};

//static 
QString MFlagEditor::edit(QWidget*parent,QString flags,QString label)
{
	MFlagEditor fe(parent,flags,label);
	if(fe.exec()==QDialog::Accepted)
		return fe.currentFlags();
	else
		return flags;
}

MFlagEditor::MFlagEditor(QWidget*par,QString f,QString l)
	:QDialog(par)
{
	setWindowTitle(tr("Edit Flags"));
	setSizeGripEnabled(true);
	
	//parse flags
	flags=f.split(" ",QString::SkipEmptyParts);
	//get all defined flags
	allflags=MTGetValidFlags::query().getflags();
	
	//create widgets
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	QPushButton*p;
	setLayout(vl=new QVBoxLayout);
	if(l!=""){
		vl->addWidget(new QLabel(l),0);
		vl->addSpacing(15);
	}
	vl->addWidget(table=new QTableView,1);
	table->setModel(model=new QStandardItemModel(this));
	table->setItemDelegate(new MFlagEditorDelegate(this));
	table->verticalHeader()->hide();
	
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(1);
	hl->addWidget(p=new QPushButton(tr("Reset")));
	connect(p,SIGNAL(clicked()),this,SLOT(reset()));
	hl->addSpacing(20);
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
	//populate
	reset();
}

QString MFlagEditor::currentFlags()const
{
	//gather
	QStringList fl;
	for(int i=0;i<model->rowCount();i++){
		switch(model->data(model->index(i,0),Qt::UserRole).toInt()){
			case MMUST:fl.append("+"+allflags[i].flag());break;
			case MNOT:fl.append("-"+allflags[i].flag());break;
			default: /*ignore*/ break;
		}
	}
	//convert to string
	QString f;
	for(int i=0;i<fl.size();i++){
		if(i)f+=" ";
		f+=fl[i];
	}
	return f;
}
QString MFlagEditor::originalFlags()const
{
	QString f;
	for(int i=0;i<flags.size();i++){
		if(i)f+=" ";
		f+=flags[i];
	}
	return f;
}
void MFlagEditor::reset()
{
	model->clear();
	model->insertRows(0,allflags.size());
	model->insertColumns(0,3);
	model->setHorizontalHeaderLabels(QStringList()<<tr("Mode")<<tr("Flag")<<tr("Description"));
	for(int i=0;i<allflags.size();i++){
		int md=MIGNORE;
		QString f=allflags[i].flag();
		if(flags.contains("+"+f))md=MMUST;else
		if(flags.contains("-"+f))md=MNOT;
		model->setData(model->index(i,0),tr(modes[md]));
		model->setData(model->index(i,0),md,Qt::UserRole);
		model->setData(model->index(i,1),f);
		model->setData(model->index(i,2),allflags[i].description().value());
	}
	table->resizeColumnsToContents();
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++

MFlagWidget::MFlagWidget(QWidget* parent, Qt::WindowFlags f): QWidget(parent, f)
{
	init();
}

MFlagWidget::MFlagWidget(QString flags, QWidget* parent, Qt::WindowFlags f): QWidget(parent, f)
{
	init();
	setFlags(flags);
}

MFlagWidget::MFlagWidget(QStringList flags, QWidget* parent, Qt::WindowFlags f): QWidget(parent, f)
{
	init();
	setFlags(flags);
}

void MFlagWidget::init()
{
	QHBoxLayout*hl;
	setLayout(hl=new QHBoxLayout);
	hl->addWidget(m_line=new QLabel("  "),1);
	m_line->setFrameStyle(QFrame::StyledPanel|QFrame::Sunken);
	m_btn=0;
	setEditable(true);
}

void MFlagWidget::setEditable(bool e)
{
	QHBoxLayout*hl=qobject_cast< QHBoxLayout* >(layout());
	if(hl==0){
		qDebug()<<"oops! MFlagWidget::setEditable has no layout!"<<hex<<(quint64)this;
		return;
	}
	if(e){
		if(m_btn==0){
			hl->addWidget(m_btn=new QToolButton,0);
			m_btn->setText("...");
			connect(m_btn,SIGNAL(clicked()),this,SLOT(doedit()));
		}
	}else{
		if(m_btn){
			m_btn->deleteLater();
			m_btn=0;
		}
	}
}

void MFlagWidget::setReadonly(bool ro)
{
	setEditable(!ro);
}

void MFlagWidget::setFlags(QString f)
{
	m_flags=f;
	m_line->setText(f);
}

void MFlagWidget::setFlags(QStringList fl)
{
	QString ff;
	foreach(QString f,fl){
		if(!ff.isEmpty())ff+=" ";
		ff+=f;
	}
	setFlags(ff);
}

void MFlagWidget::setText(QString f)
{
	setFlags(f);
}

void MFlagWidget::setEditorLabel(QString e)
{m_label=e;}

void MFlagWidget::doedit()
{
	MFlagEditor mf(this,m_flags,m_label);
	if(mf.exec()==QDialog::Accepted)
		setFlags(mf.currentFlags());
}
