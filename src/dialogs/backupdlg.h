//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_BACKUPDLG_H
#define MAGICSMOKE_BACKUPDLG_H

#include <QDialog>

#include "tabwin.h"

class QCheckBox;
class QLineEdit;
class QSpinBox;

/**Helper class for Backup settings*/
class MBackupDialog:public QDialog
{
	Q_OBJECT
	public:
		MBackupDialog(QWidget*,QString);
		
	private slots:
		void getpath();
		void save();
	private:
		QLineEdit*lpath;
		QSpinBox*interv,*gener;
		QCheckBox*autob;
		QString profilekey;
};

#endif
