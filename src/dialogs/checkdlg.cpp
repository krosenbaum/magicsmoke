//
// C++ Implementation: checkdlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "checkdlg.h"

#include <QCheckBox>
#include <QScrollArea>
#include <QPushButton>
#include <QBoxLayout>

MCheckDialog::MCheckDialog(QWidget*parent,const MCheckList&checks,QString title)
	:QDialog(parent),m_list(checks)
{
	setWindowTitle(title);
	//basic layout
	QVBoxLayout *vl=new QVBoxLayout;
	setLayout(vl);
	QScrollArea *sa=new QScrollArea;
	vl->addWidget(sa,10);
	QHBoxLayout *hl=new QHBoxLayout;
	vl->addLayout(hl,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	//fill area
	QWidget*w=new QWidget;
	w->setLayout(vl=new QVBoxLayout);
	for(int i=0;i<m_list.size();i++){
		QCheckBox *cb=new QCheckBox(m_list[i].label());
		cb->setChecked(m_list[i].isSet());
		vl->addWidget(cb);
		m_boxes.append(cb);
	}
	sa->setWidget(w);
}

MCheckList MCheckDialog::getCheckList()const
{
	for(int i=0;i<m_boxes.size();i++)
		m_list[i].set(m_boxes[i]->isChecked());
	return m_list;
}

