//
// C++ Interface: shipping
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_PAYEDIT_H
#define MAGICSMOKE_PAYEDIT_H

#include <QString>
#include <QDialog>

class QLabel;
class QComboBox;
class QLineEdit;
class MCentSpinBox;
class QStandardItemModel;
class QTableView;

#include "MOPaymentType"

///an editor for payment types, this is a direct interface to the paymenttype table on the server side
class MPaymentEditor:public QDialog
{
	Q_OBJECT
	public:
		///instantiate the editor
		MPaymentEditor(QWidget*);
	
	private slots:
		/// \internal change the description of the paytype
		void changeDescription();
		/// \internal change the data type of the paytype
		void changeData();
		/// \internal change the flags of the paytype
		void changeAvail();
		/// \internal change the default paytype
		void changeDefault();
		/// \internal create a new paytype
		void addNew();
		/// \internal delete a paytype
		void deletePay();
		/// \internal update the table of paytypes
		void updateTable();
		
	private:
		QList<MOPaymentType>all;
		QStandardItemModel*model;
		QTableView*table;
		QLabel*deftype;
};

///simple dialog to enter payment data
class MPaymentDialog:public QDialog
{
	Q_OBJECT
	public:
		///instantiate the dialog
		MPaymentDialog(QWidget* parent = 0, Qt::WindowFlags f = 0);
		
		///returns the currently set money value in cents
		qint64 value()const;
		
		///returns the selected payment type
		QString paytype()const;
		///returns the currently entered payment data
		QString paydata()const;
		
	public slots:
		///set the current money value in cents
		void setValue(qint64);
		///set the maximum that can be entered
		void setMaximum(qint64);
		
	private slots:
		/// \internal update the data fields
		void setPType();
	private:
		MCentSpinBox*m_cent;
		QComboBox*m_ptype;
		QLineEdit*m_data;
		QLabel*m_dlabel;
};

#endif
