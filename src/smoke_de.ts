<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MAclWindow</name>
    <message>
        <location filename="mwin/aclwin.cpp" line="47"/>
        <source>ACL Editor</source>
        <translation>Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="54"/>
        <source>&amp;Window</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="55"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="59"/>
        <source>Users</source>
        <translation>Nutzer</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="61"/>
        <source>Roles</source>
        <translation>Rollen</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="63"/>
        <source>Hosts</source>
        <translation>Hosts</translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="65"/>
        <source>Flags</source>
        <translation>Flags</translation>
    </message>
</context>
<context>
    <name>MAddressChoiceDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="818"/>
        <source>Chose an Address</source>
        <translation>Adresse wählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="833"/>
        <source>Add Address</source>
        <translation>Adresse hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="836"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="869"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="869"/>
        <source>Unable to save changes made to addresses: %1</source>
        <translation>Änderungen an dieser Adresse können nicht gespeichert werden: %1</translation>
    </message>
</context>
<context>
    <name>MAddressDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="693"/>
        <source>Edit Address</source>
        <translation>Adresse ändern</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="695"/>
        <source>Create Address</source>
        <translation>Adresse anlegen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="704"/>
        <source>Last used:</source>
        <translation>Zuletzt benutzt:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="705"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="706"/>
        <location filename="dialogs/customerdlg.cpp" line="707"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="708"/>
        <source>City:</source>
        <translation>Stadt:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="709"/>
        <source>State:</source>
        <translation>Bundesland:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="710"/>
        <source>ZIP Code:</source>
        <translation>Postleitzahl:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="711"/>
        <source>Country:</source>
        <translation>Land:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="720"/>
        <location filename="dialogs/customerdlg.cpp" line="759"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="722"/>
        <location filename="dialogs/customerdlg.cpp" line="761"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="790"/>
        <source> Create New Country...</source>
        <comment>this pseudo-entry must contain leading space to distinguish it from genuine countries</comment>
        <translation> Neues Land anlegen...</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="793"/>
        <source>Select Country</source>
        <translation>Land auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="793"/>
        <source>Please select a country:</source>
        <translation>Bitte wählen Sie ein Land:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="746"/>
        <source>Create New Country</source>
        <translation>Neues Land anlegen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="754"/>
        <source>Country Name:</source>
        <translation>Name des Landes:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="755"/>
        <source>Abbreviation:</source>
        <translation>Abkürzung:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="766"/>
        <location filename="dialogs/customerdlg.cpp" line="772"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="766"/>
        <source>The country name and abbreviation must contain something!</source>
        <translation>Der Landesname und die Abkürzung müssen Daten enthalten!</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="772"/>
        <source>Error while creating country: %1</source>
        <translation>Fehler beim Anlegen des Landes: %1</translation>
    </message>
</context>
<context>
    <name>MAddressWidget</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="516"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="520"/>
        <source>Edit</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="523"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="576"/>
        <source>Delete Address</source>
        <translation>Adresse löschen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="576"/>
        <source>Really delete this address?
%1</source>
        <translation>Diese Adresse wirklich löschen?
%1</translation>
    </message>
</context>
<context>
    <name>MBackupDialog</name>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="42"/>
        <source>Backup Settings</source>
        <translation>Einstellungen Sicherungskopie</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="47"/>
        <source>Backup File:</source>
        <translation>Sicherungskopie Datei:</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="49"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="52"/>
        <source>Generations to keep:</source>
        <translation>Anzahl Generationen:</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="57"/>
        <source>Automatic Backup:</source>
        <translation>Automatische Sicherung:</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="61"/>
        <source>Interval in days:</source>
        <translation>Intervall in Tagen:</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="69"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="72"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="78"/>
        <source>Backup File</source>
        <translation>Sicherungsdatei</translation>
    </message>
</context>
<context>
    <name>MCartTab</name>
    <message>
        <location filename="mwin/carttab.cpp" line="75"/>
        <source>Add Ticket</source>
        <translation>Eintrittskarte hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="77"/>
        <source>Add Voucher</source>
        <translation>Gutschein hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="79"/>
        <source>Add Shop Item</source>
        <oldsource>Remove Item</oldsource>
        <translation>Waren hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="84"/>
        <source>Remove Line</source>
        <translation>Zeile entfernen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="111"/>
        <source>Customer:</source>
        <translation>Kunde:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="117"/>
        <source>Invoice Address:</source>
        <translation>Rechnungsadresse:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="130"/>
        <source>Shipping Method:</source>
        <translation>Versandoption:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="123"/>
        <source>Delivery Address:</source>
        <translation>Lieferadresse:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="137"/>
        <source>Comments:</source>
        <translation>Kommentare:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="144"/>
        <source>Order</source>
        <translation>bestellen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="150"/>
        <source>Reserve</source>
        <translation>reservieren</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="153"/>
        <source>Clear</source>
        <translation>zurücksetzen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="170"/>
        <source>Add &amp;Ticket</source>
        <translation>Eintrittskarte &amp;hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="171"/>
        <source>Add &amp;Voucher</source>
        <translation>&amp;Gutschein hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="168"/>
        <source>Ca&amp;rt</source>
        <translation>Warenkorb</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="90"/>
        <source>Total Price Sum:</source>
        <translation>Gesamtpreis:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="147"/>
        <source>Sell</source>
        <translation>verkaufen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="172"/>
        <source>Add &amp;Shop-Item</source>
        <translation>Waren hinzufügen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="173"/>
        <source>&amp;Remove Line</source>
        <translation>Zeile entfernen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="174"/>
        <source>&amp;Abort Shopping</source>
        <translation>&amp;Einkauf abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="176"/>
        <source>&amp;Update Shipping Options</source>
        <translation>Versandoptionen auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="190"/>
        <source>(No Shipping)</source>
        <translation>(Kein Versand)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Amount</source>
        <translation>Anzahl</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Start Time</source>
        <translation>Anfangszeit</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="277"/>
        <location filename="mwin/carttab.cpp" line="332"/>
        <location filename="mwin/carttab.cpp" line="339"/>
        <location filename="mwin/carttab.cpp" line="421"/>
        <location filename="mwin/carttab.cpp" line="537"/>
        <location filename="mwin/carttab.cpp" line="575"/>
        <location filename="mwin/carttab.cpp" line="579"/>
        <location filename="mwin/carttab.cpp" line="618"/>
        <location filename="mwin/carttab.cpp" line="628"/>
        <location filename="mwin/carttab.cpp" line="857"/>
        <location filename="mwin/carttab.cpp" line="867"/>
        <location filename="mwin/carttab.cpp" line="871"/>
        <location filename="mwin/carttab.cpp" line="877"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="277"/>
        <source>Please set the customer first.</source>
        <translation>Bitte wählen Sie einen Kunden aus.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="367"/>
        <location filename="mwin/carttab.cpp" line="460"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="332"/>
        <source>Error getting event, please try again.</source>
        <translation>Konnte Veranstalung nicht herunterlden, bitte versuchen Sie es noch einmal.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="95"/>
        <source>Coupon Code:</source>
        <translation>Rabattcode:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="96"/>
        <source>(none)</source>
        <comment>coupon code label</comment>
        <translation>(keiner)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="97"/>
        <source>Add Coupon...</source>
        <translation>Rabattgutschein hinzufügen...</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="100"/>
        <source>Pay with Vouchers:</source>
        <translation>Mit Gutschein bezahlen:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="102"/>
        <source>Add Voucher...</source>
        <translation>Gutschein hinzufügen...</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="105"/>
        <source>Remaining Sum:</source>
        <translation>Restbetrag:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Notes</source>
        <translation>Anmerkungen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="339"/>
        <source>This event has no prices associated that you can use for sales. Cannot sell tickets.</source>
        <translation>Diese Veranstaltung hat keine frei verkäuflichen Plätze mehr.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="345"/>
        <source>Select Price Category</source>
        <translation>Preiskategorie wählen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="351"/>
        <source>Please chose a price category:</source>
        <translation>Bitte wählen Sie eine Preiskategorie:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="365"/>
        <location filename="mwin/carttab.cpp" line="458"/>
        <location filename="mwin/carttab.cpp" line="667"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="421"/>
        <source>Cannot pay for vouchers with another voucher!</source>
        <translation>Gutscheine können nicht mit anderen Gutscheinen bezahlt werden!</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="427"/>
        <source>Select Voucher</source>
        <translation>Gutschein wählen</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="438"/>
        <source>Select voucher price and value:</source>
        <translation>Bitte Gutschein-Preis und -Wert wählen:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="440"/>
        <source>Price:</source>
        <translation>Preis:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="446"/>
        <source>Value:</source>
        <translation>Wert:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="478"/>
        <source>Voucher (value %1)</source>
        <translation>Gutschein (Wert: %1)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="537"/>
        <source>There are problems with the contents of the cart, please check and then try again.</source>
        <translation>Es gibt Probleme mit dem Inhalt des Wahrenkorbs. Bitte prüfen Sie die rot markierten Inhalte und probieren Sie es erneut.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="560"/>
        <location filename="mwin/carttab.cpp" line="564"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="560"/>
        <source>There is nothing in the order. Ignoring it.</source>
        <translation>Bestellung ist leer. Vorgang abgebrochen.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="564"/>
        <source>Please chose a customer first!</source>
        <translation>Bitte wählen Sie zunächst einen Kunden aus!</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="568"/>
        <source>Shipping</source>
        <translation>Versand</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="568"/>
        <source>You have chosen a shipping method, but no address. Are you sure you want to continue?</source>
        <translation>Sie haben eine Versandmethode, aber keine Adresse gewählt. Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="575"/>
        <source>Reservations can only contain tickets.</source>
        <translation>Reservierungen können nur Eintrittskarten enthalten.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="579"/>
        <source>Reservations cannot contain any vouchers for payment yet.</source>
        <translation>Reservierungen können noch keine Gutscheine zur Bezahlung enthalten.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="618"/>
        <source>Error while creating reservation: %1</source>
        <translation>Fehler beim Anlegen der Reservierung: %1</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="628"/>
        <source>Error while creating order: %1</source>
        <translation>Fehler beim Anlegen der Bestellung: %1</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="642"/>
        <source>Voucher State</source>
        <translation>Gutscheinzustand</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Voucher</source>
        <translation>Gutschein</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Original Value</source>
        <translation>Ursprünglicher Wert</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Used</source>
        <translation>Benutzt</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Remaining Value</source>
        <translation>Verbleibender Wert</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="661"/>
        <source>Paid in Cash: %1</source>
        <translation>In Bar bezahlt: %1</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="678"/>
        <source>The customer is not valid, please chose another one.</source>
        <translation>Der Kunde existiert nicht oder kann nicht benutzt werden, bitte wählen Sie einen anderen Kunden.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="683"/>
        <source>The delivery address is not valid, please chose another one.</source>
        <translation>Die Lieferadresse kann nicht benutzt werden, bitte wählen Sie eine andere Adresse.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="688"/>
        <source>The invoice address is not valid, please chose another one.</source>
        <translation>Die Rechnungsadresse kann nicht benutzt werden, bitte wählen Sie eine andere.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="695"/>
        <source>Shipping Type does not exist or you do not have permission to use it.</source>
        <translation>Die Versandart existiert nicht oder Sie haben nicht das Recht diese Versandart zu verwenden.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="728"/>
        <source>The event is already over, please remove this entry.</source>
        <translation>Diese Veranstaltung ist bereits vorbei. Bitte entfernen Sie diesen Eintrag.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="733"/>
        <source>You cannot order tickets for this event anymore, ask a more privileged user.</source>
        <translation>Sie können keine Entrittskarten mehr für diese Veranstaltung verkaufen, bitte fragen Sie einen höher privilegierten Nutzer.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="739"/>
        <source>The event or category is (almost) sold out, there are %1 tickets left. (%2)</source>
        <translation>Die Veranstaltung oder Kategorie ist (fast) ausverkauft, es gibt nur noch %1 Karten. (Details: %2)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="745"/>
        <source>The event does not exist or there is another serious problem, please remove this entry. (%1)</source>
        <translation>Die Veranstaltung existiert nicht oder es ist ein anderes ernstes Problem aufgetreten. Bitte entfernen Sie diesen Eintrag. (Details: %1)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="819"/>
        <source>(none)</source>
        <comment>empty coupon code label</comment>
        <translation>(keiner)</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="820"/>
        <source>[Add Coupon]</source>
        <comment>link/label</comment>
        <translation>[Rabattgutschein hinzufügen]</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="834"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; Paying for vouchers with older vouchers!</source>
        <translation>&lt;b&gt;Vorsicht:&lt;/b&gt; bezahle neue Gutscheine mit älteren Gutscheinen!</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="839"/>
        <source>[remove]</source>
        <comment>remove voucher link/label</comment>
        <translation>[entfernen]</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="844"/>
        <source>[Add Voucher]</source>
        <comment>link/label</comment>
        <translation>[Gutschein hinzufügen]</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="857"/>
        <source>Only one coupon can be used on an order.</source>
        <translation>Nur ein Rabattcode kann pro Bestellung genutzt werden.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="861"/>
        <source>Enter Coupon ID</source>
        <translation>Rabattcode eingeben</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="861"/>
        <source>Please enter the coupon ID:</source>
        <translation>Bitte geben Sie den Rabattcode ein:</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="867"/>
        <source>&apos;%1&apos; is not a valid coupon.</source>
        <translation>&apos;%1&apos; ist kein gültiger Rabattcode.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="871"/>
        <source>Sorry, you are not allowed to use this coupon.</source>
        <translation>Sie haben nicht das Recht diesen Rabattcode zu benutzen.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="877"/>
        <source>Sorry, not implemented yet. Please wait a few days.</source>
        <translation>Diese Funktion ist noch nicht implementiert. Bitte warten Sie ein paar Tage/Wochen auf das nächste Update.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="941"/>
        <source>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Sorry, coupon limits are not implemented yet.&lt;/font&gt;</source>
        <translation>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Rabatt-Limits sind noch nicht implementiert. Dieser Code kann nicht angewendet werden.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="946"/>
        <source>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Sorry, coupon rule limits are not implemented yet.&lt;/font&gt;</source>
        <translation>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Rabatt-Regel-Limits sind noch nicht implementiert. Dieser Code kann nicht angewendet werden.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="1005"/>
        <source>Original price: %1 (%2)</source>
        <translation>Ursprünglicher Preis: %1 (%2)</translation>
    </message>
    <message>
        <source>The event is (almost) sold out, there are %1 tickets left.</source>
        <translation type="vanished">Diese Veranstaltung ist (nahezu) ausverkauft. Es gibt nur noch %1 Karten.</translation>
    </message>
    <message>
        <source>The event does not exist or there is another serious problem, please remove this entry.</source>
        <translation type="vanished">Diese Veranstaltung existiert nicht oder es gibt ein anderes Problem - bitte entfernen Sie diesen Eintrag.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="783"/>
        <source>You do not have permission to create vouchers with this value, please remove it.</source>
        <translation>Sie haben nicht die Berechtigung Gutscheine mit diesem Wert anzulegen - bitte entfernen Sie ihn.</translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="788"/>
        <source>The price tag of this voucher is not valid, please remove and recreate it.</source>
        <translation>Der Preis dieses Gutscheins ist nicht gültig. Bitte entfernen Sie ihn und legen Sie ihn neu an.</translation>
    </message>
</context>
<context>
    <name>MCheckDialog</name>
    <message>
        <location filename="dialogs/checkdlg.cpp" line="33"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/checkdlg.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MContactTableDelegate</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="447"/>
        <source>(New Contact Type)</source>
        <translation>(Neuer Kontakttyp)</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="466"/>
        <source>Create new Contact Type</source>
        <translation>Neuen Kontakttyp Anlegen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="474"/>
        <source>Contact Type Name:</source>
        <translation>Kontakttypname:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="475"/>
        <source>Contact Type URI Prefix:</source>
        <translation>Kontakttyp URI-Prefix:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="478"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="480"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="485"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="485"/>
        <source>Error while creating contact type: %1</source>
        <translation>Fehler beim anlegen des Kontakttyps: %1</translation>
    </message>
</context>
<context>
    <name>MCustomerDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="235"/>
        <source>Customer %1</source>
        <translation>Kunde %1</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="237"/>
        <source>New Customer</source>
        <translation>Neuer Kunde</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="249"/>
        <source>Customer</source>
        <translation>Kunde</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="257"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="264"/>
        <source>Web-Login/eMail:</source>
        <translation>Web-Login/eMail:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="266"/>
        <source>Edit Login</source>
        <translation>Login Ändern</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="269"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="275"/>
        <source>Addresses</source>
        <translation>Adressen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="280"/>
        <source>Add Address</source>
        <translation>Adresse hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="283"/>
        <source>Contact Information</source>
        <translation>Kontaktinformationen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="290"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="292"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="297"/>
        <source>Type</source>
        <comment>table: contact type</comment>
        <translation>Kontakttyp</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="297"/>
        <source>Contact</source>
        <comment>table: contact info</comment>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="312"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="316"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="387"/>
        <source>Please enter the mail address for this customer to log into the web portal:</source>
        <translation>Bitte geben Sie die Mailadresse ein, die dieser Kunde benutzt um sich am Webportal anzumelden:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="391"/>
        <source>Unable to set new email: %1</source>
        <translation>Neue E-Mail-Addresse kann nicht gesetzt werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="403"/>
        <source>Error while resetting password: %1</source>
        <translation>Fehler beim Zurücksetzen des Passworts: %1</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="405"/>
        <source>Password Reset</source>
        <translation>Passwort Zurücksetzen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="405"/>
        <source>A password reset mail has been sent to the customer.</source>
        <translation>Eine E-Mail mit den Daten zum Zurücksetzen des Passwortes wurde an den Kunden geschickt.</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="259"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="252"/>
        <location filename="dialogs/customerdlg.cpp" line="387"/>
        <source>Change Mail Address</source>
        <translation>E-Mail-Adresse Ändern</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="253"/>
        <source>Reset Password</source>
        <translation>Passwort Zurücksetzen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="261"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="371"/>
        <location filename="dialogs/customerdlg.cpp" line="378"/>
        <location filename="dialogs/customerdlg.cpp" line="391"/>
        <location filename="dialogs/customerdlg.cpp" line="403"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="371"/>
        <source>Error while changing customer data: %1</source>
        <translation>Fehler beim Ändern der Kundendaten: %1</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="378"/>
        <source>Error while creating customer data: %1</source>
        <translation>Fehler beim Anlegen der Kundendaten: %1</translation>
    </message>
</context>
<context>
    <name>MCustomerListDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="57"/>
        <source>Select a Customer</source>
        <translation>Kunde auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="59"/>
        <source>Customers</source>
        <translation>Kunden</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="80"/>
        <source>Details...</source>
        <translation>Details...</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="82"/>
        <source>Create new...</source>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="85"/>
        <source>Delete...</source>
        <translation>Löschen...</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="93"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="99"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="191"/>
        <source>Delete Customer</source>
        <translation>Kunden Löschen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="194"/>
        <source>Really delete this customer (%1)?</source>
        <translation>Diesen Kunden (%1) wirklich löschen?</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="197"/>
        <source>merge with other entry:</source>
        <translation>mit anderem Eintrag vereinen:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="209"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="211"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="218"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="218"/>
        <source>Failed to delete customer: %1</source>
        <translation>Kann Kunden nicht löschen: %1</translation>
    </message>
</context>
<context>
    <name>MEEPriceEdit</name>
    <message>
        <location filename="dialogs/eventedit.cpp" line="281"/>
        <source>Change Price</source>
        <translation>Preis ändern</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="285"/>
        <source>Price category:</source>
        <translation>Preiskategorie:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="287"/>
        <source>Price:</source>
        <translation>Preis:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="289"/>
        <source>Maximum Seats:</source>
        <translation>Maximale Zahl der Plätze:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="290"/>
        <source>Ordering:</source>
        <translation>Reihenfolge:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="291"/>
        <source>Flags:</source>
        <translation>Flags:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="304"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="306"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="312"/>
        <source>Edit flags of price %1:</source>
        <translation>Flags des Preises %1 ändern:</translation>
    </message>
</context>
<context>
    <name>MEntranceTab</name>
    <message>
        <location filename="mwin/entrancetab.cpp" line="50"/>
        <source>Enter or scan Ticket-ID:</source>
        <translation>Kartennummer eingeben oder scannen:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="65"/>
        <source>Open Order</source>
        <translation>Bestellung öffnen</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="71"/>
        <source>Total:</source>
        <translation>Summe:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="78"/>
        <source>Used:</source>
        <translation>Benutzt:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="83"/>
        <source>Unused:</source>
        <translation>Unbenutzt:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="154"/>
        <source>searching...</source>
        <comment>entrance control</comment>
        <translation>suche Kartendaten...</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="161"/>
        <source>Ticket &quot;%1&quot; Not Valid</source>
        <translation>Karte &quot;%1&quot; ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="165"/>
        <source>Ticket &quot;%1&quot; is not for this event.</source>
        <translation>Karte &quot;%1&quot; ist nicht für diese Veranstaltung.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="169"/>
        <source>Ticket &quot;%1&quot; has already been used</source>
        <translation>Karte &quot;%1&quot; wurde bereits verwendet.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="173"/>
        <source>Ticket &quot;%1&quot; has not been bought.</source>
        <translation>Karte &quot;%1&quot; wurde nicht gekauft.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="177"/>
        <source>Ticket &quot;%1&quot; Ok</source>
        <translation>Karte &quot;%1&quot; Okay.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="181"/>
        <source>Ticket &quot;%1&quot; is not paid for!</source>
        <translation>Karte &quot;%1&quot; ist nicht bezahlt!!!</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="185"/>
        <source>Ticket &quot;%1&quot; cannot be accepted, please check the order!</source>
        <translation>Karte &quot;%1&quot; kann nicht akzeptiert werden, bitte prüfen Sie die Bestellung.</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="262"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="262"/>
        <source>Error while retrieving order: %1</source>
        <translation>Fehler beim Einholen der Bestelldaten: %1</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="284"/>
        <source>Entrance Configuration</source>
        <translation>Konfiguration der Eingangskontrolle</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="290"/>
        <source>Show events that start within hours:</source>
        <translation>Veranstaltungen zeigen die innerhalb von x Stunden beginnen:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="293"/>
        <source>Show events a maximum of hours after they end:</source>
        <translation>Veranstaltungen bis maximal x Stunden nach ihrem Ende zeigen:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="296"/>
        <source>Use Cache:</source>
        <translation>Puffer verwenden:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="297"/>
        <source>Cache update interval:</source>
        <translation>Puffer Auffrischintervall:</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="302"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="304"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MEventEditor</name>
    <message>
        <location filename="dialogs/eventedit.cpp" line="85"/>
        <source>Event Editor</source>
        <translation>Veranstaltungseditor</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="99"/>
        <source>Event</source>
        <translation>Veranstaltung</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="108"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="113"/>
        <source>Artist:</source>
        <translation>Künstler:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="122"/>
        <source>Start Time:</source>
        <translation>Startzeit:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="125"/>
        <location filename="dialogs/eventedit.cpp" line="132"/>
        <source>ddd MMMM d yyyy, h:mm ap</source>
        <comment>time format</comment>
        <translation>ddd, d.M.yyyy hh:mm</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="129"/>
        <source>End Time:</source>
        <translation>Endzeit:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="144"/>
        <source>Room/Place:</source>
        <translation>Raum/Ort:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="153"/>
        <source>Seat Plan:</source>
        <translation>Raumplan:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="162"/>
        <source>Capacity:</source>
        <translation>Sitzplätze:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="168"/>
        <source>Event Cancelled:</source>
        <translation>Veranstaltung absagen:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="176"/>
        <source>Flags:</source>
        <translation>Flags:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="179"/>
        <source>Edit Flags of Event &apos;%1&apos;</source>
        <translation>Flags der Veranstaltung &apos;%1&apos; ändern</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="181"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="183"/>
        <source>The description will be displayed on the web site, please use HTML syntax.</source>
        <translation>Diese Beschreibung wird auf der Webseite benutzt, bitte nutzen Sie HTML.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="187"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="189"/>
        <source>The comment is for internal use only, please add any hints relevant for your collegues.</source>
        <translation>Dieser Kommentar ist nur für den internen Gebrauch, bitte fügen Sie für Kollegen relevante Hinweise hinzu.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="193"/>
        <source>Prices</source>
        <translation>Preise</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="198"/>
        <source>Change Price</source>
        <translation>Preis ändern</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="200"/>
        <source>Add Price</source>
        <translation>Preis hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="202"/>
        <source>Remove Price</source>
        <translation>Preis entfernen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="208"/>
        <source>Hint: Prices with the lowest Ordering number are shown first when selecting a category for tickets.</source>
        <translation>Hinweis: Preise mit der niedrigsten Zahl bei Reihenfolge werden als erstes aufgeführt.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="213"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="216"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="245"/>
        <source>Error while creating event: %1</source>
        <translation>Fehler beim anlegen der Veranstaltung: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="249"/>
        <source>Error while changing event: %1</source>
        <translation>Fehler beim Ändern der Veranstaltung: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Price Category</source>
        <translation>Preiskategorie</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Ticket Capacity</source>
        <translation>max. Anzahl Karten</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Tickets</source>
        <translation>Karten</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Seats Blocked</source>
        <translation>blockierte Plätze</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Ordering</source>
        <translation>Reihenfolge</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Flags</source>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="352"/>
        <source>Price category already exists in this event.</source>
        <translation>Diese Preiskategorie existiert bereits für diese Veranstaltung.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="395"/>
        <source>Cannot remove price &apos;%1&apos; - it has tickets in the database.</source>
        <translation>Preis &apos;%1&apos; kann nicht entfernt werden - es existieren Karten dafür.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="425"/>
        <location filename="dialogs/eventedit.cpp" line="470"/>
        <location filename="dialogs/eventedit.cpp" line="556"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="438"/>
        <source>Select a Seat Plan</source>
        <translation>Raumplan auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="458"/>
        <source>New...</source>
        <comment>new seatplan</comment>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="462"/>
        <source>Select</source>
        <comment>select seatplan</comment>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="464"/>
        <source>Remove Seat Plan</source>
        <comment>remove seatplan from event</comment>
        <translation>Raumplan entfernen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="493"/>
        <source>Error while creating new room: %1</source>
        <translation>Fehler beim Anlegen eines Raumes: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="503"/>
        <source>Please select a room first, since a seat plan must have a room.</source>
        <translation>Bitte wählen Sie zunächst einen Raum aus, da ein Raumplan einen Raum haben muss.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="506"/>
        <source>Please select a seat plan file</source>
        <translation>Bitte wählen Sie eine Raumplan-Datei</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="511"/>
        <source>Cannot open seat plan file &apos;%1&apos;.</source>
        <translation>Raumplan-Datei &apos;%1&apos; kann nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="517"/>
        <source>Seat Plan Name</source>
        <translation>Raumplan-Name</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="517"/>
        <source>Please enter a name for your seat plan:</source>
        <translation>Bitte geben Sie einen Namen für den Raumplan ein:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="525"/>
        <source>Error while creating seat plan: %1</source>
        <translation>Fehler beim Anlegen des Raumplans: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="536"/>
        <source>Select an Artist</source>
        <translation>Künstler auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="550"/>
        <source>New...</source>
        <comment>new artist</comment>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="554"/>
        <source>Select</source>
        <comment>select artist</comment>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="570"/>
        <source>New Artist</source>
        <translation>Neuer Künstler</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="570"/>
        <source>Name of new artist:</source>
        <translation>Name des Künstlers:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="574"/>
        <source>Error while creating new artist: %1</source>
        <translation>Fehler beim Anlegen des Künstlers: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="60"/>
        <location filename="dialogs/eventedit.cpp" line="245"/>
        <location filename="dialogs/eventedit.cpp" line="249"/>
        <location filename="dialogs/eventedit.cpp" line="352"/>
        <location filename="dialogs/eventedit.cpp" line="395"/>
        <location filename="dialogs/eventedit.cpp" line="493"/>
        <location filename="dialogs/eventedit.cpp" line="503"/>
        <location filename="dialogs/eventedit.cpp" line="511"/>
        <location filename="dialogs/eventedit.cpp" line="525"/>
        <location filename="dialogs/eventedit.cpp" line="574"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="60"/>
        <source>Unable to load event from server.</source>
        <translation>Veranstaltung kann nicht vom Server geladen werden.</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="408"/>
        <source>Select a Room</source>
        <translation>Raum auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="419"/>
        <source>New...</source>
        <comment>new room</comment>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="423"/>
        <source>Select</source>
        <comment>select room</comment>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="489"/>
        <source>New Room</source>
        <translation>Neuer Raum</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="489"/>
        <source>Name of new room:</source>
        <translation>Name des Raumes:</translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="102"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
</context>
<context>
    <name>MEventSummary</name>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="39"/>
        <source>Summary for Event %1</source>
        <translation>Übersicht zu Veranstaltung %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="49"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="51"/>
        <source>Artist:</source>
        <translation>Künstler:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="53"/>
        <source>Start:</source>
        <translation>Beginn:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="55"/>
        <source>Capacity:</source>
        <translation>Sitzplätze:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="57"/>
        <source>Tickets currently reserved:</source>
        <translation>Momentan reservierte Karten:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="59"/>
        <source>Tickets currently cancelled:</source>
        <translation>Momentan abgesagte Karten:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="61"/>
        <source>Tickets currently usable:</source>
        <translation>Momentan nutzbare Karten:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="63"/>
        <source>Total Income:</source>
        <translation>erwarteter Umsatz:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Bought</source>
        <translation>Gekauft</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Used</source>
        <translation>Benutzt</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Unused</source>
        <translation>Unbenutzt</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="103"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="105"/>
        <source>Save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="108"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="123"/>
        <location filename="dialogs/eventsummary.cpp" line="187"/>
        <location filename="dialogs/eventsummary.cpp" line="205"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="45"/>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="68"/>
        <source>Tickets</source>
        <translation>Karten</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="85"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="91"/>
        <source>Order: </source>
        <translation>Bestellung:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="93"/>
        <source>Customer: </source>
        <translation>Kunde:</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="123"/>
        <source>Error while retrieving data: %1</source>
        <translation>Fehler beim Datentransfer: %1</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="187"/>
        <location filename="dialogs/eventsummary.cpp" line="205"/>
        <source>Unable to get template file (eventsummary). Giving up.</source>
        <translation>Kann Vorlage (eventsummary) nicht finden. Gebe auf.</translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="212"/>
        <source>Open Document File (*.%1)</source>
        <translation>ODF Datei (*.%1)</translation>
    </message>
</context>
<context>
    <name>MEventsTab</name>
    <message>
        <location filename="mwin/eventstab.cpp" line="96"/>
        <source>&amp;Update Event List</source>
        <translation>&amp;Veranstaltungsliste auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="108"/>
        <source>&amp;Show/Edit details...</source>
        <translation>&amp;Details anzeigen/editieren...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="110"/>
        <source>&amp;New Event...</source>
        <translation>&amp;Neue Veranstaltung...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="101"/>
        <source>Show &amp;old Events</source>
        <translation>vergangene Veranstaltungen anzeigen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="94"/>
        <source>&amp;View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="98"/>
        <source>Event &amp;Summary...</source>
        <translation>Veranstaltungszusammenfassung...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="106"/>
        <source>&amp;Edit</source>
        <translation>Änderungen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="112"/>
        <source>&amp;Clone Current Event...</source>
        <translation>Markierte Veranstaltung klonen...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="114"/>
        <source>C&amp;ancel Event...</source>
        <translation>Veranstaltung absagen...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="117"/>
        <source>&amp;Edit Price Categories...</source>
        <translation>Preiskategorien ändern...</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Start Time</source>
        <translation>Anfangszeit</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Free</source>
        <translation>Frei</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Sold</source>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Capacity</source>
        <translation>Sitzplätze</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="254"/>
        <source>Cancel Event</source>
        <translation>Veranstaltung absagen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="254"/>
        <source>Please enter a reason to cancel event &quot;%1&quot; or abort:</source>
        <translation>Bitte geben Sie einen Grund für die Absage der Veranstaltung &quot;%1&quot; ein:</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="258"/>
        <source>Event Cancelled</source>
        <translation>Veranstaltung abgesagt</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="258"/>
        <source>The event &quot;%1&quot; has been cancelled. Please inform everybody who bought a ticket.</source>
        <translation>Die Veranstaltung &quot;%1&quot; wurde abgesagt. Bitte informieren Sie alle Kunden.</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="260"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="260"/>
        <source>Unable to cancel event &quot;%1&quot;: %2.</source>
        <translation>Kann Veranstaltung &apos;%1&apos; nicht absagen: %2</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="303"/>
        <source>Select Event</source>
        <translation>Veranstaltung auswählen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="318"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="321"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MFlagEditor</name>
    <message>
        <location filename="dialogs/flagedit.cpp" line="35"/>
        <source>ignore</source>
        <translation>ignorieren</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="36"/>
        <source>must have</source>
        <translation>muss gesetzt sein</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="37"/>
        <source>must not have</source>
        <translation>darf nicht gesetzt sein</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="81"/>
        <source>Edit Flags</source>
        <translation>Flags ändern</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="106"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="109"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="111"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
</context>
<context>
    <name>MFlagTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="810"/>
        <source>New Flag...</source>
        <translation>Neues Flag...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="813"/>
        <source>Delete Flag...</source>
        <translation>Flag löschen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="817"/>
        <source>Change Description...</source>
        <translation>Beschreibung ändern...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="835"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="835"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="850"/>
        <source>Create New Flag</source>
        <translation>Neues Flag anlegen</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="850"/>
        <source>Please enter a name for the flag, it must contain only letters and digits.</source>
        <translation>Bitte geben Sie einen Namen für das Flag ein, es darf nur Buchstaben und Ziffern enthalten.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="858"/>
        <location filename="mwin/acltabs.cpp" line="877"/>
        <location filename="mwin/acltabs.cpp" line="897"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="858"/>
        <source>Error while creating flag: %1</source>
        <translation>Fehler beim Anlegen des Flags: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="873"/>
        <source>Really Delete?</source>
        <translation>Wirklich löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="873"/>
        <source>Really delete the flag &apos;%1&apos;? Doing so may make some entities visible or invisible unexpectedly.</source>
        <translation>Das Flag &apos;%1&apos; wirklich löschen? Dies kann einige Veranstaltungen oder andere Einträge unerwartet sichtbar oder unsichtbar machen.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="877"/>
        <source>Error while attempting to delete flag: %1</source>
        <translation>Fehler beim Löschen des Flags: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="891"/>
        <source>Change Flag</source>
        <translation>Flag ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="891"/>
        <source>Please enter a new description for flag &apos;%1&apos;:</source>
        <translation>Bitte geben Sie eine neue Beschreibung für das Flag &apos;%1&apos; ein:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="897"/>
        <source>Error while attempting to alter flag: %1</source>
        <translation>Fehler beim ändern des Flags: %1</translation>
    </message>
</context>
<context>
    <name>MHostTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="344"/>
        <source>New Host...</source>
        <translation>Neuer Host...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="347"/>
        <source>Delete Host...</source>
        <translation>Host löschen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="351"/>
        <source>Generate New Key...</source>
        <translation>Neuen Schlüssel anlegen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="354"/>
        <source>Import...</source>
        <translation>Importieren...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="357"/>
        <source>Export...</source>
        <translation>Exportieren...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="377"/>
        <source>Host Name</source>
        <translation>Hostname</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="377"/>
        <source>Host Key</source>
        <translation>Hostkey</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="391"/>
        <source>Create New Host</source>
        <translation>Neuen Host anlegen</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="391"/>
        <source>Please enter a host name:</source>
        <translation>Bitte geben Sie einen neuen Hostnamen ein:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="410"/>
        <location filename="mwin/acltabs.cpp" line="429"/>
        <location filename="mwin/acltabs.cpp" line="461"/>
        <location filename="mwin/acltabs.cpp" line="483"/>
        <location filename="mwin/acltabs.cpp" line="506"/>
        <location filename="mwin/acltabs.cpp" line="514"/>
        <location filename="mwin/acltabs.cpp" line="519"/>
        <location filename="mwin/acltabs.cpp" line="524"/>
        <location filename="mwin/acltabs.cpp" line="530"/>
        <location filename="mwin/acltabs.cpp" line="536"/>
        <location filename="mwin/acltabs.cpp" line="541"/>
        <location filename="mwin/acltabs.cpp" line="549"/>
        <location filename="mwin/acltabs.cpp" line="577"/>
        <location filename="mwin/acltabs.cpp" line="593"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="410"/>
        <source>Error while creating new host: %1</source>
        <translation>Fehler beim Anlegen eines Host: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="425"/>
        <source>Delete this Host?</source>
        <translation>Diesen Host löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="425"/>
        <source>Really delete host &apos;%1&apos;?</source>
        <translation>Den Host &apos;%1&apos; wirklich löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="429"/>
        <source>Error while deleting host: %1</source>
        <translation>Fehler beim Löschen des Host: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="443"/>
        <source>Change Host Key?</source>
        <translation>Hostkey ändern?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="443"/>
        <source>Really change the key of host &apos;%1&apos;? It will lock users from thist host out until you install the key at it.</source>
        <translation>Wollen Sie wirklich den Hostkey von Host &apos;%1&apos; ändern? Dies wird die Nutzer von diesem Host aussperren bis Sie den neuen Key dort installiert haben.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="461"/>
        <location filename="mwin/acltabs.cpp" line="549"/>
        <source>Error while changing host: %1</source>
        <translation>Fehler beim ändern des Host: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="495"/>
        <source>Import Key from File</source>
        <translation>Key aus Datei importieren</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="506"/>
        <source>Unable to open file %1 for reading: %2</source>
        <translation>Kann Datei %1 nicht zum Lesen öffnen: %2</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="514"/>
        <source>This is not a host key file.</source>
        <translation>Dies ist keine Hostkeydatei.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="519"/>
        <source>This is not a host key/hash file.</source>
        <translation>Dies ist keine Hostdatei.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="524"/>
        <source>This host key file does not contain a valid host name.</source>
        <translation>Die Hostkeydatei enthält keinen gültigen Hostnamen.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="536"/>
        <source>This host hash file does not contain a valid key hash.</source>
        <translation>Diese Datei enthält keinen gültigen Host.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="541"/>
        <source>This host key file does not contain a valid key.</source>
        <translation>Diese Datei enthält keinen gültigen Hostkey.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="582"/>
        <source>Export Hash to File</source>
        <translation>Hashcode in Datei exportieren</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="530"/>
        <source>The key check sum did not match. Please get a clean copy of the host key file.</source>
        <translation>Die Checksumme dieser Datei ist fehlgeschlagen. Bitte besorgen Sie eine neue Kopie der Datei.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="577"/>
        <source>This host cannot be exported.</source>
        <translation>Dieser Host kann nicht exportiert werden.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="471"/>
        <source>Export Key to File</source>
        <translation>Hostkey als Datei speichern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="483"/>
        <location filename="mwin/acltabs.cpp" line="593"/>
        <source>Unable to open file %1 for writing: %2</source>
        <translation>Datei %1 kann nicht zum Schreiben geöffnet werden: %2</translation>
    </message>
</context>
<context>
    <name>MNewCustomerWizard</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="905"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="908"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="911"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="914"/>
        <source>Finish</source>
        <translation>Fertigstellen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="944"/>
        <source>Please enter name and address information.
Please enter it also if it is not needed immediately.</source>
        <translation>Bitte geben Sie Namen und Adressinfomationen ein.
Bitte auch eingeben wenn nicht versendet werden soll!</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="945"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="951"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="954"/>
        <source>Family Name</source>
        <translation>Familienname</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="956"/>
        <source>Given Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="957"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="959"/>
        <source>123 Example Street</source>
        <translation>Beispielstraße 123</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="960"/>
        <source>City:</source>
        <translation>Stadt:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="963"/>
        <source>Zip Code</source>
        <translation>Postleitzahl</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="968"/>
        <source>Chose City</source>
        <translation>Stadt auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="970"/>
        <source>State:</source>
        <translation>Bundesland:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="975"/>
        <source>State (optional)</source>
        <translation>Bundesland (optional - freilassen)</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="977"/>
        <source>Country:</source>
        <translation>Land:</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="984"/>
        <source>New...</source>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1000"/>
        <source>Please enter at least one way of contacting the customer.</source>
        <translation>Bitte geben Sie mindestens eine Möglichkeit ein den Kunden zu erreichen.</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1005"/>
        <source>Please enter %1</source>
        <comment>%1=contact type name</comment>
        <translation>Bitte %1 eingeben</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1055"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1055"/>
        <source>There was an error while creating the customer: %1</source>
        <translation>Beim Anlegen des Kunden ist ein Fehler aufgetreten: %1</translation>
    </message>
</context>
<context>
    <name>MOAItem</name>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="183"/>
        <source>Order</source>
        <translation>bestellen</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="184"/>
        <source>Ticket</source>
        <translation>Eintrittskarte</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="185"/>
        <source>Voucher</source>
        <translation>Gutschein</translation>
    </message>
</context>
<context>
    <name>MOdfEditor</name>
    <message>
        <location filename="templates/odfedit.cpp" line="144"/>
        <location filename="templates/odfedit.cpp" line="546"/>
        <source>ODF Template Editor</source>
        <translation>ODF Template Editor</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="147"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="148"/>
        <source>&amp;Open Template File...</source>
        <translation>Template &amp;Datei öffnen...</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="148"/>
        <source>Ctrl+O</source>
        <comment>open file shortcut</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="149"/>
        <source>&amp;Import ODF File...</source>
        <translation>ODF-Datei &amp;importieren...</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="149"/>
        <source>Ctrl+Shift+O</source>
        <comment>import ODF file shortcut</comment>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="150"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="150"/>
        <source>Ctrl+S</source>
        <comment>save file shortcut</comment>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="151"/>
        <source>Save &amp;as...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="153"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="155"/>
        <source>&amp;Edit</source>
        <translation>Änd&amp;ern</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="156"/>
        <source>Insert &amp;Calculation into current</source>
        <translation>Berechnung an dieser Stelle einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="157"/>
        <source>Insert Calculation behind current</source>
        <translation>Berechnung dahinter einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="158"/>
        <source>&amp;Wrap in Condition</source>
        <translation>In Bedingung verpacken</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="159"/>
        <source>Wrap in &amp;Loop</source>
        <translation>In Schleife einpacken</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="160"/>
        <source>Insert &amp;Else behind current</source>
        <translation>&apos;Else&apos; dahinter einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="161"/>
        <source>Insert Comment into current</source>
        <translation>Kommentar an dieser Stelle einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="162"/>
        <source>Insert Comment behind current</source>
        <translation>Kommentar dahinter einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="164"/>
        <source>Copy current item to clipboard</source>
        <translation>Aktuelles Element kopieren</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="165"/>
        <source>Insert clipboard into current</source>
        <translation>Element einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="166"/>
        <source>Insert clipboard behind current</source>
        <translation>Nach aktuellem Element einfügen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="168"/>
        <source>Unwrap Loop/Condition</source>
        <translation>Schleife/Bedingung aufheben</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="169"/>
        <source>&amp;Remove Item</source>
        <translation>Element entfernen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="171"/>
        <source>&amp;Test</source>
        <translation>&amp;Test</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="172"/>
        <source>Test with &amp;Order...</source>
        <translation>Mit Bestellung testen...</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="173"/>
        <source>Test with Event &amp;Summary...</source>
        <translation>Mit Veranstaltungszusammenfassung testen...</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="188"/>
        <source>Document XML Tree</source>
        <translation>XML Baum</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="216"/>
        <source>&lt;h1&gt;Special Template Tag&lt;h1&gt;</source>
        <translation>&lt;h1&gt;Spezialelement&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="222"/>
        <source>&lt;h1&gt;Plain Text&lt;h1&gt;</source>
        <translation>&lt;h1&gt;Klartext&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="228"/>
        <source>&lt;h1&gt;Tag&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Tag&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="230"/>
        <source>Tag Name:</source>
        <translation>Tagname:</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="234"/>
        <source>Attributes:</source>
        <translation>Attribute:</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="256"/>
        <source>&lt;h1&gt;Loop&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Schleife&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="258"/>
        <source>Loop Variable</source>
        <translation>Schleifenvariable</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="265"/>
        <source>&lt;h1&gt;Calculation&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Berechnung&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="267"/>
        <location filename="templates/odfedit.cpp" line="276"/>
        <source>Expression</source>
        <translation>Ausdruck</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="274"/>
        <source>&lt;h1&gt;Condition&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Bedingung&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="283"/>
        <source>&lt;h1&gt;Comment&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Kommentar&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="324"/>
        <location filename="templates/odfedit.cpp" line="329"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="324"/>
        <source>Unable to open file &apos;%1&apos; for reading.</source>
        <translation>Kann Datei &apos;%1&apos; nicht zum lesen öffnen.</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="329"/>
        <source>Unable to interpret file &apos;%1&apos;. It is not an ODF container (PKZip format).</source>
        <translation>Die Datei &apos;%1&apos; kann nicht als ODF geöffnet werden.</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="382"/>
        <location filename="templates/odfedit.cpp" line="425"/>
        <location filename="templates/odfedit.cpp" line="510"/>
        <location filename="templates/odfedit.cpp" line="851"/>
        <location filename="templates/odfedit.cpp" line="919"/>
        <location filename="templates/odfedit.cpp" line="935"/>
        <location filename="templates/odfedit.cpp" line="1000"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="382"/>
        <source>The file &apos;%1&apos; does not contain a valid ODF file or template of any version.</source>
        <translation>Die Datei &apos;%1&apos; enthält keine gültiges ODF-Datei oder Template.</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="388"/>
        <source>Conversion Info</source>
        <translation>Konvertierungsinformation</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="388"/>
        <source>The file &apos;%1&apos; did contain a version 1 template. It has been converted to version 2.
Please correct all formulas.</source>
        <translation>Die Datei &apos;%1&apos; enthielt ein Template aus MagicSmoke 1. Sie wurde konvertiert.
Bitte passen Sie alle Formeln an!</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="399"/>
        <source>Open ODF Template</source>
        <translation>ODF-Template öffnen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="399"/>
        <source>Open ODF File</source>
        <translation>ODF-Datei öffnen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="400"/>
        <source>ODF Template File (*.od?t);;All Files (*)</source>
        <translation>ODF Templatedatei (*.od?t);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="400"/>
        <source>ODF File (*.od?);;All Files (*)</source>
        <translation>ODF Datei (*.od?);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="425"/>
        <source>Unable to write to file %1</source>
        <translation>Kann Datei %1 nicht schreiben</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="434"/>
        <source>Save ODF Template</source>
        <translation>ODF-Datei Speichern</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="434"/>
        <source>ODF Template (*%1);;All ODF Templates (*.od?t);;All Files (*)</source>
        <translation>ODF Template (*%1);;Alle ODF Templates (*.od?t);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="506"/>
        <source>Test with Order</source>
        <translation>Mit Bestellung Testen</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="506"/>
        <source>Please enter the Order ID of the order you want to use for testing:</source>
        <translation>Bitte geben Sie die Bestellnummer ein, die Sie zum Testen nutzen wollen:</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="510"/>
        <source>Sorry I cannot retrieve this order: %1</source>
        <translation>Es tut mir leid, ich kann diese Bestellung nicht holen: %1</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="548"/>
        <source>ODF Template Editor [%1]</source>
        <translation>ODF Template Editor [%1]</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="642"/>
        <source>&lt;b&gt;Tag Type:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Tagtyp:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="669"/>
        <source>Attribute</source>
        <translation>Attribut</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="669"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="819"/>
        <location filename="templates/odfedit.cpp" line="831"/>
        <source>new comment</source>
        <translation>neuer Kommentar</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="851"/>
        <source>The selected items do not have a common parent, this means I cannot wrap them without screwing up the DOM tree. Please check your selection!</source>
        <translation>Die selektierten Elemente haben kein gemeinsames Elternelement. Dies bedeutet sie können nicht eingepackt werden ohne die Elemente-Hierarchie zu zerstören. Bitte prüfen Sie Ihre Selektion.</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="919"/>
        <location filename="templates/odfedit.cpp" line="935"/>
        <source>There is nothing in the clipboard. Please copy a node first.</source>
        <translation>Es ist nichts in der Zwischenablage.</translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="1000"/>
        <source>Sorry, this kinde of node cannot be copied.</source>
        <translation>Diese Art von Element kann nicht kopiert werden.</translation>
    </message>
</context>
<context>
    <name>MOrderAuditDialog</name>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="109"/>
        <location filename="dialogs/orderauditdlg.cpp" line="113"/>
        <location filename="dialogs/orderauditdlg.cpp" line="394"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="109"/>
        <source>Error while retrieving audit data: %1</source>
        <translation>Fehler beim herunterladen der Logdaten: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="113"/>
        <source>Sorry, no audit data available.</source>
        <translation>Es sind leider keine Log-Daten verfügbar.</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="123"/>
        <source>Order Audit [%1]</source>
        <translation>Bestellungslog [%1]</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="284"/>
        <location filename="dialogs/orderauditdlg.cpp" line="341"/>
        <source>No Data</source>
        <translation>Keine Daten</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="284"/>
        <source>No audit data found for this ticket.</source>
        <translation>Es gibt keine Log-Daten für diese Eintrittskarte.</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="292"/>
        <source>Ticket Audit: %1</source>
        <translation>Eintrittskarten-Log: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="306"/>
        <location filename="dialogs/orderauditdlg.cpp" line="362"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="312"/>
        <location filename="dialogs/orderauditdlg.cpp" line="368"/>
        <source>Date/Time</source>
        <translation>Datum/Zeit</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="313"/>
        <location filename="dialogs/orderauditdlg.cpp" line="369"/>
        <source>User</source>
        <translation>Nutzer</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="314"/>
        <location filename="dialogs/orderauditdlg.cpp" line="370"/>
        <source>Transaction</source>
        <translation>Transaktion</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="315"/>
        <location filename="dialogs/orderauditdlg.cpp" line="372"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="316"/>
        <source>Event Date</source>
        <translation>Veranstaltungsdatum</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="317"/>
        <source>Event</source>
        <translation>Veranstaltung</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="341"/>
        <source>No audit data found for this voucher.</source>
        <translation>Es existieren keine Log-Daten für diesen Gutschein.</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="348"/>
        <source>Voucher Audit: %1</source>
        <translation>Gutschein-Log: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="371"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="373"/>
        <source>Validity</source>
        <translation>Gültigkeit</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="374"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="394"/>
        <source>Unable to get user audit data: %1</source>
        <translation>Kann die Nutzerdaten nicht holen: %1</translation>
    </message>
</context>
<context>
    <name>MOrderItemView</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="994"/>
        <source>Preview Tickets</source>
        <translation>Karten-Vorschau</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1002"/>
        <source>Ticket: </source>
        <translation>Eintrittskarte:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1004"/>
        <source>Voucher: </source>
        <translation>Gutschein:</translation>
    </message>
</context>
<context>
    <name>MOrderWindow</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="106"/>
        <source>Order Details</source>
        <translation>Bestelldetails</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="111"/>
        <source>&amp;Order</source>
        <translation>&amp;Bestellung</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="112"/>
        <source>&amp;Order...</source>
        <translation>Reservierung fest &amp;bestellen...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="113"/>
        <source>C&amp;ancel Order...</source>
        <translation>Bestellung &amp;Stornieren...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="116"/>
        <source>Change Item &amp;Price...</source>
        <translation>Artikelpreis ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="118"/>
        <source>C&amp;hange Ticket Price Category...</source>
        <translation>Preiskategorie der Eintrittskarte ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="138"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="140"/>
        <source>&amp;Payment</source>
        <translation>&amp;Bezahlung</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="141"/>
        <source>Receive &amp;Payment...</source>
        <translation>&amp;bezahlen...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="142"/>
        <source>&amp;Refund...</source>
        <translation>&amp;zurückgeben...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="145"/>
        <source>P&amp;rinting</source>
        <translation>&amp;Druck</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="146"/>
        <source>Print &amp;Bill...</source>
        <translation>&amp;Rechnung drucken...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="147"/>
        <source>Save Bill &amp;as file...</source>
        <translation>Rechnung &amp;speichern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="149"/>
        <source>Print &amp;Tickets...</source>
        <translation>&amp;Eintrittskarten drucken...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="171"/>
        <source>Order ID:</source>
        <translation>Bestell-Nr.:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="175"/>
        <source>Order Date:</source>
        <translation>Bestelldatum:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="177"/>
        <source>Shipping Date:</source>
        <translation>Versandtdatum:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="179"/>
        <source>Customer:</source>
        <translation>Kunde:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="199"/>
        <source>Sold by:</source>
        <translation>Verkauft durch:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="193"/>
        <source>Total Price:</source>
        <translation>Gesamtpreis:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="195"/>
        <source>Already Paid:</source>
        <translation>bereits bezahlt:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="173"/>
        <source>Order State:</source>
        <translation>Bestellstatus:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="114"/>
        <source>&amp;Mark Order as Shipped...</source>
        <translation>Bestellung als versandt markieren...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="366"/>
        <location filename="dialogs/orderwin.cpp" line="372"/>
        <location filename="dialogs/orderwin.cpp" line="405"/>
        <location filename="dialogs/orderwin.cpp" line="411"/>
        <location filename="dialogs/orderwin.cpp" line="443"/>
        <location filename="dialogs/orderwin.cpp" line="468"/>
        <location filename="dialogs/orderwin.cpp" line="511"/>
        <location filename="dialogs/orderwin.cpp" line="536"/>
        <location filename="dialogs/orderwin.cpp" line="561"/>
        <location filename="dialogs/orderwin.cpp" line="592"/>
        <location filename="dialogs/orderwin.cpp" line="598"/>
        <location filename="dialogs/orderwin.cpp" line="622"/>
        <location filename="dialogs/orderwin.cpp" line="629"/>
        <location filename="dialogs/orderwin.cpp" line="662"/>
        <location filename="dialogs/orderwin.cpp" line="668"/>
        <location filename="dialogs/orderwin.cpp" line="683"/>
        <location filename="dialogs/orderwin.cpp" line="691"/>
        <location filename="dialogs/orderwin.cpp" line="708"/>
        <location filename="dialogs/orderwin.cpp" line="751"/>
        <location filename="dialogs/orderwin.cpp" line="765"/>
        <location filename="dialogs/orderwin.cpp" line="779"/>
        <location filename="dialogs/orderwin.cpp" line="813"/>
        <location filename="dialogs/orderwin.cpp" line="846"/>
        <location filename="dialogs/orderwin.cpp" line="880"/>
        <location filename="dialogs/orderwin.cpp" line="899"/>
        <location filename="dialogs/orderwin.cpp" line="917"/>
        <location filename="dialogs/orderwin.cpp" line="928"/>
        <location filename="dialogs/orderwin.cpp" line="947"/>
        <location filename="dialogs/orderwin.cpp" line="962"/>
        <location filename="dialogs/orderwin.cpp" line="977"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="372"/>
        <source>Unable to get template file (ticket.xtt). Giving up.</source>
        <translation>Kann Vorlage (ticket.xtt) nicht finden. Gebe auf.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="449"/>
        <location filename="dialogs/orderwin.cpp" line="489"/>
        <location filename="dialogs/orderwin.cpp" line="788"/>
        <source>Mark as shipped?</source>
        <translation>Als versandt markieren?</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="449"/>
        <location filename="dialogs/orderwin.cpp" line="489"/>
        <location filename="dialogs/orderwin.cpp" line="788"/>
        <source>Mark this order as shipped now?</source>
        <translation>Diese Bestellung jetzt als versandt markieren?</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="511"/>
        <source>Error while trying to pay: %1</source>
        <translation>Fehler während der Bezahlung: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="555"/>
        <source>Enter Refund</source>
        <translation>Rückgabe eingeben</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="555"/>
        <source>Please enter the amount that will be refunded:</source>
        <translation>Bitte geben Sie den Betrag ein, der zurückgegeben wird:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="760"/>
        <source>Cancel Order?</source>
        <translation>Bestellung stornieren?</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="760"/>
        <source>Cancel this order now?</source>
        <translation>Diese Bestellung jetzt stornieren?</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="185"/>
        <source>Delivery Address:</source>
        <translation>Lieferadresse:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="129"/>
        <source>Change Sh&amp;ipping Method...</source>
        <oldsource>Change Commen&amp;t...</oldsource>
        <translation>Versandoption ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="837"/>
        <location filename="dialogs/orderwin.cpp" line="871"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="745"/>
        <location filename="dialogs/orderwin.cpp" line="839"/>
        <location filename="dialogs/orderwin.cpp" line="873"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="120"/>
        <source>Change Voucher Validity...</source>
        <translation>Gutscheingültigkeit ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="122"/>
        <source>&amp;Return Item...</source>
        <translation>Artikel zurückgeben...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="125"/>
        <source>Add Commen&amp;t...</source>
        <translation>Kommentar hinzufügen...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="128"/>
        <source>Change C&amp;omments...</source>
        <translation>Kommentare ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="131"/>
        <source>Change Invoice Address...</source>
        <translation>Rechnungsadresse ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="133"/>
        <source>Change Delivery Address...</source>
        <translation>Lieferadresse ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="135"/>
        <source>Customer Data...</source>
        <translation>Kundendaten...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="150"/>
        <source>Print V&amp;ouchers...</source>
        <translation>Gutscheine drucken...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="151"/>
        <source>Print &amp;Current Item...</source>
        <translation>Aktuellen Artikel drucken...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="152"/>
        <source>&amp;View Items...</source>
        <translation>Artikel ansehen...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="154"/>
        <source>&amp;Audit</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="155"/>
        <source>Voucher History...</source>
        <translation>Gutscheinhistorie...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="157"/>
        <source>Ticket History...</source>
        <translation>Eintrittskartenhistorie...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="159"/>
        <source>Order History...</source>
        <translation>Bestellhistorie...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="181"/>
        <source>Invoice Address:</source>
        <translation>Rechnungsadresse:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="189"/>
        <source>Shipping Method:</source>
        <translation>Versandoption:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="191"/>
        <source>Shipping Costs:</source>
        <translation>Versandkosten:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="197"/>
        <source>Coupon:</source>
        <translation>Rabattcode:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="201"/>
        <source>Order Comments:</source>
        <translation>Bestellkommentare:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="245"/>
        <source>(none)</source>
        <comment>no coupon</comment>
        <translation>(keiner)</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Item ID</source>
        <translation>Artikelnummer</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="277"/>
        <source>Begin: %1</source>
        <translation>Beginn: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="284"/>
        <source>Voucher (current value: %1)</source>
        <translation>Gutschein (aktueller Wert: %1)</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="285"/>
        <source>Valid till: %1</source>
        <translation>Gültig bis: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="294"/>
        <source>%1x %2</source>
        <translation>%1x %2</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="366"/>
        <source>There are no tickets left to print.</source>
        <translation>Es gibt keine Eintrittskarten zu drucken.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="405"/>
        <source>There are no vouchers left to print.</source>
        <translation>Es gibt keine Gutscheine zu drucken.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="411"/>
        <source>Unable to get template file (voucher.xtt). Giving up.</source>
        <translation>Kann Vorlage (voucher) nicht finden. Gebe auf.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="443"/>
        <location filename="dialogs/orderwin.cpp" line="468"/>
        <source>Unable to get template file (bill). Giving up.</source>
        <translation>Kann Vorlage (bill) nicht finden. Gebe auf.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="518"/>
        <source>Payment Info</source>
        <translation>Bezahlinfo</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="519"/>
        <source>Payment successful, but only %1 was required, please hand back the remaining %2.</source>
        <translation>Bezahlung erfolgreich. Nur %1 waren nötig, bitte %2 Wechselgeld zurückgeben.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="622"/>
        <source>Error getting event, please try again.</source>
        <translation>Konnte Veranstalung nicht herunterlden, bitte versuchen Sie es noch einmal.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="629"/>
        <source>Cannot select another price category - there are none left.</source>
        <translation>Kann keine weitere Preiskategorie wählen - es sind keine mehr übrig.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="635"/>
        <source>Select Price Category</source>
        <translation>Preiskategorie wählen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="641"/>
        <source>Please chose a price category:</source>
        <translation>Bitte wählen Sie eine Preiskategorie:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="653"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="708"/>
        <source>This is not a voucher.</source>
        <translation>Dies ist kein Gutschein.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="720"/>
        <source>Change Validity Date</source>
        <translation>Gültigkeitsdatum ändern</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="723"/>
        <source>Changing validity date of voucher &apos;%1&apos;:</source>
        <translation>Ändere Gültigkeitsdatum von Gutschein &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="724"/>
        <source>Current validity: &apos;%1&apos;</source>
        <translation>Aktuelle Gültigkeit: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="726"/>
        <source>Indefinitely valid</source>
        <translation>Unbegrenzt gültig</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="731"/>
        <source>yyyy-MM-dd</source>
        <comment>Date format for editing voucher validity date</comment>
        <translation>dd.MM.yyyy</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="736"/>
        <source>Comment...</source>
        <translation>Kommentar...</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="742"/>
        <source>&amp;Set Validity</source>
        <translation>Gültigkeit ändern</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="751"/>
        <source>Error whily trying to change voucher validity: %1</source>
        <translation>Ein Fehler ist beim Ändern der Gültigkeit aufgetreten: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="813"/>
        <source>Error while marking order as shipped: %1</source>
        <translation>Fehler während die Bestellung and versandt markiert wurde: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="826"/>
        <source>Change comments: order %1</source>
        <translation>Bestellkommentare ändern [%1]</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="846"/>
        <location filename="dialogs/orderwin.cpp" line="880"/>
        <source>There was a problem uploading the new comment: %1</source>
        <translation>Fehler beim Hochladen des neuen Kommentars: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="858"/>
        <source>Add comment: order %1</source>
        <translation>Kommentar hinzufügen [%1]</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="899"/>
        <source>Error while changing shipping: %1</source>
        <translation>Fehler beim Ändern der Versandmethode: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="912"/>
        <source>No Delivery Address</source>
        <translation>Keine Lieferadresse</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="917"/>
        <location filename="dialogs/orderwin.cpp" line="947"/>
        <source>Unable to set address, server error: %1</source>
        <translation>Kann die Adresse nicht ändern, Fehler: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="928"/>
        <source>Unable to get customer data: %1</source>
        <translation>Kann Kundendaten nicht abholen: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="942"/>
        <source>No Invoice Address</source>
        <translation>Keine Rechnungsadresse</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="962"/>
        <source>The selected item is not a voucher.</source>
        <translation>Sie haben keinen Gutschein gewählt.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="977"/>
        <source>The selected item is not a ticket.</source>
        <translation>Das ausgewählte Element ist keine Eintrittskarte.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Date/Time</source>
        <translation>Datum/Zeit</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="477"/>
        <source>Open Document File (*.%1)</source>
        <translation>ODF Datei (*.%1)</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="536"/>
        <source>Error while trying to pay with voucher &apos;%1&apos;: %2</source>
        <translation>Fehler bei der Bezahlung mit Gutschein &apos;%1&apos;: %2</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="541"/>
        <source>Successfully paid order %1 with voucher &apos;%2&apos;.
Amount deducted: %3
Remaining value of this voucher: %4</source>
        <translation>Bestellung %1 wurde erfolgreich mit Gutschein &apos;%2&apos; bezahlt.
Betrag der vom Gutschein abgezogen wurde: %3
Verbleibender Betrag auf dem Gutschein: %4</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="561"/>
        <source>Error while trying to refund: %1</source>
        <translation>Fehler beim Zurückgeben des Betrags: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="586"/>
        <source>Enter Price</source>
        <translation>Bitte Preis eingeben</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="586"/>
        <source>Please enter the new price for the ticket:</source>
        <translation>Bitte neuen Preis für die Eintrittskarte eingeben:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="592"/>
        <location filename="dialogs/orderwin.cpp" line="662"/>
        <source>Error while attempting to change ticket price: %1</source>
        <translation>Fehler beim Ändern des Kartenpreises: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="598"/>
        <location filename="dialogs/orderwin.cpp" line="668"/>
        <source>Cannot change this item type.</source>
        <translation>Diese Artikelart kann nicht geändert werden.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="683"/>
        <source>Cannot return this item type.</source>
        <translation>Diese Artikelart kann nicht zurückgegeben werden.</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="687"/>
        <source>Return Ticket or Voucher</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="687"/>
        <source>Do you really want to return this ticket or voucher?</source>
        <translation>Wollen Sie diese Karten oder diesen Gutschein zurückgeben?</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="691"/>
        <source>Error whily trying to return item: %1</source>
        <translation>Fehler beim Zurückgeben des Artikels: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="765"/>
        <source>Error while cancelling order: %1</source>
        <translation>Fehler beim Stornieren der Bestellung: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="779"/>
        <source>Error while changing order status: %1</source>
        <translation>Fehler beim Ändern des Bestellstatus: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="792"/>
        <source>Set shipping time</source>
        <translation>Versandzeit setzen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="798"/>
        <source>Enter the shipping time:</source>
        <translation>Bitte geben Sie die Versandzeit ein:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="804"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="655"/>
        <location filename="dialogs/orderwin.cpp" line="806"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="530"/>
        <source>Enter Voucher</source>
        <translation>Gutschein eingeben</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="530"/>
        <source>Please enter the ID of the voucher you want to use:</source>
        <translation>Bitte geben Sie die Nummer des Gutscheins ein, den Sie verwenden wollen:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="540"/>
        <source>Voucher Info</source>
        <translation>Gutscheininformation</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="143"/>
        <source>Pay with &amp;Voucher...</source>
        <translation>Mit Gutschein bezahlen...</translation>
    </message>
</context>
<context>
    <name>MOrdersByUserDlg</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="444"/>
        <source>Select User Criteria</source>
        <translation>Nutzer auswählen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="456"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="458"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="461"/>
        <source>My orders:</source>
        <translation>Meine Bestellungen:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="461"/>
        <source>look for my own orders</source>
        <translation>nur die eigenen Bestellungen zeigen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="462"/>
        <source>User Name:</source>
        <translation>Nutzername:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="465"/>
        <source>Maximum Age (days):</source>
        <translation>Maximales Alter (in Tagen):</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="468"/>
        <source>Limit:</source>
        <translation>Limit:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="469"/>
        <source>Orders that the user created</source>
        <translation>Bestellungen, die der Nutzer angelegt hat</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="470"/>
        <source>Include all orders the user touched</source>
        <translation>alle Bestellungen zeigen, die der Nutzer bearbeitet hat (sonst: nur solche, die er angelegt hat)</translation>
    </message>
</context>
<context>
    <name>MOrdersReport</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="520"/>
        <source>Order Report</source>
        <translation>Bestellreport</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="530"/>
        <location filename="mwin/orderstab.cpp" line="796"/>
        <source>Sums</source>
        <translation>Summen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="535"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="535"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Orders</source>
        <translation>Anzahl Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="536"/>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Tickets</source>
        <translation>Anzahl Eintrittskarten</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="536"/>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Vouchers</source>
        <translation>Anzahl Gutscheine</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Money</source>
        <translation>Geldsumme</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Paid</source>
        <translation>Summe bezahlt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Due</source>
        <translation>Summe ausstehend</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="541"/>
        <source>By Order</source>
        <translation>Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Order ID</source>
        <translation>Bestell-Nr.</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Paid</source>
        <translation>Bezahlt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Due</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="550"/>
        <location filename="mwin/orderstab.cpp" line="798"/>
        <source>By Day</source>
        <translation>Nach Tagen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="555"/>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="561"/>
        <location filename="mwin/orderstab.cpp" line="800"/>
        <source>Financial</source>
        <translation>Finanzbericht</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>User</source>
        <translation>Nutzer</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Moved</source>
        <translation>Transfersumme</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="576"/>
        <source>Save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="579"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="702"/>
        <source>Total Orders</source>
        <translation>Alle Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="703"/>
        <source>Reserved Orders</source>
        <translation>Reservierungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="704"/>
        <source>Cancelled</source>
        <translation>Stornierungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="705"/>
        <source>Placed</source>
        <translation>Bestellt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="706"/>
        <source>Sent</source>
        <translation>Versandt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="763"/>
        <source>Save Report</source>
        <translation>Report speichern</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="776"/>
        <location filename="mwin/orderstab.cpp" line="781"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="776"/>
        <source>Unable to create file &apos;%1&apos;.</source>
        <translation>Kann Datei &apos;%1&apos; nicht erzeugen.</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="781"/>
        <source>Ooops. Unable to create ZIP structure in file &apos;%1&apos;.</source>
        <translation>Hoppla. Kann in &apos;%1&apos; keine ZIP-Struktur anlegen.</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="797"/>
        <source>Orders</source>
        <translation>Bestellungen</translation>
    </message>
</context>
<context>
    <name>MOrdersTab</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="73"/>
        <source>-select mode-</source>
        <translation>-Modus auswählen-</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="74"/>
        <source>All Orders</source>
        <translation>Alle Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="75"/>
        <source>Open Orders</source>
        <translation>Offene Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="76"/>
        <source>Open Reservations</source>
        <translation>Reservierungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="77"/>
        <source>Outstanding Payments</source>
        <translation>Noch nicht bezahlt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="78"/>
        <source>Outstanding Refunds</source>
        <translation>Offene Rückerstattungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="79"/>
        <source>Undelivered Orders</source>
        <translation>Nicht ausgelieferte Bestellungen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="81"/>
        <source>-search result-</source>
        <translation>-Suchresultat-</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="91"/>
        <source>Update</source>
        <translation>Auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="94"/>
        <source>Details...</source>
        <translation>Details...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="98"/>
        <source>Orders since...</source>
        <translation>Bestellungen seit...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="102"/>
        <source>Find by Ticket...</source>
        <translation>Mit Kartennummer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="105"/>
        <source>Find by Event...</source>
        <translation>Nach Veranstaltung suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="108"/>
        <source>Find by Customer...</source>
        <translation>Nach Kunde suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="111"/>
        <source>Find by User...</source>
        <translation>Nach Nutzer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="114"/>
        <source>Find by Order ID...</source>
        <translation>Nach Bestellnummer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="132"/>
        <source>&amp;Order List</source>
        <translation>Bestellungsliste</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="134"/>
        <source>Update Order &amp;List</source>
        <translation>Bestellliste auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="136"/>
        <source>Order &amp;Details...</source>
        <translation>Bestelldetails</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="139"/>
        <source>Orders &amp;since...</source>
        <translation>Bestellungen seit...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="142"/>
        <source>Find by &amp;Ticket...</source>
        <translation>Mit Kartennummer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="144"/>
        <source>Find by &amp;Event...</source>
        <translation>Nach Veranstaltung suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="146"/>
        <source>Find by &amp;Customer...</source>
        <translation>Nach Kunde suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="148"/>
        <source>Find by &amp;User...</source>
        <translation>Nach Nutzer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="150"/>
        <source>Find by &amp;Order ID...</source>
        <translation>Nach Bestellnummer suchen...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="153"/>
        <source>Generate &amp;Report...</source>
        <translation>Report generieren...</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Total</source>
        <translation>Gesamt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Paid</source>
        <translation>bezahlt</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Customer</source>
        <translation>Kunde</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="233"/>
        <location filename="mwin/orderstab.cpp" line="276"/>
        <location filename="mwin/orderstab.cpp" line="300"/>
        <location filename="mwin/orderstab.cpp" line="317"/>
        <location filename="mwin/orderstab.cpp" line="321"/>
        <location filename="mwin/orderstab.cpp" line="342"/>
        <location filename="mwin/orderstab.cpp" line="371"/>
        <location filename="mwin/orderstab.cpp" line="393"/>
        <location filename="mwin/orderstab.cpp" line="397"/>
        <location filename="mwin/orderstab.cpp" line="416"/>
        <location filename="mwin/orderstab.cpp" line="424"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="233"/>
        <location filename="mwin/orderstab.cpp" line="276"/>
        <location filename="mwin/orderstab.cpp" line="342"/>
        <source>There was a problem retrieving the order list: %1</source>
        <translation>Fehler beim Herunterladen des Bestellliste: %1</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="249"/>
        <source>Select Date</source>
        <translation>Datum wählen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="253"/>
        <source>Please select the date and time:</source>
        <translation>Bitte wählen Sie ein Datum und eine Zeit:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="256"/>
        <source>ddd MMMM d yyyy, h:mm ap</source>
        <comment>time format</comment>
        <translation>ddd, d.M.yyyy hh:mm</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="300"/>
        <location filename="mwin/orderstab.cpp" line="393"/>
        <source>Error while retrieving order: %1</source>
        <translation>Fehler beim Herunterladen der Bestellung: %1</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="312"/>
        <source>Enter Ticket</source>
        <translation>Bitte Ticket eingeben</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="312"/>
        <source>Please enter the ID of one of the tickets of the order you seek:</source>
        <translation>Bitte geben Sie die Nr. einer Karte aus der gesuchten Bestellung ein:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="317"/>
        <source>Error while searching for order: %1</source>
        <translation>Fehler beim Suchen nach der Bestellung: %1</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="321"/>
        <source>Order for barcode &apos;%1&apos; not found.</source>
        <translation>Zu Barcode &apos;%1&apos; wurde keine Bestellung gefunden.</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="416"/>
        <location filename="mwin/orderstab.cpp" line="424"/>
        <source>Unable to get user orders, server error: %1</source>
        <translation>Kann nutzerspezifische Bestellungen nicht holen, Fehler: %1</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="263"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="265"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="371"/>
        <source>Error while retrieving order list: %1</source>
        <translation>Fehler beim Herunterladen der Bestellliste: %1</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="388"/>
        <source>Enter Order ID</source>
        <translation>Bestellnummer eingeben</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="388"/>
        <source>Please enter the ID of the order you want to display:</source>
        <translation>Bitte geben Sie die Bestellnummer der Bestellung ein, die Sie ansehen wollen:</translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="397"/>
        <source>This order does not exist.</source>
        <translation>Diese Bestellung existiert nicht.</translation>
    </message>
</context>
<context>
    <name>MOverview</name>
    <message>
        <location filename="mwin/overview.cpp" line="90"/>
        <source>&amp;Session</source>
        <translation>&amp;Session</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="91"/>
        <source>&amp;Re-Login</source>
        <translation>&amp;Login wiederholen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="95"/>
        <source>&amp;Close Session</source>
        <translation>Session &amp;schließen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="163"/>
        <source>Events</source>
        <translation>Veranstaltungen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="102"/>
        <source>Return &amp;ticket or voucher...</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="97"/>
        <source>&amp;Actions</source>
        <translation>&amp;Aktionen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="101"/>
        <source>Tickets and &amp;Vouchers</source>
        <translation>Eintrittskarten und Gutscheine</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="107"/>
        <source>&amp;Empty voucher...</source>
        <translation>Gutschein entleeren...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="116"/>
        <source>&amp;Templates</source>
        <translation>Vorlagen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="139"/>
        <source>&amp;Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="242"/>
        <location filename="mwin/overview.cpp" line="252"/>
        <location filename="mwin/overview.cpp" line="293"/>
        <location filename="mwin/overview.cpp" line="329"/>
        <location filename="mwin/overview.cpp" line="333"/>
        <location filename="mwin/overview.cpp" line="371"/>
        <location filename="mwin/overview.cpp" line="590"/>
        <location filename="mwin/overview.cpp" line="594"/>
        <location filename="mwin/overview.cpp" line="615"/>
        <location filename="mwin/overview.cpp" line="744"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="98"/>
        <source>&amp;Show all customers</source>
        <translation>&amp;Alle Kunden anzeigen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="167"/>
        <source>Shopping Cart</source>
        <translation>Einkaufswagen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="92"/>
        <source>Change my &amp;Password</source>
        <translation>Mein &amp;Passwort ändern</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="252"/>
        <source>Error setting password: %1</source>
        <translation>Passwort kann nicht gesetzt werden: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="179"/>
        <source>Order List</source>
        <translation>Bestellungsliste</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="322"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="185"/>
        <source>Entrance</source>
        <translation>Einlasskontrolle</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="136"/>
        <source>&amp;Backup now...</source>
        <translation>&amp;Jetzt Backup machen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="141"/>
        <source>&amp;Auto-Refresh settings...</source>
        <translation>Auto-Auffrisch-Einstellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="143"/>
        <source>&amp;Display settings...</source>
        <translation>Anzeigeeinstellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="130"/>
        <source>&amp;Administration</source>
        <translation>&amp;Administration</translation>
    </message>
    <message>
        <source>Switch to &amp;Wizard...</source>
        <translation type="vanished">Auf Assistent umschalten...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="110"/>
        <source>&amp;Options</source>
        <translation>&amp;Optionen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="113"/>
        <source>Edit &amp;Payment Options...</source>
        <translation>Bezahloptionen ändern...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="122"/>
        <source>&amp;ODF Editor...</source>
        <translation>ODF Editor</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="124"/>
        <source>A&amp;udit</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="125"/>
        <source>&amp;Order Audit...</source>
        <translation>Bestellungslog...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="126"/>
        <source>&amp;Ticket Audit...</source>
        <translation>Eintrittskarten-Log...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="127"/>
        <source>&amp;Voucher Audit...</source>
        <translation>Gutschein-Log...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="128"/>
        <source>&amp;User Audit...</source>
        <translation>Nutzerlog...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="131"/>
        <source>&amp;User Administration...</source>
        <translation>Nutzerverwaltung...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="140"/>
        <source>&amp;Change Language...</source>
        <translation>Sprache ändern...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="144"/>
        <source>&amp;Label Printing settings...</source>
        <translation>Etikettendruck einrichten</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="145"/>
        <source>&amp;OpenOffice settings...</source>
        <translation>OpenOffice Einstellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="146"/>
        <source>&amp;Barcode Scanner settings...</source>
        <translation>&amp;Barcode-Scanner-Einstellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="148"/>
        <source>&amp;Session Manager</source>
        <translation>&amp;Session Manager</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="155"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="156"/>
        <source>Getting data...</source>
        <translation>Hole Daten...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="161"/>
        <source>Getting events...</source>
        <translation>Hole Veranstaltungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="177"/>
        <source>Getting Orders...</source>
        <translation>Hole Bestellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="183"/>
        <source>Getting Entrance Data...</source>
        <translation>Hole Eingangskontrolldaten...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="288"/>
        <source>Return Ticket/Voucher</source>
        <translation>Eintrittskarte/Gutschein zurückgeben</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="288"/>
        <source>Please enter the ticket or voucher ID to return:</source>
        <translation>Bitte geben Sie die Eintrittskarten- oder Gutscheinnummer ein, die zurückgegeben werden soll:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="293"/>
        <source>This ticket/voucher cannot be returned: %1</source>
        <translation>Diese Karte/dieser Gutschein kann nicht zurückgegeben werden: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="310"/>
        <source>Reason for deducting:</source>
        <translation>Grund für den Abzug:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="329"/>
        <source>Unable to deduct voucher: %1</source>
        <translation>Kann kein Geld von diesem Gutschein abziehen: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="333"/>
        <source>Voucher does not contain enough money. Money left: %1</source>
        <translation>Dieser Gutschein hat nicht genug Restwert: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="347"/>
        <source>Invalidate Voucher</source>
        <translation>Ungültiger Gutschein</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="371"/>
        <source>Unable to invalidate voucher: %1</source>
        <translation>Kann Gutschein nicht ungültig machen: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="374"/>
        <source>Invalidated Voucher</source>
        <translation>Gutschein ungültig</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="375"/>
        <source>The voucher &apos;%1&apos;has been invalidated.</source>
        <translation>Der Gutschein &apos;%1&apos; wurde ungültig gemacht.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="394"/>
        <source>Refresh Settings</source>
        <translation>Auffrischeinstellungen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="399"/>
        <source>Refresh Rate (minutes):</source>
        <translation>Auffrischrate (Minuten):</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="405"/>
        <source>refresh &amp;event list</source>
        <translation>Veranstaltungsliste auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="407"/>
        <source>refresh &amp;user list</source>
        <translation>Nutzerliste auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="409"/>
        <source>refresh &amp;host list</source>
        <translation>Rechnerliste auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="363"/>
        <location filename="mwin/overview.cpp" line="418"/>
        <location filename="mwin/overview.cpp" line="473"/>
        <location filename="mwin/overview.cpp" line="551"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="350"/>
        <source>This will invalidate/empty the voucher. It will no longer be usable afterwards, but will still have to be paid for.</source>
        <translation>Diese Aktion macht den Gutschein ungültig. Er kann danach nicht mehr zur Bezahlung eingesetzt werden, muss aber selbst bezahlt werden/sein.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="351"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="354"/>
        <source>Please enter a reason for invalidating the voucher.</source>
        <translation>Bitte geben Sie einen Grund für die Ungültigkeit des Gutscheins ein.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="355"/>
        <source>Voucher ID/Barcode:</source>
        <translation>Gutscheincode/Barcode:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="357"/>
        <source>Please scan the barcode.</source>
        <translation>Bitte scannen Sie den Barcode.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="365"/>
        <location filename="mwin/overview.cpp" line="420"/>
        <location filename="mwin/overview.cpp" line="475"/>
        <location filename="mwin/overview.cpp" line="553"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="457"/>
        <source>No Logging</source>
        <translation>Kein Log</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="459"/>
        <source>Medium Logging</source>
        <translation>Mittlere Logdetails</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="503"/>
        <source>Display Settings</source>
        <translation>Anzeigeeinstellungen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="508"/>
        <source>Event settings</source>
        <translation>Veranstaltungseinstellungen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="511"/>
        <source>Maximum event age (days, 0=show all):</source>
        <translation>Maximales Veranstaltungsalter (in Tagen, 0=alle anzeigen):</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="674"/>
        <source>Order ID</source>
        <translation>Bestell-Nr.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="674"/>
        <source>Please enter the ID of the order you want to audit:</source>
        <translation>Bitte geben Sie die Bestellnummer der Bestellung ein, die Sie ansehen wollen:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="684"/>
        <source>Ticket ID</source>
        <translation>Eintrittskarten-Nummer</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="684"/>
        <source>Please enter the ID of the ticket you want to audit:</source>
        <translation>Bitte geben Sie die Nummer der Eintrittskarte ein, den Sie verwenden wollen:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="694"/>
        <source>Voucher ID</source>
        <translation>Gutscheinnummer</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="694"/>
        <source>Please enter the ID of the voucher you want to audit:</source>
        <translation>Bitte geben Sie die Nummer des Gutscheins ein, den Sie verwenden wollen:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="715"/>
        <source>Audit User</source>
        <comment>audit dialog</comment>
        <translation>Nutzerlog</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="720"/>
        <location filename="mwin/overview.cpp" line="723"/>
        <source>User Name:</source>
        <comment>audit dialog</comment>
        <translation>Nutzername:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="727"/>
        <source>Earliest Info</source>
        <comment>audit dialog</comment>
        <translation>Beginnend ab:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="744"/>
        <source>Cannot query an empty user name.</source>
        <comment>audit dialog</comment>
        <translation>Ein leerer Nutzername kann nicht verwendet werden.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="117"/>
        <source>&amp;Edit Templates...</source>
        <translation>Vorlagen ändern...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="119"/>
        <source>&amp;Update Templates Now</source>
        <translation>Vorlagen jetzt auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="111"/>
        <source>Edit &amp;Shipping Options...</source>
        <translation>Versandoptionen editieren</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="411"/>
        <source>refresh &amp;shipping list</source>
        <translation>Versandoptionen auffrischen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="105"/>
        <source>&amp;Deduct from voucher...</source>
        <translation>Geld von Gutschein abziehen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="142"/>
        <source>&amp;Server Access settings...</source>
        <translation>Serverzugriffseinstellungen...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="134"/>
        <source>Backup &amp;Settings...</source>
        <translation>Einstellungen Sicherungskopie...</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="301"/>
        <source>Deduct from Voucher</source>
        <translation>Von Gutschein abziehen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="304"/>
        <source>Using a voucher to pay outside the system.</source>
        <translation>Einen Gutschein nutzen um außerhalb des Systems zu bezahlen.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="308"/>
        <source>Amount to deduct:</source>
        <translation>Abzuziehender Betrag:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="312"/>
        <source>Voucher ID:</source>
        <translation>Gutscheinnummer:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="320"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="336"/>
        <source>Deducted from Voucher</source>
        <translation>Von Gutschein abziehen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="337"/>
        <source>Value taken from voucher: %1
Value remaining on voucher: %2</source>
        <translation>Vom Gutschein abgezogener Betrag: %1
Verbleibender Betrag: %2</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="445"/>
        <source>Server Access Settings</source>
        <translation>Serverzugriffseinstellungen</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="449"/>
        <source>Request Timeout (seconds):</source>
        <translation>max. Anfragezeit (Sekunden):</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="455"/>
        <source>Log Level:</source>
        <translation>Logstufe:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="458"/>
        <source>Minimal Logging</source>
        <translation>Minimales Log</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="460"/>
        <source>Log Details on Error</source>
        <translation>Bei Fehlern Details</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="461"/>
        <source>Always Log Details</source>
        <translation>Immer Details</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="611"/>
        <source>Backup</source>
        <translation>Sicherung</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="611"/>
        <source>The backup was successful.</source>
        <translation>Die Sicherung war erfolgreich.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="615"/>
        <source>Cannot create backup file.</source>
        <translation>Kann Sicherungsdatei nicht anlegen.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="242"/>
        <source>I was unable to renew the login at the server.</source>
        <translation>Der Login am Server konnte nicht erneuert werden.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="517"/>
        <source>Maximum order list age</source>
        <translation>Maximales Alter von Bestellungen in der Liste</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="520"/>
        <source>Age in days, 0=show all.</source>
        <translation>Alter in Tagen, 0=alle zeigen.</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="521"/>
        <source>When showing all orders:</source>
        <translation>Wenn alle Bestellungen angezeigt werden:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="526"/>
        <source>When showing open orders:</source>
        <translation>Wenn offene Bestellungen angezeigt werden:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="531"/>
        <source>When searching by event:</source>
        <translation>Wenn nach Veranstaltung gesucht wird:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="536"/>
        <source>When searching by customer:</source>
        <translation>Wenn nach Kunde gesucht wird:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="541"/>
        <source>Default age when searching by date:</source>
        <translation>Standardalter wenn nach Datum gesucht wird:</translation>
    </message>
    <message>
        <source>Script settings</source>
        <translation type="vanished">Scripteinstellungen</translation>
    </message>
    <message>
        <source>Show script debugger:</source>
        <translation type="vanished">Script-Debugger zeigen:</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="590"/>
        <source>Backup failed with error (%2): %1</source>
        <translation>Das Backup ist mit Fehler (%2) fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="594"/>
        <source>Backup returned empty.</source>
        <translation>Das Backup ist leer.</translation>
    </message>
</context>
<context>
    <name>MPCDEdit</name>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="123"/>
        <source>New Price Category</source>
        <translation>Neue Preiskategorie</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="123"/>
        <source>Change Price Category</source>
        <translation>Preiskategorie ändern</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="130"/>
        <source>Category Name:</source>
        <translation>Name der Kategorie:</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="131"/>
        <source>Category Abbreviation:</source>
        <translation>Abkürzung der Kategorie:</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="133"/>
        <source>Formula:</source>
        <translation>Formel:</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="134"/>
        <source>Flags:</source>
        <translation>Flags:</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="141"/>
        <source>Create</source>
        <translation>Anlegen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="141"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="143"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="150"/>
        <source>Flags of price category &apos;%1&apos;:</source>
        <translation>Flags der Preiskategorie &apos;%1&apos;:</translation>
    </message>
</context>
<context>
    <name>MPasswordChange</name>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="26"/>
        <source>Change my password</source>
        <translation>Mein Passwort ändern</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="32"/>
        <source>Old Password:</source>
        <translation>Altes Passwort:</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="36"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="39"/>
        <source>Repeat Password:</source>
        <translation>Paswort wiederholen:</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="47"/>
        <source>Set Password</source>
        <translation>Passwort setzen</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="49"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="28"/>
        <source>Reset password of user &quot;%1&quot;</source>
        <translation>Passwort des Nutzers &quot;%1&quot; zurücksetzen</translation>
    </message>
</context>
<context>
    <name>MPaymentDialog</name>
    <message>
        <location filename="dialogs/payedit.cpp" line="315"/>
        <source>Payment</source>
        <translation>Bezahlung</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="318"/>
        <source>Please enter the payment data below.</source>
        <translation>Bitte geben Sie die Bezahlinformationen ein.</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="319"/>
        <source>Amount paid:</source>
        <translation>Bezahlter Betrag:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="320"/>
        <source>Payment Type:</source>
        <translation>Zahlart:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="322"/>
        <source>Data?:</source>
        <translation>Daten?:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="328"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="330"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="383"/>
        <location filename="dialogs/payedit.cpp" line="386"/>
        <source>(not required)</source>
        <translation>(nicht benötigt)</translation>
    </message>
</context>
<context>
    <name>MPaymentEditor</name>
    <message>
        <location filename="dialogs/payedit.cpp" line="50"/>
        <source>Edit Payment Options</source>
        <translation>Bezahloptionen ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="64"/>
        <source>Default Payment Type:</source>
        <translation>Standardbezahlart:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="69"/>
        <source>Change Description</source>
        <translation>Beschreibung ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="72"/>
        <source>Change Data</source>
        <translation>Daten ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="75"/>
        <source>Change Flags</source>
        <translation>Flags ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="78"/>
        <source>Change Default</source>
        <translation>Standard ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="82"/>
        <source>Add Option</source>
        <translation>Option hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="85"/>
        <source>Delete Option</source>
        <translation>Option löschen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="93"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Data Name</source>
        <translation>Daten-Name</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Data Default</source>
        <translation>Daten-Vorgabe</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Flags</source>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="121"/>
        <source>Payment Option Description</source>
        <translation>Bezahloptionsbeschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="121"/>
        <source>Please select a new description for this payment option:</source>
        <translation>Bitte geben Sie eine neue Beschreibung für diese Bezahlart ein:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="126"/>
        <location filename="dialogs/payedit.cpp" line="171"/>
        <location filename="dialogs/payedit.cpp" line="191"/>
        <location filename="dialogs/payedit.cpp" line="231"/>
        <location filename="dialogs/payedit.cpp" line="235"/>
        <location filename="dialogs/payedit.cpp" line="281"/>
        <location filename="dialogs/payedit.cpp" line="302"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="126"/>
        <location filename="dialogs/payedit.cpp" line="171"/>
        <location filename="dialogs/payedit.cpp" line="281"/>
        <source>Could not store the changes: %1</source>
        <translation>Änderungen können nicht gespeichert werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="144"/>
        <source>Change Payment Data</source>
        <translation>Bezahldaten ändern</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="150"/>
        <location filename="dialogs/payedit.cpp" line="255"/>
        <source>Data Name (human readable):</source>
        <translation>Daten-Name (für Menschen lesbar):</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="152"/>
        <location filename="dialogs/payedit.cpp" line="256"/>
        <source>Data Default (pattern):</source>
        <translation>Standarddaten (Muster):</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="154"/>
        <source>Hint: %Y=year, %M=month, %D=day, %%=%-sign, %O=order ID, %U=user</source>
        <translation>Hinweis: %Y=Jahr, %M=Monat, %D=Tag, %%=%-Zeichen, %O=Bestellnr., %U=Nutzername</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="160"/>
        <location filename="dialogs/payedit.cpp" line="221"/>
        <location filename="dialogs/payedit.cpp" line="266"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="162"/>
        <location filename="dialogs/payedit.cpp" line="223"/>
        <location filename="dialogs/payedit.cpp" line="268"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="187"/>
        <source>Edit Flags of payment option &apos;%1&apos;.</source>
        <translation>Flags der Bezahloption &apos;%1&apos; ändern.</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="191"/>
        <source>Could not store the changes.</source>
        <translation>Konnte Änderungen nicht speichern.</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="203"/>
        <source>Set Default Payment Type</source>
        <translation>Standardbezahlart setzen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="231"/>
        <source>The payment type &apos;%1&apos; has flags set, it may not be usable for every user, please consider removing those flags.</source>
        <translation>Der Bezahltyp &apos;%1&apos; besitzt Flags - unter Umständen ist er deshalb nicht für alle Nutzer verfügbar.</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="235"/>
        <source>Unable to set the new default: %1</source>
        <translation>Der neue Standard kann nicht gesetzt werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="246"/>
        <source>Create new Payment Option</source>
        <translation>Neue Bezahlart erstellen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="252"/>
        <source>Payment Type Name:</source>
        <translation>Name der Bezahlart:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="253"/>
        <source>[a-zA-Z-/_\(\),\.]{1,63}</source>
        <comment>payment type pattern - allow national chars!</comment>
        <translation>[a-zA-ZäöüÄÖÜß-/_\(\),\.]{1,63}</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="254"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="257"/>
        <source>Hint: %Y=year, %M=month, %D=day, %H=hour(0..23), %h=hour(1..12), %m=minute, %a=AM/PM, %%=%-sign</source>
        <translation>Hinweis: %Y=Jahr, %M=Monat, %D=Tag, %H=Stunde (0-23), %h=Stunde (1-12), %m=Minute, %a=AM/PM, %%=%-Zeichen</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="259"/>
        <source>Flags:</source>
        <translation>Flags:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="260"/>
        <source>Edit flags of the new payment option:</source>
        <translation>Flags der neuen Bezahloption ändern:</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="297"/>
        <source>Really Delete?</source>
        <translation>Wirklich löschen?</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="297"/>
        <source>Really delete payment option &apos;%1&apos;?</source>
        <translation>Wirklich die Bezahlart &apos;%1&apos; löschen?</translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="302"/>
        <source>Unable to delete this option: %1</source>
        <translation>Option kann nicht gelöscht werden: %1</translation>
    </message>
</context>
<context>
    <name>MPriceCategoryDialog</name>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="33"/>
        <source>Select a Price Category</source>
        <translation>Preiskategorie wählen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="47"/>
        <source>New...</source>
        <comment>new price category</comment>
        <translation>Neu...</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="50"/>
        <source>Edit...</source>
        <comment>edit price category</comment>
        <translation>Ändern...</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="54"/>
        <source>Select</source>
        <comment>select price category</comment>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="56"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="59"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="81"/>
        <location filename="dialogs/pricecatdlg.cpp" line="106"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="81"/>
        <location filename="dialogs/pricecatdlg.cpp" line="106"/>
        <source>Error while creating new price category: %1</source>
        <translation>Fehler beim Anlegen der Preiskategorie: %1</translation>
    </message>
</context>
<context>
    <name>MRoleTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="618"/>
        <source>New Role...</source>
        <translation>Neue Rolle...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="621"/>
        <source>Delete Role...</source>
        <translation>Rolle löschen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="625"/>
        <source>Change Description...</source>
        <translation>Beschreibung ändern...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="628"/>
        <source>Edit Flags...</source>
        <translation>Flags ändern...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="631"/>
        <source>Edit Rights...</source>
        <translation>Rechte ändern...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="653"/>
        <source>Role Name</source>
        <translation>Rollenname</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="653"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="667"/>
        <source>Create New Role</source>
        <translation>Neue Rolle anlegen</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="667"/>
        <source>Please enter a role name:</source>
        <translation>Bitte geben Sie einen Rollennamen ein:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="674"/>
        <location filename="mwin/acltabs.cpp" line="692"/>
        <location filename="mwin/acltabs.cpp" line="723"/>
        <location filename="mwin/acltabs.cpp" line="728"/>
        <location filename="mwin/acltabs.cpp" line="748"/>
        <location filename="mwin/acltabs.cpp" line="760"/>
        <location filename="mwin/acltabs.cpp" line="765"/>
        <location filename="mwin/acltabs.cpp" line="791"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="674"/>
        <source>Error while trying to create role: %1</source>
        <translation>Fehler beim Anlegen der Rolle: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="688"/>
        <source>Delete this Role?</source>
        <translation>Diese Rolle löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="688"/>
        <source>Really delete role &apos;%1&apos;?</source>
        <translation>Wollen Sie wirklich die Rolle &apos;%1&apos; löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="692"/>
        <source>Error while trying to delete role: %1</source>
        <translation>Fehler beim Löschen der Rolle: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="707"/>
        <source>Edit Description</source>
        <translation>Beschreibung ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="707"/>
        <source>Description of role %1:</source>
        <translation>Beschreibung der Rolle %1:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="723"/>
        <location filename="mwin/acltabs.cpp" line="760"/>
        <source>Cannot retrieve role: %1</source>
        <translation>Kann Rolle nicht herunterladen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="728"/>
        <source>Cannot retrieve flag list: %1</source>
        <translation>Kann Liste der Flags nicht herunterladen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="739"/>
        <source>Edit flags of role %1</source>
        <translation>Flags der Rolle %1 ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="748"/>
        <source>Error while setting flags: %1</source>
        <translation>Fehler beim Setzen der Flags: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="765"/>
        <source>Cannot retrieve right list: %1</source>
        <translation>Kann Liste der Rechte nicht herunterladen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="782"/>
        <source>Edit rights of role %1</source>
        <translation>Rechter der Rolle %1 ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="791"/>
        <source>Error while setting rights: %1</source>
        <translation>Fehler beim Setzen der Rechte: %1</translation>
    </message>
</context>
<context>
    <name>MScriptEngine</name>
    <message>
        <source>Never show debugger</source>
        <translation type="vanished">Debugger nie zeigen.</translation>
    </message>
    <message>
        <source>Show on error</source>
        <translation type="vanished">Im Fehlerfall zeigen</translation>
    </message>
    <message>
        <source>Show immediately on script start</source>
        <translation type="vanished">Sofort zeigen</translation>
    </message>
</context>
<context>
    <name>MShippingChange</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1053"/>
        <source>Change Shipping Method</source>
        <translation>Versandoption ändern</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1060"/>
        <source>Method:</source>
        <translation>Option:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1062"/>
        <source>Price:</source>
        <translation>Preis:</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1070"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1072"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1079"/>
        <source>(None)</source>
        <comment>shipping method</comment>
        <translation>(Keine)</translation>
    </message>
</context>
<context>
    <name>MShippingEditor</name>
    <message>
        <location filename="dialogs/shipping.cpp" line="41"/>
        <source>Edit Shipping Options</source>
        <translation>Versandoptionen editieren</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="55"/>
        <source>Change Description</source>
        <translation>Beschreibung ändern</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="57"/>
        <source>Change Price</source>
        <translation>Preis ändern</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="59"/>
        <source>Change Flags</source>
        <translation>Flags ändern</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="62"/>
        <source>Add Option</source>
        <translation>Option hinzufügen</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="64"/>
        <source>Delete Option</source>
        <translation>Option löschen</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="71"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="73"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="167"/>
        <source>Could not store the data: %1</source>
        <translation>Daten können nicht gespeichert werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="182"/>
        <source>Really Delete?</source>
        <translation>Wirklich löschen?</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="182"/>
        <source>Really delete shipping option &apos;%1&apos;?</source>
        <translation>Wirklich Versandoption &apos;%1&apos; löschen?</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="188"/>
        <source>Unable to delete this option: %1</source>
        <translation>Option kann nicht gelöscht werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="100"/>
        <location filename="dialogs/shipping.cpp" line="156"/>
        <source>Shipping Option Description</source>
        <translation>Versandoptionsbeschreibung</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="100"/>
        <source>Please select a new description for this shipping option:</source>
        <translation>Bitte geben Sie eine Beschreibung für diese Versandoption ein:</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="105"/>
        <location filename="dialogs/shipping.cpp" line="127"/>
        <location filename="dialogs/shipping.cpp" line="146"/>
        <location filename="dialogs/shipping.cpp" line="167"/>
        <location filename="dialogs/shipping.cpp" line="188"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="105"/>
        <location filename="dialogs/shipping.cpp" line="127"/>
        <source>Could not store the changes: %1</source>
        <translation>Änderungen können nicht gespeichert werden: %1</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="142"/>
        <source>Edit Flags of shipping option &apos;%1&apos;.</source>
        <translation>Flags der Versandoption &apos;%1&apos; ändern.</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="146"/>
        <source>Could not store the changes.</source>
        <translation>Konnte Änderungen nicht speichern.</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="122"/>
        <location filename="dialogs/shipping.cpp" line="159"/>
        <source>Shipping Option Price</source>
        <translation>Versandoptionspreis</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Flags</source>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="122"/>
        <source>Please select a new price for this shipping option:</source>
        <translation>Bitte geben Sie einen Preis für diese Versandoption ein:</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="156"/>
        <source>Please select a new description for this new shipping option:</source>
        <translation>Bitte geben Sie eine Beschreibung für diese Versandoption ein:</translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="159"/>
        <source>Please select a new price for this new shipping option:</source>
        <translation>Bitte geben Sie einen Preis für diese Versandoption ein:</translation>
    </message>
</context>
<context>
    <name>MTELabelDelegate</name>
    <message>
        <location filename="templates/ticketedit.cpp" line="610"/>
        <source>edged</source>
        <translation>eckig</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="611"/>
        <source>smooth</source>
        <translation>glatt</translation>
    </message>
</context>
<context>
    <name>MTemplateChoice</name>
    <message>
        <location filename="templates/templatedlg.cpp" line="34"/>
        <source>Chose Template</source>
        <translation>Vorlage auswählen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="38"/>
        <source>Please chose a variant of template %1:</source>
        <translation>Bitte wählen Sie eine Variante für die Vorlage %1:</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="49"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MTemplateEditor</name>
    <message>
        <location filename="templates/templatedlg.cpp" line="77"/>
        <source>Edit Template Directory</source>
        <translation>Vorlagenverzeichnis editieren</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="93"/>
        <source>&amp;Add Variant</source>
        <oldsource>Delete Variant</oldsource>
        <translation>Variante hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="101"/>
        <source>&amp;Save Template...</source>
        <oldsource>Edit Template</oldsource>
        <translation>Vorlage speichern</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="90"/>
        <source>&amp;Update Now</source>
        <translation>Jetzt auffrischen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="95"/>
        <source>&amp;Delete Variant</source>
        <translation>Variante löschen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="98"/>
        <source>Change &amp;Flags</source>
        <translation>Flags ändern</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="103"/>
        <source>&amp;Edit Template</source>
        <translation>Template ändern</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="110"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Template/Variant</source>
        <translation>Vorlage/Variante</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Checksum</source>
        <translation>Checksumme</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Flags</source>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="188"/>
        <location filename="templates/templatedlg.cpp" line="208"/>
        <location filename="templates/templatedlg.cpp" line="227"/>
        <location filename="templates/templatedlg.cpp" line="244"/>
        <location filename="templates/templatedlg.cpp" line="289"/>
        <location filename="templates/templatedlg.cpp" line="309"/>
        <location filename="templates/templatedlg.cpp" line="347"/>
        <location filename="templates/templatedlg.cpp" line="356"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="188"/>
        <source>Unable to delete this template.</source>
        <translation>Kann diese Vorlage nicht löschen.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="202"/>
        <source>Select Template File</source>
        <translation>Vorlagendatei wählen</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="208"/>
        <source>Files with this extension (%1) are not legal for this template.</source>
        <translation>Dateien mit der Erweiterung %1 sind für diese Vorlage nicht erlaubt.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="227"/>
        <source>Unable to upload file.</source>
        <translation>Kann Datei nicht hochladen.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="244"/>
        <source>Unable to send new description to server.</source>
        <translation>Kann die neue Beschreibung nicht speichern.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="289"/>
        <source>Unknown template type, cannot edit it.</source>
        <translation>Unbekannter Vorlagentyp. Kann Editor nicht starten.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="309"/>
        <source>Ooops. Lost the template file, cannot store it.</source>
        <translation>Ups. Vorlagendatei verloren, kann sie nicht speichern.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="315"/>
        <source>Save template as...</source>
        <translation>Vorlage speichern unter...</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="328"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="328"/>
        <source>Unable to save the template file.</source>
        <translation>Kann Vorlage nicht speichern.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="347"/>
        <source>Ooops. Lost the template file, cannot alter it.</source>
        <translation>Ups! Die Template-Datei ist verloren gegange, kann sie nicht ändern.</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="351"/>
        <source>Edit flags of template &apos;%1&apos;</source>
        <translation>Flags des Template &apos;%1&apos; ändern</translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="356"/>
        <source>Unable to send updated flags to server.</source>
        <translation>Die geänderten Flags können nicht zum Server gesendet werden.</translation>
    </message>
</context>
<context>
    <name>MTicketEditor</name>
    <message>
        <location filename="templates/ticketedit.cpp" line="154"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="155"/>
        <source>&amp;Open File...</source>
        <translation>Datei öffnen...</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="156"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="157"/>
        <source>Save &amp;as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="159"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="160"/>
        <source>&amp;Edit</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="207"/>
        <source>Add Item</source>
        <translation>Zeile hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="209"/>
        <source>Remove Item</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="224"/>
        <source>Add File</source>
        <translation>Datei hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="226"/>
        <source>Remove File</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="151"/>
        <source>Label Editor</source>
        <translation>Aufkleber-Editor</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="155"/>
        <source>Ctrl+O</source>
        <comment>open file shortcut</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="156"/>
        <source>Ctrl+S</source>
        <comment>save file shortcut</comment>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="161"/>
        <source>&amp;Add Item</source>
        <translation>Element hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="162"/>
        <source>&amp;Remove Item</source>
        <translation>Element entfernen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="164"/>
        <source>Add &amp;File</source>
        <translation>Datei hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="165"/>
        <source>Remove F&amp;ile</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="167"/>
        <source>Add &amp;Text Item</source>
        <translation>Textzeile hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="168"/>
        <source>Add &amp;Picture Item</source>
        <translation>Bild hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="169"/>
        <source>Add &amp;Barcode Item</source>
        <translation>Barcode hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="170"/>
        <source>Add &amp;Load Font Item</source>
        <translation>Schriftladezeile hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="184"/>
        <source>Label Size:</source>
        <translation>Aufklebergröße:</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="189"/>
        <source>Millimeter</source>
        <translation>Millimeter</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="190"/>
        <source>Inch</source>
        <translation>Zoll</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="202"/>
        <source>Move up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="204"/>
        <source>Move down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="231"/>
        <source>As Label</source>
        <translation>Als Aufkleber</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="234"/>
        <source>Zoom:</source>
        <translation>Zoom:</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="240"/>
        <source>Refresh</source>
        <translation>Auffrischen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="248"/>
        <source>Example Data</source>
        <translation>Beispieldaten</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="276"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="276"/>
        <source>Content</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="296"/>
        <location filename="templates/ticketedit.cpp" line="301"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="296"/>
        <source>Unable to open file &apos;%1&apos; for reading.</source>
        <translation>Kann Datei &apos;%1&apos; nicht zum lesen öffnen.</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="301"/>
        <source>Unable to interpret file &apos;%1&apos;.</source>
        <translation>Kann Datei &apos;%1&apos; nicht interpretieren.</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="350"/>
        <source>top</source>
        <translation>oben</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="352"/>
        <source>bottom</source>
        <translation>unten</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="355"/>
        <source>center</source>
        <translation>zentriert</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="357"/>
        <source>left</source>
        <translation>links</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="359"/>
        <source>right</source>
        <translation>rechts</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="361"/>
        <source>align (%1)</source>
        <translation>Ausrichtung (%1)</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="388"/>
        <location filename="templates/ticketedit.cpp" line="446"/>
        <location filename="templates/ticketedit.cpp" line="820"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="388"/>
        <source>Unable to interpret template data.</source>
        <translation>Kann Vorlagendaten nicht interpretieren.</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="427"/>
        <source>Open Ticket Template</source>
        <translation>Eintrittskartenvorlage öffnen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="446"/>
        <source>Unable to write to file %1</source>
        <translation>Kann Datei %1 nicht schreiben</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="454"/>
        <source>Save Ticket Template</source>
        <translation>Eintrittskartenvorlage speichern</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="465"/>
        <source>Label Template Editor</source>
        <translation>Aufklebervorlageneditor</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="467"/>
        <source>Label Template Editor [%1]</source>
        <translation>Aufklebervorlageneditor [%1]</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="479"/>
        <location filename="templates/ticketedit.cpp" line="827"/>
        <source>File Name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="479"/>
        <location filename="templates/ticketedit.cpp" line="492"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="490"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="491"/>
        <source>Offset</source>
        <translation>Abstand</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="493"/>
        <source>File/Font</source>
        <translation>Datei/Schrift</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="494"/>
        <source>Font Size</source>
        <translation>Schriftgröße</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="495"/>
        <source>Scaling</source>
        <translation>Skalierung</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="496"/>
        <source>Horiz. Alignment</source>
        <translation>horiz. Ausrichtung</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="497"/>
        <source>Vert. Alignment</source>
        <translation>vert. Ausrichtung</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="498"/>
        <source>Text Data</source>
        <translation>Textdaten</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="504"/>
        <source>Load Font File</source>
        <translation>Schriftdatei laden</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="505"/>
        <source>Show Picture</source>
        <translation>Bild anzeigen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="506"/>
        <source>Show Text Line</source>
        <translation>Textzeile anzeigen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="507"/>
        <source>Show Barcode</source>
        <translation>Barcode anzeigen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="508"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="523"/>
        <source>smooth</source>
        <translation>glatt</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="523"/>
        <source>edged</source>
        <translation>eckig</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="814"/>
        <source>Add File to Label</source>
        <translation>Datei zu Aufkleber hinzufügen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="820"/>
        <source>Unable to read file %1</source>
        <translation>Kann Datei %1 nicht lesen</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="827"/>
        <source>Please enter the internal file name:</source>
        <translation>Bitte geben Sie den internen Dateinamen ein:</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="844"/>
        <source>Really delete?</source>
        <translation>Wirklich löschen?</translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="844"/>
        <source>Really remove file &apos;%1&apos; from the label?</source>
        <translation>Wirklich Datei &apos;%1&apos; aus dem Aufkleber löschen?</translation>
    </message>
</context>
<context>
    <name>MUserAuditDialog</name>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="415"/>
        <source>User Audit: %1</source>
        <translation>Nutzerlog: %1</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="427"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="432"/>
        <source>Orders</source>
        <translation>Bestellungen</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Order ID</source>
        <translation>Bestell-Nr.</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="449"/>
        <location filename="dialogs/orderauditdlg.cpp" line="474"/>
        <location filename="dialogs/orderauditdlg.cpp" line="502"/>
        <source>Show Order</source>
        <translation>Bestellung anzeigen</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="451"/>
        <location filename="dialogs/orderauditdlg.cpp" line="476"/>
        <location filename="dialogs/orderauditdlg.cpp" line="504"/>
        <source>Audit Order</source>
        <translation>Bestellungslog</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="456"/>
        <source>Tickets</source>
        <translation>Karten</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <source>Ticket</source>
        <translation>Eintrittskarte</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="479"/>
        <source>Audit Ticket</source>
        <translation>Eintrittskarten-Log</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="484"/>
        <source>Vouchers</source>
        <translation>Gutscheine</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Voucher</source>
        <translation>Gutschein</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="507"/>
        <source>Audit Voucher</source>
        <translation>Gutschein-Log</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="524"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="524"/>
        <source>Unable to retrieve order: %1</source>
        <translation>Die Bestellung kann nicht geholt werden: %1</translation>
    </message>
</context>
<context>
    <name>MUserTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="77"/>
        <source>New User...</source>
        <translation>Neuer Nutzer...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="80"/>
        <source>Delete User...</source>
        <translation>Nutzer löschen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="84"/>
        <source>Description...</source>
        <translation>Beschreibung...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="87"/>
        <source>Hosts...</source>
        <translation>Hosts...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="90"/>
        <source>Roles...</source>
        <translation>Rollen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="93"/>
        <source>Flags...</source>
        <translation>Flags...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="96"/>
        <source>Set Password...</source>
        <translation>Passwort setzen...</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="122"/>
        <source>Login Name</source>
        <translation>Loginname</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="122"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="136"/>
        <source>New User</source>
        <translation>Neuer Nutzer</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="136"/>
        <source>Please enter new user name (only letters, digits, and underscore allowed):</source>
        <translation>Neuen Nutzernamen eingeben (Kleinbuchstaben, Ziffern, Unterstrich, Minus):</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="141"/>
        <location filename="mwin/acltabs.cpp" line="172"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="141"/>
        <source>The user name must contain only letters, digits, dots and underscores and must be at least one character long!</source>
        <translation>Nutzernamen dürfen nur Kleinbuchstaben, Ziffern, Punkte, Bindestriche und Unterstriche enthalten und müssen mindestens ein Zeichen lang sein!</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="145"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="145"/>
        <source>Please enter an initial password for the user:</source>
        <translation>Bitte geben Sie ein intiales Passwort ein:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="160"/>
        <source>Delete User?</source>
        <translation>Nutzer löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="160"/>
        <source>Really delete user &apos;%1&apos;?</source>
        <translation>Nutzer &apos;%1&apos; wirklich löschen?</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="164"/>
        <source>(Nobody)</source>
        <comment>this is a username for no user, the string must contain &apos;(&apos; to distinguish it from the others</comment>
        <translation>(Niemand)</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="167"/>
        <source>Delete User</source>
        <translation>Nutzer Löschen</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="167"/>
        <source>Select which user will inherit this users database objects:</source>
        <translation>Bitte wählen Sie einen Nutzer, der die Datenbankobjekte des gelöschten Nutzers erbt:</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="172"/>
        <source>Cannot delete user: %1</source>
        <translation>Kann Nutzer nicht löschen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="186"/>
        <source>Edit Description</source>
        <translation>Beschreibung ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="186"/>
        <source>Description of user %1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="203"/>
        <location filename="mwin/acltabs.cpp" line="208"/>
        <location filename="mwin/acltabs.cpp" line="228"/>
        <location filename="mwin/acltabs.cpp" line="241"/>
        <location filename="mwin/acltabs.cpp" line="246"/>
        <location filename="mwin/acltabs.cpp" line="265"/>
        <location filename="mwin/acltabs.cpp" line="280"/>
        <location filename="mwin/acltabs.cpp" line="286"/>
        <location filename="mwin/acltabs.cpp" line="300"/>
        <location filename="mwin/acltabs.cpp" line="305"/>
        <location filename="mwin/acltabs.cpp" line="325"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="203"/>
        <source>Cannot retrieve user roles: %1</source>
        <translation>Kann Nutzerrollen nicht abfragen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="208"/>
        <source>Cannot retrieve role descriptions: %1</source>
        <translation>Kann Rollenbeschreibung nicht abfragen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="219"/>
        <source>Edit Roles of user %1</source>
        <translation>Rollen von Nutzer %1 ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="228"/>
        <source>Error while setting users roles: %1</source>
        <translation>Fehler beim Setzen der Nutzerrollen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="241"/>
        <source>Cannot retrieve users hosts: %1</source>
        <translation>Kann die Hosts des Nutzers nicht herunterladen: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="246"/>
        <source>Cannot retrieve host descriptions: %1</source>
        <translation>Kann die Beschreibung des Hosts nicht laden: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="256"/>
        <source>Edit hosts of user %1</source>
        <translation>Hosts von Nutzer %1 ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="265"/>
        <source>Error while setting users hosts: %1</source>
        <translation>Fehler beim Setzen der Nutzerhosts: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="280"/>
        <source>The password must be non-empty and both lines must match</source>
        <translation>Das Passwort darf nicht leer sein und beide Zeilen müssen übereinstimmen.</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="286"/>
        <source>Error while setting password: %1</source>
        <translation>Fehler beim Setzen des Passwortes: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="300"/>
        <source>Cannot retrieve user data: %1</source>
        <translation>Kann Nutzerdaten nicht laden: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="305"/>
        <source>Cannot retrieve flag list: %1</source>
        <translation>Kann Liste der Flags nicht laden: %1</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="316"/>
        <source>Edit flags of user %1</source>
        <translation>Flags des Nutzers %1 ändern</translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="325"/>
        <source>Error while setting users flags: %1</source>
        <translation>Fehler beim Setzen der Nutzerflags: %1</translation>
    </message>
</context>
<context>
    <name>MWizard</name>
    <message>
        <source>Wizard</source>
        <translation type="vanished">Assistent</translation>
    </message>
</context>
</TS>
