//
// C++ Implementation: main
//
// Description: Main Program
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#include "overview.h"

#include <scli.h>
#include <mapplication.h>
#include "msinterface.h"

#include "main.h"
#include <WTransaction>
#include "barcode-plugin.h"


int MagicSmokeMain::realmain(int argc,char**argv)
{
	//create global app
	MApplication app(argc,argv);
	
	//init
	app.initialize();
        //init plugins
        MBarcodeHub::instance()->initialize();

	//get session
	MSessionClient sc;
	sc.connect(&sc,SIGNAL(sessionLost()),&app,SLOT(quit()));
	sc.connect(&sc,SIGNAL(managerLost()),&app,SLOT(quit()));
	if(sc.waitForSessionAvailable()){
		WTransaction::setLogPrefix("Main-T");
		MSInterface*ms=new MSInterface(sc.currentProfileId());
		ms->loginSession(sc.currentUsername(), sc.currentSessionId());
		ms->initialize();
		MOverview *mo=new MOverview(sc.currentProfileId());
		mo->showRestored();
		QObject::connect(&sc,SIGNAL(sessionIdChanged(QString)),ms,SLOT(setSessionId(QString)));
	}else{
		qDebug()<<"Unable to get session. Giving up.";
		return 1;
	}

        //open main window
	//MLogin mmw;
	//mmw.show();
	
	return app.exec();
}
