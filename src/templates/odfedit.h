//
// C++ Interface: ODF template editor
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2012
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ODFEDIT_H
#define MAGICSMOKE_ODFEDIT_H

#include <QMainWindow>
#include <QMap>
#include <QItemDelegate>

#include <DPtrBase>

class MOEvent;
class QFile;
class QIODevice;
class QDomNode;
/**An editor for ticket templates.*/
class MOdfEditor:public QMainWindow
{
	Q_OBJECT
	DECLARE_DPTR(d);
	public:
		///instantiates the editor
		MOdfEditor(QWidget* parent = 0, Qt::WindowFlags f = 0);
		///clean it up
		virtual ~MOdfEditor();
		
	public slots:
		///loads a template file, this is a helper for openFile and download
		void loadFile(QString,bool isTemplate=true);
		///shows a file open dialog and then opens the selected file
		void openFile(bool isTemplate=true);
		///shows a file open dialog and then opens the selected file
		void openFile2();
		///saves the file - if it is local, otherwise calls saveFileAs
		void saveFile();
		///asks for a new file name
		void saveFileAs();
		///helper for saveFile and upload
		void saveFile(QString);

	private slots:
		///parses the XML part of the template file and fills internal ressources
		void parseTemplate(QByteArray,bool isTemplate=true);
		///pushes all data from internal ressources to the display
		void updateDisplay();
		///saves all data to the file (must be open ReadWrite)
		void saveTemplate(QFile&);
		///helper: saves the XML part of the template (device must be writable)
		void saveXmlPart(QIODevice&);
		///helper: react on selection changes in the DOM tree
		void selectionChange();
		///helper: remember that an editor element changed
		void setChanged(bool ch=true);
		///helper: insert calc into current
		void insCalcIntoCurrent();
		///helper: insert calc after current
		void insCalcBehindCurrent();
		///helper: insert calc into current
		void insCommentIntoCurrent();
		///helper: insert calc after current
		void insCommentBehindCurrent();
		///helper: insert else after current
		void insElse();
		///helper: delete an item
		void delItem();
		///helper: wrap marked items in a condition
		void wrapInCond(QString tag="if");
		///helper: wrap marked items in a loop
		void wrapInLoop();
		///helper: unwrap the loop or condition
		void unwrapItem();
                ///helper: copy arbitrary item to clipboard
                void copyItem();
                ///helper: insert clipboard item into current
                void pasteItemInto();
                ///helper: insert clipboard item after current
                void pasteItemBehind();
		
		///helper: test new template on an order
		void testOrder();
		///helper: test new template on an event summary
		void testEventSum();
		///helper: save into the test file
		void testSave();
	signals:
		///used to switch to the correct editor widget
		void switchStack(int);
		///used to select an event
		void getEvent(int&,bool&);
	private:
		///helper to save the current node when selection changes
		void saveCurrentNode();
		///helper to display a tag
		void displayTag();
		///helper to determine template type
		/// \returns correct file extension (.odtt, .odst, ...) or empty string if the type cannot be determined
		QString detectTemplateType()const;
		///cleans up the test file
		void cleanTestFile();
};

#endif
