//
// C++ Interface: ticket template editor
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TICKETEDIT_H
#define MAGICSMOKE_TICKETEDIT_H

#include <QMainWindow>
#include <QMap>
#include <QItemDelegate>

#include <DPtrBase>

class QFile;
class QIODevice;
/**An editor for ticket templates.*/
class MTicketEditor:public QMainWindow
{
	Q_OBJECT
	DECLARE_DPTR(d);
	friend class MTELabelDelegate;
	public:
		///instantiates the editor
		MTicketEditor(QWidget* parent = 0, Qt::WindowFlags f = 0);
		
	public slots:
		///loads a template file, this is a helper for openFile and download
		void loadFile(QString);
		///shows a file open dialog and then opens the selected file
		void openFile();
		///saves the file - if it is local, otherwise calls saveFileAs
		void saveFile();
		///asks for a new file name
		void saveFileAs();
		///helper for saveFile and upload
		void saveFile(QString);
	private slots:
		///renders the label
		void rerender();
		///parses the XML part of the template file and fills internal ressources
		void parseTemplate(QByteArray);
		///pushes all data from internal ressources to the display
		void updateDisplay();
		///add a specific kind of item line
		void addItem(int);
		///delete currently selected item line
		void delItem();
		///move selected item up
		void upItem();
		///move selected item down
		void downItem();
		///add a file (image/font) to the template
		void addFile();
		///delete the selected file from the template
		void delFile();
		///used by constructor to load example data for the example label
		void loadExampleData();
		///saves all data to the file (must be open ReadWrite)
		void saveTemplate(QFile&);
		///helper: saves the XML part of the template (device must be writable)
		void saveXmlPart(QIODevice&);
};

/// \internal helper for the ticket editor
class MTELabelDelegate:public QItemDelegate
{
	Q_OBJECT
	MTicketEditor*mParent;
	public:
		MTELabelDelegate(MTicketEditor *parent):QItemDelegate(parent) 
		{mParent=parent;}
		QWidget *createEditor(QWidget *, const QStyleOptionViewItem &,
		        const QModelIndex &) const;
		void setEditorData(QWidget *, const QModelIndex &) const;
		void setModelData(QWidget *, QAbstractItemModel *,
		        const QModelIndex &) const ;
		void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, 
			  const QModelIndex &) const {editor->setGeometry(option.rect);}
};

#endif
