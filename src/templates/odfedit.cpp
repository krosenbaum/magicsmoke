//
// C++ Implementation: ticket template editor
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "odfedit.h"
#include "centbox.h"
#include "misc.h"
#include "odtrender.h"
#include "dommodel.h"
#include "cleanup.h"
#include "domiterator.h"

#include "orderwin.h"
#include "eventsummary.h"

#include "MOOrder"
#include "msinterface.h"

#include <DPtr>

#include <QAction>
#include <QBoxLayout>
#include <QBuffer>
#include <QComboBox>
#include <QCoreApplication>
#include <QDateTimeEdit>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <QHeaderView>
#include <QInputDialog>
#include <QItemDelegate>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPointer>
#include <QPointF>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QSettings>
#include <QSignalMapper>
#include <QSizeF>
#include <QSplitter>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QStatusBar>
#include <QTableView>
#include <QTemporaryFile>
#include <QTextEdit>
#include <QTreeView>
#include <Unzip>
#include <Zip>
#include <MTGetOrder>

#include <functional>


class DPTR_CLASS_NAME(MOdfEditor)
{
	public:
		//template file
		QString mFileName;
		//content.xml
		QDomDocument mContent;
		//widgets
		QTreeView*mDomTree;
		MDomItemModel*mDomModel;
		void expandTree(const QModelIndex&);
		//stack indexes
		int st_none,st_tag,st_text,st_loop,st_calc,st_comment,st_cond,st_special;
		//stack widgets
		QLineEdit*mLoopVar,*mCalcExpr,*mTagName,*mCondExpr;
		QTextEdit*mText,*mComment;
		QTableView*mTagAttrs;
		QStandardItemModel*mTagAttrModel;
		QStackedWidget*mStack;
		QLabel*mSpecial;
		//tree menu
		QAction*maAddIntoCalc,*maWrapInCond,*maAddIntoComment;
		QAction*maInsBehindElse,*maWrapInLoop;
		QAction*maInsBehindCalc,*maInsBehindComment,*maUnwrap;
                QAction*maInsItemInto,*maInsItemBehind,*maCpyItem,*maDelItem;
		void setActions(const QList<QAction*>&,bool haveselect=true);
		//current node
		QModelIndex mCurIndex;
		QDomNode mCurNode,mClipNode;
		bool mChanged;
		//test printing
		QString mTestFile;
		QPointer<MOrderWindow>mTestOrderWin;
		QPointer<MEventSummary>mTestEventWin;
		//file contents
		struct File{
			QString name;
			enum class Type{None,Directory,FlatFile};
			Type type;
			QByteArray content;
			File(){type=Type::None;}
			File(QString n,Type t,const QByteArray&c):name(n),type(t),content(c){}
		};
		QList<File> mFiles;
		Private():mChanged(false)
		{
			st_none=st_tag=st_text=st_loop=st_calc=st_comment=st_cond=st_special=-1;
		}
};

DEFINE_DPTR(MOdfEditor);

static QVariant displayColorHelper(const QDomNode& node)
{
	static const QColor textbg("#c0c0ff"),commentbg("#c0c0c0"),specialbg("#ffc0c0");
	switch(node.nodeType()){
		case QDomNode::CommentNode:return commentbg;
		case QDomNode::TextNode:
		case QDomNode::CDATASectionNode:
			return textbg;
		case QDomNode::ElementNode:
			if(node.nodeName().startsWith(OdfTemplatePrefix+":"))
				return specialbg;
			//fall through
		default: //use default color
			return QVariant();
	}
}


MOdfEditor::MOdfEditor(QWidget* parent, Qt::WindowFlags f): QMainWindow(parent, f)
{
	setWindowTitle(tr("ODF Template Editor"));
	//menu
	QMenuBar*mb=menuBar();
	QMenu*m=mb->addMenu(tr("&File"));
	m->addAction(tr("&Open Template File..."), this, SLOT(openFile()), tr("Ctrl+O","open file shortcut"));
	m->addAction(tr("&Import ODF File..."), this, SLOT(openFile2()), tr("Ctrl+Shift+O","import ODF file shortcut"));
	m->addAction(tr("&Save"), this, SLOT(saveFile()), tr("Ctrl+S","save file shortcut"));
	m->addAction(tr("Save &as..."),this,SLOT(saveFileAs()));
	m->addSeparator();
	m->addAction(tr("&Close"),this,SLOT(close()));
	
	QMenu*medit=m=mb->addMenu(tr("&Edit"));
	d->maAddIntoCalc=m->addAction(tr("Insert &Calculation into current"),this,SLOT(insCalcIntoCurrent()));
	d->maInsBehindCalc=m->addAction(tr("Insert Calculation behind current"),this,SLOT(insCalcBehindCurrent()));
	d->maWrapInCond=m->addAction(tr("&Wrap in Condition"),this,SLOT(wrapInCond()));
	d->maWrapInLoop=m->addAction(tr("Wrap in &Loop"),this,SLOT(wrapInLoop()));
	d->maInsBehindElse=m->addAction(tr("Insert &Else behind current"),this,SLOT(insElse()));
	d->maAddIntoComment=m->addAction(tr("Insert Comment into current"),this,SLOT(insCommentIntoCurrent()));
	d->maInsBehindComment=m->addAction(tr("Insert Comment behind current"),this,SLOT(insCommentBehindCurrent()));
        m->addSeparator();
        d->maCpyItem=m->addAction(tr("Copy current item to clipboard"),this,SLOT(copyItem()));
        d->maInsItemInto=m->addAction(tr("Insert clipboard into current"),this,SLOT(pasteItemInto()));
        d->maInsItemBehind=m->addAction(tr("Insert clipboard behind current"),this,SLOT(pasteItemBehind()));
	m->addSeparator();
	d->maUnwrap=m->addAction(tr("Unwrap Loop/Condition"),this,SLOT(unwrapItem()));
	d->maDelItem=m->addAction(tr("&Remove Item"),this,SLOT(delItem()));
	
	m=mb->addMenu(tr("&Test"));
	m->addAction(tr("Test with &Order..."),this,SLOT(testOrder()));
	m->addAction(tr("Test with Event &Summary..."),this,SLOT(testEventSum()));
	
	//central
	QSplitter*central=new QSplitter(Qt::Vertical);
	setCentralWidget(central);
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	central->setStretchFactor(0,1);
	central->setStretchFactor(1,5);
	central->setOrientation(Qt::Horizontal);
	
	//DOM tree
	central->addWidget(d->mDomTree=new QTreeView);
	d->mDomTree->setModel(d->mDomModel=new MDomItemModel(this));
	d->mDomTree->setEditTriggers(QAbstractItemView::NoEditTriggers);
	d->mDomModel->setHeaderData(0,Qt::Horizontal,tr("Document XML Tree"),Qt::DisplayRole);
	d->mDomModel->showAllNodeTypes();
	d->mDomModel->showTypeLabel(false);
	d->mDomModel->setContentFromNodeCall(Qt::BackgroundColorRole,displayColorHelper);
	d->mDomTree->setSelectionMode(QAbstractItemView::ContiguousSelection);
	d->mDomTree->setSelectionBehavior(QAbstractItemView::SelectRows);
	d->mDomTree->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	d->mDomTree->header()->setSectionResizeMode(0,QHeaderView::ResizeToContents);
	d->mDomTree->header()->setStretchLastSection(false);
	connect(d->mDomTree->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this,SLOT(selectionChange()));
	auto ml=[=](QPoint p){
		QModelIndex idx=d->mDomTree->indexAt(p);
		if(idx.isValid())
			medit->popup(d->mDomTree->mapToGlobal(p));
	};
	connect(d->mDomTree,&QTreeView::customContextMenuRequested,ml);
	d->mDomTree->setContextMenuPolicy(Qt::CustomContextMenu);
	
	//the editors...
	QStackedWidget*stack;
	central->addWidget(stack=new QStackedWidget);
	d->mStack=stack;
	connect(this,SIGNAL(switchStack(int)),stack,SLOT(setCurrentIndex(int)));
	d->st_none=stack->addWidget(new QWidget);
	QWidget*w;
	//text node editor
	d->st_special=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Special Template Tag<h1>")));
	vl->addWidget(d->mSpecial=new QLabel("..."));
	vl->addStretch(1);
	//text node editor
	d->st_text=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Plain Text<h1>")));
	vl->addWidget(d->mText=new QTextEdit);
	connect(d->mText,SIGNAL(textChanged()),this,SLOT(setChanged()));
	//tag editor
	d->st_tag=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Tag</h1>")));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Tag Name:")));
	hl->addWidget(d->mTagName=new QLineEdit,1);
	connect(d->mTagName,SIGNAL(textEdited(QString)),this,SLOT(setChanged()));
	vl->addSpacing(15);
	vl->addWidget(new QLabel(tr("Attributes:")));
	vl->addWidget(d->mTagAttrs=new QTableView,1);
	d->mTagAttrs->setModel(d->mTagAttrModel=new QStandardItemModel);
	connect(d->mTagAttrModel,SIGNAL(dataChanged(QModelIndex,QModelIndex)), this,SLOT(setChanged()));
	connect(d->mTagAttrModel,SIGNAL(rowsRemoved(const QModelIndex&,int,int)), this,SLOT(setChanged()));
	connect(d->mTagAttrModel,SIGNAL(rowsInserted(const QModelIndex&,int,int)), this,SLOT(setChanged()));
	vl->addLayout(hl=new QHBoxLayout);
	QPushButton*p;
	std::function<void()> btn=[=](){d->mTagAttrModel->insertRow(d->mTagAttrModel->rowCount());};
	hl->addStretch(1);
	hl->addWidget(p=new QPushButton("Add"));
	connect(p,&QPushButton::clicked,btn);
	btn=[=](){
		QModelIndex idx=d->mTagAttrs->currentIndex();
		if(idx.isValid())
			d->mTagAttrModel->removeRow(idx.row(),idx.parent());
	};
	hl->addWidget(p=new QPushButton("Remove"));
	connect(p,&QPushButton::clicked,btn);
	//loop editor
	d->st_loop=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Loop</h1>")));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Loop Variable")));
	hl->addWidget(d->mLoopVar=new QLineEdit);
	connect(d->mLoopVar,SIGNAL(textEdited(QString)), this,SLOT(setChanged()));
	vl->addStretch(1);
	//calc editor
	d->st_calc=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Calculation</h1>")));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Expression")));
	hl->addWidget(d->mCalcExpr=new QLineEdit);
	connect(d->mCalcExpr,SIGNAL(textEdited(QString)), this,SLOT(setChanged()));
	vl->addStretch(1);
	//calc editor
	d->st_cond=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Condition</h1>")));
	vl->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Expression")));
	hl->addWidget(d->mCondExpr=new QLineEdit);
	connect(d->mCondExpr,SIGNAL(textEdited(QString)), this,SLOT(setChanged()));
	vl->addStretch(1);
	//comment editor
	d->st_comment=stack->addWidget(w=new QWidget);
	w->setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("<h1>Comment</h1>")));
	vl->addWidget(d->mComment=new QTextEdit,1);
	connect(d->mComment,SIGNAL(textChanged()),this,SLOT(setChanged()));
	//statusbar
	statusBar()->setSizeGripEnabled(true);
	setAttribute(Qt::WA_DeleteOnClose);
	updateDisplay();
}

MOdfEditor::~MOdfEditor()
{
	cleanTestFile();
}

void MOdfEditor::cleanTestFile()
{
	if(!d->mTestFile.isEmpty()){
		QFile(d->mTestFile).remove();
		d->mTestFile.clear();
	}
	if(d->mTestOrderWin)
		d->mTestOrderWin->deleteLater();
	if(d->mTestEventWin)
		d->mTestEventWin->deleteLater();
}

void MOdfEditor::setChanged(bool ch)
{
	if(ch!=d->mChanged)
// 	qDebug()<<"set changed state"<<ch;
	d->mChanged=ch;
}

void MOdfEditor::loadFile(QString fn,bool istempl)
{
	qDebug()<<"loading ODF template file"<<fn;
	cleanTestFile();
	//try to open the file
	if(fn=="")return;
	QFile fd(fn);
	if(!fd.open(QIODevice::ReadOnly)){
		QMessageBox::warning(this, tr("Error"), tr("Unable to open file '%1' for reading.").arg(fn));
		return;
	}
	Unzip unz;
	if(!unz.open(&fd)){
		QMessageBox::warning(this, tr("Error"), tr("Unable to interpret file '%1'. It is not an ODF container (PKZip format).").arg(fn));
		return;
	}
	//clear cache
	d->mFiles.clear();
	d->mFileName=fn;
	//read data
	unz.firstFile();
	do{
		QBuffer buf;buf.open(QIODevice::ReadWrite);
		ZipFileInfo inf=unz.currentFile(buf);
		if(inf.fileName()=="content.xml")
			parseTemplate(buf.data(),istempl);
		else
			d->mFiles<<Private::File(inf.fileName(), Private::File::Type::FlatFile, buf.data());
	}while(unz.nextFile());
	//update display
	updateDisplay();
}


void MOdfEditor::parseTemplate(QByteArray bytes,bool istempl)
{
	d->mContent.clear();
	d->mDomModel->clear();
	//convert V1->V2
	QString err;int errln,errcl;
	bool dov1=false;
	const QString tpename=OdfTemplatePrefix+":template";
	if(!d->mContent.setContent(bytes,false,&err,&errln,&errcl)){
		qDebug()<<"Hmm, not XML, trying version 1 converter...";
		qDebug()<<" Info: line ="<<errln<<"column ="<<errcl<<"error ="<<err;
		dov1=true;
	}else{
		//check for template element
		QDomElement de=d->mContent.documentElement();
		if(de.isNull()||de.tagName()!=tpename){
			qDebug()<<"Template is not v2, trying to convert...";
			dov1=true;
			d->mContent.clear();
		}
	}
	//conversion process
	if(!dov1){
		d->mDomModel->setDomDocument(d->mContent);
		d->mDomTree->expandAll();
		qDebug()<<"done parsing template";
		return;
	}
	//try again
	if(!d->mContent.setContent(MOdtRenderer::convertV1toV2(bytes),false,&err,&errln,&errcl)){
		qDebug()<<"Hmm, definitely not XML - even after conversion, aborting...";
		qDebug()<<" Info: line ="<<errln<<"column ="<<errcl<<"error ="<<err;
		QMessageBox::warning(this,tr("Warning"),tr("The file '%1' does not contain a valid ODF file or template of any version.").arg(d->mFileName));
		return;
	}else{
		qDebug()<<"Successfully converted the template.";
// 		qDebug()<<"document has"<<d->mContent.documentElement().childNodes().size()<<"children and"<<d->mContent.documentElement().attributes().size()<<"attribs";
		if(istempl)
			QMessageBox::information(this,tr("Conversion Info"),tr("The file '%1' did contain a version 1 template. It has been converted to version 2.\nPlease correct all formulas.").arg(d->mFileName));
	}
// 	qDebug()<<"dump\n"<<d->mContent.toByteArray();
	d->mDomModel->setDomDocument(d->mContent);
	d->mDomTree->expandAll();
	qDebug()<<"done converting and parsing template";
}

void MOdfEditor::openFile(bool istemp)
{
        d->mClipNode.clear();
	QString title=istemp?tr("Open ODF Template"):tr("Open ODF File");
	QString flt=istemp?tr("ODF Template File (*.od?t);;All Files (*)"):tr("ODF File (*.od?);;All Files (*)");
	QString fn=QFileDialog::getOpenFileName(this,title,currentDir(),flt);
	if(fn!=""){
		setCurrentDir(fn);
		d->mFileName=fn;
		loadFile(fn,istemp);
	}
}

void MOdfEditor::openFile2()
{
	openFile(false);
}

void MOdfEditor::saveFile()
{
	if(d->mFileName=="" || d->mFileName.left(2)==":/")
		saveFileAs();
	else
		saveFile(d->mFileName);
}
void MOdfEditor::saveFile ( QString fn)
{
	QFile fd(fn);
	if(!fd.open(QIODevice::ReadWrite|QIODevice::Truncate)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to write to file %1").arg(fn));
		return;
	}
	qDebug()<<"saving template to file"<<fn;
	saveTemplate(fd);
	fd.close();
}
void MOdfEditor::saveFileAs()
{
	QString fn=QFileDialog::getSaveFileName(this,tr("Save ODF Template"),d->mFileName,tr("ODF Template (*%1);;All ODF Templates (*.od?t);;All Files (*)").arg(detectTemplateType()));
	if(fn!=""){
		d->mFileName=fn;
		saveFile(fn);
		setCurrentDir(fn);
	}
}

void MOdfEditor::testSave()
{
	//generate temp file
	if(d->mTestFile.isEmpty()){
		QTemporaryFile tf(QDir::tempPath()+"/msmokeXXXXXX"+QFileInfo(d->mFileName).completeBaseName()+detectTemplateType());
		tf.setAutoRemove(false);
		tf.open();
		d->mTestFile=tf.fileName();
		qDebug()<<"created template test file"<<d->mTestFile;
	}
	//save the file
	saveFile(d->mTestFile);
}

QString MOdfEditor::detectTemplateType() const
{
	//get document
	QDomDocument doc=d->mDomModel->domDocument();
	if(doc.isNull())return QString();
	//recurse through elements: helper lambda to do it
	const auto findElement=[](const QDomElement&el,const QString&nm)->QDomElement{
		QDomNodeList nl=el.childNodes();
		for(int i=0;i<nl.size();i++){
			QDomElement el2=nl.at(i).toElement();
			if(el2.isNull())continue;
			const QString &tn=el2.tagName().split(':',QString::SkipEmptyParts).takeLast();
			if(tn==nm)return el2;
		}
		//not found
		return QDomElement();
	};
	//template 
	QDomElement el=doc.documentElement();
	if(el.isNull() || el.tagName()!= (OdfTemplatePrefix+":template"))
		return QString();
	//-> *:document-content 
	el=findElement(el,"document-content");
	if(el.isNull())return QString();
	//-> *:-> *:body 
	el=findElement(el,"body");
	if(el.isNull())return QString();
	// -> *:text(odtt)/spreadsheet(odst)/database(odbt)/drawing(odgt)/presentation(odpt)
	QDomNodeList nl=el.childNodes();
	for(int i=0;i<nl.size();i++){
		QDomElement el2=nl.at(i).toElement();
		if(el2.isNull())continue;
		const QString &tn=el2.tagName().split(':',QString::SkipEmptyParts).takeLast();
		if(tn=="text")return ".odtt";
		if(tn=="spreadsheet")return ".odst";
		if(tn=="database")return ".odbt";
		if(tn=="drawing")return ".odgt";
		if(tn=="presentation")return ".odpt";
	}
	//not found
	return QString();
}

void MOdfEditor::testOrder()
{
	//save it
	testSave();
	//open order win if not already open
	if(d->mTestOrderWin.isNull()){
		bool ok;
		qint64 oid=QInputDialog::getInt(this,tr("Test with Order"),tr("Please enter the Order ID of the order you want to use for testing:"),0,0,0x7fffffff,1,&ok);
		if(!ok)return;
		MTGetOrder go=req->queryGetOrder(oid);
		if(go.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Sorry I cannot retrieve this order: %1").arg(go.errorString()));
			return;
		}
		MOOrder ord=go.getorder();
		d->mTestOrderWin=new MOrderWindow(this,ord);
		d->mTestOrderWin->setTestTemplateFile(d->mTestFile);
	}
	//bring into foreground
	d->mTestOrderWin->show();
	d->mTestOrderWin->raise();
	d->mTestOrderWin->activateWindow();
}

void MOdfEditor::testEventSum()
{
	//save it
	testSave();
	//create window
	if(d->mTestEventWin.isNull()){
		//get event
		int eid=-1;bool ok=false;
		getEvent(eid,ok);
		if(!ok || eid<0)return;
		//open
		d->mTestEventWin=new MEventSummary(this,eid);
		d->mTestEventWin->setTestTemplateFile(d->mTestFile);
	}
	//bring into foreground
	d->mTestEventWin->show();
	d->mTestEventWin->raise();
	d->mTestEventWin->activateWindow();
}

void MOdfEditor::updateDisplay()
{
	if(d->mFileName=="")
		setWindowTitle(tr("ODF Template Editor"));
	else
		setWindowTitle(tr("ODF Template Editor [%1]").arg(d->mFileName));
	//basics
	selectionChange();
}

void MOdfEditor::saveTemplate(QFile& fd)
{
	Zip zip;
	zip.open(&fd);
	{QBuffer buf;buf.open(QIODevice::ReadWrite);
	saveXmlPart(buf);buf.seek(0);
	zip.storeFile("content.xml",buf);}
	for(int i=0;i<d->mFiles.size();i++){
		if(d->mFiles[i].type!=Private::File::Type::FlatFile)continue;
		QBuffer buf;
		buf.open(QIODevice::ReadWrite);
		buf.write(d->mFiles[i].content);
		buf.seek(0);
		zip.storeFile(d->mFiles[i].name,buf);
	}
	zip.close();
	fd.flush();
}

void MOdfEditor::saveXmlPart(QIODevice& fd)
{
	//save
	fd.write(d->mContent.toByteArray());
}

void MOdfEditor::selectionChange()
{
	saveCurrentNode();
	MCleanup clean([&](){setChanged(false);});
	d->mCurIndex=QModelIndex();
	d->mCurNode=QDomNode();
	d->setActions(QList<QAction*>(),false);
	//get selection
	QModelIndexList idxl=d->mDomTree->selectionModel()->selectedRows();
	if(idxl.size()!=1){
		emit switchStack(d->st_none);
		if(idxl.size()>1)
			d->setActions(QList<QAction*>()<<d->maWrapInCond<<d->maWrapInLoop,false);
		return;
	}
	//check element type
	d->mCurIndex=idxl[0];
	d->mCurNode=d->mDomModel->node(idxl[0]);
	QDomNode pn=d->mDomModel->node(idxl[0].parent());
	bool allowElse=pn.isElement() && pn.nodeName()==(OdfTemplatePrefix+":if");
	//comment?
	if(d->mCurNode.isComment()){
		emit switchStack(d->st_comment);
		d->mComment->setText(d->mCurNode.nodeValue());
		d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
		return;
	}
	//is it normal text?
	if(d->mCurNode.isCharacterData()){
		emit switchStack(d->st_text);
		d->mText->setText(d->mCurNode.nodeValue());
		d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maWrapInCond<<d->maWrapInLoop<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
		return;
	}
	//henceforth we are only interested in elements.
	if(!d->mCurNode.isElement()){
		emit switchStack(d->st_none);
		d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
		return;
	}
	//is it special?
	const QStringList nn=d->mCurNode.nodeName().split(':');
	if(nn.size()==2 && nn[0]==OdfTemplatePrefix){
		QDomElement el=d->mCurNode.toElement();
		if(nn[1]=="loop"){
			emit switchStack(d->st_loop);
			d->mLoopVar->setText(el.attribute("variable"));
			d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maAddIntoCalc<<d->maAddIntoComment<<d->maWrapInCond<<d->maWrapInLoop<<d->maUnwrap<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
			return;
		}
		if(nn[1]=="calculate"){
			emit switchStack(d->st_calc);
			d->mCalcExpr->setText(el.attribute("exec"));
			d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maAddIntoComment<<d->maWrapInCond<<d->maWrapInLoop<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
			return;
		}
		if(nn[1]=="if"){
			emit switchStack(d->st_cond);
			d->mCondExpr->setText(el.attribute("select"));
			d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maAddIntoCalc<<d->maAddIntoComment<<d->maWrapInCond<<d->maWrapInLoop<<d->maUnwrap<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
			return;
		}
		if(nn[1]=="template" || nn[1]=="else"){
			emit switchStack(d->st_special);
			d->mSpecial->setText(tr("<b>Tag Type:</b> %1").arg(htmlize(nn[1])));
			if(nn[1]=="else"){
				d->setActions(QList<QAction*>()<<d->maDelItem<<d->maInsBehindCalc<<d->maInsBehindComment);
			}else
				d->setActions(QList<QAction*>());
			return;
		}
		//else: fall through to normal tags
	}
	//else: other tags
	d->setActions(QList<QAction*>()<<d->maInsBehindCalc<<d->maInsBehindComment<<d->maAddIntoCalc<<d->maAddIntoComment<<d->maWrapInCond<<d->maWrapInLoop<<d->maDelItem<<(allowElse?d->maInsBehindElse:nullptr));
	emit switchStack(d->st_tag);
	displayTag();
}

void MOdfEditor::displayTag()
{
	QDomElement el=d->mCurNode.toElement();
	d->mTagAttrModel->clear();
	if(el.isNull()){
		d->mTagName->setText("");
		return;
	}
	d->mTagName->setText(el.tagName());
	//redo model
	if(d->mTagAttrModel->columnCount()<2){
		d->mTagAttrModel->insertColumns(0,2);
		d->mTagAttrModel->setHorizontalHeaderLabels(QStringList()<<tr("Attribute")<<tr("Value"));
	}
	QDomNamedNodeMap al=el.attributes();
	d->mTagAttrModel->insertRows(0,al.size());
	for(int i=0;i<al.size();i++){
		QDomAttr at=al.item(i).toAttr();
		d->mTagAttrModel->setData(d->mTagAttrModel->index(i,0),at.name());
		d->mTagAttrModel->setData(d->mTagAttrModel->index(i,1),at.value());
	}
	d->mTagAttrs->resizeColumnsToContents();
}

void MOdfEditor::saveCurrentNode()
{
	//nothing to do?
	if(!d->mChanged)return;
	setChanged(false);//prevent it from calling itself
	//make sure we are in non-changed state afterwards
	MCleanup cleanchanged([&](){setChanged(false);});
	//what is being displayed?
	const int itemtype=d->mStack->currentIndex();
	//non-changeable types
	if(!d->mCurIndex.isValid() || itemtype==d->st_none || itemtype==d->st_special){
		qDebug()<<"Warning: invalid item, or uneditable item type - why is it changed though?";
		return;
	}
	//get current item
	QDomNode cnode=d->mCurNode;
	QModelIndex cidx=d->mCurIndex;
	//remember new current item
	QModelIndex ncidx=d->mDomTree->currentIndex();
	d->mDomTree->setCurrentIndex(QModelIndex());
	MCleanup cleanindex([&](){d->mDomTree->setCurrentIndex(ncidx);});
	//handle item
	if(itemtype==d->st_comment){
		cnode.setNodeValue(d->mComment->toPlainText());
	}else if(itemtype==d->st_text){
		cnode.setNodeValue(d->mText->toPlainText());
	}else if(itemtype==d->st_tag){
		QDomElement el=cnode.toElement();
		QMap<QString,QString>nattr;
		for(int i=0;i<d->mTagAttrModel->rowCount();i++)
			nattr.insert(
			  d->mTagAttrModel->data(d->mTagAttrModel->index(i,0)).toString(),
			  d->mTagAttrModel->data(d->mTagAttrModel->index(i,1)).toString()
			);
		QDomNamedNodeMap oattr=el.attributes();
		for(int i=0;i<oattr.size();i++){
			QString nm=oattr.item(i).nodeName();
			if(!nattr.contains(nm))
				el.removeAttribute(nm);
		}
		for(QString nm:nattr.keys()){
			el.setAttribute(nm,nattr[nm]);
		}
		el.setTagName(d->mTagName->text());
// 		unneccessary, since it is shared: cnode=el;
	}else if(itemtype==d->st_calc){
		cnode.toElement().setAttribute("exec",d->mCalcExpr->text());
	}else if(itemtype==d->st_loop){
		cnode.toElement().setAttribute("variable",d->mLoopVar->text());
	}else if(itemtype==d->st_cond){
		cnode.toElement().setAttribute("select",d->mCondExpr->text());
	}else{
		qDebug()<<"Internal Error: unknown node type"<<itemtype<<"- cannot retrieve data and save it.";
		return;
	}
// 	qDebug()<<"saving item"<<cidx<<"type"<<itemtype;
	//done!
	d->mDomModel->setNode(cidx,cnode);
// 	qDebug()<<"done saving";
}

void MOdfEditor::Private::setActions(const QList< QAction* >& act,bool haveselect)
{
	maAddIntoCalc->setEnabled(act.contains( maAddIntoCalc));
	maWrapInCond->setEnabled(act.contains( maWrapInCond));
	maAddIntoComment->setEnabled(act.contains( maAddIntoComment));
	maInsBehindElse->setEnabled(act.contains( maInsBehindElse));
	maWrapInLoop->setEnabled(act.contains( maWrapInLoop));
	maInsBehindCalc->setEnabled(act.contains( maInsBehindCalc));
	maInsBehindComment->setEnabled(act.contains( maInsBehindComment));
	maUnwrap->setEnabled(act.contains(maUnwrap));
	maDelItem->setEnabled(act.contains(maDelItem));
        maCpyItem->setEnabled(haveselect);
        maInsItemBehind->setEnabled(haveselect && !mClipNode.isNull());
        maInsItemInto->setEnabled(haveselect && !mClipNode.isNull());
}

void MOdfEditor::insCalcBehindCurrent()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	QDomNode cnode=d->mDomModel->node(idx);
	if(cnode.isNull())return;
	//insert calculation
	QDomNode nn=cnode.ownerDocument().createElement(OdfTemplatePrefix+":calculate");
	d->mDomModel->insertNode(nn,idx.parent(),idx.row()+1);
}

void MOdfEditor::insCalcIntoCurrent()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	QDomNode cnode=d->mDomModel->node(idx);
	if(cnode.isNull())return;
	//insert calculation
	QDomNode nn=cnode.ownerDocument().createElement(OdfTemplatePrefix+":calculate");
	d->mDomModel->insertNode(nn,idx,0);
	//make sure it is visible
	d->expandTree(idx);
}

void MOdfEditor::insElse()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	QDomNode cnode=d->mDomModel->node(idx);
	if(cnode.isNull())return;
	//check parent
	QDomElement pnode=d->mDomModel->node(idx.parent()).toElement();
	if(pnode.isNull() || pnode.tagName()!= (OdfTemplatePrefix+":if")){
		qDebug()<<"oops. trying to insert an else into a parent that is not an if";
		return;
	}
	//insert else tag
	QDomNode nn=cnode.ownerDocument().createElement(OdfTemplatePrefix+":else");
	d->mDomModel->insertNode(nn,idx.parent(),idx.row()+1);
}

void MOdfEditor::delItem()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	//delete
	d->mDomModel->removeNode(idx);
}

void MOdfEditor::insCommentBehindCurrent()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	QDomNode cnode=d->mDomModel->node(idx);
	if(cnode.isNull())return;
	//insert comment
	QDomNode nn=cnode.ownerDocument().createComment(tr("new comment"));
	d->mDomModel->insertNode(nn,idx.parent(),idx.row()+1);
}

void MOdfEditor::insCommentIntoCurrent()
{
	//get current
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	QDomNode cnode=d->mDomModel->node(idx);
	if(cnode.isNull())return;
	//insert comment
	QDomNode nn=cnode.ownerDocument().createComment(tr("new comment"));
	d->mDomModel->insertNode(nn,idx,0);
	//make sure it is visible
	d->expandTree(idx);
}

void MOdfEditor::wrapInCond(QString tag)
{
	//get selection
	const QModelIndexList &idxl=d->mDomTree->selectionModel()->selectedRows();
	if(idxl.size()==0)return;
	//keep only items that have no parent in the list
	// (i.e. eliminate children, since this algo is recursive anyway)
	QModelIndexList idxl2;
	for(QModelIndex idx:idxl)
		if(!idxl.contains(idx.parent()))idxl2<<idx;
	//sanity check: do all remaining items have the same parent?
	QModelIndex pidx=idxl2[0].parent();
	for(QModelIndex idx:idxl2)
		if(idx.parent()!=pidx){
			QMessageBox::warning(this,tr("Warning"),tr("The selected items do not have a common parent, this means I cannot wrap them without screwing up the DOM tree. Please check your selection!"));
			return;
		}
	//get first and last index, get copies of nodes
	int frow=-1,lrow=-1;
	QList<QDomNode> ndlist;
	qSort(idxl2.begin(),idxl2.end(),[](const QModelIndex&a,const QModelIndex&b){return a.row()<b.row();});
	for(QModelIndex idx:idxl2){
		if(lrow<0)frow=lrow=idx.row();
		if(lrow<idx.row())lrow=idx.row();
		if(frow>idx.row())frow=idx.row();
		ndlist<<d->mDomModel->node(idx).cloneNode(true);
	}
	if(ndlist.size()<1)return;
	//create new parent node
	QDomElement np=ndlist[0].ownerDocument().createElement(OdfTemplatePrefix+":"+tag);
	//copy the moved items in
	for(QDomNode nd:ndlist)np.appendChild(nd);
	//remove the old items
	d->mDomModel->removeRows(frow,lrow-frow+1,pidx);
	//add the new item
	d->mDomModel->insertNode(np,pidx,frow);
	//expand
	d->expandTree(d->mDomModel->index(frow,0,pidx));
}

void MOdfEditor::Private::expandTree(const QModelIndex& idx)
{
	//expand this
	mDomTree->expand(idx);
	//recurse
	const int rc=mDomModel->rowCount(idx);
	for(int i=0;i<rc;i++)
		expandTree(mDomModel->index(i,0,idx));
}


void MOdfEditor::wrapInLoop()
{
	wrapInCond("loop");
}

void MOdfEditor::unwrapItem()
{
	//get the item
	QModelIndex idx=d->mDomTree->currentIndex();
	if(!idx.isValid())return;
	//get the node and parent index
	QDomElement el=d->mDomModel->node(idx).cloneNode(true).toElement();
	QModelIndex pidx=idx.parent();
	const int row=idx.row();
	//remove the original
	d->mDomModel->removeNode(idx);
	//insert the children into the parent node at the correct position
	d->mDomModel->insertNodes(el.childNodes(),pidx,row);
	//expand the tree
	d->expandTree(pidx);
}

void MOdfEditor::pasteItemBehind()
{
        //get current
        QModelIndex idx=d->mDomTree->currentIndex();
        if(!idx.isValid())return;
        QDomNode cnode=d->mDomModel->node(idx);
        if(cnode.isNull())return;
        //check
        if(d->mClipNode.isNull()){
                QMessageBox::warning(this,tr("Warning"),tr("There is nothing in the clipboard. Please copy a node first."));
                return;
        }
        //insert item
        d->mDomModel->insertNode(d->mClipNode,idx.parent(),idx.row()+1);
}

void MOdfEditor::pasteItemInto()
{
        //get current
        QModelIndex idx=d->mDomTree->currentIndex();
        if(!idx.isValid())return;
        QDomNode cnode=d->mDomModel->node(idx);
        if(cnode.isNull())return;
        //check
        if(d->mClipNode.isNull()){
                QMessageBox::warning(this,tr("Warning"),tr("There is nothing in the clipboard. Please copy a node first."));
                return;
        }
        //insert item
        d->mDomModel->insertNode(d->mClipNode,idx,0);
        //make sure it is visible
        d->expandTree(idx);
}

static inline
QDomNode xclonenode(const QDomNode&node)
{
        if(node.isNull())return QDomNode();
        QDomDocument doc=node.ownerDocument();
        QDomNode nn;
        switch(node.nodeType()){
                case QDomNode::ElementNode:
                        nn=doc.createElement(node.nodeName());
                        for(const QDomNode&ond:node.childNodes())
                                nn.appendChild(xclonenode(ond));
                        break;
                case QDomNode::AttributeNode:
                        nn=doc.createAttribute(node.nodeName());
                        nn.setNodeValue(node.nodeValue());
                        break;
                case QDomNode::TextNode:
                        nn=doc.createTextNode(node.nodeValue());
                        break;
                case QDomNode::CharacterDataNode:
                case QDomNode::CDATASectionNode:
                        nn=doc.createCDATASection(node.nodeValue());
                        break;
                case QDomNode::ProcessingInstructionNode:
                        nn=doc.createProcessingInstruction(node.nodeName(),node.nodeValue());
                        break;
                case QDomNode::CommentNode:
                        nn=doc.createComment(node.nodeValue());
                        break;
                //those are currently not supported or it is completely useless to copy them
                case QDomNode::EntityReferenceNode:
                case QDomNode::EntityNode:
                case QDomNode::DocumentNode:
                case QDomNode::DocumentTypeNode:
                case QDomNode::DocumentFragmentNode:
                case QDomNode::NotationNode:
                case QDomNode::BaseNode:
                        return QDomNode();
        }
        return nn;
}

void MOdfEditor::copyItem()
{
        //get current
        QModelIndex idx=d->mDomTree->currentIndex();
        if(!idx.isValid())return;
        QDomNode cnode=d->mDomModel->node(idx);
        if(cnode.isNull())return;
        //remember
        if(!d->mClipNode.isNull())d->mClipNode.clear();
        d->mClipNode=cnode.cloneNode(true);
        //housekeeping
        d->maInsItemBehind->setEnabled(!d->mClipNode.isNull());
        d->maInsItemInto->setEnabled(!d->mClipNode.isNull());
        if(d->mClipNode.isNull())
                QMessageBox::warning(this,tr("Warning"),tr("Sorry, this kinde of node cannot be copied."));
}
