//
// C++ Interface: templatedlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TEMPLATEDLG_H
#define MAGICSMOKE_TEMPLATEDLG_H

#include <QDialog>
#include <QString>

#include "templates.h"

class QComboBox;
class QStandardItem;
class QStandardItemModel;
class QStringList;
class QTreeView;

/**gives the user a choice of template variants; used by MTemplateStore only!*/
class MTemplateChoice:public QDialog
{
	Q_OBJECT
	public:
		MTemplateChoice(const QList<MTemplate>&);
		
		///returns the choice made by the user
		MTemplate choice()const;
	private:
		//selection box
		QComboBox*box;
		//data
		QList<MTemplate>templst;
};

/**lets the user add and remove templates and variants to/from the database; used by MWebRequest!*/
class MTemplateEditor:public QDialog
{
	Q_OBJECT
	public:
		MTemplateEditor(MTemplateStore*);
		
	private slots:
 		void updateView();
		void deleteItem();
		void addItem();
		void changeDescr(QStandardItem*);
		void changeFlags();
		void forceUpdate();
		void editItem();
		void saveItem();
	private:
 		MTemplateStore*store;
 		QTreeView*tree;
 		QStandardItemModel*model;
 		bool nochange;
};



#endif
