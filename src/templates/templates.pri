HEADERS += \
	$$PWD/ticketedit.h \
	$$PWD/odfedit.h \
	$$PWD/templatedlg.h

SOURCES += \
	$$PWD/ticketedit.cpp \
	$$PWD/odfedit.cpp \
	$$PWD/templatedlg.cpp

INCLUDEPATH += $$PWD
