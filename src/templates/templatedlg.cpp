//
// C++ Implementation: templatedlg
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2013
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "templatedlg.h"
#include "ticketedit.h"
#include "odfedit.h"
#include "flagedit.h"
#include "misc.h"

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTreeView>

MTemplateChoice::MTemplateChoice(const QList<MTemplate>&tl)
	:templst(tl)
{
	setWindowTitle(tr("Chose Template"));
	setSizeGripEnabled(true);
	QVBoxLayout *vl;
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("Please chose a variant of template %1:").arg(templst.value(0).fileName())));
	vl->addWidget(box=new QComboBox);
	box->setEditable(false);
	for(const MTemplate&tmp:templst){
		box->addItem(tmp.fileName()+": "+tmp.description());
	}
	vl->addStretch(1);
	QHBoxLayout *hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	p->setDefault(true);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
}

MTemplate MTemplateChoice::choice()const
{
        return templst.value(box->currentIndex());
}

static inline int initTemplateChoser()
{
        MTemplate::setTemplateChoiceFunction([](const QList<MTemplate>&tl)->MTemplate{
                MTemplateChoice tc(tl);
                if(tc.exec()==QDialog::Accepted)
                        return tc.choice();
                return MTemplate();
        });
        return 0;
}
static int dummy=initTemplateChoser();
/**************************************************************/

MTemplateEditor::MTemplateEditor(MTemplateStore*s)
{
	store=s;
	nochange=false;
	
	setWindowTitle(tr("Edit Template Directory"));
	setSizeGripEnabled(true);
	
	QHBoxLayout*hl;
	QVBoxLayout*vl,*vl2;
	
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout,1);
	hl->addWidget(tree=new QTreeView,1);
	tree->setModel(model=new QStandardItemModel(this));
	connect(model,SIGNAL(itemChanged(QStandardItem*)),this,SLOT(changeDescr(QStandardItem*)));
	QPushButton*p;
	hl->addLayout(vl2=new QVBoxLayout);
	vl2->addWidget(p=new QPushButton(tr("&Update Now")));
	connect(p,SIGNAL(clicked()),this,SLOT(forceUpdate()));
	vl2->addSpacing(20);
	vl2->addWidget(p=new QPushButton(tr("&Add Variant")));
	connect(p,SIGNAL(clicked()),this,SLOT(addItem()));
	vl2->addWidget(p=new QPushButton(tr("&Delete Variant")));
	connect(p,SIGNAL(clicked()),this,SLOT(deleteItem()));
	vl2->addSpacing(20);
	vl2->addWidget(p=new QPushButton(tr("Change &Flags")));
	connect(p,SIGNAL(clicked()),this,SLOT(changeFlags()));
	vl2->addSpacing(20);
	vl2->addWidget(p=new QPushButton(tr("&Save Template...")));
	connect(p,SIGNAL(clicked()),this,SLOT(saveItem()));
	vl2->addWidget(p=new QPushButton(tr("&Edit Template")));
	connect(p,SIGNAL(clicked()),this,SLOT(editItem()));
	vl2->addStretch(1);
	
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	hl->addWidget(p=new QPushButton(tr("&Close")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	
	updateView();
}

static const int BASEROLE=Qt::UserRole;
static const int NAMEROLE=Qt::UserRole+1;

void MTemplateEditor::updateView()
{
	//basic cleanup
	nochange=true;
	model->clear();
	model->insertColumns(0,4);
	model->setHorizontalHeaderLabels(QStringList() <<tr("Template/Variant") <<tr("Description") <<tr("Checksum") <<tr("Flags"));
	//get base names
	QStringList base=MTemplate::legalBaseNames();
	//get all templates, sort them into buckets
	QList<MTemplate>all=store->allTemplates();
	QMap<QString,QList<MTemplate> >hier;
	for(int i=0;i<all.size();i++){
		QString b=all[i].baseName();
		if(!base.contains(b))base.append(b);
		if(!hier.contains(b))hier.insert(b,QList<MTemplate>());
		hier[b].append(all[i]);
	}
	//create hierarchy
	qSort(base);
	model->insertRows(0,base.size());
	for(int i=0;i<base.size();i++){
		QModelIndex idx=model->index(i,0);
		model->setData(idx,base[i]);
		model->setData(idx,base[i],BASEROLE);
		model->setData(idx,"",NAMEROLE);//base must contain empty name for logic in deleteItem and addItem
		model->itemFromIndex(idx)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
		model->setData(model->index(i,1),"");
		model->itemFromIndex(model->index(i,1))->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
		model->setData(model->index(i,2),"");
		model->itemFromIndex(model->index(i,1))->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
		if(!hier.contains(base[i]))continue;
		if(hier[base[i]].size()<1)continue;
		QList<MTemplate>sub=hier[base[i]];
		model->insertRows(0,sub.size(),idx);
		model->insertColumns(0,4,idx);
		for(int j=0;j<sub.size();j++){
			QModelIndex idx2=model->index(j,0,idx);
			model->setData(idx2,sub[j].fileName()+" ("+sub[j].variantID()+")");
			model->setData(idx2,base[i],BASEROLE);
			model->setData(idx2,sub[j].completeFileName(),NAMEROLE);
			model->itemFromIndex(idx2)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
			model->setData(model->index(j,1,idx),sub[j].description());
			model->setData(model->index(j,2,idx),sub[j].checksum());
			model->itemFromIndex(model->index(j,2,idx))->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
			model->setData(model->index(j,3,idx),sub[j].flags());
			model->itemFromIndex(model->index(j,3,idx))->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
		}
	}
	//expand all
	tree->expandAll();
	tree->resizeColumnToContents(0);
	tree->resizeColumnToContents(1);
	tree->resizeColumnToContents(2);
	tree->resizeColumnToContents(3);
	nochange=false;
}
void MTemplateEditor::deleteItem()
{
	//get selection
	QModelIndex idx=tree->currentIndex();
	if(!idx.isValid())return;
	QModelIndex bidx=model->index(idx.row(),0,idx.parent());
	QString fn=model->data(bidx,NAMEROLE).toString();
	if(fn=="")return;
	//delete
	if(store->deleteTemplate(fn))
		model->removeRow(idx.row(),idx.parent());
	else
		QMessageBox::warning(this,tr("Warning"),tr("Unable to delete this template."));
}

void MTemplateEditor::addItem()
{
	//get selection
	QModelIndex idx=tree->currentIndex();
	if(!idx.isValid())return;
	//parent index is the one with the base (top level), bidx is the first one below
	QModelIndex pidx=model->data(idx,NAMEROLE).toString().trimmed().isEmpty()?idx:idx.parent();
	QString base=model->data(idx,BASEROLE).toString();
// 	qDebug()<<"base"<<base<<"name"<<model->data(idx,NAMEROLE).toString()<<(pidx.isValid()?"v":"nv");
	if(base=="")return;
	//query file name
	QString fn=QFileDialog::getOpenFileName(this,tr("Select Template File"),currentDir());
	if(fn=="")return;
	setCurrentDir(fn);
	//check extension
	QString ext=QFileInfo(fn).completeSuffix();
	if(!MTemplate::legalSuffixes(base).contains(ext)){
		QMessageBox::warning(this,tr("Warning"),tr("Files with this extension (%1) are not legal for this template.").arg(ext));
		return;
	}
	//get new variant name
	QStringList lst;
	for(int i=0;i<model->rowCount(pidx);i++){
		QStringList f=model->data(model->index(i,0,pidx),NAMEROLE).toString().split(",");
		if(f.size()>1)
			lst<<f[1];
	}
	QString nvar;
	for(int i=0;i<1000000000;i++){
		nvar=QString::number(i,16).toLower();
		if(!lst.contains(nvar))break;
	}
	//send up
	if(store->setTemplate(base+"."+ext+","+nvar,fn))
		forceUpdate();
	else
		QMessageBox::warning(this,tr("Warning"),tr("Unable to upload file."));
}

void MTemplateEditor::changeDescr(QStandardItem*item)
{
	if(nochange)return;
	if(!item)return;
	//sanity check
	if(item->column()!=1)return;
	//get full name & data
	QModelIndex idx=item->index();
	QString dsc=model->data(idx).toString();
	QModelIndex bidx=model->index(item->row(),0,idx.parent());
	QString fn=model->data(bidx,NAMEROLE).toString();
	if(fn=="")return;
	//send to server
	if(!store->setTemplateDescription(fn,dsc))
		QMessageBox::warning(this,tr("Warning"),tr("Unable to send new description to server."));
}

void MTemplateEditor::forceUpdate()
{
	store->updateTemplates(true);
	updateView();
}

void MTemplateEditor::editItem()
{
	//get selection
	QModelIndex idx=tree->currentIndex();
	if(!idx.isValid())return;
	QModelIndex pidx=idx.parent();
	QModelIndex bidx=model->index(idx.row(),0,pidx);
	QString base=model->data(bidx,BASEROLE).toString();
	if(base=="")return;
	//query file name
	QString fn=model->data(bidx,NAMEROLE).toString();
	qDebug()<<"editing base"<<base<<"fn"<<fn;
	//check extension
	MTemplate::Types tp=MTemplate::UnknownTemplate;
	MTemplate tmp;
	if(fn!=""){
		tmp=store->getTemplateByFile(fn);
		tp=tmp.type();
	}else{
		tp=MTemplate::legalTypes(base);
	}
	if(tp&MTemplate::OdfTemplateMask){
		MOdfEditor *ed=new MOdfEditor(this);
		if(fn!="")
			ed->loadFile(tmp.cacheFileName());
		ed->show();
	}else
	if(tp==MTemplate::LabelTemplate){
		MTicketEditor *ed=new MTicketEditor(this);
		if(fn!="")
			ed->loadFile(tmp.cacheFileName());
		ed->show();
	}else
	if(tp==MTemplate::ZipTemplate){
		QMessageBox::warning(this,"Sorry","Sorry, no ZIP selection");
	}else{
		QMessageBox::warning(this,tr("Warning"),tr("Unknown template type, cannot edit it."));
	}
}

void MTemplateEditor::saveItem()
{
	//get selection
	QModelIndex idx=tree->currentIndex();
	if(!idx.isValid())return;
	QModelIndex pidx=idx.parent();
	QModelIndex bidx=model->index(idx.row(),0,pidx);
	QString base=model->data(bidx,BASEROLE).toString();
	if(base=="")return;
	//query file name
	QString fn=model->data(bidx,NAMEROLE).toString();
	if(fn=="")return;
	//get it
	MTemplate tmp=store->getTemplateByFile(fn);
	fn=tmp.cacheFileName();
	if(fn==""){
		QMessageBox::warning(this,tr("Warning"),tr("Ooops. Lost the template file, cannot store it."));
		return;
	}
	//get file name
	QString nfn;
	{
		QFileDialog fd(this,tr("Save template as..."));
		fd.setAcceptMode(QFileDialog::AcceptSave);
		fd.setDefaultSuffix(tmp.extension());
		fd.setDirectory(currentDir());
		fd.selectFile(tmp.baseName());
		if(fd.exec()!=QDialog::Accepted)
			return;
		nfn=fd.selectedFiles().at(0);
		if(nfn=="")return;
		setCurrentDir(nfn);
	}
	//store it
	if(!QFile::copy(fn,nfn))
		QMessageBox::warning(this,tr("Error"),tr("Unable to save the template file."));
	
}

void MTemplateEditor::changeFlags()
{
	//get selection
	QModelIndex idx=tree->currentIndex();
	if(!idx.isValid())return;
	QModelIndex pidx=idx.parent();
	QModelIndex bidx=model->index(idx.row(),0,pidx);
	QString base=model->data(bidx,BASEROLE).toString();
	if(base=="")return;
	//query file name
	QString fn=model->data(bidx,NAMEROLE).toString();
	if(fn=="")return;
	//get it
	MTemplate tmp=store->getTemplateByFile(fn);
	if(tmp.cacheFileName()==""){
		QMessageBox::warning(this,tr("Warning"),tr("Ooops. Lost the template file, cannot alter it."));
		return;
	}
	//edit flags
	MFlagEditor fe(this,tmp.flags(),tr("Edit flags of template '%1'").arg(fn));
	if(fe.exec()!=QDialog::Accepted)return;
	//send to server
	QString nflags=fe.currentFlags();
	if(!store->setTemplateFlags(fn,nflags)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to send updated flags to server."));
		return;
	}
	//update locally
	bidx=model->index(idx.row(),3,pidx);
	model->setData(bidx,nflags);
}
