//
// C++ Interface: main
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MAIN_H
#define MAGICSMOKE_MAIN_H

#include <QtCore/QtGlobal>

#include "smokeexport.h"

///shortcut: the main function
class MAGICSMOKE_EXPORT MagicSmokeMain
{
public:
	static int realmain(int,char**);
};

#endif
