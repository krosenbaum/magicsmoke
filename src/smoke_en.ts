<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MAclWindow</name>
    <message>
        <location filename="mwin/aclwin.cpp" line="47"/>
        <source>ACL Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="54"/>
        <source>&amp;Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="55"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="59"/>
        <source>Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="61"/>
        <source>Roles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="63"/>
        <source>Hosts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/aclwin.cpp" line="65"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAddressChoiceDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="818"/>
        <source>Chose an Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="833"/>
        <source>Add Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="836"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="869"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="869"/>
        <source>Unable to save changes made to addresses: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAddressDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="693"/>
        <source>Edit Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="695"/>
        <source>Create Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="704"/>
        <source>Last used:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="705"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="706"/>
        <location filename="dialogs/customerdlg.cpp" line="707"/>
        <source>Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="708"/>
        <source>City:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="709"/>
        <source>State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="710"/>
        <source>ZIP Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="711"/>
        <source>Country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="720"/>
        <location filename="dialogs/customerdlg.cpp" line="759"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="722"/>
        <location filename="dialogs/customerdlg.cpp" line="761"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="790"/>
        <source> Create New Country...</source>
        <comment>this pseudo-entry must contain leading space to distinguish it from genuine countries</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="793"/>
        <source>Select Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="793"/>
        <source>Please select a country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="746"/>
        <source>Create New Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="754"/>
        <source>Country Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="755"/>
        <source>Abbreviation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="766"/>
        <location filename="dialogs/customerdlg.cpp" line="772"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="766"/>
        <source>The country name and abbreviation must contain something!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="772"/>
        <source>Error while creating country: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAddressWidget</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="516"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="520"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="523"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="576"/>
        <source>Delete Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="576"/>
        <source>Really delete this address?
%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MBackupDialog</name>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="42"/>
        <source>Backup Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="47"/>
        <source>Backup File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="49"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="52"/>
        <source>Generations to keep:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="57"/>
        <source>Automatic Backup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="61"/>
        <source>Interval in days:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="69"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="72"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/backupdlg.cpp" line="78"/>
        <source>Backup File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCartTab</name>
    <message>
        <location filename="mwin/carttab.cpp" line="75"/>
        <source>Add Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="77"/>
        <source>Add Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="79"/>
        <source>Add Shop Item</source>
        <oldsource>Remove Item</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="84"/>
        <source>Remove Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="111"/>
        <source>Customer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="117"/>
        <source>Invoice Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="130"/>
        <source>Shipping Method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="123"/>
        <source>Delivery Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="137"/>
        <source>Comments:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="144"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="150"/>
        <source>Reserve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="153"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="170"/>
        <source>Add &amp;Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="171"/>
        <source>Add &amp;Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="168"/>
        <source>Ca&amp;rt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="90"/>
        <source>Total Price Sum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="147"/>
        <source>Sell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="172"/>
        <source>Add &amp;Shop-Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="173"/>
        <source>&amp;Remove Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="174"/>
        <source>&amp;Abort Shopping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="176"/>
        <source>&amp;Update Shipping Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="190"/>
        <source>(No Shipping)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Start Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="277"/>
        <location filename="mwin/carttab.cpp" line="332"/>
        <location filename="mwin/carttab.cpp" line="339"/>
        <location filename="mwin/carttab.cpp" line="421"/>
        <location filename="mwin/carttab.cpp" line="537"/>
        <location filename="mwin/carttab.cpp" line="575"/>
        <location filename="mwin/carttab.cpp" line="579"/>
        <location filename="mwin/carttab.cpp" line="618"/>
        <location filename="mwin/carttab.cpp" line="628"/>
        <location filename="mwin/carttab.cpp" line="857"/>
        <location filename="mwin/carttab.cpp" line="867"/>
        <location filename="mwin/carttab.cpp" line="871"/>
        <location filename="mwin/carttab.cpp" line="877"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="277"/>
        <source>Please set the customer first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="367"/>
        <location filename="mwin/carttab.cpp" line="460"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="332"/>
        <source>Error getting event, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="95"/>
        <source>Coupon Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="96"/>
        <source>(none)</source>
        <comment>coupon code label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="97"/>
        <source>Add Coupon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="100"/>
        <source>Pay with Vouchers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="102"/>
        <source>Add Voucher...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="105"/>
        <source>Remaining Sum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="215"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="339"/>
        <source>This event has no prices associated that you can use for sales. Cannot sell tickets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="345"/>
        <source>Select Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="351"/>
        <source>Please chose a price category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="365"/>
        <location filename="mwin/carttab.cpp" line="458"/>
        <location filename="mwin/carttab.cpp" line="667"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="421"/>
        <source>Cannot pay for vouchers with another voucher!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="427"/>
        <source>Select Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="438"/>
        <source>Select voucher price and value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="440"/>
        <source>Price:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="446"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="478"/>
        <source>Voucher (value %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="537"/>
        <source>There are problems with the contents of the cart, please check and then try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="560"/>
        <location filename="mwin/carttab.cpp" line="564"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="560"/>
        <source>There is nothing in the order. Ignoring it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="564"/>
        <source>Please chose a customer first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="568"/>
        <source>Shipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="568"/>
        <source>You have chosen a shipping method, but no address. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="575"/>
        <source>Reservations can only contain tickets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="579"/>
        <source>Reservations cannot contain any vouchers for payment yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="618"/>
        <source>Error while creating reservation: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="628"/>
        <source>Error while creating order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="642"/>
        <source>Voucher State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Original Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="651"/>
        <source>Remaining Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="661"/>
        <source>Paid in Cash: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="678"/>
        <source>The customer is not valid, please chose another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="683"/>
        <source>The delivery address is not valid, please chose another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="688"/>
        <source>The invoice address is not valid, please chose another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="695"/>
        <source>Shipping Type does not exist or you do not have permission to use it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="728"/>
        <source>The event is already over, please remove this entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="733"/>
        <source>You cannot order tickets for this event anymore, ask a more privileged user.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="739"/>
        <source>The event or category is (almost) sold out, there are %1 tickets left. (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="745"/>
        <source>The event does not exist or there is another serious problem, please remove this entry. (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="819"/>
        <source>(none)</source>
        <comment>empty coupon code label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="820"/>
        <source>[Add Coupon]</source>
        <comment>link/label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="834"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; Paying for vouchers with older vouchers!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="839"/>
        <source>[remove]</source>
        <comment>remove voucher link/label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="844"/>
        <source>[Add Voucher]</source>
        <comment>link/label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="857"/>
        <source>Only one coupon can be used on an order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="861"/>
        <source>Enter Coupon ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="861"/>
        <source>Please enter the coupon ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="867"/>
        <source>&apos;%1&apos; is not a valid coupon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="871"/>
        <source>Sorry, you are not allowed to use this coupon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="877"/>
        <source>Sorry, not implemented yet. Please wait a few days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="941"/>
        <source>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Sorry, coupon limits are not implemented yet.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="946"/>
        <source>&lt;html&gt;&lt;font color=&quot;red&quot;&gt;Sorry, coupon rule limits are not implemented yet.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="1005"/>
        <source>Original price: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="783"/>
        <source>You do not have permission to create vouchers with this value, please remove it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/carttab.cpp" line="788"/>
        <source>The price tag of this voucher is not valid, please remove and recreate it.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCheckDialog</name>
    <message>
        <location filename="dialogs/checkdlg.cpp" line="33"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/checkdlg.cpp" line="35"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MContactTableDelegate</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="447"/>
        <source>(New Contact Type)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="466"/>
        <source>Create new Contact Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="474"/>
        <source>Contact Type Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="475"/>
        <source>Contact Type URI Prefix:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="478"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="480"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="485"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="485"/>
        <source>Error while creating contact type: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCustomerDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="235"/>
        <source>Customer %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="237"/>
        <source>New Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="249"/>
        <source>Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="252"/>
        <location filename="dialogs/customerdlg.cpp" line="387"/>
        <source>Change Mail Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="253"/>
        <source>Reset Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="257"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="264"/>
        <source>Web-Login/eMail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="266"/>
        <source>Edit Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="269"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="275"/>
        <source>Addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="280"/>
        <source>Add Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="283"/>
        <source>Contact Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="290"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="292"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="297"/>
        <source>Type</source>
        <comment>table: contact type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="297"/>
        <source>Contact</source>
        <comment>table: contact info</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="312"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="316"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="387"/>
        <source>Please enter the mail address for this customer to log into the web portal:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="391"/>
        <source>Unable to set new email: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="403"/>
        <source>Error while resetting password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="405"/>
        <source>Password Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="405"/>
        <source>A password reset mail has been sent to the customer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="259"/>
        <source>First Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="261"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="371"/>
        <location filename="dialogs/customerdlg.cpp" line="378"/>
        <location filename="dialogs/customerdlg.cpp" line="391"/>
        <location filename="dialogs/customerdlg.cpp" line="403"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="371"/>
        <source>Error while changing customer data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="378"/>
        <source>Error while creating customer data: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCustomerListDialog</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="57"/>
        <source>Select a Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="59"/>
        <source>Customers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="80"/>
        <source>Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="82"/>
        <source>Create new...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="85"/>
        <source>Delete...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="93"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="96"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="99"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="191"/>
        <source>Delete Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="194"/>
        <source>Really delete this customer (%1)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="197"/>
        <source>merge with other entry:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="209"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="211"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="218"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="218"/>
        <source>Failed to delete customer: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEEPriceEdit</name>
    <message>
        <location filename="dialogs/eventedit.cpp" line="281"/>
        <source>Change Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="285"/>
        <source>Price category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="287"/>
        <source>Price:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="289"/>
        <source>Maximum Seats:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="290"/>
        <source>Ordering:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="291"/>
        <source>Flags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="304"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="306"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="312"/>
        <source>Edit flags of price %1:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEntranceTab</name>
    <message>
        <location filename="mwin/entrancetab.cpp" line="50"/>
        <source>Enter or scan Ticket-ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="65"/>
        <source>Open Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="71"/>
        <source>Total:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="78"/>
        <source>Used:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="83"/>
        <source>Unused:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="154"/>
        <source>searching...</source>
        <comment>entrance control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="161"/>
        <source>Ticket &quot;%1&quot; Not Valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="165"/>
        <source>Ticket &quot;%1&quot; is not for this event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="169"/>
        <source>Ticket &quot;%1&quot; has already been used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="173"/>
        <source>Ticket &quot;%1&quot; has not been bought.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="177"/>
        <source>Ticket &quot;%1&quot; Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="181"/>
        <source>Ticket &quot;%1&quot; is not paid for!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="185"/>
        <source>Ticket &quot;%1&quot; cannot be accepted, please check the order!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="262"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="262"/>
        <source>Error while retrieving order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="284"/>
        <source>Entrance Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="290"/>
        <source>Show events that start within hours:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="293"/>
        <source>Show events a maximum of hours after they end:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="296"/>
        <source>Use Cache:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="297"/>
        <source>Cache update interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="302"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/entrancetab.cpp" line="304"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEventEditor</name>
    <message>
        <location filename="dialogs/eventedit.cpp" line="60"/>
        <location filename="dialogs/eventedit.cpp" line="245"/>
        <location filename="dialogs/eventedit.cpp" line="249"/>
        <location filename="dialogs/eventedit.cpp" line="352"/>
        <location filename="dialogs/eventedit.cpp" line="395"/>
        <location filename="dialogs/eventedit.cpp" line="493"/>
        <location filename="dialogs/eventedit.cpp" line="503"/>
        <location filename="dialogs/eventedit.cpp" line="511"/>
        <location filename="dialogs/eventedit.cpp" line="525"/>
        <location filename="dialogs/eventedit.cpp" line="574"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="60"/>
        <source>Unable to load event from server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="85"/>
        <source>Event Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="99"/>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="102"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="108"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="113"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="122"/>
        <source>Start Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="125"/>
        <location filename="dialogs/eventedit.cpp" line="132"/>
        <source>ddd MMMM d yyyy, h:mm ap</source>
        <comment>time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="129"/>
        <source>End Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="144"/>
        <source>Room/Place:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="153"/>
        <source>Seat Plan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="162"/>
        <source>Capacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="168"/>
        <source>Event Cancelled:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="176"/>
        <source>Flags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="179"/>
        <source>Edit Flags of Event &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="181"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="183"/>
        <source>The description will be displayed on the web site, please use HTML syntax.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="187"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="189"/>
        <source>The comment is for internal use only, please add any hints relevant for your collegues.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="193"/>
        <source>Prices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="198"/>
        <source>Change Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="200"/>
        <source>Add Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="202"/>
        <source>Remove Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="208"/>
        <source>Hint: Prices with the lowest Ordering number are shown first when selecting a category for tickets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="213"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="216"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="245"/>
        <source>Error while creating event: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="249"/>
        <source>Error while changing event: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Ticket Capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Seats Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Ordering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="257"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="352"/>
        <source>Price category already exists in this event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="395"/>
        <source>Cannot remove price &apos;%1&apos; - it has tickets in the database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="425"/>
        <location filename="dialogs/eventedit.cpp" line="470"/>
        <location filename="dialogs/eventedit.cpp" line="556"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="438"/>
        <source>Select a Seat Plan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="458"/>
        <source>New...</source>
        <comment>new seatplan</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="462"/>
        <source>Select</source>
        <comment>select seatplan</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="464"/>
        <source>Remove Seat Plan</source>
        <comment>remove seatplan from event</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="489"/>
        <source>New Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="489"/>
        <source>Name of new room:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="493"/>
        <source>Error while creating new room: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="503"/>
        <source>Please select a room first, since a seat plan must have a room.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="506"/>
        <source>Please select a seat plan file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="511"/>
        <source>Cannot open seat plan file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="517"/>
        <source>Seat Plan Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="517"/>
        <source>Please enter a name for your seat plan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="525"/>
        <source>Error while creating seat plan: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="536"/>
        <source>Select an Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="550"/>
        <source>New...</source>
        <comment>new artist</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="554"/>
        <source>Select</source>
        <comment>select artist</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="570"/>
        <source>New Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="570"/>
        <source>Name of new artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="574"/>
        <source>Error while creating new artist: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="408"/>
        <source>Select a Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="419"/>
        <source>New...</source>
        <comment>new room</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventedit.cpp" line="423"/>
        <source>Select</source>
        <comment>select room</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEventSummary</name>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="39"/>
        <source>Summary for Event %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="45"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="49"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="51"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="53"/>
        <source>Start:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="55"/>
        <source>Capacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="57"/>
        <source>Tickets currently reserved:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="59"/>
        <source>Tickets currently cancelled:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="61"/>
        <source>Tickets currently usable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="63"/>
        <source>Total Income:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="68"/>
        <source>Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Bought</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Unused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="73"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="85"/>
        <source>Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="91"/>
        <source>Order: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="93"/>
        <source>Customer: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="103"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="105"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="108"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="123"/>
        <location filename="dialogs/eventsummary.cpp" line="187"/>
        <location filename="dialogs/eventsummary.cpp" line="205"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="123"/>
        <source>Error while retrieving data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="187"/>
        <location filename="dialogs/eventsummary.cpp" line="205"/>
        <source>Unable to get template file (eventsummary). Giving up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/eventsummary.cpp" line="212"/>
        <source>Open Document File (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MEventsTab</name>
    <message>
        <location filename="mwin/eventstab.cpp" line="96"/>
        <source>&amp;Update Event List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="108"/>
        <source>&amp;Show/Edit details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="110"/>
        <source>&amp;New Event...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="101"/>
        <source>Show &amp;old Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="94"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="98"/>
        <source>Event &amp;Summary...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="106"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="112"/>
        <source>&amp;Clone Current Event...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="114"/>
        <source>C&amp;ancel Event...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="117"/>
        <source>&amp;Edit Price Categories...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Start Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Free</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Sold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="150"/>
        <source>Capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="254"/>
        <source>Cancel Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="254"/>
        <source>Please enter a reason to cancel event &quot;%1&quot; or abort:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="258"/>
        <source>Event Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="258"/>
        <source>The event &quot;%1&quot; has been cancelled. Please inform everybody who bought a ticket.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="260"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="260"/>
        <source>Unable to cancel event &quot;%1&quot;: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="303"/>
        <source>Select Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="318"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/eventstab.cpp" line="321"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MFlagEditor</name>
    <message>
        <location filename="dialogs/flagedit.cpp" line="35"/>
        <source>ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="36"/>
        <source>must have</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="37"/>
        <source>must not have</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="81"/>
        <source>Edit Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="106"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="109"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="111"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/flagedit.cpp" line="151"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MFlagTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="810"/>
        <source>New Flag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="813"/>
        <source>Delete Flag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="817"/>
        <source>Change Description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="835"/>
        <source>Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="835"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="850"/>
        <source>Create New Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="850"/>
        <source>Please enter a name for the flag, it must contain only letters and digits.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="858"/>
        <location filename="mwin/acltabs.cpp" line="877"/>
        <location filename="mwin/acltabs.cpp" line="897"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="858"/>
        <source>Error while creating flag: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="873"/>
        <source>Really Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="873"/>
        <source>Really delete the flag &apos;%1&apos;? Doing so may make some entities visible or invisible unexpectedly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="877"/>
        <source>Error while attempting to delete flag: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="891"/>
        <source>Change Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="891"/>
        <source>Please enter a new description for flag &apos;%1&apos;:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="897"/>
        <source>Error while attempting to alter flag: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MHostTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="344"/>
        <source>New Host...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="347"/>
        <source>Delete Host...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="351"/>
        <source>Generate New Key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="354"/>
        <source>Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="357"/>
        <source>Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="377"/>
        <source>Host Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="377"/>
        <source>Host Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="391"/>
        <source>Create New Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="391"/>
        <source>Please enter a host name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="410"/>
        <location filename="mwin/acltabs.cpp" line="429"/>
        <location filename="mwin/acltabs.cpp" line="461"/>
        <location filename="mwin/acltabs.cpp" line="483"/>
        <location filename="mwin/acltabs.cpp" line="506"/>
        <location filename="mwin/acltabs.cpp" line="514"/>
        <location filename="mwin/acltabs.cpp" line="519"/>
        <location filename="mwin/acltabs.cpp" line="524"/>
        <location filename="mwin/acltabs.cpp" line="530"/>
        <location filename="mwin/acltabs.cpp" line="536"/>
        <location filename="mwin/acltabs.cpp" line="541"/>
        <location filename="mwin/acltabs.cpp" line="549"/>
        <location filename="mwin/acltabs.cpp" line="577"/>
        <location filename="mwin/acltabs.cpp" line="593"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="410"/>
        <source>Error while creating new host: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="425"/>
        <source>Delete this Host?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="425"/>
        <source>Really delete host &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="429"/>
        <source>Error while deleting host: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="443"/>
        <source>Change Host Key?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="443"/>
        <source>Really change the key of host &apos;%1&apos;? It will lock users from thist host out until you install the key at it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="461"/>
        <location filename="mwin/acltabs.cpp" line="549"/>
        <source>Error while changing host: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="495"/>
        <source>Import Key from File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="506"/>
        <source>Unable to open file %1 for reading: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="514"/>
        <source>This is not a host key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="519"/>
        <source>This is not a host key/hash file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="524"/>
        <source>This host key file does not contain a valid host name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="536"/>
        <source>This host hash file does not contain a valid key hash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="541"/>
        <source>This host key file does not contain a valid key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="582"/>
        <source>Export Hash to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="530"/>
        <source>The key check sum did not match. Please get a clean copy of the host key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="577"/>
        <source>This host cannot be exported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="471"/>
        <source>Export Key to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="483"/>
        <location filename="mwin/acltabs.cpp" line="593"/>
        <source>Unable to open file %1 for writing: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MNewCustomerWizard</name>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="905"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="908"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="911"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="914"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="944"/>
        <source>Please enter name and address information.
Please enter it also if it is not needed immediately.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="945"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="951"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="954"/>
        <source>Family Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="956"/>
        <source>Given Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="957"/>
        <source>Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="959"/>
        <source>123 Example Street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="960"/>
        <source>City:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="963"/>
        <source>Zip Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="968"/>
        <source>Chose City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="970"/>
        <source>State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="975"/>
        <source>State (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="977"/>
        <source>Country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="984"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1000"/>
        <source>Please enter at least one way of contacting the customer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1005"/>
        <source>Please enter %1</source>
        <comment>%1=contact type name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1055"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/customerdlg.cpp" line="1055"/>
        <source>There was an error while creating the customer: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOAItem</name>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="183"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="184"/>
        <source>Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg_p.h" line="185"/>
        <source>Voucher</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOdfEditor</name>
    <message>
        <location filename="templates/odfedit.cpp" line="144"/>
        <location filename="templates/odfedit.cpp" line="546"/>
        <source>ODF Template Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="147"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="148"/>
        <source>&amp;Open Template File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="148"/>
        <source>Ctrl+O</source>
        <comment>open file shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="149"/>
        <source>&amp;Import ODF File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="149"/>
        <source>Ctrl+Shift+O</source>
        <comment>import ODF file shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="150"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="150"/>
        <source>Ctrl+S</source>
        <comment>save file shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="151"/>
        <source>Save &amp;as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="153"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="155"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="156"/>
        <source>Insert &amp;Calculation into current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="157"/>
        <source>Insert Calculation behind current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="158"/>
        <source>&amp;Wrap in Condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="159"/>
        <source>Wrap in &amp;Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="160"/>
        <source>Insert &amp;Else behind current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="161"/>
        <source>Insert Comment into current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="162"/>
        <source>Insert Comment behind current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="164"/>
        <source>Copy current item to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="165"/>
        <source>Insert clipboard into current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="166"/>
        <source>Insert clipboard behind current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="168"/>
        <source>Unwrap Loop/Condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="169"/>
        <source>&amp;Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="171"/>
        <source>&amp;Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="172"/>
        <source>Test with &amp;Order...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="173"/>
        <source>Test with Event &amp;Summary...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="188"/>
        <source>Document XML Tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="216"/>
        <source>&lt;h1&gt;Special Template Tag&lt;h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="222"/>
        <source>&lt;h1&gt;Plain Text&lt;h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="228"/>
        <source>&lt;h1&gt;Tag&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="230"/>
        <source>Tag Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="234"/>
        <source>Attributes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="256"/>
        <source>&lt;h1&gt;Loop&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="258"/>
        <source>Loop Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="265"/>
        <source>&lt;h1&gt;Calculation&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="267"/>
        <location filename="templates/odfedit.cpp" line="276"/>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="274"/>
        <source>&lt;h1&gt;Condition&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="283"/>
        <source>&lt;h1&gt;Comment&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="324"/>
        <location filename="templates/odfedit.cpp" line="329"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="324"/>
        <source>Unable to open file &apos;%1&apos; for reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="329"/>
        <source>Unable to interpret file &apos;%1&apos;. It is not an ODF container (PKZip format).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="382"/>
        <location filename="templates/odfedit.cpp" line="425"/>
        <location filename="templates/odfedit.cpp" line="510"/>
        <location filename="templates/odfedit.cpp" line="851"/>
        <location filename="templates/odfedit.cpp" line="919"/>
        <location filename="templates/odfedit.cpp" line="935"/>
        <location filename="templates/odfedit.cpp" line="1000"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="382"/>
        <source>The file &apos;%1&apos; does not contain a valid ODF file or template of any version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="388"/>
        <source>Conversion Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="388"/>
        <source>The file &apos;%1&apos; did contain a version 1 template. It has been converted to version 2.
Please correct all formulas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="399"/>
        <source>Open ODF Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="399"/>
        <source>Open ODF File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="400"/>
        <source>ODF Template File (*.od?t);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="400"/>
        <source>ODF File (*.od?);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="425"/>
        <source>Unable to write to file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="434"/>
        <source>Save ODF Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="434"/>
        <source>ODF Template (*%1);;All ODF Templates (*.od?t);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="506"/>
        <source>Test with Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="506"/>
        <source>Please enter the Order ID of the order you want to use for testing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="510"/>
        <source>Sorry I cannot retrieve this order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="548"/>
        <source>ODF Template Editor [%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="642"/>
        <source>&lt;b&gt;Tag Type:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="669"/>
        <source>Attribute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="669"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="819"/>
        <location filename="templates/odfedit.cpp" line="831"/>
        <source>new comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="851"/>
        <source>The selected items do not have a common parent, this means I cannot wrap them without screwing up the DOM tree. Please check your selection!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="919"/>
        <location filename="templates/odfedit.cpp" line="935"/>
        <source>There is nothing in the clipboard. Please copy a node first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/odfedit.cpp" line="1000"/>
        <source>Sorry, this kinde of node cannot be copied.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrderAuditDialog</name>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="109"/>
        <location filename="dialogs/orderauditdlg.cpp" line="113"/>
        <location filename="dialogs/orderauditdlg.cpp" line="394"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="109"/>
        <source>Error while retrieving audit data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="113"/>
        <source>Sorry, no audit data available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="123"/>
        <source>Order Audit [%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="284"/>
        <location filename="dialogs/orderauditdlg.cpp" line="341"/>
        <source>No Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="284"/>
        <source>No audit data found for this ticket.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="292"/>
        <source>Ticket Audit: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="306"/>
        <location filename="dialogs/orderauditdlg.cpp" line="362"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="312"/>
        <location filename="dialogs/orderauditdlg.cpp" line="368"/>
        <source>Date/Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="313"/>
        <location filename="dialogs/orderauditdlg.cpp" line="369"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="314"/>
        <location filename="dialogs/orderauditdlg.cpp" line="370"/>
        <source>Transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="315"/>
        <location filename="dialogs/orderauditdlg.cpp" line="372"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="316"/>
        <source>Event Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="317"/>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="341"/>
        <source>No audit data found for this voucher.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="348"/>
        <source>Voucher Audit: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="371"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="373"/>
        <source>Validity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="374"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="394"/>
        <source>Unable to get user audit data: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrderItemView</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="994"/>
        <source>Preview Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1002"/>
        <source>Ticket: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1004"/>
        <source>Voucher: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrderWindow</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="106"/>
        <source>Order Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="111"/>
        <source>&amp;Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="112"/>
        <source>&amp;Order...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="113"/>
        <source>C&amp;ancel Order...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="114"/>
        <source>&amp;Mark Order as Shipped...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="116"/>
        <source>Change Item &amp;Price...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="118"/>
        <source>C&amp;hange Ticket Price Category...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="120"/>
        <source>Change Voucher Validity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="125"/>
        <source>Add Commen&amp;t...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="128"/>
        <source>Change C&amp;omments...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="129"/>
        <source>Change Sh&amp;ipping Method...</source>
        <oldsource>Change Commen&amp;t...</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="131"/>
        <source>Change Invoice Address...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="133"/>
        <source>Change Delivery Address...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="138"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="140"/>
        <source>&amp;Payment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="141"/>
        <source>Receive &amp;Payment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="142"/>
        <source>&amp;Refund...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="145"/>
        <source>P&amp;rinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="146"/>
        <source>Print &amp;Bill...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="147"/>
        <source>Save Bill &amp;as file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="149"/>
        <source>Print &amp;Tickets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="157"/>
        <source>Ticket History...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="171"/>
        <source>Order ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="175"/>
        <source>Order Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="177"/>
        <source>Shipping Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="179"/>
        <source>Customer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="185"/>
        <source>Delivery Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="181"/>
        <source>Invoice Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="135"/>
        <source>Customer Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="154"/>
        <source>&amp;Audit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="155"/>
        <source>Voucher History...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="159"/>
        <source>Order History...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="197"/>
        <source>Coupon:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="199"/>
        <source>Sold by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="201"/>
        <source>Order Comments:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="245"/>
        <source>(none)</source>
        <comment>no coupon</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Item ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="262"/>
        <source>Date/Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="277"/>
        <source>Begin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="284"/>
        <source>Voucher (current value: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="285"/>
        <source>Valid till: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="294"/>
        <source>%1x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="366"/>
        <source>There are no tickets left to print.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="372"/>
        <source>Unable to get template file (ticket.xtt). Giving up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="405"/>
        <source>There are no vouchers left to print.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="411"/>
        <source>Unable to get template file (voucher.xtt). Giving up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="443"/>
        <location filename="dialogs/orderwin.cpp" line="468"/>
        <source>Unable to get template file (bill). Giving up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="449"/>
        <location filename="dialogs/orderwin.cpp" line="489"/>
        <location filename="dialogs/orderwin.cpp" line="788"/>
        <source>Mark as shipped?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="449"/>
        <location filename="dialogs/orderwin.cpp" line="489"/>
        <location filename="dialogs/orderwin.cpp" line="788"/>
        <source>Mark this order as shipped now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="477"/>
        <source>Open Document File (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="511"/>
        <source>Error while trying to pay: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="518"/>
        <source>Payment Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="519"/>
        <source>Payment successful, but only %1 was required, please hand back the remaining %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="530"/>
        <source>Enter Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="530"/>
        <source>Please enter the ID of the voucher you want to use:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="536"/>
        <source>Error while trying to pay with voucher &apos;%1&apos;: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="540"/>
        <source>Voucher Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="541"/>
        <source>Successfully paid order %1 with voucher &apos;%2&apos;.
Amount deducted: %3
Remaining value of this voucher: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="555"/>
        <source>Enter Refund</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="555"/>
        <source>Please enter the amount that will be refunded:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="561"/>
        <source>Error while trying to refund: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="586"/>
        <source>Enter Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="586"/>
        <source>Please enter the new price for the ticket:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="592"/>
        <location filename="dialogs/orderwin.cpp" line="662"/>
        <source>Error while attempting to change ticket price: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="598"/>
        <location filename="dialogs/orderwin.cpp" line="668"/>
        <source>Cannot change this item type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="622"/>
        <source>Error getting event, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="629"/>
        <source>Cannot select another price category - there are none left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="635"/>
        <source>Select Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="641"/>
        <source>Please chose a price category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="653"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="683"/>
        <source>Cannot return this item type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="687"/>
        <source>Return Ticket or Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="687"/>
        <source>Do you really want to return this ticket or voucher?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="691"/>
        <source>Error whily trying to return item: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="708"/>
        <source>This is not a voucher.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="720"/>
        <source>Change Validity Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="723"/>
        <source>Changing validity date of voucher &apos;%1&apos;:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="724"/>
        <source>Current validity: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="726"/>
        <source>Indefinitely valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="731"/>
        <source>yyyy-MM-dd</source>
        <comment>Date format for editing voucher validity date</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="736"/>
        <source>Comment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="742"/>
        <source>&amp;Set Validity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="751"/>
        <source>Error whily trying to change voucher validity: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="760"/>
        <source>Cancel Order?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="760"/>
        <source>Cancel this order now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="765"/>
        <source>Error while cancelling order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="779"/>
        <source>Error while changing order status: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="792"/>
        <source>Set shipping time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="798"/>
        <source>Enter the shipping time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="804"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="912"/>
        <source>No Delivery Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="917"/>
        <location filename="dialogs/orderwin.cpp" line="947"/>
        <source>Unable to set address, server error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="928"/>
        <source>Unable to get customer data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="942"/>
        <source>No Invoice Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="962"/>
        <source>The selected item is not a voucher.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="977"/>
        <source>The selected item is not a ticket.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="655"/>
        <location filename="dialogs/orderwin.cpp" line="806"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="813"/>
        <source>Error while marking order as shipped: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="826"/>
        <source>Change comments: order %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="837"/>
        <location filename="dialogs/orderwin.cpp" line="871"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="745"/>
        <location filename="dialogs/orderwin.cpp" line="839"/>
        <location filename="dialogs/orderwin.cpp" line="873"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="846"/>
        <location filename="dialogs/orderwin.cpp" line="880"/>
        <source>There was a problem uploading the new comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="858"/>
        <source>Add comment: order %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="899"/>
        <source>Error while changing shipping: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="193"/>
        <source>Total Price:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="195"/>
        <source>Already Paid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="173"/>
        <source>Order State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="366"/>
        <location filename="dialogs/orderwin.cpp" line="372"/>
        <location filename="dialogs/orderwin.cpp" line="405"/>
        <location filename="dialogs/orderwin.cpp" line="411"/>
        <location filename="dialogs/orderwin.cpp" line="443"/>
        <location filename="dialogs/orderwin.cpp" line="468"/>
        <location filename="dialogs/orderwin.cpp" line="511"/>
        <location filename="dialogs/orderwin.cpp" line="536"/>
        <location filename="dialogs/orderwin.cpp" line="561"/>
        <location filename="dialogs/orderwin.cpp" line="592"/>
        <location filename="dialogs/orderwin.cpp" line="598"/>
        <location filename="dialogs/orderwin.cpp" line="622"/>
        <location filename="dialogs/orderwin.cpp" line="629"/>
        <location filename="dialogs/orderwin.cpp" line="662"/>
        <location filename="dialogs/orderwin.cpp" line="668"/>
        <location filename="dialogs/orderwin.cpp" line="683"/>
        <location filename="dialogs/orderwin.cpp" line="691"/>
        <location filename="dialogs/orderwin.cpp" line="708"/>
        <location filename="dialogs/orderwin.cpp" line="751"/>
        <location filename="dialogs/orderwin.cpp" line="765"/>
        <location filename="dialogs/orderwin.cpp" line="779"/>
        <location filename="dialogs/orderwin.cpp" line="813"/>
        <location filename="dialogs/orderwin.cpp" line="846"/>
        <location filename="dialogs/orderwin.cpp" line="880"/>
        <location filename="dialogs/orderwin.cpp" line="899"/>
        <location filename="dialogs/orderwin.cpp" line="917"/>
        <location filename="dialogs/orderwin.cpp" line="928"/>
        <location filename="dialogs/orderwin.cpp" line="947"/>
        <location filename="dialogs/orderwin.cpp" line="962"/>
        <location filename="dialogs/orderwin.cpp" line="977"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="122"/>
        <source>&amp;Return Item...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="150"/>
        <source>Print V&amp;ouchers...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="151"/>
        <source>Print &amp;Current Item...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="152"/>
        <source>&amp;View Items...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="189"/>
        <source>Shipping Method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="191"/>
        <source>Shipping Costs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="143"/>
        <source>Pay with &amp;Voucher...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrdersByUserDlg</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="444"/>
        <source>Select User Criteria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="456"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="458"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="461"/>
        <source>My orders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="461"/>
        <source>look for my own orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="462"/>
        <source>User Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="465"/>
        <source>Maximum Age (days):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="468"/>
        <source>Limit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="469"/>
        <source>Orders that the user created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="470"/>
        <source>Include all orders the user touched</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrdersReport</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="520"/>
        <source>Order Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="530"/>
        <location filename="mwin/orderstab.cpp" line="796"/>
        <source>Sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="535"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="535"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="536"/>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="536"/>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="556"/>
        <source>Number Vouchers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Money</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Paid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="537"/>
        <source>Sum Due</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="541"/>
        <source>By Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="546"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Paid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="547"/>
        <location filename="mwin/orderstab.cpp" line="557"/>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Due</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="550"/>
        <location filename="mwin/orderstab.cpp" line="798"/>
        <source>By Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="555"/>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="561"/>
        <location filename="mwin/orderstab.cpp" line="800"/>
        <source>Financial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="566"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="567"/>
        <source>Moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="576"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="579"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="702"/>
        <source>Total Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="703"/>
        <source>Reserved Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="704"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="705"/>
        <source>Placed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="706"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="763"/>
        <source>Save Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="776"/>
        <location filename="mwin/orderstab.cpp" line="781"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="776"/>
        <source>Unable to create file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="781"/>
        <source>Ooops. Unable to create ZIP structure in file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="797"/>
        <source>Orders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOrdersTab</name>
    <message>
        <location filename="mwin/orderstab.cpp" line="73"/>
        <source>-select mode-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="74"/>
        <source>All Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="75"/>
        <source>Open Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="76"/>
        <source>Open Reservations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="77"/>
        <source>Outstanding Payments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="78"/>
        <source>Outstanding Refunds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="79"/>
        <source>Undelivered Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="81"/>
        <source>-search result-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="91"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="94"/>
        <source>Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="98"/>
        <source>Orders since...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="102"/>
        <source>Find by Ticket...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="105"/>
        <source>Find by Event...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="108"/>
        <source>Find by Customer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="111"/>
        <source>Find by User...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="114"/>
        <source>Find by Order ID...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="132"/>
        <source>&amp;Order List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="134"/>
        <source>Update Order &amp;List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="136"/>
        <source>Order &amp;Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="139"/>
        <source>Orders &amp;since...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="142"/>
        <source>Find by &amp;Ticket...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="144"/>
        <source>Find by &amp;Event...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="146"/>
        <source>Find by &amp;Customer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="148"/>
        <source>Find by &amp;User...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="150"/>
        <source>Find by &amp;Order ID...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="153"/>
        <source>Generate &amp;Report...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Paid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="200"/>
        <source>Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="233"/>
        <location filename="mwin/orderstab.cpp" line="276"/>
        <location filename="mwin/orderstab.cpp" line="300"/>
        <location filename="mwin/orderstab.cpp" line="317"/>
        <location filename="mwin/orderstab.cpp" line="321"/>
        <location filename="mwin/orderstab.cpp" line="342"/>
        <location filename="mwin/orderstab.cpp" line="371"/>
        <location filename="mwin/orderstab.cpp" line="393"/>
        <location filename="mwin/orderstab.cpp" line="397"/>
        <location filename="mwin/orderstab.cpp" line="416"/>
        <location filename="mwin/orderstab.cpp" line="424"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="233"/>
        <location filename="mwin/orderstab.cpp" line="276"/>
        <location filename="mwin/orderstab.cpp" line="342"/>
        <source>There was a problem retrieving the order list: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="249"/>
        <source>Select Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="253"/>
        <source>Please select the date and time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="256"/>
        <source>ddd MMMM d yyyy, h:mm ap</source>
        <comment>time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="300"/>
        <location filename="mwin/orderstab.cpp" line="393"/>
        <source>Error while retrieving order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="312"/>
        <source>Enter Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="312"/>
        <source>Please enter the ID of one of the tickets of the order you seek:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="317"/>
        <source>Error while searching for order: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="321"/>
        <source>Order for barcode &apos;%1&apos; not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="416"/>
        <location filename="mwin/orderstab.cpp" line="424"/>
        <source>Unable to get user orders, server error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="263"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="265"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="371"/>
        <source>Error while retrieving order list: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="388"/>
        <source>Enter Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="388"/>
        <source>Please enter the ID of the order you want to display:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/orderstab.cpp" line="397"/>
        <source>This order does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOverview</name>
    <message>
        <location filename="mwin/overview.cpp" line="90"/>
        <source>&amp;Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="91"/>
        <source>&amp;Re-Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="92"/>
        <source>Change my &amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="95"/>
        <source>&amp;Close Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="98"/>
        <source>&amp;Show all customers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="141"/>
        <source>&amp;Auto-Refresh settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="143"/>
        <source>&amp;Display settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="136"/>
        <source>&amp;Backup now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="163"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="167"/>
        <source>Shopping Cart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="179"/>
        <source>Order List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="185"/>
        <source>Entrance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="242"/>
        <location filename="mwin/overview.cpp" line="252"/>
        <location filename="mwin/overview.cpp" line="293"/>
        <location filename="mwin/overview.cpp" line="329"/>
        <location filename="mwin/overview.cpp" line="333"/>
        <location filename="mwin/overview.cpp" line="371"/>
        <location filename="mwin/overview.cpp" line="590"/>
        <location filename="mwin/overview.cpp" line="594"/>
        <location filename="mwin/overview.cpp" line="615"/>
        <location filename="mwin/overview.cpp" line="744"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="252"/>
        <source>Error setting password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="394"/>
        <source>Refresh Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="399"/>
        <source>Refresh Rate (minutes):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="405"/>
        <source>refresh &amp;event list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="407"/>
        <source>refresh &amp;user list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="409"/>
        <source>refresh &amp;host list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="363"/>
        <location filename="mwin/overview.cpp" line="418"/>
        <location filename="mwin/overview.cpp" line="473"/>
        <location filename="mwin/overview.cpp" line="551"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="365"/>
        <location filename="mwin/overview.cpp" line="420"/>
        <location filename="mwin/overview.cpp" line="475"/>
        <location filename="mwin/overview.cpp" line="553"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="117"/>
        <source>&amp;Edit Templates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="119"/>
        <source>&amp;Update Templates Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="111"/>
        <source>Edit &amp;Shipping Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="411"/>
        <source>refresh &amp;shipping list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="105"/>
        <source>&amp;Deduct from voucher...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="97"/>
        <source>&amp;Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="101"/>
        <source>Tickets and &amp;Vouchers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="102"/>
        <source>Return &amp;ticket or voucher...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="107"/>
        <source>&amp;Empty voucher...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="116"/>
        <source>&amp;Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="139"/>
        <source>&amp;Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="142"/>
        <source>&amp;Server Access settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="130"/>
        <source>&amp;Administration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="110"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="113"/>
        <source>Edit &amp;Payment Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="122"/>
        <source>&amp;ODF Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="124"/>
        <source>A&amp;udit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="125"/>
        <source>&amp;Order Audit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="126"/>
        <source>&amp;Ticket Audit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="127"/>
        <source>&amp;Voucher Audit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="128"/>
        <source>&amp;User Audit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="131"/>
        <source>&amp;User Administration...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="134"/>
        <source>Backup &amp;Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="140"/>
        <source>&amp;Change Language...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="144"/>
        <source>&amp;Label Printing settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="145"/>
        <source>&amp;OpenOffice settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="146"/>
        <source>&amp;Barcode Scanner settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="148"/>
        <source>&amp;Session Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="155"/>
        <source>Login</source>
        <translation type="unfinished">log into the server</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="156"/>
        <source>Getting data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="161"/>
        <source>Getting events...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="177"/>
        <source>Getting Orders...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="183"/>
        <source>Getting Entrance Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="288"/>
        <source>Return Ticket/Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="288"/>
        <source>Please enter the ticket or voucher ID to return:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="293"/>
        <source>This ticket/voucher cannot be returned: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="301"/>
        <source>Deduct from Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="304"/>
        <source>Using a voucher to pay outside the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="308"/>
        <source>Amount to deduct:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="310"/>
        <source>Reason for deducting:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="312"/>
        <source>Voucher ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="320"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="322"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="329"/>
        <source>Unable to deduct voucher: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="333"/>
        <source>Voucher does not contain enough money. Money left: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="336"/>
        <source>Deducted from Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="337"/>
        <source>Value taken from voucher: %1
Value remaining on voucher: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="347"/>
        <source>Invalidate Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="354"/>
        <source>Please enter a reason for invalidating the voucher.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="371"/>
        <source>Unable to invalidate voucher: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="374"/>
        <source>Invalidated Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="375"/>
        <source>The voucher &apos;%1&apos;has been invalidated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="445"/>
        <source>Server Access Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="449"/>
        <source>Request Timeout (seconds):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="455"/>
        <source>Log Level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="457"/>
        <source>No Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="458"/>
        <source>Minimal Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="459"/>
        <source>Medium Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="460"/>
        <source>Log Details on Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="461"/>
        <source>Always Log Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="503"/>
        <source>Display Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="508"/>
        <source>Event settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="511"/>
        <source>Maximum event age (days, 0=show all):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="611"/>
        <source>Backup</source>
        <translation type="unfinished">make backups of the server database</translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="611"/>
        <source>The backup was successful.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="615"/>
        <source>Cannot create backup file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="674"/>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="674"/>
        <source>Please enter the ID of the order you want to audit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="684"/>
        <source>Ticket ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="684"/>
        <source>Please enter the ID of the ticket you want to audit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="694"/>
        <source>Voucher ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="694"/>
        <source>Please enter the ID of the voucher you want to audit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="715"/>
        <source>Audit User</source>
        <comment>audit dialog</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="720"/>
        <location filename="mwin/overview.cpp" line="723"/>
        <source>User Name:</source>
        <comment>audit dialog</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="727"/>
        <source>Earliest Info</source>
        <comment>audit dialog</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="744"/>
        <source>Cannot query an empty user name.</source>
        <comment>audit dialog</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="242"/>
        <source>I was unable to renew the login at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="350"/>
        <source>This will invalidate/empty the voucher. It will no longer be usable afterwards, but will still have to be paid for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="351"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="355"/>
        <source>Voucher ID/Barcode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="357"/>
        <source>Please scan the barcode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="517"/>
        <source>Maximum order list age</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="520"/>
        <source>Age in days, 0=show all.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="521"/>
        <source>When showing all orders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="526"/>
        <source>When showing open orders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="531"/>
        <source>When searching by event:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="536"/>
        <source>When searching by customer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="541"/>
        <source>Default age when searching by date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="590"/>
        <source>Backup failed with error (%2): %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/overview.cpp" line="594"/>
        <source>Backup returned empty.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPCDEdit</name>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="123"/>
        <source>New Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="123"/>
        <source>Change Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="130"/>
        <source>Category Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="131"/>
        <source>Category Abbreviation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="133"/>
        <source>Formula:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="134"/>
        <source>Flags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="141"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="141"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="143"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="150"/>
        <source>Flags of price category &apos;%1&apos;:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPasswordChange</name>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="26"/>
        <source>Change my password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="28"/>
        <source>Reset password of user &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="32"/>
        <source>Old Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="36"/>
        <source>New Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="39"/>
        <source>Repeat Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="47"/>
        <source>Set Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/passwdchg.cpp" line="49"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPaymentDialog</name>
    <message>
        <location filename="dialogs/payedit.cpp" line="315"/>
        <source>Payment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="318"/>
        <source>Please enter the payment data below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="319"/>
        <source>Amount paid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="320"/>
        <source>Payment Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="322"/>
        <source>Data?:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="328"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="330"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="383"/>
        <location filename="dialogs/payedit.cpp" line="386"/>
        <source>(not required)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPaymentEditor</name>
    <message>
        <location filename="dialogs/payedit.cpp" line="50"/>
        <source>Edit Payment Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="64"/>
        <source>Default Payment Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="69"/>
        <source>Change Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="72"/>
        <source>Change Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="75"/>
        <source>Change Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="78"/>
        <source>Change Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="82"/>
        <source>Add Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="85"/>
        <source>Delete Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="93"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Data Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Data Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="101"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="121"/>
        <source>Payment Option Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="121"/>
        <source>Please select a new description for this payment option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="126"/>
        <location filename="dialogs/payedit.cpp" line="171"/>
        <location filename="dialogs/payedit.cpp" line="191"/>
        <location filename="dialogs/payedit.cpp" line="231"/>
        <location filename="dialogs/payedit.cpp" line="235"/>
        <location filename="dialogs/payedit.cpp" line="281"/>
        <location filename="dialogs/payedit.cpp" line="302"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="126"/>
        <location filename="dialogs/payedit.cpp" line="171"/>
        <location filename="dialogs/payedit.cpp" line="281"/>
        <source>Could not store the changes: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="144"/>
        <source>Change Payment Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="150"/>
        <location filename="dialogs/payedit.cpp" line="255"/>
        <source>Data Name (human readable):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="152"/>
        <location filename="dialogs/payedit.cpp" line="256"/>
        <source>Data Default (pattern):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="154"/>
        <source>Hint: %Y=year, %M=month, %D=day, %%=%-sign, %O=order ID, %U=user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="160"/>
        <location filename="dialogs/payedit.cpp" line="221"/>
        <location filename="dialogs/payedit.cpp" line="266"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="162"/>
        <location filename="dialogs/payedit.cpp" line="223"/>
        <location filename="dialogs/payedit.cpp" line="268"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="187"/>
        <source>Edit Flags of payment option &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="191"/>
        <source>Could not store the changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="203"/>
        <source>Set Default Payment Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="231"/>
        <source>The payment type &apos;%1&apos; has flags set, it may not be usable for every user, please consider removing those flags.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="235"/>
        <source>Unable to set the new default: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="246"/>
        <source>Create new Payment Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="252"/>
        <source>Payment Type Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="253"/>
        <source>[a-zA-Z-/_\(\),\.]{1,63}</source>
        <comment>payment type pattern - allow national chars!</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="254"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="257"/>
        <source>Hint: %Y=year, %M=month, %D=day, %H=hour(0..23), %h=hour(1..12), %m=minute, %a=AM/PM, %%=%-sign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="259"/>
        <source>Flags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="260"/>
        <source>Edit flags of the new payment option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="297"/>
        <source>Really Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="297"/>
        <source>Really delete payment option &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/payedit.cpp" line="302"/>
        <source>Unable to delete this option: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPriceCategoryDialog</name>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="33"/>
        <source>Select a Price Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="47"/>
        <source>New...</source>
        <comment>new price category</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="50"/>
        <source>Edit...</source>
        <comment>edit price category</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="54"/>
        <source>Select</source>
        <comment>select price category</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="56"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="59"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="81"/>
        <location filename="dialogs/pricecatdlg.cpp" line="106"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/pricecatdlg.cpp" line="81"/>
        <location filename="dialogs/pricecatdlg.cpp" line="106"/>
        <source>Error while creating new price category: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MRoleTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="618"/>
        <source>New Role...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="621"/>
        <source>Delete Role...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="625"/>
        <source>Change Description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="628"/>
        <source>Edit Flags...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="631"/>
        <source>Edit Rights...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="653"/>
        <source>Role Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="653"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="667"/>
        <source>Create New Role</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="667"/>
        <source>Please enter a role name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="674"/>
        <location filename="mwin/acltabs.cpp" line="692"/>
        <location filename="mwin/acltabs.cpp" line="723"/>
        <location filename="mwin/acltabs.cpp" line="728"/>
        <location filename="mwin/acltabs.cpp" line="748"/>
        <location filename="mwin/acltabs.cpp" line="760"/>
        <location filename="mwin/acltabs.cpp" line="765"/>
        <location filename="mwin/acltabs.cpp" line="791"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="674"/>
        <source>Error while trying to create role: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="688"/>
        <source>Delete this Role?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="688"/>
        <source>Really delete role &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="692"/>
        <source>Error while trying to delete role: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="707"/>
        <source>Edit Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="707"/>
        <source>Description of role %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="723"/>
        <location filename="mwin/acltabs.cpp" line="760"/>
        <source>Cannot retrieve role: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="728"/>
        <source>Cannot retrieve flag list: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="739"/>
        <source>Edit flags of role %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="748"/>
        <source>Error while setting flags: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="765"/>
        <source>Cannot retrieve right list: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="782"/>
        <source>Edit rights of role %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="791"/>
        <source>Error while setting rights: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MShippingChange</name>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1053"/>
        <source>Change Shipping Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1060"/>
        <source>Method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1062"/>
        <source>Price:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1070"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1072"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderwin.cpp" line="1079"/>
        <source>(None)</source>
        <comment>shipping method</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MShippingEditor</name>
    <message>
        <location filename="dialogs/shipping.cpp" line="41"/>
        <source>Edit Shipping Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="55"/>
        <source>Change Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="57"/>
        <source>Change Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="59"/>
        <source>Change Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="62"/>
        <source>Add Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="64"/>
        <source>Delete Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="71"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="73"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="81"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="100"/>
        <location filename="dialogs/shipping.cpp" line="156"/>
        <source>Shipping Option Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="100"/>
        <source>Please select a new description for this shipping option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="105"/>
        <location filename="dialogs/shipping.cpp" line="127"/>
        <location filename="dialogs/shipping.cpp" line="146"/>
        <location filename="dialogs/shipping.cpp" line="167"/>
        <location filename="dialogs/shipping.cpp" line="188"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="105"/>
        <location filename="dialogs/shipping.cpp" line="127"/>
        <source>Could not store the changes: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="122"/>
        <location filename="dialogs/shipping.cpp" line="159"/>
        <source>Shipping Option Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="122"/>
        <source>Please select a new price for this shipping option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="142"/>
        <source>Edit Flags of shipping option &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="146"/>
        <source>Could not store the changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="156"/>
        <source>Please select a new description for this new shipping option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="159"/>
        <source>Please select a new price for this new shipping option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="167"/>
        <source>Could not store the data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="182"/>
        <source>Really Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="182"/>
        <source>Really delete shipping option &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/shipping.cpp" line="188"/>
        <source>Unable to delete this option: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MTELabelDelegate</name>
    <message>
        <location filename="templates/ticketedit.cpp" line="610"/>
        <source>edged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="611"/>
        <source>smooth</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MTemplateChoice</name>
    <message>
        <location filename="templates/templatedlg.cpp" line="34"/>
        <source>Chose Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="38"/>
        <source>Please chose a variant of template %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="49"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MTemplateEditor</name>
    <message>
        <location filename="templates/templatedlg.cpp" line="77"/>
        <source>Edit Template Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="93"/>
        <source>&amp;Add Variant</source>
        <oldsource>Delete Variant</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="101"/>
        <source>&amp;Save Template...</source>
        <oldsource>Edit Template</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="90"/>
        <source>&amp;Update Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="95"/>
        <source>&amp;Delete Variant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="98"/>
        <source>Change &amp;Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="103"/>
        <source>&amp;Edit Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="110"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Template/Variant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Checksum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="125"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="188"/>
        <location filename="templates/templatedlg.cpp" line="208"/>
        <location filename="templates/templatedlg.cpp" line="227"/>
        <location filename="templates/templatedlg.cpp" line="244"/>
        <location filename="templates/templatedlg.cpp" line="289"/>
        <location filename="templates/templatedlg.cpp" line="309"/>
        <location filename="templates/templatedlg.cpp" line="347"/>
        <location filename="templates/templatedlg.cpp" line="356"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="188"/>
        <source>Unable to delete this template.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="202"/>
        <source>Select Template File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="208"/>
        <source>Files with this extension (%1) are not legal for this template.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="227"/>
        <source>Unable to upload file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="244"/>
        <source>Unable to send new description to server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="289"/>
        <source>Unknown template type, cannot edit it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="309"/>
        <source>Ooops. Lost the template file, cannot store it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="315"/>
        <source>Save template as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="328"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="328"/>
        <source>Unable to save the template file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="347"/>
        <source>Ooops. Lost the template file, cannot alter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="351"/>
        <source>Edit flags of template &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/templatedlg.cpp" line="356"/>
        <source>Unable to send updated flags to server.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MTicketEditor</name>
    <message>
        <location filename="templates/ticketedit.cpp" line="154"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="155"/>
        <source>&amp;Open File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="156"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="157"/>
        <source>Save &amp;as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="159"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="160"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="207"/>
        <source>Add Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="209"/>
        <source>Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="224"/>
        <source>Add File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="226"/>
        <source>Remove File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="151"/>
        <source>Label Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="155"/>
        <source>Ctrl+O</source>
        <comment>open file shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="156"/>
        <source>Ctrl+S</source>
        <comment>save file shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="161"/>
        <source>&amp;Add Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="162"/>
        <source>&amp;Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="164"/>
        <source>Add &amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="165"/>
        <source>Remove F&amp;ile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="167"/>
        <source>Add &amp;Text Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="168"/>
        <source>Add &amp;Picture Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="169"/>
        <source>Add &amp;Barcode Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="170"/>
        <source>Add &amp;Load Font Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="184"/>
        <source>Label Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="189"/>
        <source>Millimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="190"/>
        <source>Inch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="202"/>
        <source>Move up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="204"/>
        <source>Move down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="231"/>
        <source>As Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="234"/>
        <source>Zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="240"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="248"/>
        <source>Example Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="276"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="276"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="296"/>
        <location filename="templates/ticketedit.cpp" line="301"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="296"/>
        <source>Unable to open file &apos;%1&apos; for reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="301"/>
        <source>Unable to interpret file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="350"/>
        <source>top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="352"/>
        <source>bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="355"/>
        <source>center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="357"/>
        <source>left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="359"/>
        <source>right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="361"/>
        <source>align (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="388"/>
        <location filename="templates/ticketedit.cpp" line="446"/>
        <location filename="templates/ticketedit.cpp" line="820"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="388"/>
        <source>Unable to interpret template data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="427"/>
        <source>Open Ticket Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="446"/>
        <source>Unable to write to file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="454"/>
        <source>Save Ticket Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="465"/>
        <source>Label Template Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="467"/>
        <source>Label Template Editor [%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="479"/>
        <location filename="templates/ticketedit.cpp" line="827"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="479"/>
        <location filename="templates/ticketedit.cpp" line="492"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="490"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="491"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="493"/>
        <source>File/Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="494"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="495"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="496"/>
        <source>Horiz. Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="497"/>
        <source>Vert. Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="498"/>
        <source>Text Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="504"/>
        <source>Load Font File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="505"/>
        <source>Show Picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="506"/>
        <source>Show Text Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="507"/>
        <source>Show Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="508"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="523"/>
        <source>smooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="523"/>
        <source>edged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="814"/>
        <source>Add File to Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="820"/>
        <source>Unable to read file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="827"/>
        <source>Please enter the internal file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="844"/>
        <source>Really delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templates/ticketedit.cpp" line="844"/>
        <source>Really remove file &apos;%1&apos; from the label?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MUserAuditDialog</name>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="415"/>
        <source>User Audit: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="427"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="432"/>
        <source>Orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="439"/>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="449"/>
        <location filename="dialogs/orderauditdlg.cpp" line="474"/>
        <location filename="dialogs/orderauditdlg.cpp" line="502"/>
        <source>Show Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="451"/>
        <location filename="dialogs/orderauditdlg.cpp" line="476"/>
        <location filename="dialogs/orderauditdlg.cpp" line="504"/>
        <source>Audit Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="456"/>
        <source>Tickets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="463"/>
        <source>Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="479"/>
        <source>Audit Ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="484"/>
        <source>Vouchers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="491"/>
        <source>Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="507"/>
        <source>Audit Voucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="524"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/orderauditdlg.cpp" line="524"/>
        <source>Unable to retrieve order: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MUserTab</name>
    <message>
        <location filename="mwin/acltabs.cpp" line="77"/>
        <source>New User...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="80"/>
        <source>Delete User...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="84"/>
        <source>Description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="87"/>
        <source>Hosts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="90"/>
        <source>Roles...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="93"/>
        <source>Flags...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="96"/>
        <source>Set Password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="122"/>
        <source>Login Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="122"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="136"/>
        <source>New User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="136"/>
        <source>Please enter new user name (only letters, digits, and underscore allowed):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="141"/>
        <location filename="mwin/acltabs.cpp" line="172"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="141"/>
        <source>The user name must contain only letters, digits, dots and underscores and must be at least one character long!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="145"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="145"/>
        <source>Please enter an initial password for the user:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="160"/>
        <source>Delete User?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="160"/>
        <source>Really delete user &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="164"/>
        <source>(Nobody)</source>
        <comment>this is a username for no user, the string must contain &apos;(&apos; to distinguish it from the others</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="167"/>
        <source>Delete User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="167"/>
        <source>Select which user will inherit this users database objects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="172"/>
        <source>Cannot delete user: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="186"/>
        <source>Edit Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="186"/>
        <source>Description of user %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="203"/>
        <location filename="mwin/acltabs.cpp" line="208"/>
        <location filename="mwin/acltabs.cpp" line="228"/>
        <location filename="mwin/acltabs.cpp" line="241"/>
        <location filename="mwin/acltabs.cpp" line="246"/>
        <location filename="mwin/acltabs.cpp" line="265"/>
        <location filename="mwin/acltabs.cpp" line="280"/>
        <location filename="mwin/acltabs.cpp" line="286"/>
        <location filename="mwin/acltabs.cpp" line="300"/>
        <location filename="mwin/acltabs.cpp" line="305"/>
        <location filename="mwin/acltabs.cpp" line="325"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="203"/>
        <source>Cannot retrieve user roles: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="208"/>
        <source>Cannot retrieve role descriptions: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="219"/>
        <source>Edit Roles of user %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="228"/>
        <source>Error while setting users roles: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="241"/>
        <source>Cannot retrieve users hosts: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="246"/>
        <source>Cannot retrieve host descriptions: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="256"/>
        <source>Edit hosts of user %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="265"/>
        <source>Error while setting users hosts: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="280"/>
        <source>The password must be non-empty and both lines must match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="286"/>
        <source>Error while setting password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="300"/>
        <source>Cannot retrieve user data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="305"/>
        <source>Cannot retrieve flag list: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="316"/>
        <source>Edit flags of user %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mwin/acltabs.cpp" line="325"/>
        <source>Error while setting users flags: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
