//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_CARTTAB_H
#define MAGICSMOKE_CARTTAB_H

#include <QDateTime>
#include <QDialog>
#include <QItemDelegate>
#include <QMainWindow>
#include <QTimer>

#include "MOCustomer"
#include "MOCoupon"

class QAction;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class QStandardItemModel;
class QTabWidget;
class QTableView;
class QTextEdit;

class MSInterface;

class MOCartOrder;
class MOCartItem;
class MOCartTicket;
class MOCartVoucher;
class MOOrder;
class MOVoucher;

/**Main Overview Window: cart tab*/
class MCartTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the window with QSettings-key for current profile*/
		MCartTab(QString);
		
		/**creates the menu for this tab*/
		QList<QMenu*>menu();
		
		/**destructs the tab*/
		~MCartTab();
	
	signals:
		/**must be connected to the slot in the event tab: returns the currently selected event ID*/
		int currentEventId();
		/**must be connected to the slot in the event tab: returns the currently selected event title*/
		QString currentEventTitle();
		/**must be connected to the slot in the event tab: returns the currently selected event start time (as string)*/
		QString currentEventStart();
		
		/**emitted when the cart tab wants focus because something important changed*/
		void requestFocus();
		
		///select an event
		void getEventId(int&,bool&);
		
	public slots:
		/**order ticket from event tab*/
		void eventOrderTicket();
		/**order ticket from event tab*/
		void eventOrderTicket(qint64,qint64);
		
		/**update shipping info*/
		void updateShipping();
		
	private slots:
		/**decide what customer to use*/
		void setCustomer();
		/**decide about delivery address*/
		void setDeliveryAddr();
		/**decide about invoice address*/
		void setInvoiceAddr();
		/**initialize cart tab*/
		void initCart();
		/**add a ticket to the cart*/
		void cartAddTicket();
		/**add a voucher to the cart*/
		void cartAddVoucher();
		/**add a product item to the cart*/
		void cartAddItem();
		/**remove item from the cart*/
		void cartRemoveItem();
		/**create the order on the server*/
		void cartOrder(bool isreserve=false,bool issale=false);
		/**create a reservation on the server*/
		void cartReserve();
		/**create a sale on the server*/
		void cartSell();
		/**reset the colors of the tab*/
		void resetColor(bool addronly=false);
		/**update the price shown*/
		void updatePrice();
		///remove/add a voucher from the payment area
		void changeVoucher(QString url);
		
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*carttable;
		QStandardItemModel*cartmodel;
		QLabel*cartcustomer,*deliveryaddr,*invoiceaddr,*price,*coupon,*vouchers,*priceremain;
		QTextEdit *cartcomment;
		QComboBox*cartship;
		//cart
		MOCustomer customer;
		qint64 deliveryaddrid,invoiceaddrid;
		MOCoupon couponinfo;
		QList<QPair<QString,int>> vouchersforpay;
		
		/**helper for order: returns true if an order can be sent*/
		bool canorder(bool isreserve);
		/**helper for order: turns the cart table into order items*/
		void cartTableToOrder(MOCartOrder&);
		/**helper for order: makes the actual order or reserve; returns the result in the first two parameters; returns false if the transaction was aborted and processing should stop here*/
		bool orderExecute(MOCartOrder&cart,MOOrder&order,bool isreserve,bool issale);
		/**helper for order: verifies customer properties*/
		void verifyOrderCustomer(MOCartOrder&);
		/**helper for order: verifies tickets*/
		void verifyOrderTickets(const QList<MOCartTicket>&);
		/**helper for order: verifies vouchers*/
		void verifyOrderVouchers(const QList<MOCartVoucher>&);
		/**helper for order: verifies product items*/
		void verifyOrderItems(const QList<MOCartItem>&);
		
		/**helper for cartAddTicket and eventOrderTicket: choses a price category and then adds the ticket*/
		void addTicketForEvent(qint64,qint64 prcid=-1);

		///helper to apply coupon rules
		void applyCoupon();

		///helper to display vouchers after a sale/order
		void displayUsedVouchers(const QList<MOVoucher>&,int paidcash);
};

/**Helper class for shopping cart: allow editing amount, but nothing else*/
class MCartTableDelegate:public QItemDelegate
{
	public:
		MCartTableDelegate(QObject *parent = 0);
		
		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
				      const QModelIndex &index) const;
		
		void setEditorData(QWidget *editor, const QModelIndex &index) const;
		void setModelData(QWidget *editor, QAbstractItemModel *model,
				  const QModelIndex &index) const;
};

#endif
