//
// C++ Interface: acldialog
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ACLWIN_H
#define MAGICSMOKE_ACLWIN_H

#include <QMainWindow>
#include <QPointer>
#include <QTimer>

#include "tabwin.h"
#include "smokeexport.h"

class MUserTab;
class MHostTab;
class MRoleTab;
class MFlagTab;

class MAGICSMOKE_EXPORT MAclWindow:public MTabWin
{
	Q_OBJECT
	public:
		static void showWindow(QWidget*);
		
	private slots:
		void refreshData();
	private:
		MAclWindow(QWidget*);
		
		static QPointer<MAclWindow>instance;
	
		//widgets
		MUserTab*usertab;
		MHostTab*hosttab;
		MRoleTab*roletab;
		MFlagTab*flagtab;
		//refresh timers
		QTimer rtimer;
};

#endif
