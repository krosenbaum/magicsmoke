//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "centbox.h"
#include "main.h"
#include "misc.h"
#include "msinterface.h"
#include "orderwin.h"

#include "customerdlg.h"

#include "orderstab.h"

#include <TimeStamp>

#include <Zip>

#include "MTGetOrder"
#include "MTGetOrderList"
#include "MTGetMyOrders"
#include "MTGetOrderByBarcode"
#include "MTGetOrdersByCustomer"
#include "MTGetOrdersByEvents"
#include "MTGetOrdersByUser"
#include "MTGetAllUsers"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QFormLayout>
#include <QInputDialog>
#include <QLabel>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QTableView>
#include <QFileDialog>

#define ORDERNONE	0
#define ORDERALL	0xff
#define ORDEROPEN	0xf
#define ORDERPAY	1
#define ORDERREFUND	2
#define ORDERUNSENT	4
#define ORDERRESERVE	8

MOrdersTab::MOrdersTab(QString pk)
{
	profilekey=pk;
	
	QVBoxLayout*vl;QHBoxLayout*hl;
	QPushButton*p;
	//Order List Tab
	setLayout(hl=new QHBoxLayout);
	hl->addLayout(vl=new QVBoxLayout,10);
	vl->addWidget(ordermode=new QComboBox,0);
	ordermode->addItem(tr("-select mode-"),ORDERNONE);
	ordermode->addItem(tr("All Orders"),ORDERALL);
	ordermode->addItem(tr("Open Orders"),ORDEROPEN);
	ordermode->addItem(tr("Open Reservations"),ORDERRESERVE);
	ordermode->addItem(tr("Outstanding Payments"),ORDERPAY);
	ordermode->addItem(tr("Outstanding Refunds"),ORDERREFUND);
	ordermode->addItem(tr("Undelivered Orders"),ORDERUNSENT);
	//make sure this entry is the last one, since we use count()-1 to select it
	ordermode->addItem(tr("-search result-"),ORDERNONE);
	connect(ordermode,SIGNAL(currentIndexChanged(int)),this,SLOT(updateOrders()));
	vl->addWidget(ordertable=new QTableView);
	ordertable->setModel(ordermodel=new QStandardItemModel(this));
	ordertable->setSelectionMode(QAbstractItemView::SingleSelection);
        ordertable->setSelectionBehavior(QAbstractItemView::SelectRows);
	ordertable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ordertable->setSortingEnabled(true);
	connect(ordertable,SIGNAL(doubleClicked(const QModelIndex&)),this,SLOT(orderDetails()));
	hl->addLayout(vl=new QVBoxLayout,0);
	vl->addWidget(p=new QPushButton(tr("Update")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(updateOrders()));
	vl->addSpacing(15);
	vl->addWidget(p=new QPushButton(tr("Details...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderDetails()));
	p->setEnabled(req->hasRight(req->RGetOrder));
	vl->addSpacing(15);
	vl->addWidget(p=new QPushButton(tr("Orders since...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderSinceDate()));
	p->setEnabled(req->hasRight(req->RGetOrderList));
	vl->addSpacing(15);
	vl->addWidget(p=new QPushButton(tr("Find by Ticket...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderByTicket()));
	p->setEnabled(req->hasRight(req->RGetOrderByBarcode));
	vl->addWidget(p=new QPushButton(tr("Find by Event...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderByEvent()));
	p->setEnabled(req->hasRight(req->RGetOrdersByEvents));
	vl->addWidget(p=new QPushButton(tr("Find by Customer...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderByCustomer()));
	p->setEnabled(req->hasRight(req->RGetOrdersByCustomer));
	vl->addWidget(p=new QPushButton(tr("Find by User...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderByUser()));
	p->setEnabled(req->hasRight(req->RGetOrdersByUser) || req->hasRight(req->RGetMyOrders));
	vl->addWidget(p=new QPushButton(tr("Find by Order ID...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(orderByOrder()));
	p->setEnabled(req->hasRight(req->RGetOrder));
	vl->addStretch(10);
	
	
	//fill tables
	if(req->hasRight(req->RGetOrderList)){
		updateOrders();
	}else{
		setEnabled(false);
	}
}

QList< QMenu* > MOrdersTab::menu()
{
        //menu
        QList<QMenu*>ret;
        QMenu*m=new QMenu(tr("&Order List"));
        ret<<m;
        m->addAction(tr("Update Order &List"),this,SLOT(updateOrders()));
        m->addSeparator();
        m->addAction(tr("Order &Details..."),this,SLOT(orderDetails()))
         ->setEnabled(req->hasRight(req->RGetOrder));
        m->addSeparator();
        m->addAction(tr("Orders &since..."),this,SLOT(orderSinceDate()))
         ->setEnabled(req->hasRight(req->RGetOrderList));
        m->addSeparator();
        m->addAction(tr("Find by &Ticket..."),this,SLOT(orderByTicket()))
         ->setEnabled(req->hasRight(req->RGetOrderByBarcode));
        m->addAction(tr("Find by &Event..."),this,SLOT(orderByEvent()))
         ->setEnabled(req->hasRight(req->RGetOrdersByEvents));
        m->addAction(tr("Find by &Customer..."),this,SLOT(orderByCustomer()))
         ->setEnabled(req->hasRight(req->RGetOrdersByCustomer));
        m->addAction(tr("Find by &User..."),this,SLOT(orderByUser()))
         ->setEnabled(req->hasRight(req->RGetOrdersByUser) || req->hasRight(req->RGetMyOrders));
        m->addAction(tr("Find by &Order ID..."),this,SLOT(orderByOrder()))
         ->setEnabled(req->hasRight(req->RGetOrder));
        m->addSeparator();
        m->addAction(tr("Generate &Report..."),this,SLOT(genReport()));
        //return menu
        return ret;
}

qint64 MOrdersTab::oldestOrderStamp(OrderTimeStampMode mode,int age)
{
	if(age<0){//age not given, get configuration
		QSettings set;
		set.beginGroup("profiles/"+profilekey);
		switch(mode){
			case OldestForAll:age=set.value("maxorderage",0).toInt();break;
			case OldestForOpen:age=set.value("maxorderageopen",0).toInt();break;
			case OldestForEvent:age=set.value("maxorderageevent",0).toInt();break;
			case OldestForUser://currently we re-use the customer setting
			case OldestForCustomer:age=set.value("maxorderagecust",0).toInt();break;
			case OldestForSince:age=set.value("deforderagesince",7).toInt();break;
			default: age=0;break;
		}
	}
	//zero means no limit
	if(age<=0)return 0;
	//calculate
	TimeStamp ts=TimeStamp::now();ts.addDays(-age);
	return ts.toUnix();
}

//helper: finds out whether an order should be printed.
static inline bool candoUpdateOrders(int omode,const MOOrderInfo&ord)
{
	//show all?
	if(omode==ORDERALL)return true;
	//select by mask: outstanding payments and refunds
	if((omode&ORDERPAY)!=0 && ord.needsPayment())return true;
	if((omode&ORDERREFUND)!=0 && ord.needsRefund())return true;
	//do not show cancelled ones per default
	if(ord.isCancelled())return false;
	//select by mask: not yet sent and reserved...
	if((omode&ORDERUNSENT)!=0 && !ord.isSent() && !ord.isReservation())return true;
	if((omode&ORDERRESERVE)!=0 && ord.isReservation())return true;
	//no match: ignore the entry
	return false;
}

void MOrdersTab::resetModel()
{
	ordermodel->clear();
	ordermodel->setHorizontalHeaderLabels(QStringList()<<tr("Status")<<tr("Total")<<tr("Paid")<<tr("Customer"));
        m_orders.clear();
}

void MOrdersTab::addOrderToModel(const MOOrderInfo&ord,const QList<MOCustomerInfo>&cust)
{
        m_orders.append(ord);
	int cl=0;
	ordermodel->insertRow(cl);
	ordermodel->setHeaderData(cl,Qt::Vertical,ord.orderid().value());
	ordermodel->setData(ordermodel->index(cl,0),ord.orderStatusString());
	ordermodel->setData(ordermodel->index(cl,0),ord.orderid().value(),Qt::UserRole);
	ordermodel->setData(ordermodel->index(cl,1),ord.totalPriceString());
	ordermodel->setData(ordermodel->index(cl,2),ord.amountPaidString());
	if(ord.amountpaid()<ord.totalprice())
		ordermodel->setData(ordermodel->index(cl,2),QColor("#ff8080"),Qt::BackgroundRole);
	if(ord.amountpaid()>ord.totalprice())
		ordermodel->setData(ordermodel->index(cl,2),QColor("#80ff80"),Qt::BackgroundRole);
	int cid=ord.customerid();
	//TODO: make this more effective:
	for(int j=0;j<cust.size();j++)
		if(cust[j].customerid().value()==cid)
			ordermodel->setData(ordermodel->index(cl,3),cust[j].fullName());
	cl++;
}

void MOrdersTab::updateOrders()
{
	int omode=ordermode->itemData(ordermode->currentIndex()).toInt();
	if(omode==ORDERNONE)return;
	resetModel();
	MTGetOrderList ol=req->queryGetOrderList(oldestOrderStamp(omode==ORDERALL?OldestForAll:OldestForOpen));
	if(ol.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("There was a problem retrieving the order list: %1").arg(ol.errorString()));
		return;
	}
	QList<MOOrderInfo> orders=ol.getorders();
	QList<MOCustomerInfo> cust=ol.getcustomers();
	for(int i=0;i<orders.size();i++){
		if(!candoUpdateOrders(omode,orders[i]))continue;
		addOrderToModel(orders[i],cust);
	}
	ordertable->resizeColumnsToContents();
}

void MOrdersTab::orderSinceDate()
{
	//display selection dialog
	QDialog d(this);
	d.setWindowTitle(tr("Select Date"));
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(new QLabel(tr("Please select the date and time:")));
	QDateTimeEdit*dte;
	vl->addWidget(dte=new QDateTimeEdit());
	dte->setDisplayFormat(tr("ddd MMMM d yyyy, h:mm ap","time format"));
	dte->setDateTime(TimeStamp(oldestOrderStamp(OldestForSince)).toDateTime());
	dte->setCalendarPopup(true);
	vl->addSpacing(10);vl->addStretch(1);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//wait for user
	if(d.exec()!=QDialog::Accepted)
		return;
	//get selection
	qint64 ts=dte->dateTime().toTime_t();
	//request data and display
	resetModel();
	MTGetOrderList ol=req->queryGetOrderList(ts);
	if(ol.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("There was a problem retrieving the order list: %1").arg(ol.errorString()));
		return;
	}
	QList<MOOrderInfo> orders=ol.getorders();
	QList<MOCustomerInfo> cust=ol.getcustomers();
	for(int i=0;i<orders.size();i++){
		addOrderToModel(orders[i],cust);
	}
	ordertable->resizeColumnsToContents();
	ordermode->setCurrentIndex(ordermode->count()-1);
}

void MOrdersTab::orderDetails()
{
	//get selected order
	int id;
	QModelIndexList ilst=ordertable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=ordermodel->index(ilst[0].row(),0);
	id=ordermodel->data(idx,Qt::UserRole).toInt();
	if(id<0)return;
	//open order window
	MTGetOrder go=req->queryGetOrder(id);
	if(go.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while retrieving order: %1").arg(go.errorString()));
		return;
	}
	MOrderWindow *om=new MOrderWindow(this,go.getorder().value());
	om->setAttribute(Qt::WA_DeleteOnClose);
	om->show();
}

void MOrdersTab::orderByTicket()
{
	//get selected order
	bool ok;
	QString tid=QInputDialog::getText(this,tr("Enter Ticket"),tr("Please enter the ID of one of the tickets of the order you seek:"),QLineEdit::Normal,"",&ok);
	if(!ok || tid=="")return;
	//request it
	MTGetOrderByBarcode obb=req->queryGetOrderByBarcode(tid);
	if(obb.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while searching for order: %1").arg(obb.errorString()));
		return;
	}
	if(obb.getorder().isNull()){
		QMessageBox::warning(this,tr("Warning"),tr("Order for barcode '%1' not found.").arg(tid));
		return;
	}
	//open order window
	MOrderWindow *om=new MOrderWindow(this,obb.getorder().value());
	om->setAttribute(Qt::WA_DeleteOnClose);
	om->show();
}

void MOrdersTab::orderByEvent()
{
	//get events
	QList<qint64>eventids;
	QList<int>eids;
	selectEventIds(eids);
	if(eids.size()<1)return;
	for(int id:eids)eventids.append(id);
	//request data and display
	resetModel();
	MTGetOrdersByEvents obe=req->queryGetOrdersByEvents(eventids,oldestOrderStamp(OldestForEvent));
	if(obe.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("There was a problem retrieving the order list: %1").arg(obe.errorString()));
		return;
	}
	QList<MOOrderInfo> orders=obe.getorders();
	QList<MOCustomerInfo> cust=obe.getcustomers();
	for(int cl=0;cl<orders.size();cl++){
		addOrderToModel(orders[cl],cust);
	}
	ordermode->setCurrentIndex(ordermode->count()-1);
	ordertable->resizeColumnsToContents();
}

void MOrdersTab::orderByCustomer()
{
	//display selection dialog
	MCustomerListDialog mcl(this,true);
	//wait for user
	if(mcl.exec()!=QDialog::Accepted)
		return;
	//get selection
	MOCustomerInfo cst=mcl.getCustomer();
	if(cst.customerid().isNull()){
		qDebug("nothing selected");
		return;
	}
	//request data and display
	resetModel();
	MTGetOrdersByCustomer obc=req->queryGetOrdersByCustomer(cst.customerid(), oldestOrderStamp(OldestForCustomer));
	if(obc.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while retrieving order list: %1").arg(obc.errorString()));
		return;
	}
	QList<MOOrderInfo> orders=obc.getorders();
	if(orders.size()==0)return;
	QList<MOCustomerInfo> cust;cust<<cst;
	for(int i=0;i<orders.size();i++){
		addOrderToModel(orders[i],cust);
	}
	ordermode->setCurrentIndex(ordermode->count()-1);
	ordertable->resizeColumnsToContents();
}

void MOrdersTab::orderByOrder()
{
	//ask for OrderID
	bool ok;
	int oid=QInputDialog::getInt(this,tr("Enter Order ID"),tr("Please enter the ID of the order you want to display:"),0,0,2147483647,1,&ok);
	if(!ok)return;
	//display
	MTGetOrder go=req->queryGetOrder(oid);
	if(go.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while retrieving order: %1").arg(go.errorString()));
		return;
	}
	if(go.getorder().isNull()){
		QMessageBox::warning(this,tr("Warning"),tr("This order does not exist."));
		return;
	}
	MOrderWindow *om=new MOrderWindow(this,go.getorder().value());
	om->setAttribute(Qt::WA_DeleteOnClose);
	om->show();
}

void MOrdersTab::orderByUser()
{
	MOrdersByUserDlg d(oldestOrderStamp(OldestForUser),this);
	if(d.exec()!=QDialog::Accepted)return;
	resetModel();
// 	qDebug()<<"searching: my"<<d.mySelfOnly()<<"user"<<d.userName()<<"incall"<<d.includeAll()<<"old"<<d.oldest();
	QList<MOOrderInfo>orders;
	QList<MOCustomerInfo>cust;
	if(d.mySelfOnly()){
		MTGetMyOrders gmo=req->queryGetMyOrders(d.oldest(),d.includeAll());
		if(gmo.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Unable to get user orders, server error: %1").arg(gmo.errorString()));
			return;
		}
		orders=gmo.getorders();
		cust=gmo.getcustomers();
	}else{
		MTGetOrdersByUser obu=req->queryGetOrdersByUser(d.oldest(),d.includeAll(),d.userName());
		if(obu.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Unable to get user orders, server error: %1").arg(obu.errorString()));
			return;
		}
		orders=obu.getorders();
		cust=obu.getcustomers();
	}
	for(int i=0;i<orders.size();i++){
		addOrderToModel(orders[i],cust);
	}
	ordermode->setCurrentIndex(ordermode->count()-1);
	ordertable->resizeColumnsToContents();
}

void MOrdersTab::genReport()
{
        MOrdersReport(m_orders,this).exec();
}

MOrdersByUserDlg::MOrdersByUserDlg(int olddef,QWidget* parent, Qt::WindowFlags f): QDialog(parent, f)
{
	setWindowTitle(tr("Select User Criteria"));
	//layout
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);
	vl->addStretch(1);
	//buttons
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),this,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	//form
	fl->addRow(tr("My orders:"),m_myself=new QCheckBox(tr("look for my own orders")));
	fl->addRow(tr("User Name:"),m_uname=new QComboBox);
	m_uname->setEditable(true);
	connect(m_myself,SIGNAL(toggled(bool)),m_uname,SLOT(setDisabled(bool)));
	fl->addRow(tr("Maximum Age (days):"),m_days=new QSpinBox);
	m_days->setRange(0,9999);
	m_days->setValue(olddef);
	fl->addRow(tr("Limit:"),m_incall=new QComboBox);
        m_incall->addItem(tr("Orders that the user created"),false);
        m_incall->addItem(tr("Include all orders the user touched"),true);
	//init and preselect
	if(req->hasRight(req->RGetOrdersByUser)){
		//try to get all user names
		if(req->hasRight(req->RGetAllUsers)){
			MTGetAllUsers gau=req->queryGetAllUsers();
			if(!gau.hasError()){
				QList<MOUser>ul=gau.getusers();
				foreach(MOUser u,ul){
					m_uname->addItem(u.name());
				}
			}
		}
		//this would be odd, but it is possible
		if(!req->hasRight(req->RGetMyOrders))
			m_myself->setEnabled(false);
	}else{
		m_myself->setChecked(true);
		m_myself->setEnabled(false);
	}
	//use own name as preselection
	m_uname->setEditText(req->currentUser());
}

bool MOrdersByUserDlg::includeAll() const
{
	return m_incall->itemData(m_incall->currentIndex()).toBool();
}

bool MOrdersByUserDlg::mySelfOnly() const
{
	return m_myself->isChecked();
}

QString MOrdersByUserDlg::userName() const
{
	return m_uname->currentText();
}

qint64 MOrdersByUserDlg::oldest() const
{
	qint64 d=m_days->value();
	if(d<=0)return 0;
	return QDateTime::currentDateTime().toTime_t() - (d*86400);
}

MOrdersReport::MOrdersReport(const QList< MOOrderInfo >& aorders, QWidget* parent, Qt::WindowFlags f)
        : QDialog(parent, f), orders(aorders)
{
        setSizeGripEnabled(true);
        setWindowTitle(tr("Order Report"));
        
        QVBoxLayout*vl;
        QHBoxLayout*hl;
        QTabWidget*tab;
        setLayout(vl=new QVBoxLayout);
        
        vl->addWidget(tab=new QTabWidget,1);
        
        //tab: sums ( #orders, # tickets, #vouchers, $money, $paid, $for tickets, ...)
        tab->addTab(stable=new QTableView,tr("Sums"));
        stable->setModel(sum=new QStandardItemModel);
        stable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        stable->setSelectionMode(QAbstractItemView::NoSelection);
        sum->setHorizontalHeaderLabels(QStringList()
          <<tr("Category")<<tr("Number Orders")
          <<tr("Number Tickets")<<tr("Number Vouchers")
          <<tr("Sum Money")<<tr("Sum Paid")<<tr("Sum Due")
        );
        
        //tab: by order (status, money in, money paid, tickets, vouchers)
        tab->addTab(otable=new QTableView,tr("By Order"));
        otable->setModel(byord=new QStandardItemModel);
        otable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        otable->setSelectionMode(QAbstractItemView::NoSelection);
        byord->setHorizontalHeaderLabels(QStringList()
          <<tr("Order ID")<<tr("State")<<tr("Number Tickets")<<tr("Number Vouchers")
          <<tr("Price")<<tr("Paid")<<tr("Due") );

        //tab: by day
        tab->addTab(dtable=new QTableView,tr("By Day"));
        dtable->setModel(byday=new QStandardItemModel);
        dtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        dtable->setSelectionMode(QAbstractItemView::NoSelection);
        byday->setHorizontalHeaderLabels(QStringList()
          <<tr("Date")
          <<tr("Number Orders")<<tr("Number Tickets")<<tr("Number Vouchers")
          <<tr("Price")<<tr("Paid")<<tr("Due") );
        
        //tab: audit
        if(req->hasRight(req->RGetOrderAudit)){
                tab->addTab(atable=new QTableView,tr("Financial"));
                atable->setModel(audit=new QStandardItemModel);
                atable->setEditTriggers(QAbstractItemView::NoEditTriggers);
                atable->setSelectionMode(QAbstractItemView::NoSelection);
                audit->setHorizontalHeaderLabels(QStringList()
                <<tr("Order ID")<<tr("Action")<<tr("User")<<tr("Date")
                <<tr("Price")<<tr("Paid")<<tr("Due")<<tr("Moved") );
                
        }
        
        //buttons
        vl->addSpacing(10);
        vl->addLayout(hl=new QHBoxLayout);
        hl->addStretch(1);
        QPushButton*p;
        hl->addWidget(p=new QPushButton(tr("Save as...")));
        connect(p,SIGNAL(clicked(bool)),this,SLOT(saveAs()));
        hl->addSpacing(15);
        hl->addWidget(p=new QPushButton(tr("Close")));
        connect(p,SIGNAL(clicked(bool)),this,SLOT(accept()));
        p->setDefault(true);

        //create display
        drawAllOrders();
        drawDayOrders();
        drawOrderSums();
        drawAudit();
}

void MOrdersReport::drawAllOrders()
{
        byord->removeRows(0,byord->rowCount());
        for(const MOOrderInfo&ord:orders){
                //order table
                const int cl=byord->rowCount();
                byord->insertRow(cl);
                byord->setData(byord->index(cl,0),ord.orderid().value());
                byord->setData(byord->index(cl,1),ord.orderStatusString());
                byord->setData(byord->index(cl,2),ord.amounttickets().value());
                byord->setData(byord->index(cl,3),ord.amountvouchers().value());
                byord->setData(byord->index(cl,4),ord.totalPriceString());
                byord->setData(byord->index(cl,5),ord.amountPaidString());
                byord->setData(byord->index(cl,6),ord.amountDueString());
        }
        //adjust
        otable->resizeColumnsToContents();

}

void MOrdersReport::drawDayOrders()
{
        QMap<QDate,QList<MOOrderInfo>>dayorders;
        byday->removeRows(0,byday->rowCount());
        //sort by day
        for(const MOOrderInfo&ord:orders){
                const QDate date=QDateTime::fromTime_t(ord.ordertime().value()).date();
                if(!dayorders.contains(date))dayorders.insert(date,QList<MOOrderInfo>());
                dayorders[date].append(ord);
        }
        //fill date tab
        for(const QDate&date:dayorders.keys()){
                const int cl=byday->rowCount();
                byday->insertRow(cl);
                byday->setData(byday->index(cl,0),date.toString());
                const QList<MOOrderInfo>&day=dayorders[date];
                byday->setData(byday->index(cl,1),day.size());
                qint64 ticks=0,vous=0,money=0,paid=0,due=0;
                for(const MOOrderInfo&ord:day){
                        ticks+=ord.amounttickets();
                        vous+=ord.amountvouchers();
                        money+=ord.totalprice();
                        paid+=ord.amountpaid();
                        due+=ord.amountdue();
                }
                byday->setData(byday->index(cl,2),ticks);
                byday->setData(byday->index(cl,3),vous);
                byday->setData(byday->index(cl,4),cent2str(money));
                byday->setData(byday->index(cl,5),cent2str(paid));
                byday->setData(byday->index(cl,6),cent2str(due));
        }

        dtable->resizeColumnsToContents();
}

void MOrdersReport::drawOrderSums()
{
        qint64 sumtickets=0,sumvouchers=0,summoney=0,sumpaid=0,sumdue=0,
                sumreserve=0,sumreservemoney=0,sumreservetick=0,sumreservevou=0,sumreservepaid=0,sumreservedue=0,
                sumcancel=0,sumcancelmoney=0,sumcanceltick=0,sumcancelvou=0,sumcancelpaid=0,sumcanceldue=0,
                sumsent=0,sumsentmoney=0,sumsenttick=0,sumsentvou=0,sumsentpaid=0,sumsentdue=0,
                sumplaced=0,sumplacedmoney=0,sumplacedtick=0,sumplacedvou=0,sumplacedpaid=0,sumplaceddue=0;
        sum->removeRows(0,sum->rowCount());
        for(const MOOrderInfo&ord:orders){
                //total sums
                summoney+=ord.totalprice();
                sumtickets+=ord.amounttickets();
                sumvouchers+=ord.amountvouchers();
                sumpaid+=ord.amountpaid();
                sumdue+=ord.amountdue();
                //part sums
                if(ord.isReservation()){
                        sumreserve++;sumreservemoney+=ord.totalprice();
                        sumreservetick+=ord.amounttickets();
                        sumreservevou+=ord.amountvouchers();
                        sumreservepaid+=ord.amountpaid();
                        sumreservedue+=ord.amountdue();
                }
                if(ord.isCancelled()){
                        sumcancel++;sumcancelmoney+=ord.totalprice();
                        sumcanceltick+=ord.amounttickets();
                        sumcancelvou+=ord.amountvouchers();
                        sumcancelpaid+=ord.amountpaid();
                        sumcanceldue+=ord.amountdue();
                }
                if(ord.isSent()){
                        sumsent++;sumsentmoney+=ord.totalprice();
                        sumsenttick+=ord.amounttickets();
                        sumsentvou+=ord.amountvouchers();
                        sumsentpaid+=ord.amountpaid();
                        sumsentdue+=ord.amountdue();
                }
                if(ord.isPlaced()){
                        sumplaced++;sumplacedmoney+=ord.totalprice();
                        sumplacedtick+=ord.amounttickets();
                        sumplacedvou+=ord.amountvouchers();
                        sumplacedpaid+=ord.amountpaid();
                        sumplaceddue+=ord.amountdue();
                }
        }
        //fill total sum tab
        auto sumline=[&](QString title,int num,qint64 tick,qint64 vou,qint64 summ,qint64 paid,qint64 due){
                const int cl=sum->rowCount();
                sum->insertRow(cl);
                sum->setData(sum->index(cl,0),title);
                sum->setData(sum->index(cl,1),num);
                sum->setData(sum->index(cl,2),tick);
                sum->setData(sum->index(cl,3),vou);
                sum->setData(sum->index(cl,4),cent2str(summ));
                sum->setData(sum->index(cl,5),cent2str(paid));
                sum->setData(sum->index(cl,6),cent2str(due));
        };
        sumline(tr("Total Orders"), orders.size(), sumtickets, sumvouchers, summoney, sumpaid, sumdue);
        sumline(tr("Reserved Orders"), sumreserve, sumreservetick, sumreservevou, sumreservemoney, sumreservepaid, sumreservedue);
        sumline(tr("Cancelled"), sumcancel, sumcanceltick, sumcancelvou, sumcancelmoney, sumcancelpaid, sumcanceldue);
        sumline(tr("Placed"), sumplaced, sumplacedtick, sumplacedvou, sumplacedmoney, sumplacedpaid, sumplaceddue);
        sumline(tr("Sent"), sumsent, sumsenttick, sumsentvou, sumsentmoney, sumsentpaid, sumsentdue);

        stable->resizeColumnsToContents();
}

void MOrdersReport::drawAudit()
{
        if(atable==nullptr || audit==nullptr)return;
}


static const QByteArray odsmetainf(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        "<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\" manifest:version=\"1.2\">\n"
        " <manifest:file-entry manifest:full-path=\"/\" manifest:version=\"1.2\" manifest:media-type=\"application/vnd.oasis.opendocument.spreadsheet\"/>\n"
        " <manifest:file-entry manifest:full-path=\"content.xml\" manifest:media-type=\"text/xml\"/>\n"
        "</manifest:manifest>"
);

static inline void exportTable(QDomDocument&doc, QDomElement&sheet, QStandardItemModel*model, QString title)
{
        //basics
        QDomElement table=doc.createElement("table:table"); 
        table.setAttribute("table:name",title);
        QDomElement cols=doc.createElement("table:table-column");
        cols.setAttribute("table:number-columns-repeated",model->columnCount());
        table.appendChild(cols);
        //headers
        QDomElement row=doc.createElement("table:table-row");
        for(int c=0;c<model->columnCount();c++){
                QDomElement cell=doc.createElement("table:table-cell");
                cell.setAttribute("office:value-type","string");
                QDomElement p=doc.createElement("text:p");
                p.appendChild(doc.createTextNode(model->headerData(c,Qt::Horizontal).toString()));
                cell.appendChild(p);
                row.appendChild(cell);
        }
        table.appendChild(row);
        //data
        for(int r=0;r<model->rowCount();r++){
                row=doc.createElement("table:table-row");
                for(int c=0;c<model->columnCount();c++){
                        QDomElement cell=doc.createElement("table:table-cell");
                        cell.setAttribute("office:value-type","string");
                        QDomElement p=doc.createElement("text:p");
                        p.appendChild(doc.createTextNode(model->data(model->index(r,c)).toString()));
                        cell.appendChild(p);
                        row.appendChild(cell);
                }
                table.appendChild(row);
        }
        //done
        sheet.appendChild(table);
}

void MOrdersReport::saveAs()
{
        QFileDialog fdlg(this,tr("Save Report"));
        fdlg.setAcceptMode(QFileDialog::AcceptSave);
        fdlg.setDefaultSuffix("ods");
        fdlg.setFileMode(QFileDialog::AnyFile);
        fdlg.setDirectory(currentDir());
        if(!fdlg.exec())return;
        const QString fn=fdlg.selectedFiles().value(0);
        if(fn.isEmpty())return;
        setCurrentDir(fn);
        //TODO: use template if available
        //create file
        QFile fd(fn);
        if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
                QMessageBox::warning(this,tr("Warning"),tr("Unable to create file '%1'.").arg(fn));
                return;
        }
        Zip zip;
        if(!zip.open(&fd)){
                QMessageBox::warning(this,tr("Warning"),tr("Ooops. Unable to create ZIP structure in file '%1'.").arg(fn));
                fd.close();
                fd.remove();
                return;
        }
        //metadata
        zip.storeFile("mimetype",QByteArray("application/vnd.oasis.opendocument.spreadsheet"));
        zip.storeFile("META-INF/manifest.xml",odsmetainf);
        //generate content
        QDomDocument doc;
        QDomElement root=doc.createElement("office:document-content");
        root.setAttribute("xmlns:office","urn:oasis:names:tc:opendocument:xmlns:office:1.0"); root.setAttribute("xmlns:text","urn:oasis:names:tc:opendocument:xmlns:text:1.0"); root.setAttribute("xmlns:table","urn:oasis:names:tc:opendocument:xmlns:table:1.0");
        root.setAttribute("office:version","1.2");
        QDomElement body=doc.createElement("office:body");
        QDomElement sheet=doc.createElement("office:spreadsheet");
        exportTable(doc,sheet,sum,tr("Sums"));
        exportTable(doc,sheet,byord,tr("Orders"));
        exportTable(doc,sheet,byday,tr("By Day"));
        if(audit)
                exportTable(doc,sheet,audit,tr("Financial"));
        body.appendChild(sheet);
        root.appendChild(body);
        doc.appendChild(root);
        //store
        zip.storeFile("content.xml",doc.toByteArray());
        zip.close();
        fd.close();
}
