//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ACLTABS_H
#define MAGICSMOKE_ACLTABS_H

#include <QDialog>
#include <QItemDelegate>
#include <QWidget>

class QAction;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class QStandardItemModel;
class QTabWidget;
class QTableView;

class MSInterface;

/**user admin tab*/
class MUserTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the window QSettings-key for current profile*/
		MUserTab(QString);
	public slots:
		/**update list of users*/
		void updateUsers();
	private slots:
		/**create new user*/
		void newUser();
		/**delete selected user*/
		void deleteUser();
		/**set users description*/
		void editUserDescription();
		/**set users roles*/
		void editUserRoles();
		/**set users hosts*/
		void editUserHosts();
		/**set a users password*/
		void setUserPassword();
		/**change users flags*/
		void editFlags();
		
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*usertable;
		QStandardItemModel*usermodel;
};

/**host admin tab*/
class MHostTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the window QSettings-key for current profile*/
		MHostTab(QString);
	public slots:
		/**update list of hosts*/
		void updateHosts();
	private slots:
		/**create new host*/
		void newHost();
		/**delete a host*/
		void deleteHost();
		/**generate a new key*/
		void changeHostKey();
		/**import host from file*/
		void importHost();
		/**export host to hash file*/
		void exportHost();
                /**export fresh host as key file*/
                void exportKey(QString name,QString key);
		
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*hosttable;
		QStandardItemModel*hostmodel;
		
		QString key2hash(QString);
};

/**role admin tab*/
class MRoleTab:public QWidget
{
	Q_OBJECT
	public:
		MRoleTab(QString);
	public slots:
		void updateRoles();
		void newRole();
		void deleteRole();
		void editDescription();
		void editFlags();
		void editRights();
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*roletable;
		QStandardItemModel*rolemodel;
};

/**flag admin tab*/
class MFlagTab:public QWidget
{
	Q_OBJECT
	public:
		MFlagTab(QString);
	public slots:
		void updateFlags();
		void addFlag();
		void removeFlag();
		void changeFlag();
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*table;
		QStandardItemModel*model;
};

#endif
