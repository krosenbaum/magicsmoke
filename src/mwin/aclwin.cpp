//
// C++ Implementation: acldialog
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#include "acltabs.h"
#include "msinterface.h"
#include "main.h"

#include "aclwin.h"

#include <QBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QPushButton>
#include <QSettings>
#include <QStatusBar>
#include <QTabWidget>


QPointer<MAclWindow>MAclWindow::instance;

void MAclWindow::showWindow(QWidget*p)
{
	if(instance.isNull()){
		instance=new MAclWindow(p);
	}
	if(!instance->isVisible())instance->show();
	if(instance->isMinimized())instance->showNormal();
	instance->activateWindow();
	instance->raise();
}

MAclWindow::MAclWindow(QWidget*par)
	:MTabWin(req->profileId(),par)
{
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle(tr("ACL Editor"));
	
	rtimer.setInterval(QSettings().value("profiles/"+profilekey+"/refresh",300).toInt()*1000);
	rtimer.start();
	connect(&rtimer,SIGNAL(timeout()),this,SLOT(refreshData()));

	//menu
	QMenu*m=addMenu(tr("&Window"));
	m->addAction(tr("&Close"),this,SLOT(close()));
	
	//tabs
	//user tab
	addTab(usertab=new MUserTab(profilekey),tr("Users"));
	//role tab
	addTab(roletab=new MRoleTab(profilekey),tr("Roles"));
	//host tab
	addTab(hosttab=new MHostTab(profilekey),tr("Hosts"));
	//flags tab
	addTab(flagtab=new MFlagTab(profilekey),tr("Flags"));
	
	//unused tab disabling...
	if(!req->hasRight(req->RGetAllUsers)){
		setTabEnabled(usertab,false);
	}
	if(!req->hasRight(req->RGetAllHosts)){
		setTabEnabled(hosttab,false);
	}
	if(!req->hasRight(req->RGetAllRoles)){
		setTabEnabled(roletab,false);
	}
}


void MAclWindow::refreshData()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	if(set.value("refreshUsers",false).toBool() && req->hasRight(req->RGetAllUsers))
		usertab->updateUsers();
	if(set.value("refreshHosts",false).toBool() && req->hasRight(req->RGetAllHosts))
		hosttab->updateHosts();
	if(set.value("refreshRoles",false).toBool() && req->hasRight(req->RGetAllRoles))
		roletab->updateRoles();
}
