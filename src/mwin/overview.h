//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_OVERVIEW_H
#define MAGICSMOKE_OVERVIEW_H

#include <QDateTime>
#include <QDialog>
#include <QItemDelegate>
#include <QMainWindow>
#include <QTimer>

#include "tabwin.h"

class QAction;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class QStandardItemModel;
class QTabWidget;
class QTableView;

class MSInterface;

class MEventsTab;
class MCartTab;
class MOrdersTab;
class MEntranceTab;
class MUserTab;
class MHostTab;

/**Main Overview Window*/
class MOverview:public MTabWin
{
	Q_OBJECT
	public:
		/**construct the window with web-request/session handler and QSettings-key for current profile*/
		MOverview(QString);
		~MOverview();
	protected:
		/**handle closing the window: close the session too*/
		void closeEvent(QCloseEvent*) override;
		
	private slots:
		/**try to log in again*/
		void relogin();
		
		/**generic check which tab is active*/
		void tabChanged(int);
		
		/**set refresh timeout*/
		void setRefresh();
		
		/**web request settings dialog; shows the dialog per default, just copies settings from registry to webrequest object if false*/
		void webSettings(bool dlg=true);
		
		/**set my own password*/
		void setMyPassword();
		
		/**display property settings*/
		void displaySettings();
		/**settings for backup*/
		void backupSettings();
		///label printing settings
		void labelSettings();
                ///OpenOffice settings
                void openOfficeSettings();
                ///barcode scanner settings
                void barcodeSettings();
                ///language settings
                void changeLang();
		
		///start order audit
		void orderAudit();
		///ticket audit
		void ticketAudit();
		///voucher audit
		void voucherAudit();
		///user audit
		void userAudit();
		
	public slots:
		///restore the window
		void showRestored();

		/**manage customers*/
		void customerMgmt();
		
		/**edit shipping options*/
		void editShipping();
		///edit payment options
		void editPayment();
		
		/**return a ticket or voucher*/
		void ticketReturn();
		/**deduct some money from a voucher (to pay outside the system)*/
		void deductVoucher();
		/**empty/invalidate voucher*/
		void emptyVoucher();
		
		/**refresh data that we can refresh*/
		void refreshData();
		
		/**do a backup now*/
		void doBackup();
		
		/**switch to the cart tab*/
		void switchToCartTab();
		
		/**open ACL/user admin window*/
		void aclWindow();
		
		/**open template editor*/
		void editTemplates();
		void editOdfTemplate();
		
	private:
		friend class MWizard;
		//the profile associated with this session
		//widgets
		MEventsTab*eventtab;
		MCartTab*carttab;
		MOrdersTab*ordertab;
		MEntranceTab*entrancetab;
		//refresh timers
		QTimer rtimer,baktimer;
};

#endif
