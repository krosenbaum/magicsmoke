//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ENTRANCETAB_H
#define MAGICSMOKE_ENTRANCETAB_H

#include <QDateTime>
#include <QWidget>

class QMenu;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QStandardItemModel;

class MSInterface;

/**Main Overview Window*/
class MEntranceTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the window with web-request/session handler and QSettings-key for current profile*/
		MEntranceTab(QString);
		
		///creates a menu for the entrance tab
		QList<QMenu*>menu()const;
	public slots:
		/**initializes the tab
		\param force if true an update is forced, if false the update is only done if the last update has been more than an hour in the past*/
		void initialize(bool force=true);
	private slots:
		/**react on entry in Entrance tab*/
		void entranceValidate();
		/**resets the display after a timeout*/
		void resetLabel();
		/**tries to open the order of the current barcode*/
		void openOrder();
		/**resets the amount labels*/
		void resetAmounts();
		/**configure the entrance*/
		void configure();
		///handles the askForFocus signal from the barcode line
		void askForFocus();

	signals:
		///asks the parent to get focus
		void askForFocus(QWidget*);
	private:
		friend class METUtility;
		//the profile associated with this session
		QString profilekey;
		//widgets
		QLabel*entrancelabel;
		QComboBox*entranceevent;
		QLineEdit*entrancescan;
		QPushButton*orderbtn;
		QLabel*amtotal,*amused,*amopen,*amreserved;
		//barcode cache
		QString lastbarcode;
		QDateTime lastbcscan;
		//event update timeout
		QDateTime lasteventupdate;
};

#endif
