//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TABWIN_H
#define MAGICSMOKE_TABWIN_H

#include <QMainWindow>
#include <QMap>
#include <QPointer>
#include <QTimer>

#include "smokeexport.h"

class QTabWidget;
class QStackedWidget;

class MSInterface;

/**Main Overview Window*/
class MAGICSMOKE_EXPORT MTabWin:public QMainWindow
{
	Q_OBJECT
	public:
		/**construct the window with web-request/session handler and QSettings-key for current profile*/
		MTabWin(QString,QWidget*parent=0);
		~MTabWin();
	protected slots:
		/**generic check which tab is active*/
		virtual void tabChanged(int tabid=-1);
		
	public slots:
		///add a tab
		virtual void addTab(QWidget*tab,QString title,QList<QMenu*>menu=QList<QMenu*>());
		
		///add a menu to the left side, where it is visible for all tabs
		virtual QMenu* addMenu(QString);
		
		///returns a pointer to the help menu
		QMenu*helpMenu()const;
		
		///enables/disables a tab
		void setTabEnabled(QWidget*,bool);
		
		///returns the current tab
		QWidget* currentTab()const;
		
		///set the current tab
		void setCurrentTab(QWidget*);
		
		///overrides QWidget::setWindowTitle(const QString&) - adds basic session info
		void setWindowTitle(const QString&);
		
	protected:
		//the profile associated with this session
		QString profilekey;
	private:
		//widgets
		QTabWidget*tabw;
		QMap<int,QList<QMenu*> >tabmenu;
		QPointer<QAction>helpseparator,leftseparator;
		QPointer<QMenu>helpmenu;
};

#endif
