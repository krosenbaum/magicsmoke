//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "customerdlg.h"
#include "mapplication.h"
#include "msinterface.h"
#include "passwdchg.h"
#include "shipping.h"
#include "templatedlg.h"
#include "backupdlg.h"
#include "payedit.h"
#include "orderauditdlg.h"
#include "odfedit.h"
#include "office.h"

#include "aclwin.h"
#include "carttab.h"
#include "entrancetab.h"
#include "eventstab.h"
#include "orderstab.h"
#include "overview.h"

#include "centbox.h"
#include "sclock.h"
#include "barcode-plugin.h"
#include "labeldlg.h"

#include <TimeStamp>

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QFile>
#include <QGroupBox>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QProgressDialog>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStatusBar>
#include <QTabWidget>
#include <QFormLayout>
#include <QDateTimeEdit>

#include "MTChangeMyPassword"
#include "MTReturnTicketVoucher"
#include "MTDeductVoucher"
#include "MTEmptyVoucher"
#include "MTBackup"
#include "MTGetAllUsers"

#include "sclimenu.h"

MOverview::MOverview(QString pk)
	:MTabWin(pk),entrancetab(nullptr)
{
	rtimer.setInterval(QSettings().value("profiles/"+pk+"/refresh",300).toInt()*1000);
	rtimer.start();
	connect(&rtimer,SIGNAL(timeout()),this,SLOT(refreshData()));
	//check backup timing
	int btm=QSettings().value("profiles/"+pk+"/backupinterval",0).toInt()*86400;
	if(btm){
		//adjust for last backup time
		int iv=QDateTime::currentDateTime().toTime_t() - QSettings().value("profiles/"+pk+"/backuptime",0).toInt();
		if(iv>=btm)btm=0;
		else btm-=iv;
		//earliest allowed backup is 30s from now
		if(btm<30)btm=30;
		//start timer
		baktimer.start(btm*1000);
		connect(&baktimer,SIGNAL(timeout()),this,SLOT(doBackup()));
	}
	
	//menu
	QMenu*m=addMenu(tr("&Session"));
	m->addAction(tr("&Re-Login"),this,SLOT(relogin()));
	m->addAction(tr("Change my &Password"),this,SLOT(setMyPassword()))
	 ->setEnabled(req->hasRight(req->RChangeMyPassword));
	m->addSeparator();
	m->addAction(tr("&Close Session"),this,SLOT(close()));
	//menus not connected to any specific tab...
	m=addMenu(tr("&Actions"));
	m->addAction(tr("&Show all customers"),this,SLOT(customerMgmt()))
	 ->setEnabled(req->hasRight(req->RGetAllCustomerNames));
	
	QMenu*m2=m->addMenu(tr("Tickets and &Vouchers"));
	m2->addAction(tr("Return &ticket or voucher..."),this,SLOT(ticketReturn()))
	 ->setEnabled(req->hasRight(req->RReturnTicketVoucher));
	m2->addSeparator();
	m2->addAction(tr("&Deduct from voucher..."),this,SLOT(deductVoucher()))
	 ->setEnabled(req->hasRight(req->RDeductVoucher));
	m2->addAction(tr("&Empty voucher..."),this,SLOT(emptyVoucher()))
	 ->setEnabled(req->hasRight(req->REmptyVoucher));
	 
	m2=m->addMenu(tr("&Options"));
	m2->addAction(tr("Edit &Shipping Options..."),this,SLOT(editShipping()))
	 ->setEnabled(req->hasRight(req->RGetAllShipping));
	m2->addAction(tr("Edit &Payment Options..."),this,SLOT(editPayment()))
	 ->setEnabled(req->hasRight(req->RGetPaymentTypes));
	
	m2=m->addMenu(tr("&Templates"));
	m2->addAction(tr("&Edit Templates..."),this,SLOT(editTemplates()))
	 ->setEnabled(req->hasRight(req->RSetTemplate));
	m2->addAction(tr("&Update Templates Now"),req,SLOT(updateTemplates()))
	 ->setEnabled(req->hasRight(req->RGetTemplateList));
	m2->addSeparator();
	m2->addAction(tr("&ODF Editor..."),this,SLOT(editOdfTemplate()));

	m2=m->addMenu(tr("A&udit"));
	m2->addAction(tr("&Order Audit..."),this,SLOT(orderAudit()));
	m2->addAction(tr("&Ticket Audit..."),this,SLOT(ticketAudit()));
	m2->addAction(tr("&Voucher Audit..."),this,SLOT(voucherAudit()));
	m2->addAction(tr("&User Audit..."),this,SLOT(userAudit()));
	
	m2=m->addMenu(tr("&Administration"));
	m2->addAction(tr("&User Administration..."),this,SLOT(aclWindow()))
	 ->setEnabled(req->hasRight(req->RGetAllUsers) || req->hasRight(req->RGetAllHosts) || req->hasRight(req->RGetAllRoles));
	m2->addSeparator();
	m2->addAction(tr("Backup &Settings..."),this,SLOT(backupSettings()))
	 ->setEnabled(req->hasRight(req->RBackup));
	m2->addAction(tr("&Backup now..."),this,SLOT(doBackup()))
	 ->setEnabled(req->hasRight(req->RBackup));
	
	m2=m->addMenu(tr("&Configuration"));
	m2->addAction(tr("&Change Language..."),this,SLOT(changeLang()));
	m2->addAction(tr("&Auto-Refresh settings..."),this,SLOT(setRefresh()));
	m2->addAction(tr("&Server Access settings..."),this,SLOT(webSettings()));
	m2->addAction(tr("&Display settings..."),this,SLOT(displaySettings()));
	m2->addAction(tr("&Label Printing settings..."),this,SLOT(labelSettings()));
	m2->addAction(tr("&OpenOffice settings..."),this,SLOT(openOfficeSettings()));
	m2->addAction(tr("&Barcode Scanner settings..."),this,SLOT(barcodeSettings()));

	MSessionClient_createMenuObject(m->addMenu(tr("&Session Manager")));

	//make sure webrequest knows its settings
	webSettings(false);
	
	//Event tab
	QProgressDialog pd(this);
	pd.setWindowTitle(tr("Login"));
	pd.setLabelText(tr("Getting data..."));
	pd.setCancelButton(nullptr);
	pd.setRange(0,100);
	pd.setValue(10);
	pd.setVisible(true);
	pd.setValue(10);pd.setLabelText(tr("Getting events..."));
	eventtab=new MEventsTab(pk);
	addTab(eventtab,tr("Events"),eventtab->menu());
	
	//Shopping Cart Tab
	carttab=new MCartTab(pk);
	addTab(carttab,tr("Shopping Cart"),carttab->menu());
	connect(eventtab,SIGNAL(eventOrderTicket()),carttab,SLOT(eventOrderTicket()));
	connect(eventtab,SIGNAL(eventOrderTicket(qint64,qint64)),carttab,SLOT(eventOrderTicket(qint64,qint64)));
	connect(carttab,SIGNAL(getEventId(int&,bool&)), eventtab,SLOT(selectEventId(int&,bool&)), Qt::DirectConnection);
	connect(carttab,SIGNAL(currentEventId()), eventtab,SLOT(currentEventId()), Qt::DirectConnection);
	connect(carttab,SIGNAL(currentEventTitle()), eventtab,SLOT(currentEventTitle()), Qt::DirectConnection);
	connect(carttab,SIGNAL(currentEventStart()), eventtab,SLOT(currentEventStart()), Qt::DirectConnection);
	connect(carttab,SIGNAL(requestFocus()), this,SLOT(switchToCartTab()));
	
	//Order List Tab
	pd.setValue(40);pd.setLabelText(tr("Getting Orders..."));
	ordertab=new MOrdersTab(pk);
	addTab(ordertab, tr("Order List"), ordertab->menu());
	connect(ordertab,SIGNAL(selectEventIds(QList<int>&)), eventtab,SLOT(selectEventIds(QList<int>&)), Qt::DirectConnection);
	
	//Entrance Control Tab
        pd.setValue(70);pd.setLabelText(tr("Getting Entrance Data..."));
	entrancetab=new MEntranceTab(pk);
	addTab(entrancetab,tr("Entrance"),entrancetab->menu());
	connect(entrancetab,SIGNAL(askForFocus(QWidget*)),this,SLOT(setCurrentTab(QWidget*)));

	//unused tab disabling...
	if(!req->hasRight(req->RGetAllEvents)){
		eventtab->setEnabled(false);
		setTabEnabled(eventtab,false);
	}
	if(!req->hasRight(req->RCreateOrder)&&!req->hasRight(req->RCreateReservation)){
		setTabEnabled(carttab,false);
	}
	if(!req->hasRight(req->RGetOrderList)){
		setTabEnabled(ordertab,false);
	}
	//make sure correct menu is displayed
	tabChanged(-1);
}

#define GEOGROUP "magicSmokeClient/MainWindow/state"

void MOverview::closeEvent(QCloseEvent*ce)
{
	//store geometry
	QSettings set;
	set.beginGroup(GEOGROUP);
	set.setValue("geometry",saveGeometry());
	set.setValue("state",saveState());
	//actually close window
	QMainWindow::closeEvent(ce);
}

void MOverview::showRestored()
{
	QSettings set;
	set.beginGroup(GEOGROUP);
	if(!restoreGeometry(set.value("geometry").toByteArray())||!restoreState(set.value("state").toByteArray()))
		showMaximized();
	else
		show();
}

MOverview::~MOverview()
{
	qDebug()<<"destructing overview"<<hex<<(long)this;
	//free requestor
	req->deleteLater();
}

void MOverview::switchToCartTab()
{
	setCurrentTab(carttab);
}

void MOverview::relogin()
{
	setEnabled(false);
	if(!req->relogin())
		QMessageBox::warning(this,tr("Warning"),tr("I was unable to renew the login at the server."));
	setEnabled(true);
}

void MOverview::setMyPassword()
{
	MPasswordChange pc(this);
	if(pc.exec()==QDialog::Accepted){
		MTChangeMyPassword cmp=MTChangeMyPassword::query(pc.oldPassword(),pc.newPassword());
		if(cmp.hasError())
			QMessageBox::warning(this,tr("Warning"),tr("Error setting password: %1").arg(cmp.errorString()));
	}
}

void MOverview::customerMgmt()
{
	MCustomerListDialog mcl(this);
	mcl.exec();
}

void MOverview::editShipping()
{
	MShippingEditor se(this);
	se.exec();
	carttab->updateShipping();
}

void MOverview::editPayment()
{
	MPaymentEditor se(this);
	se.exec();
}

void MOverview::tabChanged(int idx)
{
	MTabWin::tabChanged(idx);
	QWidget*w=currentTab();
	if(w==entrancetab){
		entrancetab->initialize(false);
	}
}

void MOverview::ticketReturn()
{
	//get ticket
	bool ok;
	QString tid=QInputDialog::getText(this,tr("Return Ticket/Voucher"),tr("Please enter the ticket or voucher ID to return:"),QLineEdit::Normal,"",&ok);
	if(!ok || tid=="")return;
	MTReturnTicketVoucher rtv=MTReturnTicketVoucher::query(tid.trimmed());
	//check state
	if(rtv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("This ticket/voucher cannot be returned: %1").arg(rtv.errorString()));
	}
}

void MOverview::deductVoucher()
{
	//get voucher ID and amount
	QDialog d;
	d.setWindowTitle(tr("Deduct from Voucher"));
	QGridLayout *gl;
	d.setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("Using a voucher to pay outside the system.")),0,0,1,2);
	
	QLineEdit*vid,*cmt;
	MCentSpinBox*cent;
	gl->addWidget(new QLabel(tr("Amount to deduct:")),1,0);
	gl->addWidget(cent=new MCentSpinBox,1,1);
	gl->addWidget(new QLabel(tr("Reason for deducting:")),2,0);
	gl->addWidget(cmt=new QLineEdit,2,1);
	gl->addWidget(new QLabel(tr("Voucher ID:")),3,0);
	gl->addWidget(vid=new QLineEdit,3,1);
	
	gl->setRowMinimumHeight(4,15);
	QHBoxLayout*hl;
	gl->addLayout(hl=new QHBoxLayout,5,0,1,2);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("OK")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	if(vid->text().trimmed()=="" || cent->value()<=0)return;
	//query server
	MTDeductVoucher dv=req->queryDeductVoucher(vid->text(),cent->value(),cmt->text());
	if(dv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to deduct voucher: %1").arg(dv.errorString()));
		return;
	}
	if(dv.getamount().value()==0){
		QMessageBox::warning(this,tr("Warning"),tr("Voucher does not contain enough money. Money left: %1").arg(dv.getvoucher().value().value()));
		return;
	}
	QMessageBox::information(this,tr("Deducted from Voucher"),
	  tr("Value taken from voucher: %1\nValue remaining on voucher: %2")
	  .arg(cent2str(dv.getamount()))
	  .arg(cent2str(dv.getvoucher().value().value()))
	);
}

void MOverview::emptyVoucher()
{
	//get voucher ID
	QDialog d(this);
        d.setWindowTitle(tr("Invalidate Voucher"));
        QGridLayout*gl;
        d.setLayout(gl=new QGridLayout);
        gl->addWidget(new QLabel(tr("This will invalidate/empty the voucher. It will no longer be usable afterwards, but will still have to be paid for.")),0,0,1,2);
        gl->addWidget(new QLabel(tr("Comment:")),1,0);
        QLineEdit*vid,*comment;
        gl->addWidget(comment=new QLineEdit,1,1);
        comment->setPlaceholderText(tr("Please enter a reason for invalidating the voucher."));
        gl->addWidget(new QLabel(tr("Voucher ID/Barcode:")),2,0);
        gl->addWidget(vid=new QLineEdit,2,1);
        vid->setPlaceholderText(tr("Please scan the barcode."));
        gl->setRowMinimumHeight(3,15);
        QHBoxLayout*hl;
        gl->addLayout(hl=new QHBoxLayout,4,0,1,2);
        hl->addStretch(1);
        QPushButton*p;
        hl->addWidget(p=new QPushButton(tr("&OK")));
        connect(p,SIGNAL(clicked(bool)),&d,SLOT(accept()));
        hl->addWidget(p=new QPushButton(tr("&Cancel")));
        connect(p,SIGNAL(clicked(bool)),&d,SLOT(reject()));
        if(d.exec()!=QDialog::Accepted)return;
	//query server
	MTEmptyVoucher dv=req->queryEmptyVoucher(vid->text(),comment->text());
	if(dv.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to invalidate voucher: %1").arg(dv.errorString()));
		return;
	}
	QMessageBox::information(this,tr("Invalidated Voucher"),
	  tr("The voucher '%1'has been invalidated.").arg(vid->text()));
}

void MOverview::refreshData()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	if(set.value("refreshEvents",false).toBool() && req->hasRight(req->RGetAllEvents))
		eventtab->updateEvents();
	if(set.value("refreshShipping",false).toBool() && req->hasRight(req->RGetAllShipping))
		carttab->updateShipping();
}

void MOverview::setRefresh()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	//dialog
	QDialog d;
	d.setWindowTitle(tr("Refresh Settings"));
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addWidget(new QLabel(tr("Refresh Rate (minutes):")),1);
	QSpinBox*rate;
	hl->addWidget(rate=new QSpinBox,0);
	rate->setRange(1,999);
	rate->setValue(set.value("refresh",300).toInt()/60);
	QCheckBox *rev,*rus,*rho,*rsh;
	vl->addWidget(rev=new QCheckBox(tr("refresh &event list")));
	rev->setChecked(set.value("refreshEvents",false).toBool());
	vl->addWidget(rus=new QCheckBox(tr("refresh &user list")));
	rus->setChecked(set.value("refreshUsers",false).toBool());
	vl->addWidget(rho=new QCheckBox(tr("refresh &host list")));
	rho->setChecked(set.value("refreshHosts",false).toBool());
	vl->addWidget(rsh=new QCheckBox(tr("refresh &shipping list")));
	rho->setChecked(set.value("refreshShipping",false).toBool());
	vl->addSpacing(15);
	vl->addStretch(10);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&OK")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	//write settings
	set.setValue("refreshEvents",rev->isChecked());
	set.setValue("refreshUsers",rus->isChecked());
	set.setValue("refreshHosts",rho->isChecked());
	set.setValue("refreshShipping",rsh->isChecked());
	set.setValue("refresh",rate->value()*60);
	//reset timer
	rtimer.stop();
	rtimer.setInterval(rate->value()*60000);
	rtimer.start();
}

void MOverview::webSettings(bool showdlg)
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	//get settings
	MSInterface::LogLevel lvl=MSInterface::LogLevel(set.value("webloglevel", MSInterface::LogOnError).toInt());
	int timeout=set.value("webtimeout",30).toInt();
	//dialog
	if(showdlg){
		QDialog d;
		d.setWindowTitle(tr("Server Access Settings"));
		QGridLayout*gl;
		QHBoxLayout*hl;
		d.setLayout(gl=new QGridLayout);
		gl->addWidget(new QLabel(tr("Request Timeout (seconds):")),0,0);
		QSpinBox*tmout;
		gl->addWidget(tmout=new QSpinBox,0,1);
		tmout->setRange(10,999);
		tmout->setValue(timeout);
		QComboBox *log;
		gl->addWidget(new QLabel(tr("Log Level:")),1,0);
		gl->addWidget(log=new QComboBox,1,1);
		log->addItem(tr("No Logging"),MSInterface::LogNone);
		log->addItem(tr("Minimal Logging"),MSInterface::LogMinimal);
		log->addItem(tr("Medium Logging"),MSInterface::LogInfo);
		log->addItem(tr("Log Details on Error"),MSInterface::LogOnError);
		log->addItem(tr("Always Log Details"),MSInterface::LogDetailed);
		switch(lvl){
			case MSInterface::LogNone:log->setCurrentIndex(0);break;
			case MSInterface::LogMinimal:log->setCurrentIndex(1);break;
			case MSInterface::LogInfo:log->setCurrentIndex(2);break;
			case MSInterface::LogOnError:log->setCurrentIndex(3);break;
			case MSInterface::LogDetailed:log->setCurrentIndex(4);break;
		}
		gl->setRowMinimumHeight(2,15);
		gl->addLayout(hl=new QHBoxLayout,3,0,1,2);
		hl->addStretch(10);
		QPushButton*p;
		hl->addWidget(p=new QPushButton(tr("&OK")));
		connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
		hl->addWidget(p=new QPushButton(tr("&Cancel")));
		connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
		if(d.exec()!=QDialog::Accepted)return;
		//write settings
		lvl=MSInterface::LogLevel(log->itemData(log->currentIndex()).toInt());
		timeout=tmout->value();
		set.setValue("webloglevel",lvl);
		set.setValue("webtimeout",timeout);
	}
	//reset webrequest
	req->setLogLevel(lvl);
	req->setWebTimeout(timeout*1000);
}

void MOverview::displaySettings()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	//get settings
	int maxage=set.value("maxeventage", 0).toInt();
	int maxageo=set.value("maxorderage", 0).toInt();
	int maxageoo=set.value("maxorderageopen", 0).toInt();
	int maxageoe=set.value("maxorderageevent", 0).toInt();
	int maxageoc=set.value("maxorderagecust", 0).toInt();
	int maxageos=set.value("deforderagesince", 7).toInt();
	//dialog
	QDialog d;
	QGroupBox*gbox;
	d.setWindowTitle(tr("Display Settings"));
	QGridLayout*gl;
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	vl->addWidget(gbox=new QGroupBox(tr("Event settings")),0);
	gbox->setLayout(gl=new QGridLayout);
	int cl=-1;
	gl->addWidget(new QLabel(tr("Maximum event age (days, 0=show all):")),++cl,0);
	QSpinBox*mage;
	gl->addWidget(mage=new QSpinBox,cl,1);
	mage->setRange(0,99999);
	mage->setValue(maxage);
	
	vl->addWidget(gbox=new QGroupBox(tr("Maximum order list age")),0);
	gbox->setLayout(gl=new QGridLayout);
	cl=-1;
	gl->addWidget(new QLabel(tr("Age in days, 0=show all.")),++cl,0,1,2);
	gl->addWidget(new QLabel(tr("When showing all orders:")),++cl,0);
	QSpinBox*oage;
	gl->addWidget(oage=new QSpinBox,cl,1);
	oage->setRange(0,99999);
	oage->setValue(maxageo);
	gl->addWidget(new QLabel(tr("When showing open orders:")),++cl,0);
	QSpinBox*ooage;
	gl->addWidget(ooage=new QSpinBox,cl,1);
	ooage->setRange(0,99999);
	ooage->setValue(maxageoo);
	gl->addWidget(new QLabel(tr("When searching by event:")),++cl,0);
	QSpinBox*oeage;
	gl->addWidget(oeage=new QSpinBox,cl,1);
	oeage->setRange(0,99999);
	oeage->setValue(maxageoe);
	gl->addWidget(new QLabel(tr("When searching by customer:")),++cl,0);
	QSpinBox*ocage;
	gl->addWidget(ocage=new QSpinBox,cl,1);
	ocage->setRange(0,99999);
	ocage->setValue(maxageoc);
	gl->addWidget(new QLabel(tr("Default age when searching by date:")),++cl,0);
	QSpinBox*osage;
	gl->addWidget(osage=new QSpinBox,cl,1);
	osage->setRange(0,99999);
	osage->setValue(maxageos);
	
	vl->addSpacing(15);vl->addStretch(1);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&OK")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	//write settings
	maxage=mage->value();
	maxageo=oage->value();
	maxageoo=ooage->value();
	maxageoe=oeage->value();
	maxageoc=ocage->value();
	set.setValue("maxeventage",maxage);
	set.setValue("maxorderage",maxageo);
	set.setValue("maxorderageopen",maxageoo);
	set.setValue("maxorderageevent",maxageoe);
	set.setValue("maxorderagecust",maxageoc);
	set.setValue("deforderagesince",maxageos);
}

void MOverview::changeLang()
{
	MApplication::choseLanguage();
}


void MOverview::doBackup()
{
	baktimer.stop();
	//sanity check
	if(!req->hasRight(req->RBackup))return;
	//get settings
	QSettings set;
	set.beginGroup("profiles/"+profilekey);
	QString path=set.value("backupfile",req->dataDir()+"/backup").toString();
	int gens=set.value("backupgenerations",3).toInt();
	//get data
	MTBackup bc;
	bc=MTBackup::query();
	if(bc.stage()!=bc.Success){
		QMessageBox::warning(this,tr("Warning"),tr("Backup failed with error (%2): %1").arg(bc.errorString()).arg(bc.errorType()));
		return;
	}
	if(bc.getbackup().isNull()||bc.getbackup().value().isEmpty()){
		QMessageBox::warning(this,tr("Warning"),tr("Backup returned empty."));
		return;
	}
	//rotate files
	QFile(path+"."+QString::number(gens)).remove();
	for(int i=gens-1;i>=0;i--){
		if(i)
			QFile(path+"."+QString::number(i)).rename(path+"."+QString::number(i+1));
		else
			QFile(path).rename(path+".1");
	}
	//store new data
	QFile fd(path);
	if(fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		fd.write(bc.getbackup().value().toLatin1());
		fd.close();
		set.setValue("backuptime",QDateTime::currentDateTime().toTime_t());
		QMessageBox::information(this,tr("Backup"),tr("The backup was successful."));
		int tm=set.value("backupinterval",0).toInt()*86400000;
		if(tm)baktimer.start(tm);
	}else
		QMessageBox::warning(this,tr("Warning"),tr("Cannot create backup file."));
}

void MOverview::backupSettings()
{
	//show dialog
	MBackupDialog d(this,profilekey);
	d.exec();
	//reset timer
	int btm=QSettings().value("profiles/"+profilekey+"/backupinterval",0).toInt()*86400;
	if(btm){
		//adjust for last backup time
		int iv=QDateTime::currentDateTime().toTime_t() - QSettings().value("profiles/"+profilekey+"/backuptime",0).toInt();
		if(iv>=btm)btm=0;
		else btm-=iv;
		//start timer
		baktimer.start(btm*1000);
	}
}

void MOverview::labelSettings()
{
	MLabelConfig::configDialog();
}

void MOverview::openOfficeSettings()
{
        MOfficeConfig c(this);
        c.exec();
}

void MOverview::barcodeSettings()
{
        MBarcodeConfiguration bc(this);
        bc.exec();
}

void MOverview::aclWindow()
{
	MAclWindow::showWindow(this);
}

void MOverview::editTemplates()
{
	MTemplateEditor *te=new MTemplateEditor(MSInterface::instance()->templateStore());
	te->show();
}

void MOverview::editOdfTemplate()
{
	MOdfEditor *oe=new MOdfEditor(this);
	connect(oe,SIGNAL(getEvent(int&,bool&)),eventtab,SLOT(selectEventId(int&,bool&)));
	oe->show();
}

void MOverview::orderAudit()
{
	//get order ID
	bool ok;
	qint64 oid=QInputDialog::getInt(this, tr("Order ID"), tr("Please enter the ID of the order you want to audit:"), 0, 0, 2147483647, 1, &ok);
	if(!ok)return;
	//open dialog
	MOrderAuditDialog::openOrderAuditDialog(oid);
}

void MOverview::ticketAudit()
{
	//get ticket ID
	bool ok;
	QString id=QInputDialog::getText ( this, tr("Ticket ID"), tr("Please enter the ID of the ticket you want to audit:"),QLineEdit::Normal, QString(), &ok);
	if(!ok || id.isEmpty())return;
	//open dialog
	MOrderAuditDialog::openTicketAuditDialog(id);
}

void MOverview::voucherAudit()
{
	//get voicher ID
	bool ok;
	QString id=QInputDialog::getText ( this, tr("Voucher ID"), tr("Please enter the ID of the voucher you want to audit:"),QLineEdit::Normal, QString(), &ok);
	if(!ok || id.isEmpty())return;
	//open dialog
	MOrderAuditDialog::openVoucherAuditDialog(id);
}

void MOverview::userAudit()
{
	QString uid;
	QStringList names;
	//can we query the users?
	if(req->hasRight(req->RGetAllUsers)){
		MTGetAllUsers gau=req->queryGetAllUsers();
		if(!gau.hasError()){
			QList<MOUser>ul=gau.getusers();
			foreach(MOUser u,ul)names<<u.name();
		}else
			qDebug()<<"error while retrieving user names, will fall back to manual entering"<<gau.errorString();
	}
	//get user id
	QDialog d(this);
	d.setWindowTitle(tr("Audit User","audit dialog"));
	QFormLayout *fl;
	d.setLayout(fl=new QFormLayout);
	QLineEdit*uline=0;QComboBox*ucombo=0;
	if(names.size()>0){
		fl->addRow(tr("User Name:","audit dialog"),ucombo=new QComboBox);
		ucombo->addItems(names);
	}else{
		fl->addRow(tr("User Name:","audit dialog"),uline=new QLineEdit);
		uline->setText(req->currentUser());
	}
	QDateEdit *oldest;
	fl->addRow(tr("Earliest Info","audit dialog"),oldest=new QDateEdit);
	oldest->setDate(QDate::currentDate());
	oldest->setMaximumDate(QDate::currentDate());
	oldest->setCalendarPopup(true);
	fl->addRow(new QLabel("  "));
	QHBoxLayout*hl;
	fl->addRow(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton("Ok"));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton("Cancel"));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	if(d.exec()!=QDialog::Accepted)return;
	if(uline)uid=uline->text();
	if(ucombo)uid=ucombo->currentText();
	if(uid.isEmpty()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot query an empty user name.","audit dialog"));
		return;
	}
	//audit
	MOrderAuditDialog::openUserAuditDialog(uid,oldest->dateTime().toTime_t(),this);
}
