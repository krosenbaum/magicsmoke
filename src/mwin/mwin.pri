HEADERS += \
	mwin/tabwin.h \
	mwin/overview.h \
	mwin/aclwin.h \
	mwin/eventstab.h \
	mwin/carttab.h \
	mwin/orderstab.h \
	mwin/entrancetab.h \
	mwin/acltabs.h

SOURCES += \
	mwin/tabwin.cpp \
	mwin/overview.cpp \
	mwin/aclwin.cpp \
	mwin/eventstab.cpp \
	mwin/carttab.cpp \
	mwin/orderstab.cpp \
	mwin/entrancetab.cpp \
	mwin/acltabs.cpp

INCLUDEPATH += ./mwin