//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "checkdlg.h"
#include "msinterface.h"
#include "passwdchg.h"
#include "keygen.h"

#include "acltabs.h"

#include "MOHost"
#include "MOUser"
#include "MTChangePassword"
#include "MTCreateRole"
#include "MTCreateUser"
#include "MTCreateUser"
#include "MTDeleteFlag"
#include "MTDeleteHost"
#include "MTDeleteRole"
#include "MTDeleteUser"
#include "MTGetAllHostNames"
#include "MTGetAllHosts"
#include "MTGetAllRightNames"
#include "MTGetAllRoles"
#include "MTGetAllUsers"
#include "MTGetRole"
#include "MTGetUser"
#include "MTGetUserHosts"
#include "MTGetUserRoles"
#include "MTGetValidFlags"
#include "MTSetFlag"
#include "MTSetHost"
#include "MTSetRoleDescription"
#include "MTSetRoleFlags"
#include "MTSetRoleRights"
#include "MTSetUserDescription"
#include "MTSetUserFlags"
#include "MTSetUserHosts"
#include "MTSetUserRoles"

#include <QBoxLayout>
#include <QCryptographicHash>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTableView>
#include <misc.h>

MUserTab::MUserTab(QString pk)
{
	profilekey=pk;
	//user tab
	QHBoxLayout*hl;QVBoxLayout*vl;
	QPushButton*p;
	setLayout(hl=new QHBoxLayout);
	hl->addWidget(usertable=new QTableView,10);
	usertable->setModel(usermodel=new QStandardItemModel(this));
	usertable->setSelectionMode(QAbstractItemView::SingleSelection);
	usertable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	hl->addSpacing(5);
	hl->addLayout(vl=new QVBoxLayout,0);
	vl->addWidget(p=new QPushButton(tr("New User...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newUser()));
	p->setEnabled(req->hasRight(req->RCreateUser));
	vl->addWidget(p=new QPushButton(tr("Delete User...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(deleteUser()));
	p->setEnabled(req->hasRight(req->RDeleteUser));
	vl->addSpacing(20);
	vl->addWidget(p=new QPushButton(tr("Description...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editUserDescription()));
	p->setEnabled(req->hasRight(req->RSetUserDescription));
	vl->addWidget(p=new QPushButton(tr("Hosts...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editUserHosts()));
	p->setEnabled(req->hasRight(req->RGetUserHosts));
	vl->addWidget(p=new QPushButton(tr("Roles...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editUserRoles()));
	p->setEnabled(req->hasRight(req->RGetUserRoles));
	vl->addWidget(p=new QPushButton(tr("Flags...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editFlags()));
	p->setEnabled(req->hasRight(req->RSetRoleFlags));
	vl->addWidget(p=new QPushButton(tr("Set Password...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(setUserPassword()));
	p->setEnabled(req->hasRight(req->RChangePassword));
	vl->addStretch(10);
	
	if(req->hasRight(req->RGetAllUsers)){
		updateUsers();
	}else{
		setEnabled(false);
	}
}

static bool MOUser_Less(const MOUser&u1,const MOUser&u2)
{
	return u1.name().value()<u2.name().value();
}

void MUserTab::updateUsers()
{
	MTGetAllUsers au=req->queryGetAllUsers();
	if(au.hasError())return;
	QList<MOUser>usl=au.getusers();
	qSort(usl.begin(),usl.end(),MOUser_Less);
	usermodel->clear();
	usermodel->insertColumns(0,2);
	usermodel->insertRows(0,usl.size());
	usermodel->setHorizontalHeaderLabels(QStringList()<<tr("Login Name")<<tr("Description"));
	for(int i=0;i<usl.size();i++){
		usermodel->setData(usermodel->index(i,0),usl[i].name().value());
		usermodel->setData(usermodel->index(i,1),usl[i].description().value());
	}
	usertable->resizeColumnsToContents();
}

void MUserTab::newUser()
{
	//get name
	QString name;
	while(1){
		bool ok;
		name=QInputDialog::getText(this,tr("New User"),tr("Please enter new user name (only letters, digits, and underscore allowed):"),QLineEdit::Normal,name,&ok);
		if(!ok)
			return;
		if(QRegExp("[A-Za-z0-9_\\.,:-]+").exactMatch(name))
			break;
		if(QMessageBox::warning(this,tr("Error"),tr("The user name must contain only letters, digits, dots and underscores and must be at least one character long!"),QMessageBox::Retry|QMessageBox::Abort)!=QMessageBox::Retry)
			return;
	}
	//get password
	QString pwd=QInputDialog::getText(this,tr("Password"),tr("Please enter an initial password for the user:"),QLineEdit::Password);
	//send request
	req->queryCreateUser(name,pwd,"");
	//update display
	updateUsers();
}

void MUserTab::deleteUser()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	//make sure user wants this
	if(QMessageBox::question(this,tr("Delete User?"),tr("Really delete user '%1'?").arg(name),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//get replacement
	bool ok;
	QStringList rplc;
	rplc<<tr("(Nobody)","this is a username for no user, the string must contain '(' to distinguish it from the others");
	for(int i=0;i<usermodel->rowCount();i++)
		rplc<<usermodel->data(usermodel->index(i,0)).toString();
	QString rp=QInputDialog::getItem(this,tr("Delete User"),tr("Select which user will inherit this users database objects:"),rplc,0,false,&ok);
	if(!ok)return;
	//delete
	MTDeleteUser ret=req->queryDeleteUser(name,rp[0]=='('?"":rp);
	if(ret.hasError())
		QMessageBox::warning(this,tr("Error"),tr("Cannot delete user: %1").arg(ret.errorString()));
	updateUsers();
}

void MUserTab::editUserDescription()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	QString descr=usermodel->data(usermodel->index(sel.row(),1)).toString();
	//edit descr
	bool ok;
	descr=QInputDialog::getText(this,tr("Edit Description"),tr("Description of user %1:").arg(name),QLineEdit::Normal,descr,&ok);
	if(ok)
		req->querySetUserDescription(name,descr);
	//update
	updateUsers();
}

void MUserTab::editUserRoles()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	//...
	MTGetUserRoles gr=req->queryGetUserRoles(name);
	if(gr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve user roles: %1").arg(gr.errorString()));
		return;
	}
	MTGetAllRoles ar=req->queryGetAllRoles();
	if(ar.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve role descriptions: %1").arg(ar.errorString()));
		return;
	}
	MCheckList acl;
	QStringList urole=gr.getroles();
	QList<MORole>aroles=ar.getroles();
	for(int i=0;i<aroles.size();i++){
		QString nm=aroles[i].name();
		QString lb=nm+": "+aroles[i].description();
		acl<<MCheckItem(nm,urole.contains(nm),lb);
	}
	MCheckDialog cd(this,acl,tr("Edit Roles of user %1").arg(name));
	if(cd.exec()!=QDialog::Accepted)return;
	urole.clear();
	acl=cd.getCheckList();
	for(int i=0;i<acl.size();i++)
		if(acl[i].isSet())
			urole<<acl[i].key();
	MTSetUserRoles sur=MTSetUserRoles::query(name,urole);
	if(sur.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting users roles: %1").arg(sur.errorString()));
}

void MUserTab::editUserHosts()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	//...
	MTGetUserHosts gr=req->queryGetUserHosts(name);
	if(gr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve users hosts: %1").arg(gr.errorString()));
		return;
	}
	MTGetAllHostNames ar=req->queryGetAllHostNames();
	if(ar.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve host descriptions: %1").arg(ar.errorString()));
		return;
	}
	MCheckList acl;
	QStringList uhost=gr.gethosts();
	QStringList ahosts=ar.gethostnames();
	for(int i=0;i<ahosts.size();i++){
		QString nm=ahosts[i];
		acl<<MCheckItem(nm,uhost.contains(nm),nm);
	}
	MCheckDialog cd(this,acl,tr("Edit hosts of user %1").arg(name));
	if(cd.exec()!=QDialog::Accepted)return;
	uhost.clear();
	acl=cd.getCheckList();
	for(int i=0;i<acl.size();i++)
		if(acl[i].isSet())
			uhost<<acl[i].key();
	MTSetUserHosts sur=MTSetUserHosts::query(name,uhost);
	if(sur.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting users hosts: %1").arg(sur.errorString()));
}

void MUserTab::setUserPassword()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	//get new password
	MPasswordChange pc(this,name);
	if(pc.exec()!=QDialog::Accepted)return;
	QString p=pc.newPassword();
	if(p==""){
		QMessageBox::warning(this,tr("Warning"),tr("The password must be non-empty and both lines must match"));
		return;
	}
	//query
	MTChangePassword cp=req->queryChangePassword(name,p);
	if(cp.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting password: %1").arg(cp.errorString()));
	}
}

void MUserTab::editFlags()
{
	//get selection
	QModelIndex sel=usertable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=usermodel->data(usermodel->index(sel.row(),0)).toString();
	//...
	MTGetUser gr=req->queryGetUser(name);
	if(gr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve user data: %1").arg(gr.errorString()));
		return;
	}
	MTGetValidFlags gvf=req->queryGetValidFlags();
	if(gvf.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve flag list: %1").arg(gvf.errorString()));
		return;
	}
	QList<MOFlag> aflags=gvf.getflags();
	MCheckList acl;
	QStringList uflag=gr.getuser().value().flags().value().split(" ",QString::SkipEmptyParts);
	for(int i=0;i<aflags.size();i++){
		QString nm=aflags[i].flag();
		QString lb=nm+": "+aflags[i].description();
		acl<<MCheckItem(nm,uflag.contains(nm),lb);
	}
	MCheckDialog cd(this,acl,tr("Edit flags of user %1").arg(name));
	if(cd.exec()!=QDialog::Accepted)return;
	uflag.clear();
	acl=cd.getCheckList();
	for(int i=0;i<acl.size();i++)
		if(acl[i].isSet())
			uflag<<acl[i].key();
	MTSetUserFlags sur=MTSetUserFlags::query(name,uflag);
	if(sur.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting users flags: %1").arg(sur.errorString()));
}

/*****************************************************************************/

MHostTab::MHostTab(QString pk)
{
	profilekey=pk;
	
	QHBoxLayout*hl;QVBoxLayout*vl;
	QPushButton*p;
	//host tab
	setLayout(hl=new QHBoxLayout);
	hl->addWidget(hosttable=new QTableView,10);
	hosttable->setModel(hostmodel=new QStandardItemModel(this));
	hosttable->setSelectionMode(QAbstractItemView::SingleSelection);
	hosttable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	hl->addSpacing(5);
	hl->addLayout(vl=new QVBoxLayout,0);
	vl->addWidget(p=new QPushButton(tr("New Host...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newHost()));
	p->setEnabled(req->hasRight(req->RSetHost));
	vl->addWidget(p=new QPushButton(tr("Delete Host...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(deleteHost()));
	p->setEnabled(req->hasRight(req->RDeleteHost));
	vl->addSpacing(20);
	vl->addWidget(p=new QPushButton(tr("Generate New Key...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(changeHostKey()));
	p->setEnabled(req->hasRight(req->RSetHost));
	vl->addWidget(p=new QPushButton(tr("Import...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(importHost()));
	p->setEnabled(req->hasRight(req->RSetHost));
	vl->addWidget(p=new QPushButton(tr("Export...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(exportHost()));
	vl->addStretch(10);
	
	if(req->hasRight(req->RGetAllHosts)){
		updateHosts();
	}else{
		setEnabled(false);
	}
}



void MHostTab::updateHosts()
{
	MTGetAllHosts ah=req->queryGetAllHosts();
	QList<MOHost>hsl=ah.gethosts();
	hostmodel->clear();
	hostmodel->insertColumns(0,2);
	hostmodel->insertRows(0,hsl.size());
	hostmodel->setHorizontalHeaderLabels(QStringList()<<tr("Host Name")<<tr("Host Key"));
	for(int i=0;i<hsl.size();i++){
		hostmodel->setData(hostmodel->index(i,0),hsl[i].name().value());
		hostmodel->setData(hostmodel->index(i,1),hsl[i].key().value());
	}
	hosttable->resizeColumnsToContents();
}

void MHostTab::newHost()
{
	//get Host Name
	QString hname;
	do{
		bool ok;
		hname=QInputDialog::getText(this,tr("Create New Host"),tr("Please enter a host name:"),QLineEdit::Normal,hname,&ok);
		if(!ok)return;
		if(!QRegExp("[A-Za-z][A-Za-z0-9_]*").exactMatch(hname))continue;
	}while(false);
	//create it
	QString key;
	if(1){//limit visibility of key generator
		MKeyGen mkg;
		key=mkg.getKey();
		if(key=="")
			if(mkg.exec()!=QDialog::Accepted)
				return;
		key=mkg.getKey();
	}
        //save it
        exportKey(hname,key);
	//set it
	MTSetHost sh=MTSetHost::query(hname,key2hash(key));
	if(sh.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while creating new host: %1").arg(sh.errorString()));
		return;
	}
	//update
	updateHosts();
}

void MHostTab::deleteHost()
{
	//get selection
	QModelIndex sel=hosttable->currentIndex();
	if(!sel.isValid())return;
	//get hname
	QString name=hostmodel->data(hostmodel->index(sel.row(),0)).toString();
	//ask
	if(QMessageBox::question(this,tr("Delete this Host?"),tr("Really delete host '%1'?").arg(name),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//delete it
	MTDeleteHost dh=MTDeleteHost::query(name);
	if(dh.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while deleting host: %1").arg(dh.errorString()));
		return;
	}
	updateHosts();
}

void MHostTab::changeHostKey()
{
	//get selection
	QModelIndex sel=hosttable->currentIndex();
	if(!sel.isValid())return;
	//get hname
	QString name=hostmodel->data(hostmodel->index(sel.row(),0)).toString();
	//ask
	if(QMessageBox::question(this,tr("Change Host Key?"),tr("Really change the key of host '%1'? It will lock users from thist host out until you install the key at it.").arg(name), QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//change it
	QString key;
	if(1){//limit visibility of key generator
		MKeyGen mkg;
		key=mkg.getKey();
		if(key=="")
			if(mkg.exec()!=QDialog::Accepted)
				return;
		key=mkg.getKey();
	}
	//save as mshk file
	exportKey(name,key);
	//convert key to hash
	key=key2hash(key);
	//set it
	MTSetHost sh=MTSetHost::query(name,key);
	if(sh.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while changing host: %1").arg(sh.errorString()));
		return;
	}
	//update
	updateHosts();
}

void MHostTab::exportKey(QString name, QString key)
{
        QStringList fn;
        QFileDialog fdlg(this,tr("Export Key to File"),QString(),"Magic Smoke Host Key (*.mshk)");
        fdlg.setDefaultSuffix("mshk");
        fdlg.setAcceptMode(QFileDialog::AcceptSave);
        fdlg.setFileMode(QFileDialog::AnyFile);
        fdlg.setDirectory(currentDir());
        fdlg.selectFile(name+".mshk");
        if(!fdlg.exec())return;
        fn=fdlg.selectedFiles();
        if(fn.size()!=1)return;
        setCurrentDir(fn[0]);
        QFile fd(fn[0]);
        if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
                QMessageBox::warning(this,tr("Warning"),tr("Unable to open file %1 for writing: %2").arg(fn[0]).arg(fd.errorString()));
                return;
        }
        const QString chk=QCryptographicHash::hash(key.toLatin1(),QCryptographicHash::Md5).toHex();
        const QString out="MagicSmokeHostKey\n"+name+"\n"+key+"\n"+chk;
        fd.write(out.toLatin1());
        fd.close();
}

void MHostTab::importHost()
{
	QStringList fn;
	QFileDialog fdlg(this,tr("Import Key from File"),QString(),"Magic Smoke Host Key (*.mshk);;Magic Smoke Host Hash (*.mshh);;All Files (*)");
	fdlg.setDefaultSuffix("mshk");
	fdlg.setAcceptMode(QFileDialog::AcceptOpen);
	fdlg.setFileMode(QFileDialog::ExistingFile);
	fdlg.setDirectory(currentDir());
	if(!fdlg.exec())return;
	fn=fdlg.selectedFiles();
	if(fn.size()!=1)return;
	setCurrentDir(fn[0]);
	QFile fd(fn[0]);
	if(!fd.open(QIODevice::ReadOnly)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to open file %1 for reading: %2").arg(fn[0]).arg(fd.errorString()));
		return;
	}
	//read content (max: 10k to limit potential damage)
	QStringList fc=QString::fromLatin1(fd.read(10240)).split("\n",QString::SkipEmptyParts);
	fd.close();
	//check content
	if(fc.size()<3){
		QMessageBox::warning(this,tr("Warning"),tr("This is not a host key file."));
		return;
	}
	bool ishash = fc[0].trimmed()=="MagicSmokeHostHash";
	if(!ishash && fc[0].trimmed()!="MagicSmokeHostKey"){
		QMessageBox::warning(this,tr("Warning"),tr("This is not a host key/hash file."));
		return;
	}
	QString hname=fc[1].trimmed();
	if(!QRegExp("[A-Za-z][A-Za-z0-9_]*").exactMatch(hname)){
		QMessageBox::warning(this,tr("Warning"),tr("This host key file does not contain a valid host name."));
		return;
	}
	QString key=fc[2].trimmed();
	QString chk=QCryptographicHash::hash(key.toLatin1(),QCryptographicHash::Md5).toHex();
	if(chk!=fc[3].trimmed()){
		QMessageBox::warning(this,tr("Warning"),tr("The key check sum did not match. Please get a clean copy of the host key file."));
		return;
	}
	//convert to hash
	if(ishash){
		if(!QRegExp("[0-9a-fA-F]+ [0-9a-fA-F]{32,40}").exactMatch(key)){
			QMessageBox::warning(this,tr("Warning"),tr("This host hash file does not contain a valid key hash."));
			return;
		}
	}else{
		if(!QRegExp("[0-9a-fA-F]+").exactMatch(key) || key.size()<40){
			QMessageBox::warning(this,tr("Warning"),tr("This host key file does not contain a valid key."));
			return;
		}
		key=key2hash(key);
	}
	//save
	MTSetHost sh=MTSetHost::query(hname,key);
	if(sh.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while changing host: %1").arg(sh.errorString()));
		return;
	}
	updateHosts();
}

QString MHostTab::key2hash(QString key)
{
	QString salt;
	MKeyGen mkg;
	salt=mkg.getKey(8);
	if(salt=="")
		if(mkg.exec()!=QDialog::Accepted)
			return key;
	salt=mkg.getKey(8);
	return salt+" "+ QCryptographicHash::hash((salt+key).toLatin1(),QCryptographicHash::Sha1).toHex();
}


void MHostTab::exportHost()
{
	//get selection
	QModelIndex sel=hosttable->currentIndex();
	if(!sel.isValid())return;
	//get hname
	QString name=hostmodel->data(hostmodel->index(sel.row(),0)).toString();
	QString key=hostmodel->data(hostmodel->index(sel.row(),1)).toString();
	if(name.isEmpty() || name[0]=='_' || key==""){
		QMessageBox::warning(this,tr("Warning"),tr("This host cannot be exported."));
		return;
	}
	//save
	QStringList fn;
	QFileDialog fdlg(this,tr("Export Hash to File"),QString(),"Magic Smoke Host Hash (*.mshh)");
	fdlg.setDefaultSuffix("mshh");
	fdlg.setAcceptMode(QFileDialog::AcceptSave);
	fdlg.setFileMode(QFileDialog::AnyFile);
	fdlg.setDirectory(currentDir());
	if(!fdlg.exec())return;
	fn=fdlg.selectedFiles();
	if(fn.size()!=1)return;
	setCurrentDir(fn[0]);
	QFile fd(fn[0]);
	if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to open file %1 for writing: %2").arg(fn[0]).arg(fd.errorString()));
		return;
	}
	QString chk=QCryptographicHash::hash(key.toLatin1(),QCryptographicHash::Md5).toHex();
	QString out="MagicSmokeHostHash\n"+name+"\n"+key+"\n"+chk;
	fd.write(out.toLatin1());
	fd.close();
}

/*****************************************************************************/

MRoleTab::MRoleTab(QString pk)
{
	profilekey=pk;
	
	QHBoxLayout*hl;QVBoxLayout*vl;
	QPushButton*p;
	//host tab
	setLayout(hl=new QHBoxLayout);
	hl->addWidget(roletable=new QTableView,10);
	roletable->setModel(rolemodel=new QStandardItemModel(this));
	roletable->setSelectionMode(QAbstractItemView::SingleSelection);
	roletable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	hl->addSpacing(5);
	hl->addLayout(vl=new QVBoxLayout,0);
	vl->addWidget(p=new QPushButton(tr("New Role...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(newRole()));
	p->setEnabled(req->hasRight(req->RCreateRole));
	vl->addWidget(p=new QPushButton(tr("Delete Role...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(deleteRole()));
	p->setEnabled(req->hasRight(req->RDeleteRole));
	vl->addSpacing(20);
	vl->addWidget(p=new QPushButton(tr("Change Description...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editDescription()));
	p->setEnabled(req->hasRight(req->RSetRoleDescription));
	vl->addWidget(p=new QPushButton(tr("Edit Flags...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editFlags()));
	p->setEnabled(req->hasRight(req->RSetRoleFlags));
	vl->addWidget(p=new QPushButton(tr("Edit Rights...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(editRights()));
	p->setEnabled(req->hasRight(req->RSetRoleRights));
	vl->addStretch(10);
	
	if(req->hasRight(req->RGetAllRoles)){
		updateRoles();
	}else{
		setEnabled(false);
	}
}



void MRoleTab::updateRoles()
{
	QString thisHost=req->hostName();
	MTGetAllRoles ah=req->queryGetAllRoles();
	QList<MORole>rsl=ah.getroles();
	rolemodel->clear();
	rolemodel->insertColumns(0,2);
	rolemodel->insertRows(0,rsl.size());
	rolemodel->setHorizontalHeaderLabels(QStringList()<<tr("Role Name")<<tr("Description"));
	for(int i=0;i<rsl.size();i++){
		rolemodel->setData(rolemodel->index(i,0),rsl[i].name().value());
		rolemodel->setData(rolemodel->index(i,1),rsl[i].description().value());
	}
	roletable->resizeColumnsToContents();
}

void MRoleTab::newRole()
{
	//get Role Name
	QString rname;
	do{
		bool ok;
		rname=QInputDialog::getText(this,tr("Create New Role"),tr("Please enter a role name:"),QLineEdit::Normal,rname,&ok);
		if(!ok)return;
		if(!QRegExp("[A-Za-z][A-Za-z0-9-_\\.]*").exactMatch(rname))continue;
	}while(false);
	//create it
	MTCreateRole cr=MTCreateRole::query(rname);
	if(cr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while trying to create role: %1").arg(cr.errorString()));
		return;
	}
	//update
	updateRoles();
}
void MRoleTab::deleteRole()
{
	//get selection
	QModelIndex sel=roletable->currentIndex();
	if(!sel.isValid())return;
	//get hname
	QString name=rolemodel->data(rolemodel->index(sel.row(),0)).toString();
	//ask
	if(QMessageBox::question(this,tr("Delete this Role?"),tr("Really delete role '%1'?").arg(name),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//delete it
	MTDeleteRole dr=MTDeleteRole::query(name);
	if(dr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while trying to delete role: %1").arg(dr.errorString()));
		return;
	}
	updateRoles();
}
void MRoleTab::editDescription()
{
	//get selection
	QModelIndex sel=roletable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=rolemodel->data(rolemodel->index(sel.row(),0)).toString();
	QString descr=rolemodel->data(rolemodel->index(sel.row(),1)).toString();
	//edit descr
	bool ok;
	descr=QInputDialog::getText(this,tr("Edit Description"),tr("Description of role %1:").arg(name),QLineEdit::Normal,descr,&ok);
	if(ok)
		req->querySetRoleDescription(name,descr);
	//update
	updateRoles();
}
void MRoleTab::editFlags()
{
	//get selection
	QModelIndex sel=roletable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=rolemodel->data(rolemodel->index(sel.row(),0)).toString();
	//...
	MTGetRole gr=req->queryGetRole(name);
	if(gr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve role: %1").arg(gr.errorString()));
		return;
	}
	MTGetValidFlags gvf=req->queryGetValidFlags();
	if(gvf.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve flag list: %1").arg(gvf.errorString()));
		return;
	}
	MCheckList acl;
	QStringList rflags=gr.getrole().value().flags().value().split(" ",QString::SkipEmptyParts);
	QList<MOFlag> aflags=gvf.getflags();
	for(int i=0;i<aflags.size();i++){
		QString nm=aflags[i].flag();
		QString lb=nm+": "+aflags[i].description();
		acl<<MCheckItem(nm,rflags.contains(nm),lb);
	}
	MCheckDialog cd(this,acl,tr("Edit flags of role %1").arg(name));
	if(cd.exec()!=QDialog::Accepted)return;
	acl=cd.getCheckList();
	rflags.clear();
	for(int i=0;i<acl.size();i++)
		if(acl[i].isSet())
			rflags<<acl[i].key();
	MTSetRoleFlags srr=MTSetRoleFlags::query(name,rflags);
	if(srr.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting flags: %1").arg(srr.errorString()));
}
void MRoleTab::editRights()
{
	//get selection
	QModelIndex sel=roletable->currentIndex();
	if(!sel.isValid())return;
	//get uname & descr
	QString name=rolemodel->data(rolemodel->index(sel.row(),0)).toString();
	//...
	MTGetRole gr=req->queryGetRole(name);
	if(gr.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve role: %1").arg(gr.errorString()));
		return;
	}
	MTGetAllRightNames ar=req->queryGetAllRightNames();
	if(ar.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Cannot retrieve right list: %1").arg(ar.errorString()));
		return;
	}
	MCheckList acl;
	QStringList rrights=gr.getrole().value().rights();
	QStringList arights=ar.getrights();
	qSort(arights);
	for(int i=0;i<arights.size();i++){
		QString nm=arights[i];
		QString lb;
		MInterface::Right rg=req->stringToRight(nm);
		if(rg!=MInterface::NoRight){
			lb=req->rightToLocalString(rg);
			if(nm!=lb)lb=nm+": "+lb;
		}else lb=nm;
		acl<<MCheckItem(nm,rrights.contains(nm),lb);
	}
	MCheckDialog cd(this,acl,tr("Edit rights of role %1").arg(name));
	if(cd.exec()!=QDialog::Accepted)return;
	acl=cd.getCheckList();
	rrights.clear();
	for(int i=0;i<acl.size();i++)
		if(acl[i].isSet())
			rrights<<acl[i].key();
	MTSetRoleRights srr=MTSetRoleRights::query(name,rrights);
	if(srr.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while setting rights: %1").arg(srr.errorString()));
}

/*****************************************************************************/

MFlagTab::MFlagTab(QString pk)
{
	profilekey=pk;
	
	QHBoxLayout*hl;QVBoxLayout*vl;
	QPushButton*p;
	//host tab
	setLayout(hl=new QHBoxLayout);
	hl->addWidget(table=new QTableView,10);
	table->setModel(model=new QStandardItemModel(this));
	table->setSelectionMode(QAbstractItemView::SingleSelection);
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	hl->addSpacing(5);
	hl->addLayout(vl=new QVBoxLayout,0);
	vl->addWidget(p=new QPushButton(tr("New Flag...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(addFlag()));
	p->setEnabled(req->hasRight(req->RSetFlag));
	vl->addWidget(p=new QPushButton(tr("Delete Flag...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(removeFlag()));
	p->setEnabled(req->hasRight(req->RDeleteFlag));
	vl->addSpacing(20);
	vl->addWidget(p=new QPushButton(tr("Change Description...")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(changeFlag()));
	p->setEnabled(req->hasRight(req->RSetFlag));
	vl->addStretch(10);
	
	if(req->hasRight(req->RGetValidFlags)){
		updateFlags();
	}else{
		setEnabled(false);
	}
}

void MFlagTab::updateFlags()
{
	MTGetValidFlags gvf=MTGetValidFlags::query();
	if(gvf.hasError())return;
	model->clear();
	model->insertColumns(0,2);
	model->setHorizontalHeaderLabels(QStringList()<<tr("Flag")<<tr("Description"));
	QList<MOFlag> flg=gvf.getflags();
	model->insertRows(0,flg.size());
	for(int i=0;i<flg.size();i++){
		model->setData(model->index(i,0),flg[i].flag().value());
		model->setData(model->index(i,1),flg[i].description().value());
	}
	table->resizeColumnsToContents();
}

void MFlagTab::addFlag()
{
	QString flg;
	do{
		bool ok;
		flg=QInputDialog::getText(this,tr("Create New Flag"),tr("Please enter a name for the flag, it must contain only letters and digits."),QLineEdit::Normal,flg,&ok);
		if(!ok)return;
		if(QRegExp("[a-zA-Z0-9]+").exactMatch(flg))break;
	}while(1);
	MOFlag f;
	f.setflag(flg);
	MTSetFlag cf=MTSetFlag::query(f);
	if(cf.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while creating flag: %1").arg(cf.errorString()));
	else
		updateFlags();
}

void MFlagTab::removeFlag()
{
	//get flag
	QModelIndex idx=table->currentIndex();
	if(!idx.isValid())return;
	QString flg=model->data(model->index(idx.row(),0)).toString();
	//sanity check
	if(flg=="")return;
	if(flg[0]=='_')return;
	//ask nicely (a'ka "annoy user")
	if(QMessageBox::question(this,tr("Really Delete?"),tr("Really delete the flag '%1'? Doing so may make some entities visible or invisible unexpectedly.").arg(flg),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes)return;
	//now attempt deletion
	MTDeleteFlag df=MTDeleteFlag::query(flg);
	if(df.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while attempting to delete flag: %1").arg(df.errorString()));
	else
		updateFlags();
}

void MFlagTab::changeFlag()
{
	//get flag
	QModelIndex idx=table->currentIndex();
	if(!idx.isValid())return;
	QString flg=model->data(model->index(idx.row(),0)).toString();
	QString d=model->data(model->index(idx.row(),1)).toString();
	//get new text
	bool ok;
	d=QInputDialog::getText(this,tr("Change Flag"),tr("Please enter a new description for flag '%1':").arg(flg),QLineEdit::Normal,d,&ok);
	if(!ok)return;
	//do it
	MOFlag f;f.setflag(flg);f.setdescription(d);
	MTSetFlag sf=MTSetFlag::query(f);
	if(sf.hasError())
		QMessageBox::warning(this,tr("Warning"),tr("Error while attempting to alter flag: %1").arg(sf.errorString()));
	else
		updateFlags();
}
