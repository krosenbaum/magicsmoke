//
// C++ Implementation: overview
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "centbox.h"
#include "main.h"
#include "misc.h"
#include "msinterface.h"
#include "orderwin.h"
#include "customerdlg.h"

#include "carttab.h"

#include "MTGetAllShipping"
#include "MTGetCoupon"
#include "MTGetEvent"
#include "MTGetValidVoucherPrices"
#include "MTGetVoucher"
#include "MTCreateOrder"
#include "MTCreateReservation"

#include "MOCoupon"
#include "MOCouponRule"

#include <QApplication>
#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QFrame>
#include <QGridLayout>
#include <QInputDialog>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextEdit>

MCartTab::MCartTab(QString pk)
{
	profilekey=pk;
	deliveryaddrid=invoiceaddrid=-1;

	QVBoxLayout*vl;QHBoxLayout*hl;
	QPushButton*p;
	QLabel*lab;
	QFrame*frm;

	//Shopping Cart Tab
	setLayout(vl=new QVBoxLayout);
	vl->addLayout(hl=new QHBoxLayout);
	QVBoxLayout*vl2;
	hl->addLayout(vl2=new QVBoxLayout,2);
	vl2->addWidget(carttable=new QTableView,10);
	carttable->setModel(cartmodel=new QStandardItemModel(this));
	carttable->setSelectionMode(QAbstractItemView::SingleSelection);
	carttable->setItemDelegate(new MCartTableDelegate(this));
	connect(cartmodel,SIGNAL(itemChanged(QStandardItem*)),this,SLOT(updatePrice()));
	QHBoxLayout*hl2;
	vl2->addLayout(hl2=new QHBoxLayout,0);
	hl2->addStretch(10);
	hl2->addWidget(p=new QPushButton(tr("Add Ticket")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(cartAddTicket()));
	hl2->addWidget(p=new QPushButton(tr("Add Voucher")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(cartAddVoucher()));
	hl2->addWidget(p=new QPushButton(tr("Add Shop Item")),0);
	//TODO: implement items
	p->setEnabled(false);
	connect(p,SIGNAL(clicked()),this,SLOT(cartAddItem()));
	hl2->addSpacing(15);
	hl2->addWidget(p=new QPushButton(tr("Remove Line")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(cartRemoveItem()));
	vl2->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::HLine);
	QFormLayout *fl;
	vl2->addLayout(fl=new QFormLayout,0);
	fl->addRow(lab=new QLabel(tr("Total Price Sum:")),price=new QLabel("0.0"));
	QFont fnt=lab->font();
	fnt.setPointSizeF(fnt.pointSizeF()*1.5);
	lab->setFont(fnt);
	price->setFont(fnt);
	fl->addRow(tr("Coupon Code:"),hl2=new QHBoxLayout);
	hl2->addWidget(coupon=new QLabel(tr("(none)","coupon code label")),1);
	hl2->addWidget(p=new QPushButton(tr("Add Coupon...")),0);
	connect(p,&QPushButton::clicked,this,[&](){changeVoucher("vadd:coupon");});
	connect(coupon,SIGNAL(linkActivated(const QString&)),this,SLOT(changeVoucher(QString)));
	fl->addRow(tr("Pay with Vouchers:"),hl2=new QHBoxLayout);
	hl2->addWidget(vouchers=new QLabel("..."),1);
	hl2->addWidget(p=new QPushButton(tr("Add Voucher...")),0);
	connect(p,&QPushButton::clicked,this,[&](){changeVoucher("vadd:voucher");});
	connect(vouchers,SIGNAL(linkActivated(const QString&)),this,SLOT(changeVoucher(QString)));
	fl->addRow(tr("Remaining Sum:"),priceremain=new QLabel("0.0"));
	//hl2->addStretch(1);

	hl->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::VLine);
	hl->addLayout(vl2=new QVBoxLayout,1);
	vl2->addWidget(p=new QPushButton(tr("Customer:")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(setCustomer()));
	vl2->addWidget(cartcustomer=new QLabel("..."));
	cartcustomer->setAutoFillBackground(true);
	vl2->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::HLine);
	vl2->addWidget(p=new QPushButton(tr("Invoice Address:")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(setInvoiceAddr()));
	vl2->addWidget(invoiceaddr=new QLabel("..."));
	invoiceaddr->setAutoFillBackground(true);
	vl2->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::HLine);
	vl2->addWidget(p=new QPushButton(tr("Delivery Address:")),0);
	connect(p,SIGNAL(clicked()),this,SLOT(setDeliveryAddr()));
	vl2->addWidget(deliveryaddr=new QLabel("..."));
	deliveryaddr->setAutoFillBackground(true);
	vl2->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::HLine);
	vl2->addSpacing(10);
	vl2->addWidget(new QLabel(tr("Shipping Method:")),0);
	vl2->addWidget(cartship=new QComboBox,0);
	cartship->setEditable(false);
	cartship->setMinimumContentsLength(15);
	cartship->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	connect(cartship,SIGNAL(currentIndexChanged(int)),this,SLOT(updatePrice()));
	vl2->addSpacing(10);
	vl2->addWidget(new QLabel(tr("Comments:")),0);
	vl2->addWidget(cartcomment=new QTextEdit);
	vl2->addStretch(10);
	vl->addWidget(frm=new QFrame,0);
	frm->setFrameShape(QFrame::HLine);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Order")));
	p->setEnabled(req->hasRight(req->RCreateOrder)&&req->hasRight(req->PCreateOrder_CanOrder));
	connect(p,SIGNAL(clicked()),this,SLOT(cartOrder()));
	hl->addWidget(p=new QPushButton(tr("Sell")));
	p->setEnabled(req->hasRight(req->RCreateOrder)&&req->hasRight(req->PCreateOrder_CanSell));
	connect(p,SIGNAL(clicked()),this,SLOT(cartSell()));
	hl->addWidget(p=new QPushButton(tr("Reserve")));
	p->setEnabled(req->hasRight(req->RCreateReservation));
	connect(p,SIGNAL(clicked()),this,SLOT(cartReserve()));
	hl->addWidget(p=new QPushButton(tr("Clear")));
	connect(p,SIGNAL(clicked()),this,SLOT(initCart()));

	//fill tables
	if(!req->hasRight(req->RCreateOrder)&&!req->hasRight(req->RCreateReservation)){
		setEnabled(false);
	}else{
		initCart();
	}
	updateShipping();
}

QList<QMenu*>MCartTab::menu()
{
	QList<QMenu*>ret;
	QMenu*m=new QMenu(tr("Ca&rt"));
	ret<<m;
	m->addAction(tr("Add &Ticket"),this,SLOT(cartAddTicket()));
	m->addAction(tr("Add &Voucher"),this,SLOT(cartAddVoucher()));
	m->addAction(tr("Add &Shop-Item"),this,SLOT(cartAddItem()));
	m->addAction(tr("&Remove Line"),this,SLOT(cartRemoveItem()));
	m->addAction(tr("&Abort Shopping"),this,SLOT(initCart()));
	m->addSeparator();
	m->addAction(tr("&Update Shipping Options"),this,SLOT(updateShipping()));
	return ret;
}

MCartTab::~MCartTab()
{
}

static const int SHIPPING_IDROLE = Qt::UserRole;
static const int SHIPPING_PRICEROLE = Qt::UserRole+1;

void MCartTab::updateShipping()
{
	cartship->clear();
	cartship->addItem(tr("(No Shipping)"),-1);
	MTGetAllShipping sh=MTGetAllShipping::query();
	QList<MOShipping>ship=sh.getshipping();
	MSInterface*ifc=MSInterface::instance();
	for(int i=0;i<ship.size();i++){
		if(ifc)if(!ifc->checkFlags(ship[i].flags()))continue;
		cartship->addItem(ship[i].description());
		cartship->setItemData(cartship->count()-1,(int)ship[i].shipid(),SHIPPING_IDROLE);
		cartship->setItemData(cartship->count()-1,(int)ship[i].cost(),SHIPPING_PRICEROLE);
	}
	QPalette pal2=QComboBox().palette();
	cartship->setPalette(pal2);
	cartship->setToolTip(QString());
}

#define COL_AMOUNT 0
#define COL_TITLE 1
#define COL_START 2
#define COL_PRICE 3
#define COL_NOTES 4

void MCartTab::initCart()
{
	//clear cart
	cartmodel->clear();
	cartmodel->setHorizontalHeaderLabels(QStringList()<<tr("Amount")<<tr("Title")<<tr("Start Time")<<tr("Price")<<tr("Notes"));
	//clear customer
	customer=MOCustomer();
	cartcustomer->setText("");
	//clear address
	deliveryaddr->setText("");
	invoiceaddr->setText("");
	deliveryaddrid=invoiceaddrid=-1;
	//clear comment
	cartcomment->setPlainText("");
	//clear shipping
	cartship->setCurrentIndex(0);
	//reset colors
	resetColor();
	//reset coupon and voucher labels
	couponinfo=MOCoupon();
	vouchersforpay.clear();
	//update price display
	updatePrice();
}

void MCartTab::resetColor(bool addronly)
{
	QPalette pal=QLabel().palette();
	cartcustomer->setPalette(pal);
	cartcustomer->setToolTip(QString());
	invoiceaddr->setPalette(pal);
	invoiceaddr->setToolTip(QString());
	deliveryaddr->setPalette(pal);
	deliveryaddr->setToolTip(QString());
	if(!addronly){
		for(int i=0;i<cartmodel->rowCount();i++)
			for(int j=0;j<cartmodel->columnCount();j++){
				QModelIndex idx=cartmodel->index(i,j);
				cartmodel->setData(idx,QVariant(),Qt::ToolTipRole);
				cartmodel->setData(idx,QVariant(),Qt::BackgroundRole);
			}
		QPalette pal2=QComboBox().palette();
		cartship->setPalette(pal2);
		cartship->setToolTip(QString());
	}
}

void MCartTab::setCustomer()
{
	MCustomerListDialog mcl(this,true,customer.id());
	if(mcl.exec()!=QDialog::Accepted)return;
	customer=mcl.getCustomer();
	cartcustomer->setText(customer.fullName());
	//clear address
	deliveryaddr->setText("");
	invoiceaddr->setText("");
	deliveryaddrid=invoiceaddrid=-1;
	resetColor(true);
	//follow up with address dialog
	if(!customer.customerid().isNull())
		setInvoiceAddr();
}

void MCartTab::setDeliveryAddr()
{
	if(customer.customerid().isNull()){
		QMessageBox::warning(this,tr("Warning"),tr("Please set the customer first."));
		return;
	}
	MAddressChoiceDialog d(this,customer);
	if(d.exec()!=QDialog::Accepted)return;
	customer=d.customer();
	deliveryaddr->setText(d.address().fullAddress(customer.fullName()));
	deliveryaddrid=d.addressId();
	deliveryaddr->setPalette(QLabel().palette());
	deliveryaddr->setToolTip(QString());
}

void MCartTab::setInvoiceAddr()
{
	if(customer.customerid().isNull()){
		//customer not set yet, chose it
		setCustomer();
		//the setCustomer routine will recurse back to here
		return;
	}
	MAddressChoiceDialog d(this,customer);
	if(d.exec()!=QDialog::Accepted)return;
	customer=d.customer();
	invoiceaddr->setText(d.address().fullAddress(customer.fullName()));
	invoiceaddrid=d.addressId();
	invoiceaddr->setPalette(QLabel().palette());
	invoiceaddr->setToolTip(QString());
}

static const int CART_TICKET=1;
static const int CART_VOUCHER=2;

static const int CART_IDROLE=Qt::UserRole;//ticket id
static const int CART_PRICEIDROLE=Qt::UserRole+3;//for tickets: price category id
static const int CART_ORIGPRICEIDROLE=Qt::UserRole+5;//for tickets: original price ID if it has been changed by a coupon
static const int CART_EVENTDATAROLE=Qt::UserRole+6;//for tickets: the full event
static const int CART_PRICEROLE=Qt::UserRole+4;//voucher/ticket price
static const int CART_VALUEROLE=Qt::UserRole+1;//voucher value
static const int CART_TYPEROLE=Qt::UserRole+2;//which is it? ticket or voucher?
static const int CART_NOTECOPY=Qt::UserRole+7;//a copy of the original note field, in case it gets overwritten by an error

void MCartTab::cartAddTicket()
{
	//create ticket selection dialog
	int id=-1;bool ok=false;
	getEventId(id,ok);
	if(id<0 || !ok)return;
	addTicketForEvent(id);
}

void MCartTab::addTicketForEvent(qint64 id, qint64 prcid)
{
	//get event
	MTGetEvent ge=MTGetEvent::query(id);
	if(ge.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error getting event, please try again."));
		return;
	}
	MOEvent ev=ge.getevent();
	//get Price Category
	QList<MOEventPrice> ep=ev.priceFiltered();
	if(ep.size()==0){
		QMessageBox::warning(this,tr("Warning"),tr("This event has no prices associated that you can use for sales. Cannot sell tickets."));
		return;
	}
	int pcidx=0;
	if(ep.size()>1){
		QDialog d(this);
		d.setWindowTitle(tr("Select Price Category"));
		QVBoxLayout*vl;
		QHBoxLayout*hl;
		QComboBox*box;
		QPushButton*p;
		d.setLayout(vl=new QVBoxLayout);
		vl->addWidget(new QLabel(tr("Please chose a price category:")));
		vl->addWidget(box=new QComboBox);
		int prcidx=-1;
		for(int i=0;i<ep.size();i++){
			box->addItem(ep[i].pricecategory().value().name().value()+": "+cent2str(ep[i].price()));
			if(ep[i].pricecategoryid()==prcid)
				prcidx=i;
		}
		if(prcidx>=0)box->setCurrentIndex(prcidx);
		box->setEditable(false);
		vl->addSpacing(10);
		vl->addStretch(10);
		vl->addLayout(hl=new QHBoxLayout);
		hl->addStretch(10);
		hl->addWidget(p=new QPushButton(tr("Ok")));
		connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
		hl->addWidget(p=new QPushButton(tr("Cancel")));
		connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
		if(d.exec()!=QDialog::Accepted)return;
		pcidx=box->currentIndex();
	}
	//copy to cart
	int cr=cartmodel->rowCount();
	cartmodel->insertRows(cr,1);
	cartmodel->setData(cartmodel->index(cr,0),1);
	cartmodel->setData(cartmodel->index(cr,0),id,CART_IDROLE);
	cartmodel->setData(cartmodel->index(cr,0),CART_TICKET,CART_TYPEROLE);
	cartmodel->setData(cartmodel->index(cr,0),ep[pcidx].pricecategoryid().value(),CART_PRICEIDROLE);
	cartmodel->setData(cartmodel->index(cr,0),ep[pcidx].pricecategoryid().value(),CART_ORIGPRICEIDROLE);
	cartmodel->setData(cartmodel->index(cr,0),ep[pcidx].price().value(),CART_PRICEROLE);
	cartmodel->setData(cartmodel->index(cr,0),QVariant::fromValue(ev),CART_EVENTDATAROLE);
	cartmodel->setData(cartmodel->index(cr,1),ev.title().value());
	cartmodel->setData(cartmodel->index(cr,2),ev.startTimeString());
	QString pcn=cent2str(ep[pcidx].price())+" ("+ep[pcidx].pricecategory().value().name().value()+")";
	cartmodel->setData(cartmodel->index(cr,3),pcn);
	carttable->resizeColumnsToContents();
	//adjust coupon calcs
	applyCoupon();
}

void MCartTab::eventOrderTicket()
{
	int id=emit currentEventId();
	if(id<0)return;
	//activate order tab
	emit requestFocus();
	//insert event into table
	addTicketForEvent(id);
}

void MCartTab::eventOrderTicket(qint64 id,qint64 prcid)
{
	qDebug()<<"xxxx";
	//activate order tab
	emit requestFocus();
	//insert event into table
	addTicketForEvent(id,prcid);
}

void MCartTab::cartAddVoucher()
{
	//check privileges
	if(!req->hasRight(MInterface::PCreateOrder_CanPayVoucherWithVoucher)){
		bool vfound=false;
		for(int r=0;r<cartmodel->rowCount();r++)
			if(cartmodel->data(cartmodel->index(r,0),CART_TYPEROLE).toInt()==CART_VOUCHER){
				vfound=true;
				break;
			}
		if(vfound){
			QMessageBox::warning(this,tr("Warning"),tr("Cannot pay for vouchers with another voucher!"));
			return;
		}
	}
	//create voucher selection dialog
	QDialog dlg;
	dlg.setWindowTitle(tr("Select Voucher"));
	QComboBox*cp,*cv;
	QHBoxLayout*hl;
	QGridLayout*gl;
	QStandardItemModel mdl;
	QRegularExpressionValidator regv(priceRegExp(),this);
	QList<qint64>prc=MTGetValidVoucherPrices::query().getprices();
	mdl.insertRows(0,prc.size());
	mdl.insertColumns(0,1);
	for(int i=0;i<prc.size();i++)mdl.setData(mdl.index(i,0),cent2str(prc[i]));
	dlg.setLayout(gl=new QGridLayout);
	gl->addWidget(new QLabel(tr("Select voucher price and value:")),0,0,1,2);
	if(req->hasRight(req->PCreateOrder_DiffVoucherValuePrice)){
		gl->addWidget(new QLabel(tr("Price:")),1,0);
		gl->addWidget(cp=new QComboBox,1,1);
		cp->setModel(&mdl);
		cp->setEditable(true);
		cp->setValidator(&regv);
	}else cp=0;
	gl->addWidget(new QLabel(tr("Value:")),2,0);
	gl->addWidget(cv=new QComboBox,2,1);
	cv->setModel(&mdl);
	cv->setEditable(req->hasRight(req->PCreateOrder_AnyVoucherValue));
	cv->setValidator(&regv);
	gl->setRowMinimumHeight(3,15);
	gl->setColumnStretch(0,0);
	gl->setColumnStretch(1,1);
	gl->setRowStretch(3,1);
	gl->addLayout(hl=new QHBoxLayout,4,0,1,2);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")),0);
	connect(p,SIGNAL(clicked()),&dlg,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&dlg,SLOT(reject()));
	//wait for it
	if(dlg.exec()==QDialog::Accepted){
		//get selection
		int price,value;
		value=str2cent(cv->currentText());
		if(req->hasRight(req->PCreateOrder_DiffVoucherValuePrice) && cp)
			price=str2cent(cp->currentText());
		else
			price=value;
		//copy to cart
		int cr=cartmodel->rowCount();
		cartmodel->insertRows(cr,1);
		cartmodel->setData(cartmodel->index(cr,0),1);
		cartmodel->setData(cartmodel->index(cr,0),price,CART_PRICEROLE);
		cartmodel->setData(cartmodel->index(cr,0),value,CART_VALUEROLE);
		cartmodel->setData(cartmodel->index(cr,0),CART_VOUCHER,CART_TYPEROLE);
		cartmodel->setData(cartmodel->index(cr,1),tr("Voucher (value %1)").arg(cent2str(value)));
		cartmodel->setData(cartmodel->index(cr,3),cent2str(price));
		carttable->resizeColumnsToContents();
	}
}

void MCartTab::cartAddItem()
{
	//TODO: implement items
}

void MCartTab::cartRemoveItem()
{
	//get selection
	QModelIndex idx=carttable->currentIndex();
	if(!idx.isValid())return;
	//remove row
	cartmodel->removeRow(idx.row());
	//update display
	updatePrice();
}

void MCartTab::cartOrder(bool isreserve,bool issale)
{
	//sanity checks
	if(!canorder(isreserve))return;
	resetColor(false);
	///////////////
	//create order
	MOCartOrder cord;
	cord.setcustomerid(customer.id());
	//is there a delivery address
	if(deliveryaddrid>=0)cord.setdeliveryaddressid(deliveryaddrid);
	if(invoiceaddrid>=0)cord.setinvoiceaddressid(invoiceaddrid);
	//is there a comment?
	QString s=cartcomment->toPlainText().trimmed();
	if(s!="")cord.setcomment(s);
	//coupon?
	if(!couponinfo.couponid().isNull())
		cord.setcouponid(couponinfo.couponid());
	//scan tickets & scan vouchers
	cartTableToOrder(cord);
	//set shipping info
	if(cartship->currentIndex()>0)
		cord.setshippingtypeid(cartship->itemData(cartship->currentIndex(),SHIPPING_IDROLE).toInt());
	/////
	//send
	MOOrder ord;
	if(!orderExecute(cord,ord,isreserve,issale))return;
	/////
	//everything ok, move on
	if(!ord.orderid().isNull()){
		initCart();
		MOrderWindow *ow=new MOrderWindow(window(),ord);
		ow->show();
		return;
	}
	/////
	//something went wrong: verify cart
	QMessageBox::warning(this,tr("Warning"),tr("There are problems with the contents of the cart, please check and then try again."));
	//verify customer
	verifyOrderCustomer(cord);
	//verify tickets
	verifyOrderTickets(cord.tickets());
	verifyOrderVouchers(cord.vouchers());
	verifyOrderItems(cord.items());
}

void MCartTab::cartReserve()
{
	cartOrder(true);
}

void MCartTab::cartSell()
{
	cartOrder(false,true);
}

bool MCartTab::canorder(bool isreserve)
{
	//basics
	if(cartmodel->rowCount()<1){
		QMessageBox::warning(this,tr("Error"),tr("There is nothing in the order. Ignoring it."));
		return false;
	}
	if(!customer.isValid()){
		QMessageBox::warning(this,tr("Error"),tr("Please chose a customer first!"));
		return false;
	}
	if(cartship->currentIndex()>0 && deliveryaddrid<0 && invoiceaddrid<0){
		if(QMessageBox::question(this,tr("Shipping"),tr("You have chosen a shipping method, but no address. Are you sure you want to continue?"),QMessageBox::Yes|QMessageBox::No) != QMessageBox::Yes)return false;
	}
	//if for a reservation: tickets only please
	if(isreserve)
	for(int i=0;i<cartmodel->rowCount();i++){
		int tp=cartmodel->data(cartmodel->index(i,0),CART_TYPEROLE).toInt();
		if(tp!=CART_TICKET){
			QMessageBox::warning(this,tr("Warning"),tr("Reservations can only contain tickets."));
			return false;
		}
		if(vouchersforpay.size()>0){
			QMessageBox::warning(this,tr("Warning"),tr("Reservations cannot contain any vouchers for payment yet."));
			return false;
		}
	}
	//all clear
	return true;
}

void MCartTab::cartTableToOrder(MOCartOrder&cord)
{
	for(int i=0;i<cartmodel->rowCount();i++){
		QModelIndex idx=cartmodel->index(i,0);
		int tp=cartmodel->data(idx,CART_TYPEROLE).toInt();
		if(tp==CART_TICKET){
			MOCartTicket ct;
			ct.setamount(cartmodel->data(idx).toInt());
			ct.seteventid(cartmodel->data(idx,CART_IDROLE).toInt());
			ct.setpricecategoryid(cartmodel->data(idx,CART_PRICEIDROLE).toInt());
			ct.setorigpricecategoryid(cartmodel->data(idx,CART_ORIGPRICEIDROLE).toInt());
			ct.setcartlineid(i);
			cord.addtickets(ct);
		}else
		if(tp==CART_VOUCHER){
			MOCartVoucher cv;
			cv.setamount(cartmodel->data(idx).toInt());
			cv.setprice(cartmodel->data(idx,CART_PRICEROLE).toInt());
			cv.setvalue(cartmodel->data(idx,CART_VALUEROLE).toInt());
			cv.setcartlineid(i);
			cord.addvouchers(cv);
		}
		//TODO: product/items
	}
}

bool MCartTab::orderExecute(MOCartOrder&cord,MOOrder&ord,bool isreserve,bool issale)
{
	if(isreserve){
		MTCreateReservation co=req->queryCreateReservation(cord);
		if(co.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating reservation: %1").arg(co.errorString()));
			return false;
		}
		ord=co.getorder();
		cord=co.getcart();
	}else{
		QStringList vouchers;
		for(auto v:vouchersforpay)vouchers<<v.first;
		MTCreateOrder co=req->queryCreateOrder(cord,issale,vouchers);
		if(co.hasError()){
			QMessageBox::warning(this,tr("Warning"),tr("Error while creating order: %1").arg(co.errorString()));
			return false;
		}
		ord=co.getorder();
		cord=co.getcart();
		if(vouchers.size()>0)
			displayUsedVouchers(co.getvouchers(),co.getpaidcash());
	}
	return true;
}

void MCartTab::displayUsedVouchers(const QList<MOVoucher>&vouchers,int paidcash)
{
	QDialog d(this);
	d.setWindowTitle(tr("Voucher State"));
	d.setSizeGripEnabled(true);
	QVBoxLayout *vl;
	d.setLayout(vl=new QVBoxLayout);
	QTableView *tv;
	QStandardItemModel *tm;
	vl->addWidget(tv=new QTableView,1);
	tv->setModel(tm=new QStandardItemModel(&d));
	tv->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tm->setHorizontalHeaderLabels(QStringList()<<tr("Voucher")<<tr("Original Value")<<tr("Used")<<tr("Remaining Value"));
	tm->insertRows(vouchers.size(),0);
	for(int i=0;i<vouchers.size();i++){
		tm->setData(tm->index(i,0),vouchers[i].voucherid().data());
		int oprc=0;
		for(auto v:vouchersforpay)if(v.first==vouchers[i].voucherid())oprc=v.second;
		tm->setData(tm->index(i,1),cent2str(oprc));
		tm->setData(tm->index(i,2),cent2str(oprc-vouchers[i].value()));
		tm->setData(tm->index(i,3),cent2str(vouchers[i].value()));
	}
	vl->addWidget(new QLabel(tr("Paid in Cash: %1").arg(cent2str(paidcash))));
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	d.exec();
}

void MCartTab::verifyOrderCustomer(MOCartOrder&cord)
{
	QPalette red=QLabel().palette();red.setColor(QPalette::Window,Qt::red);
	if(cord.customerid() != customer.customerid()){
		customer=MOCustomer();
		cartcustomer->setPalette(red);
		cartcustomer->setToolTip(tr("The customer is not valid, please chose another one."));
	}
	if(deliveryaddrid>=0 && cord.deliveryaddressid() != deliveryaddrid){
		deliveryaddrid=-1;
		deliveryaddr->setPalette(red);
		deliveryaddr->setToolTip(tr("The delivery address is not valid, please chose another one."));
	}
	if(invoiceaddrid>=0 && cord.invoiceaddressid() != invoiceaddrid){
		invoiceaddrid=-1;
		invoiceaddr->setPalette(red);
		invoiceaddr->setToolTip(tr("The invoice address is not valid, please chose another one."));
	}
	if(cartship->currentIndex()>0 && cord.shippingtypeid().isNull()){
		QPalette red2=QComboBox().palette();
		red2.setColor(QPalette::Window,Qt::red);
		red2.setColor(QPalette::Button,Qt::red);
		cartship->setPalette(red2);
		cartship->setToolTip(tr("Shipping Type does not exist or you do not have permission to use it."));
	}
}

void MCartTab::verifyOrderTickets(const QList<MOCartTicket>&ticks)
{
	//reset model
	for(int i=0;i<cartmodel->rowCount();i++){
		if(cartmodel->data(cartmodel->index(i,0),CART_TYPEROLE).toInt() != CART_TICKET)continue;
		for(int j=0;j<cartmodel->columnCount();j++){
			QModelIndex idx=cartmodel->index(i,j);
			cartmodel->setData(idx,QVariant(),Qt::BackgroundRole);
			cartmodel->setData(idx,QVariant(),Qt::ToolTipRole);
		}
		QModelIndex idx=cartmodel->index(i,COL_NOTES);
		cartmodel->setData(idx,cartmodel->data(idx,CART_NOTECOPY));
	}
	//insert
	for(int i=0;i<ticks.size();i++){
		if(ticks[i].status()!=MOCartTicket::Ok){
			//find it
			int j=ticks[i].cartlineid();
			if(j<0 || j>=cartmodel->rowCount())continue;
			const QModelIndex idx0=cartmodel->index(j,COL_AMOUNT);
			if(cartmodel->data(idx0,CART_TYPEROLE).toInt() != CART_TICKET)continue;
			const QModelIndex idx1=cartmodel->index(j,COL_TITLE);
			const auto red=QColor(Qt::red);
			cartmodel->setData(idx1,red,Qt::BackgroundRole);
			const QModelIndex idxn=cartmodel->index(j,COL_NOTES);
			//check state, color it
			QString tt;
			switch(ticks[i].status().value()){
				case MOCartTicket::EventOver:
					tt=tr("The event is already over, please remove this entry.");
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				case MOCartTicket::TooLate:
					tt=tr("You cannot order tickets for this event anymore, ask a more privileged user.");
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				case MOCartTicket::Exhausted:
					cartmodel->setData(idx0,red,Qt::BackgroundRole);
					tt=tr("The event or category is (almost) sold out, there are %1 tickets left. (%2)").arg(ticks[i].maxamount()).arg(ticks[i].statustext());
					cartmodel->setData(idx0,tt,Qt::ToolTipRole);
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				case MOCartTicket::Invalid:
					tt=tr("The event does not exist or there is another serious problem, please remove this entry. (%1)").arg(ticks[i].statustext());
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				default:break;//ignore all others
			}
		}
	}
}

void MCartTab::verifyOrderVouchers(const QList<MOCartVoucher>&voucs)
{
	//reset model
	for(int i=0;i<cartmodel->rowCount();i++){
		if(cartmodel->data(cartmodel->index(i,0),CART_TYPEROLE).toInt() != CART_VOUCHER)continue;
		for(int j=0;j<cartmodel->columnCount();j++){
			QModelIndex idx=cartmodel->index(i,j);
			cartmodel->setData(idx,QVariant(),Qt::BackgroundRole);
			cartmodel->setData(idx,QVariant(),Qt::ToolTipRole);
		}
		QModelIndex idx=cartmodel->index(i,COL_NOTES);
		cartmodel->setData(idx,cartmodel->data(idx,CART_NOTECOPY));
	}
	//insert
	for(int i=0;i<voucs.size();i++){
		if(voucs[i].status()!=MOCartVoucher::Ok){
			//find it
			int j=voucs[i].cartlineid();
			if(j<0 || j>=cartmodel->rowCount())continue;
			QModelIndex idx0=cartmodel->index(j,0);
			if(cartmodel->data(idx0,CART_TYPEROLE).toInt() != CART_VOUCHER)continue;
			QModelIndex idx1=cartmodel->index(j,1);
			cartmodel->setData(idx1,QColor(Qt::red),Qt::BackgroundRole);
			const QModelIndex idxn=cartmodel->index(j,COL_NOTES);
			//check state, color it
			QString tt;
			switch(voucs[i].status().value()){
				case MOCartVoucher::InvalidValue:
					tt=tr("You do not have permission to create vouchers with this value, please remove it.");
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				case MOCartVoucher::InvalidPrice:
					tt=tr("The price tag of this voucher is not valid, please remove and recreate it.");
					cartmodel->setData(idx1,tt,Qt::ToolTipRole);
					cartmodel->setData(idxn,tt);
					break;
				default:break;//ignore all others
			}
		}
	}
}

void MCartTab::verifyOrderItems(const QList<MOCartItem>&)
{
	//TODO: implement items
}

void MCartTab::updatePrice()
{
	int prc=0;
	//get items in table
	for(int i=0;i<cartmodel->rowCount();i++){
		QModelIndex idx=cartmodel->index(i,0);
		int amt=cartmodel->data(idx).toInt();
		int itp=cartmodel->data(idx,CART_PRICEROLE).toInt();
		prc+=amt*itp;
	}
	//get shipping
	prc+=cartship->itemData(cartship->currentIndex(),SHIPPING_PRICEROLE).toInt();
	//display price
	price->setText(cent2str(prc));
	//format coupon
	if(couponinfo.couponid().isNull())
		coupon->setText(tr("(none)","empty coupon code label")+
			QString(" <a href=\"vadd:coupon\">%1</a>").arg(tr("[Add Coupon]","link/label")));
	else
		coupon->setText(QString("%1 (%2)").arg(couponinfo.couponid()).arg(couponinfo.description()));
	//format vouchers
	QString vt;
	if(vouchersforpay.size()>0){
		//check for vouchers in cart
		bool vfound=false;
		for(int r=0;r<cartmodel->rowCount();r++)
			if(cartmodel->data(cartmodel->index(r,0),CART_TYPEROLE).toInt()==CART_VOUCHER){
				vfound=true;
				break;
			}
		if(vfound)
			vt+="<font color=\"red\">"+tr("<b>Warning:</b> Paying for vouchers with older vouchers!")+"</font><br/>";
		//render vouchers for payment
		vt+="<html><table>";
		for(auto vou:vouchersforpay){
			vt+=QString("<tr><td>%1 &nbsp;</td><td> -&nbsp; %2</td><td> &nbsp; <a href=\"vremove:%1\">%3</a></td></tr>")
				.arg(vou.first).arg(cent2str(vou.second)).arg(tr("[remove]","remove voucher link/label"));
			prc-=vou.second;
		}
		vt+="</table><br/>";
	}
	vt+="<a href=\"vadd:voucher\">"+tr("[Add Voucher]","link/label")+"</a>";
	vouchers->setText(vt);
	//format remaining price
	if(prc<0)prc=0;
	priceremain->setText(cent2str(prc));
}

void MCartTab::changeVoucher ( QString url )
{
	qDebug()<<url;
	if(url=="vadd:coupon"){
		//sanity check
		if(!couponinfo.couponid().isNull()){
			QMessageBox::warning(this,tr("Warning"),tr("Only one coupon can be used on an order."));
			return;
		}
		//ask for ID
		const QString cid=QInputDialog::getText(this,tr("Enter Coupon ID"),tr("Please enter the coupon ID:")).trimmed().toUpper();
		if(cid.isEmpty())return;
		//get coupon from server
		MOCoupon cp=req->queryGetCoupon(cid).getcoupon();
		//verify and use
		if(cp.couponid().isNull()){
			QMessageBox::warning(this,tr("Warning"),tr("'%1' is not a valid coupon.").arg(cid));
			return;
		}
		if(!req->checkFlags(cp.flags())){
			QMessageBox::warning(this,tr("Warning"),tr("Sorry, you are not allowed to use this coupon."));
			return;
		}
		couponinfo=cp;
		applyCoupon();
	}else if(url=="vadd:voucher"){
		QMessageBox::warning(this,tr("Warning"),tr("Sorry, not implemented yet. Please wait a few days."));
		return;/*
		//sanity check
		bool priv=req->hasRight(MInterface::PCreateOrder_CanPayVoucherWithVoucher);
		bool vfound=false;
		for(int r=0;r<cartmodel->rowCount();r++)
			if(cartmodel->data(cartmodel->index(r,0),CART_TYPEROLE).toInt()==CART_VOUCHER){
				vfound=true;
				break;
			}
		if(!priv && vfound){
			QMessageBox::warning(this,tr("Warning"),tr("Cannot pay for vouchers with another voucher!"));
			return;
		}
		//get voucher ID
		const QString vid=QInputDialog::getText(this,tr("Enter Voucher ID"),tr("Please enter the voucher ID:")).trimmed().toUpper();
		if(vid.isEmpty())return;
		for(auto v:vouchersforpay)
			if(v.first==vid){
				QMessageBox::warning(this,tr("Warning"),tr("Voucher '%1' is already in list.").arg(vid));
				return;
			}
		//retrieve it and check
		MTGetVoucher vou=req->queryGetVoucher(vid);
		if(vou.getvoucher().isNull() || vou.getvoucher().value().voucherid().isNull()){
			QMessageBox::warning(this,tr("Warning"),tr("The voucher '%1' does not exist.").arg(vid));
			return;
		}
		if(vou.getvoucher().data().value().isNull()||vou.getvoucher().data().value().data()<=0){
			QMessageBox::warning(this,tr("Warning"),tr("The voucher '%1' does not have any value left.").arg(vid));
			return;
		}
		if(!vou.getispaidfor().value()){
			QMessageBox::warning(this,tr("Warning"),tr("The voucher '%1' cannot be used: it has not been paid yet.").arg(vid));
			return;
		}
		//add
		vouchersforpay.append(QPair<QString,int>(vid,vou.getvoucher().data().value()));*/
	}else if(url.startsWith("vremove:")){
		const QString vid=url.mid(8);
		for(int i=0;i<vouchersforpay.size();i++)
			if(vouchersforpay[i].first==vid){
				vouchersforpay.removeAt(i);
				break;
			}
	}
	//TODO: if coupon removal is implemented - change applyCoupon accordingly
	//update the listing
	updatePrice();
}

void MCartTab::applyCoupon()
{
	//TODO:
	// - implement maxtickets (coupon) and maxuse (rule) limits
	// - implement need* limits (rule)
	// -> current implementation bails out if it encounters any of the above

	//sanity check
	if(couponinfo.couponid().isNull())return;
	//TODO: once removal of coupon gets implemented - do a reset instead of quitting

	//check for not implemented limits - TODO: implement instead
	if(!couponinfo.amountuse().isNull() || !couponinfo.ticketsperorder().isNull()){
		coupon->setText(tr("<html><font color=\"red\">Sorry, coupon limits are not implemented yet.</font>"));
		return;
	}
	for(auto rule:couponinfo.rules())
		if(!rule.maxuse().isNull() || !rule.needcategory().isNull() || !rule.needtickets().isNull()){
			coupon->setText(tr("<html><font color=\"red\">Sorry, coupon rule limits are not implemented yet.</font>"));
			return;
		}

	//go through order and reset tickets
	//TODO: count and accumulate
	for(int i=0;i<cartmodel->rowCount();i++){
		QModelIndex idx0=cartmodel->index(i,0);
		if(cartmodel->data(idx0,CART_TYPEROLE).toInt()!=CART_TICKET)continue;
		MOEvent ev=cartmodel->data(idx0,CART_EVENTDATAROLE).value<MOEvent>();
		int pid=cartmodel->data(idx0,CART_ORIGPRICEIDROLE).toInt();
		MOEventPrice ep;
		for(auto prc:ev.price())
			if(prc.pricecategoryid()==pid){
				ep=prc;
				break;
			}
		cartmodel->setData(idx0,pid,CART_PRICEIDROLE);
		cartmodel->setData(idx0,ep.price().data(),CART_PRICEROLE);
		QModelIndex idxp=cartmodel->index(i,COL_PRICE);
		cartmodel->setData(idxp,QString("%1 (%2)").arg(cent2str(ep.price())).arg(ep.pricecategory().value().name().value()));
		QModelIndex idxn=cartmodel->index(i,COL_NOTES);
		cartmodel->setData(idxn,cartmodel->data(idxn,CART_NOTECOPY));
	}

	//go through rules and adjust tickets
	//TODO: work on accumulated data and observe limits
	for(int i=0;i<cartmodel->rowCount();i++){
		//get price id
		QModelIndex idx0=cartmodel->index(i,0);
		if(cartmodel->data(idx0,CART_TYPEROLE).toInt()!=CART_TICKET)continue;
		int pid=cartmodel->data(idx0,CART_ORIGPRICEIDROLE).toInt();
		//do we have a rule?
		int npid=pid;
		for(auto rule:couponinfo.rules()){
			if(pid==rule.fromcategory().data()){
				npid=rule.tocategory();
				break;
			}
		}
		if(pid==npid)continue;
		//corrections
		MOEvent ev=cartmodel->data(idx0,CART_EVENTDATAROLE).value<MOEvent>();
		MOEventPrice ep,epo;
		for(auto prc:ev.price()){
			if(prc.pricecategoryid()==npid)
				ep=prc;
			if(prc.pricecategoryid()==pid)
				epo=prc;
		}
		//ignore if not exist
		if(ep.eventid().isNull() || epo.eventid().isNull())continue;

		//change table
		cartmodel->setData(idx0,npid,CART_PRICEIDROLE);
		cartmodel->setData(idx0,ep.price().data(),CART_PRICEROLE);
		QModelIndex idxp=cartmodel->index(i,COL_PRICE);
		cartmodel->setData(idxp,QString("%1 (%2)").arg(cent2str(ep.price())).arg(ep.pricecategory().value().name().value()));
		QModelIndex idxn=cartmodel->index(i,COL_NOTES);
		cartmodel->setData(idxn,tr("Original price: %1 (%2)").arg(cent2str(epo.price())).arg(epo.pricecategory().value().name().value()));
	}
	carttable->resizeColumnsToContents();
}

/********************************************************************************/

MCartTableDelegate::MCartTableDelegate(QObject*p)
	:QItemDelegate(p)
{
}

QWidget *MCartTableDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &/* option */,
        const QModelIndex & index ) const
{
	//base class
	if(index.column()!=0)return 0;

	QSpinBox *editor = new QSpinBox(parent);
	editor->setRange(1,1000);
	editor->installEventFilter(const_cast<MCartTableDelegate*>(this));
	return editor;
}

void MCartTableDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	//name field
	int c=index.column();
	if(c==0)
		((QSpinBox*)editor)->setValue(index.model()->data(index).toInt());
}

void MCartTableDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
	const QModelIndex &index) const
{
	//name field
	int c=index.column();
	if(c==0){
		int newval=((QSpinBox*)editor)->value();
		model->setData(index,newval);
	}
}
