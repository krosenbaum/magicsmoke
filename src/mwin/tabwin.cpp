//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "customerdlg.h"
#include "mapplication.h"
#include "msinterface.h"
#include "passwdchg.h"
#include "shipping.h"
#include "templatedlg.h"

#include "aclwin.h"
#include "carttab.h"
#include "entrancetab.h"
#include "eventstab.h"
#include "orderstab.h"
#include "overview.h"

#include "centbox.h"
#include "sclock.h"

#include <TimeStamp>

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QFile>
#include <QGroupBox>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStatusBar>
#include <QTabWidget>

MTabWin::MTabWin(QString pk,QWidget*parent)
	:QMainWindow(parent)
{
	profilekey=pk;
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowTitle(QString());
	
	//menu
	QMenuBar*mb=menuBar();
	leftseparator=mb->addAction("|");
	leftseparator->setIcon(QIcon(":/separator.png"));
	leftseparator->setEnabled(false);
	helpseparator=mb->addAction("|");
	helpseparator->setIcon(QIcon(":/separator.png"));
	helpseparator->setEnabled(false);
	mb->addMenu(helpmenu=MApplication::helpMenu());
	
	//tabs
	setCentralWidget(tabw=new QTabWidget);
	connect(tabw,SIGNAL(currentChanged(int)), this,SLOT(tabChanged(int)));
	
	//status bar
	QStatusBar *sb=statusBar();
	sb->setSizeGripEnabled(true);
	const QString sbtxt=req->statusBarText().trimmed();
	if(!sbtxt.isEmpty()){
		QLabel*l=new QLabel(sbtxt);
		l->setFrameStyle(QFrame::Panel|QFrame::Sunken);
		sb->addPermanentWidget(l);
	}
	sb->addPermanentWidget(new MServerClock);
}

MTabWin::~MTabWin()
{
}

void MTabWin::tabChanged(int idx)
{
	if(idx<0)idx=tabw->currentIndex();
	QList<int>tabs=tabmenu.keys();
	for(int i=0;i<tabs.size();i++){
		for(int j=0;j<tabmenu[tabs[i]].size();j++){
			tabmenu[tabs[i]][j]->menuAction()->setVisible(tabs[i]==idx);
		}
	}
}

void MTabWin::addTab(QWidget* tw, QString title, QList<QMenu*> menu)
{
	int idx=tabw->addTab(tw,title);
	tabmenu.insert(idx,menu);
	QMenuBar*mb=menuBar();
	for(int i=0;i<menu.size();i++){
		mb->insertMenu(helpseparator,menu[i]);
	}
}

QMenu* MTabWin::addMenu(QString l)
{
	QMenu*m;
	menuBar()->insertMenu(leftseparator,m=new QMenu(l));
	return m;
}


QWidget* MTabWin::currentTab() const
{
	return tabw->currentWidget();
}

void MTabWin::setCurrentTab(QWidget* w)
{
	tabw->setCurrentWidget(w);
}

void MTabWin::setTabEnabled(QWidget* w, bool e)
{
	tabw->setTabEnabled(tabw->indexOf(w),e);
}

QMenu* MTabWin::helpMenu() const
{
	return helpmenu;
}

void MTabWin::setWindowTitle(const QString& t)
{
	QString nt;
	if(t=="")nt="MagicSmoke: ";
	else nt="MagicSmoke "+t+": ";
	QMainWindow::setWindowTitle(nt+ req->currentUser()+ "@"+ QSettings().value("profiles/"+ profilekey+ "/name").toString());
}

