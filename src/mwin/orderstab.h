//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ORDERSTAB_H
#define MAGICSMOKE_ORDERSTAB_H

#include <QDialog>

#include <MOOrder>
#include <MOOrderInfo>

class QAction;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QStandardItemModel;
class QTabWidget;
class QTableView;

class MSInterface;
class MOOrderInfo;
class MOCustomerInfo;

/**Main Overview Window: order list tab*/
class MOrdersTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the window with QSettings-key for current profile*/
		MOrdersTab(QString);
		
                /**create menu for this tab*/
                QList<QMenu*>menu();
                
	private slots:
		/**get all orders, update list*/
		void updateOrders();
		/**open order detail window*/
		void orderDetails();
		
		/**find an order by ticket*/
		void orderByTicket();
		/**find/select orders by event*/
		void orderByEvent();
		/**find/select orders by customer*/
		void orderByCustomer();
		/**find and display order by order ID*/
		void orderByOrder();
		/**display all orders since a specific date*/
		void orderSinceDate();
		///find and display orders by user who created it
		void orderByUser();
                
                ///generate the report
                void genReport();
		
	signals:
		/**needs to be connected to the event tab*/
		void selectEventIds(QList<int>&);
		
	private:
		/**helper function: enters a single order into the model*/
		void addOrderToModel(const MOOrderInfo&,const QList<MOCustomerInfo>&);
		/**helper function: resets the model to an empty state*/
		void resetModel();
		
		enum OrderTimeStampMode{
			OldestForAll,
			OldestForOpen,
			OldestForEvent,
			OldestForCustomer,
			OldestForSince,
			OldestForUser,
		};
		/**returns timestamp for oldest order; if age is given it returns a timestamp age days in the past, if age is not given it uses the configured time*/
		qint64 oldestOrderStamp(OrderTimeStampMode mode,int age=-1);
		
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*ordertable;
		QStandardItemModel*ordermodel;
		QComboBox*ordermode;
                QList<MOOrderInfo>m_orders;
};

class MOrdersByUserDlg:public QDialog
{
	Q_OBJECT
	public:
		MOrdersByUserDlg(int oldestdefault, QWidget* parent, Qt::WindowFlags f = 0);
		
		bool mySelfOnly()const;
		QString userName()const;
		bool includeAll()const;
		qint64 oldest()const;
		
	private:
		QComboBox*m_uname,*m_incall;
		QCheckBox*m_myself;
		QSpinBox*m_days;
};

class MOrdersReport:public QDialog
{
        Q_OBJECT
        public:
                explicit MOrdersReport(const QList<MOOrderInfo>&orders,QWidget* parent = 0, Qt::WindowFlags f = 0);
                
        private slots:
                void saveAs();
        private:
                QStandardItemModel*sum,*byord,*byday,*audit=nullptr;
                QTableView*stable,*otable,*dtable,*atable=nullptr;
                const QList<MOOrderInfo>orders;
                
                void drawAllOrders();
                void drawDayOrders();
                void drawOrderSums();
                void drawAudit();
};

#endif
