//
// C++ Implementation: event list tab
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2013
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "centbox.h"
#include "eventedit.h"
#include "eventsummary.h"
#include "eventview.h"
#include "main.h"
#include "misc.h"
#include "msinterface.h"
#include "pricecatdlg.h"

#include "eventstab.h"

#include "MTGetAllEvents"
#include "MTGetEvent"
#include "MTCancelEvent"

#include <TimeStamp>

#include <QApplication>
#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QDesktopServices>
#include <QFrame>
#include <QHeaderView>
#include <QInputDialog>
#include <QItemSelectionModel>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QSplitter>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QEventLoop>



MEventsTab::MEventsTab(QString pk)
{
	profilekey=pk;
	showoldevents=0;
	setObjectName("eventstab");
	
	//Event tab
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QSplitter*split;
	vl->addWidget(split=new QSplitter);
	split->addWidget(eventtable=new QTableView);
	eventtable->setModel(eventmodel=new QStandardItemModel(this));
	eventtable->setSelectionMode(QAbstractItemView::SingleSelection);
	eventtable->setSelectionBehavior(QAbstractItemView::SelectRows);
	eventtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	eventtable->verticalHeader()->hide();
	eventtable->setSortingEnabled(true);
	eventmodel->setSortRole(SortRole);
	connect(eventtable->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(updateDetails()));
	split->addWidget(eventdetail=new MEventView);
	connect(eventdetail,SIGNAL(eventOrderTicket(qint64,qint64)),this,SIGNAL(eventOrderTicket(qint64,qint64)));
	connect(eventdetail,SIGNAL(eventOrderTicket()),this,SIGNAL(eventOrderTicket()));

	//fill tables
	if(req->hasRight(req->RGetAllEvents)){
		updateEvents();
	}
}

QList<QMenu*>MEventsTab::menu()
{
	//menu
	QList<QMenu*>ret;
	QMenu*m=new QMenu(tr("&View"));
	ret<<m;
	m->addAction(tr("&Update Event List"),this,SLOT(updateEvents()))
		->setEnabled(req->hasRight(req->RGetAllEvents));
	m->addAction(tr("Event &Summary..."),this,SLOT(eventSummary()))
		->setEnabled(req->hasRight(MInterface::RGetEventSummary));
	m->addSeparator();
	showoldevents=m->addAction(tr("Show &old Events"),this,SLOT(updateEvents()));
	showoldevents->setEnabled(req->hasRight(req->RGetAllEvents));
	showoldevents->setCheckable(true);
	showoldevents->setChecked(QSettings().value("profiles/"+profilekey+"/showOldEvents",false).toBool());

	m=new QMenu(tr("&Edit"));
	ret<<m;
	m->addAction(tr("&Show/Edit details..."),this,SLOT(editEvent()))
		->setEnabled(req->hasRight(req->RGetEvent));
	m->addAction(tr("&New Event..."),this,SLOT(newEvent()))
		->setEnabled(req->hasRight(req->RCreateEvent));
	m->addAction(tr("&Clone Current Event..."),this,SLOT(cloneEvent()))
		->setEnabled(req->hasRight(req->RCreateEvent));
	m->addAction(tr("C&ancel Event..."),this,SLOT(eventCancel()))
		->setEnabled(req->hasRight(req->MInterface::RCancelEvent));
	m->addSeparator();
	m->addAction(tr("&Edit Price Categories..."),this,SLOT(editPriceCat()));
	
	return ret;
}


void MEventsTab::updateEvents()
{
	//get events
	MTGetAllEvents gae=req->queryGetAllEvents();
	if(gae.stage()!=gae.Success){
		qDebug("Error getting all events (%s): %s",gae.errorType().toLatin1().data(),gae.errorString().toLatin1().data());
		return;
	}
	QList<MOEvent>evl=gae.getevents();
	//calculate max age
	TimeStamp now=TimeStamp::now();
	bool showveryold=false;
	bool showold;
	if(showoldevents)showold=showoldevents->isChecked();
	else showold=QSettings().value("profiles/"+profilekey+"/showOldEvents",false).toBool();
	if(showold){
		QSettings set;
		set.beginGroup("profiles/"+profilekey);
		int maxage=set.value("maxeventage", 0).toInt();
		if(maxage==0)showveryold=true;
		now.addDays(-maxage);
	}
	//fill model
	eventdetail->clear();
	eventdetail->clearHistory();
	eventmodel->clear();
	eventmodel->insertColumns(0,6);
	eventmodel->setHorizontalHeaderLabels(QStringList()<<tr("Start Time")<<tr("Title")<<tr("Free")<<tr("Reserved")<<tr("Sold")<<tr("Capacity"));
	for(int i=0,j=0;i<evl.size();i++){
		//filter by time
		TimeStamp stime=TimeStamp(evl[i].start());
		if(!showveryold && stime<now)continue;
		//show
		eventmodel->insertRow(j);
		eventmodel->setData(eventmodel->index(j,0),evl[i].id().value(),IdRole);
		eventmodel->setData(eventmodel->index(j,0),QVariant::fromValue(evl[i]),ObjectRole);
		eventmodel->setData(eventmodel->index(j,0),evl[i].startTimeString());
		eventmodel->setData(eventmodel->index(j,0),evl[i].start().value(),SortRole);
		eventmodel->setData(eventmodel->index(j,1),evl[i].title().value());
		eventmodel->setData(eventmodel->index(j,1),evl[i].title().value(),SortRole);
		eventmodel->setData(eventmodel->index(j,2),evl[i].capacity()-evl[i].amountSold()-evl[i].amountReserved());
		eventmodel->setData(eventmodel->index(j,2),evl[i].capacity()-evl[i].amountSold()-evl[i].amountReserved(),SortRole);
		eventmodel->setData(eventmodel->index(j,3),evl[i].amountReserved().value());
		eventmodel->setData(eventmodel->index(j,3),evl[i].amountReserved().value(),SortRole);
		eventmodel->setData(eventmodel->index(j,4),evl[i].amountSold().value());
		eventmodel->setData(eventmodel->index(j,4),evl[i].amountSold().value(),SortRole);
		eventmodel->setData(eventmodel->index(j,5),evl[i].capacity().value());
		eventmodel->setData(eventmodel->index(j,5),evl[i].capacity().value(),SortRole);
		j++;
	}
	eventtable->resizeColumnsToContents();
	//in case this was called from changing the check state
	if(showoldevents)
		QSettings().setValue("profiles/"+profilekey+"/showOldEvents",showold);
}

MEventsTab::~MEventsTab()
{
}

void MEventsTab::updateDetails()
{
	//clear
	eventdetail->clearEvent();
	//get selection
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	//get event
	MOEvent event=eventmodel->data(idx,ObjectRole).value<MOEvent>();
	if(!event.isValid())return;
	eventdetail->setEvent(event);
}

void MEventsTab::newEvent()
{
	MEventEditor ed(this);
	ed.exec();
	updateEvents();
}

void MEventsTab::cloneEvent()
{
	int id;
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	id=eventmodel->data(idx,IdRole).toInt();
	if(id<0)return;
	MEventEditor ed(this,MEventEditor::OpenMode::Create,id);
	ed.exec();
	updateEvents();
}

void MEventsTab::editEvent()
{
	int id;
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	id=eventmodel->data(idx,IdRole).toInt();
	if(id<0)return;
	MEventEditor ed(this,id);
	ed.exec();
	updateEvents();
}

void MEventsTab::eventSummary()
{
	int id;
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	id=eventmodel->data(idx,IdRole).toInt();
	if(id<0)return;
	MEventSummary ed(this,id);
	ed.exec();
}

void MEventsTab::eventCancel()
{
	int id;
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	id=eventmodel->data(idx,IdRole).toInt();
	if(id<0)return;
	MTGetEvent getev=MTGetEvent::query(id);
	if(!getev.hasError()){
		bool ok;
		MOEvent ev=getev.getevent();
		QString r=QInputDialog::getText(this,tr("Cancel Event"),tr("Please enter a reason to cancel event \"%1\" or abort:").arg(ev.title()),QLineEdit::Normal,"",&ok);
		if(!ok)return;
		MTCancelEvent cev=MTCancelEvent::query(id,r);
		if(!cev.hasError())
			QMessageBox::information(this,tr("Event Cancelled"),tr("The event \"%1\" has been cancelled. Please inform everybody who bought a ticket.").arg(ev.title()));
		else
			QMessageBox::warning(this,tr("Warning"),tr("Unable to cancel event \"%1\": %2.").arg(ev.title()).arg(cev.errorString()));
	}
}

int MEventsTab::currentEventId()const
{
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return -1;
	QModelIndex idx=eventmodel->index(ilst[0].row(),0);
	return eventmodel->data(idx,IdRole).toInt();
}

QString MEventsTab::currentEventTitle()const
{
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return "";
	return eventmodel->data(eventmodel->index(ilst[0].row(),1)).toString();
}

QString MEventsTab::currentEventStart()const
{
	QModelIndexList ilst=eventtable->selectionModel()->selectedIndexes();
	if(ilst.size()<1)return "";
	return eventmodel->data(eventmodel->index(ilst[0].row(),0)).toString();
}

void MEventsTab::editPriceCat()
{
	MPriceCategoryDialog(this,false).exec();
}

void MEventsTab::selectEventId(int& eid, bool& ok) const
{
	QList<int>ids;
	selectEventIds(ids,false);
	ok=ids.size()>=1;
	if(ok)eid=ids[0];
}

void MEventsTab::selectEventIds(QList< int >& eids, bool multi) const
{
	eids.clear();
	QDialog dlg;
	dlg.setWindowTitle(tr("Select Event"));
	QTableView*tv;
	QHBoxLayout*hl;
	QVBoxLayout*vl;
	dlg.setLayout(vl=new QVBoxLayout);
	vl->addWidget(tv=new QTableView,10);
	tv->setModel(eventmodel);
	tv->setEditTriggers(QAbstractItemView::NoEditTriggers);
	tv->setSelectionBehavior(QAbstractItemView::SelectRows);
	tv->setSelectionMode(multi?QAbstractItemView::MultiSelection:QAbstractItemView::SingleSelection);
	tv->resizeColumnsToContents();
	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Select")),0);
	connect(p,SIGNAL(clicked()),&dlg,SLOT(accept()));
	connect(tv,SIGNAL(doubleClicked(const QModelIndex&)),&dlg,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")),0);
	connect(p,SIGNAL(clicked()),&dlg,SLOT(reject()));
	//wait for it
	if(dlg.exec()!=QDialog::Accepted)return;
	//get selection
	QModelIndexList ilst=tv->selectionModel()->selectedIndexes();
	if(ilst.size()<1){
		qDebug("nothing selected");
		return;
	}
	//get events
	for(int i=0;i<ilst.size();i++){
		int eid=eventmodel->data(eventmodel->index(ilst[i].row(),0),IdRole).toInt();
		if(!eids.contains(eid))eids.append(eid);
	}
}
