//
// C++ Interface: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_EVENTSTAB_H
#define MAGICSMOKE_EVENTSTAB_H

#include <QDateTime>
#include <QDialog>
#include <QItemDelegate>
#include <QMainWindow>
#include <QTimer>

class QAction;
class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class QStandardItemModel;
class QTabWidget;
class QTableView;
class MEventView;

class MSInterface;

/**Main Overview Window: Event List Tab*/
class MEventsTab:public QWidget
{
	Q_OBJECT
	public:
		/**construct the tab with QSettings-key for current profile*/
		MEventsTab(QString);
		
		/**create menu for this tab*/
		QList<QMenu*>menu();
		
		/**destruct events list tab*/
		~MEventsTab();
	private slots:
		/**create a new event*/
		void newEvent();
		/**create a new event as copy*/
		void cloneEvent();
		/**edit existing event*/
		void editEvent();
		/**open event summary*/
		void eventSummary();
		/**cancel the event*/
		void eventCancel();
		/**edit price categories*/
		void editPriceCat();
	public slots:
		/**returns the currently selected event (as ID) or -1 if none is selected*/
		int currentEventId()const;
		/**returns the currently selected event start time as string*/
		QString currentEventStart()const;
		/**returns the currently selected event title*/
		QString currentEventTitle()const;
		
		/**update list of events*/
		void updateEvents();
		
		/**update details view when selection changes*/
		void updateDetails();
		
		///shows an event selection dialog and returns the selected event
		/// \param eid the ID of the selected event, if the dialog was normally closed
		/// \param ok true if the dialog was closed normally, false if the user aborted
		void selectEventId(int&eid,bool&ok)const;
		///shows an event selection dialog and returns the selected event
		/// \param eids the list of IDs of the selected events, empty if nothing was selected or the user aborted
		/// \param multi true if multiple IDs can be selected, false if only one can be selec ted
		void selectEventIds(QList<int>&eids,bool multi=true)const;

	signals:
		/**order ticket from event tab*/
		void eventOrderTicket();
		/**order ticket from event tab*/
		void eventOrderTicket(qint64,qint64);
		
	private:
		//the profile associated with this session
		QString profilekey;
		//widgets
		QTableView*eventtable;
		QStandardItemModel*eventmodel;
		MEventView*eventdetail;
		//event list
		QAction*showoldevents;
		//refresh timers
		QTimer rtimer;
		//table role definitions
		enum ExtraTableRoles{
			IdRole=Qt::UserRole,
			SortRole,
			ObjectRole
		};
};

#endif
