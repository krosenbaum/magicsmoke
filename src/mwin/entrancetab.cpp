//
// C++ Implementation: overview
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "main.h"
#include "misc.h"
#include "msinterface.h"
#include "orderwin.h"
#include "barcodeline.h"

#include "entrancetab.h"

#include "MTUseTicket"
#include "MTGetEntranceEvents"
#include "MTGetOrderByBarcode"

#include <QBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTimer>
#include <QFormLayout>
#include <QSpinBox>

MEntranceTab::MEntranceTab(QString pk)
{
	profilekey=pk;
	
	QVBoxLayout*vl;QHBoxLayout*hl;
	//Entrance Control Tab
	setLayout(vl=new QVBoxLayout);
	vl->addWidget(entranceevent=new QComboBox,0);
	entranceevent->setEditable(false);
	entranceevent->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
	entranceevent->setMinimumContentsLength(20);
	vl->addSpacing(30);
	vl->addWidget(new QLabel(tr("Enter or scan Ticket-ID:")),0);
	vl->addWidget(entrancescan=new MBarcodeLine,0);
	connect(entrancescan,SIGNAL(returnPressed()),this,SLOT(entranceValidate()));
	connect(entrancescan,SIGNAL(askForFocus()),this,SLOT(askForFocus()));
	qobject_cast< MBarcodeLine* >(entrancescan)->setScanMode(MBarcodeLine::ScanMode::ForceFocus);
	vl->addWidget(entrancelabel=new QLabel("  "),10);
	entrancelabel->setAutoFillBackground(true);
	QFont fnt=entrancelabel->font();
	fnt.setBold(true);fnt.setPointSize(24);
	entrancelabel->setFont(fnt);
	entrancelabel->setAlignment(Qt::AlignCenter);
	
	vl->addSpacing(10);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(10);
	hl->addWidget(orderbtn=new QPushButton(tr("Open Order")));
	connect(orderbtn,SIGNAL(clicked()),this,SLOT(openOrder()));
	vl->addSpacing(20);
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(1);
	QLabel*lab;
	hl->addWidget(lab=new QLabel(tr("Total:")));
	fnt=lab->font();
	fnt.setPointSizeF(fnt.pointSizeF()*1.5);
	lab->setFont(fnt);
	hl->addWidget(amtotal=new QLabel("0"));
	amtotal->setFont(fnt);
	hl->addSpacing(10);
	hl->addWidget(lab=new QLabel(tr("Used:")));
	lab->setFont(fnt);
	hl->addWidget(amused=new QLabel("0"));
	amused->setFont(fnt);
	hl->addSpacing(10);
	hl->addWidget(lab=new QLabel(tr("Unused:")));
	lab->setFont(fnt);
	hl->addWidget(amopen=new QLabel("0"));
	amopen->setFont(fnt);
// 	hl->addStretch(1);
// 	hl->addWidget(new QLabel(tr("Reserved:")));
// 	hl->addWidget(amreserved=new QLabel("0"));
	
	QTimer*tm=new QTimer(this);
	connect(tm,SIGNAL(timeout()),this,SLOT(resetLabel()));
	tm->setSingleShot(false);
	tm->start(10000);
}

QList<QMenu*> MEntranceTab::menu() const
{
	QList<QMenu*>ret;
	QMenu*m=new QMenu("&Entrance");
	ret<<m;
	m->addAction("&Configure...",this,SLOT(configure()));
	m->addAction("&Recheck events",this,SLOT(initialize()));
	return ret;
}

/**while the object lives the tab handed to it is disabled, it automatically reenables when the disable object dies; it also refocuses the scan line*/
class METUtility
{
	private:
		MEntranceTab*widget;
	public:
		/**disables the widget*/
		METUtility(MEntranceTab*w){widget=w;w->setEnabled(false);}
		/**enables the widget*/
		~METUtility(){
			widget->setEnabled(true);
			widget->entrancelabel->setEnabled(true);
			widget->entrancescan->setText("");
			QTimer::singleShot(1,widget->entrancescan,SLOT(setFocus()));
			QTimer::singleShot(1,widget->entrancelabel,SLOT(update()));
		}
};

void MEntranceTab::askForFocus()
{
	emit askForFocus(this);
}

void MEntranceTab::entranceValidate()
{
	//get event ID
	int ci=entranceevent->currentIndex();
	if(ci<0)return;
	int cev=entranceevent->itemData(ci).toInt();
	//check content
	QString tid=entrancescan->text().trimmed();
	entrancescan->setText("");
	//avoid spurious events
	if(tid=="")return;
	//get serious
	METUtility mdis(this);
	//avoid double scans
	QDateTime now=QDateTime::currentDateTime();
	if(tid==lastbarcode && lastbcscan.addSecs(10)>=now)
		return;
	lastbarcode=tid;
	lastbcscan=now;
	//reset display
	QPalette pal=entrancelabel->palette();
	QPalette::ColorRole rl=QPalette::Window;
	pal.setColor(rl,Qt::lightGray);
	entrancelabel->setPalette(pal);
	entrancelabel->setText(tr("searching...","entrance control"));
	//ask the server
	MTUseTicket ut=MTUseTicket::query(tid,cev);
	MOTicketUse tuse=ut.getticketuse();
	//decide what to do
	switch(tuse.usestatus().value()){
		case MOTicketUse::NotFound:
			entrancelabel->setText(tr("Ticket \"%1\" Not Valid").arg(tid));
			pal.setColor(rl,Qt::red);
			break;
		case MOTicketUse::WrongEvent:
			entrancelabel->setText(tr("Ticket \"%1\" is not for this event.").arg(tid));
			pal.setColor(rl,Qt::red);
			break;
		case MOTicketUse::AlreadyUsed:
			entrancelabel->setText(tr("Ticket \"%1\" has already been used").arg(tid));
			pal.setColor(rl,Qt::magenta);
			break;
		case MOTicketUse::NotUsable:
			entrancelabel->setText(tr("Ticket \"%1\" has not been bought.").arg(tid));
			pal.setColor(rl,Qt::red);
			break;
		case MOTicketUse::Ok:
			entrancelabel->setText(tr("Ticket \"%1\" Ok").arg(tid));
			pal.setColor(rl,Qt::green);
			break;
		case MOTicketUse::Unpaid:
			entrancelabel->setText(tr("Ticket \"%1\" is not paid for!").arg(tid));
			pal.setColor(rl,Qt::red);
			break;
		default:
			entrancelabel->setText(tr("Ticket \"%1\" cannot be accepted, please check the order!").arg(tid));
			pal.setColor(rl,Qt::red);
			break;
	}
	
	entrancelabel->setPalette(pal);
	amtotal->setText(QString::number(tuse.amounttickets()));
	amused->setText(QString::number(tuse.amountused()));
	amopen->setText(QString::number(tuse.amountopen()));
// 	amreserved->setText(QString::number(tuse.amountreserved()));
	if(tuse.usestatus().value()!=MOTicketUse::NotFound)
		orderbtn->setEnabled(req->hasRight(req->RGetOrderByBarcode));
	else
		orderbtn->setEnabled(false);
}

static bool eventIsEarlier(const MOEvent&e1,const MOEvent&e2)
{
	return e1.start()<e2.start();
}

void MEntranceTab::initialize(bool force)
{
	METUtility mdis(this);
	//check whether an update should be forced
	QDateTime now=QDateTime::currentDateTime();
	if(!force)
		if(lasteventupdate.addSecs(3600)>now)return;
	//get currently displayed event
	int ci=entranceevent->currentIndex();
	int cev=-1;
	if(ci>=0)cev=entranceevent->itemData(ci).toInt();
	ci=-1;
	//get event list
	QSettings set;
	set.beginGroup("profiles/"+profilekey+"/entrance");
	MTGetEntranceEvents gee=MTGetEntranceEvents::query(
		set.value("maxstart",24).toInt()*3600,
		set.value("maxend",0).toInt()*3600
	);
	if(gee.hasError())return;
	lasteventupdate=now;
	QList<MOEvent>evlst=gee.getevents();
	qSort(evlst.begin(),evlst.end(),eventIsEarlier);
	//fill combobox from eventmodel
	entranceevent->clear();
	for(int i=0;i<evlst.size();i++){
		QString ev=evlst[i].startTimeString()+" "+evlst[i].title();
		int eid=evlst[i].eventid();
		entranceevent->addItem(ev,eid);
		if(eid==cev)ci=i;
	}
	if(ci>=0)entranceevent->setCurrentIndex(ci);
	else resetAmounts();
	orderbtn->setEnabled(false);
}

void MEntranceTab::resetLabel()
{
	QDateTime now=QDateTime::currentDateTime();
	if(lastbcscan.addSecs(300)<=now){
		//reset timer
		lastbarcode="";
		lastbcscan=now;
		//reset label
		entrancelabel->setText(" ");
		entrancelabel->setPalette(QLabel().palette());
		//reset buttons
		orderbtn->setEnabled(false);
	}
}

void MEntranceTab::openOrder()
{
	if(lastbarcode=="")return;
	MTGetOrderByBarcode obt=MTGetOrderByBarcode::query(lastbarcode);
	if(obt.hasError()){
		QMessageBox::warning(this,tr("Warning"),tr("Error while retrieving order: %1").arg(obt.errorString()));
		return;
	}
	MOrderWindow *ow=new MOrderWindow(this,obt.getorder());
	ow->show();
	//reset, just in case the user is fast
	lastbcscan=lastbcscan.addSecs(-20);
}

void MEntranceTab::resetAmounts()
{
	amtotal->setText("0");
	amused->setText("0");
	amopen->setText("0");
// 	amreserved->setText("0");
}

void MEntranceTab::configure()
{
	QSettings set;
	set.beginGroup("profiles/"+profilekey+"/entrance");
	QDialog d(this);
	d.setWindowTitle(tr("Entrance Configuration"));
	QVBoxLayout*vl;
	d.setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout,1);
	QSpinBox*maxstart,*maxend;
	fl->addRow(tr("Show events that start within hours:"),maxstart=new QSpinBox);
	maxstart->setRange(0,10000);
	maxstart->setValue(set.value("maxstart",24).toInt());
	fl->addRow(tr("Show events a maximum of hours after they end:"),maxend=new QSpinBox);
	maxend->setRange(0,1000);
	maxend->setValue(set.value("maxend",0).toInt());
	fl->addRow(tr("Use Cache:"),new QLabel);
	fl->addRow(tr("Cache update interval:"),new QLabel);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout,0);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("Ok")));
	connect(p,SIGNAL(clicked()),&d,SLOT(accept()));
	hl->addWidget(p=new QPushButton(tr("Cancel")));
	connect(p,SIGNAL(clicked()),&d,SLOT(reject()));
	//show
	if(d.exec()!=QDialog::Accepted)return;
	//save
	set.setValue("maxstart",maxstart->value());
	set.setValue("maxend",maxend->value());
}
