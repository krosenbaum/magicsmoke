<?xml version="1.0" encoding="utf-8"?>
<!-- Basic MagicSmoke WOLF
  - base tables for login etc.
  -
  - (c) Konrad Rosenbaum, 2009-2011
  - this file is protected under the GNU AGPLv3 or at your option any newer
  - see COPYING.AGPL for details
  -->
<Wolf>
	<Class name="Flag">
		<Doc>Transport class for flags and their descriptions. They are actually stored in the config table.</Doc>
		<Abstract lang="php"/>
		<Property name="flag" type="astring"/>
		<Property name="description" type="string"/>
	</Class>
	
	<!-- <Class name="TimeTransition">
		<Doc>Helper class: encapsulates a single transition between UTC-offsets for a timezone. This is based on the Olsen TZ database and used by ServerFormat.</Doc>
		<Property name="ts" type="int64">The Unix timestamp (seconds since Epoch) at which the transition occurs.</Property>
		<Property name="offset" type="int">Offset in seconds from UTC (positive is east).</Property>
		<Property name="abbr" type="astring">The abbreviated name of the timezone during this period</Property>
		<Property name="isdst" type="bool">true if this is a daylight saving time</Property>
	</Class> -->
	
	<Class name="ServerFormat">
		<Doc>Transport class for server formatting settings. This is used by both UIs to format numbers, dates, money, etc. The XML version of it is stored in the template directory, distributed between main and language dependent directory.</Doc>
		<Abstract lang="php"/>
		<Property name="weekdays" type="List:string">The list of week day names, starting with monday</Property>
		<Property name="shortweekdays" type="List:string">The list of abbreviated week day names, starting with monday</Property>
		<Property name="months" type="List:string">The list of month names</Property>
		<Property name="shortmonths" type="List:string">The list of abbreviated month names</Property>
		<Property name="dateformat" type="astring">The default format for dates, see the docu of MLocalFormat (Qt side) and LanguageManager (PHP side) for details.</Property>
		<Property name="timeformat" type="astring">The default format for time.</Property>
		<Property name="datetimeformat" type="astring">The default format for date and time.</Property>
		<Property name="amtext" type="astring">The text for AM in time stamps</Property>
		<Property name="pmtext" type="astring">The text for PM in time stamps</Property>
		<Property name="decimaldot" type="astring">The decimal dot for numbers</Property>
		<Property name="thousandseparator" type="astring">The thousand separator for numbers, if it is a space the Web UI will actually use &amp;nbsp; to prevent breaking the number.</Property>
		<Property name="thousanddigits" type="int">The amount of digits between thousand separators, 0 for no thousand separator</Property>
		<Property name="moneydecimals" type="int">The amount of decimals for money values - this must be the same for all languages on a server, the default is 2</Property>
		<Property name="currencysymbol" type="astring">The currency used</Property>
		<Property name="currencysymbolhtml" type="astring">The currency used in HTML notation</Property>
		<Property name="currencysymbolplain" type="astring">The currency used in ASCII notation</Property>
		<Property name="currencysymbolpos" type="bool">Where the currency symbol is found: false: before the number, true: behind the number</Property>
		<Property name="moneynegative" type="astring">Zero to three characters: for negative money values the first one is placed in front of the money value and the second one behind the money value</Property>
		<Property name="moneypositive" type="astring">Zero to three characters: for positive money values the first one is placed in front of the money value and the second one behind the money value</Property>
		<Enum name="MoneyPos">
		  <Doc>Position of the money sign in relation to number and currency symbol</Doc>
		  <Value name="NoSign" value="0">no sign is shown, regardless of what symbol is set</Value>
		  <Value name="SignBeforeNum" value="1">the sign is shown before the numeric value</Value>
		  <Value name="SignAfterNum" value="2">the sign is shown after the numeric value</Value>
		  <Value name="SignBeforeSym" value="3">the sign is shown before the currency symbol</Value>
		  <Value name="SignAfterSym" value="4">the sign is shown after the currency symbol</Value>
		  <Value name="SignParen" value="5">parentheses are drawn around the numeric value</Value>
		</Enum>
		<Property name="moneypositivepos" type="MoneyPos">position of the positive money sign</Property>
		<Property name="moneynegativepos" type="MoneyPos">position of the negative money sign</Property>
		<Property name="timezone" type="astring">Olsen database time zone name</Property>
		<!-- <Property name="TZtransitions" type="List:TimeTransition">list of all precalculated transitions in this timezone, how far into the future calculations go depends on the server, usually at the moment till 2037</Property> -->
	</Class>
	
	<Class name="KeyValuePair">
		<Abstract lang="qt"/>
		<Property name="key" type="string"/>
		<Property name="value" type="string"/>
		<Property name="isnull" type="bool"/>
	</Class>
</Wolf>
