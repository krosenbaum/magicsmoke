<?xml version="1.0" encoding="utf-8"?>
<!-- Event Definition MagicSmoke WOLF
  - declares everything needed to define events
  -
  - (c) Konrad Rosenbaum, 2009-2011
  - this file is protected under the GNU AGPLv3 or at your option any newer
  - see COPYING.AGPL for details
  -->
<Wolf>
	<Transaction name="GetAllArtists" updating="no">
		<Doc>returns a list of all existing artists</Doc>
		<Call lang="php" method="$this->setartists(WOArtist::fromTableArrayartist(WTartist::selectFromDB('','ORDER BY artistname')));"/>
		<Output>
			<Var name="artists" type="List:Artist"/>
		</Output>
	</Transaction>
	<Transaction name="CreateArtist">
		<Doc>creates a new artist</Doc>
		<Input>
			<Var name="name" type="string"/>
			<Var name="description" type="string"/>
			<Var name="comment" type="string"/>
		</Input>
		<Call lang="php" method="WOArtist::createArtist($this);"/>
		<Output>
			<Var name="artist" type="Artist"/>
		</Output>
	</Transaction>

	<Transaction name="GetAllPriceCategories" updating="no">
		<Doc>returns a list of all existing price categories (not event specific)</Doc>
		<Call lang="php" method="$this->setpricecategories(WOPriceCategory::fromTableArraypricecategory(WTpricecategory::selectFromDB()));"/>
		<Output>
			<Var name="pricecategories" type="List:PriceCategory"/>
		</Output>
	</Transaction>
	
	<Transaction name="CreatePriceCategory">
		<Doc>creates a new price category (generic, not yet event specific)</Doc>
		<Input>
			<Var name="pricecategory" type="PriceCategory">properties as requested by user</Var>
		</Input>
		<Call lang="php" method="WOPriceCategory::createCategory($this);"/>
		<Output>
			<Var name="pricecategory" type="PriceCategory">properties as actually stored</Var>
		</Output>
	</Transaction>
	<Transaction name="ChangePriceCategory">
		<Doc>changes a price category globally</Doc>
		<Input>
			<Var name="pricecategory" type="PriceCategory">properties of the category</Var>
		</Input>
		<Call lang="php" method="WOPriceCategory::changeCategory($this);"/>
		<Output>
			<Var name="pricecategory" type="PriceCategory">properties as actually stored</Var>
		</Output>
	</Transaction>
	
	<Transaction name="GetEvent" updating="no">
		<Doc>returns detailed event data</Doc>
		<Input>
			<Var name="eventid" type="int"/>
		</Input>
		<Call lang="php" method="$this->setevent(WOEvent::fromTableevent(WTevent::getFromDB($this->geteventid())));"/>
		<Output>
			<Var name="event" type="Event"/>
		</Output>
	</Transaction>
	
	<Transaction name="GetAllEvents" updating="no">
		<Doc>returns data about all existing events</Doc>
		<Input/>
		<Call lang="php" method="WOEvent::getAllEvents($this);"/>
		<Output>
			<Var name="events" type="List:Event"/>
		</Output>
	</Transaction>
	
	<Transaction name="GetEventList" updating="no">
		<Doc>returns a list of specific events (eg. used by the order window to request exactly those needed by the tickets it shows)</Doc>
		<Input>
			<Var name="eventids" type="List:int"/>
		</Input>
		<Call lang="php" method="WOEvent::getEventList($this);"/>
		<Output>
			<Var name="events" type="List:Event"/>
		</Output>
	</Transaction>

	<Transaction name="GetEventSaleInfo" updating="no">
		<Doc>returns the information necessary to handle sales with seat plans</Doc>
		<Input>
			<Var name="eventid" type="int"/>
		</Input>
		<Call lang="php" method="$this->setevent(WOEventSaleInfo::fromTableevent(WTevent::getFromDB($this->geteventid())));"/>
		<Output>
			<Var name="event" type="EventSaleInfo"/>
		</Output>
	</Transaction>

	<Transaction name="CreateEvent">
		<Doc>create a new event</Doc>
		<Input>
			<Var name="event" type="Event"/>
		</Input>
		<Call lang="php" method="WOEvent::createEvent($this);"/>
		<Output>
			<Var name="event" type="Event"/>
		</Output>
	</Transaction>
	<Transaction name="ChangeEvent">
		<Doc>change event data (like date/time, artist, room, ...)</Doc>
		<Privilege name="CancelEvent">users with this privilege are allowed to set the iscancelled property and hence cancel or uncancel events</Privilege>
		<Input>
			<Var name="event" type="Event"/>
		</Input>
		<Call lang="php" method="WOEvent::changeEvent($this);"/>
		<Output>
			<Var name="event" type="Event"/>
		</Output>
	</Transaction>
	
	<Transaction name="CancelEvent">
		<Doc>cancel an event</Doc>
		<Input>
			<Var name="eventid" type="int"/>
			<Var name="reason" type="string"/>
		</Input>
		<Call lang="php" method="WOEvent::cancelEvent($this);"/>
		<Output/>
	</Transaction>
	
	<Transaction name="GetAllRooms" updating="no">
		<Doc>returns a list of all existing rooms</Doc>
		<Input/>
		<Call lang="php" method="$this->setrooms(WORoom::fromTableArrayroom(WTroom::selectFromDB('')));"/>
		<Output>
			<Var name="rooms" type="List:Room"/>
		</Output>
	</Transaction>
	
	<Transaction name="CreateRoom">
		<Doc>creates a new room</Doc>
		<Input>
			<Var name="roomid" type="string"/>
			<Var name="capacity" type="int"/>
			<Var name="description" type="string"/>
		</Input>
		<Call lang="php" method="WORoom::createRoom($this);"/>
		<Output>
			<Var name="room" type="Room"/>
		</Output>
	</Transaction>

	<Transaction name="GetAllSeatPlans" updating="no">
		<Doc>Returns a list of all existing seat plans</Doc>
		<Input/>
		<Call lang="php" method="$this->setplans(WOSeatPlanInfo::fromTableArrayseatplan(WTseatplan::selectFromDB('')));"/>
		<Output>
			<Var name="plans" type="List:SeatPlanInfo"/>
		</Output>
	</Transaction>
	<Transaction name="CreateSeatPlan" updating="yes">
		<Input>
			<Var name="plan" type="SeatPlanInfo"/>
		</Input>
		<Call lang="php" method="$spt=WTseatplan::newRow();$this->getplan()->toTableseatplan($spt); $spt->insert();$this->setplan(WOSeatPlanInfo::fromTableseatplan($spt));"/>
		<Output>
			<Var name="plan" type="SeatPlanInfo"/>
		</Output>
	</Transaction>
	<Transaction name="UpdateSeatPlan" updating="yes">
		<Input>
			<Var name="plan" type="SeatPlanInfo"/>
		</Input>
		<Call lang="php" method="$spt=WTseatplan::getFromDB($this->getplan()->getseatplanid());$this->getplan()->toTableseatplan($spt); $spt->update();"/>
		<Output/>
	</Transaction>

	<Transaction name="GetEventSummary" updating="no">
		<Doc>Returns the event plus all orders concerning it</Doc>
		<Input>
			<Var name="eventid" type="int"/>
		</Input>
		<Call lang="php" method="WOEvent::getSummary($this);"/>
		<Output>
			<Var name="event" type="Event"/>
			<Var name="orders" type="List:Order"/>
		</Output>
	</Transaction>
</Wolf>
