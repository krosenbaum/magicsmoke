source(findFile("scripts","common.py"))

def init():
    cleanUpClient()
    startServer()
    initServer()

def cleanup():
    stopServer()

def createProfile():
    #we'll be asked for the language, ack C
    clickButton(waitForObject(":Chose Language.OK_QPushButton",100))
    #yes we want an initial profile
    clickButton(waitForObject(":Initial Profile Warning.OK_QPushButton",100))
    #create a profile
    activateItem(waitForObjectItem(":Magic Smoke Configuration_QMenuBar", "Profile",100))
    activateItem(waitForObjectItem(":Magic Smoke Configuration.Profile_QMenu", "New Profile...",100))
    type(waitForObject(":Please enter a profile name. It must be non-empty and must not be used yet:_QLineEdit",100), "test")
    type(waitForObject(":Please enter a profile name. It must be non-empty and must not be used yet:_QLineEdit",1), "<Return>")
    #enter URL
    type(waitForObject(":https://_QLineEdit",100), "<Ctrl+A>")
    type(waitForObject(":https://_QLineEdit",1), "<Del>")
    type(waitForObject(":https://_QLineEdit",1), "localhost:20989/machine.php")
    #go to authentication, import host key
    clickTab(waitForObject(":Magic Smoke Configuration.qt_tabwidget_tabbar_QTabBar",100), "Authentication")
    activateItem(waitForObjectItem(":Magic Smoke Configuration_QMenuBar", "Profile",100))
    activateItem(waitForObjectItem(":Magic Smoke Configuration.Profile_QMenu", "Import Host Key...",100))
    clickButton(waitForObject(":Warning.Yes_QPushButton",100))
    #we usually start in the application bin directory
    # from here we go to ../tests/testhost.mshk
    clickButton(waitForObject(":Import Key from File.toParentButton_QToolButton",2000))
    waitForObjectItem(":stackedWidget.listView_QListView", "tests",1000)
    clickItem(":stackedWidget.listView_QListView", "tests", 41, 8, 0, Qt.LeftButton)
    doubleClick(waitForObject(":stackedWidget.listView_QListView",1000), 136, 8, 0, Qt.LeftButton)
    waitForObjectItem(":stackedWidget.listView_QListView", "testhost\\.mshk",1000)
    clickItem(":stackedWidget.listView_QListView", "testhost\\.mshk", 103, 14, 0, Qt.LeftButton)
    #enter a default user name too: admin
    mouseClick(waitForObject(":Default Username:_QLineEdit",100), 14, 14, 0, Qt.LeftButton)
    type(waitForObject(":Default Username:_QLineEdit",1), "admin")
    #get the SSL exceptions
    clickTab(waitForObject(":Magic Smoke Configuration.qt_tabwidget_tabbar_QTabBar",100), "SSL Exceptions")
    clickButton(waitForObject(":Probe Server_QPushButton",100))
    clickButton(waitForObject(":SSL Warning.Yes_QPushButton",2000))
    clickButton(waitForObject(":Server Probe.OK_QPushButton",1000))
    #done, close dialog
    activateItem(waitForObjectItem(":Magic Smoke Configuration_QMenuBar", "Profile",100))
    activateItem(waitForObjectItem(":Magic Smoke Configuration.Profile_QMenu", "Close Window",100))


def main():
    startMagicSmoke()
    createProfile()
    sendEvent("QCloseEvent", waitForObject(":Magic Smoke Login_MLogin",1000))
