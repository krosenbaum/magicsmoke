import subprocess

MagicSmokeBasePath=squishinfo.testCase + "/../.."

def startMagicSmoke():
    startApplication("magicsmoke -datadir=$APP/../tests/appdata -confdir=$APP/../tests/appconf")
    
def cleanUpMagicSmoke():
    subprocess.call(MagicSmokeBasePath+"/tests/setup-stop.sh")
    subprocess.call(MagicSmokeBasePath+"/tests/setup-clean.sh")

def cleanUpClient():
    subprocess.call("rm -rf "+MagicSmokeBasePath+"/tests/appdata "+MagicSmokeBasePath+"/tests/appconf",shell=True)

def startServer():
    subprocess.call(MagicSmokeBasePath+"/tests/setup-start.sh")

def initServer():
    subprocess.call(MagicSmokeBasePath+"/tests/setup-create.sh")

def stopServer():
    subprocess.call(MagicSmokeBasePath+"/tests/setup-stop.sh")