#!/bin/bash

cd $(dirname $0)/bin

echo "Copying plugins..."
QPLGDIR=$(qmake -query QT_INSTALL_PLUGINS)
cp -r $QPLGDIR/generic .
cp -r $QPLGDIR/iconengines .
cp -r $QPLGDIR/imageformats .
cp -r $QPLGDIR/platforminputcontexts .
mkdir -p platforms
for fn in qminimal qoffscreen qxcb ; do
	cp $QPLGDIR/platforms/lib$fn.so platforms
done
cp -r $QPLGDIR/printsupport .
cp -r $QPLGDIR/xcb* .

echo "Adjusting RPath of Plugins..."
for i in generic/* iconengines/* imageformats/* platforminputcontexts/* platforms/* printsupport/* xcb*/* ; do
	chrpath -r '$ORIGIN/..' $i
done

echo "Checking linked Libraries..."
QLIBDIR=$(qmake -query QT_INSTALL_LIBS)
export LD_LIBRARY_PATH=$QLIBDIR
QLIBS="$(for i in magicsmoke* $(find . -name '*.so'); do ldd $i ; done | grep $QLIBDIR | cut -d = -f 1 | sort | uniq)"

echo "Copying Libraries..."
for fn in $QLIBS ; do
	echo "  $fn"
	cp $QLIBDIR/$fn .
done

echo Done.
