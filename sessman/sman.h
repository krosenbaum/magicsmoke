//
// C++ Interface: Session Client
//
// Description: Session Client Class - connects to a session manager (or even starts one)
//   and enables exchange of session data.
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SC_SCLI_H
#define MAGICSMOKE_SC_SCLI_H

#include <QObject>
#include <QList>
#include <QPair>
#include <QProcess>

class QLocalServer;
class QLocalSocket;
class QSystemTrayIcon;

class MSessionManager:public QObject
{
	Q_OBJECT
public:
	explicit MSessionManager ( QObject* parent = 0 );
	virtual ~MSessionManager();

	virtual bool isActive()const;
	virtual bool hasSession()const;
	virtual QString sessionId()const;
	virtual QString username()const;
	virtual QString profile()const;
	virtual QString profileName()const;

	int connectionCount()const{return mconnections.count();}
	bool haveSlave()const{return mhaveslave;}

	virtual QList<QPair<QString,QString>> menuItems()const;

	static MSessionManager*instance();
	
	void setReady();

private slots:
	void newConnection();
	void readyRead();
	void socketClosed();
	void socketLost(QObject*);

	void execCmd(const QString&);

	void loginSucceeded();
	void sendNewSessionId();
	void loginLost();
	void processLost(int,QProcess::ExitStatus);

signals:
	void openConfig();
	void setProfile(QString);
	void setUsername(QString);
	void setPassword(QString);
	void startLogin();

private:
	QLocalServer*mserver=nullptr;
	QList<QLocalSocket*>mconnections;
	QString mkey;
	QSystemTrayIcon*micon;
	bool mhaveslave=false;
	bool misready=false;

	void sendMenu(QLocalSocket*);
	void sendProfiles(QLocalSocket*);
	void sendSessionInfo(QLocalSocket*);
	void startBinary(QString);
};


#endif
