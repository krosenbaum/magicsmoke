#Project File for MagicSmoke Session Manager
# (c) Konrad Rosenbaum, 2016

#basics
TEMPLATE = app
VERSION = 
TARGET = magicsmoke-sessman

include(../basics.pri)
include(../sesscli/scrand.pri)
include(../iface/iface.pri)
include(../commonlib/commonlib.pri)

#Localization
TRANSLATIONS = \
	smoke-sm_de.ts \
	smoke-sm_en.ts

#main source files
SOURCES += sman.cpp login.cpp
HEADERS += sman.h login.h
INCLUDEPATH += .
DEPENDPATH += $$INCLUDEPATH

#make sure the correct Qt DLLs are used
QT += network gui widgets
