//
// C++ Implementation: Session Manager
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#include <QAction>
#include <QApplication>
#include <QDebug>
#include <QLocalServer>
#include <QLocalSocket>
#include <QMenu>
#include <QMessageBox>
#include <QProcess>
#include <QSignalMapper>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QSettings>

#include "sman.h"
#include "login.h"
#include "boxwrapper.h"

#include "../sesscli/scrand.h"

#include <msinterface.h>
#include <mapplication.h>
#include <WTransaction>

static QPointer<MLogin> loginwindow;
static QPointer<MSessionManager> sessmanobj;

MSessionManager::MSessionManager ( QObject* parent ) : QObject ( parent )
{
	sessmanobj=this;
	// create socket
	mkey=QString::fromLatin1(MagicSmokeRandom::getRandomBytes(32).toHex());
	if(mkey.size()<64){
		qDebug()<<"Error: unable to get a secure name for my server socket!";
		qApp->exit(1);
		return;
	}
	QLocalServer *serv=new QLocalServer(this);
	serv->setSocketOptions(QLocalServer::UserAccessOption);
	if(!serv->listen("magicsmoke-sms-"+mkey)){
		qDebug()<<"Error: unable to open server socket -"<<serv->errorString();
		serv->deleteLater();
		return;
	}
	mserver=serv;
	connect(mserver,SIGNAL(newConnection()),this,SLOT(newConnection()));
	qDebug()<<"Server socket ready.";

	// check for slave socket
	for(QString arg:qApp->arguments()){
		if(arg.startsWith("-slave:")){
			QString sk=arg.mid(7).trimmed();
			qDebug()<<"Attempting to communicate with slave"<<sk;
			QLocalSocket ls;
			ls.connectToServer("magicsmoke-smr-"+sk);
			if(!ls.waitForConnected(5000)){
				qDebug()<<"Unable to connect to slave.";
				continue;
			}
			ls.write(mkey.toLatin1()+"\n");
			ls.waitForBytesWritten(1000);
			ls.close();
			mhaveslave=true;
		}
	}

	//create systray icon
	micon=new QSystemTrayIcon(QIcon(":/icon.png"),this);
	micon->setToolTip(tr("MagicSmoke Session Manager, waiting for login..."));
	QMenu*m=new QMenu;
	QSignalMapper*sm=new QSignalMapper(m);
	for(auto entry:menuItems()){
		if(!MClientConfig::inMenu(entry.first))continue;
		QAction*a=m->addAction(entry.second);
		sm->setMapping(a,entry.first);
		connect(a,SIGNAL(triggered()),sm,SLOT(map()));
	}
	connect(sm,SIGNAL(mapped(QString)),this,SLOT(execCmd(QString)));
	micon->setContextMenu(m);
	micon->show();
}

MSessionManager::~MSessionManager()
{
	if(mserver){
		mserver->close();
		mserver->deleteLater();
		mserver=nullptr;
	}
	MSInterface*ms=MSInterface::instance();
	if(ms){
		ms->logout();
		delete ms;
	}
	qApp->quit();
}

MSessionManager* MSessionManager::instance()
{
	return sessmanobj;
}

bool MSessionManager::hasSession() const
{
	MSInterface*ms=MSInterface::instance();
	if(ms)
		return !ms->sessionId().isEmpty();
	else
		return false;
}

QString MSessionManager::sessionId() const
{
	MSInterface*ms=MSInterface::instance();
	if(ms)
		return ms->sessionId();
	else
		return QString();
}

QString MSessionManager::username() const
{
	MSInterface*ms=MSInterface::instance();
	if(ms)
		return ms->currentUser();
	else
		return QString();
}

QString MSessionManager::profile() const
{
	MSInterface*ms=MSInterface::instance();
	if(ms)
		return ms->profileId();
	else
		return QString();
}

QString MSessionManager::profileName() const
{
	MSInterface*ms=MSInterface::instance();
	if(ms)
		return ms->profileName();
	else
		return QString();
}

bool MSessionManager::isActive() const
{
	return mserver!=nullptr && mserver->isListening();
}

void MSessionManager::newConnection()
{
	while(mserver->hasPendingConnections()){
		QLocalSocket*s=mserver->nextPendingConnection();
		if(!s)return;
		s->setParent(this);
		connect(s,SIGNAL(readyRead()),this,SLOT(readyRead()));
		connect(s,SIGNAL(disconnected()),this,SLOT(socketClosed()));
		connect(s,SIGNAL(error(QLocalSocket::LocalSocketError)),this,SLOT(socketClosed()));
		connect(s,SIGNAL(destroyed(QObject*)),this,SLOT(socketLost(QObject*)));
		mconnections.append(s);
		if(misready)s->write("ready\n");
	}
}

void MSessionManager::readyRead()
{
	//find socket
	QLocalSocket*s=qobject_cast<QLocalSocket*>(sender());
	if(s==nullptr){
		qDebug()<<"Warning: readyRead called without socket caller.";
		return;
	}
	if(!mconnections.contains(s)){
		qDebug()<<"Warning: readyRead called from unknown socket.";
		return;
	}
	//get command
	while(s->canReadLine()){
		const QString line=QString::fromUtf8(s->readLine().trimmed());
		if(line.isEmpty())continue;
		const int pos=line.indexOf(' ');
		const QString cmd=pos>0?line.left(pos):line;
		const QString par=pos>0?line.mid(pos+1):QString();
		qDebug()<<"Received session client request"<<cmd<<"with param"<<par;
		if(cmd=="clientinit"){
			if(par!="magicsmoke"){
				qDebug()<<"Warning: clientinit from non magicsmoke! Very strange.";
				s->deleteLater();
				return;
			}
			sendSessionInfo(s);
		}else if(cmd=="getmenu")
			sendMenu(s);
		else if(cmd=="getprofiles")
			sendProfiles(s);
		else if(cmd=="exec")
			execCmd(par.trimmed());
		else if(cmd=="setprofile")
			emit setProfile(par.trimmed());
		else if(cmd=="setuser")
			emit setUsername(par.trimmed());
		else if(cmd=="setpasswd")
			emit setPassword(QString::fromUtf8(QByteArray::fromBase64(par.trimmed().toLatin1())));
		else if(cmd=="login")
			emit startLogin();
		else
			qDebug()<<"Warning: unknown client command"<<cmd;
	}
}

void MSessionManager::socketClosed()
{
	socketLost(sender());
}

void MSessionManager::socketLost ( QObject* o)
{
	QLocalSocket*s=qobject_cast<QLocalSocket*>(o);
	if(s==nullptr){
		qDebug()<<"Warning: socketLost called without socket caller.";
		return;
	}
	if(!mconnections.contains(s)){
		qDebug()<<"Warning: socketLost called from unknown socket.";
		return;
	}
	//remove from list
	mconnections.removeAll(s);
	//was this the last?
	if(mconnections.isEmpty()){
		qDebug()<<"This was the last connection. Quitting now.";
		deleteLater();
	}
}

void MSessionManager::sendMenu(QLocalSocket*s)
{
	s->write("newmenu\n");
	//send "menu command entry-text" for each entry
	for(auto entry:menuItems())
		if(MClientConfig::inClientMenu(entry.first))
			s->write(QString("menu %1 %2\n").arg(entry.first).arg(entry.second).toUtf8());
	s->write("endmenu\n");
}

void MSessionManager::sendProfiles(QLocalSocket*s)
{
	s->write("newprofiles\n");
	QSettings set;
	s->write(QString("defaultprofile %1\n").arg(set.value("defaultprofile").toString()).toUtf8());
	set.beginGroup("profiles");
	QStringList prf=set.childGroups();
	for(int i=0;i<prf.size();i++){
		s->write(QString("haveprofile %1 %2\n").arg(prf[i]).arg(set.value(prf[i]+"/name").toString()).toUtf8());
	}
	s->write("endprofiles\n");
}


void MSessionManager::execCmd (const QString &cmd)
{
	qDebug()<<"Received command"<<cmd;
	if(cmd=="config"){
		emit openConfig();
	}else if(cmd=="quit"){
		qDebug()<<"Sending QUIT command to all clients and exiting!";
		for(auto c:mconnections){
			c->write("quit\n");
			c->flush();
		}
		QTimer::singleShot(1000,qApp,SLOT(quit()));
	}else if(cmd.startsWith("@")){
		QString bin=(cmd=="@@main")?QString():"-"+cmd.mid(1);
		startBinary(bin);
	}else
		qDebug()<<"I wish I knew how to execute"<<cmd<<"but it does not seem to exist.";
}

void MSessionManager::startBinary ( QString bin )
{
	if(!hasSession()){
		QMessageBox::warning(nullptr,tr("Warning"),tr("Cannot start a client while not logged in!"));
		return;
	}
	QProcess *p=new QProcess(this);
	p->setProcessChannelMode(QProcess::ForwardedChannels);
	connect(p,SIGNAL(finished(int,QProcess::ExitStatus)), this,SLOT(processLost(int,QProcess::ExitStatus)));
	QString binname=qApp->applicationDirPath()+"/magicsmoke"+bin;
	p->start(binname,QStringList()<<"-sms:"+mkey);
	qDebug()<<"starting MagicSmoke client binary"<<binname;
	if(!p->waitForStarted(5000)){
		qDebug()<<"Error: binary does not start!"<<p->errorString();
		delete p;
		return;
	}
	qDebug()<<"Started"<<binname<<"as PID"<<p->processId();
}

void MSessionManager::processLost ( int exitCode, QProcess::ExitStatus exitStatus)
{
	QProcess *p=qobject_cast<QProcess*>(sender());
	qDebug()<<"Process exited with status"<<exitCode<<"and status" <<(exitStatus==QProcess::NormalExit?"normal exit":"crash!");
	if(p){
		qDebug()<<"   ...process was a"<<p->program();
		qDebug()<<"   ...Last Error reads"<<p->errorString();
		p->deleteLater();
	}
}


void MSessionManager::loginSucceeded()
{
	micon->setToolTip(tr("MagicSmoke Session Manager, logged in as %1 at %2.").arg(username()).arg(profileName()));
	if(mconnections.count()>0)
		sendNewSessionId();
	else
		execCmd(loginwindow->getCurrentClient());
}

void MSessionManager::sendNewSessionId()
{
	qDebug()<<"Sending session data...";
	for(QLocalSocket*s:mconnections)
		sendSessionInfo(s);
}

void MSessionManager::sendSessionInfo ( QLocalSocket* s)
{
	if(sessionId().isEmpty())return;
	s->write(QString("profile "+profile()+"\n").toUtf8());
	s->write(QString("user "+username()+"\n").toUtf8());
	s->write(QString("sid "+sessionId()+"\n").toLatin1());
	s->flush();
}

void MSessionManager::loginLost()
{
	for(QLocalSocket*s:mconnections)
		s->write("closed\n");
}

void MSessionManager::setReady()
{
	if(misready)return;
	misready=true;
	for(QLocalSocket*s:mconnections)
		s->write("ready\n");
}

QList<QPair< QString, QString >> MSessionManager::menuItems() const
{
	static QList<QPair<QString,QString>> items;
	if(items.isEmpty()){
		auto _ = [&](QString c,QString t){items.append(QPair<QString,QString>(c,t));};
		_("config",tr("&Configuration..."));
		_("@@main",tr("Start &Expert Client"));
		_("@wizard",tr("Start &Wizard"));
		_("@statistic",tr("Start &Statistics Client"));
		_("@admin",tr("Start &Admin Client"));
		_("@print",tr("Start &Print@Home Daemon"));
		_("quit",tr("&Quit Session"));
	}
	return items;
}



int main(int ac,char**av)
{
	MApplication app(ac,av);
	WTransaction::setLogPrefix("SM-T");
	app.initialize();
        //init updater
        app.initUpdater();

	MSessionManager *sm=new MSessionManager(&app);
	MLogin lw(sm);
	loginwindow=&lw;
	lw.connect(&lw,SIGNAL(loginSucceeded()),sm,SLOT(loginSucceeded()));
	lw.connect(&lw,SIGNAL(lostSession()),sm,SLOT(loginLost()));
	lw.connect(sm,SIGNAL(openConfig()),&lw,SLOT(configwin()));
	lw.connect(sm,SIGNAL(setProfile(QString)),&lw,SLOT(setProfile(QString)));
	lw.connect(sm,SIGNAL(setUsername(QString)),&lw,SLOT(setUsername(QString)));
	lw.connect(sm,SIGNAL(setPassword(QString)),&lw,SLOT(setPassword(QString)));
	lw.connect(sm,SIGNAL(startLogin()),&lw,SLOT(startLogin()));
	lw.show();
	
	sm->setReady();

	return app.exec();
}
