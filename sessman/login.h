//
// C++ Interface: mainwindow
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_LOGIN_H
#define MAGICSMOKE_LOGIN_H

#include <QDialog>
#include <QMap>
#include <QPair>
#include <QWidget>

class QComboBox;
class QLineEdit;
class QSizeGrip;
class QCheckBox;

class MSessionManager;

/**login and profile configuration window*/
class MLogin:public QWidget
{
	Q_OBJECT
	public:
		explicit MLogin(MSessionManager*);
		
		QString getCurrentClient()const;
	private:
		QString sessionid;
		QLineEdit*username,*password;
		QComboBox*profiles,*clients;
		QSizeGrip*resizer;
		
	protected:
		void resizeEvent(QResizeEvent *);
		
	private slots:
		void initProfiles();
		void loadProfile();
		void initClients(MSessionManager*);

	public slots:
		void configwin();
		void relogin();
		void clientConfig();
		void setProfile(QString);
		void setUsername(QString);
		void setPassword(QString);
		void startLogin();

	signals:
		void loginSucceeded();
		void lostSession();
};

///Configures which client is available for selection.
class MClientConfig:public QDialog
{
	Q_OBJECT
public:
	explicit MClientConfig ( QWidget* parent = 0, Qt::WindowFlags f = 0 );

	static bool selectable(QString client);
	static bool inMenu(QString client);
	static bool inClientMenu(QString client);

	static bool preselectLast();
	static QString preselection();
	static bool selectionChangeable();

private slots:
	void saveData();

private:
	QCheckBox*mallowchange;
	QComboBox*mpresel;
	struct s_checkbox{
		QCheckBox*presel=nullptr,*menu=nullptr,*client=nullptr;
		s_checkbox(){}
		s_checkbox(QCheckBox*m,QCheckBox*p,QCheckBox*c):presel(p),menu(m),client(c){}
		s_checkbox(const s_checkbox&)=default;
		s_checkbox(s_checkbox&&)=default;
		s_checkbox& operator=(const s_checkbox&)=default;
		s_checkbox& operator=(s_checkbox&&)=default;
	};
	QMap<QString,s_checkbox>mclients;
};

#endif
