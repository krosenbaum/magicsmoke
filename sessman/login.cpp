//
// C++ Implementation: mainwindow
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "login.h"
#include "msinterface.h"
#include "configdialog.h"
#include "sman.h"

#include <QApplication>
#include <QByteArray>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSizeGrip>
#include <QProgressDialog>

#include "mapplication.h"

MLogin::MLogin(MSessionManager*sm)
{
	setWindowTitle(tr("Magic Smoke Login"));
	setWindowFlags(windowFlags()|Qt::Window|Qt::Dialog);
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	//create Menu Bar
	QMenuBar*mb;
	vl->setMenuBar(mb=new QMenuBar);
	QMenu*m=mb->addMenu(tr("&File"));
	m->addAction(tr("&Exit"),this,SLOT(close()));
	m=mb->addMenu(tr("&Configure"));
	m->addAction(tr("&Configuration..."),this,SLOT(configwin()));
	m->addAction(tr("Client &Selection..."),this,SLOT(clientConfig()));
	mb->addMenu(MApplication::helpMenu());
	
	//create central widget
	QGridLayout*gl;
	vl->addLayout(gl=new QGridLayout,10);
	QLabel*lab;
	int lctr=0;
	gl->addWidget(lab=new QLabel(tr("Profile:")),lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(profiles=new QComboBox,lctr,1);
	connect(profiles,SIGNAL(currentIndexChanged(int)),this,SLOT(loadProfile()));
	gl->addWidget(lab=new QLabel(tr("Client:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(clients=new QComboBox,lctr,1);
	gl->addWidget(lab=new QLabel(tr("Username:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(username=new QLineEdit,lctr,1);
	gl->addWidget(lab=new QLabel(tr("Password:")),++lctr,0);
	lab->setAlignment(Qt::AlignRight);
	gl->addWidget(password=new QLineEdit,lctr,1);
	password->setEchoMode(QLineEdit::Password);
	connect(password,SIGNAL(returnPressed()),this,SLOT(startLogin()));
	connect(username,SIGNAL(returnPressed()),password,SLOT(setFocus()));
	gl->setRowStretch(++lctr,10);
	QHBoxLayout *hl=new QHBoxLayout;
	gl->addLayout(hl,++lctr,0,1,2);
	QPushButton*p;
	hl->addStretch(10);
	hl->addWidget(p=new QPushButton(tr("Login")),0);
	vl->addLayout(hl=new QHBoxLayout);
	resizer=new QSizeGrip(this);
	connect(p,SIGNAL(clicked()),this,SLOT(startLogin()));
	initProfiles();
	initClients(sm);
	loadProfile();
	
	//react to Escape button
	QAction *act=new QAction(this);
	act->setShortcut(Qt::Key_Escape);
	connect(act,SIGNAL(triggered(bool)),this,SLOT(close()));
	addAction(act);
}

void MLogin::initProfiles()
{
	QSettings set;
	int defpronum=-1;
	const QString defpro=set.value("defaultprofile").toString();
	set.beginGroup("profiles");
	QStringList prf=set.childGroups();
	profiles->clear();
	for(int i=0;i<prf.size();i++){
		if(prf[i]==defpro)defpronum=i;
		profiles->addItem(set.value(prf[i]+"/name").toString(),prf[i]);
	}
	if(defpronum>=0)
		profiles->setCurrentIndex(defpronum);
}

void MLogin::loadProfile()
{
	QString key=profiles->itemData(profiles->currentIndex()).toString();
	QSettings set;
	set.beginGroup("profiles/"+key);
	username->setText(set.value("username").toString());
        username->setPlaceholderText(set.value("usernamehint").toString());
	password->setText("");
        if(username->text().isEmpty())
                username->setFocus();
        else
                password->setFocus();
}

#define LASTCLIENTSETTING "SessionManager/LastClient"
void MLogin::initClients(MSessionManager*sm)
{
	//was sman started by a slave? If so: do not allow selection.
	if(sm->connectionCount()>0 || sm->haveSlave()){
		clients->setEnabled(false);
		return;
	}
	//get last active client or forced preselection
	QString last= MClientConfig::preselectLast()? QSettings().value(LASTCLIENTSETTING).toString().trimmed() : MClientConfig::preselection();
	//create entries
	int pos=-1;
	for(auto entry:sm->menuItems()){
		if(!entry.first.startsWith('@'))continue;
		if(!MClientConfig::selectable(entry.first))continue;
		if(last==entry.first)pos=clients->count();
		clients->addItem(entry.second.replace("&",""),entry.first);
	}
	//set last selected or preselected
	if(pos>=0)
		clients->setCurrentIndex(pos);
	//disable if user is not supposed to change this
	clients->setEnabled(MClientConfig::selectionChangeable());
	//make sure we remember the selection
	connect(clients,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
		this, [this]{QSettings().setValue(LASTCLIENTSETTING,clients->currentData());} );
}

QString MLogin::getCurrentClient() const
{
	return clients->currentData().toString();
}

void MLogin::startLogin()
{
	qDebug()<<"Logging in...";
	//make it impossible for the user to interfere
	setEnabled(false);
        QProgressDialog pd(this);
        pd.setWindowTitle(tr("Login"));
        pd.setLabelText(tr("Logging in..."));
        pd.setCancelButton(nullptr);
        pd.setRange(0,100);
        pd.setValue(10);
        pd.setVisible(true);
	//create request object
	MSInterface *mw=new MSInterface(profiles->itemData(profiles->currentIndex()).toString());
	mw->setAutoLogout(true);//this is the session manager - it handles these things
	//check server version
	if(!mw->checkServer()){
		//no need for messagebox: checkServer displays one if necessary
		mw->deleteLater();
		setEnabled(true);
		return;
	}
	pd.setValue(30);
	//start login request
	if(!mw->login(username->text(),password->text())){
		QMessageBox::warning(this,tr("Warning"),tr("Unable to log in."));
		mw->deleteLater();
		setEnabled(true);
		return;
	}
	pd.setValue(70);
        pd.setLabelText(tr("Getting data..."));
	//initialize
// 	mw->initialize();
        pd.setValue(100);
	//open window
	emit loginSucceeded();
	
	//make sure the application exits (only) after everything is cleaned up
	qApp->setQuitOnLastWindowClosed(false);
	
	hide();

	// make sure relogin works
	connect(mw,SIGNAL(needRelogin()),this,SLOT(relogin()));
}

void MLogin::configwin()
{
	MConfigDialog cd;
	cd.exec();
	initProfiles();
	loadProfile();
}

void MLogin::resizeEvent(QResizeEvent* )
{
	resizer->resize(resizer->sizeHint());
	if (isRightToLeft())
		resizer->move(rect().bottomLeft() -resizer->rect().bottomLeft());
	else
		resizer->move(rect().bottomRight() -resizer->rect().bottomRight());
	resizer->raise();
}

void MLogin::relogin()
{
	MSInterface*ms=MSInterface::instance();
	if(ms==nullptr)return;
	if(ms->relogin())
		emit loginSucceeded();
	else
		emit lostSession();
}

void MLogin::setProfile(QString p)
{
	bool ok;
	int n=p.toInt(&ok);
	if(!ok)return;
	profiles->setCurrentIndex(n);
}

void MLogin::setUsername(QString u)
{
	username->setText(u);
}

void MLogin::setPassword(QString pw)
{
	password->setText(pw);
}

void MLogin::clientConfig()
{
	MClientConfig cc(this);
	if(cc.exec()==QDialog::Accepted)
		QMessageBox::information(this,tr("Restart Needed"),tr("You have to restart MagicSmoke for these settings to take effect."));
}

#define LASTTOKEN "@@last@@"
MClientConfig::MClientConfig ( QWidget* parent, Qt::WindowFlags f ) : QDialog ( parent, f )
{
	setWindowTitle(tr("Client Selection Settings"));
	QVBoxLayout*vl;
	QHBoxLayout*hl;
	setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);

	fl->addRow(tr("Editing:"),mallowchange=new QCheckBox(tr("Allow user to change client selection.")));
	mallowchange->setChecked(selectionChangeable());

	fl->addRow(tr("Preselection:"),mpresel=new QComboBox);
	mpresel->addItem(tr("Use Last Active Client"),LASTTOKEN);

	int pos=-1;
	const QString psel=preselection();
	for(auto entry:MSessionManager::instance()->menuItems()){
		if(!entry.first.startsWith('@'))continue;
		//selection
		if(psel==entry.first)pos=mpresel->count();
		mpresel->addItem(entry.second.remove('&'),entry.first);
		//choices
		fl->addRow(entry.second,hl=new QHBoxLayout);
		QCheckBox*m,*p,*c;
		hl->addWidget(p=new QCheckBox(tr("Show in Preselection")));
		p->setChecked(selectable(entry.first));
		QFrame*f;
		hl->addWidget(f=new QFrame);
		f->setFrameShadow(QFrame::Sunken);
		f->setFrameShape(QFrame::VLine);
		hl->addWidget(m=new QCheckBox(tr("Show in Tray Menu")));
		m->setChecked(inMenu(entry.first));
		hl->addWidget(f=new QFrame);
		f->setFrameShadow(QFrame::Sunken);
		f->setFrameShape(QFrame::VLine);
		hl->addWidget(c=new QCheckBox(tr("Show in Client Menu")));
		c->setChecked(inClientMenu(entry.first));
		mclients.insert(entry.first,s_checkbox(m,p,c));
	}
	if(!preselectLast()&&pos>=0)
		mpresel->setCurrentIndex(pos);

	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&Save")));
	p->setDefault(true);
	connect(p,SIGNAL(clicked(bool)),this,SLOT(accept()));
	connect(p,SIGNAL(clicked(bool)),this,SLOT(saveData()));
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked(bool)),this,SLOT(reject()));
}

#define CCFG_SETTINGS "SessionManager/ClientConfig/"
#define PRESELECTLAST "preselectLast"
#define SELECTABLE "selectable/"
#define INMENU "inmenu/"
#define INCLIENT "inclient/"
#define PRESELECTION "preselection"
#define PRESELECTCHANGE "preselectChange"

void MClientConfig::saveData()
{
	QSettings set;
	set.setValue(CCFG_SETTINGS PRESELECTCHANGE,mallowchange->isChecked());

	QString psel=mpresel->currentData().toString();
	if(psel==LASTTOKEN){
		set.setValue(CCFG_SETTINGS PRESELECTLAST,true);
		set.setValue(CCFG_SETTINGS PRESELECTION,QString());
	}else{
		set.setValue(CCFG_SETTINGS PRESELECTLAST,false);
		set.setValue(CCFG_SETTINGS PRESELECTION,psel);
	}

	for(QString k:mclients.keys()){
		set.setValue(CCFG_SETTINGS INMENU + k,mclients[k].menu->isChecked());
		set.setValue(CCFG_SETTINGS SELECTABLE + k,mclients[k].presel->isChecked());
		set.setValue(CCFG_SETTINGS INCLIENT + k,mclients[k].client->isChecked());
	}
}

bool MClientConfig::preselectLast()
{
	return QSettings().value(CCFG_SETTINGS PRESELECTLAST,true).toBool();
}

bool MClientConfig::selectable ( QString client )
{
	return QSettings().value(CCFG_SETTINGS SELECTABLE + client,true).toBool();
}

bool MClientConfig::inMenu ( QString client )
{
	return QSettings().value(CCFG_SETTINGS INMENU + client ,true).toBool();
}

bool MClientConfig::inClientMenu ( QString client )
{
	return QSettings().value(CCFG_SETTINGS INCLIENT + client ,true).toBool();
}

QString MClientConfig::preselection()
{
	return QSettings().value(CCFG_SETTINGS PRESELECTION,"").toString();
}

bool MClientConfig::selectionChangeable()
{
	return QSettings().value(CCFG_SETTINGS PRESELECTCHANGE,true).toBool();
}
