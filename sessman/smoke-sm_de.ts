<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MClientConfig</name>
    <message>
        <location filename="login.cpp" line="262"/>
        <source>Client Selection Settings</source>
        <translation>Programmauswahlkonfiguration</translation>
    </message>
    <message>
        <location filename="login.cpp" line="269"/>
        <source>Editing:</source>
        <translation>Auswahl ändern:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="269"/>
        <source>Allow user to change client selection.</source>
        <translation>Dem Nutzer erlauben die Auswahl zu ändern.</translation>
    </message>
    <message>
        <location filename="login.cpp" line="272"/>
        <source>Preselection:</source>
        <translation>Vorauswahl:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="273"/>
        <source>Use Last Active Client</source>
        <translation>Zuletzt aktives Programm vorauswählen</translation>
    </message>
    <message>
        <location filename="login.cpp" line="285"/>
        <source>Show in Preselection</source>
        <translation>In Vorauswahl zeigen</translation>
    </message>
    <message>
        <location filename="login.cpp" line="291"/>
        <source>Show in Tray Menu</source>
        <oldsource>Show in Menu</oldsource>
        <translation>In SysTray-Menü zeigen</translation>
    </message>
    <message>
        <location filename="login.cpp" line="296"/>
        <source>Show in Client Menu</source>
        <translation>In Client-Programm-Menü zeigen</translation>
    </message>
    <message>
        <location filename="login.cpp" line="307"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="login.cpp" line="311"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>MLogin</name>
    <message>
        <location filename="login.cpp" line="41"/>
        <source>Magic Smoke Login</source>
        <translation>Magic Smoke Login</translation>
    </message>
    <message>
        <location filename="login.cpp" line="48"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="login.cpp" line="49"/>
        <source>&amp;Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="login.cpp" line="50"/>
        <source>&amp;Configure</source>
        <translation>&amp;Konfigurieren</translation>
    </message>
    <message>
        <location filename="login.cpp" line="51"/>
        <source>&amp;Configuration...</source>
        <translation>&amp;Konfigurieren...</translation>
    </message>
    <message>
        <location filename="login.cpp" line="52"/>
        <source>Client &amp;Selection...</source>
        <translation>&amp;Programmauswahl...</translation>
    </message>
    <message>
        <location filename="login.cpp" line="60"/>
        <source>Profile:</source>
        <translation>Profil:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="64"/>
        <source>Client:</source>
        <translation>Programm:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="67"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="70"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="login.cpp" line="81"/>
        <location filename="login.cpp" line="165"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="login.cpp" line="166"/>
        <source>Logging in...</source>
        <translation>Einloggen...</translation>
    </message>
    <message>
        <location filename="login.cpp" line="184"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="login.cpp" line="184"/>
        <source>Unable to log in.</source>
        <translation>Der Login ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="login.cpp" line="190"/>
        <source>Getting data...</source>
        <translation>Hole Daten...</translation>
    </message>
    <message>
        <location filename="login.cpp" line="256"/>
        <source>Restart Needed</source>
        <translation>Neustart</translation>
    </message>
    <message>
        <location filename="login.cpp" line="256"/>
        <source>You have to restart MagicSmoke for these settings to take effect.</source>
        <translation>Sie müssen MagicSmoke neu starten, damit diese Einstellungen aktiv werden.</translation>
    </message>
</context>
<context>
    <name>MSessionManager</name>
    <message>
        <location filename="sman.cpp" line="79"/>
        <source>MagicSmoke Session Manager, waiting for login...</source>
        <translation>MagicSmoke Session Manager, warte auf Login...</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="295"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="295"/>
        <source>Cannot start a client while not logged in!</source>
        <translation>Programm kann nicht gestartet werden wenn noch kein Login besteht!</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="326"/>
        <source>MagicSmoke Session Manager, logged in as %1 at %2.</source>
        <translation>MagicSmoke Session Manager, eingeloggt als %1 auf Profil %2.</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="368"/>
        <source>&amp;Configuration...</source>
        <translation>&amp;Konfigurieren...</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="369"/>
        <source>Start &amp;Expert Client</source>
        <translation>Starte &amp;Experten-Programm</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="370"/>
        <source>Start &amp;Wizard</source>
        <translation>Starte &amp;Wizard</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="371"/>
        <source>Start &amp;Statistics Client</source>
        <translation>Starte &amp;Statistik-Programm</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="372"/>
        <source>Start &amp;Admin Client</source>
        <translation>Starte &amp;Administrationsprogramm</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="373"/>
        <source>Start &amp;Print@Home Daemon</source>
        <translation>Starte &amp;Print@Home Hintergrundprogramm</translation>
    </message>
    <message>
        <location filename="sman.cpp" line="374"/>
        <source>&amp;Quit Session</source>
        <translation>Session &amp;Beenden</translation>
    </message>
</context>
</TS>
