###########################################################
# global Makefile for MagicSmoke
# (c) Konrad Rosenbaum, 2008-2015
#
# This Makefile is used for creating the complete
# MagicSmoke package - including all sub-packages in
# the correct order.
#
# Adjust the configuration values below to match your
# environment.
###########################################################

######
#global configuration

#change this if the correct Qt is not in your PATH
#QMAKE = qmake
QMAKE = /usr/lib/x86_64-linux-gnu/qt5/bin/qmake
#QMAKE = /usr/local/Qt-5.6.0/bin/qmake

#set this if you want additional arguments to qmake
# normally only a spec is usefule for this, see below
QMAKEFLAGS =
#if you want to use a non-standard spec use something like this:
#QMAKEFLAGS = -spec linux-icc-64

#change this if doxygen uses a different name or is not in your PATH
DOXYGEN = doxygen

#the directory to install MagicSmoke to for preparation of packages
# change this only if you want to install directly without building
# packages first
PREFIX = /usr/local

#this builds the installation package on win32
NSIS = makensis

#set this to pass additional options to nsis
NOPT = 

#Translation tools of Qt
LUPD=lupdate
LREL=lrelease
XPAT=xmlpatterns

#basic commands
COPY=cp -af
COPYREC=cp -raf
MKDIR=mkdir -p

###########################################################
# END OF CONFIGURATION
# No user editable content below this line
###########################################################

QTDIR=$(shell $(QMAKE) -query QT_INSTALL_PREFIX)
QTBIN=$(shell $(QMAKE) -query QT_INSTALL_BINS)
QTTRANS=$(shell $(QMAKE) -query QT_INSTALL_TRANSLATIONS)

QMCFG = DESTDIR=$(CURDIR)/bin CONFIG+=separate_debug_info

none:
	@echo "Usage: $(MAKE) target"
	@echo " "
	@echo "Please chose a target!"
	@echo " "
	@echo "Building:"
	@echo "  client - build the client (in Unix/Windows mode)"
	@echo "  server - build the server (in Unix mode)"
	@echo "  all - build everything (in Unix mode)"
	@echo "  debug - build client in debug mode"
#	@echo "  macosx - build the client and server on MacOS/X"
	@echo "Documentation, Translation:"
	@echo "  sdoc - generate source docu, needs doxygen"
	@echo "  lupdate - update translation source files"
	@echo "  lrelease - create binary translation files"
	@echo "Direct Installation:"
	@echo "  install-client - install the client (Unix/Mac)"
	@echo "  install-server - install the server (Unix/Mac)"
	@echo "  install - install everything (Unix/Mac)"
	@echo "  --> all these install into $(PREFIX) per default, you can"
	@echo "      override by specifying PREFIX=/my/path for make"
	@echo "Installation Packages:"
	@echo "  macosx - create the client as dmg for MacOS/X"
	@echo "  nsis - generate NSIS package (Win32)"
	@echo "  dpkg - generate Debian packages"
	@echo "  tgz - generate generic tar.gz packages (Unix/Linux)"
	@echo "Cleanup:"
	@echo "  clean - clean up intermediate files (Unix/Mac)"
	@echo "  distclean - clean up built files (Unix/Mac)"

all: server client lrelease

wob: woc
	cd wob && $(abspath pack/woc/woc) magicsmoke.wolf

woc:
	cd pack && $(MAKE) QMAKE=$(QMAKE) LRELEASE=$(LREL) woc qtbase

wbase: woc
	mkdir -p bin
	cp -a pack/qtbase/lib* bin
	-cp -a pack/qtbase/*.dll bin
	cp -a pack/qtbase/*.qm bin

server: wob

client: wob taurus tzone wbase
	-$(MKDIR) bin
	cd iface && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd commonlib && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd sesscli && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd src && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd mainapp && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd sessman && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd adminapp && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd printathome && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)
	cd plugins && $(QMAKE) $(QMAKEFLAGS) && $(MAKE)

winclient: COPY=copy /y
winclient: MKDIR=mkdir
winclient: COPYREC=xcopy /q /s /e /h /y
winclient: client

taurus:
	-$(MKDIR) bin
	cd taurus && $(QMAKE) $(QMCFG) $(QMAKEFLAGS) && $(MAKE)
	#cd taurus/aurora && bash ./build-gpg.sh
	$(COPY) taurus/lib/* bin/
	-$(COPY) taurus/aurora/gpg/bin/gpg bin/
	-$(COPY) taurus/aurora/gpg/bin/gpg.exe bin/
	-$(COPY) taurus/aurora/gpg/lib/* bin/

tzone:
	-$(MKDIR) bin
	cd tzone && $(QMAKE) $(QMCFG) $(QMAKEFLAGS) && $(MAKE)
	-$(COPY) tzone/libQtTz* bin/
	-$(COPY) tzone/QtTz*.dll bin/

sdoc:
	$(DOXYGEN) Doxyfile-php
	$(DOXYGEN) Doxyfile-cpp
	$(MAKE) -C pack doc
	cd tzone && $(DOXYGEN) Doxyfile
	cd taurus && ./build-src-doc.sh
	cd twig/doc && touch contents.rst && sphinx-build -C . ../../doc/twig

lrelease:
	cd src && $(LREL) smoke.pro
	cd iface && $(LREL) iface.pro
	cd commonlib && $(LREL) commonlib.pro
	cd sessman && $(LREL) sessman.pro
	cd pack/qtbase && $(LREL) wbase.pro
	for i in `find plugins -name '*.pro'` ; do $(LREL) $$i ; done
	mkdir -p bin && cp src/smoke*.qm iface/smoke*.qm sessman/*.qm commonlib/*.qm pack/qtbase/*.qm bin
	cp `find plugins -name '*.qm'` bin
	-$(MAKE) -C www/translations LREL=$(LREL) XPAT=$(XPAT) lrelease

lupdate:
	cd src && $(LUPD) smoke.pro
	cd iface && $(LUPD) iface.pro
	cd commonlib && $(LUPD) commonlib.pro
	cd sessman && $(LUPD) sessman.pro
	cd pack/qtbase && $(LUPD) wbase.pro
	for i in `find plugins -name '*.pro'` ; do $(LUPD) $$i ; done
	$(MAKE) -C www/translations LUPD=$(LUPD) lupdate

install: install-client install-server install-doc

install-client: client lrelease
	-rm -rf $(PREFIX)
	mkdir -p $(PREFIX)
	install -D bin/msp*.so bin/magicsmoke* bin/lib*.so.? $(PREFIX)
	-install -D bin/gpg $(PREFIX)
	-rm $(PREFIX)/*.debug
	strip $(PREFIX)/*
	cp src/images/icon.png $(PREFIX)/
	cp bin/*.qm $(PREFIX)/
	./linuxdeployqt.sh $(PREFIX)

SPREFIX=$(PREFIX)/www
install-server: server
	mkdir -p $(SPREFIX)
	cp -rLTf www $(SPREFIX)
	-rm -f $(SPREFIX)/config.php
	-rm -rf `find $(SPREFIX) -name .svn`

DPREFIX=$(PREFIX)/doc
install-doc: sdoc
	mkdir -p $(DPREFIX)
	cp -rLTf doc $(DPREFIX)/html

clean:
	-rm -rf src/.ctmp src/Makefile* src/msmoke
	-rm -rf src/wob www/inc/wob doc/wob wob/core*
	-$(MAKE) -C pack distclean
	-$(MAKE) -C taurus distclean
	-$(MAKE) -C mainapp distclean
	-$(MAKE) -C plugins distclean
	-rm -rf tzone/zoneinfo* tzone/libQtTzData* tzone/Makefile* tzone/.ctmp
	-rm -rf doc/source-php doc/source-cpp
	-rm -rf taurus/lib/* taurus/bin/*
	-rm -rf `find . -name '*~'`
	-rm -rf `find . -name '*.bak'`
	-rm -rf `find . -name 'core*'`
	-rm -rf `find . -name '.ctmp'`
	-rm -rf bin

distclean: clean

debug: QMAKEFLAGS+=CONFIG+=debug
debug: client server

##installation packages

#MacOS is currently not supported
#macosx: MAKE=xcodebuild
#macosx: client server lrelease
#	cd src ; cp *.qm msmoke.app/Contents/MacOS
#	cd src ; for i in *.qm ; do cp -f $(QTTRANS)/qt$${i#smoke} msmoke.app/Contents/MacOS || true ; done
#	cd src ; rm -f msmoke.dmg ; macdeployqt msmoke.app -dmg
#	@echo "....."
#	@echo "You find the package in src/msmoke.dmg"

#TODO: fix this for MS 2.x
nsis: script client
	echo !define QTDIR / >qtpath.nsh
	$(QMAKE) -query QT_INSTALL_PREFIX >>qtpath.nsh
	$(NSIS) $(NOPT) smoke.nsi

dpkg:
	fakeroot debian/rules binary

tgz-clean:
	-rm -rf dist-srv dist-cli dist-doc

tgz-server: PREFIX=dist-srv
tgz-server: install-server
	cd dist-srv ; tar c . |gzip > ../magicsmoke2-server.tar.gz

tgz-client: PREFIX=dist-cli
tgz-client: install-client
	cd dist-cli && tar c . |gzip >../magicsmoke2-client.tar.gz

tgz-doc: PREFIX=dist-doc
tgz-doc: install-doc
	cd dist-doc && tar c . |gzip >../magicsmoke2-doc.tar.gz

tgz: tgz-clean tgz-server tgz-client tgz-doc


#tell Make that the rules above are symbolic:
.PHONY: script woc wob server client sdoc taurus wbase clean distclean tzone debug macosx
