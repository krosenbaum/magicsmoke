//
// C++ Implementation: main
//
// Description: Main Program
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//


#include "aclwin.h"

#include <scli.h>
#include <mapplication.h>
#include "msinterface.h"

#include <WTransaction>
#include "barcode-plugin.h"


int main(int argc,char**argv)
{
	//create global app
	MApplication app(argc,argv);
	
	//init
	app.initialize();

	//get session
	MSessionClient sc;
	sc.connect(&sc,SIGNAL(sessionLost()),&app,SLOT(quit()));
	sc.connect(&sc,SIGNAL(managerLost()),&app,SLOT(quit()));
	if(sc.waitForSessionAvailable()){
		WTransaction::setLogPrefix("Admin-T");
		MSInterface*ms=new MSInterface(sc.currentProfileId());
		ms->loginSession(sc.currentUsername(), sc.currentSessionId());
		ms->initialize();
		MAclWindow::showWindow(nullptr);
		QObject::connect(&sc,SIGNAL(sessionIdChanged(QString)),ms,SLOT(setSessionId(QString)));
	}else{
		qDebug()<<"Unable to get session. Giving up.";
		return 1;
	}
	
	return app.exec();
}
