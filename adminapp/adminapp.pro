#Project File for MagicSmoke
# (c) Konrad Rosenbaum, 2016

TEMPLATE = app
TARGET = magicsmoke-admin


#main source files
SOURCES = main.cpp
INCLUDEPATH += . ../src ../src/mwin
LIBS+= -lmagicsmoke

include (../basics.pri)

include (../iface/iface.pri)
include (../commonlib/commonlib.pri)
include (../sesscli/sesscli.pri)

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
