TEMPLATE = app
TARGET = seatplangui

include(../basics.pri)
include(../iface/iface.pri)
include(../commonlib/commonlib.pri)

#sources
SOURCES += spgui.cpp
HEADERS += spgui.h

#pathes
INCLUDEPATH += .
DEPENDPATH += $$INCLUDEPATH

#make sure the correct Qt DLLs are used
QT += gui widgets
