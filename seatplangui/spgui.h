//
// C++ Interface: seatplan viewer exe
//
// Description: simple executable that can view seat plans
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2017-18
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef SEATPLANGUI_H
#define SEATPLANGUI_H

#include <QDialog>

class MSeatPlanView;
class QLineEdit;

///GUI main window
class SPGUI:public QDialog
{
    Q_OBJECT
public:
    SPGUI();
public slots:
    ///open new seat plan file
    void openfile();
    ///reload and redisplay current file
    void reload();
private:
    ///file name of seat plan file
    QLineEdit*fname;
    ///viewer
    MSeatPlanView*view;
};

#endif
