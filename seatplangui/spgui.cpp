//
// C++ Implementation: seatplan viewer exe
//
// Description: simple executable that can view seat plans
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2017-18
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include <QApplication>
#include <QBoxLayout>
#include <QDebug>
#include <QDialog>
#include <QDomDocument>
#include <QFile>
#include <QFileDialog>
#include <QLineEdit>
#include <QMessageBox>
#include <QPointer>
#include <QPushButton>
#include <QTextBrowser>

#include "seatplanview.h"
#include "spgui.h"

#if defined(Q_OS_UNIX) || defined(Q_OS_LINUX) || defined(Q_OS_MACOS)
#define DOPRINTF 1
#include <stdio.h>
#else
#define DOPRINTF 0
#endif


SPGUI::SPGUI()
{
    setWindowTitle("Seat Plan Viewer");
    setSizeGripEnabled(true);
    
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(fname=new QLineEdit,10);
    QPushButton*p;
    hl->addWidget(p=new QPushButton("..."),0);
    connect(p,SIGNAL(clicked()),this,SLOT(openfile()));
    hl->addWidget(p=new QPushButton("Reload"),0);
    connect(p,SIGNAL(clicked()),this,SLOT(reload()));
    vl->addWidget(view=new MSeatPlanView(MOSeatPlan()),10);
    view->setRightIsBlock(true);
}

void SPGUI::openfile()
{
    fname->setText(QFileDialog::getOpenFileName(this,"Open SeatPlan"));
    reload();
}

void SPGUI::reload()
{
    //load file
    QFile fd(fname->text());
    QDomDocument doc;
    if(fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Loading XML...";
        doc.setContent(&fd);
        fd.close();
    }else
        QMessageBox::warning(this,"Warning","Unable to open file.");
    
    //interpret
    view->resetPlan(MOSeatPlan::fromXml(doc.documentElement()));
}

class DGUI:public QDialog
{
    static QPointer<DGUI>instance;
    static void myMessageOutput(QtMsgType, const QMessageLogContext &, const QString &msg)
    {
#if DOPRINTF
        fprintf(stderr,"%s\n",msg.toUtf8().data());
#endif
        if(instance.isNull())return;
        instance->view->append(msg);
        instance->view->moveCursor(QTextCursor::End);
    }
    QTextBrowser *view;
public:
    DGUI(QDialog*parent):QDialog(parent)
    {
        setWindowTitle("Debug Output");
        setSizeGripEnabled(true);
        instance=this;
        view=new QTextBrowser;
        QHBoxLayout*hl;
        QVBoxLayout*vl;
        setLayout(vl=new QVBoxLayout);
        vl->addWidget(view,1);
        vl->addLayout(hl=new QHBoxLayout,0);
        hl->addStretch(1);
        QPushButton*p;
        hl->addWidget(p=new QPushButton("Clear"),0);
        connect(p,&QPushButton::clicked,this,[=](){this->view->clear();});
        setAttribute(Qt::WA_DeleteOnClose);
        qInstallMessageHandler(myMessageOutput);
    }
};
QPointer<DGUI>DGUI::instance;

int main(int ac,char**av)
{
    QApplication app(ac,av);
    SPGUI g;
    g.show();
    DGUI d(&g);
    d.show();
    return app.exec();
}
