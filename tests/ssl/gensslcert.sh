#!/bin/bash

#Call this script to regenerate the SSL key.
#Normally you won't need to do this, unless you feel paranoid about
#anybody disturbing your tests, but then there are bigger holes...

export RANDFILE=/dev/urandom
SER=`date +%Y%m%d%H%M`

openssl req -new -x509 -nodes -out server.crt \
  -keyout server.key -days 4000 -set_serial $SER \
  -subj "/C=OZ/ST=Greenwitchcounty/L=EmeraldCity/O=Localhosters/CN=localhost/emailAddress=root@localhost"

chmod 600 server.key