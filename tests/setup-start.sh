#!/bin/bash
########################################################################
# MagicSmoke Test Setup: Start Script
########################################################################
#
# This script starts the server components for MagicSmoke Tests.
#
########################################################################

cd `dirname $0`

#load defaults
test -f config || {
        echo "No configuration found! Please copy config.tmpl to config and adapt it!" >&2
        exit 1
}
. ./config

####################################
#Postgres

#create Postgres environment
( 
        PG_DD="`pwd`/db"
        cd db/etc
        for i in *.tmpl ; do
                sed "s,@@,$PG_DD," <$i | \
                sed "s,@PGPORT@,$PG_PORT," | \
                sed "s,@HTTPPORT@,$HTTP_PORT," | \
                sed "s,@HTTPSPORT@,$HTTPS_PORT," \
                >${i%.tmpl}
        done
)
test -d db/data || {
        mkdir -p db/data
        chmod 700 db/data
        $PG_BINDIR/initdb -D db/data
        ( cd db/data ; ln -s ../../ssl/server.* . )
}
test -d db/run || mkdir -p db/run

#start Postgres
echo Starting Postgres.
$PG_SERVER -D `pwd`/db/data -c config_file=`pwd`/db/etc/postgresql.conf >>db/run/server.log 2>&1 &

        
####################################
#Apache

#create environment
mkdir -p apache/log
(
        ADIR="`pwd`/apache"
        cd apache/etc
        for i in *.tmpl ; do
                sed "s,@@,$ADIR," <$i | \
                sed "s,@PGPORT@,$PG_PORT," | \
                sed "s,@HTTPPORT@,$HTTP_PORT," | \
                sed "s,@HTTPSPORT@,$HTTPS_PORT," \
                >${i%.tmpl}
        done
        ln -sf ../../ssl/server.* . || true
        echo "$APACHE_MODS" >mods.conf
)

test -d apache/www || (
        mkdir apache/www
        ../www/install.sh apache/www
        ( cd apache/www ; rm -f config.php ; ln -s ../etc/config.php config.php )
)

#start Apache
echo Starting Apache.
$APACHE_CTL -k start -f `pwd`/apache/etc/apache2.conf

exit 0
