LIBS += -lmagicsmoke -L$$PWD/../bin
INCLUDEPATH += $$PWD/../src \
        $$PWD/../src/crypto \
        $$PWD/../src/dialogs \
        $$PWD/../src/iface \
        $$PWD/../src/misc \
        $$PWD/../src/mwin \
        $$PWD/../src/script \
        $$PWD/../src/templates \
        $$PWD/../src/wext \
        $$PWD/../src/widgets \
        $$PWD/../src/wob \
        $$PWD

include($$PWD/../src/libs.pri)
QT += testlib widgets printsupport

gcc { QMAKE_CXXFLAGS += -std=gnu++11 }

linux-* { QMAKE_LFLAGS += -Wl,-rpath,$$PWD/../bin }

#compilation output:
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp
DESTDIR = $$PWD