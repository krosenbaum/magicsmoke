#!/bin/bash
########################################################################
# MagicSmoke Test Setup: Clean-Up Script
########################################################################
#
# This script deletes all remnants of previous setups.
#
########################################################################

cd `dirname $0`

####################################
#Postgres

rm -rf db/data db/run
(
 cd db/etc
 for i in *.tmpl ; do rm -f ${i%.tmpl} ; done
)

####################################
#Apache

rm -rf apache/log apache/www
(
 cd apache/etc
 for i in *.tmpl ; do rm -f ${i%.tmpl} ; done
 rm -f server.* mods.conf
)

####################################
#Client

rm -rf appdata appconf

exit 0
