#!/bin/bash
########################################################################
# MagicSmoke Test Setup: Creation Script
########################################################################
#
# This script (re-)creates the database.
#
########################################################################

cd `dirname $0`

#load defaults
test -f config || {
        echo "No configuration found! Please copy config.tmpl to config and adapt it!" >&2
        exit 1
}
. ./config

####################################
#Postgres

sleep 1 
echo Dropping/Creating Postgres DB and User.
$PG_BINDIR/createuser -h localhost -p $PG_PORT -d -R -S smoke 2>/dev/null || true
$PG_BINDIR/dropdb -h localhost -p $PG_PORT -U smoke -w smoke 2>/dev/null || true
$PG_BINDIR/createdb -h localhost -p $PG_PORT -U smoke -w smoke

        
####################################
#Apache

#base settings
PARAM="--user=Admin --password=Test -O /dev/null"
URL=http://localhost:$HTTP_PORT/admin.php

#create tables
echo Creating DB Structure.
wget $URL?CreateDB=now $PARAM

#add admin user (uname=admin passwd=admin)
echo Creating first MagicSmoke User.
wget $URL --post-data='adminuser=admin&adminpwd1=admin&adminpwd2=admin' $PARAM

#add host key
echo Submitting Test Host Key.

cat >key.web <<EOF
------bb
Content-Disposition: form-data; name="host"; filename="testhost.mshk"
Content-Type: application/octet-stream
EOF
echo >>key.web
cat testhost.mshk >>key.web
cat >>key.web <<EOF
------bb
Content-Disposition: form-data; name="updatehost"

Upload
------bb--
EOF

wget $URL --post-file=key.web --header="Content-Type: multipart/form-data; boundary=----bb" $PARAM
rm -f key.web


##############
#done
exit 0
