#!/bin/bash

#This script runs inside the container to start the Jenkins Slave.

cd /jenkins
rm -f slave.jar
wget $1/jnlpJars/slave.jar
chmod 644 slave.jar

test -d /home/konrad/.config/qtchooser && {
	mkdir -p /jenkins/.config
	ln -sf /home/konrad/.config/qtchooser /jenkins/.config/qtchooser
	echo "Just Linked Qt-Chooser Dir."
} || echo "Qt-Chooser Dir not Found, not Linking it."
su jenkins -c "nice java -jar slave.jar -jnlpUrl $1/computer/$2/slave-agent.jnlp -jnlpCredentials $3"
