#!/bin/bash

#This script runs inside the container to start the Jenkins Slave.

### start Apache
apachectl start

### start MySQL
/etc/init.d/mysql start
#craete smoke user and give it full admin rights
mysql -e "CREATE USER smoke@localhost"
mysql -e 'GRANT ALL PRIVILEGES ON *.* TO smoke'

### start Postgres
# make sure PG allows use without password/auth
cat >/etc/postgresql/*/main/pg_hba.conf <<EOF
local all all trust
host all all 127.0.0.1/32 trust
host all all ::1/128 trust
EOF
#start server
/etc/init.d/postgresql start
#create smoke admin user
createuser -U postgres -dl smoke

### start Jenkins
cd /jenkins
rm -f slave.jar
wget $1/jnlpJars/slave.jar
chmod 644 slave.jar

su jenkins -c "nice java -jar slave.jar -jnlpUrl $1/computer/$2/slave-agent.jnlp -jnlpCredentials $3"
