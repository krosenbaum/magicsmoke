#!/bin/bash


cd "`dirname $0`"
BASE="`pwd`"

echo "Getting base images..."
docker pull debian:jessie
docker pull debian:stretch

for img in qtenv php5 php7 ; do
  echo
  echo "Re-building $img image! This may take a while..."
  cd $BASE/$img
  docker build -t magicsmoke-$img -f ./Dockerfile .
  echo "Done with $img."
done

echo Done.
