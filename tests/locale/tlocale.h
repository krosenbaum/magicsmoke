#include <QObject>
#include "misc.h"

class TLocale:public QObject
{
        Q_OBJECT
        MLocalFormat mf;
        
        void switchToBadFormat();
        void resetFormat();
private slots:
        void initialize();
        void printFormatting();
        void scanData();
        void testMoneyRegexp();
        void weirdPrintFormatting();
        void weirdScanData();
        void weirdMoneyRegexp();
        void resetPrintFormatting();
        void resetScanData();
        void resetMoneyRegexp();
};