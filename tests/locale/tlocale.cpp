//
// C++ Implementation: locale
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mtest.h"
#include "tlocale.h"
#include <TimeStamp>

MSTEST_MAIN(TLocale)



void TLocale::printFormatting()
{
        QCOMPARE(mf.formatNumber(10000),(QString)"10000");
        QCOMPARE(mf.formatNumber(1234.5),(QString)"1234.5000");
        QCOMPARE(mf.formatNumber(-10000),(QString)"-10000");
        QCOMPARE(mf.formatNumber(-1234.5),(QString)"-1234.5000");
        QCOMPARE(mf.formatMoney(12345),(QString)"123.45");
        
        QDateTime tm(QDate(2003,2,1),QTime(16,5,6,7),Qt::UTC);
        QCOMPARE(mf.formatDate(tm.date()),QString("2003-02-01"));
        QCOMPARE(mf.formatTime(tm.time()),QString("16:05"));
        QCOMPARE(mf.formatDateTime(tm),QString("2003-02-01 16:05"));
        QCOMPARE(mf.formatDateTime(tm,"%Y/%y %M/%m/%N/%n %D/%d/%W/%w"), QString("2003/03 02/2/February/Feb 01/1/Saturday/Sat"));
        QCOMPARE(mf.formatDateTime(tm,"%H/%h/%A/%a %I/%i %S/%s %P/%p"), QString("16/16/04/4 05/5 06/6 PM/pm"));
        TimeStamp ts(tm,"Europe/Berlin");
        QCOMPARE(ts.msecs(),(unsigned short)7);
        QCOMPARE(mf.formatDateTime(tm,"%Z/%z"), QString("007/7"));
        QCOMPARE(mf.formatDateTime(TimeStamp(1363031849,"Europe/Berlin"),"%t"), QString("+01:00"));
}

void TLocale::weirdPrintFormatting()
{
        switchToBadFormat();

        QCOMPARE(mf.formatNumber(10000),(QString)"1_00_00");
        QCOMPARE(mf.formatNumber(1234.5),(QString)"12_34;5000");
        QCOMPARE(mf.formatNumber(-10000),(QString)"-1_00_00");
        QCOMPARE(mf.formatNumber(-1234.5),(QString)"-12_34;5000");
        QCOMPARE(mf.formatMoney(12345),(QString)"12;345 Credits");
        
        QDateTime tm(QDate(2003,2,1),QTime(16,5,6,7),Qt::UTC);
        QCOMPARE(mf.formatDate(tm.date()),QString("2003-02-01"));
        QCOMPARE(mf.formatTime(tm.time()),QString("16:05"));
        QCOMPARE(mf.formatDateTime(tm),QString("2003-02-01 16:05"));
        QCOMPARE(mf.formatDateTime(tm,"%Y/%y %M/%m/%N/%n %D/%d/%W/%w"), QString("2003/03 02/2/febber/f2 01/1/satt-dai/satt"));
        QCOMPARE(mf.formatDateTime(tm,"%H/%h/%A/%a %I/%i %S/%s %P/%p"), QString("16/16/04/4 05/5 06/6 PP/pp"));
        TimeStamp ts(tm,"Europe/Berlin");
        QCOMPARE(ts.msecs(),(unsigned short)7);
        QCOMPARE(mf.formatDateTime(tm,"%Z/%z"), QString("007/7"));
        QCOMPARE(mf.formatDateTime(TimeStamp(1363031849,"Europe/Berlin"),"%t"), QString("+01:00"));
}


void TLocale::testMoneyRegexp()
{
        QRegExp r=mf.moneyRegExp(true,true);
        QVERIFY(r.isValid());
        QVERIFY(r.exactMatch("123.45"));
        QVERIFY(r.exactMatch("1,23.45"));
        QVERIFY(r.exactMatch("-1,23.45"));
        QVERIFY(!r.exactMatch("1,23.456"));
        QVERIFY(!r.exactMatch("1_23;456"));
        QVERIFY(!r.exactMatch("123.45x"));
        QVERIFY(!r.exactMatch("123;456Credits"));
        QVERIFY(!r.exactMatch("-123;456Credits"));
        QVERIFY(!r.exactMatch("[123;456]Credits"));
}

void TLocale::weirdMoneyRegexp()
{
        switchToBadFormat();
        QRegExp r=mf.moneyRegExp(true,true);
        QVERIFY(r.isValid());
        QVERIFY(!r.exactMatch("123.45"));
        QVERIFY(!r.exactMatch("1,23.45"));
        QVERIFY(!r.exactMatch("-1,23.45"));
        QVERIFY(!r.exactMatch("1,23.456"));
        QVERIFY(r.exactMatch("1_23;456"));
        QVERIFY(!r.exactMatch("123.45x"));
        QVERIFY(r.exactMatch("123;456Credits"));
        QVERIFY(r.exactMatch("[123;456]Credits"));
        QVERIFY(r.exactMatch("-123;456Credits"));
}


void TLocale::scanData()
{
        QCOMPARE(mf.scanInt(" 1234 "),1234ll);
        QCOMPARE(mf.scanInt(" -1234 "),-1234ll);
        QCOMPARE(mf.scanInt(" 12,34 "),1234ll);
        QCOMPARE(mf.scanInt(" 12_34 "),12ll);
        QCOMPARE(mf.scanInt(" 12/34 "),12ll);
        QCOMPARE(mf.scanInt(" 12.34 "),12ll);

        QCOMPARE(mf.scanFloat(" 1234.56 "),1234.56);
        QCOMPARE(mf.scanFloat(" 1234 "),1234.);
        QCOMPARE(mf.scanFloat(" 12,34.56 "),1234.56);
        QCOMPARE(mf.scanFloat(" 12_34;56 "),12.);
        QCOMPARE(mf.scanFloat(" -12,34.56 "),-1234.56);

        QCOMPARE(mf.scanMoney(" 1234.56 "),123456ll);
        QCOMPARE(mf.scanMoney(" 12,34.56 "),123456ll);
        QCOMPARE(mf.scanMoney(" 12_34;56 "),1200ll);
        QCOMPARE(mf.scanMoney(" 12,34.5 "),123450ll);
        QCOMPARE(mf.scanMoney(" 12,34.567 "),123456ll);
        QCOMPARE(mf.scanMoney(" 12,34.5.6 "),123456ll);
        QCOMPARE(mf.scanMoney(" 12,34.5,6 "),123456ll);
        QCOMPARE(mf.scanMoney(" 12,34.5 6 "),123450ll);
        QCOMPARE(mf.scanMoney(" -12,34.56 "),-123456ll);
        QCOMPARE(mf.scanMoney(" [12,34.56] "),0ll);
        QCOMPARE(mf.scanMoney(" [12_34;56]Cu "),0ll);
        QCOMPARE(mf.scanMoney(" -12_34;56Cu "),-1200ll);
        QCOMPARE(mf.scanMoney(" -[12_34;56]Cu "),0ll);
}

void TLocale::weirdScanData()
{
        switchToBadFormat();
        QCOMPARE(mf.scanInt(" 1234 "),1234ll);
        QCOMPARE(mf.scanInt(" -1234 "),-1234ll);
        QCOMPARE(mf.scanInt(" 12,34 "),12ll);
        QCOMPARE(mf.scanInt(" 12_34 "),1234ll);
        QCOMPARE(mf.scanInt(" 12/34 "),12ll);
        QCOMPARE(mf.scanInt(" 12.34 "),12ll);

        QCOMPARE(mf.scanFloat(" 1234.56 "),1234.);
        QCOMPARE(mf.scanFloat(" 1234 "),1234.);
        QCOMPARE(mf.scanFloat(" 12,34.56 "),12.);
        QCOMPARE(mf.scanFloat(" 12_34;56 "),1234.56);
        QCOMPARE(mf.scanFloat(" -12,34.56 "),-12.);

        QCOMPARE(mf.scanMoney(" 1234.56 "),1234000ll);
        QCOMPARE(mf.scanMoney(" 12,34.56 "),12000ll);
        QCOMPARE(mf.scanMoney(" 12_34;56 "),1234560ll);
        QCOMPARE(mf.scanMoney(" 12,34.5 "),12000ll);
        QCOMPARE(mf.scanMoney(" 12,34.567 "),12000ll);
        QCOMPARE(mf.scanMoney(" 12,34.5.6 "),12000ll);
        QCOMPARE(mf.scanMoney(" 12,34.5,6 "),12000ll);
        QCOMPARE(mf.scanMoney(" 12,34.5 6 "),12000ll);
        QCOMPARE(mf.scanMoney(" -12,34.56 "),-12000ll);
        QCOMPARE(mf.scanMoney(" [12,34.56] "),-12000ll);
        QCOMPARE(mf.scanMoney(" [12_34;56]Cu "),-1234560ll);
        QCOMPARE(mf.scanMoney(" -12_34;56Cu "),-1234560ll);
        QCOMPARE(mf.scanMoney(" -[12_34;56]Cu "),-1234560ll);
}



void TLocale::resetMoneyRegexp()
{
        resetFormat();
        testMoneyRegexp();
}

void TLocale::resetPrintFormatting()
{
        resetFormat();
        printFormatting();
}

void TLocale::resetScanData()
{
        resetFormat();
        scanData();
}




void TLocale::initialize()
{
        mf.setTimeZone("Europe/Berlin");
}

void TLocale::switchToBadFormat()
{
        if(mf.decimalDot()==';')return;
        qDebug()<<"\nchanging settings:";
        qDebug()<<"strange names for days and months";
        mf.setWeekDays(QStringList()<<"sunndai"<<"mundai"<<"toosdai"<<"whensdai"<<"soorsdai"<<"fraidai"<<"satt-dai");
        mf.setShortWeekDays(QStringList()<<"sunn"<<"mun"<<"too"<<"when"<<"soo"<<"frai"<<"satt");
        mf.setMonths(QStringList()<<"janner"<<"febber"<<"marsh"<<"abbrill"<<"mai"<<"shoon"<<"shoolai"<<"ogust"<<"sebtumbr"<<"oggdobr"<<"noffembr"<<"dessembr");
        mf.setShortMonths(QStringList()<<"j1"<<"f2"<<"m3"<<"a4"<<"m5"<<"j6"<<"j7"<<"a8"<<"s9"<<"o10"<<"n11"<<"d12");
        qDebug()<<"decimal dot=; thousand divider=_ 2 digits per block";
        mf.setNumberFormat(';','_',2);
        qDebug()<<"currency=Credits, 3 currency decimals, negative is [123;45]";
        mf.setMoneyFormat("Credits",3,true);
        mf.setMoneySign("[]");
        mf.setAP("AA","PP");
}

void TLocale::resetFormat()
{
        if(mf.decimalDot()=='.')return;
        qDebug()<<"Resetting formatting";
        mf=MLocalFormat();
        mf.setTimeZone("Europe/Berlin");
}
