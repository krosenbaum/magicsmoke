#ifndef MAGICSMOKE_TEST_H
#define MAGICSMOKE_TEST_H

#include <QtTest>
#include "main.h"

#define MSTEST_MAIN(TestObject) \
int main(int argc, char *argv[]) \
{ \
    MApplication app(argc, argv); \
    app.setConfigDir("$APP/appconf");\
    app.setDataDir("$APP/appdata");\
    app.setAttribute(Qt::AA_Use96Dpi, true); \
    QTEST_DISABLE_KEYPAD_NAVIGATION \
    TestObject tc; \
    return QTest::qExec(&tc, argc, argv); \
}

#endif
