#!/bin/bash
###########################################################
# Test Runner Script for MagicSmoke
###########################################################


cd `dirname $0`

TASKS="
test_locale
"

rm -f test_*.xml

for tcase in $TASKS ; do
  ./$tcase -xunitxml >$tcase.xml
done
