#!/bin/bash
########################################################################
# MagicSmoke Test Setup: Stop Script
########################################################################
#
# This script stops the server components for MagicSmoke Tests.
#
########################################################################

cd `dirname $0`

#load defaults
test -f config || {
        echo "No configuration found! Please copy config.tmpl to config and adapt it!" >&2
        exit 1
}
. ./config

####################################
#Postgres

#stop Postgres
test -f db/run/pg_server.pid && {
        echo Shutting down Postgres.
        kill `cat db/run/pg_server.pid`
        rm -f db/run/pg_server.pid
} || {
        echo Postgres does not seem to be running.
        test $(ps x|egrep '[p]ostgres|[p]ostmaster' | wc -l) -gt 0 && {
          echo "However, a Postgres process seems to be running under your UID."
          echo "It may be a left-over and you may want to kill it."
        }
}


####################################
#Apache

#stop Apache
echo Shutting down Apache.
$APACHE_CTL -k stop -f `pwd`/apache/etc/apache2.conf


echo Done.
exit 0
