//
// C++ Implementation: print q home
//
// Description: Main Program
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "mapplication.h"
#include "msinterface.h"
#include "scli.h"
#include "WTransaction"
#include "MTGetOrderList"

#include "pah.h"
#include "clientcfg.h"
#include "servercfg.h"
#include "printrun.h"

#include <QAction>
#include <QDateTime>
#include <QIcon>
#include <QMenu>
#include <QMessageBox>
#include <QSettings>
#include <QTimer>

static PrintIcon*piInstance=nullptr;

#define PRINTACTIVESETTING PRINTATHOME_SETTINGSGROUP "/isActive"

PrintIcon::PrintIcon()
{
	setPrintMode(PrintMode::Blocked);
	
	QMenu*ctx=new QMenu;
	ctx->addAction(tr("Start &Printing now"),this,SIGNAL(startPrinting()));
	QAction *a=ctx->addAction(tr("Printing &Active"));
	a->setCheckable(true);
	a->setChecked(misactive=QSettings().value(PRINTACTIVESETTING,true).toBool());
	connect(a,SIGNAL(toggled(bool)),this,SLOT(activatePrint(bool)));
	ctx->addSeparator();
	ctx->addAction(tr("&Client Configuration..."),this,SLOT(clientConfig()));
	ctx->addAction(tr("&Server Configuration..."),this,SLOT(serverConfig()));
	ctx->addSeparator();
	ctx->addAction(tr("&Quit"),qApp,SLOT(quit()));
	setContextMenu(ctx);
	
	show();

	piInstance=this;
}

PrintIcon* PrintIcon::instance()
{
	return piInstance;
}


void PrintIcon::setPrintMode(PrintMode m)
{
	switch(m){
		case PrintMode::Normal:
			setIcon(QIcon(":/icon-print.png"));
			mmode=tr("Ready");
			break;
		case PrintMode::Blocked:
			setIcon(QIcon(":/icon-block.png"));
			mmode=tr("Blocked");
			break;
		case PrintMode::Printing:
			setIcon(QIcon(":/icon-active.png"));
			mmode=tr("Printing...");
			break;
		case PrintMode::Problem:
			setIcon(QIcon(":/icon-problem.png"));
			mmode=tr("Configuration Problems!");
			break;
	}
	toolTipUpdate();
}

void PrintIcon::toolTipUpdate()
{
	QString user=tr("(not logged in)"),host;
	if(req){
		user=req->currentUser();
		host=req->profileName();
	}
	setToolTip(tr("MagicSmoke Print@Home: %1\nlogged in as %2 at %3\nNext print: %4")
		.arg(mmode)
		.arg(user).arg(host)
		.arg(mnexttime)
	);
}

void PrintIcon::setNextTime(QString s)
{
	mnexttime=s;
	toolTipUpdate();
}

void PrintIcon::activatePrint(bool a)
{
	misactive=a;
	emit printActivated(a);
}

void PrintIcon::clientConfig()
{
	MPClientConfig cc;
	if(cc.exec()==QDialog::Accepted)
		emit settingsChanged();
}

void PrintIcon::serverConfig()
{
	MPServerConfig sc;
	if(sc.exec()==QDialog::Accepted)
		emit settingsChanged();
}


PrintScheduler::PrintScheduler()
{
	//set icon to active mode
	PrintIcon *pi=PrintIcon::instance();
	pi->setPrintMode(pi->printIsActive()?PrintIcon::PrintMode::Normal:PrintIcon::PrintMode::Blocked);

	//create timer
	mtimer=new QTimer;
	mtimer->setSingleShot(false);
	connect(mtimer,SIGNAL(timeout()),this,SLOT(startPrinting()));

	//connect signals
	connect(pi,SIGNAL(printActivate(bool)),this,SLOT(activatePrint(bool)));
	connect(pi,SIGNAL(settingsChanged()),this,SLOT(reinit()));
	connect(pi,SIGNAL(startPrinting()),this,SLOT(startPrinting()));

	//init
	reinit();
}

void PrintScheduler::activatePrint(bool a)
{
	if(a!=mtimer->isActive())
		reinit();
}

void PrintScheduler::reinit()
{
	PrintIcon*pi=PrintIcon::instance();
	//check and change timer interval
	const int interval=MPClientConfig::timerInMinutes()*60000;
	if(interval!=mtimer->interval())
		mtimer->setInterval(interval);

	//validate server config
	if(!MPServerConfig::validateConfig()){
		pi->setNextTime(tr("not running"));
		pi->setPrintMode(PrintIcon::PrintMode::Problem);
		mtimer->stop();
		return;
	}

	//check whether the user blocked it
	if(!PrintIcon::instance()->printIsActive()){
		pi->setNextTime(tr("not running"));
		PrintIcon::instance()->setPrintMode(PrintIcon::PrintMode::Blocked);
		mtimer->stop();
		return;
	}

	//make sure timer is running
	if(!mtimer->isActive())mtimer->start();
	pi->setNextTime(QDateTime::currentDateTime().addMSecs(mtimer->remainingTime()).time().toString());
	PrintIcon::instance()->setPrintMode(PrintIcon::PrintMode::Normal);
}

void PrintScheduler::startPrinting()
{
	//sanity check
	if(!MPServerConfig::validateConfig()){
		QMessageBox::warning(nullptr,tr("Warning"),tr("Cannot execute Print@Home: the server configuration is not consistent!"));
		return;
	}

	//take care of logistics
	static bool isprinting=false;
	if(isprinting){
		qDebug()<<"Printing is already active. Ignoring.";
		return;
	}
	qDebug()<<"Starting to print now...";
	isprinting=true;
	PrintIcon*pi=PrintIcon::instance();
	pi->setPrintMode(PrintIcon::PrintMode::Printing);

	// print
	PrintingRun().exec();

	// done
	pi->setPrintMode(pi->printIsActive()?PrintIcon::PrintMode::Normal:PrintIcon::PrintMode::Blocked);
	isprinting=false;
}


int main(int argc,char**argv)
{
	//create global app
	MApplication app(argc,argv);
	
	//init icon
	PrintIcon pi;
	
	//init
	app.initialize();
	app.setQuitOnLastWindowClosed(false);

	//get session
	MSessionClient sc;
	sc.connect(&sc,SIGNAL(sessionLost()),&app,SLOT(quit()));
	sc.connect(&sc,SIGNAL(managerLost()),&app,SLOT(quit()));
	MPClientConfig::performAutoLogin(sc);
	if(sc.waitForSessionAvailable()){
		WTransaction::setLogPrefix("P@H-T");
		MSInterface*ms=new MSInterface(sc.currentProfileId());
		ms->loginSession(sc.currentUsername(), sc.currentSessionId());
		ms->initialize();
		QObject::connect(&sc,SIGNAL(sessionIdChanged(QString)),ms,SLOT(setSessionId(QString)));
	}else{
		qDebug()<<"Unable to get session. Giving up.";
		return 1;
	}

	//activate scheduler
	PrintScheduler ps;

	//event loop
	return app.exec();
}
