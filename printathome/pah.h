//
// C++ Interface: print @ home
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef PAH_MAIN_H
#define PAH_MAIN_H

#include <QSystemTrayIcon>

#include "MOOrderInfo"
#include "MOCustomerInfo"

#define PRINTATHOME_SETTINGSGROUP "PrintAtHome"

class QTimer;

///This represents the icon in the system tray and the main UI of the Print at Home client
class PrintIcon:public QSystemTrayIcon
{
	Q_OBJECT
public:
	PrintIcon();

	///returns the Icon instance
	static PrintIcon* instance();
	
	enum class PrintMode{
		///Normal operation, waiting for events
		Normal,
		///Actively printing
		Printing,
		///operation blocked, no connection or printing deactivated
		Blocked,
		///operation not possible, problem with settings
		Problem
	};

	///returns true if the "Activate Printing" menu item is ticked
	bool printIsActive(){return misactive;}

public slots:
	///sets the icon variant
	void setPrintMode(PrintMode);
	///tells the tool tio when we will print next time
	void setNextTime(QString s);

private slots:
	///opens the client configuration dialog
	void clientConfig();
	///opens the server configuration dialog
	void serverConfig();

	///callback for the menu
	void activatePrint(bool);
	///updates the tool tip text with current settings
	void toolTipUpdate();

signals:
	///emitted when the "Activate Printing" menu item is changed
	void printActivated(bool);
	///emitted whenever settings have changed
	void settingsChanged();
	///emitted when the user wants to start printing immediately
	void startPrinting();

private:
	bool misactive=false;
	QString mmode,mnexttime;
};

///Central scheduler class for the Print at Home client.
class PrintScheduler:public QObject
{
	Q_OBJECT
public:
	PrintScheduler();
public slots:
	///initiates and controls the print job.
	void startPrinting();
	///call back for the menu item - affects the timer
	void activatePrint(bool);
	///re-initializes the scheduler with current settings
	void reinit();
private:
	QTimer*mtimer;
};

#endif
