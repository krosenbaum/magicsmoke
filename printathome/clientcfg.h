//
// C++ Interface: print @ home, client config
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MSMOKE_PAH_CLIENT_H
#define MSMOKE_PAH_CLIENT_H

#include <QDialog>

class QCheckBox;
class QLineEdit;
class QComboBox;
class QSpinBox;
class MSessionClient;

class MPClientConfig:public QDialog
{
	Q_OBJECT
public:
	MPClientConfig(QWidget*parent=nullptr);
	
	static int timerInMinutes();
	static int maximumAgeOfOrders();
	static bool preselectProfile();
	static QString preselectedProfileId();
	static bool preselectUser();
	static QString preselectedUserName();
	static bool autoLogin();
	static QString password();
	
	static void performAutoLogin(MSessionClient&);
	
public slots:
	void save();
	
private:
	QSpinBox*mtimer,*morderage;
	QCheckBox*msetprofile,*msetuser,*mautologin;
	QLineEdit*musername,*mpassword;
	QComboBox*mprofile;
	
};

#endif
