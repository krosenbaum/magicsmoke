//
// C++ Interface: print @ home run
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef PAH_RUN_H
#define PAH_RUN_H

#include <QObject>
#include <QMap>

#include "MOOrderInfo"
#include "MOCustomerInfo"
#include "MOCustomer"
#include "MOEvent"

#include "templates/templates.h"

class MOOrderDocument;
class MOOrder;

///Represents a single printing run with multiple orders to be printed.
class PrintingRun:public QObject
{
	Q_OBJECT
public:
	///retrieves all basic information for open orders
	PrintingRun();

public slots:
	///executes the actual print run
	void exec();
private:
	QList<MOOrderInfo>morders;
	QList<MOCustomerInfo>mcustomers;
	QMap<int,MOCustomer>mcustomerdetails;
	MTemplate ticket,voucher,invoice,mail;
	QString mticketname,mvouchername,minvoicename;
	QMap<int,MOEvent>mevents;

	void printOneOrder(const MOOrderInfo&);
	void uploadFile(int orderid,QString localfile,QString remotefile,QList<MOOrderDocument>&doclist);
	void sendMail(const MOOrder&,const QList<MOOrderDocument>&);
};

#endif
