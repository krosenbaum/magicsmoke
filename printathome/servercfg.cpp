//
// C++ Implementation: print @ home
//
// Description: Client Settings
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "servercfg.h"

#include "msinterface.h"
#include "templates.h"

#include "MTGetAllContactTypes"
#include "MTGetAllShipping"
#include "MTGetPrintAtHomeSettings"
#include "MTSetPrintAtHomeSettings"

#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QCoreApplication>
#include <QDebug>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>

#define TICKETCS "TicketChecksum"
#define VOUCHERCS "VoucherChecksum"
#define INVOICECS "InvoiceChecksum"
#define MAILCS "MailChecksum"
#define MAILCONTACTS "MailContactTypes"
#define SHIPPING "ShippingTypes"
#define TICKETNM "TicketName"
#define VOUCHERNM "VoucherName"
#define INVOICENM "InvoiceName"

//seconds between forced re-inits
#define INIT_TIMER 5*60

QList<MOContactType> MPServerConfig::mcontacttypes;
QDateTime MPServerConfig::mlastinit;
QMap<QString,QString> MPServerConfig::msettings;
QList<MOShipping> MPServerConfig::mshiptypes;

//helpers to store sets of checksum:name pairs
static inline QString strpair2str(const QPair<QString,QString>&pair)
{
	QByteArray a;
	QDataStream stream(&a,QIODevice::WriteOnly);
	stream<<pair;
	return QString::fromLatin1(a.toBase64());
}

static inline QString strpair2str(const QString&checksum,const QString&filename)
{
	return strpair2str(QPair<QString,QString>(checksum,filename));
}

static inline QPair<QString,QString> str2strpair(const QString& str)
{
	QByteArray a(QByteArray::fromBase64(str.toLatin1()));
	QDataStream stream(a);
	QPair<QString,QString>pair;
	stream>>pair;
	return pair;
}



MPServerConfig::MPServerConfig(QWidget* parent)
:QDialog(parent)
{
	setWindowTitle(tr("Print@Home Server Configuration"));
	QVBoxLayout*vl,*vl2,*vl3;
	QHBoxLayout*hl;
	setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);
	//template for tickets
	fl->addRow(tr("Ticket Template:"),mticket=new QComboBox);
	fl->addRow(tr("Ticket File Name:"),hl=new QHBoxLayout);
	hl->addWidget(mticketn=new QLineEdit(ticketFileName()),1);
	hl->addWidget(new QLabel(".pdf"),0);
	//template for vouchers
	fl->addRow(tr("Voucher Template:"),mvoucher=new QComboBox);
	fl->addRow(tr("Voucher File Name:"),hl=new QHBoxLayout);
	hl->addWidget(mvouchern=new QLineEdit(voucherFileName()),1);
	hl->addWidget(new QLabel(".pdf"),0);
	//template for invoice
	fl->addRow(tr("Invoice Template:"),minvoice=new QComboBox);
	fl->addRow(tr("Invoice File Name:"),hl=new QHBoxLayout);
	hl->addWidget(minvoicen=new QLineEdit(billFileName()),1);
	hl->addWidget(new QLabel(".pdf"),0);
	//template for mail
	fl->addRow(tr("Mail Template:"),mmail=new QComboBox);
	//contacts
	QFrame*frm;
	fl->addRow(tr("eMail Contacts:"),frm=new QFrame);
	frm->setFrameStyle(QFrame::StyledPanel|QFrame::Plain);
	frm->setLayout(vl2=new QVBoxLayout);
	//shipping types
	fl->addRow(tr("Shipping Types:"),frm=new QFrame);
	frm->setFrameStyle(QFrame::StyledPanel|QFrame::Plain);
	frm->setLayout(vl3=new QVBoxLayout);
	//get settings
	init();
	//update templates
	QList<MTemplate>tpl=req->templateStore()->allTemplates();
	int tp=-1,vp=-1,ip=-1,mp=-1;
	const QPair<QString,QString> tc=ticketTemplateCS(), vc=voucherTemplateCS(), ic=invoiceTemplateCS(), mc=mailTemplateCS();
	qDebug()<<"DEFAULTS tick"<<tc<<", vouch"<<vc<<", inv"<<ic;
	for(MTemplate tmp:tpl){
		qDebug()<<"JFF"<<tmp.baseName()<<tmp.description()<<tmp.checksum()<<tmp.completeFileName();
		if(tmp.baseName()=="ticket"){
			mticket->addItem(tmp.description(), strpair2str(tmp.checksum(),tmp.completeFileName()));
			if(tc.first==tmp.checksum()&&tc.second==tmp.completeFileName())
				tp=mticket->count()-1;
		}
		else if(tmp.baseName()=="voucher"){
			mvoucher->addItem(tmp.description(), strpair2str(tmp.checksum(),tmp.completeFileName()));
			if(vc.first==tmp.checksum()&&vc.second==tmp.completeFileName())
				vp=mvoucher->count()-1;
		}
		else if(tmp.baseName()=="bill"){
			minvoice->addItem(tmp.description(), strpair2str(tmp.checksum(),tmp.completeFileName()));
			if(ic.first==tmp.checksum(),ic.second==tmp.completeFileName())
				ip=minvoice->count()-1;
		}
		else if(tmp.baseName()=="printathome"){
			mmail->addItem(tmp.description(), strpair2str(tmp.checksum(),tmp.completeFileName()));
			if(mc.first==tmp.checksum(),mc.second==tmp.completeFileName())
				mp=mmail->count()-1;
		}
	}
	qDebug()<<"tp"<<tp<<"vp"<<vp<<"ip"<<ip<<"mp"<<mp;
	mticket->setCurrentIndex(tp);
	mvoucher->setCurrentIndex(vp);
	minvoice->setCurrentIndex(ip);
	mmail->setCurrentIndex(mp);
	//update contacts
	QList<int> mcont=mailContactTypeIds();
	qDebug()<<"DEFAULT contacts"<<mcont<<mailContactTypes();
	for(auto ctc:mcontacttypes){
		const QString cn=ctc.contacttype();
		QCheckBox*cb=new QCheckBox(cn);
		cb->setChecked(mcont.contains(ctc.contacttypeid()));
		vl2->addWidget(cb);
		mcontacts.insert(ctc.contacttypeid(),cb);
	}
	//update shipping types
	QList<int> mship=shippingIds();
	qDebug()<<"DEFAULT shipping types"<<mship;
	for(auto ctc:mshiptypes){
		const QString cn=ctc.description();
		QCheckBox*cb=new QCheckBox(cn);
		cb->setChecked(mship.contains(ctc.shipid()));
		vl3->addWidget(cb);
		mshipping.insert(ctc.shipid(),cb);
	}

	vl->addSpacing(15);
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&OK")));
	connect(p,SIGNAL(clicked()),this,SLOT(save()),Qt::DirectConnection);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()),Qt::QueuedConnection);
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
}

void MPServerConfig::init(bool force)
{
	if(!force && mlastinit>=QDateTime::currentDateTime().addSecs(-(INIT_TIMER)))return;
	
	auto getsettings=req->queryGetPrintAtHomeSettings();
	if(getsettings.hasError())
		QMessageBox::warning(nullptr,tr("Warning"),tr("Unable to retrieve Print@Home Settings: %1").arg(getsettings.errorString()));
	else
		msettings=toMap(getsettings.getsettings());
	qDebug()<<"NEW-SETTINGS"<<msettings;
	
	mcontacttypes=req->queryGetAllContactTypes().gettypes();
	mshiptypes=req->queryGetAllShipping().getshipping();
	
	mlastinit=QDateTime::currentDateTime();
}

void MPServerConfig::save()
{
	//gather data
	QList<MOKeyValuePair> settings;
	settings.append(MOKeyValuePair(TICKETCS,mticket->currentData().toString()));
	settings.append(MOKeyValuePair(VOUCHERCS,mvoucher->currentData().toString()));
	settings.append(MOKeyValuePair(INVOICECS,minvoice->currentData().toString()));
	settings.append(MOKeyValuePair(MAILCS,mmail->currentData().toString()));
	settings.append(MOKeyValuePair(TICKETNM,mticketn->text()));
	settings.append(MOKeyValuePair(VOUCHERNM,mvouchern->text()));
	settings.append(MOKeyValuePair(INVOICENM,minvoicen->text()));
	QString mct;
	for(int cid:mcontacts.keys())
		if(mcontacts[cid]->isChecked())
			mct+=QString("%1 ").arg(cid);
	settings.append(MOKeyValuePair(MAILCONTACTS,mct.trimmed()));
	mct.clear();
	for(int sid:mshipping.keys())
		if(mshipping[sid]->isChecked())
			mct+=QString("%1 ").arg(sid);
	settings.append(MOKeyValuePair(SHIPPING,mct.trimmed()));
	qDebug()<<"SAVING"<<toMap(settings);
	
	//save
	auto setsettings=req->querySetPrintAtHomeSettings(settings);
	if(setsettings.hasError())
		QMessageBox::warning(nullptr,tr("Warning"),tr("Unable to store Print@Home Settings: %1").arg(setsettings.errorString()));

	//force re-init
	init(true);
}

QPair<QString,QString> MPServerConfig::ticketTemplateCS()
{
	init();
	return str2strpair(msettings.value(TICKETCS));
}

QPair<QString,QString> MPServerConfig::voucherTemplateCS()
{
	init();
	return str2strpair(msettings.value(VOUCHERCS));
}

QPair<QString,QString> MPServerConfig::invoiceTemplateCS()
{
	init();
	return str2strpair(msettings.value(INVOICECS));
}

QPair<QString,QString> MPServerConfig::mailTemplateCS()
{
	init();
	return str2strpair(msettings.value(MAILCS));
}

QStringList MPServerConfig::mailContactTypes()
{
	QStringList ret;
	for(int id:mailContactTypeIds())
		for(auto ct:mcontacttypes)
			if(ct.contacttypeid()==id)
				ret.append(ct.contacttype());
	return ret;
}

QList<int> MPServerConfig::mailContactTypeIds()
{
	init();
	QList<int>ret;
	for(auto s:msettings.value(MAILCONTACTS).split(' ',QString::SkipEmptyParts))
		ret.append(s.toInt());
	return ret;
}

QList<int> MPServerConfig::shippingIds()
{
	init();
	QList<int>ret;
	for(auto s:msettings.value(SHIPPING).split(' ',QString::SkipEmptyParts))
		ret.append(s.toInt());
	return ret;
}

QString MPServerConfig::ticketFileName()
{
	return msettings.value(TICKETNM,QCoreApplication::translate("MPServerConfig","tickets","file name"));
}

QString MPServerConfig::voucherFileName()
{
	return msettings.value(VOUCHERNM,QCoreApplication::translate("MPServerConfig","vouchers","file name"));
}

QString MPServerConfig::billFileName()
{
	return msettings.value(INVOICENM,QCoreApplication::translate("MPServerConfig","invoice","file name"));
}

bool MPServerConfig::validateConfig()
{
	init();
	//check we even have a config
	QPair<QString,QString>
		tc=ticketTemplateCS(),
		vc=voucherTemplateCS(),
		ic=invoiceTemplateCS(),
		mc=mailTemplateCS();
	if(tc.first.isEmpty()||vc.first.isEmpty()||ic.first.isEmpty())
		return false;
	//validate templates exist
	MTemplateStore*ts=req->templateStore();
	MTemplate tmp=ts->getTemplateByFile(tc.second);
	if(!tmp.isValid()||tmp.checksum()!=tc.first)return false;
	tmp=ts->getTemplateByFile(vc.second);
	if(!tmp.isValid()||tmp.checksum()!=vc.first)return false;
	tmp=ts->getTemplateByFile(ic.second);
	if(!tmp.isValid()||tmp.checksum()!=ic.first)return false;
	tmp=ts->getTemplateByFile(mc.second);
	if(!tmp.isValid()||tmp.checksum()!=mc.first)return false;
	//validate contacts exist
	for(int id:mailContactTypeIds()){
		bool found=false;
		for(const MOContactType &ct:mcontacttypes)
			if(ct.contacttypeid()==id){
				found=true;
				break;
			}
		if(!found)return false;
	}
	//validate shipping types
	for(int id:shippingIds()){
		bool found=false;
		for(const MOShipping&st:mshiptypes)
			if(st.shipid()==id){
				found=true;
				break;
			}
		if(!found)return false;
	}
	//done
	return true;
}
