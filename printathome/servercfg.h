//
// C++ Interface: print @ home, client config
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MSMOKE_PAH_SERVER_H
#define MSMOKE_PAH_SERVER_H

#include <QDialog>
#include <QMap>
#include <QList>
#include <QDateTime>
#include <QPair>

#include "MOContactType"
#include "MOShipping"

class QCheckBox;
class QLineEdit;
class QComboBox;
class QSpinBox;

class MPServerConfig:public QDialog
{
	Q_OBJECT
public:
	MPServerConfig(QWidget*parent=nullptr);
	
	///returns all contact types that are mail addresses
	static QStringList mailContactTypes();
	///returns the IDs of all contact types that are mail addresses
	static QList<int> mailContactTypeIds();

	///returns the IDs of shipping types used for PrintAtHome
	static QList<int> shippingIds();

	///returns true if the server configuration is consistent
	static bool validateConfig();

	///returns the template name for tickets
	static QString ticketTemplateName(){return ticketTemplateCS().second;}
	///returns the template name for vouchers
	static QString voucherTemplateName(){return voucherTemplateCS().second;}
	///returns the template name for bills
	static QString billTemplateName(){return invoiceTemplateCS().second;}
	///returns the template name for mails
	static QString mailTemplateName(){return mailTemplateCS().second;}

	///returns the name under which the ticket PDF should be stored on the server
	static QString ticketFileName();
	///returns the name under which the voucher PDF should be stored on the server
	static QString voucherFileName();
	///returns the name under which the bill/invoice PDF should be stored on the server
	static QString billFileName();
public slots:
	void save();
	
private:
	QComboBox*mticket,*mvoucher,*minvoice,*mmail;
	QLineEdit*mticketn,*mvouchern,*minvoicen;
	QMap<int,QCheckBox*>mcontacts,mshipping;
	static QMap<QString,QString>msettings;
	static QList<MOContactType>mcontacttypes;
	static QList<MOShipping>mshiptypes;
	static QDateTime mlastinit;
		
	static QPair<QString,QString> ticketTemplateCS();
	static QPair<QString,QString> voucherTemplateCS();
	static QPair<QString,QString> invoiceTemplateCS();
	static QPair<QString,QString> mailTemplateCS();
	static void init(bool force=false);
};

#endif
