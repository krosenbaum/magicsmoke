TEMPLATE = app
TARGET = magicsmoke-print

include(../basics.pri)
include(../sesscli/sesscli.pri)
include(../iface/iface.pri)
include(../commonlib/commonlib.pri)

#sources
SOURCES += pah.cpp clientcfg.cpp servercfg.cpp printrun.cpp
HEADERS += pah.h clientcfg.h servercfg.h printrun.h

RESOURCES += files.qrc

#pathes
INCLUDEPATH += .
DEPENDPATH += $$INCLUDEPATH

#make sure the correct Qt DLLs are used
QT += network gui widgets printsupport
