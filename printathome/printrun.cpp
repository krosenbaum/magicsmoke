//
// C++ Implementation: print @ home run
//
// Description: Print runner
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "mapplication.h"
#include "msinterface.h"
#include "MOOrder"
#include "MOOrderDocument"
#include "MTGetCustomer"
#include "MTGetOrder"
#include "MTGetOrderList"
#include "MTSetOrderDocument"
#include "WTransaction"
#include "MTSendCustomerMail"
#include "MTChangeCustomerMail"

#include "ticketrender.h"
#include "odtrender.h"
#include "billrender.h"
#include "stick.h"

#include "pah.h"
#include "printrun.h"
#include "servercfg.h"
#include "clientcfg.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QPainter>
#include <QPrinter>
#include <QStringList>
#include <QTemporaryFile>
#include <QVariant>

PrintingRun::PrintingRun()
{
	// get templates
	MTemplateStore *ts=req->templateStore();
	ticket=ts->getTemplateByFile(MPServerConfig::ticketTemplateName());
	voucher=ts->getTemplateByFile(MPServerConfig::voucherTemplateName());
	invoice=ts->getTemplateByFile(MPServerConfig::billTemplateName());
	mail=ts->getTemplateByFile(MPServerConfig::mailTemplateName());

	mticketname=MPServerConfig::ticketFileName()+".pdf";
	mvouchername=MPServerConfig::voucherFileName()+".pdf";
	minvoicename=MPServerConfig::billFileName()+".pdf";

	// get orders
	const int age=MPClientConfig::maximumAgeOfOrders();
	const qint64 oldest=age<=0?0:QDateTime::currentDateTime().addDays(-age).toTime_t();
	MTGetOrderList olt=req->queryGetOrderList(oldest);
	QList<MOOrderInfo> olist=olt.getorders();
	mcustomers=olt.getcustomers();

	// filter orders
	const QList<int>shipids=MPServerConfig::shippingIds();
	for(const MOOrderInfo&ord:olist)
		if(ord.isPlaced() && shipids.contains(ord.shippingtypeid()))
			morders.append(ord);
	qDebug()<<"Processing"<<morders.size()<<"orders of"<<olist.size()<<"since"<<age<<"days";
}

void PrintingRun::exec()
{
	for(const MOOrderInfo&ord:morders)
		printOneOrder(ord);
	qDebug()<<"Finished Print Run.";
}

void PrintingRun::printOneOrder(const MOOrderInfo&orderinfo)
{
	qDebug()<<"Printing order"<<orderinfo.orderid();
	//get order details
	//TODO: mark as shipped instead
	MOOrder order=req->queryGetOrder(orderinfo.orderid()).getorder();

	QList<MOOrderDocument> doclist;

	//generate tickets
	if(order.tickets().size()>0){
		QTemporaryFile tmp(QDir::tempPath()+"/mspah-XXXXXX.pdf");
		tmp.open();tmp.close();
		QPrinter prn(QPrinter::HighResolution);
		prn.setOutputFileName(tmp.fileName());
		MTicketRenderer render(ticket);
		prn.setPageSizeMM(render.labelSizeMM());
		QPainter*paint=new QPainter(&prn);
		bool np=false;
		for(MOTicket tick:order.tickets()){
			//do we know the event?
			if(mevents.contains(tick.eventid()))
				tick.setEvent(mevents[tick.eventid()]);
			else
				//this automatically retrieves the event
				if(tick.event().isValid())
					mevents.insert(tick.eventid(),tick.event());
			//print the ticket
			if(np)prn.newPage();
			else np=true;
			render.render(tick,prn,paint);
		}
		delete paint;
		uploadFile(order.orderid(),tmp.fileName(),mticketname,doclist);
	}

	//generate vouchers
	if(order.vouchers().size()>0){
		QTemporaryFile tmp(QDir::tempPath()+"/mspah-XXXXXX.pdf");
		tmp.open();tmp.close();
		QPrinter prn(QPrinter::HighResolution);
		prn.setOutputFileName(tmp.fileName());
		MVoucherRenderer render(voucher);
		prn.setPageSizeMM(render.labelSizeMM());
		QPainter*paint=new QPainter(&prn);
		bool np=false;
		for(MOVoucher vouc:order.vouchers()){
			//print the voucher
			if(np)prn.newPage();
			else np=true;
			render.render(vouc,prn,paint);
		}
		delete paint;
		uploadFile(order.orderid(),tmp.fileName(),mvouchername,doclist);
	}

	//generate bill
	MBillRenderer orender(order,invoice);
	QString pname=orender.renderToPdf();
	qDebug()<<"rendered bill to"<<pname;
	uploadFile(order.orderid(),pname,minvoicename,doclist);

	//send mail
	sendMail(order,doclist);
}

void PrintingRun::uploadFile(int orderid,QString localfile,QString remotefile,QList<MOOrderDocument>&doclist)
{
	MOOrderDocument doc;
	doc.setorderid(orderid);
	doc.setfilename(remotefile);
	QFile fd(localfile);
	fd.open(QIODevice::ReadOnly);
	doc.setcontent(fd.readAll());
	fd.close();
	doc.setvisible(true);
	MTSetOrderDocument trans=MTSetOrderDocument::query(doc);
	doclist.append(doc);
}

void PrintingRun::sendMail(const MOOrder&order,const QList<MOOrderDocument>&doclist)
{
	//get customer details
	MOCustomer cust;
	if(mcustomerdetails.contains(order.customerid()))
		cust=mcustomerdetails[order.customerid()];
	else
		cust=MTGetCustomer::query(order.customerid()).getcustomer();
	QStringList cc;
	const QList<int>contacttypes=MPServerConfig::mailContactTypeIds();
	for(auto co:cust.contacts())
		if(contacttypes.contains(co.contactid()))
			cc<<co.contact();
	//feed renderer
	MStickRenderer render;
	render.setProperty("order",QVariant::fromValue(order));
	render.setProperty("docs",QVariant::fromValue(doclist));
	render.setProperty("customer",QVariant::fromValue(cust));
	render.setProperty("cc",cc);
	render.setTemplateFile(mail.cacheFileName());
	//execute
	QString mymail=render.render();
	//make sure customer has a mail address configured
	if(cust.email().isNull() || cust.email().value().isEmpty()){
		if(cc.size()>0)
			MTChangeCustomerMail::query(cust.customerid(),cc[0]);
	}
	//send mail
	MTSendCustomerMail::query(order.customerid(),mymail);
}
