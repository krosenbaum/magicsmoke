//
// C++ Implementation: print @ home
//
// Description: Client Settings
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "clientcfg.h"
#include "pah.h"

#include "scli.h"

#include <QFormLayout>
#include <QBoxLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QSettings>

#define GROUP PRINTATHOME_SETTINGSGROUP "/Client"
#define TIMER GROUP "/timermin"
#define ORDERAGE GROUP "/orderage"
#define SETPROFILE GROUP "/setprofile"
#define PROFILE GROUP "/profile"
#define SETUSER GROUP "/setuser"
#define USERNAME GROUP "/username"
#define AUTOLOGIN GROUP "/autologin"
#define PASSWD GROUP "/passwd"


MPClientConfig::MPClientConfig(QWidget* parent)
:QDialog(parent)
{
	setWindowTitle(tr("Print@Home Client Configuration"));
	QVBoxLayout*vl;
	setLayout(vl=new QVBoxLayout);
	QFormLayout*fl;
	vl->addLayout(fl=new QFormLayout);
	//check Interval
	fl->addRow(tr("Check Interval in Minutes:"),mtimer=new QSpinBox);
	mtimer->setRange(1,10080);
	mtimer->setValue(timerInMinutes());
	//age of orders
	fl->addRow(tr("Maximum Age of Orders in Days:"),morderage=new QSpinBox);
	morderage->setRange(0,999);
	morderage->setValue(maximumAgeOfOrders());
	//profile
	fl->addRow("",msetprofile=new QCheckBox(tr("Preselect Profile")));
	fl->addRow(tr("Profile:"),mprofile=new QComboBox);
	connect(msetprofile,SIGNAL(clicked(bool)),mprofile,SLOT(setEnabled(bool)));
	msetprofile->setChecked(preselectProfile());
	//get profile names
	auto prfs=MSessionClient::instance()->profiles();
	const QString cprf=preselectedProfileId();
	int cprfn=-1;
	for(auto prf:prfs){
		if(prf.first==cprf)cprfn=mprofile->count();
		mprofile->addItem(prf.second,prf.first);
	}
	mprofile->setCurrentIndex(cprfn);
	//user
	fl->addRow("",msetuser=new QCheckBox(tr("Set User Name")));
	fl->addRow(tr("User Name:"),musername=new QLineEdit);
	musername->setText(preselectedUserName());
	connect(msetuser,SIGNAL(clicked(bool)),musername,SLOT(setEnabled(bool)));
	msetuser->setChecked(preselectUser());
	//password
	fl->addRow("",mautologin=new QCheckBox(tr("Auto-Login")));
	fl->addRow(tr("Password:"),mpassword=new QLineEdit);
	connect(mautologin,SIGNAL(clicked(bool)),mpassword,SLOT(setEnabled(bool)));
	mautologin->setChecked(autoLogin());
	mpassword->setEchoMode(QLineEdit::Password);
	mpassword->setText(password());
	
	vl->addSpacing(15);
	QHBoxLayout*hl;
	vl->addLayout(hl=new QHBoxLayout);
	hl->addStretch(1);
	QPushButton*p;
	hl->addWidget(p=new QPushButton(tr("&OK")));
	connect(p,SIGNAL(clicked()),this,SLOT(save()),Qt::DirectConnection);
	connect(p,SIGNAL(clicked()),this,SLOT(accept()),Qt::QueuedConnection);
	hl->addWidget(p=new QPushButton(tr("&Cancel")));
	connect(p,SIGNAL(clicked()),this,SLOT(reject()));
	
}

//note: this is NOT an encryption algorithm - it merely masks against accidental reading, it is trivial to reverse
static inline QByteArray maskPwd(QString pwd)
{
	QByteArray r=pwd.toUtf8();
	int m=0x58;
	for(int i=0;i<r.size();i++){
		r[i]=r[i]^(char)m;
		m++;
	}
	return r.toBase64();
}

//note 2: this is how trivial it is...
// maybe we should use TRIPLE-rot13? ;-)
static inline QString unmaskPwd(QByteArray ms)
{
	ms=QByteArray::fromBase64(ms);
	int m=0x58;
	for(int i=0;i<ms.size();i++){
		ms[i]=ms[i]^(char)m;
		m++;
	}
	return QString::fromUtf8(ms);
}

void MPClientConfig::save()
{
	QSettings set;
	set.setValue(SETPROFILE,msetprofile->isChecked());
	set.setValue(TIMER,mtimer->value());
	set.setValue(ORDERAGE,morderage->value());
	set.setValue(PROFILE,mprofile->currentData());
	set.setValue(SETUSER,msetuser->isChecked());
	set.setValue(USERNAME,musername->text());
	set.setValue(AUTOLOGIN,mautologin->isChecked());
	set.setValue(PASSWD,maskPwd(mpassword->text()));
}

bool MPClientConfig::preselectProfile()
{
	return QSettings().value(SETPROFILE,false).toBool();
}

int MPClientConfig::timerInMinutes()
{
	return QSettings().value(TIMER,60).toInt();
}

int MPClientConfig::maximumAgeOfOrders()
{
	return QSettings().value(ORDERAGE,7).toInt();
}


QString MPClientConfig::preselectedProfileId()
{
	return QSettings().value(PROFILE).toString();
}

bool MPClientConfig::preselectUser()
{
	return QSettings().value(SETUSER,false).toBool();
}

QString MPClientConfig::preselectedUserName()
{
	return QSettings().value(USERNAME).toString();
}

bool MPClientConfig::autoLogin()
{
	return QSettings().value(AUTOLOGIN,false).toBool();
}

QString MPClientConfig::password()
{
	return unmaskPwd(QSettings().value(PASSWD).toByteArray());
}

void MPClientConfig::performAutoLogin(MSessionClient&sc)
{
	sc.waitForReady();
	if(preselectProfile())
		sc.setProfile(preselectedProfileId());
	if(preselectUser())
		sc.login(preselectedUserName(),password(),autoLogin());
}
