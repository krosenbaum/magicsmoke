#Project File for MagicSmoke
# (c) Konrad Rosenbaum, 2007-2013

TEMPLATE = app
TARGET = magicsmoke

#add the icon for windoze
win32-* {
 #RC-File containing the icon:
 RC_FILE += win.rc
}

#main source files
SOURCES = main.cpp
INCLUDEPATH += . ../src

#make sure dependencies are found
DEPENDPATH += $$INCLUDEPATH
LIBS += -lmagicsmoke

#security features
linux-g++* {
  message("activating ASLR and friends")
  QMAKE_CFLAGS += -fPIE
  QMAKE_CXXFLAGS += -fPIE
  QMAKE_LFLAGS += -pie
}

include (../basics.pri)
include (../src/libs.pri)
