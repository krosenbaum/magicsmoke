<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

//check for maintenance mode
if(file_exists("maintenance.php"))
	die("<!-- Sorry, Down For Maintenance! -->");

//fix content-type to something that is not manipulated by proxies
header("Content-Type: application/x-MagicSmoke");

//initialize
include("inc/loader.php");
include("inc/loader_nonadmin.php");

//init
Session::initialize();

//let wob do the rest
MSmokeTransaction::handle();
WobTransaction::printDebug();

//done
exit(0);
?>
