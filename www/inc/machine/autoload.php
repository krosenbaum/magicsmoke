<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


wob_autoclass("Session","./inc/machine/session.php");
wob_autoclass("Version","./inc/machine/version.php");
wob_autoclass("Translation","./inc/machine/translation.php");
wob_autoclass("MachineUser","./inc/machine/muser.php");

//eof
return;
?>
