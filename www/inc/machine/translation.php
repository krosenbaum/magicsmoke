<?php
// (c) Konrad Rosenbaum, 2007-2017
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class Translation
{
	static public function getFile($trans)
	{
		//status bar display
		global $StatusBarText;
		if(isset($StatusBarText))
			$trans->setStatusBarText($StatusBarText);

		//split language string into components
		$nlst=explode("_",trim($trans->getlanguage()));
		//set format object
		if(count($nlst)>0)
			$trans->setformats(new WOServerFormat($nlst[0]));
		else
			$trans->setformats(new WOServerFormat());
		//if this is "C", abort parsing and return
		if(strtolower(trim($trans->getlanguage()))=="c")
			return;
		//continue to parse language request
		$form=$trans->getformat();
		if($form!="ts" && $form!="qm")
			$trans->abortWithError(translate("Translation","Format must be either 'ts' or 'qm'."));
		//does the syntax match?
		foreach($nlst as $nm)
			if(!preg_match("/^[a-zA-Z]{1,3}$/",$nm))
				$trans->abortWithError(translate("Translation","Language invalid."));
		//find the longest match for language
		for($i=count($nlst);$i>0;$i--){
			$name=implode("_",array_slice($nlst,0,$i));
			$fn="translations/_server_".$name.".".$form;
			if(file_exists($fn)){
				$trans->setfile(file_get_contents($fn,FILE_BINARY));
				return;
			}
		}

		//none found. sadly.
		$trans->abortWithError(translate("Translation","Language unknown."));
	}
};

//eof
return;
?>
