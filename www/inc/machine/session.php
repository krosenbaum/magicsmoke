<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/* TRANSLATOR php:: */


/**The session class*/
class Session
{
	protected $sessid="";
	protected $user="";
	protected $roles=array();
	protected $rights=array();
	protected $flags=array();
	
	///initialize session sub-system
	static public function initialize()
	{
		//prune session cache
		global $db;
		$db->beginTransaction();
		$db->deleteRows("session","timeout<=".time());
		$db->commitTransaction();
	}
	
	/**construct the session object, check validity*/
	public function __construct($trans)
	{
		global $db,$session;
		$sid=$trans->getHeader("SessionId");
		$res=$db->select("session","sessionid,uname","sessionid=".$db->escapeString($sid));
		if(count($res)>0){
			$this->sessid=$sid;
			$this->user=$res[0]["uname"];
			$this->initRights();
		}
		$session=$this;
	}
	
	/**return the session instance*/
	public static function instance()
	{
		global $session;
		if(!isset($session))return null;
		return $session;
	}
	
	/**return the name of the currently logged in user*/
	public static function currentUserName()
	{
		global $session;
		if(isset($session))return $session->getUser();
		else return "(unknown)";
	}
	
	/**initialize system in web session mode (ie. some user called the web page in a browser)*/
	public static function setWebSession()
	{
		global $session;
		$session=new DummyWebSession;
	}
	
	/**internal: retrieve and remember the rights, roles, and flags of this user*/
	protected function initRights()
	{
		global $db;
		//get roles
		$res=$db->select("userrole","role","uname=".$db->escapeString($this->user));
		for($i=0;$i<count($res);$i++){
			$this->roles[]=$res[$i][0];
		}
		//get rights
		$res=$db->select("roleright","rightname","rolename IN ".$db->escapeStringList($this->roles));
		for($j=0;$j<count($res);$j++)
			$this->rights[]=$res[$j][0];
		//get flags
		//user flags
		$res=$db->select("user","uname,flags","uname=".$db->escapeString($this->user));
		if(count($res)>0)
			foreach(explode(" ",$res[0]['flags']) as $f)
				if(!in_array($f,$this->flags))
					$this->flags[]=$f;
		//role flags
		$res=$db->select("role","flags","rolename IN ".$db->escapeStringList($this->roles));
		for($j=0;$j<count($res);$j++)
			foreach(explode(" ",$res[$j][0]) as $f)
				if(!in_array($f,$this->flags))
					$this->flags[]=$f;
	}
	
	/**returns all rights of this user*/
	public function getRights(){return $this->rights;}
	
	/**returns all roles of this user*/
	public function getRoles(){return $this->roles;}
	
	/**returns the flags of this user*/
	public function getFlags(){return $this->flags;}
	
	/**returns whether the user has a specific flag*/
	public function hasFlag($f){return in_array($f,$this->flags);}
	
	/**creates a new session, called from the Login transaction*/
	static public function login($trans)
	{
		global $db,$ClientSessionTimeout;
		//get host data
		$uhres=$db->select("userhost","host","uname=".$db->escapeString($trans->getusername()));
		$hres=$db->select("host","*","hostname=".$db->escapeString($trans->gethostname()));
		$hosts=array();
		foreach($uhres as $hst)
			$hosts[]=$hst["host"];
		//logic check 1: abort if host is unknown
		if(count($hres)==0){
			$trans->abortWithError(translate("Session","Unknown Host"),"auth");
		}
		//logic check: login is allowed if
		// a) $hosts contains _any and the host is known, or
		// b) $hosts contains the transmitted host name
		$hostname=$trans->gethostname();
		if( !in_array($hostname,$hosts) && !in_array("_any",$hosts)){
			$trans->abortWithError(translate("Session","Host/User combination not allowed"),"auth");
		}
		
		//validate host
		if(!self::passwdVerify($trans->gethostkey(),$hres[0]["hostkey"])){
			$trans->abortWithError(translate("Session","Host authentication failed"),"auth");
		}
		
		//get user data
		$ures=$db->select("user","*","uname=".$db->escapeString($trans->getusername()));
		if(count($ures)<1){
			$trans->abortWithError(translate("Session","User Authentication failed"),"auth");
		}
		//validate user
		if(!self::passwdVerify($trans->getpassword(),$ures[0]["passwd"])){
			$trans->abortWithError(translate("Session","User Authentication failed"),"auth");
		}
		
		//create session and return
		//get random bits
		$sid=getRandom(128);
		//try to create entry
		$db->beginTransaction();
		while(1){
			//check for existence
			$res=$db->select("session","sessionid","sessionid='".$sid."'");
			if(count($res)==0)break;
			//create new SID and repeat
			$sid=getRandom(128);
		}
		$valid=time()+$ClientSessionTimeout;
		$ret=array("sessionid"=>$sid,"uname"=>$trans->getusername(),"timeout"=>$valid);
		$db->insert("session",$ret);
		$db->commitTransaction();
		
		$trans->setsessionid($sid);
		$trans->setvalidtill($valid);
	}
	
	/**delete current session*/
	function deleteSession()
	{
		global $db;
		$db->deleteRows("session","sessionid=".$db->escapeString($this->sessid));
	}

	/**returns true if the session exists, it may still be temporary and unauthenticated*/
	public function isValid()
	{
		return $this->sessid!="";
	}
	
	/**returns the user name of the session*/
	public function getUser()
	{
		return $this->user;
	}
	
	/**returns true if the session is actually authenticated*/
	public function isAuthenticated()
	{
		return $this->user!="";
	}
	
	/**set my own password; called from SetMyPasswd transaction*/
	public function setMyPasswd($trans)
	{
		$old=$trans->getoldpassword();
		$nwp=$trans->getnewpassword();
		//sanity check
		if($nwp=="")$trans->abortWithError(tr("cannot set an empty password"));
		//check old password
		global $db;
		$res=$db->select("user","passwd","uname=".$db->escapeString($this->user));
		if(count($res)!=1)
			$trans->abortWithError(tr("Ooops. Unable to find user. You have been deleted."));
		if(!self::passwdVerify($old,$res[0]["passwd"])){
			$trans->abortWithError(tr("Wrong password. Request denied."));
		}
		//set new password
		$pwh=self::passwdHash($nwp);
		$db->update("user",array("passwd"=>($pwh)),"uname=".$db->escapeString($this->user));
	}
	
	/**checks whether user can execute this transaction, returns true on success; it always returns true for admins*/
	public function canExecute($transaction)
	{
		global $db;
		if(in_array("_admin",$this->roles))return true;
		if(in_array("_admin",$this->flags))return true;
		return in_array($transaction,$this->rights);
	}
	
	/**checks the given flags item pattern (string or array of strings) and returns true if they match*/
	public function checkFlags($iflg)
	{
		//admin shortcut: allow everything
		if($this->hasFlag("_admin"))return true;
		if(in_array("_admin",$this->roles))return true;
		//actual check for "mere mortals"
		if(is_array($iflg))$fp=$iflg;
		else $fp=explode(" ",$iflg);
		foreach($fp as $f){
			//discard dummies
			$f=trim($f);
			if($f=="")continue;
			// flag starting with "+": check that user has it
			if($f[0]=="+"){
				if(!$this->hasFlag(substr($f,1)))return false;
			}else
			// flag starting with "-": check that user does not have it
			if($f[0]=="-"){
				if($this->hasFlag(substr($f,1)))return false;
			}
			// flag starting with anything else: errrm. error out
			else return false;
		}
		//no matches, must be ok then
		return true;
	}
	
	///helper function to generate salted user or customer password hash
	/// \param $passwd unhashed password
	/// \returns the salted password hash
	public static function passwdHash($passwd)
	{
		$salt=getSalt();
		return $salt." ".sha1($salt.$passwd);
	}
	
	///helper function to verify user or customer login
	/// \param $passwd unhashed password
	/// \param $hash salted password hash
	/// \returns true on success, false on error or if the password does not match
	public static function passwdVerify($passwd,$hash)
	{
		//separate salt and hash
		$splt=explode(" ",$hash);
		if(count($splt)!=2){
			return false;
		}
		//redo calculation
		$cmp=sha1($splt[0].$passwd);
		//compare (tolower since we use string compare on a hex number)
		return strtolower($cmp)==strtolower($splt[1]);
	}
};

/**dummy class used by browsed pages to represent the virtual web user*/
class DummyWebSession extends Session
{
	public function __construct()
	{
		//DO NOT call parent constructor:
		// it would try to verify the session and deny us access
		global $db;
		//there is often no real user for web (so we enforce it)
		$this->user="_web";
		//fake web role and flag, some objects check for it
		$this->roles[]="_web";
		$this->flags[]="_web";
		//load rights, roles, etc.
		$this->initRights();
	}
}

//eof
return;
?>
