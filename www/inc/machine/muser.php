<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/**encapsulated machine user/role/host management in several static functions;
it is called directly from the user centered transactions;
DO NOT USE THIS CLASS OUTSIDE TRANSACTION CONTEXT!*/
class MachineUser
{
	/**create a new user*/
	static public function createUser($trans)
	{
		//check whether user exists
		$usr=WTuser::getFromDB($trans->getusername());
		if($usr!==false){
			$trans->abortWithError(tr("User already exists."));
			return;
		}
		//verify syntax
		if(!preg_match("/^[a-zA-Z]([a-zA-Z_\\.,:-]*)$/",$trans->getusername())){
			$trans->abortWithError(tr("Username is invalid."));
			return;
		}
		//create DB entry
		$usr=WTuser::newRow();
		$usr->uname=$trans->getusername();
		$slt=getSalt();
		$hsh=sha1($slt.$trans->getpassword());
		$usr->passwd=$slt." ".$hsh;
		$usr->insert();
		$trans->setuser(WOUser::fromTableuser($usr));
	}
	
	/**deletes or merges a user*/
	static public function deleteUser($trans)
	{
		//sanity check: do users exist
		$un=$trans->getusername();
		$usr=WTuser::getFromDB($un);
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		$mun=$trans->getmergewithuser();
		if($mun=="")$mun=false;
		$mu=false;
		if($mun!==false){
			$mu=WTuser::getFromDB($mun);
			if($mu===false){
				$trans->abortWithError(tr("Merge target user does not exist!"));
				return;
			}
		}
		//check: are users identical?
		if($un==$mun){
			$trans->abortWithError(tr("User and merge target user are identical."));
			return;
		}
		//perform merge
		global $db;
		if($mu!==false){
			//TODO: merge users
		}
		//delete user
		$usr->deleteFromDB();
	}
	
	/**change a users password*/
	static public function changePasswd($trans)
	{
		$usr=WTuser::getFromDB($trans->getusername());
		if(!is_a($usr,"WTuser")){
			$trans->abortWithError(tr("User does not exist."));
			retrun;
		}
		$slt=getSalt();
		$hsh=sha1($slt.$trans->getpassword());
		$usr->passwd=$slt." ".$hsh;
		$usr->update();
	}
	
	/**sets the description of a user*/
	static public function setUserDescription($trans)
	{
		$usr=WTuser::getFromDB($trans->getusername());
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		$usr->description=$trans->getdescription();
		$usr->update();
	}
	
	/**returns the roles of a user*/
	static public function getUserRoles($trans)
	{
		//sanity check
		$usr=WTuser::getFromDB($trans->getusername());
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//get roles
		global $db;
		$rls=WTuserrole::selectFromDB("uname=".$db->escapeString($trans->getusername()));
		$r=array();
		foreach($rls as $rl){
			$r[]=$rl->role;
		}
		$trans->setroles($r);
	}
	
	/**set roles of a user*/
	static public function setUserRoles($trans)
	{
		//sanity check
		$uname=$trans->getusername();
		$usr=WTuser::getFromDB($uname);
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//verify roles
		global $db;
		$res=$db->select("role","rolename");
		$arole=array();
		foreach($res as $r)
			$arole[]=$r["rolename"];
		$roles=array_unique(array_values($trans->getroles()));
		foreach($roles as $r){
			if(!in_array($r,$arole)){
				$trans->abortWithError(tr("Trying to assign non-existent role."));
				return;
			}
		}
		//delete old ones
		$db->deleteRows("userrole","uname=".$db->escapeString($uname));
		//create new ones
		foreach($roles as $r)
			$db->insert("userrole",array("uname"=>$uname,"role"=>$r));
	}
	
	/**get hosts of a user*/
	static public function getUserHosts($trans)
	{
		//sanity check
		$uname=$trans->getusername();
		$usr=WTuser::getFromDB($uname);
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//get hosts
		global $db;
		$res=$db->select("userhost","host","uname=".$db->escapeString($uname));
		$ret=array();
		foreach($res as $r)
			$ret[]=$r["host"];
		$trans->sethosts($ret);
	}
	
	/**set the hosts for a user*/
	static public function setUserHosts($trans)
	{
		//sanity check
		$uname=$trans->getusername();
		$usr=WTuser::getFromDB($uname);
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//verify roles
		global $db;
		$res=$db->select("host","hostname");
		$ahost=array();
		foreach($res as $r)
			$ahost[]=$r["hostname"];
		$hosts=array_unique(array_values($trans->gethosts()));
		foreach($hosts as $r){
			if(!in_array($r,$ahost)){
				$trans->abortWithError(tr("Trying to assign non-existent host."));
				return;
			}
		}
		//delete old ones
		$db->deleteRows("userhost","uname=".$db->escapeString($uname));
		//create new ones
		foreach($hosts as $r)
			$db->insert("userhost",array("uname"=>$uname,"host"=>$r));
	}
	
	/**get all host names (sub-routine for clients user host dialog)*/
	static public function getAllHostNames($trans)
	{
		global $db;
		$res=$db->select("host","hostname");
		$ret=array();
		foreach($res as $r)
			$ret[]=$r["hostname"];
		$trans->sethostnames($ret);
	}
	
	/**sets the flags of a role*/
	static public function setRoleFlags($trans)
	{
		//sanity check
		$rname=$trans->getrole();
		$role=WTrole::getFromDB($rname);
		if($role===false){
			$trans->abortWithError(tr("Role does not exist."));
			return;
		}
		//verify flags
		$aflags=array();
		foreach(WOFlag::getAll() as $f)$aflags[]=$f->getflag();
		$flags=array_unique(array_values($trans->getflags()));
		foreach($flags as $f){
			if(!in_array($f,$aflags)){
				$trans->abortWithError(tr("Trying to assign non-existent flag."));
				return;
			}
		}
		//set
		$role->flags=implode(" ",$flags);
		$role->update();
	}
	
	/**sets the flags of a user*/
	static public function setUserFlags($trans)
	{
		//sanity check
		$uname=$trans->getusername();
		$usr=WTuser::getFromDB($uname);
		if($usr===false){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//verify flags
		$aflags=array();
		foreach(WOFlag::getAll() as $f)$aflags[]=$f->getflag();
		$flags=array_unique(array_values($trans->getflags()));
		foreach($flags as $f){
			if(!in_array($f,$aflags)){
				$trans->abortWithError(tr("Trying to assign non-existent flag."));
				return;
			}
		}
		//set
		$usr->flags=implode(" ",$flags);
		$usr->update();
	}

	/**sets the description of a role*/
	static public function setRoleDescription($trans)
	{
		$role=WTrole::getFromDB($trans->getrole());
		if(!is_a($role,"WTrole")){
			$trans->abortWithError(tr("Role does not exist."));
			return;
		}
		//update
		$role->description=$trans->getdescription();
		$role->update();
	}
	
	/**creates a role*/
	static public function createRole($trans)
	{
		//check for syntax
		$rnm=trim($trans->getrole()."");
		if(!preg_match("/^[a-zA-Z][a-zA-z0-9_\\.-]*$/",$rnm)){
			$trans->abortWithError(tr("Illegal role name."));
			return;
		}
		//check whether it exists
		$role=WTrole::getFromDB($rnm);
		if(is_a($role,"WTrole")){
			$trans->abortWithError(tr("Role already exists."));
			return;
		}
		//create
		$role=WTrole::newRow();
		$role->rolename=$rnm;
		$role->insert();
	}
	
	/**changes the rights attached to a role*/
	static public function setRoleRights($trans)
	{
		global $db;
		//check role
		$rnm=trim($trans->getrole());
		$role=WTrole::getFromDB($rnm);
		if(!is_a($role,"WTrole")){
			$trans->abortWithError(tr("Role does not exist."));
			return;
		}
		//check rights
		$allrights=array_merge(WobTransaction::transactionNames(),
			WobTransaction::privilegeNames());
		$set=$trans->getrights();
		foreach($set as $r){
			if(!in_array($r,$allrights)){
				$trans->abortWithError(tr("Trying to set an illegal right."));
				return;
			}
		}
		//delete old set
		$db->deleteRows("roleright","rolename=".$db->escapeString($rnm));
		//set new set
		foreach($set as $r){
			$db->insert("roleright",array("rolename"=>$rnm,"rightname"=>$r));
		}
	}
	
	/**delete role*/
	static public function deleteRole($trans)
	{
		$rnm=trim($trans->getrole());
		//privileged?
		if(substr($rnm,0,1)=="_"){
			$trans->abortWithError(tr("Cannot delete special roles."));
			return;
		}
		//find
		$role=WTrole::getFromDB($rnm);
		if(!is_a($role,"WTrole")){
			$trans->abortWithError(tr("Role does not exist."));
			return;
		}
		//delete foreign key refs
		global $db;
		$db->deleteRows("userrole","role=".$db->escapeString($rnm));
		$db->deleteRows("roleright","rolename=".$db->escapeString($rnm));
		//delete
		$role->deleteFromDB();
	}
	
	/**create/update host*/
	static public function setHost($trans)
	{
		//check host name
		$hname=$trans->getname();
		if(substr($hname,0,1)=="_"){
			$trans->abortWithError(tr("Cannot set/create special hosts."));
			return;
		}
		if(!preg_match("/^[a-zA-Z][a-zA-z0-9_\\.-]*$/",$hname)){
			$trans->abortWithError(tr("Illegal host name."));
			return;
		}
		//get host object
		$hst=WThost::getFromDB($hname);
		if(!is_a($hst,"WThost")){
			$hst=WThost::newRow();
			$hst->hostname=$hname;
		}
		//update
		$hst->hostkey=$trans->getkey();
		$hst->insertOrUpdate();
	}
	
	/**deletes a host*/
	static public function deleteHost($trans)
	{
		//check host name
		$hname=$trans->getname();
		if(substr($hname,0,1)=="_"){
			$trans->abortWithError(tr("Cannot delete special hosts."));
			return;
		}
		//get host object
		$hst=WThost::getFromDB($hname);
		if(!is_a($hst,"WThost")){
			$trans->abortWithError(tr("Host does not exist."));
			return;
		}
		$hst->deleteFromDB();
	}
};

//eof
return;
?>
