<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

class Version {
public static function serverInfoXml($trans)
{
	global $MSVERSION;
	$trans->setServerVersion($trans->version());
	$trans->setMinimumProtocolVersion($trans->needCommVersion());
	$trans->setServerProtocolVersion($trans->commVersion());
}
};

//eof
return;
?>
