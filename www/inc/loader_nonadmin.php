<?php
// (c) Konrad Rosenbaum, 2007-2011
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

//check database
if(!$db->canUseDb())
	die("Database is not correctly configured. Giving up.");
//TODO: convert random into class and move to framework
include_once('./inc/classes/random.php');
//load external Twig library
require_once('inc/Twig/lib/Twig/Autoloader.php');

//load web-UI specific class-files
include('inc/classes/autoload.php');
include("inc/rendering/autoload.php");
//load globals
include('inc/global_functions.php');

//done
return;
?>
