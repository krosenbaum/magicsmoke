<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


abstract class BarcodeTable extends WobTable
{
	private static $NumTicketChars=false;
	private static $NumVoucherChars=false;
	private static $NumCouponChars=false;
	private static function init()
	{
		global $db;
		if(self::$NumTicketChars===false){
			self::$NumTicketChars=$db->getConfig("TicketIDChars")+0;
			if(self::$NumTicketChars<=5)self::$NumTicketChars=10;
		}
		if(self::$NumVoucherChars===false){
			self::$NumVoucherChars=$db->getConfig("VoucherIDChars")+0;
			if(self::$NumVoucherChars<=5)self::$NumVoucherChars=10;
		}
		if(self::$NumCouponChars===false){
			self::$NumCouponChars=$db->getConfig("CouponIDChars")+0;
			if(self::$NumCouponChars<=5)self::$NumCouponChars=10;
		}
	}
	public static function getNewTicketId()
	{
		global $db;
		self::init();
		do{
			$tid=getCode39ID(self::$NumTicketChars,RND_TICKET);
			$res=$db->select("ticket","ticketid","ticketid=".$db->escapeString($tid));
			if(count($res)==0)return $tid;
		}while(true);
	}
	public static function getNewVoucherId()
	{
		global $db;
		self::init();
		do{
			$tid=getCode39ID(self::$NumVoucherChars,RND_VOUCHER);
			$res=$db->select("voucher","voucherid","voucherid=".$db->escapeString($tid));
			if(count($res)==0)return $tid;
		}while(true);
	}
	public static function getNewCouponId()
	{
		global $db;
		self::init();
		do{
			$tid=getCode39ID(self::$NumCouponChars,RND_COUPON);
			$res=$db->select("coupon","couponid","couponid=".$db->escapeString($tid));
			if(count($res)==0)return $tid;
		}while(true);
	}
	///checks that the ID is valid as a coupon and could not interfere with tickets or vouchers
	public static function checkCouponIdValid($couponid)
	{
		self::init();
		//DB limits
		if(strlen($couponid)<1 || strlen($couponid)>32)return false;
		//not a ticket or voucher
		if(code39IsInRange($couponid,RND_TICKET,self::$NumTicketChars) || code39IsInRange($couponid,RND_VOUCHER,self::$NumVoucherChars))
			return false;
		//seems to be ok then...
		return true;
	}
	///checks whether a coupon ID exists
	public static function checkCouponIdExists($couponid)
	{
		global $db;
		self::init();
		$res=$db->select("coupon","couponid","couponid=".$db->escapeString($couponid));
		return count($res)!=0;
	}
};

//eof
return;
?>
