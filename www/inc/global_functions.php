<?php
// +----------------------------------------------------------------------
// | PHP Source: some global basic functions
// +----------------------------------------------------------------------
// | Copyright (C) 2008 by Peter Keller <peter@silmor.de>
// |           (c) Konrad Rosenbaum <konrad@silmor.de>, 2010-2017
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

/** checks if a variable is empty, needs to be used to check class methods */
function isEmpty($var)
{
	return empty($var);
}

/** checks if given value is a valid email address 
\returns true if the value has e-mail syntax */
function isEmail($value)
{
	if(defined('FILTER_FLAG_EMAIL_UNICODE'))$opt=FILTER_FLAG_EMAIL_UNICODE;
	else $opt=0;
	return filter_var($value,FILTER_VALIDATE_EMAIL,$opt) != false;
	//old regex: preg_match("/^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+$/", $value)
}

/**redirects the browser to index.php*/
function redirectHome(array $query=array())
{
	global $_SERVER;
	$url=$_SERVER["SCRIPT_NAME"];
	$i=0;
	foreach($query as $k => $v){
		//character signalling next parameter
		if($i)$url.="&";
		else $url.="?";//first one...
		$i++;
		//append
		$url.=urlencode($k)."=".urlencode($v);
	}
	Header("Location: ".$url);
	print("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0; URL=".$url."\" />\n");
	print("<title>Redirect</title>\n</head><body>\n");
	print("If you are not automatically redirected, please click ");
	print("<a href=\"".$url."\">here</a>.\n</body></html>");
	exit();
}

///redirects back to the referer page
function redirectBack()
{
	global $_SERVER;
	if(isset($_SERVER['HTTP_REFERER'])){
		Header("Location: ".$_SERVER['HTTP_REFERER']);
		exit();
	}else{
		redirectHome();
	}
}


//done
return;
?>
