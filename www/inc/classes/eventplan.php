<?php
// (c) Konrad Rosenbaum, 2016-17
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

///This class is used to verify tickets in a new order and to assign seats.
class EventPlan
{
	//reference to the WOEvent
	private $event=null;
	//reference to the corresponding WOSeatPlan (not WOSeatPlanInfo, that is part of $event)
	private $seatplan=null;
	//list of WOTickets sold for this event (including cancelled)
	private $oldtickets=null;
	//list of WOCartTickets added to the event by the current Order
	private $newtickets=null;
	//integer number of tickets right now (only blocking, not cancelled)
	private $numtickets=0;
	//per category ticket numbers
	private $numcattickets;

	///creates a new EventPlan object by the event ID it represents
	///automatically loads the seat plan and all tickets
	public function __construct($eventid)
	{
		$this->newtickets=array();
		$this->oldtickets=array();
		$this->numcattickets=array();
		$et=WTevent::getFromDB($eventid);
		if(!is_a($et,"WTevent"))return;
		$this->event=WOEvent::fromTableevent($et);
		$seatplaninfo=$this->event->getseatplan();
		if(is_a($seatplaninfo,"WOSeatPlanInfo")){
			$this->seatplan=WOSeatPlan::fromString($seatplaninfo->getplan());
		}
		global $db;
		$this->oldtickets=WOTicket::fromTableArrayticket(WTticket::selectFromDB("eventid=".$db->escapeInt($eventid)));

		//initialize plan fields, TODO: use the concept of seat number
		if($this->seatplan!==null)
			$this->seatplan->initGroups($eventid);
		foreach($this->event->getprice() as $prc)
			$this->numcattickets[$prc->getpricecategoryid()]=0;
		foreach($this->oldtickets as $tick){
			if(!$tick->isBlocking())continue;
			$this->numtickets++;
			$this->numcattickets[$tick->getpricecategoryid()]++;
			if($this->seatplan!==null)
				$this->seatplan->addTickets(1,$tick->getpricecategoryid());
		}
	}

	///returns true if the references event exists
	public function eventIsValid()
	{
		return $this->event !== null;
	}

	///returns true if the user can sell for this event in general, ie.:
	/// - it exists
	/// - the user has the right to do it
	/// - does not check whether it is too late to sell
	public function canSellEvent()
	{
		global $session;
		if(!$this->eventIsValid())return false;
		if(!$session->checkFlags($this->event->getflags()))return false;
		return true;
	}

	///checks whether the event has already started at the given timestamp
	public function eventHasStarted($timestamp)
	{
		return $timestamp>=$this->event->getstart();
	}

	///checks whether the event has already ended at the given timestamp
	public function eventHasEnded($timestamp)
	{
		return $timestamp>=$this->event->getend();
	}

	///adds the ticket to the list of new tickets and validates whether the sale is possible;
	///does not validate the sale timing, this is done in the cart using eventHasStarted and eventHasEnded;
	///adjusts the ticket state in case of failure
	public function addTickets($key,&$ticket)
	{
		$this->newtickets[$key]=&$ticket;
		$num=$ticket->getamount();
		$catid=$ticket->getpricecategoryid();
		$ocatid=$ticket->getorigpricecategoryid();
		if(!is_numeric($ocatid))$ocatid=$catid;
		//basic checks
		if($num<=0){
			$ticket->setstatus(WOCartTicket::Invalid);
			$ticket->setstatustext(tr("Cannot sell a negative amount of tickets."));
			return false;
		}
		if(!is_numeric($catid)){
			$ticket->setstatus(WOCartTicket::Invalid);
			$ticket->setstatustext(tr("The category is not numeric."));
			return false;
		}
		if(!$this->canSellEvent()){
			$ticket->setstatus(WOCartTicket::Invalid);
			$ticket->setstatustext(tr("Not a valid event or you do not have access to it."));
			return false;
		}
		//seats available in event
		$avail=$this->event->getcapacity()-$this->numtickets;
		if($num>$avail){
			$ticket->setstatus(WOCartTicket::Exhausted);
			$ticket->setstatustext(tr("Not enough tickets in this event."));
			$ticket->setmaxamount($avail);
			return false;
		}
		//event has both categories, set price
		$havcat=false;$havocat=false;$maxcat=0;$catobj=null;
		foreach($this->event->getprice() as $prc){
			if($catid==$prc->getpricecategoryid()){
				$havcat=true;
				$ticket->setprice($prc->getprice());
				$maxcat=$prc->getmaxavailable();
			}
			if($ocatid==$prc->getpricecategoryid()){
				$havocat=true;
				$catobj=$prc;
			}
		}
		if(!$havcat || !$havocat){
			$ticket->setstatus(WOCartTicket::Invalid);
			$ticket->setstatustext(tr("The category is not part of the event."));
			return false;
		}
		//have access to orig category (new cat is checked later in coupon)
		global $session;
		if(!$session->checkFlags($catobj->getflags())){
			$ticket->setstatus(WOCartTicket::Invalid);
			$ticket->setstatustext(tr("You cannot order in this category."));
                        return false;
		}
		//category has enough seats
		$avail=$maxcat-$this->numcattickets[$catid];
		if($num>$avail){
			$ticket->setstatus(WOCartTicket::Exhausted);
			$ticket->setstatustext(tr("Not enough tickets in this category."));
			$ticket->setmaxamount($avail);
			return false;
		}
		//adjust numbers
		$this->numtickets+=$num;
		$this->numcattickets[$catid]+=$num;
		//plan allows it
		if($this->seatplan!==null){
			if(!$this->seatplan->addTickets($num,$catid)){
				$ticket->setstatus(WOCartTicket::Invalid);
				$ticket->setstatustext(tr("Rejected by seat plan: ").$this->seatplan->lastError());
				return false;
			}else
				return true;
		}else{
			return true;
		}
	}

	///returns the event
	public function getEvent()
	{
		return $this->event;
	}

	///returns the newly added ticket objects (array of WOCartTicket)
	public function getTickets()
	{
		return $this->newtickets;
	}
};

///This class verifies that the rules of a coupon are adhered to.
///Also used to execute those rules on the Cart before ordering.
class CouponVerifier
{
	private $couponid=null;
	private $coupon=null;

	///create a new verifier object
	public function __construct($couponid)
	{
		$this->couponid=$couponid;
		if($couponid!==null)
			$this->coupon=WOCoupon::fromTablecoupon(WTcoupon::getFromDB($couponid));
	}

	///checks that the coupon can be used
	public function isUseableOrNull()
	{
		if($this->coupon===null)return true;
		//check time window
		$now=time();
		if($this->coupon->getvalidfrom()!==null && $this->coupon->getvalidfrom()>$now)return false;
		if($this->coupon->getvalidtill()!==null && $this->coupon->getvalidtill()<$now)return false;
		//check access rights
		global $session;
		if(!$session->checkFlags($this->coupon->getflags()))return false;
		//done, must be ok now
		return true;
	}

	///verify the contents of the event plan
	public function verify($eventplan)
	{
		if(!$this->isUseableOrNull())return false;
		if($this->coupon===null)return $this->verifyNull($eventplan);
		//go through tickets
		$vret=true;
		foreach($eventplan->getTickets() as &$tick){
			$opc=$tick->getorigpricecategoryid();
			$pc=$tick->getpricecategoryid();
			//ignore if there is no move
			if($opc===null || $opc==$pc)continue;
			//find rule that matches the move
			$found=false;
			foreach($this->coupon->getrules() as $rule){
				if($rule->getfromcategory()==$opc && $rule->gettocategory()==$pc){
					$found=true;
					break;
				}
			}
			if(!$found){
				$vret=false;
				$tick->setstatus(WOCartTicket::Invalid);
				$tick->setstatustext(tr("Category conversion is not covered by the coupon."));
			}
		}
		//TODO: implement limits
		//done
		return $vret;
	}

	///verification if the coupon is NULL
	private function verifyNull($eventplan)
	{
		$vret=true;
		//make sure there are no altered categories
		foreach($eventplan->getTickets() as $tick){
			$opc=$tick->getorigpricecategoryid();
			$pc=$tick->getpricecategoryid();
			if($opc!==null && $opc!=$pc){
				$vret=false;
				$tick->setstatus(WOCartTicket::Invalid);
			}
		}
		return $vret;
	}

	///applies the coupon to the WebCart
	//TODO: check limits, check time
	static public function applyToCart($cartid)
	{
		//get cart
		$dcart=WTcart::getFromDB($cartid);
		if(!is_a($dcart,"WTcart"))return;
		//get coupon
		$coupon=WOCoupon::fromTablecoupon(WTcoupon::getFromDB($dcart->couponid));
		if(!is_a($coupon,"WOCoupon"))return;
		//collect
		global $db;
		$tlines=WTcartticket::selectFromDB("cartid=".$db->escapeString($cartid));
		foreach($tlines as $tick){
			//get event
			$event=WOEvent::fromTableevent(WTevent::getFromDB($tick->eventid));
			//get price ID
			if($db->isNull($tick->origpricecategoryid))
				$pid=$tick->pricecategoryid;
			else
				$pid=$tick->origpricecategoryid;
			//check for rule
			$npid=$pid;
			foreach($coupon->getrules() as $rule){
				if($rule->getfromcategory()==$pid){
					$n=$rule->gettocategory();
					foreach($event->getprice() as $prc){
						if($prc->getpricecategoryid()==$n){
							$npid=$n;
							break;
						}
					}
					if($npid!=$pid)break;
				}
			}
			//TODO: this may fail if the user adds more tickets later
			$tick->pricecategoryid=$npid;
			$tick->origpricecategoryid=$pid;
			$tick->update();
		}
	}
};



return;
?>
