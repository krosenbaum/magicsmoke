<?php
// (c) Konrad Rosenbaum, 2007-2017
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

wob_autoclass('LanguageManager','./inc/classes/language_manager.php');
wob_autoclass('ConfigManager','./inc/classes/config_manager.php');
wob_autoclass('BaseVars','./inc/classes/basevars.php');
wob_autoclass('EventPlan','./inc/classes/eventplan.php');
wob_autoclass('CouponVerifier','./inc/classes/eventplan.php');

return;
?>
