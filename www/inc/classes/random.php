<?php
// (c) Konrad Rosenbaum, 2007-2016
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


//try to use /dev/*random
function randseedfile($fn,$sz)
{
	//open /dev/(u)random
	$fd=fopen($fn,"r");
	//don't block - we do not have much time
	stream_set_blocking($fd,0);
	//add entropy
	global $RANDSEED;
	$RANDSEED.=bin2hex(fread($fd,$sz));
	fclose($fd);
}

//get current random seed
$RANDSEED="";
$RANDCTR=0;

//initialize random seed
function randseedinit()
{
	global $RANDSEED,$_SERVER;
	//very basic initialization
	$RANDSEED.=microtime().uniqid("",true);
	//attempt to use OpenSSL
	if (function_exists('openssl_random_pseudo_bytes')) {
		$RANDSEED .= bin2hex(openssl_random_pseudo_bytes(64, $strong));
		// if it was strong, be happy and leave (it probably already used /dev/random)
		if($strong === true) {
			return;
		}
	}
	//attempt to use /dev/(u)random
	if(file_exists("/dev/urandom"))randseedfile("/dev/urandom",64);
	else if(file_exists("/dev/random"))randseedfile("/dev/random",20);
	//last resort: get what we got from apache
	if(isset($_SERVER["UNIQUE_ID"]))$RANDSEED.=$_SERVER["UNIQUE_ID"];
}

/**return $bits bits of random data as hex string*/
function getRandom($bits)
{
	//number of hex digits...
	$bits/=4;
	//init
	global $RANDSEED,$RANDCTR;
	if($RANDSEED=="")randseedinit();
	$ret="";
	//get string
	while(strlen($ret)<$bits){
		$ret.=sha1($RANDSEED.microtime().$RANDCTR);
		$RANDCTR++;
	}
	//return
	$ret=substr($ret,0,$bits);
	return $ret;
}

/**return a salt value for Customer::setPassword */
function getSalt()
{
	return getRandom(16*4);
}

//don't apply de-randomization
define("RND_ANYRANGE",-1);
//bits to keep
define("RND_MASK",7);
//added to a ticket
define("RND_TICKET",0x00);
//added to a voucher
define("RND_VOUCHER",0x08);
//not used yet:
define("RND_COUPON",0x10);
define("RND_OTHER2",0x18);

//Code 39 chars that are used for barcodes
define("RND_CODE39_CHARS","123456789ABCDEFGHJKLMNPQRSTUVWXYZ");

/**return a new Code-39 capable ID; length is the amount of characters*/
function getCode39ID($length,$range=RND_ANYRANGE)
{
	//we use a subset of the Code-39 characters that is moderately un-ambiguous
	$c39=RND_CODE39_CHARS;
	//get plenty of random bits (2 hex chars for each Code-39 char)
	$rnd=getRandom($length*8);
	//construct the code
	$ret="";
	for($i=0;$i<$length;$i++){
		//cut random
		$r=hexdec(substr($rnd,$i*2,2)) & 31;
		//if this is the first char, further manipulate it;
		//this is relied upon by the ticket/voucher/item code to minimize
		//search for existing keys
		if($i==0 && $range!=RND_ANYRANGE){
			$r=($r&RND_MASK)|$range;
		}
		//append char
		$ret.=substr($c39,$r,1);
	}
	return $ret;
}

///returns whether a barcode matches a specific range
function code39IsInRange($code,$range=RND_ANYRANGE,$length=-1)
{
	//check length
	if($length>0){
		if(strlen($code)!=$length)return false;
	}
	//convert first char back to int
	$c=strtoupper(substr($code,0,1));
	$i=strpos(RND_CODE39_CHARS,$c);
	if($i===false)return false;
	//shortcut
	if($range==RND_ANYRANGE)return true;
	//mask and compare
	$i &= ~RND_MASK;
	return $i == $range;
}

//eof
return;
?>
