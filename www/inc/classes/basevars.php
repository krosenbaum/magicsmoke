<?php
// (c) Konrad Rosenbaum, 2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

///helper class to populate template basevars array
class BaseVars{

///if Twig is not yet initialized: initialize it, sets the $twig variable to the interpreter
static public function initTwig(){
	global $twig;
	if(!isset($twig))self::initTwigPriv();
}

/// \internal actually initializes Twig
static private function initTwigPriv()
{
	Twig_Autoloader::register();
	global $loader,$twig,$twigoptions,$twigextensions;
	$loader = new Twig\Loader\FilesystemLoader(LanguageManager::singleton()->templateFolders());
	$twig = new Twig_Environment($loader, $twigoptions );
	foreach($twigextensions as $te){
		$t='Twig_Extension_'.$te;
		$twig->addExtension(new $t());
	}
	$twig->addExtension(new LangFilterExtension);
	$twig->addExtension(new SmokeFilterExtension);
}

/// initializes the $basevars array
public static function init(){
	global $basevars;
	if(!isset($basevars))self::initPriv();
}

/// \internal actually initializes $basevars
public static function initPriv(){
	global $basevars,$_SERVER,$BaseUrl,$BaseUrlDir;
	self::initBaseUrlPriv();
	//basic variables shared by all templates
	// script URLs
	$basevars['script']['root']=$BaseUrl;
	$basevars['script']['this']=$BaseUrl;
	$basevars['script']['basedir']=$BaseUrlDir;
	if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING']!="")
		$basevars['script']['this'].="?".$_SERVER['QUERY_STRING'];
	$basevars['script']['index']=$BaseUrl."?mode=index";
	$basevars['script']['eventDetails']=$BaseUrl."?mode=eventDetails&event=";
	$basevars['script']['eventOrder']=$BaseUrl."?mode=eventOrder";
	$basevars['script']['tickets']=$BaseUrl."?mode=tickets";
	$basevars['script']['vouchers']=$BaseUrl."?mode=vouchers";
	$basevars['script']['voucherOrder']=$BaseUrl."?mode=voucherOrder";
	$basevars['script']['shop']=$BaseUrl."?mode=shop";
	$basevars['script']['cart']=$BaseUrl."?mode=cart";
	$basevars['script']['mycart']=$BaseUrl."?mode=mycart";
	$basevars['script']['checkout']=$BaseUrl."?mode=checkout";
	$basevars['script']['removeItem']=$BaseUrl."?mode=removeItem";
	$basevars['script']['orderLogin']=$BaseUrl."?mode=orderLogin";
	$basevars['script']['placeOrder']=$BaseUrl."?mode=placeOrder";
	$basevars['script']['customerLoginOrder']=$BaseUrl."?mode=customerLoginOrder";
	$basevars['script']['customerResetOrder']=$BaseUrl."?mode=customerResetOrder";
	$basevars['script']['customerRegistrationOrder']=$BaseUrl."?mode=customerRegistrationOrder";
	$basevars['script']['changeInvoiceAddress']=$BaseUrl."?mode=changeInvoiceAddress";
	$basevars['script']['changeDeliveryAddress']=$BaseUrl."?mode=changeDeliveryAddress";
	$basevars['script']['customerResetLogin']=$BaseUrl."?mode=customerResetLogin";
	$basevars['script']['setlanguage']=$BaseUrl."?mode=setlanguage&lang=";
	$basevars['script']['addcoupon']=$BaseUrl."?mode=addcoupon";
	// form elements
	$basevars['inputnames']['amountTickets']="amountTickets";
	$basevars['inputnames']['event']="event";
	$basevars['inputnames']['ticket']="event";
	$basevars['inputnames']['voucher']="voucher";
	$basevars['inputnames']['mode']="mode";
	$basevars['inputnames']['login']['name']='customer_mail';
	$basevars['inputnames']['login']['passwd']='customer_passwd';
	$basevars['inputnames']['login']['passwdrepeat']='customer_passwd2';
	$basevars['inputnames']['login']['lastname']='customer_name';
	$basevars['inputnames']['login']['firstname']='customer_firstname';
	$basevars['inputnames']['login']['title']='customer_title';
	$basevars['inputnames']['address']['setaddress']='setaddr';
	$basevars['inputnames']['address']['createaddress']='createaddr';
	$basevars['inputnames']['address']['name']='addr_name';
	$basevars['inputnames']['address']['addr1']='addr_line1';
	$basevars['inputnames']['address']['addr2']='addr_line2';
	$basevars['inputnames']['address']['city']='addr_city';
	$basevars['inputnames']['address']['zip']='addr_zip';
	$basevars['inputnames']['address']['state']='addr_state';
	$basevars['inputnames']['address']['country']='addr_country';
	$basevars['inputnames']['coupon']='coupon';
	//end of basevars
}

private static function initBaseUrlPriv(){
	global $BaseUrl,$_SERVER,$BaseUrlDir;
	//check whether it is already set
	if(!isset($BaseUrl))$BaseUrl="";
	if($BaseUrl!="")return;
	//reconstruct
	$BaseUrl="http";
	// HTTPS?
	if(isset($_SERVER["HTTPS"])){
		if($_SERVER["HTTPS"]!="" && $_SERVER["HTTPS"]!="off")
			$BaseUrl.="s";
	}
	$BaseUrl.="://";
	// host name
	if(isset($_SERVER["SERVER_NAME"]))$BaseUrl.=$_SERVER["SERVER_NAME"];
	else if(isset($_SERVER["HTTP_HOST"]))$BaseUrl.=$_SERVER["HTTP_HOST"];
	else $BaseUrl="";//darn! no host found
	// add path, correct machine.php to index.php
	$pl=explode("/",$_SERVER["SCRIPT_NAME"]);
	$path="";
	$last=array_pop($pl);
	foreach($pl as $p){
		if($p=="")continue;
		$path.="/".$p;
	}
	$BaseUrlDir=$BaseUrl.$path;
	$pl=explode(".",$last);
	if(count($pl)!=2)$path.="/".$last;
	else $path.="/index.".$pl[1];
	$BaseUrl.=$path;
}

public static function initCartAndUserData()
{
    global $websession,$basevars;
    $basevars['inputnames']['cartid']=WebCart::cartIdName;
    $basevars['cartcookie']=WebCart::cartIdName;
    $basevars['sessionid']=$websession->getsessionid();
    $cust=$websession->getcustomer();
    if($cust!==false){
        $basevars['user']=array(
            'name'=>$cust->getname(),
            'firstname'=>$cust->getfirstname(),
            'title'=>$cust->gettitle(),
            'email'=>$cust->getemail()
        );
    }else{
        $basevars['user']=array(
            'name'=>"",
            'firstname'=>"",
            'title'=>"",
            'email'=>""
        );
    }
}

//end of class
}

//eof
return;
?>
