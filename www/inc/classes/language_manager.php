<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2007-8 by Peter keller <peter@silmor.de>
// |           (c) 2010-15 Konrad Rosenbaum <konrad@silmor.de>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

define("COOKIE_LANGUAGE", "ms_lang");

/** function to replace gettext */
function i18n($key)
{
	$lang = LanguageManager::singleton();
	
	$translation = $lang->getValue($key);
	
	if ($translation != "") {
		return $translation;
	} else {
		return $key;
	}
}

/** replaces each {number} in a string with its equivalent in the array 
{1} => array[0] */
function string_format($string, $array)
{
	$num = count($array);
	
	for ($i=0; $i < $num; $i++) {
		$string = str_replace("{".($i+1)."}", $array[$i], $string);
	}
	
	return $string;
}

class LanguageManager
{
	private static $instance=null;//will be instantiated by singleton()
	private $config;
	private $templateFolder;
	private $formatcfg;
	private $timezone;
	private $lang="C";
	
	/** private constructor */
	private function __construct($xlang=false)
	{	
		global $template;
		
		$this->templateFolder = $template;
		//default fallback for empty setting
		if($this->templateFolder == "")
			$this->templateFolder = "./template/";
		//make sure it ends with /
		if(substr($this->templateFolder,-1,1) != "/")
			$this->templateFolder .= "/";
		//collect possible languages for this user
		$langs=array();
		//resetLanguage call
		if($xlang!==false) $langs[]=$xlang;
		// check if cookie is set
		if (isset($_COOKIE[COOKIE_LANGUAGE])) {
			$langs[]= $_COOKIE[COOKIE_LANGUAGE];
// 			echo "cookie ".$_COOKIE[COOKIE_LANGUAGE];
		}
		if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){
// 			echo "server lang ".$_SERVER["HTTP_ACCEPT_LANGUAGE"];
			foreach(explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]) as $l){
				//the language itself
				$langs[]=trim(strtolower($l));
				//if it has a sub-category, get the parent language
				$l2=explode("-",trim($l));
				if(count($l2)<2)continue;
				$langs[]=trim(strtolower($l2[0]));
			}
		}
		
		//language sieve: check which ones we know
		foreach($langs as $l){
			//sanity check for $lang -> must only contain letters and dashes; fallback is C
			if(preg_match("/^[a-zA-Z-]+$/",$l)<1)continue;
			//check that we have this directory
			if(!file_exists($this->templateFolder . $l))continue;
			if(!is_dir($this->templateFolder . $l))continue;
			//found it!
			$this->lang=$l;
			break;
		}
		//initialize internal formatting
		$this->formatcfg=new WOServerFormat($this->lang);
		$this->timezone=new DateTimeZone($this->formatcfg->gettimezone());
		//make sure config manager is initialized
		$this->config = ConfigManager::initialize($this->templateFolder()."lang.po");
	}
	
	/** returns the instance of the Language Manager */
	public static function singleton()
	{
                global $default_language;
		if(self::$instance===null) {
                        //this may be set in config.php, otherwise use "C"
                        if(!isset($default_language))
                                $default_language="C";
                        //instantiate LM
			self::$instance = new LanguageManager();
		}
		
		return self::$instance;
	}
	
	/** resets the singleton instance to a different language */
	public static function resetLanguage($lang)
	{
		self::$instance = new LanguageManager($lang);
		return self::$instance;
	}
	
	/** returns the configured language template folder*/
	public function templateFolder()
	{
		return $this->templateFolder.$this->lang."/";
	}
	
	/**returns the configured language template folder plus its fallback*/
	public function templateFolders()
	{
                global $default_language;
		$ret=array();
		$ret[]=realpath($this->templateFolder());
		$ret[]=realpath($this->templateFolder.$default_language."/");
		return $ret;
	}
	
	/**returns a date in current language formatted to the given format string*/
	public function formatDate($date,$format)
	{
		$out="";
		//parse
		$time=new DateTime("@".($date+0),$this->timezone);
		$time->setTimeZone($this->timezone);
		$inp=false;
		for($i=0;$i<strlen($format);$i++){
			if($inp){
				//IMPORTANT NOTICE:
				//if you change or extend any of the below please
				//also change the corresponding code in the client
				//class MLocalFormat method formatDateTime
				switch($format[$i]){
					//date format codes
					case 'Y':
					case 'y':
						$out.=$time->format($format[$i]);
						break;
					case 'm':$out.=$time->format('n');break;
					case 'M':$out.=$time->format('m');break;
					case 'n':
						$ma=$this->formatcfg->getshortmonths();
						$out.=$ma[$time->format('n')-1];
						break;
					case 'N':
						$ma=$this->formatcfg->getmonths();
						$out.=$ma[$time->format('n')-1];
						break;
					case 'd':$out.=$time->format('j');break;
					case 'D':$out.=$time->format('d');break;
					case 'w':
						$wd=($time->format('N')+0)%7;
						$wa=$this->formatcfg->getshortweekdays();
						$out.=$wa[$wd];
						break;
					case 'W':
						$wd=($time->format('N')+0)%7;
						$wa=$this->formatcfg->getweekdays();
						$out.=$wa[$wd];
						break;
					//time formats
					case 'h':$out.=$time->format('G');break;
					case 'H':$out.=$time->format('H');break;
					case 'a':$out.=$time->format('g');break;
					case 'A':$out.=$time->format('h');break;
					case 'i':$out.=$time->format('i')+0;break;
					case 'I':$out.=$time->format('i');break;
					case 's':$out.=$time->format('s')+0;break;
					case 'S':$out.=$time->format('s');break;
					case 'z':
						$out.=floor($time->format('u')/1000);
						break;
					case 'Z':
						$ms=floor($time->format('u')/1000)."";
						while(strlen($ms)<3)$ms="0".$ms;
						$out.=$ms;
						break;
					case 'p':
						$t=$time->format('G')+0;
						if($t>=1 && $t<=12)$out.=strtolower($this->formatcfg->getamtext());
						else $out.=strtolower($this->formatcfg->getpmtext());
						break;
					case 'P':
						$t=$time->format('G')+0;
						if($t>=1 && $t<=12)$out.=$this->formatcfg->getamtext();
						else $out.=$this->formatcfg->getpmtext();
						break;
					case 't':
					case 'T':
						//get diff from UTC
						$d=floor($time->getOffset()/60);
						//west(-) or east(+)?
						if($d<0){$t="-";$d=-$d;}else $t="+";
						//hours
						$s=floor($d/60)."";
						if(strlen($s)<2)$s="0".$s;
						$t.=$s;
						//T or t? make a colon?
						if($format[$i]=='t')$t.=":";
						//minutes
						$s=($d%60)."";
						if(strlen($s)<2)$s="0".$s;
						$t.=$s;
						//append
						$out.=$t;
						break;
					case 'o':$out.=$time->format('T');break;
					case 'O':$out.=$this->formatcfg->gettimezone();break;
					// % sign
					case '%':$out.="%";break;
					//mistakes
					default:$out.="%".$format[$i];break;
				}
				$inp=false;
			}else{
				if($format[$i]=='%')$inp=true;
				else $out.=$format[$i];
			}
		}
		//catch mistaken % at end
		if($inp)$out.="%";
		//return result
		return $out;
	}
	
	/** returns date in current language, default: ISO-date */
	public function getDate($date)
	{
		return $this->formatDate($date,$this->formatcfg->getdateformat());
	}
	
	/** returns date in current language, default: ISO-date */
	public function getDateTime($date)
	{
		return $this->formatDate($date,$this->formatcfg->getdatetimeformat());
	}
	
	/** returns time in current language */
	public function getTime($time)
	{
		return $this->formatDate($time,$this->formatcfg->gettimeformat());
	}
	
	///show the thoursand separator in numbers and money
	const UseThousandSeparator=1;
	///format an integer number
	public function formatIntNumber($num,$flags=0)
	{
		$ts=$this->formatcfg->getthousandseparator();
		$td=$this->formatcfg->getthousanddigits();
		$num=round($num,0);
		//any reason to abbreviate this?
		if($td===null || $td<=0 || $ts==null || $ts=="")return $num."";
		if(($flags&self::UseThousandSeparator)==0)return $num."";
		//start formatting
		if($num<0){$isneg=true;$num*=-1;}else $isneg=false;
		$s=$num."";
		//insert dividers
		$r="";
		$l=strlen($s);
		for($i=0;$i<$l;$i++){
			if($i>0 && (($l-$i)%$td)==0)$r.=$ts;
			$r.=$s[$i];
		}
		if($isneg)$r="-"+$r;
		return $r;
	}
	///format a float number
	public function formatFloatNumber($num,$digits=2,$flags=0)
	{
		if($num<0){$isneg=true;$num*=-1;}else $isneg=false;
		//format int part
		$num=round($num,$digits);
		$num1=floor($num);
		$r=$this->formatIntNumber($num1,$flags);
		//add sign
		if($isneg)$r="-".$r;
		if($digits<=0)return $r;
		//add fraction
		$num-=$num1;
		for($i=0;$i<$digits;$i++)$num*=10.;
		$r.=$this->formatcfg->getdecimaldot();
		$frac="".round($num,0);
		while(strlen($frac)<$digits)$frac="0".$frac;
		$r.=$frac;
		return $r;
	}
	
	///show the currency symbol
	const MoneyShowCurrency=2;
	///mask for currency symbol choice
	const MoneyCurrencyMask=0xf0;
	///show the HTML version of the currency symbol
	const MoneyCurrencyHTML=0;
	///show the plain ASCII version of the currency symbol
	const MoneyCurrencyPlain=0x10;
	///show the clients (Unicode) version of the currency symbol
	const MoneyCurrencyClient=0x20;
	
	/** returns price in current language */
	public function getPrice($price,$flags=3)
	{
		$digits=$this->formatcfg->getmoneydecimals();
		//adjust number
		if($price<0){$isnet=true;$price*=-1;}else $isneg=false;
		for($i=0;$i<$digits;$i++)$price/=10.;
		//create number part
		$np=$this->formatFloatNumber($price,$digits,$flags);
		//get currency
		if($flags&self::MoneyShowCurrency){
			switch($flags&self::MoneyCurrencyMask){
				case self::MoneyCurrencyPlain:
					$curr=$this->formatcfg->getcurrencysymbolplain();
					break;
				case self::MoneyCurrencyClient:
					$curr=$this->formatcfg->getcurrencysymbol();
					break;
				default:
					$curr=$this->formatcfg->getcurrencysymbolhtml();
					break;
			}
		}else $curr="";
		//add sign
		if($isneg){
			$sign=$this->formatcfg->getmoneynegative();
			$pos=$this->formatcfg->getmoneynegativepos();
		}else{
			$sign=$this->formatcfg->getmoneypositive();
			$pos=$this->formatcfg->getmoneypositivepos();
		}
		switch($pos){
			case WOServerFormat::NoSign:break;
			case WOServerFormat::SignAfterNum:$np.=$sign;break;
			case WOServerFormat::SignBeforeSym:$curr=$sign.$curr;break;
			case WOServerFormat::SignAfterSym:$curr.=$sign;break;
			case WOServerFormat::SignParen:$np="(".$np.")";break;
			default://fallback and before number
				$np=$sign.$np;
				break;
		}
		//connect and return
		if($curr!=""){
			if($this->formatcfg->getcurrencysymbolpos())return $np." ".$curr;
			else return $curr." ".$np;
		}
		return $np;
	}
	
	/** returns value for specified key in current language */
	public function getValue($key)
	{
		return $this->config->get($key);
	}
	
	/** returns all supported languages */
	static public function getLanguages()
	{
		global $template;
		$d=dir($template);
		$r=array();
		while(false !== ($e=$d->read())){
			if(!is_dir($template."/".$e))continue;
			if(preg_match('/^[a-z]+$/',$e)>0 && $e!="C")
				$r[]=$e;
		}
		return $r;
	}
	
	/** checks parameters, sets a language cookie returns home*/
	static public function setLanguage()
	{
		global $HTTPARGS;
		if(!isset($HTTPARGS['lang']))redirectHome();
		$l=$HTTPARGS['lang'];
		if(!in_array($l,self::getLanguages()))redirectHome();
		//set the cookie for ~10 years
		setCookie(COOKIE_LANGUAGE,$l,time()+316224000);
		redirectHome();
	}
}

//make sure it exists
LanguageManager::singleton();

//eof
return;
?>
