<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2007 by Peter keller <peter@silmor.de>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

class ConfigManager
{
	private $configFile;
	private $config;
	private static $instance;
	
	private function __construct($file)
	{
		$this->configFile = $file;
		$this->config = array();
		$this->readConfig();
	}
	
	/**used by language manager to initialize this*/
	public static function initialize($file)
	{
		if(!self::$instance) {
			self::$instance = new ConfigManager($file);
		}
		return self::$instance;
	}
	
	/** returns the instance of the Config Manager */
	public static function singleton()
	{
		return self::$instance;
	}
	
	/** reads the configuration values from the file */
	private function readConfig()
	{
		// check if file really exists
		if (file_exists($this->configFile)) {
			$lines = file($this->configFile);
			$key = "";
			foreach ($lines as $line_num => $line) {
				if ( preg_match("/^msgid.*\"(.*)\"/", $line, $reg) ) {
					$key = $reg[1];
				}
				if ( preg_match("/^msgstr.*\"(.*)\"/", $line, $reg) ) {
					$value = $reg[1];
					$this->config[$key] = $value;
				}
			}
		}
	}
	
	/** returns the value of the given configuration item */
	public function get($key)
	{
		if ($this->hasKey($key)) {
			return $this->config[$key];
		} else {
			return "";
		}
	}

	/** checks if key exists */
	public function hasKey($key)
	{
		return array_key_exists($key, $this->config);
	}
	
	/** can be used to set an alternate path to a config file */
	public function setConfigFile($file)
	{
		$this->configFile = $file;
		$this->readConfig();
	}
}

//eof
return;
?>
