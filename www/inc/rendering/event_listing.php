<?php
// (c) Konrad Rosenbaum, 2007-2011
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/** wrapper arount event (list) rendering for the web UI*/
class EventRender {

/** creates an list of events */
public static function createEventList()
{
	global $twig,$basevars,$session;

	//pass 1: get layout of single event
	$p=$twig->loadTemplate("tickets.html");
	$list=$basevars;
	$list['events']=array();
	$trn=WTrGetAllEvents::execute();
	$events = $trn->resultevents();
	$now=time();
	foreach ($events as $event){
		//only show current events
		if($event->getstart()<=$now)continue;
		//only show those available via web
		if(!$session->checkFlags($event->getflags()))continue;
		//don't show cancelled events
		if($event->getiscancelled())continue;
		//encode as array
		$list['events'][]=$event;
	}
	//pass 2: create page
	return $p->render($list);
}

/** creates the details of an event */
public static function createEventDetails()
{
	global $twig,$session,$basevars;

	if (isset($_GET["event"])) {
		$eventID = $_GET["event"]+0;
	}

	$p = $twig->loadTemplate("eventdetails.html");

	// check if event exists
	$trn=WTrGetEvent::execute($eventID);
	$event = $trn->resultevent();
	if(!is_a($event,"WoEvent"))
		throw new Exception("Unknown Event");

	// set event details
	$list=$basevars;
	$list['event']=$event;

	// create page
	return $p->render($list);
}

//end of class
};


//eof
return;
?>
