<?php
// (c) Konrad Rosenbaum, 2007-2016
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class LangFilterExtension extends Twig_Extension
{
	private $lang;
	
	public function __construct()
	{
		$this->lang=LanguageManager::singleton();
	}
	
	public function getFilters()
	{
		$safe=array('is_safe'=>array('html'));
		return array(
			'asMoney' => new Twig_Filter_Method($this, 'getPrice', $safe),
			'asMoneyPlain' => new Twig_Filter_Method($this, 'getPricePlain', $safe),
			'asMoneyHtml' => new Twig_Filter_Method($this, 'getPriceHtml', $safe),
			'asMoneyClient' => new Twig_Filter_Method($this, 'getPriceClient', $safe),
			'asDate'  => new Twig_Filter_Method($this, 'getDate'),
			'asTime'  => new Twig_Filter_Method($this, 'getTime'),
			'asDateTime'  => new Twig_Filter_Method($this, 'getDateTime'),
			'formatDate' => new Twig_Filter_Method($this, 'formatDate'),
		);
	}
	
	public function getPrice($i){return $this->lang->getPrice($i);}
	public function getPricePlain($i)
	{
		$flags=LanguageManager::UseThousandSeparator | LanguageManager::MoneyShowCurrency | LanguageManager::MoneyCurrencyPlain;
		return $this->lang->getPrice($i,$flags);
	}
	public function getPriceHtml($i)
	{
		$flags=LanguageManager::UseThousandSeparator | LanguageManager::MoneyShowCurrency | LanguageManager::MoneyCurrencyHTML;
		return $this->lang->getPrice($i,$flags);
	}
	public function getPriceClient($i)
	{
		$flags=LanguageManager::UseThousandSeparator | LanguageManager::MoneyShowCurrency | LanguageManager::MoneyCurrencyClient;
		return $this->lang->getPrice($i,$flags);
	}
	public function getDate($i){return $this->lang->getDate($i);}
	public function getTime($i){return $this->lang->getTime($i);}
	public function getDateTime($i){return $this->lang->getDateTime($i);}
	
	public function formatDate($i,$f){return $this->lang->formatDate($i,$f);}
	
	public function getName()
	{
		return 'LangFilter';
	}
}

class SmokeFilterExtension extends Twig_Extension
{
	public function __construct()
	{
		$this->lang=LanguageManager::singleton();
	}

	public function getFilters()
	{
		return array(
			'isObject' => new Twig_Filter_Method($this, 'isObjectTest'),
			'isFalse'  => new Twig_Filter_Method($this, 'isFalse'),
			'isNull'  => new Twig_Filter_Method($this, 'isFalse'),
			'getClass' => new Twig_Filter_Method($this, 'getClass'),
			'asInt'  => new Twig_Filter_Method($this, 'asInt'),
		);
	}
	
	public function isObjectTest($i)
	{
		if(!is_object($i))return false;
		return is_subclass_of($i,"WObject");
	}
	public function isFalse($i){return $i===false || $i === null;}
	public function getClass($o)
	{
		if(is_object($o))
			return get_class($o);
		else
			return "basic:".gettype($o);
	}
	public function asInt($v){return $v+0;}
	
	public function getName()
	{
		return 'SmokeFilter';
	}
}


//eof
return;
?>
