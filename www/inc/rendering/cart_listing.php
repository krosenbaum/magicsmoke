<?php
// (c) Konrad Rosenbaum, 2007-2011
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/** helper class wrapping Web Cart handling logic,
the class wrapping the tables is WOWebCart in wext/webcart.php */
class WebCart{
static private $cartid=false;

/**contains the name of the cookie and the HTTP parameter for the cart ID*/
const cartIdName = "smoke_cartid";

/**contains the name for the web UI field that is supposed to contain the amount of tickets*/
const TicketAmountField = "amountTickets";

/**called from index.php - add tickets to cart*/
static public function addTickets()
{
	global $HTTPARGS;
	//get the cart
	$cartid=self::getOrCreateCart();
	//find event
	if(!isset($HTTPARGS[self::TicketAmountField]))
		redirectHome();
	//find price categories
	$pcs=$HTTPARGS[self::TicketAmountField];
	if(!is_array($pcs) || count($pcs)==0)
		redirectHome();
	//go through them
	foreach($pcs as $pcid => $amount){
		//extract event ID from pcid
		$evid=WOEventPrice::getEventIdFromAmountIndex($pcid);
		//convert pcid to int
		$pcid=WOEventPrice::getCategoryIdFromAmountIndex($pcid);
		//convert amount to int
		$amount=$amount+0;
		//actually add
		WTrWebCartAddTicket::execute($cartid,$evid,$pcid,$amount);
	}
	//go to the cart
	redirectHome(array("mode"=>"cart",self::cartIdName=>$cartid));
}

///called from index.php - adds a voucher to the cart
static public function addVoucher()
{
	global $HTTPARGS,$basevars;
	//get the cart
	$cartid=self::getOrCreateCart();
	//find event
// 	echo "checking";
	$vi=$basevars['inputnames']['voucher'];
	if(!isset($HTTPARGS[$vi])){
		redirectHome();
		return;
	}
	//price
	$prc=$HTTPARGS[$vi];
	//add
// 	echo "adding";
	WTrWebCartAddVoucher::execute($cartid,$prc);
	//go to the cart
	redirectHome(array("mode"=>"cart",self::cartIdName=>$cartid));
}

///called from index.php - adds a coupon to the cart
static public function addCoupon()
{
	global $HTTPARGS,$basevars;
	//get the cart
	$cartid=self::getOrCreateCart();
	//find event
// 	echo "checking";
	$vi=$basevars['inputnames']['coupon'];
	if(!isset($HTTPARGS[$vi])){
		$basevars['invalidcoupon']='none';
		return;
	}
	//price
	$cpid=strtoupper(trim($HTTPARGS[$vi]));
	//add
// 	echo "adding";
	try{
		WTrWebCartAddCoupon::execute($cartid,$cpid);
	}catch(WobTransactionError $e){
		$basevars['invalidcoupon']=$cpid;
	}
	//rendering is called after this...
}

/**called from index.php - removes tickets/vouchers from cart*/
static public function removeItem()
{
	global $HTTPARGS,$basevars;
	$cartid=self::getOrCreateCart();
	//find event
	$ti=$basevars["inputnames"]["ticket"];
	if(isset($HTTPARGS[$ti])){
		$evid=WOEventPrice::getEventIdFromAmountIndex($HTTPARGS[$ti]);
		$pcid=WOEventPrice::getCategoryIdFromAmountIndex($HTTPARGS[$ti]);
		//remove
		WTrWebCartRemoveTicket::execute($cartid,$evid,$pcid);
	}
	$vi=$basevars['inputnames']['voucher'];
	if(isset($HTTPARGS[$vi])){
		$vid=$HTTPARGS[$vi]+0;
		//find and remove voucher
		WTrWebCartRemoveVoucher::execute($cartid,$vid);
	}
	redirectHome(array("mode"=>"cart",self::cartIdName=>$cartid));
}

///clean up old carts
static protected function cleanupDb()
{
	global $db;
	$res=$db->select("cart","cartid","timeout < ".time());
	foreach($res as $row){
		$where="cartid = ".$db->escapeString($row['cartid']);
		$db->deleteRows("cartticket",$where);
		$db->deleteRows("cartvoucher",$where);
		$db->deleteRows("cartitem",$where);
		$db->deleteRows("cart",$where);
	}
}

/**returns the current cart ID, or an empty string if there is no cart, automatically updates its timeout*/
static public function getCart(){
	//DB cleanup
	self::cleanupDb();
	//actually look for cart
	global $CartTimeout;
	$c=self::findCart();
	if($c!=""){
		$crt=WTcart::getFromDB($c);
		$crt->timeout=time()+$CartTimeout;
		$crt->update();
	}
	return $c;
}

/**returns the current cart ID, or an empty string if there is no cart, does not update*/
static protected function findCart(){
	global $HTTPARGS,$_COOKIE;
	if(self::$cartid!==false){
            $res=WTcart::getFromDB(self::$cartid);
            if(is_a($res,"WTcart"))
                return self::$cartid;
            else
                self::$cartid=false;
        }
	//search GET/POST parms
	if(isset($HTTPARGS[self::cartIdName])){
		//found it, test it!
		$ci=trim($HTTPARGS[self::cartIdName]);
		if($ci!=""){
			$res=WTcart::getFromDB($ci);
			if(is_a($res,"WTcart")){
				self::$cartid=$ci;
				return $ci;
			}
		}
	}
	//search cookies
	if(isset($_COOKIE[self::cartIdName])){
		//found it, test it!
		$ci=trim($_COOKIE[self::cartIdName]);
		if($ci!=""){
			$res=WTcart::getFromDB($ci);
			if(is_a($res,"WTcart")){
				self::$cartid=$ci;
				return $ci;
			}
		}
	}
	//none found
	self::$cartid="";
	return "";
}

/**return existing cart or create a new one*/
static public function getOrCreateCart(){
	global $CartTimeout,$WebDefaultShipping;
	//try to find it, if found update it
	self::getCart();
	//found?
	if(self::$cartid==""){
		//none there, create it
		$c=WTcart::newRow();
		$exp=time()+$CartTimeout;
		$c->timeout=$exp;
		if($WebDefaultShipping>=0)
			$c->shippingtype=$WebDefaultShipping;
		$c->insert();
		//set cookies
		setCookie(self::cartIdName,$c->cartid,0);
		//remember
		self::$cartid=$c->cartid;
	}
	//return
	return self::$cartid;
}

/** \internal called to generate a new cart ID, used by WTcart to generate the primary key*/
static public function getNewCartId(){
	do{
		//generate ID
		$ci=getCode39ID(32);
		//look for duplicate
		$res=WTcart::getFromDB($ci);
		if(is_a($res,"WTcart"))continue;
		//return ID
		return $ci;
	}while(true);
}


/** creates the cart overview 

for templating info see \ref tpl_cart Cart Variables
and \ref tpl_base Base Variables
*/
static public function createCartOverview()
{
	global $twig,$basevars,$WebForceShipping;
	
	//get cart id and check it
	$cartid=self::getCart();
	if($cartid==""){
		$p=$twig->loadTemplate("carterror.html");
		return $p->render($basevars);
	}

	//apply coupon
	CouponVerifier::applyToCart($cartid);
	
	//cart is ok, now get the object
	$cart = WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
	if(!is_a($cart,"WOWebCart")){
		//ooops. internal problem
		$p=$twig->loadTemplate("carterror.html");
		return $p->render($basevars);
	}
	$list=$basevars;
	$list["cart"]=$cart;
	
	//additional info: available shipping types
	$list["shipping"]=array();
	$list["forceshipping"]=$WebForceShipping;
	$sl=WOShipping::fromTableArrayshipping(WTshipping::selectFromDB());
	foreach($sl as $s)
		if($s->getcanuse())
			$list["shipping"][]=$s;

	//display
	$p = $twig->loadTemplate("cart.html");
	return $p->render($list);
}

/** creates an overview of known good voucher types

for templating info see \ref tpl_voucher Voucher Variables
and \ref tpl_base Base Variables
*/
static public function createVoucherOverview()
{
	global $twig,$basevars;
	//get voucher values
	$list=$basevars;
	$list['voucherprices']=WTrGetValidVoucherPrices::execute()->resultprices();
	//display
	$p=$twig->loadTemplate("vouchers.html");
	return $p->render($list);
}

/**renders the checkout page

see the \ref tpl_cout Checkout Template
page for details */
static public function checkout()
{
	global $twig,$basevars,$db;
	//populate list
	$list=$basevars;
	//get cart id and check it
	$cartid=self::getCart();
	if($cartid==""){
		$p=$twig->loadTemplate("carterror.html");
		return $p->render($basevars);
	}
	
	//cart is ok, now get the object
	$cart = WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
	if(!is_a($cart,"WOWebCart")){
		//ooops. internal problem
		$p=$twig->loadTemplate("carterror.html");
		return $p->render($basevars);
	}
	//if empty: abort
	if($cart->getisempty()){
		redirectHome(array("mode"=>"cart"));
		return;
	}
	//add cart object
	$list["cart"]=$cart;
	//render
	return $twig->loadTemplate("checkout.html")->render($list);
}

static public function changeAddressPage($mode)
{
	global $twig,$HTTPARGS,$db,$basevars;
	//get customer
	$cartid=self::getCart();
	if($cartid==""){
		redirectHome();
		return;
	}
	$ct=WTcart::getFromDB($cartid);
	$cart=WOWebCart::fromTablecart($ct);
	$cust=$cart->getcustomer();
	if(!is_a($cust,"WOCustomer")){
		//no customer: return to cart, something is very wrong
		redirectHome(array("mode"=>"cart"));
		return;
	}
	//check whether we have arguments for setting
	$setaddr=$basevars['inputnames']['address']['setaddress'];
	if(isset($HTTPARGS[$setaddr])){
		$at=WTaddress::getFromDB($HTTPARGS['setaddr']);
		if(!is_a($at,"WTaddress")){
			redirectHome();
			return;
		}
		//check it is part of the customer
		$found=false;
		foreach($cust->getaddresses() as $adr){
			if($adr->getaddressid() == $at->addressid){
				$found=true;
				break;
			}
		}
		if(!$found){
			redirectHome();
			return;
		}
		//set it
		if($mode=='changeInvoiceAddress'){
			$ct->invoiceaddress=$at->addressid;
		}else{
			$ct->deliveryaddress=$at->addressid;
		}
		$ct->update();
		redirectHome(array("mode"=>"checkout"));
		return;
	}
	//check whether we have arguments for creating
	$creat=$basevars['inputnames']['address']['createaddress'];
	if(isset($HTTPARGS[$creat])){
		//create address
		$at=WTaddress::newrow();
		$at->customerid=$cust->getid();
		$at->name=getHttpArg($basevars['inputnames']['address']['name']);
		$at->addr1=getHttpArg($basevars['inputnames']['address']['addr1']);
		$at->addr2=getHttpArg($basevars['inputnames']['address']['addr2']);
		$at->city=getHttpArg($basevars['inputnames']['address']['city']);
		$at->zipcode=getHttpArg($basevars['inputnames']['address']['zip']);
		$at->state=getHttpArg($basevars['inputnames']['address']['state']);
		$at->countryid=getHttpArg($basevars['inputnames']['address']['country']);
		$at->insert();
		//set it
		if($mode=='changeInvoiceAddress'){
			$ct->invoiceaddress=$at->addressid;
		}else{
			$ct->deliveryaddress=$at->addressid;
		}
		$ct->update();
		redirectHome(array("mode"=>"checkout"));
		return;
	}
	//render addresses
	$list=$basevars;
	$list['customer']=$cust;
	$list['cart']=$cart;
	$list['countries']=WOCountry::fromTableArraycountry(WTcountry::selectFromDB("","ORDER BY countryname"));
	return $twig->loadTemplate('changeaddress.html')->render($list);
}

static public function placeOrder()
{
	global $basevars,$twig,$db;
	//get cart object
	$cartid=self::getCart();
	if($cartid==""){
		return $twig->loadTemplate("carterror.html")->render($basevars);
	}
	$cart=WOCartOrder::fromTablecart(WTcart::getFromDB($cartid));
	if(!is_a($cart,"WOCartOrder")){
		return $twig->loadTemplate("carterror.html")->render($basevars);
	}
	//get the web-object
    $wcart = WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
    if(!is_a($wcart,"WOWebCart")){
        //ooops. internal problem
        $p=$twig->loadTemplate("carterror.html");
        return $p->render($basevars);
    }
	//push it to order; TODO: add voucher feature (arg #3)
	$trans=WTrCreateOrder::execute($cart,false,array());
	//delete cart
	if($trans->resultorder()!==false){
        $where="cartid = ".$db->escapeString($cartid);
        $db->deleteRows("cartticket",$where);
        $db->deleteRows("cartvoucher",$where);
        $db->deleteRows("cartitem",$where);
        $db->deleteRows("cart",$where);
    }
	//go home
	$basevars['incart']=$wcart;
    $basevars['order']=$trans->resultorder();
    $basevars['outcart']=$trans->resultcart();
	return $twig->loadTemplate("placeorder.html")->render($basevars);
}

//end of WebCart
};


//eof
return;
?>
