<?php
// (c) Konrad Rosenbaum, 2007-2011
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/**helper class that compiles templates and caches them*/
class TemplateCompiler
{
	/**public interface for the template compiler, called from index.php?mode=compile */
	static public function execute()
	{
		self::checkAdmin();
		//go through directories
		global $template;
		$tpl=$template;
		$d=dir($template);
		while(false !== ($entry = $d->read())) {
			if($entry == "." || $entry == ".." || $entry == ".svn")continue;
			if(!is_dir($template."/".$entry))continue;
			echo "<p>entering directory ".$entry."<br>\n";
			self::compileDir($tpl,$entry);
		}
		//done
		echo "<p>Done.<p>";
		exit;
	}

	/**compiles all template files of one specific directory*/
	static private function compileDir($template,$lng)
	{
		global $twig,$basevars;
		//reset the language manager to match the current directories' language
		$lang=LanguageManager::resetLanguage($lng);
		//go through all files
		$d=dir($template."/".$lng);
		$loader=new Twig_Loader_Filesystem($lang->templateFolders());
		$twig->setLoader($loader);
		while(false !== ($entry = $d->read())) {
			//ignore anything that starts with a dot
			if($entry[0] == ".")continue;
			//only handle normal files
			if(!is_file($template."/".$lng."/".$entry))continue;
			//actually load and (implicitly) compile it
			echo " &nbsp; compiling ".$entry."... ";
			$twig->loadTemplate($entry)->render($basevars);
			echo "<br>\n";
		}
	}
	
	/**check whether the user is admin*/
	static private function checkAdmin()
	{
		global $db;
		if(!$db->canAdministrate())
			die("The Administratpr account is turned off. Sorry. Cannot compile templates for you.");
		if(!$db->checkAdmin()){
			header("HTTP/1.0 401 Unauthorized");
			header("WWW-Authenticate: Basic realm=\"Smoke Admin Login\"");
			echo "Need to login in order to administrate.";
			exit;
		}else
			echo "Compiling all templates I can find...<p>\n";
	}
}

//eof
return;
?>
