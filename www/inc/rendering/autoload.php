<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

$d=dirname(__FILE__);
wob_autoclass("EventRender",$d.'/event_listing.php');
wob_autoclass("WebCart",$d.'/cart_listing.php');
wob_autoclass("TemplateCompiler",$d.'/tcompiler.php');

wob_autoclass("LangFilterExtension",$d.'/twig_extensions.php');
wob_autoclass("SmokeFilterExtension",$d.'/twig_extensions.php');


//eof
return;
?>
