<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/**shortened info about orders*/
class WOOrderInfo extends WOOrderInfoAbstract
{
	/**mapping helper*/
	protected function getTotalPriceFromDB()
	{
		global $db;
		//if order is closed, return 0
		if($this->prop_status==self::Closed)
			return 0;
		//get shipping price
		$prc=$this->prop_shippingcosts;
		//if this order is cancelled the items are not counted
		if($this->prop_status==self::Cancelled)
			return $prc;
		//get tickets
		$itms=WOTicket::fromTableArrayticket(WTticket::selectFromDB("orderid=".$db->escapeInt($this->prop_orderid)));
		foreach($itms as $it)
			$prc+=$it->amountToPay();
		//get vouchers
		$itms=WOVoucher::fromTableArrayvoucher(WTvoucher::selectFromDB("orderid=".$db->escapeInt($this->prop_orderid)));
		foreach($itms as $it)
			$prc+=$it->getprice();
		//get others
		$itms=WTitem::selectFromDB("orderid=".$db->escapeInt($this->prop_orderid));
		foreach($itms as $it)
			$prc+=$it->totalprice;
		
		//return result
		return $prc;
	}
	/**mapping helper*/
	protected function getAmountTicketsFromDB()
	{
		global $db;
		$res=$db->select("ticket","count(*)as count","orderid=".$db->escapeInt($this->prop_orderid));
		return $res[0]["count"];
	}
	/**mapping helper*/
	protected function getAmountVouchersFromDB()
	{
		global $db;
		$res=$db->select("voucher","count(*)as count","orderid=".$db->escapeInt($this->prop_orderid));
		return $res[0]["count"];
	}
	/**mapping helper*/
	protected function getAmountItemsFromDB()
	{
		global $db;
		$res=$db->select("item","count(*)as count","orderid=".$db->escapeInt($this->prop_orderid));
		return $res[0]["count"];
	}
	
	/**called from GetOrderList transaction*/
	static public function getAllOrders($trans)
	{
		global $db;
		$old=$trans->getoldest();
		if($old===false)$old=0;
		$olst=self::fromTableArrayorder(WTorder::selectFromDB("ordertime>=".$db->escapeInt($old),"ORDER BY orderid"));
		$trans->setorders($olst);
		$trans->setcustomers(self::getCustomerListByOrders($olst));
	}
	
	/**called from GetOrdersByEvents transaction*/
	static public function getOrdersByEvents($trans)
	{
		global $db;
		$old=$trans->getoldest();
		if($old===false)$old=0;
		//get list of valid orderids
		$res=$db->select("ticket","orderid","eventid in ".$db->escapeIntList($trans->getevents()),"GROUP BY orderid ORDER BY orderid");
		$oidl=array();
		foreach($res as $r)
			if(!in_array($r["orderid"],$oidl))$oidl[]=$r["orderid"];
		//get orders
		$olst=self::fromTableArrayorder(WTorder::selectFromDB("ordertime>=".$db->escapeInt($old)." AND orderid in ".$db->escapeIntList($oidl),"ORDER BY orderid"));
		$trans->setorders($olst);
		$trans->setcustomers(self::getCustomerListByOrders($olst));
	}
	
	/**called from GetOrdersByCustomer transaction*/
	static public function getOrdersByCustomer($trans)
	{
		$cid=$trans->getcustomerid();
		$old=$trans->getoldest();
		if($old===false)$old=0;
		global $db;
		$trans->setorders(self::fromTableArrayorder(WTorder::selectFromDB("customerid=".$db->escapeInt($cid)." AND ordertime>=".$db->escapeInt($old),"ORDER BY orderid")));
	}

	/**called from GetOrdersByUser and getMyOrders transaction*/
	static public function getOrdersByUser($trans,$meonly)
	{
		global $session;
		if($meonly)
			$uid=$session->currentUserName();
		else
			$uid=$trans->getusername();
		$old=$trans->getoldest();
		if($old===false)$old=0;
		$incall=$trans->getincludeall();
		//get primary list: orders of this user
		global $db;
		$prim=WTorder::selectFromDB("soldby=".$db->escapeString($uid)." AND ordertime>=".$db->escapeInt($old));
		//get secondary list: orders the user touched
		if($incall){
			$seca=WTorder_audit::selectFromDB("audituname=".$db->escapeString($uid)." AND audittime>=".$db->escapeInt($old));
			$rs=array();
			foreach($seca as $oa)
				if(!in_array($oa->orderid,$rs))
					$rs[]=$oa->orderid;
			$sec=WTorder::selectFromDB("orderid IN ".$db->escapeIntList($rs));
		}else $sec=array();
		//merge and sort
		$res=array();
		$cst=array();
		foreach($prim as $o){
			$res[$o->orderid]=$o;
			$cst[]=$o->customerid;
		}
		foreach($sec as $o){
			$res[$o->orderid]=$o;
			$cst[]=$o->customerid;
		}
		ksort($res);
		//construct result
		$trans->setorders(self::fromTableArrayorder(array_values($res)));
		$cust=WTcustomer::selectFromDB("customerid IN ".$db->escapeIntList(array_unique($cst,SORT_NUMERIC)));
		$trans->setcustomers(WOCustomerInfo::fromTableArraycustomer($cust));
	}

	/**called from GetOrdersByUser and getMyOrders transaction*/
	static public function getOrdersByCoupon($trans)
	{
		global $session;
		$cid=$trans->getcouponid();
		$old=$trans->getoldest();
		if($old===false)$old=0;
		//get primary list: orders of this user
		global $db;
		$res=WTorder::selectFromDB("couponid=".$db->escapeString($cid)." AND ordertime>=".$db->escapeInt($old));
		//get list of customerids
		$cst=array();
		foreach($res as $ord)
			$cst[]=$ord->customerid;
		//construct result
		$trans->setorders(self::fromTableArrayorder(array_values($res)));
		$cust=WTcustomer::selectFromDB("customerid IN ".$db->escapeIntList(array_unique($cst,SORT_NUMERIC)));
		$trans->setcustomers(WOCustomerInfo::fromTableArraycustomer($cust));
	}

	/**helper function for several transactions: selects customers by their IDs references by the given order info objects*/
	static protected function getCustomerListByOrders(array $olist)
	{
		global $db;
		//filter olist
		$cl=array();
		foreach($olist as $o){
			$id=$o->prop_customerid;
			if(!in_array($id,$cl))
				$cl[]=$id;
		}
		//request data
		$q="";
		if(count($cl)>0)$q="customerid in ".$db->escapeIntList($cl);
		return WOCustomerInfo::fromTableArraycustomer(WTcustomer::selectFromDB($q));
	}
};



/**full order class*/
class WOOrder extends WOOrderAbstract
{
	/**analyses itself and returns price*/
	public function getTotalPrice()
	{
		//closed does not have a price
		if($this->prop_status==self::Closed)
			return 0;
		$prc=$this->prop_shippingcosts;
		//cancelled pais only shipping
		if($this->prop_status==self::Cancelled)
			return $prc;
		//add tickets
		foreach($this->prop_tickets as $it)
			$prc+=$it->amountToPay();
		//add vouchers
		foreach($this->prop_vouchers as $it)
			$prc+=$it->getprice();
		//add items
		foreach($this->prop_items as $it)
			$prc+=$it->gettotalprice();
		//return
		return $prc;
	}

	///returns true if the order is fully paid
	public function isFullyPaid()
	{
		return $this->getTotalPrice()<=$this->prop_amountpaid;
	}
	
	/**called by GetOrderByBarcode transaction*/
	static public function getOrderByBarcode($trans)
	{
		global $db;
		//find ticket
		$res=$db->select("ticket","orderid","ticketid=".$db->escapeString($trans->getbarcode()));
		//if none: find voucher
		if(count($res)<1)
			$res=$db->select("voucher","orderid","voucherid=".$db->escapeString($trans->getbarcode()));
		//find order
		if(count($res)>0)
			$trans->setorder(WOOrder::fromTableorder(WTorder::getFromDB($res[0]["orderid"])));
	}
	
	/**called by OrderChangeShipping transaction*/
	static public function changeShipping($trans)
	{
		//get shipping
		$shipid=$trans->getshippingid();
// 		print_r($shipid);
		global $session;
		if($shipid===false)$shipid=-1;else $shipid=$shipid+0;
		if($shipid>=0){
			$ship=WTshipping::getFromDB($shipid);
			if($ship===false){
				$trans->abortWithError(tr("Invalid shipping ID."));
				return;
			}
			if(!$session->checkFlags($ship->flags)){
				$trans->abortWithError(tr("You cannot use this shipping type."));
				return;
			}
		}
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		//get shipping costs
		$cost=0;
		if($shipid>=0)$cost=$ship->cost;
		if($trans->havePrivilege(WtrOrderChangeShipping::Priv_ChangePrice)){
			if($trans->getshippingcosts()!==false)
				$cost=$trans->getshippingcosts();
		}
		//set order
		$ord->shippingcosts=$cost;
		if($shipid>=0)
			$ord->shippingtype=$shipid;
		else
			$ord->shippingtype=null;
		$ord->update();
		//return order
		$trans->setorder(WOOrder::fromTableorder($ord));
	}
	
	/**called from the OrderMarkShipped transaction*/
	public static function markAsShipped($trans)
	{
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		if($ord->senttime !== null && $ord->senttime > 0){
			$trans->abortWithError(tr("Order has already been shipped."));
			return;
		}
		if($ord->status != WTorder::Placed){
			$trans->abortWithError(tr("Order is in the wrong state."));
			return;
		}
		//get timestamp
		$tm=time();
		if($trans->havePrivilege(WtrOrderMarkShipped::Priv_SetTime)){
			$tmt=$trans->getshiptime();
			if($tmt!==false && $tmt >0)
				$tm=$tmt;
		}
		//update
		$ord->senttime=$tm;
		$ord->status=WTorder::Sent;
		$ord->update();
		//return
		$order=WOOrder::fromTableorder($ord);
		$trans->setorder($order);
		//mail
		$order->mailDetails();
	}
	
	/**called from the OrderAddComment transaction*/
	public static function addComment($trans)
	{
		global $session;
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		//modify comment
		$cmt=trim($ord->comments);
		$nc=trim($trans->getcomment()."");
		if($nc!=""){
			if($cmt!="")$cmt.="\n";
			$cmt.="[".$session->currentUserName()." ".date("Y-m-d G:i")."] ".$nc;
			$ord->comments=$cmt;
			$ord->update();
		}
		//return
		$trans->setorder(WOOrder::fromTableorder($ord));
	}
	
	/**called from the OrderChangeComments transaction*/
	public static function changeComments($trans)
	{
		global $session;
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		//modify comment
		$ord->comments=trim($trans->getcomments()."");
		$ord->update();
		//return
		$trans->setorder(WOOrder::fromTableorder($ord));
	}
	
	/**called from the OrderPay transaction*/
	public static function payForOrder($trans)
	{
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		//calculate payment
		$pay=$trans->getamount()+0;
		if($pay<0){//negative: error out
			$trans->abortWithError(tr("Amount to be paid must be positive."));
			return;
		}
		$order=WOOrder::fromTableorder($ord);
		$max=$order->prop_amountdue;
		if($max<=0)$pay=0;else
		if($max<$pay)$pay=$max;
		if($pay==0){//zero: ignore it
			$trans->setorder($order);
			$trans->setamount(0);
			return;
		}
		//check payment type
		$ptype=$trans->getpaytype();
		$ptn=count(WTpaymenttype::selectFromDB());
		if($ptn==0){
			//none configured, just use the data
			$ord->paydata=$trans->getpaydata();
		}else{
			//get the one specified
			$pt=WTpaymenttype::getFromDB($ptype);
			if(!is_a($pt,"WTpaymenttype")){
				$trans->abortWithError(tr("unknown payment type"));
				return;
			}
			global $session;
			if(!$session->checkFlags($pt->flags)){
				$trans->abortWithError(tr("you are not allowed to use this payment type"));
				return;
			}
			$ord->paytype=$ptype;
			if(strlen($pt->dataname)>0)
				$ord->paydata=$trans->getpaydata();
		}
		//make corrections
		$ord->amountpaid+=$pay;
		$ord->update();
		//return (redo object conversion: calculated data has changed!)
		$trans->setorder(WOOrder::fromTableorder($ord));
		$trans->setamount($pay);
	}
	
	/**called from the UseVoucher transaction*/
	public static function payForOrderVoucher($trans)
	{
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		//get and verify voucher
		$vou=WTvoucher::getFromDB($trans->getvoucherid());
		if($vou===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		$vouobj=WOVoucher::fromTablevoucher($vou);
		if(!$vouobj->isStillValid()){
			$trans->abortWithError(tr("Voucher is past its validity date!"));
			return;
		}
		$vord=WOOrder::fromTableorder(WTorder::getFromDB($vou->orderid));
		if($vord===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		if($vord->getamountdue()>0){
			$trans->abortWithError(tr("Voucher cannot be used: it has not been paid for."));
			return;
		}
		//calculate payment
		$pay=$vou->value+0;
		if($pay<0){//negative: error out
			$trans->abortWithError(tr("Amount to be paid must be positive."));
			return;
		}
		//check that voucher is not used to pay for other vouchers
		$order=WOOrder::fromTableorder($ord);
		if(!$trans->havePrivilege(WTrUseVoucher::Priv_CanPayVoucherWithVoucher)){
			if(count($order->getvouchers())>0){
				$trans->abortWithError(tr("Cannot pay for voucher with another voucher."));
				return;
			}
		}
		//make the payment
		$max=$order->prop_amountdue;
		if($max<=0)$pay=0;else
		if($max<$pay)$pay=$max;
		if($pay==0){//zero: ignore it
			$trans->setorder($order);
			$trans->setamount(0);
			return;
		}
		//make corrections
		$ord->amountpaid+=$pay;
		//set payment type for audit
		if(WTpaymenttype::getFromDB("voucher")!==false){
                        $ord->paytype="voucher";//see db/order.wolf for this default paytype
                        $ord->paydata=$vou->voucherid;
		}else{
                        $ord->paydata=tr("Voucher: ").$vou->voucherid;
		}
		$ord->update();
		$vou->value-=$pay;
		$vou->isused=true;
		$vou->validtime=$vouobj->updatedValidity();
		$vou->update();
		//return (redo object conversion: calculated data has changed!)
		$trans->setorder(WOOrder::fromTableorder($ord));
		$trans->setvoucher(WOVoucher::fromTablevoucher($vou));
		$trans->setamount($pay);
	}
	
	/**called from the OrderRefund transaction*/
	public static function refundOrder($trans)
	{
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Order ID is not valid."));
			return;
		}
		//calculate payment
		$pay=$trans->getamount()+0;
		if($pay<0){//negative: error out
			$trans->abortWithError(tr("Amount to be refunded must be positive."));
			return;
		}
		$order=WOOrder::fromTableorder($ord);
		$max=0-$order->prop_amountdue; //refund is negative due
		if($max<=0)$pay=0;else
		if($max<$pay)$pay=$max;
		if($pay==0){//zero: ignore it
			$trans->setorder($order);
			$trans->setamount(0);
			return;
		}
		//make corrections
		$ord->amountpaid-=$pay;
		$ord->update();
		//return (redo object conversion: calculated data has changed!)
		$trans->setorder(WOOrder::fromTableorder($ord));
		$trans->setamount($pay);
	}
	
	/**called from the ReservationToOrder transaction*/
	public static function reservation2Order($trans)
	{
		global $db;
		//get order
		$ord=WTorder::getFromDB($trans->getorderid());
		if($ord===false){
			$trans->abortWithError(tr("Invalid Order ID."));
			return;
		}
		//check state
		if($ord->status != WTorder::Reserved){
			$trans->abortWithError(tr("This order is not a reservation."));
			return;
		}
		//change state
		$ord->status = WTorder::Placed;
		$ord->update();
		//change tickets
		$ticks=WTticket::selectFromDB("orderid=".$db->escapeInt($ord->orderid));
		foreach($ticks as $tick){
                        if($tick->status==WTticket::Reserved){
                                $tick->status=WTticket::Ordered;
                                $tick->update();
			}
		}
		//return
		$order=WOOrder::fromTableorder($ord);
		$trans->setorder($order);
		//mail
		$order->mailDetails();
	}
	
	/**called from the ReturnTicketVoucher transaction*/
	public static function returnTicketVoucher($trans)
	{
		//find ticket
		$obj=WTticket::getFromDB($trans->getbarcode());
		if($obj!==false){
			//verify ticket has not been used and is not in the past
			if(($obj->status & WTticket::MaskReturnable)==0){
				$trans->abortWithError(tr("This ticket cannot be returned!"));
				return;
			}
			$event=WTevent::getFromDB($obj->eventid);
			if($event === false){
				$trans->abortWithError(tr("Internal error: ticket for unknown event."));
				return;
			}
			if($event->starttime < time() && !$trans->havePrivilege(WtrReturnTicketVoucher::Priv_ReturnPastTicket)){
				$trans->abortWithError(tr("This ticket is for a past event and you do not have the privilege to return it."));
				return;
			}
			//return it
			$obj->status = WTticket::Cancelled;
			$obj->update();
		}else{
		//find voucher
		$obj=WTvoucher::getFromDB($trans->getbarcode());
		if($obj!==false){
			//verify voucher has not been used yet
			if($obj->isused){
				$trans->abortWithError(tr("This voucher has already been used, cannot return it."));
				return;
			}
			//return it
			$obj->price=0;
			$obj->value=0;
			$obj->update();
		}else{
			//nothing found
			$trans->abortWithError(tr("Ticket or voucher not found."));
			return;
		}}
		
		//return fresh order object
		$trans->setorder(WOOrder::fromTableorder(WTorder::getFromDB($obj->orderid)));
	}
	
	/**called from CancelOrder transaction*/
	static public function cancelOrder($trans)
	{
		global $db;
		//get order
		$oid=$trans->getorderid();
		$ord=WTorder::getFromDB($oid);
		if($ord===false){
			$trans->abortWithError(tr("Invalid order ID."));
			return;
		}
		//verify order status
		if($ord->status == WTorder::Cancelled){
			//ignore, already cancelled
			$trans->setorder(WOOrder::fromTableorder($ord));
			return;
		}
		//allow Placed/Reserved or Sent if privileged user
		$ok = ($ord->status == WTorder::Placed || $ord->status == WTorder::Reserved ||
		   ($ord->status == WTorder::Sent && $trans->havePrivilege(WtrCancelOrder::Priv_CancelSentOrder)));
		if(!$ok){
			$trans->abortWithError(tr("The order is in the wrong status or you do not have the privilege to cancel it."));
			return;
		}
		
		//get and verify tickets
		$ticks=WTticket::selectFromDB("orderid=".$db->escapeInt($oid));
		foreach($ticks as $tick){
			//ticket already cancelled?
			if($tick->status == WTticket::Cancelled)
				continue;
			//ticket not yet used?
			if(($tick->status & WTticket::MaskReturnable)==0){
				$trans->abortWithError(tr("This order contains a ticket that cannot be returned."));
				return;
			}
			//ticket in the past?
			$event=WTevent::getFromDB($tick->eventid);
			if($event === false){
				$trans->abortWithError(tr("Internal error: ticket for unknown event."));
				return;
			}
			if($event->starttime < time() && !$trans->havePrivilege(WtrCancelOrder::Priv_CancelPastTickets)){
				$trans->abortWithError(tr("This order contains a ticket that is for a past event and you do not have the privilege to cancel it."));
				return;
			}
		}
		//get and verify vouchers
		$voucs=WTvoucher::selectFromDB("orderid=".$db->escapeInt($oid));
		foreach($voucs as $vouc){
			//already used?
			if($vouc->isused){
				$trans->abortWithError(tr("This order contains a voucher that has already been used."));
				return;
			}
		}
		//TODO: get and verify items
		
		//return tickets
		foreach($ticks as $tick){
			$tick->status = WTticket::Cancelled;
			$tick->update();
		}
		//return vouchers
		foreach($voucs as $vouc){
			$vouc->price=0;
			$vouc->value=0;
			$vouc->update();
		}
		//TODO: return items
		
		//cancel order
		$ord->status = WTorder::Cancelled;
		$ord->update();
		
		//return
		$order=WOOrder::fromTableorder($ord);
		$trans->setorder($order);
		//send mail
		$order->mailDetails();
	}
	
	/**called from the two functions below - checks whether the ticket can be changed*/
	static protected function changeTicketPriceCheckTicket($trans,$tick,$changeused)
	{
		if($tick===false){
			$trans->abortWithError(tr("Invalid ticket ID."));
			return false;
		}
		$ok = (($tick->status & WTticket::MaskChangeable) ||
			($tick->status==WTticket::Used && $trans->havePrivilege($changeused)));
		if(!$ok){
			$trans->abortWithError(tr("The ticket cannot be changed anymore or you do not have the privilege."));
			return false;
		}
		return true;
	}
	/**called from the two functions below - checks whether the ticket can be changed*/
	static protected function changeTicketPriceCheckEvent($trans,$event,$changepast)
	{
		if($event===false){
			$trans->abortWithError(tr("Internal error: ticket for unknown event."));
			return false;
		}
		if($event->starttime < time() && !$trans->havePrivilege($changepast)){
			$trans->abortWithError(tr("The ticket is for an event in the past and you do not have the privilege to change it."));
			return false;
		}
		return true;
	}
	
	/**called from the ChangeTicketPrice transaction*/
	static public function changeTicketPrice($trans)
	{
		//get ticket
		$tick=WTticket::getFromDB($trans->getbarcode());
		//verify ticket
		if(!self::changeTicketPriceCheckTicket($trans,$tick,WTrChangeTicketPrice::Priv_ChangeUsedTicket))return;
		$event=WTevent::getFromDB($tick->eventid);
		if(!self::changeTicketPriceCheckEvent($trans,$event,WTrChangeTicketPrice::Priv_ChangePastTicket))return;
		//change ticket
		$tick->price=$trans->getprice();
		$tick->update();
		//return order
		$trans->setorder(WOOrder::fromTableorder(WTorder::getFromDB($tick->orderid)));
	}
	/**called from the ChangeTicketPriceCategory transaction*/
	static public function changeTicketPriceCategory($trans)
	{
		global $db;
		//get ticket
		$tick=WTticket::getFromDB($trans->getbarcode());
		if($tick===false){
			$trans->abortWithError(tr("Invalid ticket ID."));
			return;
		}
		//verify ticket
		$tick=WTticket::getFromDB($trans->getbarcode());
		if(!self::changeTicketPriceCheckTicket($trans,$tick,WTrChangeTicketPriceCategory::Priv_ChangeUsedTicket))return;
		$event=WTevent::getFromDB($tick->eventid);
		if(!self::changeTicketPriceCheckEvent($trans,$event,WTrChangeTicketPriceCategory::Priv_ChangePastTicket))return;
		//get category
		$cat=WTpricecategory::getFromDB($trans->getpricecategoryid());
		if($cat===false){
			$trans->abortWithError(tr("Invalid price category."));
			return;
		}
		$prc=WTeventprice::getFromDB($event->eventid,$cat->pricecategoryid);
		if($prc===false){
			$trans->abortWithError(tr("Category is not valid for this event."));
			return;
		}
		//verify category flags
		global $session;
		if(!$session->checkFlags($prc->flags)){
			$trans->abortWithError(tr("You do not have access to this category on this event."));
			return;
		}
		//make sure there are enough seats left in the category
		$ticks=WTticket::selectFromDB("eventid=".$db->escapeInt($tick->eventid)." and pricecategoryid=".$db->escapeInt($prc->pricecategoryid));
		$tcount=0;
		foreach($ticks as $t)if(($t->status & WTticket::MaskBlock)!=0)$tcount++;
		if($tcount>=$prc->maxavailable){
			$trans->abortWithError(tr("No more tickets left in this category."));
			return;
		}
		//TODO: make sure seat allows the category
		//change ticket
		$tick->pricecategoryid=$prc->pricecategoryid;
		$tick->price=$prc->price;
		$tick->update();
		//return order
		$trans->setorder(WOOrder::fromTableorder(WTorder::getFromDB($tick->orderid)));
	}
	
	/**called from the DeductVoucher transaction*/
	static public function deductVoucher($trans)
	{
		//verify amount
		$amt=$trans->getamount()+0;
		if($amt<0){//negative: error out
			$trans->abortWithError(tr("Amount to be paid must be positive."));
			return;
		}
		//get and verify voucher
		$vou=WTvoucher::getFromDB($trans->getvoucherid());
		if($vou===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		$vouobj=WOVoucher::fromTablevoucher($vou);
		if(!$vouobj->isStillValid()){
			$trans->abortWithError(tr("Voucher is past its validity date!"));
			return;
		}
		$vord=WOOrder::fromTableorder(WTorder::getFromDB($vou->orderid));
		if($vord===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		if($vord->getamountdue()>0){
			$trans->abortWithError(tr("Voucher cannot be used: it has not been paid for."));
			return;
		}
		//calculate payment
		$lft=$vou->value+0;
		if($lft<0){//negative: error out
			$trans->abortWithError(tr("Internal error: negative voucher."));
			return;
		}
		if($lft<$amt){
			//not enough left, tell user
			$trans->setamount(0);
			$trans->setvoucher(WOVoucher::fromTablevoucher($vou));
			return;
		}
		//make corrections
		$vou->value-=$amt;
		$vou->isused=true;
		$vou->validtime=$vouobj->updatedValidity();
		$vou->comment=$trans->getcomment();
		$vou->update();
		//return
		$trans->setamount($amt);
		$trans->setvoucher(WOVoucher::fromTablevoucher($vou));
	}

	/**called from the ChangeVoucherValidity transaction*/
	static public function changeVoucherValidity($trans)
	{
		//get and verify voucher
		$vou=WTvoucher::getFromDB($trans->getvoucherid());
		if($vou===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		$vord=WOOrder::fromTableorder(WTorder::getFromDB($vou->orderid));
		if($vord===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		//calculate payment
		//make corrections
		if($trans->getvalidforever())
			$vou->validtime=null;
		else
			$vou->validtime=$trans->getvalidtime()+0;
		$vou->comment=$trans->getcomment();
		$vou->update();
		//return
		$trans->setvoucher(WOVoucher::fromTablevoucher($vou));
		$vord=WOOrder::fromTableorder(WTorder::getFromDB($vou->orderid));
		if($vord!==false){
			$trans->setorder($vord);
		}
	}
	
	/**called from EmptyVoucher transaction*/
	static public function emptyVoucher($trans)
	{
		//get and verify voucher
		$vou=WTvoucher::getFromDB($trans->getvoucherid());
		if($vou===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		$vord=WOOrder::fromTableorder(WTorder::getFromDB($vou->orderid));
		if($vord===false){
			$trans->abortWithError(tr("Voucher is not valid!"));
			return;
		}
		if($vord->getamountdue()>0){
			$trans->abortWithError(tr("Voucher cannot be used: it has not been paid for."));
			return;
		}
		//empty it
		$vou->value=0;
		$vou->isused=true;
		$vou->comment=$trans->getcomment();
		$vou->update();
		//return
		$trans->setvoucher(WOVoucher::fromTablevoucher($vou));
	}	
	
	///order audit transaction
	static public function getOrderAuditTransaction($trans)
	{
		global $db;
		$oid=$trans->getorderid();
		$w='orderid='.$db->escapeInt($oid);
		$trans->setorder( WOOrderAudit::fromTableArrayorder_audit( WTorder_audit::selectFromDB($w)));
		$trans->setvouchers( WOVoucherAudit::fromTableArrayvoucher_audit( WTvoucher_audit::selectFromDB($w)));
		$trans->settickets( WOTicketAudit::fromTableArrayticket_audit( WTticket_audit::selectFromDB($w)));
	}
	
	///mail order details to customer
	public function mailDetails()
	{
		//get customer
		$ct=WTcustomer::getFromDB($this->getcustomerid());
		if(!is_a($ct,"WTcustomer")){
			return;
		}
		if(!isEmail($ct->email)){
			return;
		}
		//init twig
		BaseVars::initTwig();
		BaseVars::init();
		//gather basics and render mail
		global $IsDemoSystem,$DemoMailAddr;
		global $twig,$basevars;
		$p=$twig->loadTemplate("ordermail.txt");
		$list['customer']=WOCustomer::fromTablecustomer($ct);
		$list['order']=$this;
		$page=explode("\n",trim($p->render($list)));
		//parse mail
		if(count($page)<2)return;
		$subject=array_shift($page);
		$mode=0;
		$mailtext="";$mailheader="";
		foreach($page as $line){
			if($mode==0){
				if(trim($line)=="")$mode=1;
				else $mailheader.=$line."\n";
			}else{
				$mailtext.=$line."\n";
			}
		}
		//send mail
		if(isset($IsDemoSystem) && $IsDemoSystem==true && isset($DemoMailAddr))
			$tmail=$DemoMailAddr;
		else
			$tmail=$ct->email;
		mail($tmail,$subject,$mailtext,$mailheader);
	}
	
	///change the addresses on the order, called by ChangeOrderAddress transaction
	static public function changeAddress($trans)
	{
		global $db;
		$oid=$trans->getorderid();
		$ord=WTorder::getFromDB($oid);
		if(!is_a($ord,"WTorder")){
			$trans->abortWithError(tr("Order does not exist."));
			return;
		}
		//invoice address
		$cid=$ord->customerid;
		if($trans->getsetinvoiceaddr()){
			$iad=$trans->getinvoiceaddr();
			if($iad<0){
				$ord->invoiceaddress=null;
			}else{
				$addr=WTaddress::getFromDB($iad);
				if(!is_a($addr,"WTaddress")){
					$trans->abortWithError(tr("Invalid address ID"));
					return;
				}
				if($addr->customerid!=$cid){
					$trans->abortWithError(tr("Address does not match customer."));
					return;
				}
				$ord->invoiceaddress=$iad;
			}
		}
		//same with delivery address
		if($trans->getsetdeliveryaddr()){
			$dad=$trans->getdeliveryaddr();
			if($dad<0){
				$ord->deliveryaddress=null;
			}else{
				$addr=WTaddress::getFromDB($dad);
				if(!is_a($addr,"WTaddress")){
					$trans->abortWithError(tr("Invalid address ID"));
					return;
				}
				if($addr->customerid!=$cid){
					$trans->abortWithError(tr("Address does not match customer."));
					return;
				}
				$ord->deliveryaddress=$dad;
			}
		}
		//send to DB
		$ord->update();
		//fetch and deliver result
		$trans->setorder(WOOrder::fromTableorder(WTorder::getFromDB($oid)/*$ord*/));
	}
	
	static public function getPaymentTypesTransaction($trans)
	{
		$pts=WOPaymentType::fromTableArraypaymenttype(WTpaymenttype::selectFromDB());
		$trans->setpaytypes($pts);
		global $db;
		$trans->setdefaultpaytype($db->getConfig("defaultpaytype"));
	}
	
	static public function setPayTypeTransaction($trans)
	{
		$pt=$trans->getpaytype();
		if(!is_a($pt,"WOPaymentType")){
			$trans->abortWithError(tr("Invalid Data"));
			return;
		}
		//find it
		$ptt=WTpaymenttype::getFromDB($pt->getname());
		if(!is_a($ptt,"WTpaymenttype")){
			$ptt=WTpaymenttype::newRow();
		}
		//set data
		$pt->toTablepaymenttype($ptt);
		$ptt->insertOrUpdate();
		//done
		$trans->setpaytype(WOPaymentType::fromTablepaymenttype($ptt));
	}
	
	static public function deletePayTypeTransaction($trans)
	{
		//find it
		$ptt=WTpaymenttype::getFromDB($trans->getpaytype());
		if(!is_a($ptt,"WTpaymenttype")){
			$ptt=WTpaymenttype::newRow();
		}
		//delete
		$ptt->deleteFromDb();
	}
	
	static public function setDefaultPayTypeTransaction($trans)
	{
		global $db;
		//check that the type exists
		$dpt=$trans->getdefaultpaytype();
		$pt=WTpaymenttype::getFromDB($dpt);
		if(!is_a($pt,"WTpaymenttype")){
			$trans->abortWithError(tr("The payment type does not exist, cannot set it as default."));
			return;
		}
		//set it
		$db->setConfig("defaultpaytype",$dpt);
		$trans->setdefaultpaytype(WOPaymentType::fromTablepaymenttype($pt));
	}
	
	static public function getUserAuditTransaction($trans)
	{
		//check user exists
		$uname=$trans->getuname();
		$oldest=$trans->getoldest();
		$user=WTuser::getFromDB($uname);
		if(!is_a($user,"WTuser")){
			$trans->abortWithError(tr("User does not exist."));
			return;
		}
		//remember user name
		$trans->setuname($uname);
		//get order pieces that match
		global $db;
		$trans->setorders(WOOrderAudit::fromTableArrayorder_audit(WTorder_audit::selectFromDB("audituname=".$db->escapeString($uname)." AND audittime>=".$db->escapeInt($oldest),"ORDER BY auditid")));
		//get tickets
		$trans->settickets(WOTicketAudit::fromTableArrayticket_audit(WTticket_audit::selectFromDB("audituname=".$db->escapeString($uname)." AND audittime>=".$db->escapeInt($oldest),"ORDER BY auditid")));
		//get vouchers
		$trans->setvouchers(WOVoucherAudit::fromTableArrayvoucher_audit(WTvoucher_audit::selectFromDB("audituname=".$db->escapeString($uname)." AND audittime>=".$db->escapeInt($oldest),"ORDER BY auditid")));
		//get items
		$trans->setitems(WOItemAudit::fromTableArrayitem_audit(WTitem_audit::selectFromDB("audituname=".$db->escapeString($uname)." AND audittime>=".$db->escapeInt($oldest),"ORDER BY auditid")));
	}
	
	static public function getPrintAtHomeSettings($trans)
	{
		$res=array();
		$cfg=WTconfig::selectFromDB();
		foreach($cfg as $c) {
			if(strncmp($c->ckey,"printathome:",12)==0){
				$r=new WOKeyValuePair;
				$r->setkey(substr($c->ckey,12));
				$r->setvalue($c->cval);
				$res[]=$r;
			}
		}
		$trans->setsettings($res);
		
	}
	
	static public function setPrintAtHomeSettings($trans)
	{
		foreach($trans->getsettings() as $cfg) {
			if($cfg->getisnull()){
				$ct=WTconfig::getFromDB("printathome:".$cfg->getkey());
				$ct->deleteFromDb();
			}else{
				$ct=WTconfig::newRow(array("ckey"=> ("printathome:".$cfg->getkey()), "cval"=>$cfg->getvalue()));
				$ct->insertOrUpdate();
			}
		}
		
	}

	static public function getOrderDocumentNames($trans)
	{
		global $db;
		$q="orderid=".$db->escapeInt($trans->getorderid());
		$trans->setdocumentinfo(WOOrderDocumentInfo::fromTableArrayorderdocuments(
			WTorderdocuments::selectFromDB($q)));
	}

	static public function getOrderDocument($trans)
	{
		global $db;
		$q="orderid=".$db->escapeInt($trans->getorderid())." AND filename=".
			$db->escapeString($trans->getfilename());
		$tab=WTorderdocuments::selectFromDB($q);
		if(count($tab)>0)
			$trans->setdocument(WOOrderDocument::fromTableorderdocuments($tab[0]));
	}

	static public function setOrderDocument($trans)
	{
		global $db;
		$doc=$trans->getdocument();
		$q="orderid=".$db->escapeInt($doc->getorderid())." AND filename=".
			$db->escapeString($doc->getfilename());
		$tab=WTorderdocuments::selectFromDB($q);
		if(count($tab)>0)
			$nrow=$tab[0];
		else
			$nrow=WTorderdocuments::newRow(array(
				"orderid"=>$doc->getorderid(),
				"filename"=>$doc->getfilename()
				));
		$nrow->content=$doc->getcontent();
		$nrow->mtime=time();
		$nrow->visible=$doc->getvisible();
		$nrow->insertOrUpdate();
	}
};


//eof
return;
?>
