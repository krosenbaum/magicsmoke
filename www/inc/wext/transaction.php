<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class MSmokeTransaction extends WobTransaction
{
	public function __construct()
	{
		parent::__construct();
	}
	
	protected function mkSession()
	{
		//make sure we have a session
		global $session;
		if(!isset($session))new Session($this);
	}
	
	protected function startTransaction($updating)
	{
		global $db;
		WobTransaction::debug("Starting MSmoke Transaction as ".($updating?"updating":"read only"),WobTransaction::DebugTransactions);
		$db->setTransactionUpdating($updating);
		$db->beginTransaction();
	}
	protected function commitTransaction()
	{
		global $db;
		WobTransaction::debug("Comitting MSmoke Transaction",WobTransaction::DebugTransactions);
		$db->commitTransaction();
	}
	protected function abortTransaction()
	{
		global $db;
		WobTransaction::debug("Aborting MSmoke Transaction",WobTransaction::DebugTransactions);
		$db->rollbackTransaction();
	}
	
	protected function isAuthenticated()
	{
		$this->mkSession();
		return Session::instance()->isAuthenticated();
	}
	
	protected function isAuthorized($tn)
	{
		$this->mkSession();
		return Session::instance()->isAuthenticated() && Session::instance()->canExecute($tn);
	}
	
	protected function userName()
	{
		$this->mkSession();
		return Session::currentUserName();
	}
};


//eof
return;
?>
