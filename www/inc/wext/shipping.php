<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/**implementation of access to shipping types*/
class WOShipping extends WOShippingAbstract
{
	/**create a new shipping type*/
	static public function create($trans)
	{
		$row=WTshipping::newRow();
		$trans->getshipping()->toTableshipping($row);
		$row->insert();
		$trans->setshipping(WOShipping::fromTableshipping($row));
	}
	static public function change($trans)
	{
		$ship=$trans->getshipping();
		$row=WTshipping::getFromDB($ship->getshipid());
		if(!$row instanceof WTshipping){
			$trans->abortWithError(tr("Shipping type not found."));
			return;
		}
		$ship->toTableshipping($row);
		if($row->update())
			$trans->setshipping(WOShipping::fromTableshipping($row));
		else
			$trans->abortWithError(tr("Error while updating shipping information."));
	}
	static public function remove($trans)
	{
		$row=WTshipping::getFromDB($trans->getshipid());
		if($row instanceof WTshipping)
			$row->deleteFromDb();
	}
};


//eof
return;
?>
