<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOArtist extends WOArtistAbstract
{
	/**called from the CreateArtist transaction*/
	static public function createArtist($trans)
	{
		$nm=trim($trans->getname());
		if($nm===false || $nm==""){
			$trans->abortWithError(tr("Artist name must not be empty!"));
			return;
		}
		//check for doublettes
		global $db;
		$at=WTartist::selectFromDB('artistname='.$db->escapeString($nm));
		if(count($at)>0){
			$trans->abortWithError(tr("An artist with this name already exists."));
			return;
		}
		//create
		$at=WTartist::newRow();
		$at->artistname=$nm;
		$at->description=$trans->getdescription();
		$at->comment=$trans->getcomment();
		$at->insert();
		//return
		$trans->setartist(WOArtist::fromTableartist($at));
	}
};

//eof
return;
?>
