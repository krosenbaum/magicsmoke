<?php
// (c) Konrad Rosenbaum, 2007-2016
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOVoucher extends WOVoucherAbstract
{
	///helper for audit table creation
	public static function getTransactionComment()
	{
		//get the transaction
		$inst=WobTransaction::getInstance();
		if($inst == null)return null;
		//check whether it has a comment
		if(method_exists($inst,"getcomment"))
			return $inst->getcomment();
		//for usevoucher return the order id
		if(is_a($inst,"WTrUseVoucher"))
			return "order: ".$inst->getorderid();
		//fall back
		return null;
	}
	
	///implements the GetVoucherAudit transaction
	public static function GetVoucherAuditTransaction($trans)
	{
		global $db;
		$tab=WTvoucher_audit::selectFromDB('voucherid='.$db->escapeString($trans->getvoucherid()));
		$arr=WOVoucherAudit::fromTableArrayvoucher_audit($tab);
		$trans->setvoucher($arr);
	}
	
	///returns all valid voucher prices
	static public function validVoucherPrices()
	{
		global $db;
		return explode(' ',$db->getConfig('ValidVouchers'));
	}

	///returns true if the voucher is currently valid
	public function isStillValid()
	{
		if($this->getvalidtime()===null || $this->getvalidtime()===false)
			return true;
		//compare now with validity date (plus 24h, since we store the date at 0:00)
		return time() <= ($this->getvalidtime()+24*60*60);
	}

	///returns a timestamp to be stored for updating the validity period upon use of the voucher
	public function updatedValidity()
	{
		return self::calculateUpdatedValidity($this->getvalidtime());
	}

	///returns a timestamp to be stored for updating the validity period upon use of the voucher
	/// \param $time the current timestamp
	/// \returns the new timestamp to be stored in the DB
	public static function calculateUpdatedValidity($time)
	{
		global $VoucherUseAdd,$VoucherUseDate, $VoucherUseStart;
		$nval= self::calculateValidity($time,$VoucherUseStart,$VoucherUseAdd,$VoucherUseDate);
		if($time===null||$time===false || $nval===false||$nval===null)return null;
		if($time>$nval)return $time;
		else return $nval;
	}

	///calculates an initial validity date and returns it as timestamp
	public static function calculateInitialValidity()
	{
		global $VoucherInitAdd, $VoucherInitDate, $VoucherInitStart;
		return self::calculateValidity(null,$VoucherInitStart,$VoucherInitAdd,$VoucherInitDate);
	}

	///helper function to execute validity period calculations; see the docu in the config.php.template file for details on parameters
	/// \param $current the current validity time or null for initial calculations
	/// \param $startstr the start mode (i - infinity, n - now, c - $current)
	/// \param $addstr DateTimeInterval spec to be added (empty: no adding)
	/// \param $datestr absolute corrections to the date (e.g. to set it to the end of the month)
	/// \returns the new timestamp that can be stored in the DB
	private static function calculateValidity($current,$startstr,$addstr,$datestr)
	{
		// Start
		switch($startstr){
			case "i":$current=null;break;
			case "n":$current=time();break;
			case "c":break;
			default:
				WobTransaction::debug("Invalid Validity Calculation start block '".$startstr."'");
				return $current;
		}
		//WobTransaction::debug("start ".time()." current ".$current);
		if($current===null||$current===false)return null;
		$cdate=new DateTime();
		$cdate->setTimestamp($current);
		$cdate->setTime(0,0);
		// Add
		if($addstr!=""){
			$cdate->add(new DateInterval($addstr));
			//WobTransaction::debug("added ".$addstr." get ".$cdate->format("Y m d H i s"));
		}
		// Correct
		if($datestr!="" && $datestr!="*-*-*"){
			$datea=explode("-",$datestr);
			if(count($datea)!=3){
				WobTransaction::debug("Invalid Date correction block '".$datestr."'");
				return $cdate->getTimestamp();
			}
			$cda=explode(" ",$cdate->format("Y n j"));
			//WobTransaction::debug("cda ".$cdate->format("Y n j")." str ".$datestr);
			if($datea[0]!="*")$cda[0]=$datea[0];
			if($datea[1]!="*")$cda[1]=$datea[1];
			if($datea[2]!="*"){
				if($datea[2]=="end"){
					switch($cda[1]+0){
						case 1:$cda[2]=31;break;
						case 2:
							if((($cda[0]+0)%4)==0) //good enough for most of the century... ;-)
								$cda[2]=29;
							else
								$cda[2]=28;
							break;
						case 3:$cda[2]=31;break;
						case 4:$cda[2]=30;break;
						case 5:$cda[2]=31;break;
						case 6:$cda[2]=30;break;
						case 7:$cda[2]=31;break;
						case 8:$cda[2]=31;break;
						case 9:$cda[2]=30;break;
						case 10:$cda[2]=31;break;
						case 11:$cda[2]=30;break;
						case 12:$cda[2]=31;break;
					}
				}else
					$cda[2]=$datea[2];
			}
			//WobTransaction::debug("cda ".$cda[0]." ".$cda[1]." ".$cda[2]);
			$cdate->setDate($cda[0]+0,$cda[1]+0,$cda[2]+0);
		}
		//return
		//WobTransaction::debug("final ".$cdate->format("Y m d H i s"));
		return $cdate->getTimestamp();
	}
};


//eof
return;
?>
