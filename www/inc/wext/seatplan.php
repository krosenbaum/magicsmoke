<?php
// (c) Konrad Rosenbaum, 2017
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

class WOSeatPlan extends WOSeatPlanAbstract
{
	private $categories;
	private $eventprices;
	private $lasterror;

	///constructs a fresh seat plan object
	public function __construct()
	{
		//get categories
		$this->categories=WTpricecategory::selectFromDB("");
	}

	///initializes additional properties of groups and aligns with the event
	public function initGroups($eventid)
	{
		global $db;
		$this->eventprices=WTeventprice::selectFromDB("eventid=".$db->escapeInt($eventid));
		foreach($this->prop_Group as $g)$g->initGroup($this);
		foreach($this->prop_VGroup as $g)$g->initGroup($this);
	}

	///finds a group by its ID/abbreviation or null if it cannot be found
	public function groupById($abbr)
	{
		foreach($this->prop_Group as $g)
			if($g->getid()==$abbr)
				return $g;
		return null;
	}

	///\internal helper for group: converts price string into category IDs
	public function priceIDs($prc)
	{
		$ret=array();
		foreach(explode(' ',$prc) as $p)
		{
			$i=$p+0;
			if(is_numeric($p) && is_int($i)){
				foreach($this->categories as $cat){
					if($cat->pricecategoryid==$i){
						$ret[]=$i;
						break;
					}
				}
			}else{
				foreach($this->categories as $cat){
					if($cat->abbreviation==$p){
						$ret[]=$cat->pricecategoryid;
						break;
					}
				}
			}
		}
		return $ret;
	}

	///\internal helper for group: converts price string into category IDs
	public function priceAbbrs($prc)
	{
		$ret=array();
		foreach(explode(' ',$prc) as $p)
		{
			$i=$p+0;
			if(is_numeric($p) && is_int($i)){
				foreach($this->categories as $cat){
					if($cat->pricecategoryid==$i){
						$ret[]=$cat->abbreviation;
						break;
					}
				}
			}else{
				foreach($this->categories as $cat){
					if($cat->abbreviation==$p){
						$ret[]=$cat->abbreviation;
						break;
					}
				}
			}
		}
		return $ret;
	}

	///\internal helper for group: get category capacities
	public function priceCapacities($prc)
	{
		$ids=$this->priceIDs($prc);
		$ret=array();
		foreach($this->eventprices as $ep)
		{
			if(in_array($ep->pricecategoryid,$ids)){
				$ret[]=$ep->maxavailable;
			}
		}
		return $ret;
	}

	///tries to add tickets in a specific category to the corresponding group(s)
	///returns true on success, false if not all tickets could be added
	///in case of error the internal state may be inconsistent, so subsequent calls to this method are not reliable
	//TODO: handle seat numbers
	public function addTickets($numtickets,$categoryid)
	{
		//validate VGroups
		foreach($this->prop_VGroup as $vg){
			if(in_array($categoryid,$vg->priceIDs())){
				if($vg->availableSeats()<$numtickets){
					$this->lasterror=tr("VGroup violated - ").$vg->getname();
					return false;
				}else
					$vg->addTickets($numtickets);
			}
		}

		//distribute over Groups
		$catfound=false;
		$cats=array();
		foreach($this->prop_Group as $gr){
			if(in_array($categoryid,$gr->priceIDs())){
				$catfound=true;
				$cats[]=$gr->getname();
				$a=$gr->availableSeats();
				if($a<$numtickets){
					$gr->addTickets($a);
					$numtickets-=$a;
				}else{
					$gr->addTickets($numtickets);
					$numtickets=0;
					break;
				}
			}
		}

		//handle exclusive==false
		if($this->prop_exclusive==false && !$catfound)return true;
		//anything left?
		if($numtickets == 0)
			return true;
		else{
			$this->lasterror=tr("Not enough seats in groups ").implode(", ",$cats);
			return false;
		}
	}

	public function lastError(){return $this->lasterror;}
};

class WOSeatPlanGroup extends WOSeatPlanGroupAbstract
{
	private $owner=null;
	private $maxseats=0;
	private $blockseats=0;

	///\internal used by WOSeatPlan only
	public function initGroup($p)
	{
		$this->owner=$p;
		if(is_numeric($this->prop_capacity) && is_int($this->prop_capacity+0))
			$this->maxseats=$this->prop_capacity+0;
		else switch($this->prop_capacity){
			case "maxcat":$this->maxseats=max($this->owner->priceCapacities($this->prop_price));break;
			case "mincat":$this->maxseats=min($this->owner->priceCapacities($this->prop_price));break;
			case "sumcat":foreach($this->owner->priceCapacities($this->prop_price) as $c)$this->maxseats+=$c;break;
			case "auto"://TODO: count seats in rows
			default:$this->maxseats=0;
		}
		if($this->maxseats<0)$this->maxseats=0;
	}

	///returns the categories as IDs
	public function priceIDs()
	{
		return $this->owner->priceIDs($this->prop_price);
	}

	///returns the categories as Abbreviations
	public function priceAbbrs()
	{
		return $this->owner->priceAbbrs($this->prop_price);
	}

	///returns the maximum of seats in this group
	public function maxSeats()
	{
		return $this->maxseats;
	}

	///returns the number of currently blocked seats in this group
	public function blockedSeats()
	{
		return $this->blockseats;
	}

	///returns the number of available seats in this group
	public function availableSeats()
	{
		if($this->blockseats>=$this->maxseats)return 0;
		if($this->blockseats<=0)return $this->maxseats;
		return $this->maxseats - $this->blockseats;
	}

	///adds a number of tickets to the group
	//TODO: handle seat numbers
	public function addTickets($i)
	{
		if($i>0)$this->blockseats+=$i;
	}
};

class WOSeatPlanVGroup extends WOSeatPlanVGroupAbstract
{
	private $owner=null;
	private $maxseats=0;
	private $blockseats=0;

	///\internal used by WOSeatPlan only
	public function initGroup($p)
	{
		$this->owner=$p;
		if(is_numeric($this->prop_capacity) && is_int($this->prop_capacity+0))
			$maxseats=$this->prop_capacity+0;
		else switch($this->prop_capacity){
			case "maxcat":$this->maxseats=max($this->owner->priceCapacities($this->prop_price));break;
			case "mincat":$this->maxseats=min($this->owner->priceCapacities($this->prop_price));break;
			case "sumcat":foreach($this->owner->priceCapacities($this->prop_price) as $c)$this->maxseats+=$c;break;
			default:$this->maxseats=0;
		}
		if($this->maxseats<0)$this->maxseats=0;
	}

	///returns the categories as IDs
	public function priceIDs()
	{
		return $this->owner->priceIDs($this->prop_price);
	}

	///returns the categories as Abbreviations
	public function priceAbbrs()
	{
		return $this->owner->priceAbbrs($this->prop_price);
	}

	///returns the maximum of seats in this group
	public function maxSeats()
	{
		return $this->maxseats;
	}

	///returns the number of currently blocked seats in this group
	public function blockedSeats()
	{
		return $this->blockseats;
	}

	///returns the number of available seats in this group
	public function availableSeats()
	{
		if($this->blockseats>=$this->maxseats)return 0;
		if($this->blockseats<=0)return $this->maxseats;
		return $this->maxseats - $this->blockseats;
	}

	///adds a number of tickets to the group
	public function addTickets($i)
	{
		if($i>0)$this->blockseats+=$i;
	}
};

class WOSeatPlanDefPrice extends WOSeatPlanDefPriceAbstract
{};



return;
?>
