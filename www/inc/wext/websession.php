<?php
// (c) Konrad Rosenbaum, 2007-2011
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

define("COOKIE_WEBSESSION", "msmoke_session");

/** creates a web session to authorize a customer */
class WOWebSession extends WOWebSessionAbstract
{
	///if the cookie exists: gets the current web session, if not: creates it
	static public function getOrCreateWebSession()
	{
		global $_COOKIE;
		global $db;
		
		//prune session table
		$db->deleteRows("websession", "timeout < ".time());
		
		//check cookie
		if(isset($_COOKIE[COOKIE_WEBSESSION])){
			$wsid=$_COOKIE[COOKIE_WEBSESSION];
			$res = WTwebsession::getFromDB($wsid);
			if (is_a($res,"WTwebsession")){
				return WOWebSession::fromTablewebsession($res);
			}
		}
		//fall back
		//create entry
		global $WebSessionTimeout;
		$ws=WTwebsession::newRow();
		$ws->timeout=time()+$WebSessionTimeout;
		$ws->insert();
		//set cookie
		setCookie(COOKIE_WEBSESSION,$ws->sessionid,0);
		//return
		return WOWebSession::fromTablewebsession($ws);
	}
	
	/** \internal called to generate a new session ID, used by WTwebsession to generate the primary key*/
	static public function getNewSessionId(){
		do{
			//generate ID
			$ci=getCode39ID(32);
			//look for duplicate
			$res=WTwebsession::getFromDB($ci);
			if(is_a($res,"WTcart"))continue;
			//return ID
			return $ci;
		}while(true);
	}

	/** logs the customer out */
	public function logout()
	{
		global $db;
		$db->deleteRows("websession", "sessionid=".$db->escapeString($this->sessionid));
		setcookie(COOKIE_WEBSESSION, "", 1);
		redirectHome();
		exit();
	}
};

//eof
return;
?>
