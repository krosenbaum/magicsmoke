<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOCartOrder extends WOCartOrderAbstract
{
	const LateSale=-1;
	const AfterSale=-2;
	
	/**called by the CreateOrder and CreateReservation transactions*/
	public static function createOrder($trans)
	{
		//get cart object
		$cart=$trans->getcart();
		if(!is_a($cart,"WOCartOrder")){
			$trans->abortWithError(tr("Not a valid cart object."));
			return;
		}
// 		print_r($trans);
		//get permissions
		global $db;
		$isreserve=false;
		$issale=false;
		$vanyval=false;
		$vdiffprice=false;
		$tsalestop=0;
		if(is_a($trans,"WTrCreateOrder")){
			//gather privileges
			$vanyval=$trans->havePrivilege(WTrCreateOrder::Priv_AnyVoucherValue);
			$vdiffprice=$trans->havePrivilege(WTrCreateOrder::Priv_DiffVoucherValuePrice);
			//find out what the time limit on ordering is
			$tsalestop=$db->getConfig("OrderStop")+0;
			if($tsalestop<0)$tsalestop=0;
			if($trans->havePrivilege(WTrCreateOrder::Priv_LateSale))
				$tsalestop=self::LateSale;
			if($trans->havePrivilege(WTrCreateOrder::Priv_AfterTheFactSale))
				$tsalestop=self::AfterSale;
			//check whether we are allowed to create a sale/order
			$issale=$trans->getissale();
			if($issale){
				if(!$trans->havePrivilege(WTrCreateOrder::Priv_CanSell)){
					$trans->abortWithError(tr("Lacking privileges to create a sale."));
					return;
				}
			}else{
				if(!$trans->havePrivilege(WTrCreateOrder::Priv_CanOrder)){
					$trans->abortWithError(tr("Lacking privileges to create an order."));
					return;
				}
			}
			//check whether we can handle the content of this order
			if(count($cart->gettickets())>0 && !$trans->havePrivilege(WTrCreateOrder::Priv_CanOrderTicket)){
				$trans->abortWithError(tr("You do not have the privilege to sell tickets."));
				return;
			}
			if(count($cart->getvouchers())>0 && !$trans->havePrivilege(WTrCreateOrder::Priv_CanOrderVoucher)){
				$trans->abortWithError(tr("You do not have the privilege to sell vouchers."));
				return;
			}
			if(count($cart->getitems())>0 && !$trans->havePrivilege(WTrCreateOrder::Priv_CanOrderItem)){
				$trans->abortWithError(tr("You do not have the privilege to sell shop items."));
				return;
			}
		}else
		if(is_a($trans,"WTrCreateReservation")){
			$isreserve=true;
			$tsalestop=$db->getConfig("ReserveStop")+0;
			if($tsalestop<0)$tsalestop=0;
			if($trans->havePrivilege(WTrCreateReservation::Priv_LateReserve))
				$tsalestop=0;
		}else{
			$trans->abortWithError(tr("CreateOrder called from an unknown transaction."));
			return;
		}
		//check there is anything there
		if(count($cart->gettickets())==0 &&
		   count($cart->getvouchers())==0 &&
		   count($cart->getitems())==0){
			$trans->abortWithError(tr("Nothing in the cart."));
			return;
		}
		//verify necessary elements and content
		$very=true;
		$very&=$cart->verifyCustomer($trans);
		$very&=$cart->verifyShipping($trans);
		
		$very&=$cart->verifyTickets($trans,$tsalestop);
		if($isreserve){
			if(count($cart->getvouchers())>0 || count($cart->getitems())>0){
				$trans->abortWithError(tr("Reservations must not contain anything but tickets."));
				return;
			}
		}else{
			//verify vouchers and items are valid
			$very&=$cart->verifyVouchers($trans,$vanyval,$vdiffprice);
			$very&=$cart->verifyItems($trans);
			//verify vouchers are not paid with other vouchers (unless privilege exists)
			if(!$trans->havePrivilege(WTrCreateOrder::Priv_CanPayVoucherWithVoucher)){
				$very&=count($trans->getvouchers())==0 || count($cart->getvouchers())==0;
			}
		}
		//verification successful?
		if(!$very){
			//no: set verified cart and return to caller
			$cart->setstatus(WOCartOrder::Invalid);
			$trans->setcart($cart);
			return;
		}
		$cart->setstatus(WOCartOrder::Ok);
		//create order
		$ord=$cart->createOrderOnDB($trans,$isreserve,$issale);
		
		//return verified cart and order
		$trans->setorder($ord);
		$trans->setcart($cart);
		//send mail
		$ord->mailDetails();
	}
	
	/**helper function for createOrder: verifies customer settings*/
	private function verifyCustomer($trans)
	{
		//check customer exists
		$cust=WTcustomer::getFromDB($this->prop_customerid);
		if(!is_a($cust,"WTcustomer")){
			$this->prop_customerid=null;
			$this->prop_deliveryaddressid=null;
			$this->prop_invoiceaddressid=null;
			return false;
		}
		//check addresses belong to customer
		$ret=true;
		if($this->prop_invoiceaddressid!==null){
			$addr=WTaddress::getFromDB($this->prop_invoiceaddressid);
			if(!is_a($addr,"WTaddress")){
				$this->prop_invoiceaddressid=null;
				$ret=false;
			}else{
				if($addr->customerid != $this->prop_customerid){
					$this->prop_invoiceaddressid=null;
					$ret=false;
				}
			}
		}
		if($this->prop_deliveryaddressid!==null){
			$addr=WTaddress::getFromDB($this->prop_deliveryaddressid);
			if(!is_a($addr,"WTaddress")){
				$this->prop_deliveryaddressid=null;
				$ret=false;
			}else{
				if($addr->customerid != $this->prop_customerid){
					$this->prop_deliveryaddressid=null;
					$ret=false;
				}
			}
		}
		return $ret;
	}
	
	/**helper function for createOrder: verifies customer settings*/
	private function verifyShipping($trans)
	{
		if($this->prop_shippingtypeid !== null){
			$ship=WTshipping::getFromDB($this->prop_shippingtypeid);
			//check it exists
			if(!is_a($ship,"WTshipping")){
				$this->prop_shippingtypeid=null;
				return false;
			}
			//check we have the right
			global $session;
			if(!$session->checkFlags($ship->flags)){
				$this->prop_shippingtypeid=null;
				return false;
			}
		}
		return true;
	}
	
	/**helper for helper verifyTickets: gets statistics about sold tickets*/
	private function eventTicketStatistics($evid,$cap,&$price)
	{
		global $db;
		$ret=array("all"=>$cap);
		$price[$evid]=array();
		$epcs=WTeventprice::selectFromDB("eventid=".$db->escapeInt($evid));
		foreach($epcs as $pc){
			$ret[$pc->pricecategoryid]=$pc->maxavailable;
			$price[$evid][$pc->pricecategoryid]=$pc->price;
		}
		$ticks=WTticket::selectFromDB("eventid=".$db->escapeInt($evid));
		foreach($ticks as $t){
			if(($t->status & WTticket::MaskBlock)==0)continue;
			$ret["all"]--;
			if($t->pricecategoryid !== null)
				$ret[$t->pricecategoryid]--;
		}
		return $ret;
	}
	
	/**helper function for createOrder: verifies customer settings*/
	private function verifyTickets($trans,$salestop)
	{
		global $session;
		$ret=true;
		$now=time();
		$evplans=array();
		if($salestop>0)$now+=$salestop*3600;
		$coupon=new CouponVerifier($this->prop_couponid);
		foreach($this->prop_tickets as $key => &$tick){
			//assume ok
			$tick->setstatus(WOCartTicket::Ok);
			//check event exists
			$evid=$tick->geteventid();
			if(!isset($evplans[$evid]))
				$evplans[$evid]=new EventPlan($evid);
			if(!$evplans[$evid]->eventIsValid()){
				$tick->setstatus(WOCartTicket::Invalid);
				$ret=false;
				continue;
			}
			//try to add tickets
			$ret &= $evplans[$evid]->addTickets($key, $tick);
			//check sale time
			if($salestop==self::AfterSale)continue;
			if($salestop==self::LateSale){
				if($evplans[$evid]->eventHasEnded($now)){
					$tick->setstatus(WOCartTicket::TooLate);
					$ret=false;
				}
			}else{
				if($evplans[$evid]->eventHasStarted($now)){
					$tick->setstatus(WOCartTicket::TooLate);
					$ret=false;
				}
			}
		}
		foreach($evplans as $evid => $plan){
			//validate coupon
			$ret &= $coupon->verify($plan);
		}
		return $ret;
	}
	
	/**helper function for createOrder: verifies customer settings*/
	private function verifyVouchers($trans,$vanyval,$vdiffprice)
	{
		$okvp=array();
		foreach(explode(' ',$GLOBALS['db']->getConfig('ValidVouchers')) as $vp)
			$okvp[]=$vp+0;
		$ret=true;
		foreach($this->prop_vouchers as &$vou){
			//assume ok
			$vou->setstatus(WOCartVoucher::Ok);
			//check value
			$vv=$vou->getvalue()+0;
			if($vv<=0){
				$vou->setstatus(WOCartVoucher::InvalidValue);
				$ret=false;
				continue;
			}
			if(!$vanyval)
				if(!in_array($vv,$okvp)){
					$vou->setstatus(WOCartVoucher::InvalidValue);
					$ret=false;
					continue;
				}
			//check price
			if($vdiffprice){
				$vp=$vou->getprice()+0;
				if($vp<0){
					$vou->setstatus(WOCartVoucher::InvalidPrice);
					$ret=false;
					continue;
				}
			}else{
				//price to be ignored, so simply overwrite it
				$vou->setprice($vv);
			}
		}
		return $ret;
	}
	
	/**helper function for createOrder: verifies customer settings*/
	private function verifyItems($trans)
	{
		//TODO: implement items
		return true;
	}
	
	/**helper function for createOrder: creates the order on the database*/
	private function createOrderOnDB($trans,$isreserve,$issale)
	{
		global $session;
		//create order
		$ord=WTorder::newRow();
		$ord->customerid=$this->prop_customerid;
		$ord->invoiceaddress=$this->prop_invoiceaddressid;
		$ord->deliveryaddress=$this->prop_deliveryaddressid;
		$ord->soldby=$session->currentUserName();
		$ord->ordertime=time();
		$ord->comments=$this->prop_comment;
		$ord->amountpaid=0;
		$ord->couponid=$this->prop_couponid;
		$ord->shippingtype=$this->prop_shippingtypeid;
		$ship=WTshipping::getFromDB($this->prop_shippingtypeid);
		if(is_a($ship,"WTshipping"))
			$ord->shippingcosts=$ship->cost;
		if($isreserve)
			$ord->status=WTorder::Reserved;
		else
			$ord->status=WTorder::Placed;
		if(!$ord->insert()){
			$trans->abortWithError(tr("DB error while inserting order."));
			return;
		}
		//create tickets
		if($isreserve)
			$tstat=WTticket::Reserved;
		else
			$tstat=WTticket::Ordered;
		foreach($this->prop_tickets as $tick){
			$tck=WTticket::newRow();
			$tck->orderid=$ord->orderid;
			$tck->eventid=$tick->geteventid();
			$tck->price=$tick->getprice();
			$tck->pricecategoryid=$tick->getpricecategoryid();
			$tck->status=$tstat;
			for($i=0;$i<$tick->getamount();$i++)
				$tck->insert();
		}
		//create vouchers
		if(count($this->prop_vouchers)>0)
			$vtime=WOVoucher::calculateInitialValidity();
		foreach($this->prop_vouchers as $vou){
			$v=WTvoucher::newRow();
			$v->orderid=$ord->orderid;
			$v->price=$vou->getprice();
			$v->value=$vou->getvalue();
			$v->validtime=$vtime;
			$v->isused=false;
			for($i=0;$i<$vou->getamount();$i++)
				$v->insert();
		}
		//TODO: create items
		//for sales: update amountpaid and mark shipped
		if($issale){
			$oo=WOOrder::fromTableorder($ord);
			$ord->amountpaid=$oo->getTotalPrice();
			$ord->senttime=$ord->ordertime;
			$ord->status=WTorder::Sold;
			$ord->update();
		}
		//update address use
		$ad=WTaddress::getFromDB($ord->invoiceaddress);
		if(is_a($ad,"WTaddress")){
			$ad->lastused=time();
			$ad->update();
		}
		$ad=WTaddress::getFromDB($ord->deliveryaddress);
		if(is_a($ad,"WTaddress")){
			$ad->lastused=time();
			$ad->update();
		}
		//return created order
		return WOOrder::fromTableorder($ord);
	}
};


//eof
return;
?>
