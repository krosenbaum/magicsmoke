<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOFlag extends WOFlagAbstract
{
	/**GetValidFlags transaction*/
	public static function getAllTrn($trans)
	{
		$trans->setflags(WOFlag::getAll());
	}
	/**returns all existing flags*/
	public static function getAll()
	{
		global $db;
		$cfgs=WTconfig::selectFromDb("ckey LIKE 'Flag %'");
		$ret=array();
		foreach($cfgs as $cfg){
			$k=explode(" ",$cfg->ckey);
			if(count($k)==2 && $k[0]=="Flag"){
				$f=new WOFlag;
				$f->setflag($k[1]);
				$f->setdescription($cfg->cval);
				$ret[]=$f;
			}
		}
		return $ret;
	}
		
	/**SetFlag transaction*/
	public static function setFlagTrn($trans)
	{
		global $db;
		$f=$trans->getflag();
		//check flag name
		if(!preg_match("/^[a-zA-Z0-9]+$/",$f->getflag())){
			$trans->abortWithError("Invalid flag name.");
			return;
		}
		//set it
		$db->setConfig("Flag ".$f->getflag(),$f->getdescription());
	}
	/**DeleteFlag transaction*/
	public static function deleteFlag($trans)
	{
		global $db;
		$f=$trans->getflag();
		//check flag name
		if(!preg_match("/^[a-zA-Z0-9]+$/",$f)){
			$trans->abortWithError("Invalid flag name or attempt to delete special flag.");
			return;
		}
		//set it
		$cfg=WTconfig::getFromDb("Flag ".$f);
		if($cfg instanceof WTconfig)
			$cfg->deleteFromDb();
	}
};


//eof
return;
?>
