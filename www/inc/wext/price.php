<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOPriceCategory extends WOPriceCategoryAbstract
{
	/**called from CreatePriceCategory transaction*/
	static public function createCategory($trans)
	{
		$cat=$trans->getpricecategory();
		if(!is_a($cat,"WOPriceCategory")){
			$trans->abortWithError(tr("New Category must be a valid object."));
			return;
		}
		//TODO: verify that name and abbrev. are unique
		//convert
		$tab=WTpricecategory::newRow();
		$cat->toTablepricecategory($tab);
		$tab->revert("pricecategoryid");
		$tab->insert();
		//return
		$trans->setpricecategory(WOPriceCategory::fromTablepricecategory($tab));
	}
	/**called from ChangePriceCategory transaction*/
	static public function changeCategory($trans)
	{
		$cat=$trans->getpricecategory();
		if(!is_a($cat,"WOPriceCategory")){
			$trans->abortWithError(tr("Category must be a valid object."));
			return;
		}
		//TODO: verify that name and abbrev. are unique
		//convert
		$tab=WTpricecategory::getFromDB($cat->getpricecategoryid());
		if($tab===false){
			$trans->abortWithError(tr("Category does not exist."));
			return;
		}
		$cat->toTablepricecategory($tab);
		$tab->update();
		//return
		$trans->setpricecategory(WOPriceCategory::fromTablepricecategory($tab));
	}
};


//eof
return;
?>
