<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOTicket extends WOTicketAbstract
{
	/**returns the amount that is to be paid for this ticket:
	if it is to be paid it is equal to the price, otherwise zero*/
	public function amountToPay()
	{
		if($this->prop_status & self::MaskPay)
			return $this->prop_price;
		else
			return 0;
	}

	///returns true if the ticket actually blocks a seat (reserved, ordered, in use, etc.)
	public function isBlocking()
	{
		return ($this->prop_status & self::MaskBlock) != 0;
	}

	/**called from the UseTicket transaction*/
	public static function useTicket($trans)
	{
		global $db;
		//create result object
		$ret=new WOTicketUse;
		//check event
		$evid=$trans->geteventid();
		$evt=WTevent::getFromDB($evid);
		if(!is_a($evt,"WTevent")){
			$ret->setusestatus(WOTicketUse::InvalidEvent);
			$trans->ticketuse($ret);
			return;
		}
		//find ticket itself
		$tick=WTticket::getFromDB($trans->getticketid());
		if(!is_a($tick,"WTticket")){
			$ret->setusestatus(WOTicketUse::NotFound);
		}else{
			if($tick->eventid != $evid){
				$ret->setusestatus(WOTicketUse::WrongEvent);
			}else
			if($tick->status == WTticket::Ordered){
				//check order pay status
				$ord=WOOrder::fromTableorder(WTorder::getFromDB($tick->orderid));
				if($ord->getamountdue()>0){
					$ret->setusestatus(WOTicketUse::Unpaid);
				}else{
					//ok, update ticket
					$ret->setusestatus(WOTicketUse::Ok);
					$tick->status = WTticket::Used;
					$tick->update();
				}
			}else
			if($tick->status == WTticket::Used){
				$ret->setusestatus(WOTicketUse::AlreadyUsed);
			}else{
				$ret->setusestatus(WOTicketUse::NotUsable);
			}
			$ret->setticket(WOTicket::fromTableticket($tick));
		}
		//get event statistics data
		$ticks=WTticket::selectFromDB("eventid=".$db->escapeInt($evid));
		$am=0;
		$amus=0;
		$amop=0;
		$amrs=0;
		foreach($ticks as $tick){
			if(($tick->status & WTticket::MaskUsable)>0)$am++;
			if($tick->status == WTticket::Ordered)$amop++;
			if($tick->status == WTticket::Used)$amus++;
			if($tick->status == WTticket::Reserved)$amrs++;
		}
		$ret->setamounttickets($am);
		$ret->setamountused($amus);
		$ret->setamountopen($amop);
		$ret->setamountreserved($amrs);
		//return
		$trans->setticketuse($ret);
	}
	
	///helper to get the event data in templates
	public function getevent()
	{
		return WOEvent::fromTableevent(WTevent::getFromDB($this->geteventid()));
	}
	
	///GetTicketAudit transaction
	public static function GetTicketAuditTransaction($trans)
	{
		global $db;
		//get ticket data
		$tab=WTticket_audit::selectFromDB('ticketid='.$db->escapeString($trans->getticketid()));
		$arr=WOTicketAudit::fromTableArrayticket_audit($tab);
		$trans->setticket($arr);
		//get associated event(s)
		$evl=array();
		foreach($tab as $t)
			if(!in_array($t->eventid,$evl))
				$evl[]=$t->eventid;
		$trans->setevent(WOEvent::fromTableArrayevent(WTevent::selectFromDB("eventid IN ".$db->escapeIntList($evl))));
	}
};

class WOTicketAudit extends WOTicketAuditAbstract
{
	//helper for DB mapper
	protected function geteventnamefromdb()
	{
		$ev=WTevent::getFromDB($this->prop_eventid);
		$this->prop_eventstart=$ev->starttime;
		return $ev->title;
	}
};


//eof
return;
?>
