<?php
// (c) Konrad Rosenbaum, 2007-2021
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOCustomer extends WOCustomerAbstract
{
	/**called from CreateCustomer transaction*/
	public static function createCustomer($trans)
	{
		global $db;
		$cc=$trans->getcustomer();
		if(!is_a($cc,"WOCustomer")){
			$trans->abortWithError(tr("Not a valid customer object."));
			return;
		}
		//create customer
		$ct=WTcustomer::newRow();
		$cc->toTablecustomer($ct);
		$ct->revert("customerid");
		$ct->insert();
		$cc->setid($ct->customerid);
		//create addresses
		foreach($cc->getaddresses() as $addr){
			//skip addresses that are deleted
			if($addr->getisdeleted())continue;
			$at=WTaddress::newRow();
			$addr->toTableaddress($at);
			$at->revert("addressid");
			$at->customerid=$cc->getid();
			if($at->insert()===false){
				$trans->abortWithError("Database Error: ".$db->lastError());
				return;
			}
// 			print_r($at);
		}
		//create contacts
		foreach($cc->getcontacts() as $cont){
			$cn=WTcontact::newRow();
			$cont->toTablecontact($cn);
			$cn->revert("contactid");
			$cn->customerid=$cc->getid();
			$cn->insert();
		}

		//return
		$trans->setcustomer(WOCustomer::fromTablecustomer($ct));
	}

	/**called from ChangeCustomer transaction*/
	public static function changeCustomer($trans)
	{
		global $db;
		$cc=$trans->getcustomer();
		if(!is_a($cc,"WOCustomer")){
			$trans->abortWithError(tr("Not a valid customer object."));
			return;
		}
		$ct=WTcustomer::getFromDB($cc->getid());
		if(!is_a($ct,"WTcustomer")){
			$trans->abortWithError(tr("Customer does not exist in the database."));
			return;
		}
		//create customer
		$cc->toTablecustomer($ct);
		$ct->revert("passwd");
		$ct->revert("email");
		$ct->revert("rstcode");
		$ct->revert("rsttill");
		if($ct->isChanged())$ct->update();
		//sync addresses
		foreach($cc->getaddresses() as $addr){
			$at=WTaddress::getFromDB($addr->getaddressid());
			//new address?
			$na=false;
			if(!is_a($at,"WTaddress")){
				$na=true;
				$at=WTaddress::newRow();
			}
			if($at->customerid != $cc->getid())$na=true;
			//push data
			$addr->toTableaddress($at);
			$at->revert("customerid");
			$at->revert("addressid");
			if($na){
				//its new, force insert
				$at->addressid=null;
				$at->isdeleted=0;
				$at->customerid=$cc->getid();
				if($at->insert()===false){
					$trans->abortWithError("Database Error: ".$db->lastError());
					return;
				}
			}else{
				//its known
				if($at->isChanged())$at->update();
			}
		}
		//sync contacts
		$cnlist=array();
		foreach($cc->getcontacts() as $cont){
			$cn=WTcontact::getFromDB($cont->getcontactid());
			//new one?
			$nc=false;
			if(!is_a($cn,"WTcontact")){
				$cn=WTcontact::newRow();
				$nc=true;
			}
			if($cn->customerid != $cc->getid())$nc=true;
			//push data
			$cont->toTablecontact($cn);
			$cn->revert("contactid");
			$cn->revert("customerid");
			if($nc){
				//new one, insert
				$cn->customerid=$cc->getid();
				$cn->insert();
			}else{
				//known one, update
				if($cn->isChanged())$cn->update();
			}
			//remember
			$cnlist[]=$cn->contactid;
		}
		//remove deleted contacts
		$cns=WTcontact::selectFromDB("customerid=".$db->escapeInt($cc->getid()));
		foreach($cns as $cn){
			if(!in_array($cn->contactid,$cnlist))
				$cn->deleteFromDB();
		}

		//return
		$trans->setcustomer(WOCustomer::fromTablecustomer($ct));
	}

	/**called from DeleteCustomer transaction*/
	public static function deleteCustomer($trans)
	{
		//get customer table objects
		$dc=WTcustomer::getFromDB($trans->getcustomerid());
		$mc=WTcustomer::getFromDB($trans->getmergewithid());
		if(!is_a($dc,"WTcustomer")){
			$trans->abortWithError(tr("Not a valid customer, cannot delete."));
			return;
		}
		$domerge=is_a($mc,"WTcustomer");
		//delete any left over sessions
		global $db;
		$db->deleteRows("websession","customerid=".$db->escapeInt($dc->customerid));
		//collect owned objects
		$addr=WTaddress::selectFromDB("customerid=".$db->escapeInt($dc->customerid));
		$cont=WTcontact::selectFromDB("customerid=".$db->escapeInt($dc->customerid));
		$ordr=WTorder::selectFromDB("customerid=".$db->escapeInt($dc->customerid));
		//if not merge
		if(!$domerge){
			//sanity check
			if(count($ordr)>0){
				$trans->abortWithError(tr("This customer has orders in the system, cannot delete."));
				return;
			}
			//delete sub-objects
			foreach($addr as $ad)
				$ad->deleteFromDB();
			foreach($cont as $ct)
				$ct->deleteFromDB();
			//delete
			$dc->deleteFromDB();
		}else{//if merge...
			//sanity check
			if($dc->customerid == $mc->customerid){
				$trans->abortWithError(tr("Cannot merge a customer with itself."));
				return;
			}
			//transfer sub-objects
			foreach($ordr as $od){
				$od->customerid=$mc->customerid;
				$od->update();
			}
			foreach($cont as $od){
				$od->customerid=$mc->customerid;
				$od->update();
			}
			foreach($addr as $od){
				$od->customerid=$mc->customerid;
				$od->update();
			}
			//delete
			$dc->deleteFromDB();
			//return substitute
			$trans->setcustomer(WOCustomer::fromTablecustomer($mc));
		}
		print($db->lastError());
	}

	/**called from CreateCountry transaction*/
	public static function createCountry($trans)
	{
		$id=trim($trans->getabbrev());
		$nm=trim($trans->getname());
		if($id=="" || $nm==""){
			$trans->abortWithError(tr("Country ID and name must contain values!"));
			return;
		}
		//check for existence
		global $db;
		$res=$db->select("country","*","countryid=".$db->escapeString($id));
		if(count($res)>0){
			$trans->abortWithError(tr("Country ID exists!"));
			return;
		}
		$res=$db->select("country","*","countryname=".$db->escapeString($nm));
		if(count($res)>0){
			$trans->abortWithError(tr("Country name exists!"));
			return;
		}
		//create
		$tab=WTcountry::newRow();
		$tab->countryid=$id;
		$tab->countryname=$nm;
		$tab->insert();
		//return
		$trans->setcountry(WOCountry::fromTablecountry($tab));
	}

	/**called from CreateContactType transaction*/
	public static function createContactType($trans)
	{
		global $db;
		$cname=trim($trans->gettypename());
		if($cname==""){
			$trans->abortWithError(tr("Contact Type names must not be empty!"));
			return;
		}
		$tab=$db->select("contacttype","*","contacttype=".$db->escapeString($cname));
		if(count($tab)>0){
			$trans->abortWithError(tr("Contact Type already exists."));
			return;
		}
		//create
		$tab=WTcontacttype::newRow();
		$tab->contacttype=$cname;
		$tab->uriprefix=trim($trans->geturiprefix());
		$tab->insert();
		//return
		$trans->setcontacttype(WOContactType::fromTablecontacttype($tab));
	}

	/**called from ChangeCustomerMail transaction*/
	public static function changeCustomerMail($trans)
	{
		global $db;
		$cc=$trans->getcustomerid();
		$ct=WTcustomer::getFromDB($cc);
		if(!is_a($ct,"WTcustomer")){
			$trans->abortWithError(tr("Customer does not exist in the database."));
			return;
		}
		//try to set new mail
		$ct->email=$trans->getemail();
		if(!$ct->update()){
			$trans->abortWithError(tr("This email already exists in the database."));
			return;
		}
		//set return value
		$trans->setcustomer(WOCustomer::fromTablecustomer($ct));
	}

	///reset customer password request - internal function used by resetPassword and resetPasswordPage
	private function resetPasswordReq($ct)
	{
		//check for customer
		if(!isEmail($ct->email)){
            error_log("not a mail ".$ct->email);
			return "nomail";
		}
		//set reset parameters
		global $CustomerResetTime,$MailFrom;
		$ct->rstcode=getRandom(160);
		$ct->rsttill=time()+$CustomerResetTime;
		$ct->update();
		//init twig (init is intelligent and will not double initialize)
		BaseVars::initTwig();
		BaseVars::init();
		//gather basics and render mail
		global $twig,$basevars;
		$p=$twig->loadTemplate("resetlogin.txt");
		$list['customer']=$this;
		$list['resetUrl']=$basevars['script']['customerResetLogin']
			."&customer=".urlencode($ct->email)
			."&passcode=".$ct->rstcode;
		$list['passcode']=$ct->rstcode;
		$page=explode("\n",trim($p->render($list)));
		//parse mail
		if(count($page)<2)return "template";
		$subject=array_shift($page);
		$mode=0;
		$mailtext="";$mailheader="";
		foreach($page as $line){
			if($mode==0){
				if(trim($line)=="")$mode=1;
				else $mailheader.=$line."\n";
			}else{
				$mailtext.=$line."\n";
			}
		}
		//send mail
		global $IsDemoSystem,$DemoMailAddr;
		if(isset($IsDemoSystem) && $IsDemoSystem==true && isset($DemoMailAddr))
			$tmail=$DemoMailAddr;
		else
			$tmail=$ct->email;
		mail($tmail,$subject,$mailtext,$mailheader);
		return "ok";
	}

	///the ResetCustomerPassword transaction
	public static function resetPassword($trans)
	{
		//check for customer
		$ct=WTcustomer::getFromDB($trans->getcustomerid());
		if(!is_a($ct,"WTcustomer")){
			$trans->abortWithError(tr("Not a valid customer."));
			return;
		}
		$ret=WOCustomer::fromTablecustomer($ct)->resetPasswordReq($ct);
		if($ret=="nomail"){
			$trans->abortWithError(tr("Customer has no email address."));
			return;
		}
		if($ret=="template"){
            $trans->abortWithError(tr("Mail template Error."));
            return;
        }
	}

	///reset customer password request web page
	public static function resetPasswordPage($next)
	{
		global $HTTPARGS,$db,$basevars,$twig;
		$vars=$basevars;
		//find customer
		$nameidx=$basevars['inputnames']['login']['name'];
		if(!isset($HTTPARGS[$nameidx])){
			return self::loginError("param");
		}
		$ct=WTcustomer::selectFromDB("email=".$db->escapeString($HTTPARGS[$nameidx]));
		if(count($ct)<1){
			return self::loginError("login");
		}
		//send mail
		$co=WOCustomer::fromTablecustomer($ct[0]);
		$ret=$co->resetPasswordReq($ct[0]);
		$vars['mail']=$HTTPARGS[$nameidx];
		$vars['status']=$ret;
		$vars['nextUrl']=$basevars['script'][$next];
		//render
		$p=$twig->loadTemplate('resetloginrequest.html');
		$vars['customer']=$co;
		return $p->render($vars);
	}

	/**page shown to the customer after clicking the password reset link above*/
	static public function resetLoginPage()
	{
		global $HTTPARGS,$twig,$basevars,$db;
		$vars=$basevars;
		//get customer and reset code
		if(!isset($HTTPARGS['customer']) || !isset($HTTPARGS['passcode'])){
			$p=$twig->loadTemplate('reseterror.html');
			$vars['errorText']=tr("There is a problem with your mail client or your browser: please copy the URL into the browser manually and try again.");
			$vars['errorType']='url';
			return $p->render($vars);
		}
		//get customer
		$cts=WTcustomer::selectFromDB("email=".$db->escapeString($HTTPARGS['customer']));
		if(count($cts)<1){
			$p=$twig->loadTemplate('reseterror.html');
			$vars['errorText']=tr("This user does not exist.");
			$vars['errorType']='nonexist';
			return $p->render($vars);
		}
		$ct=$cts[0];
		//check passcode validity
		if($db->isNull($ct->rsttill) || $db->isNull($ct->rstcode) ||
		   $ct->rstcode!=$HTTPARGS['passcode'] || $ct->rsttill < time()){
			$p=$twig->loadTemplate('reseterror.html');
			$vars['errorText']=tr("The passcode supplied by your browser is either invalid or expired.");
			$vars['errorType']='invalid';
			return $p->render($vars);
		}
		//check for change request
		$p1=$basevars['inputnames']['login']['passwd'];
		$p2=$basevars['inputnames']['login']['passwdrepeat'];
		if(isset($HTTPARGS[$p1]) && isset($HTTPARGS[$p2])){
			if($HTTPARGS[$p1] != $HTTPARGS[$p2] || $HTTPARGS[$p1] == ""){
				$p=$twig->loadTemplate('reseterror.html');
				$vars['errorText']=tr("The password does not match or is empty, please use the back button of your browser and try again.");
				$vars['errorType']='mismatch';
				return $p->render($vars);
			}
			//set password
			$ct->passwd=Session::passwdHash($HTTPARGS[$p1]);
			$ct->rsttill=null;
			$ct->rstcode=null;
			$ct->update();
			$vars['passwdchanged']=true;
		}else $vars['passwdchanged']=false;
		//render page
		$p=$twig->loadTemplate('resetlogin.html');
		$vars['customer']=WOCustomer::fromTablecustomer($ct);
		$vars['customer_name']=$HTTPARGS['customer'];
		$vars['passcode']=$HTTPARGS['passcode'];
		return $p->render($vars);
	}

	/** creates a login page

	For templating info see \ref tpl_login Login Variables
	and \ref tpl_base Base Variables.
	*/
	static public function loginPage($extension)
	{
		global $twig,$basevars,$HTTPARGS,$db;
		//get cart id and check it
		$cartid=WebCart::getCart();
		if($cartid==""){
			$p=$twig->loadTemplate("carterror.html");
			return $p->render($basevars);
		}

		//cart is ok, now get the object
		$cart = WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
		if(!is_a($cart,"WOWebCart")){
			//ooops. internal problem
			$p=$twig->loadTemplate("carterror.html");
			return $p->render($basevars);
		}
		$list=$basevars;
		$list["cart"]=$cart;
		$list["customer_name"]="";
		$list['script']['customerLogin']=$basevars['script']['customerLogin'.$extension];
		$list['script']['customerReset']=$basevars['script']['customerReset'.$extension];
		$list['script']['customerRegistration']=$basevars['script']['customerRegistration'.$extension];
		$list["customer_name"]="";
		if(isset($HTTPARGS["customer_name"]))
			$list["customer_name"]=$HTTPARGS["customer_name"];
		else{
			//get cart id and check it
			$cartid=WebCart::getCart();
			if($cartid!=""){
				//cart is ok, now get the object
				$cart = WTcart::getFromDB($cartid);
				$custid=$cart->customerid;
				if(!$db->isNull($custid)){
					$cust=WTcustomer::getFromDB($custid);
					$list["customer_name"]=$cust->email;
				}
			}else{
				//get session and check it
				$sess=WOWebSession::getOrCreateWebSession();
				$cust=$sess->getcustomer();
				if(is_a("WOCustomer",$cust))
					$list["customer_name"]=$cust->getemail();
			}
		}
		$list['countries']=WOCountry::fromTableArraycountry(WTcountry::selectFromDB("","ORDER BY countryname"));
		//display
		$p=$twig->loadTemplate("login.html");
		return $p->render($list);
	}


	///renders a login error page, see \ref tpl_logerr Login Error Template
	static private function loginError($errorType)
	{
		global $basevars,$HTTPARGS,$SERVER,$twig;
		//find some basics
		$vars=$basevars;
		$vars["errorType"]=$errorType;
		//construct URL back to login page
		if(isset($_SERVER["HTTP_REFERER"])){
			$vars["backUrl"]=$_SERVER["HTTP_REFERER"];
		}else{
			//not really helpful, but nice for a fallback
			$vars["backUrl"]=$basevars['script']['root'];
		}
		if(strstr($vars["backUrl"],"?")===false)
			$vars["backUrl"].="?";
		else
			$vars["backUrl"].="&";
		$idx=$basevars['inputnames']['login']['name'];
		$vars["backUrl"].=$idx."=";
		if(isset($HTTPARGS[$idx]))$vars["backUrl"].=$HTTPARGS[$idx];
		//render page
		$p=$twig->loadTemplate("loginerror.html");
		return $p->render($vars);
	}

	///check the login of the customer, either forwards to the checkout page or shows the loginerror page
	static public function checkLogin($next,$storeincart)
	{
		global $HTTPARGS,$db,$basevars;
		//find customer
		$nameidx=$basevars['inputnames']['login']['name'];
		$passidx=$basevars['inputnames']['login']['passwd'];
		if(!isset($HTTPARGS[$nameidx]) || !isset($HTTPARGS[$passidx])){
			return self::loginError("param");
		}
		$ct=WTcustomer::selectFromDB("email=".$db->escapeString($HTTPARGS[$nameidx]));
		if(count($ct)<1){
			return self::loginError("login");
		}
		//check password
		$ct=$ct[0];
		if(!Session::passwdVerify($HTTPARGS[$passidx],$ct->passwd)){
			return self::loginError("login");
		}
		//store in cart
		if($storeincart){
			//get cart
			$cartid=WebCart::getCart();
			if($cartid==""){
				$p=$twig->loadTemplate("carterror.html");
				return $p->render($basevars);
			}
			//get customer data
			$cart=WTcart::getFromDB($cartid);
			$cart->customerid=$ct->customerid;
			$cart->update();
			//get address if there is none yet
			if($cart->deliveryaddress==false){
				$addrs=WTaddress::selectFromDB("customerid=".$ct->customerid,"ORDER BY lastused DESC");
				if(count($addrs)>0){
					$cart->deliveryaddress=$addrs[0]->addressid;
					$cart->update();
				}
			}
		}
		//store in session
		$sess=WOWebSession::getOrCreateWebSession();
		$st=WTwebsession::getFromDB($sess->getsessionid());
		$st->customerid=$ct->customerid;
		$st->update();
		//go to checkout page
		redirectHome(array("mode"=>$next));
	}

	///attempts to register a customer: if ok it creates a new customer object and redirects to the createaddress page, otherwise it shows the loginerror page
	static public function registerCustomer()
	{
		global $HTTPARGS,$db,$basevars,$twig;
		//check whether params are there
		$nameidx=$basevars['inputnames']['login']['name'];
		$passidx=$basevars['inputnames']['login']['passwd'];
		$passidx2=$basevars['inputnames']['login']['passwdrepeat'];
		if(!isset($HTTPARGS[$nameidx]) || !isset($HTTPARGS[$passidx]) || !isset($HTTPARGS[$passidx2]))
			return self::loginError("create");
		//check whether the chosen email is valid and does not exist yet
		$mail=trim($HTTPARGS[$nameidx]);
		if(!isEmail($mail))
			return self::loginError("invalid");
		$ct=WTcustomer::selectFromDB("email=".$db->escapeString($mail));
		if(count($ct)>0)
			return self::loginError("exist");
		//check the password
		$pass=$HTTPARGS[$passidx];$pass2=$HTTPARGS[$passidx2];
		if($pass != $pass2 || $pass=="")return self::loginError("mismatch");
		//create customer
		$ct=WTcustomer::newRow();
		$ct->email=$mail;
		$ct->passwd=Session::passwdHash($pass);
		if(isset($HTTPARGS[$basevars['inputnames']['login']['firstname']]))
			$ct->firstname=$HTTPARGS[$basevars['inputnames']['login']['firstname']];
		if(isset($HTTPARGS[$basevars['inputnames']['login']['lastname']]))
			$ct->name=$HTTPARGS[$basevars['inputnames']['login']['lastname']];
		if(isset($HTTPARGS[$basevars['inputnames']['login']['title']]))
			$ct->title=$HTTPARGS[$basevars['inputnames']['login']['title']];
		if(!$ct->insert())
			return self::loginError("create");
		//create address
		$addr=WTaddress::newRow();
		$addr->customerid=$ct->customerid;
		$addr->lastused=time();
		if(isset($HTTPARGS[$basevars['inputnames']['address']['addr1']]))
			$addr->addr1=$HTTPARGS[$basevars['inputnames']['address']['addr1']];
		if(isset($HTTPARGS[$basevars['inputnames']['address']['addr2']]))
			$addr->addr2=$HTTPARGS[$basevars['inputnames']['address']['addr2']];
		if(isset($HTTPARGS[$basevars['inputnames']['address']['city']]))
			$addr->city=$HTTPARGS[$basevars['inputnames']['address']['city']];
		if(isset($HTTPARGS[$basevars['inputnames']['address']['zip']]))
			$addr->zipcode=$HTTPARGS[$basevars['inputnames']['address']['zip']];
		if(isset($HTTPARGS[$basevars['inputnames']['address']['state']]))
			$addr->state=$HTTPARGS[$basevars['inputnames']['address']['state']];
		$addr->insert();
		//attempt to set countryid (done separately since it is a foreign key and not quite as essential
		if(isset($HTTPARGS[$basevars['inputnames']['address']['country']]))
			$addr->countryid=$HTTPARGS[$basevars['inputnames']['address']['country']];
		$addr->update();
		//log into cart
		$cartid=WebCart::getCart();
		if($cartid==""){
			$p=$twig->loadTemplate("carterror.html");
			return $p->render($basevars);
		}
		//get customer data
		$cart=WTcart::getFromDB($cartid);
		$cart->customerid=$ct->customerid;
		$cart->update();
		//get address if there is none yet
		if($cart->deliveryaddress==false){
			$addrs=WTaddress::selectFromDB("customerid=".$ct->customerid,"ORDER BY lastused DESC");
			if(count($addrs)>0){
				$cart->deliveryaddress=$addrs[0]->addressid;
				$cart->update();
			}
		}
		//store in session
		$sess=WOWebSession::getOrCreateWebSession();
		$st=WTwebsession::getFromDB($sess->getsessionid());
		$st->customerid=$ct->customerid;
		$st->update();
		//go to checkout page
		redirectHome(array("mode"=>"checkout"));
	}

	///callback for the GetCreateCustomerHints transaction
	static public function createHints($trans)
	{
		$trans->setcontacttypes( WOContactType::fromTableArraycontacttype( WTcontacttype::selectFromDB()));
		$trans->setcountries( WOCountry::fromTableArraycountry( WTcountry::selectFromDB()));
		global $db;
		$tt=$db->select("customer","title","","GROUP BY title ORDER BY title");
		if($tt!==false){
			$r=array();
			foreach($tt as $t)$r[]=$t["title"];
			$trans->settitles($r);
		}
		$csl=$db->select("address","state,city","","GROUP BY city,state");
		if($csl!==false){
			$rs=array();
			$rc=array();
			foreach($csl as $cs){
				if(!in_array($cs["city"],$rc))$rc[]=$cs["city"];
				if(!in_array($cs["state"],$rs))$rs[]=$cs["state"];
			}
			$trans->setcities($rc);
			$trans->setstates($rs);
		}
	}

	///callback for the SendCustomerMail transaction
	static public function sendMail($trans)
	{
		//get customer basics
		$ct=WTcustomer::getFromDB($trans->getcustomerid());
		if(!is_a($ct,"WTcustomer")){
			return;
		}
		if(!isEmail($ct->email)){
			return;
		}
		//parse mail
		$page=explode("\n",trim($trans->getcontent()));
		if(count($page)<2)return;
		$subject=array_shift($page);
		$mode=0;
		$mailtext="";$mailheader="";
		foreach($page as $line){
			if($mode==0){
				if(trim($line)=="")$mode=1;
				else $mailheader.=$line."\n";
			}else{
				$mailtext.=$line."\n";
			}
		}
		//send mail
		mail($ct->email,$subject,$mailtext,$mailheader);

	}
};

//eof
return;
?>
