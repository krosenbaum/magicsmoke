<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


/**this class extends the definition of the Wob class ServerFormat, to actually do something.
It is used on the other side by the client to configure LocalFormat and by the Web UI to configure LanguageManager.*/
class WOServerFormat extends WOServerFormatAbstract
{
	/**constructs the format object for a specific language*/
	public function __construct($lang="")
	{
		parent::__construct();
		global $template;
		$this->loadConfig($template."/format.cfg");
// 		echo "Loading base while ".$lang."<br/>";
		if($lang!="" && $lang!="C" && preg_match('/^[a-zA-Z0-9_-]+$/',$lang)){
// 			echo "Loading ".$lang."<br/>";
			$this->loadConfig($template."/".$lang."/format.cfg");
		}else{
                        global $default_language;
			//fall back to default language
// 			echo "Loading C as fallback<br/>";
			$this->loadConfig($template."/".$default_language."/format.cfg");
		}
		//check timezone
		$tz=$this->gettimezone();
		if($tz==null || $tz==false || $tz==""){
			$this->settimezone(date_default_timezone_get());
		}else{
			if(@timezone_open($tz)===false)
				$this->settimezone(date_default_timezone_get());
		}
	}

	/**load a configuration file and amend internal state by its definitions*/
	public function loadConfig($file)
	{
		//ignore if fiel not found
		if(!file_exists($file))return;
		//read file
		$lines=file($file);
		foreach($lines as $num=>$ln){
			//trim line and remove comments
			$ln=trim($ln);
			if($ln=="")continue;
			if(substr($ln,0,1)=="#")continue;
			//parse
			$match=array();
			if(preg_match("/^([a-zA-Z]+) \"(.*)\"$/",$ln,$match)<=0)
				die("Syntax error in format config $file line ".($num+1));
			$cmd=strtolower($match[1]);
			$val=stripcslashes($match[2]);
			switch($cmd){
				case "weekdays":$this->setweekdays(explode(" ",$val));break;
				case "shortweekdays":$this->setshortweekdays(explode(" ",$val));break;
				case "months":$this->setmonths(explode(" ",$val));break;
				case "shortmonths":$this->setshortmonths(explode(" ",$val));break;
				case "date":$this->setdateformat($val);break;
				case "time":$this->settimeformat($val);break;
				case "datetime":$this->setdatetimeformat($val);break;
				case "amtext":$this->setamtext($val);break;
				case "pmtext":$this->setpmtext($val);break;
				case "decimaldot":$this->setdecimaldot($val);break;
				case "thousandseparator":$this->setthousandseparator($val);break;
				case "thousanddigits":$this->setthousanddigits($val);break;
				case "moneydecimals":$this->setmoneydecimals($val);break;
				case "currencysymbol":$this->setcurrencysymbol($val);break;
				case "currencysymbolplain":$this->setcurrencysymbolplain($val);break;
				case "currencysymbolhtml":$this->setcurrencysymbolhtml($val);break;
				case "currencysymbolpos":$this->setcurrencysymbolpos($val);break;
				case "moneynegative":$this->setmoneynegative($val);break;
				case "moneypositive":$this->setmoneypositive($val);break;
				case "moneynegativepos":
					$this->setmoneynegativepos($this->mps2e($val));break;
				case "moneypositivepos":
					$this->setmoneypositivepos($this->mps2e($val));break;
				case "timezone":$this->settimezone($val);break;
				default: die("Unknown keyword in format config $file line ".($num+1));break;
			}
		}
	}
	
	///helper function to convert string money sign position to enum
	protected function mps2e($v)
	{
		switch(strtolower(trim($v))){
			case "none":return self::NoSign;
			case "beforenum":return self::SignBeforeNum;
			case "afternum":return self::SignAfterNum;
			case "beforesym":return self::SignBeforeSym;
			case "aftersym":return self::SignAfterSym;
			case "paren":return self::SignParen;
			//fallback
			default:return self::SignBeforeNum;
		}
	}
}


//eof
return;
?>
