<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOTemplate extends WOTemplateAbstract
{
	/**called by the TemplateList transaction*/
	static public function getList($trans)
	{
		$trans->settemplates(WOTemplateInfo::fromTableArraytemplate(WTtemplate::selectFromDB()));
	}
	
	/**called by the GetTemplate transaction*/
	static public function getFile($trans)
	{
		//TODO: filter by flags
		$trans->settemplatefile(WOTemplate::fromTabletemplate(WTtemplate::getFromDB($trans->getfilename())));
	}
	
	/**called by the SetTemplate transaction*/
	static public function setFile($trans)
	{
		//find it
		$tm=WTtemplate::getFromDB($trans->getfilename());
		if($tm===false)
			$tm=WTtemplate::newRow();
		//set data
		$tm->filename=$trans->getfilename();
		$tm->description=$trans->getdescription();
		$tm->content=$trans->gettemplatedata();
		$tm->hash=md5($trans->gettemplatedata());
		$tm->dolog=false;
		$tm->flags="";
		//update
		$tm->insertOrUpdate();
		//return
		$trans->settemplatefile(WOTemplate::fromTabletemplate($tm));
	}
	/**called by the SetTemplateDescription transaction*/
	static public function setTemplateDescription($trans)
	{
		//find it
		$tm=WTtemplate::getFromDB($trans->getfilename());
		if($tm===false){
			$trans->abortWithError(tr("No such template."));
			return;
		}
		//overwrite
		$tm->description=$trans->getdescription();
		$tm->update();
	}
	/**called by the DeleteTemplate transaction*/
	static public function deleteTemplate($trans)
	{
		//find it
		$tm=WTtemplate::getFromDB($trans->getfilename());
		if($tm===false){
			$trans->abortWithError(tr("No such template."));
			return;
		}
		//delete
		$tm->deleteFromDb();
	}
	
	///called by the SetTemplateFlags transaction
	static public function changeFlags($trans)
	{
		//find it
		$tm=WTtemplate::getFromDB($trans->getfilename());
		if($tm===false){
			$trans->abortWithError(tr("No such template."));
			return;
		}
		//set
		$tm->flags=$trans->getflags();
		//delete
		if(!$tm->update())
			$trans->abortWithError(tr("Unable to set flags - DB error."));
	}
};


//eof
return;
?>
