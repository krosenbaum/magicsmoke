<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WORole extends WORoleAbstract{
	protected function getRightsFromDB()
	{
		global $db;
		$rtl=$db->select("roleright","rightname","rolename=".$db->escapeString($this->prop_name));
		$ret=array();
		foreach($rtl as $rt){
			$ret[]=$rt["rightname"];
		}
		return $ret;
	}
};


//eof
return;
?>
