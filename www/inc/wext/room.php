<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WORoom extends WORoomAbstract
{
	/**called from the CreateRoom transaction*/
	public static function createRoom($trans)
	{
		//check
		$rm=WTroom::getFromDB($trans->getroomid());
		if($rm!==false){
			$trans->abortWithError(tr("Room already exists"));
			return;
		}
		//create
		$rm=WTroom::newRow();
		$rm->roomid=$trans->getroomid();
		$rm->capacity=$trans->getcapacity();
		$rm->description=$trans->getdescription();
		$rm->insert();
		//return
		$trans->setroom(WORoom::fromTableroom($rm));
	}
};


//eof
return;
?>
