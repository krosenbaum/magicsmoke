<?php
// (c) Konrad Rosenbaum, 2007-2017
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


wob_autoclass("WOArtist","inc/wext/artist.php");
wob_autoclass("WOCartOrder","inc/wext/cart.php");
wob_autoclass("WOCustomer","inc/wext/customer.php");
wob_autoclass("WOEventPrice","inc/wext/event.php");
wob_autoclass("WOEvent","inc/wext/event.php");
wob_autoclass("WOFlag","inc/wext/flag.php");
wob_autoclass("WOOrder","inc/wext/order.php");
wob_autoclass("WOOrderInfo","inc/wext/order.php");
wob_autoclass("WOPriceCategory","inc/wext/price.php");
wob_autoclass("WORole","inc/wext/role.php");
wob_autoclass("WORoom","inc/wext/room.php");
wob_autoclass("WOSeatPlanGroup","inc/wext/seatplan.php");
wob_autoclass("WOSeatPlanVGroup","inc/wext/seatplan.php");
wob_autoclass("WOSeatPlan","inc/wext/seatplan.php");
wob_autoclass("WOSeatPlanDefPrice","inc/wext/seatplan.php");
wob_autoclass("WOServerFormat","inc/wext/format.php");
wob_autoclass("WOShipping","inc/wext/shipping.php");
wob_autoclass("WOTemplate","inc/wext/template.php");
wob_autoclass("WOTicket","inc/wext/ticket.php");
wob_autoclass("WOTicketAudit","inc/wext/ticket.php");
wob_autoclass("WOVoucher","inc/wext/voucher.php");
wob_autoclass("WOWebCart","inc/wext/webcart.php");
wob_autoclass("WOWebSession","inc/wext/websession.php");
wob_autoclass("WOCartVoucher","inc/wext/webcart.php");

wob_autoclass("MSmokeTransaction","inc/wext/transaction.php");

//eof
return;
?>
