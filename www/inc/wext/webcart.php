<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

///this is the class wrapping the cart tables,
///the business logic class is WebCart in rendering/cart_listing.php
class WOWebCart extends WOWebCartAbstract
{
	/**returns is empty attribute for twig*/
	public function getIsempty()
	{
		return count($this->prop_tickets)==0 && count($this->prop_vouchers)==0 && count($this->prop_items)==0;
		$prc=0;
	}

	/**returns the overall sum for twig*/
	public function getTotalsum()
	{
		$prc=0;
		//go through tickets
		foreach($this->gettickets() as $tck){
			$prc+=$tck->getprice()*$tck->getamount();
		}
		//go through vouchers add to sum
		foreach($this->getvouchers() as $id=>$vou){
			$prc+=$vou->getvalue();
		}
		//TODO: go through items add to sum
		//add shipping option
		$ship=$this->getshipping();
		if(is_a($ship,"WOShipping"))
			$prc+=$ship->getcost();
		//return...
		return $prc;
	}

	/**transaction to add tickets to cart*/
	static public function addTickets($trans)
	{
		$cartid=$trans->getcartid();
		$evid=$trans->geteventid();
		$pcid=$trans->getpricecategoryid();
		$amount=$trans->getamount();
		//get cart
		$cart=WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
		if(!is_a($cart,"WOWebCart"))return;
		$event=WTevent::getFromDB($evid);
		if(!is_a($event,"WTevent"))return;
		//ignore non-buy
		if($amount<1)return;
		//find category, match with event, check for usability
		$pc=WTeventprice::getFromDB($evid,$pcid);
		if(!is_a($pc,"WTeventprice"))return;
		$pco=WOEventPrice::fromTableeventprice($pc);
		if(!$pco->getCanUse())return;
		//find whether cart contains this
		$itm=WTcartticket::getFromDB($cartid,$evid,$pcid);
		if(is_a($itm,"WTcartticket")){
			//yes: add to it
			$itm->amount+=$amount;
			$itm->update();
		}else{
			//no: add new item
			$itm=WTcartticket::newRow();
			$itm->cartid=$cartid;
			$itm->eventid=$evid;
			$itm->pricecategoryid=$pcid;
			$itm->amount=$amount;
			$itm->insert();
		}
	}

	///transaction to add coupon to cart
	static public function addCoupon($trans)
	{
		$cartid=$trans->getcartid();
		$cpid=$trans->getcouponid();
		//get cart
		$dcart=WTcart::getFromDB($cartid);
		$cart=WOWebCart::fromTablecart($dcart);
		if(!is_a($cart,"WOWebCart")){
			$trans->abortWithError("no cart");
			return;
		}
		//find coupon
		$dcoup=WTcoupon::getFromDB($cpid);
		if(!is_a($dcoup,"WTcoupon")){
			$trans->abortWithError("no coupon");
			return;
		}
		//TODO: check validity
		//update
		$dcart->couponid=$cpid;
		$dcart->update();
	}

	/**transaction to remove tickets from cart*/
	static public function removeTickets($trans)
	{
		$cartid=$trans->getcartid();
		$evid=$trans->geteventid();
		$pcid=$trans->getpricecategoryid();
		//get cart
		$cart=WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
		if(!is_a($cart,"WOWebCart"))return;
		//find whether cart contains this
		$itm=WTcartticket::getFromDB($cartid,$evid,$pcid);
		if(is_a($itm,"WTcartticket")){
			//yes: remove it
			$itm->deleteFromDb();
		}
	}

	///transaction to add vouchers
	static public function addVoucher($trans)
	{
		$cartid=$trans->getcartid();
		$prc=$trans->getprice();
		//get cart
		$cart=WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
		if(!is_a($cart,"WOWebCart"))return;
		//get valid prices
		if(!in_array($prc,WOVoucher::validVoucherPrices()))return;
		//add voucher
		$cv=WTcartvoucher::newRow();
		$cv->cartid=$cartid;
		$cv->value=$prc;
		$cv->insert();
	}

	///transaction to remove vouchers
	static public function removeVoucher($trans)
	{
		$cartid=$trans->getcartid();
		$lineid=$trans->getlineid();
		//get cart
		$cart=WOWebCart::fromTablecart(WTcart::getFromDB($cartid));
		if(!is_a($cart,"WOWebCart"))return;
		//get voucher
		$cv=WTcartvoucher::getFromDB($lineid);
		//checks
		if(!is_a($cv,"WTcartvoucher"))return;
		if($cv->cartid!=$cartid)return;
		//delete
		$cv->deleteFromDb();
	}
};

class WOCartVoucher extends WOCartVoucherAbstract
{
	///used for web: returns the URL part of this voucher for removal
	public function getInCartIdWeb()
	{
		global $basevars;
		return $basevars['inputnames']['voucher'].'='.$this->getcartlineid();
	}
};


//eof
return;
?>
