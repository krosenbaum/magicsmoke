<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL


class WOEventPrice extends WOEventPriceAbstract
{
	/**used by table mapping to count tickets*/
	public function getAmountTicketsFromDB()
	{
		global $db;
		$res=$db->select("ticket","count(*)as count", "eventid=".$db->escapeInt($this->prop_eventid)." AND pricecategoryid=".$db->escapeInt($this->prop_pricecategoryid));
		return $res[0]["count"];
	}
	/**used by table mapping to count tickets*/
	public function getAmountTicketsBlockFromDB()
	{
		global $db;
		$res=$db->select("ticket","status", "eventid=".$db->escapeInt($this->prop_eventid)." AND pricecategoryid=".$db->escapeInt($this->prop_pricecategoryid));
// 		print("!!!hallo:");var_dump($res);print($db->lastError());
		$cnt=0;
		if($res!==false)
		foreach($res as $r)
			if($r["status"] & WTticket::MaskBlock)
				$cnt++;
		return $cnt;
	}

	/**helper for web UI: returns the name of the field for this event price, always the configured amount field from WebCart plus an index indicating category and event IDs*/
	public function getAmountinputfield()
	{
		return WebCart::TicketAmountField."[".$this->prop_pricecategoryid.":".$this->prop_eventid."]";
	}

	/**helper for web UI: returns the value for HTML variables for this event price: an index indicating category and event IDs*/
	public function getCategoryIdWeb()
	{
		global $basevars;
		return $basevars["inputnames"]["ticket"]."=".$this->prop_pricecategoryid.":".$this->prop_eventid;
	}

	/**helper function for web UI: returns the event ID from an amount field index */
	static public function getEventIdFromAmountIndex($idx)
	{
		$sl=explode(":",$idx);
		if(count($sl)==2)return $sl[1]+0;
		else return null;
	}

	/**helper function for web UI: returns the event ID from an amount field index */
	static public function getCategoryIdFromAmountIndex($idx)
	{
		$sl=explode(":",$idx);
		if(count($sl)==2)return $sl[0]+0;
		else return null;
	}
};

class WOEvent extends WOEventAbstract
{
	/**get amount of sold tickets from DB*/
	public function getAmountSoldFromDB()
	{
		global $db;
		$res=$db->select("ticket","status", "eventid=".$db->escapeInt($this->prop_id));
		$cnt=0;
		if($res!==false)
		foreach($res as $r)
			if($r["status"] == WTticket::Ordered || $r["status"] == WTticket::Used)
				$cnt++;
		return $cnt;
	}
	/**get amount of reserved tickets from DB*/
	public function getAmountReservedFromDB()
	{
		global $db;
		$res=$db->select("ticket","status", "eventid=".$db->escapeInt($this->prop_id)." AND status=".$db->escapeInt(WTticket::Reserved));
		return count($res);
	}

	/**XML iface: get all events*/
	public static function getAllEvents($trans)
	{
		//get all of them
		$evlst=WOEvent::fromTableArrayevent(WTevent::selectFromDB("","ORDER BY starttime"));
		//filter them by flags
		$evlst2=array();
		global $session;
		foreach($evlst as $ev){
			if($session->checkFlags($ev->getflags()))
				$evlst2[]=$ev;
		}
		//return...
		$trans->setevents($evlst2);
	}

	/**XML iface: get list of events*/
	public static function getEventList($trans)
	{
		global $db;
		$evlst=WOEvent::fromTableArrayevent(WTevent::selectFromDB("eventid in ".$db->escapeIntList($trans->geteventids())));
		$trans->setevents($evlst);
	}

	/**called by CreateEvent transaction*/
	public static function createEvent($trans)
	{
		//get input
		$evt=$trans->getevent();
		if(!is_a($evt,"WOEvent")){
			$trans->abortWithError(tr("The event to be created must be a valid event object!"));
			return;
		}
		//convert for DB
		$tab=WTevent::newRow();
		$evt->toTableevent($tab);
		//make sure it fits and is not cancelled
		$tab->revert("eventid");
		$tab->iscancelled=false;
		$tab->revert("cancelreason");
		//create
		$tab->insert();
		//remaining conversions
		$evt->prop_id=$tab->eventid;
		$evt->syncPricesToDB();
		//return
		$trans->setevent(WOEvent::fromTableevent($tab));
	}

	/**called by ChangeEvent transaction*/
	public static function changeEvent($trans)
	{
		//get input
		$evt=$trans->getevent();
		if(!is_a($evt,"WOEvent")){
			$trans->abortWithError(tr("The event to be changed must be a valid event object!"));
			return;
		}
		$tab=WTevent::getFromDB($evt->getid());
		if($tab === false){
			$trans->abortWithError(tr("The event is not valid."));
			return;
		}
		//copy stuff
		$evt->toTableevent($tab);
		//TODO: fix bug in this...
		//if($evt->getseatplanid()===null)$tab->seatplanid=null;//toTable.. does not handle null correctly for this case
		//check for cancel
		if($tab->isColumnChanged("iscancelled") && $tab->iscancelled){
			if($trans->havePrivilege(WtrChangeEvent::Priv_CancelEvent)){
				//cancel tickets
				$evt->cancelAllTickets();
			}else{
				//alarm
				$trans->abortWithError(tr("You do not have the privilege to cancel events."));
				return;
			}
		}
		//now sync
		$tab->update();
		$evt->syncPricesToDB();
		//return
		$trans->setevent(WOEvent::fromTableevent($tab));
	}

	/**helper function for create/changeEvent - pushes the current list of prices to the database*/
	public function syncPricesToDB()
	{
		global $db;
		$dbprc=WTeventprice::selectFromDB("eventid=".$db->escapeInt($this->prop_id));
		//sort
		$dbids=array();$dbp=array();
		foreach($dbprc as $p){
			$dbp[$p->pricecategoryid]=$p;
			$dbids[]=$p->pricecategoryid;
		}
		$evids=array();
		foreach($this->prop_price as $p)$evids[]=$p->getpricecategoryid();
		//delete unneeded ones
		foreach($dbprc as $p){
			if(!in_array($p->pricecategoryid,$evids)){
				$p->deleteFromDB();
			}
		}
		//create new ones, change existing ones
		foreach($this->prop_price as $p){
			$pcid=$p->getpricecategoryid();
			if(in_array($pcid,$dbids)){
				//exists in DB, compare
				$pp=$dbp[$pcid];
				$p->toTableeventprice($pp);
				$pp->revert("eventid");
				if($pp->eventid != $this->prop_id)
					$pp->eventid = $this->prop_id;
				if($pp->isChanged())
					$pp->update();
			}else{
				//does not exist yet
				$tab=WTeventprice::newRow();
				$p->toTableeventprice($tab);
				$tab->eventid=$this->prop_id;//we are paranoid!
				$tab->insert();
			}
		}
	}

	/**called by CancelEvent transaction*/
	static public function cancelEvent($trans)
	{
		$tab=WTevent::getFromDB($trans->geteventid());
		if(!is_a($tab,"WTevent")){
			$trans->abortWithError(tr("This is not a valid event."));
			return;
		}
		//cancel
		$tab->iscancelled=true;
		$tab->cancelreason=$trans->getreason();
		$tab->update();
		WOEvent::fromTableevent($tab)->cancelAllTickets();
	}

	/**helper function: cancels all usable tickets of this event*/
	public function cancelAllTickets()
	{
		global $db;
		$ticks=WTticket::selectFromDB("eventid=".$db->escapeint($this->prop_eventid));
		foreach($ticks as $t){
			if($t->status & WTticket::MaskReturnable){
				$t->status=WTticket::Refund;
				$t->update();
			}
		}
	}

	/**called by the GetEventSummary transaction: returns the full event summary*/
	public static function getSummary($trans)
	{
		$evt=WOEvent::fromTableevent(WTevent::getFromDB($trans->geteventid()));
		if(!is_a($evt,"WOEvent")){
			$trans->abortWithError(tr("The event ID is not valid."));
			return;
		}
		//return event
		$trans->setevent($evt);
		//find orders
		//get list of valid orderids
		global $db;
		$res=$db->select("ticket","orderid","eventid=".$db->escapeInt($trans->geteventid()),"GROUP BY orderid ORDER BY orderid");
		$oidl=array();
		foreach($res as $r)
			if(!in_array($r["orderid"],$oidl))$oidl[]=$r["orderid"];
		//get orders
		$olst=WOOrder::fromTableArrayorder(WTorder::selectFromDB("orderid in ".$db->escapeIntList($oidl),"ORDER BY orderid"));
		$trans->setorders($olst);
	}

	/**called by the GetEntranceEvents transaction*/
	public static function getEntranceEvents($trans)
	{
		$ms=$trans->getmaxbeforestart();
		if($ms===null || $ms<10000)$ms=24*3600;
		$start=time()+$ms; //is running or starts within 24h
		$ms=$trans->getmaxafterend();
		if($ms===null || $ms<0)$ms=0;
		$end=time()-$ms; //has not ended yet
		global $db;
		$trans->setevents(WOEvent::fromTableArrayevent(WTevent::selectFromDB("starttime<=".$db->escapeInt($start)." AND endtime>=".$db->escapeInt($end))));
	}
};


//eof
return;
?>
