<?php
// (c) Konrad Rosenbaum, 2007-2021
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

//PHP error handling
ini_set("error_log",str_replace("*",date("Ymd"),"./log/error-*.log"));

//load WOB data
include('./inc/wbase/autoload.php');
include('./inc/wob/autoload.php');
include('./inc/wext/autoload.php');
//load DB drivers
include('./inc/db/autoload.php');
include('./config.php');
//try to connect
$db->tryConnect();
//make machine interface available (also used indirectly by index.php)
include("./inc/machine/autoload.php");
//move on in loader_nonadmin.php (or admin.php)
return;
?>
