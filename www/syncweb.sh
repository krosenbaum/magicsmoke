#!/bin/sh

#target host and directory
TARGET=silmor.de:/home/web/htdocs.pahl
SSH='ssh -l konrad'

rsync -vrLpEtz --exclude=.svn --exclude='*~' --exclude=syncweb.sh --exclude=config.php -e "$SSH" . "$TARGET"
