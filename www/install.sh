#!/bin/bash
#
# (c) Konrad Rosenbaum, 2007-2012
# protected under the GNU AGPL version 3 or at your option any newer
# see COPYING.AGPL

#get the directory this script comes from
BASE=$(cd `dirname $0`; pwd)

#check that we have exactly one argument: a directory
if test $# -ne 1 ; then
 echo Usage: $0 '<directory>'
 echo
 echo "This tool installs the current version of the"
 echo "MagicSmoke server into the given directory."
 exit 1
fi

#create target directory
TARGET=$1
mkdir -p $TARGET || {
	echo Unable to create directory $TARGET, giving up.
	exit 1
}

echo "Copying/Linking from $BASE to $TARGET..."

#link routine, argument: file/dir to symlink from base to target dir
xln(){
 echo "Linking $1..."
 ln -sfT $BASE/$1 $TARGET/$1
}

#link all active content: scripts, base pictures, htaccess, translations
# ...stuff that is not customized by the user
xln inc
xln images
xln admin.php
xln index.php
xln machine.php
xln translations
xln .htaccess

#copy routine, arguments:
# $1: target file name
# $2: optional - source file name, if not present identical to $1
#does nothing if the target exists
xcp(){
 a=$TARGET/$1
 test x$2 == x && b=$BASE/$1 || b=$BASE/$2
 test -e $a && echo "$a exists, please synchronize with $b manually"
 test -e $a || {
  cp -a $b $a
  echo "Installed $a: please modify it to suit your needs."
 }
}

#copy all templates that need to be adjusted by the user
xcp styles
xcp template
xcp config.php config.php.template


echo "Done. Please edit the configuration and check that the installation works."
exit 0
