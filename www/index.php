<?php
// (c) Konrad Rosenbaum, 2007-2015
// (c) Peter Keller, 2007/8
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

//bail out during maintenance
if(file_exists("maintenance.php")){
	include("maintenance.php");
	die("<!-- Maintenance! -->");
}

//basics
include('inc/loader.php');
include('inc/loader_nonadmin.php');

//unify arguments
$HTTPARGS=$_GET;
foreach($_POST as $a=>$p)$HTTPARGS[$a]=$p;

function getHttpArg($name,$default=null)
{
	global $HTTPARGS;
	if(isset($HTTPARGS[$name]))return $HTTPARGS[$name];
	return $default;
}

//set common basics
$mode="index";
if(isset($_GET["mode"])){
	$mode=$_GET["mode"];
}

//set internal $session to virtual "_web" user for use by transactions
Session::setWebSession();
$websession=WOWebSession::getOrCreateWebSession();

//initialize TWIG
BaseVars::initTwig();

//initialize basevars
BaseVars::init();
BaseVars::initCartAndUserData();
// other info
$basevars['lang']=LanguageManager::singleton();

//strings that are used to compose the overall layout
$page="(internal error: no page text yet, probably no template defined)";

try{
	//get page template and process it
	//NOTE: if you alter the meaning of any mode or add modes do not forget to change doc/web/flow.html!
	switch($mode){
		case "eventDetails": // show details of an event
			$page=EventRender::createEventDetails();
			break;
		case "eventOrder": // process of ordering tickets
			WebCart::addTickets();
			break;
		case "mycart": // show users current cart (make sure he/she has one)
			//make sure cart exists
			WebCart::getOrCreateCart();
			//fall through to show it
		case "cart": // show current cart (can be called from anywhere in order process)
			$page=WebCart::createCartOverview();
			break;
		case "addcoupon"://add a coupon to the cart
			WebCart::addCoupon();
			$page=WebCart::createCartOverview();
			break;
		case "removeItem": // remove something from cart
			WebCart::removeItem();
			break;
		case "vouchers": // allow user to select voucher value to be added to cart
			$page=WebCart::createVoucherOverview();
			break;
		case "voucherOrder": // add selected voucher to cart, then redirect to cart
			WebCart::addVoucher();
			break;
		case "shop": // Merchandising Shop
			// redirect to main page - anchor for flow resets
			redirectHome();
			break;
		case "checkout": // redirection target during checkout from login/register pages
			$page=WebCart::checkout();
			break;
		case "orderLogin": // log in or register to be able to checkout
			$page=WOCustomer::loginPage("Order");
			break;
		case "customerLoginOrder": // log in with a known customer login
			$page=WOCustomer::checkLogin("checkout",true);
			break;
		case "customerRegistrationOrder": // log in with new customer data
			$page=WOCustomer::registerCustomer("checkout",true);
			break;
		case "customerResetOrder": // reset password from order login page
			$page=WOCustomer::resetPasswordPage("orderLogin");
			break;
		case "changeDeliveryAddress":
		case "changeInvoiceAddress": // change addresses
			$page=WebCart::changeAddressPage($mode);
			break;
		case 'placeOrder': // convert cart to order
			$page=WebCart::placeOrder();
			break;
		case "customerResetLogin": // reset login page (link came via mail)
			$page=WOCustomer::resetLoginPage();
			break;
		case "setlanguage": // mini-links to change browser language
			LanguageManager::setLanguage();
			break;
		case "compile": // internal: compile the templates (the method asks for admin credentials)
			TemplateCompiler::execute();
			break;
		case "index": // show the index page
			$page=$twig->loadTemplate("index.html")->render($basevars);
			break;
		default: // if in doubt: show the event page, customers will probably like it ;-)
			//TODO: make this configurable
			$page=EventRender::createEventList();
			break;
	}
	//NOTE: if you alter the meaning of any mode or add modes do not forget to change doc/web/flow.html!
}catch(Exception $ex){
	//log to (Apache) log file
	error_log($ex->getMessage());
	//try to send error mail
	if(isset($ErrorMailAddr)){
		try{
			$txt=$ex->getMessage();
			mail($ErrorMailAddr, "MagicSmoke Error: $txt", "An error occured in MagicSmoke:\n$txt\nMode: $mode\n\nStack Trace:\n".$ex->getTraceAsString());
		}catch(Exception $x){}
	}
	//get error template (or fallback for it)
	try{
		$p=$twig->loadTemplate("error.html");
	}catch(Exception $ex2){
		$twig->setLoader(new Twig_Loader_String);
		$p=$twig->loadTemplate("<font color=\"red\">{{ErrorText}}</font><p/><pre>\n{{ErrorTrace}}\n</pre>");
	}
	//show error or some excuse for not showing one
	$e=$basevars;
	if($WebShowErrors){
		$e["ErrorText"]=$ex->getMessage();
		$e["ErrorTrace"]=$ex->getTraceAsString();
	}else
		$e["ErrorText"]=translate("WebSite","An error occured, contact the server admin for details.");
	$page=$p->render($e);
}

//spit out completed page
header("Content-Type: text/html; charset=utf-8");
header("Cache-control: no-cache");
print($page);

//prevent potentially attacking bots from successfully injecting
// code below here (some bots add harmful code to index.php)
exit(0);

?>
