<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" encoding="utf-8" doctype-public="TS"/>
	
	<xsl:template name="context">
		<xsl:for-each select="message">
			<xsl:copy-of select="."/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="/TS">
		<xsl:element name="TS">
			<xsl:attribute name="version">1.1</xsl:attribute>
			<xsl:attribute name="language"><xsl:value-of select="@language"/></xsl:attribute>
			<xsl:element name="context">
				<xsl:element name="name"><xsl:text>php::</xsl:text></xsl:element>
				<xsl:for-each select="context">
					<xsl:call-template name="context"/>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>