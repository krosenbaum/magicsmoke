<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CouponVerifier</name>
    <message>
        <location filename="../inc/classes/eventplan.php" line="232"/>
        <source>Category conversion is not covered by the coupon.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventPlan</name>
    <message>
        <location filename="../inc/classes/eventplan.php" line="96"/>
        <source>Cannot sell a negative amount of tickets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="101"/>
        <source>The category is not numeric.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="106"/>
        <source>Not a valid event or you do not have access to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="113"/>
        <source>Not enough tickets in this event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="132"/>
        <source>The category is not part of the event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="139"/>
        <source>You cannot order in this category.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="146"/>
        <source>Not enough tickets in this category.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/classes/eventplan.php" line="157"/>
        <source>Rejected by seat plan: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MachineUser</name>
    <message>
        <location filename="../inc/machine/muser.php" line="18"/>
        <source>User already exists.</source>
        <translation>Der Nutzer existier bereits.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="23"/>
        <source>Username is invalid.</source>
        <translation>Der Nutzername ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="43"/>
        <location filename="../inc/machine/muser.php" line="75"/>
        <location filename="../inc/machine/muser.php" line="89"/>
        <location filename="../inc/machine/muser.php" line="102"/>
        <location filename="../inc/machine/muser.php" line="122"/>
        <location filename="../inc/machine/muser.php" line="152"/>
        <location filename="../inc/machine/muser.php" line="171"/>
        <location filename="../inc/machine/muser.php" line="237"/>
        <source>User does not exist.</source>
        <translation>Der Nutzer existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="52"/>
        <source>Merge target user does not exist!</source>
        <translation>Der Nutzer mit dem dieser vereint werden soll existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="58"/>
        <source>User and merge target user are identical.</source>
        <translation>Der zu löschende und der Zielnutzer sind identisch.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="134"/>
        <source>Trying to assign non-existent role.</source>
        <translation>Die Rolle/Gruppe existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="183"/>
        <source>Trying to assign non-existent host.</source>
        <translation>Der Host existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="212"/>
        <location filename="../inc/machine/muser.php" line="260"/>
        <location filename="../inc/machine/muser.php" line="297"/>
        <location filename="../inc/machine/muser.php" line="330"/>
        <source>Role does not exist.</source>
        <translation>Die Rolle/Gruppe existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="221"/>
        <location filename="../inc/machine/muser.php" line="246"/>
        <source>Trying to assign non-existent flag.</source>
        <translation>Das Flag existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="274"/>
        <source>Illegal role name.</source>
        <translation>Ungülitiger Name für eine Rolle/Gruppe.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="280"/>
        <source>Role already exists.</source>
        <translation>Diese Rolle/Gruppe existiert bereits.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="306"/>
        <source>Trying to set an illegal right.</source>
        <translation>Es wird versucht ein ungültiges Recht zu setzen.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="324"/>
        <source>Cannot delete special roles.</source>
        <translation>Spezialrollen können nicht gelöscht werden.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="347"/>
        <source>Cannot set/create special hosts.</source>
        <translation>Spezialhosts können nicht geänder/erzeugt werden.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="351"/>
        <source>Illegal host name.</source>
        <translation>Ungültiger Hostname.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="371"/>
        <source>Cannot delete special hosts.</source>
        <translation>Spezialhosts können nicht gelöscht werden.</translation>
    </message>
    <message>
        <location filename="../inc/machine/muser.php" line="377"/>
        <source>Host does not exist.</source>
        <translation>Host existiert nicht.</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../inc/machine/session.php" line="118"/>
        <source>Unknown Host</source>
        <translation>Unbekannter Host.</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="125"/>
        <source>Host/User combination not allowed</source>
        <translation>Diese Kombination von Host/Nutzer ist nicht erlaubt.</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="130"/>
        <source>Host authentication failed</source>
        <translation>Host-Authentifizierung fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="136"/>
        <location filename="../inc/machine/session.php" line="140"/>
        <source>User Authentication failed</source>
        <translation>Nutzer-Authentifikation fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="195"/>
        <source>cannot set an empty password</source>
        <translation>Leeres Passwort kann nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="200"/>
        <source>Ooops. Unable to find user. You have been deleted.</source>
        <translation>Ups. Sie wurden gelöscht.</translation>
    </message>
    <message>
        <location filename="../inc/machine/session.php" line="202"/>
        <source>Wrong password. Request denied.</source>
        <translation>Falsches Passwort. Die Anfrage wird verweigert.</translation>
    </message>
</context>
<context>
    <name>SpecialHost</name>
    <message>
        <location filename="../inc/wob/schema.php" line="38"/>
        <source>_any</source>
        <translation>beliebiger (auch unregistrierter) Host</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../inc/machine/translation.php" line="29"/>
        <source>Format must be either &apos;ts&apos; or &apos;qm&apos;.</source>
        <translation>Format muss &apos;ts&apos; oder &apos;qm&apos; sein.</translation>
    </message>
    <message>
        <location filename="../inc/machine/translation.php" line="33"/>
        <source>Language invalid.</source>
        <translation>Sprachspezifikation ungültig.</translation>
    </message>
    <message>
        <location filename="../inc/machine/translation.php" line="45"/>
        <source>Language unknown.</source>
        <translation>Diese Sprache ist dem Server nicht bekannt.</translation>
    </message>
</context>
<context>
    <name>WOAddress</name>
    <message>
        <location filename="../inc/wob/wo_Address.php" line="243"/>
        <source>Unable to deserialize object of type WOAddress: invalid XML.</source>
        <translation>Das WOAddress Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOArtistAbstract</name>
    <message>
        <location filename="../inc/wext/artist.php" line="14"/>
        <source>Artist name must not be empty!</source>
        <translation>Der Künstlername darf nicht leer sein!</translation>
    </message>
    <message>
        <location filename="../inc/wext/artist.php" line="21"/>
        <source>An artist with this name already exists.</source>
        <translation>Ein Künstler dieses Namens existiert bereits.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Artist.php" line="158"/>
        <source>Unable to deserialize object of type WOArtist: invalid XML.</source>
        <translation>Das WOArtist Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCartItem</name>
    <message>
        <location filename="../inc/wob/wo_CartItem.php" line="44"/>
        <source>Unable to deserialize object of type WOCartItem: invalid XML.</source>
        <translation>Das WOCartItem Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCartOrderAbstract</name>
    <message>
        <location filename="../inc/wext/cart.php" line="18"/>
        <source>Not a valid cart object.</source>
        <translation>Dies ist kein gültiger Einkaufskorb.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="44"/>
        <source>Lacking privileges to create a sale.</source>
        <translation>Sie haben nicht die Berechtigung Verkäufe anzulegen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="49"/>
        <source>Lacking privileges to create an order.</source>
        <translation>Sie haben nicht die Berechtigung Bestellungen anzulegen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="55"/>
        <source>You do not have the privilege to sell tickets.</source>
        <translation>Sie haben nicht die Berechtigung Eintrittskarten zu verkaufen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="59"/>
        <source>You do not have the privilege to sell vouchers.</source>
        <translation>Sie haben nicht die Berechtigung Gutscheine zu verkaufen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="63"/>
        <source>You do not have the privilege to sell shop items.</source>
        <translation>Sie haben nicht die Berechtigung Produkte zu verkaufen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="74"/>
        <source>CreateOrder called from an unknown transaction.</source>
        <translation>Interner Fehler: Die &apos;CreateOrder&apos;-Funktion wurde aus einer unbekannten Transaktion aufgerufen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="81"/>
        <source>Nothing in the cart.</source>
        <translation>Der Einkaufskorb ist leer.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="92"/>
        <source>Reservations must not contain anything but tickets.</source>
        <translation>Reservierungen dürfen nur Eintrittskarten enthalten.</translation>
    </message>
    <message>
        <location filename="../inc/wext/cart.php" line="316"/>
        <source>DB error while inserting order.</source>
        <translation>Ein Datenbankfehler ist beim Anlegen der Bestellung aufgetreten.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartOrder.php" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartOrder.php" line="58"/>
        <source>Invalid</source>
        <translation>Ungültig</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartOrder.php" line="292"/>
        <source>Unable to deserialize object of type WOCartOrder: invalid XML.</source>
        <translation>Das WOCartOrder Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCartTicket</name>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="323"/>
        <source>Unable to deserialize object of type WOCartTicket: invalid XML.</source>
        <translation>Das WOCartTicket Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="125"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="126"/>
        <source>EventOver</source>
        <translation>Veranstaltung bereits vorbei</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="127"/>
        <source>TooLate</source>
        <translation>zu spät</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="128"/>
        <source>Exhausted</source>
        <translation>Kontingent verbraucht</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartTicket.php" line="129"/>
        <source>Invalid</source>
        <translation>Ungültig</translation>
    </message>
</context>
<context>
    <name>WOCartVoucherAbstract</name>
    <message>
        <location filename="../inc/wob/wo_CartVoucher.php" line="88"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartVoucher.php" line="89"/>
        <source>InvalidValue</source>
        <translation>Gutscheinwert nicht zulässig</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartVoucher.php" line="90"/>
        <source>InvalidPrice</source>
        <translation>Gutscheinpreis nicht zulässig</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_CartVoucher.php" line="190"/>
        <source>Unable to deserialize object of type WOCartVoucher: invalid XML.</source>
        <translation>Das WOCartVoucher Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOContact</name>
    <message>
        <location filename="../inc/wob/wo_Contact.php" line="129"/>
        <source>Unable to deserialize object of type WOContact: invalid XML.</source>
        <translation>Das WOContact Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOContactType</name>
    <message>
        <location filename="../inc/wob/wo_ContactType.php" line="94"/>
        <source>Unable to deserialize object of type WOContactType: invalid XML.</source>
        <translation>Das WOContactType Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCountry</name>
    <message>
        <location filename="../inc/wob/wo_Country.php" line="76"/>
        <source>Unable to deserialize object of type WOCountry: invalid XML.</source>
        <translation>Das WOCountry Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCoupon</name>
    <message>
        <location filename="../inc/wob/wo_Coupon.php" line="190"/>
        <source>Unable to deserialize object of type WOCoupon: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOCouponInfo</name>
    <message>
        <location filename="../inc/wob/wo_CouponInfo.php" line="128"/>
        <source>Unable to deserialize object of type WOCouponInfo: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOCouponRule</name>
    <message>
        <location filename="../inc/wob/wo_CouponRule.php" line="168"/>
        <source>Unable to deserialize object of type WOCouponRule: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOCustomerAbstract</name>
    <message>
        <location filename="../inc/wext/customer.php" line="15"/>
        <location filename="../inc/wext/customer.php" line="57"/>
        <source>Not a valid customer object.</source>
        <translation>Dies ist kein gültiges Kunden-Objekt.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="62"/>
        <location filename="../inc/wext/customer.php" line="256"/>
        <source>Customer does not exist in the database.</source>
        <translation>Der Kunde existiert nicht in der Datenbank.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="144"/>
        <source>Not a valid customer, cannot delete.</source>
        <translation>Dies ist kein gültiger Kunde, er kann daher nicht gelöscht werden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="159"/>
        <source>This customer has orders in the system, cannot delete.</source>
        <translation>Dieser Kunde hat Bestellungen im System, er kann daher nicht gelöscht werden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="172"/>
        <source>Cannot merge a customer with itself.</source>
        <translation>Kann einen Kunden nicht mit sich selbst zusammenlegen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="202"/>
        <source>Country ID and name must contain values!</source>
        <translation>Das Land und der Name des Kunden müssen Werte enthalten!</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="209"/>
        <source>Country ID exists!</source>
        <translation>Dieses Land (Country ID) existiert bereits!</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="214"/>
        <source>Country name exists!</source>
        <translation>Dieses Land (Name) existiert bereits!</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="232"/>
        <source>Contact Type names must not be empty!</source>
        <translation>Namen von Kontakttypen dürfen nicht leer sein!</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="237"/>
        <source>Contact Type already exists.</source>
        <translation>Dieser Kontakttyp existiert bereits.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="262"/>
        <source>This email already exists in the database.</source>
        <translation>Diese Mailadresse existiert bereits für einen anderen Kunden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="275"/>
        <source>Not a valid customer.</source>
        <translation>Dies ist kein gültiger Kunde.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="279"/>
        <source>Customer has no email address.</source>
        <translation>Der Kunde hat keine Mail-Adresse.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="329"/>
        <source>There is a problem with your mail client or your browser: please copy the URL into the browser manually and try again.</source>
        <translation>Ihr Mail-Programm oder Ihr Browser hat ein Problem: bitte kopieren Sie die URL manuell in den Browser-URL-Zeile und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="337"/>
        <source>This user does not exist.</source>
        <translation>Dieser Nutzer existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="346"/>
        <source>The passcode supplied by your browser is either invalid or expired.</source>
        <translation>Der von Ihrem Browser ausgelieferte Reset-Code ist nicht mehr gültig.</translation>
    </message>
    <message>
        <location filename="../inc/wext/customer.php" line="356"/>
        <source>The password does not match or is empty, please use the back button of your browser and try again.</source>
        <translation>Das neue Passwort ist leer oder die Bestätigung stimmt nicht überein. Bitte nutzen Sie den &quot;Zurück&quot;-Button Ihres Browsers und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Customer.php" line="226"/>
        <source>Unable to deserialize object of type WOCustomer: invalid XML.</source>
        <translation>Das WOCustomer Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOCustomerInfo</name>
    <message>
        <location filename="../inc/wob/wo_CustomerInfo.php" line="110"/>
        <source>Unable to deserialize object of type WOCustomerInfo: invalid XML.</source>
        <translation>Das WOCustomerInfo Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOEventAbstract</name>
    <message>
        <location filename="../inc/wext/event.php" line="112"/>
        <source>The event to be created must be a valid event object!</source>
        <translation>Die zu erzeugende Veranstultung muss ein gültiges Objekt sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/event.php" line="137"/>
        <source>The event to be changed must be a valid event object!</source>
        <translation>Die zu ändernde Veranstultung muss ein gültiges Objekt sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/event.php" line="142"/>
        <source>The event is not valid.</source>
        <translation>Diese Veranstaltung ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="../inc/wext/event.php" line="156"/>
        <source>You do not have the privilege to cancel events.</source>
        <translation>Sie haben nicht die Berechtigung Veranstaltungen abzusagen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/event.php" line="213"/>
        <source>This is not a valid event.</source>
        <translation>Diese Veranstaltung ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="../inc/wext/event.php" line="241"/>
        <source>The event ID is not valid.</source>
        <translation>Die Veranstaltungs-ID ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Event.php" line="420"/>
        <source>Unable to deserialize object of type WOEvent: invalid XML.</source>
        <translation>Das WOEvent Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOEventPriceAbstract</name>
    <message>
        <location filename="../inc/wob/wo_EventPrice.php" line="229"/>
        <source>Unable to deserialize object of type WOEventPrice: invalid XML.</source>
        <translation>Das WOEventPrice Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOEventSaleInfo</name>
    <message>
        <location filename="../inc/wob/wo_EventSaleInfo.php" line="215"/>
        <source>Unable to deserialize object of type WOEventSaleInfo: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOFlagAbstract</name>
    <message>
        <location filename="../inc/wob/wo_Flag.php" line="56"/>
        <source>Unable to deserialize object of type WOFlag: invalid XML.</source>
        <translation>Das WOFlag Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOHost</name>
    <message>
        <location filename="../inc/wob/wo_Host.php" line="76"/>
        <source>Unable to deserialize object of type WOHost: invalid XML.</source>
        <translation>Das WOHost Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOItemAudit</name>
    <message>
        <location filename="../inc/wob/wo_ItemAudit.php" line="107"/>
        <source>Unable to deserialize object of type WOItemAudit: invalid XML.</source>
        <translation>Das WOItemAudit Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOItemInfo</name>
    <message>
        <location filename="../inc/wob/wo_ItemInfo.php" line="148"/>
        <source>Unable to deserialize object of type WOItemInfo: invalid XML.</source>
        <translation>Das WOItemInfo Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOKeyValuePair</name>
    <message>
        <location filename="../inc/wob/wo_KeyValuePair.php" line="71"/>
        <source>Unable to deserialize object of type WOKeyValuePair: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOOrderAbstract</name>
    <message>
        <location filename="../inc/wext/order.php" line="236"/>
        <source>Invalid shipping ID.</source>
        <translation>Ungültige Versandart.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="240"/>
        <source>You cannot use this shipping type.</source>
        <translation>Sie haben nicht die Berechtigung diese Versandart zu nutzen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="270"/>
        <location filename="../inc/wext/order.php" line="306"/>
        <location filename="../inc/wext/order.php" line="329"/>
        <location filename="../inc/wext/order.php" line="345"/>
        <location filename="../inc/wext/order.php" line="399"/>
        <location filename="../inc/wext/order.php" line="471"/>
        <source>Order ID is not valid.</source>
        <translation>Die Bestellnr. ist nicht gültig.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="274"/>
        <source>Order has already been shipped.</source>
        <translation>Diese Bestellung ist bereits versandt.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="278"/>
        <source>Order is in the wrong state.</source>
        <translation>Die Bestellung befindet sich im falschen Zustand für diese Aktion.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="351"/>
        <location filename="../inc/wext/order.php" line="425"/>
        <location filename="../inc/wext/order.php" line="757"/>
        <source>Amount to be paid must be positive.</source>
        <translation>Der zu zahlende Betrag muss positiv sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="373"/>
        <source>unknown payment type</source>
        <translation>unbekannte Bezahlart</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="378"/>
        <source>you are not allowed to use this payment type</source>
        <translation>Sie haben nicht die Berechtigung diese Bezahlart zu nutzen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="405"/>
        <location filename="../inc/wext/order.php" line="415"/>
        <location filename="../inc/wext/order.php" line="763"/>
        <location filename="../inc/wext/order.php" line="773"/>
        <location filename="../inc/wext/order.php" line="809"/>
        <location filename="../inc/wext/order.php" line="814"/>
        <location filename="../inc/wext/order.php" line="839"/>
        <location filename="../inc/wext/order.php" line="844"/>
        <source>Voucher is not valid!</source>
        <translation>Der Gutschein ist nicht gültig!</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="410"/>
        <location filename="../inc/wext/order.php" line="768"/>
        <source>Voucher is past its validity date!</source>
        <translation>Das Gültigkeitsdatum des Gutscheins ist abgelaufen!</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="419"/>
        <location filename="../inc/wext/order.php" line="777"/>
        <location filename="../inc/wext/order.php" line="848"/>
        <source>Voucher cannot be used: it has not been paid for.</source>
        <translation>Dieser Gutschein kann nicht benutzt werden: er ist selbst noch nicht bezahlt.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="432"/>
        <source>Cannot pay for voucher with another voucher.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="452"/>
        <source>Voucher: </source>
        <translation>Gutschein:</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="477"/>
        <source>Amount to be refunded must be positive.</source>
        <translation>Der auszuzahlende Betrag muss positiv sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="504"/>
        <source>Invalid Order ID.</source>
        <translation>Ungültige Bestellnr.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="509"/>
        <source>This order is not a reservation.</source>
        <translation>Diese Bestellung ist keine Reservierung.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="538"/>
        <source>This ticket cannot be returned!</source>
        <translation>Diese Eintrittskarte kann nicht (mehr) zurückgegeben werden!</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="543"/>
        <location filename="../inc/wext/order.php" line="615"/>
        <location filename="../inc/wext/order.php" line="677"/>
        <source>Internal error: ticket for unknown event.</source>
        <translation>Interner Fehler: Eintrittskarte für eine unbekannte Veranstaltung.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="547"/>
        <source>This ticket is for a past event and you do not have the privilege to return it.</source>
        <translation>Diese Eintrittskarte ist für eine vergangene Veranstaltung und Sie haben nicht die Berechtigung diese zurückzugeben.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="559"/>
        <source>This voucher has already been used, cannot return it.</source>
        <translation>Dieser Gutschein wurde bereits benutzt, er kann nicht zurückgegeben werden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="568"/>
        <source>Ticket or voucher not found.</source>
        <translation>Gutschein oder Eintrittskarte wurde nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="584"/>
        <source>Invalid order ID.</source>
        <translation>Ungültige Bestellnr.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="597"/>
        <source>The order is in the wrong status or you do not have the privilege to cancel it.</source>
        <translation>Diese Bestellung ist im falsches Zustand für Storno oder Sie haben nicht die Berechtigung ein Storno durchzuführen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="609"/>
        <source>This order contains a ticket that cannot be returned.</source>
        <translation>Diese Bestellung enthält eine Eintrittskarte, die nicht zurückgegeben werden kann.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="619"/>
        <source>This order contains a ticket that is for a past event and you do not have the privilege to cancel it.</source>
        <translation>Diese Bestellung enthält eine Eintrittskarte für eine vergangene Veranstaltung und Sie haben nicht die Berechtigung diese zu stornieren.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="628"/>
        <source>This order contains a voucher that has already been used.</source>
        <translation>Diese Bestellung enthält einen Gutschein der bereits benutzt wurde.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="662"/>
        <location filename="../inc/wext/order.php" line="709"/>
        <source>Invalid ticket ID.</source>
        <translation>Ungültige Eintrittskarte.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="668"/>
        <source>The ticket cannot be changed anymore or you do not have the privilege.</source>
        <translation>Diese Eintrittskarte kann nicht mehr geändert werden oder Sie haben nicht die notwendige Berechtigung.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="681"/>
        <source>The ticket is for an event in the past and you do not have the privilege to change it.</source>
        <translation>Diese Eintrittskarte ist für eine vergangene Veranstaltung und Sie haben nicht die Berechtigung solche Karten zu ändern.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="720"/>
        <source>Invalid price category.</source>
        <translation>Ungültige Preiskategorie.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="725"/>
        <source>Category is not valid for this event.</source>
        <translation>Diese Kategorie ist für diese Veranstaltung nicht vorgesehen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="731"/>
        <source>You do not have access to this category on this event.</source>
        <translation>Sie haben nicht die Berechtigung diese Preiskategorie für diese Veranstaltung zu verkaufen.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="739"/>
        <source>No more tickets left in this category.</source>
        <translation>Es gibt keine weiteren Karten in dieser Kategorie.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="783"/>
        <source>Internal error: negative voucher.</source>
        <translation>Interner Fehler: negativer Gutschein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="920"/>
        <source>Order does not exist.</source>
        <translation>Bestellung existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="932"/>
        <location filename="../inc/wext/order.php" line="950"/>
        <source>Invalid address ID</source>
        <translation>Ungültige Adresse (ID).</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="936"/>
        <location filename="../inc/wext/order.php" line="954"/>
        <source>Address does not match customer.</source>
        <translation>Diese Adresse gehört nicht zu diesem Kunden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="978"/>
        <source>Invalid Data</source>
        <translation>ungültige Bezahldaten</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="1011"/>
        <source>The payment type does not exist, cannot set it as default.</source>
        <translation>Dieser Bezahltyp existiert nicht, daher kann er nicht als Standard gesetzt werden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/order.php" line="1026"/>
        <source>User does not exist.</source>
        <translation>Dieser Nutzer existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="224"/>
        <source>Placed</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="225"/>
        <source>Sent</source>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="226"/>
        <source>Sold</source>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="227"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="228"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="229"/>
        <source>Closed</source>
        <translation>geschlossen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Order.php" line="556"/>
        <source>Unable to deserialize object of type WOOrder: invalid XML.</source>
        <translation>Das WOOrder Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOOrderAudit</name>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="90"/>
        <source>Placed</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="91"/>
        <source>Sent</source>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="92"/>
        <source>Sold</source>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="93"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="94"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="95"/>
        <source>Closed</source>
        <translation>geschlossen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderAudit.php" line="352"/>
        <source>Unable to deserialize object of type WOOrderAudit: invalid XML.</source>
        <translation>Das WOOrderAudit Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOOrderDocument</name>
    <message>
        <location filename="../inc/wob/wo_OrderDocument.php" line="166"/>
        <source>Unable to deserialize object of type WOOrderDocument: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOOrderDocumentInfo</name>
    <message>
        <location filename="../inc/wob/wo_OrderDocumentInfo.php" line="150"/>
        <source>Unable to deserialize object of type WOOrderDocumentInfo: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOOrderInfoAbstract</name>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="128"/>
        <source>Placed</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="129"/>
        <source>Sent</source>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="130"/>
        <source>Sold</source>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="131"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="132"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="133"/>
        <source>Closed</source>
        <translation>geschlossen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_OrderInfo.php" line="384"/>
        <source>Unable to deserialize object of type WOOrderInfo: invalid XML.</source>
        <translation>Das WOOrderInfo Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOPaymentType</name>
    <message>
        <location filename="../inc/wob/wo_PaymentType.php" line="127"/>
        <source>Unable to deserialize object of type WOPaymentType: invalid XML.</source>
        <translation>Das WOPaymentType Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOPriceCategoryAbstract</name>
    <message>
        <location filename="../inc/wext/price.php" line="14"/>
        <source>New Category must be a valid object.</source>
        <translation>Die neue Kategorie muss ein gültiges Objekt sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/price.php" line="31"/>
        <source>Category must be a valid object.</source>
        <translation>Die Kategorie muss ein gültiges Objekt sein.</translation>
    </message>
    <message>
        <location filename="../inc/wext/price.php" line="38"/>
        <source>Category does not exist.</source>
        <translation>Diese Kategorie existiert nicht.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_PriceCategory.php" line="158"/>
        <source>Unable to deserialize object of type WOPriceCategory: invalid XML.</source>
        <translation>Das WOPriceCategory Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WORoleAbstract</name>
    <message>
        <location filename="../inc/wob/wo_Role.php" line="114"/>
        <source>Unable to deserialize object of type WORole: invalid XML.</source>
        <translation>Das WORole Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WORoomAbstract</name>
    <message>
        <location filename="../inc/wext/room.php" line="15"/>
        <source>Room already exists</source>
        <translation>der Raum existiert bereits</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Room.php" line="94"/>
        <source>Unable to deserialize object of type WORoom: invalid XML.</source>
        <translation>Das WORoom Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlan</name>
    <message>
        <source>Unable to deserialize object of type WOSeatPlan: invalid XML.</source>
        <translation type="vanished">Das WOSeatPlan Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanAbstract</name>
    <message>
        <location filename="../inc/wext/seatplan.php" line="113"/>
        <source>VGroup violated - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wext/seatplan.php" line="145"/>
        <source>Not enough seats in groups </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_SeatPlan.php" line="250"/>
        <source>Unable to deserialize object of type WOSeatPlan: invalid XML.</source>
        <translation type="unfinished">Das WOSeatPlan Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanBackground</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanBackground.php" line="138"/>
        <source>Unable to deserialize object of type WOSeatPlanBackground: invalid XML.</source>
        <translation>Das WOSeatPlanBackground Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanDefPriceAbstract</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanDefPrice.php" line="69"/>
        <source>Unable to deserialize object of type WOSeatPlanDefPrice: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOSeatPlanGroup</name>
    <message>
        <source>Unable to deserialize object of type WOSeatPlanGroup: invalid XML.</source>
        <translation type="vanished">Das WOSeatPlanGroup Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanGroupAbstract</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanGroup.php" line="202"/>
        <source>Unable to deserialize object of type WOSeatPlanGroup: invalid XML.</source>
        <translation type="unfinished">Das WOSeatPlanGroup Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanImage</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanImage.php" line="56"/>
        <source>Unable to deserialize object of type WOSeatPlanImage: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOSeatPlanInfo</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanInfo.php" line="126"/>
        <source>Unable to deserialize object of type WOSeatPlanInfo: invalid XML.</source>
        <translation>Das WOSeatPlanInfo Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanRow</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanRow.php" line="178"/>
        <source>Unable to deserialize object of type WOSeatPlanRow: invalid XML.</source>
        <translation>Das WOSeatPlanRow Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOSeatPlanSeat</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanSeat.php" line="73"/>
        <source>Unable to deserialize object of type WOSeatPlanSeat: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOSeatPlanVGroupAbstract</name>
    <message>
        <location filename="../inc/wob/wo_SeatPlanVGroup.php" line="69"/>
        <source>Unable to deserialize object of type WOSeatPlanVGroup: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOServerFormatAbstract</name>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="293"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="382"/>
        <source>NoSign</source>
        <translation>kein Vorzeichen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="294"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="383"/>
        <source>SignBeforeNum</source>
        <translation>Vorzeichen vor der Zahl</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="295"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="384"/>
        <source>SignAfterNum</source>
        <translation>Vorzeichen nach der Zahl</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="296"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="385"/>
        <source>SignBeforeSym</source>
        <translation>Vorzeichen vor Währungszeichen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="297"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="386"/>
        <source>SignAfterSym</source>
        <translation>Vorzeichen nach Währungszeichen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="298"/>
        <location filename="../inc/wob/wo_ServerFormat.php" line="387"/>
        <source>SignParen</source>
        <translation>Klammern als Vorzeichen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_ServerFormat.php" line="519"/>
        <source>Unable to deserialize object of type WOServerFormat: invalid XML.</source>
        <translation>Das WOServerFormat Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOShippingAbstract</name>
    <message>
        <location filename="../inc/wext/shipping.php" line="23"/>
        <source>Shipping type not found.</source>
        <translation>Versandart wurde nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../inc/wext/shipping.php" line="30"/>
        <source>Error while updating shipping information.</source>
        <translation>Es ist ein Fehler beim ändern der Versandinformationen aufgetreten.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Shipping.php" line="128"/>
        <source>Unable to deserialize object of type WOShipping: invalid XML.</source>
        <translation>Das WOShipping Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOTemplateAbstract</name>
    <message>
        <location filename="../inc/wext/template.php" line="47"/>
        <location filename="../inc/wext/template.php" line="60"/>
        <location filename="../inc/wext/template.php" line="73"/>
        <source>No such template.</source>
        <translation>Unbekannte Vorlage.</translation>
    </message>
    <message>
        <location filename="../inc/wext/template.php" line="80"/>
        <source>Unable to set flags - DB error.</source>
        <translation>Kann Flags nicht setzen - Datenbankfehler.</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Template.php" line="175"/>
        <source>Unable to deserialize object of type WOTemplate: invalid XML.</source>
        <translation>Das WOTemplate Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOTemplateInfo</name>
    <message>
        <location filename="../inc/wob/wo_TemplateInfo.php" line="109"/>
        <source>Unable to deserialize object of type WOTemplateInfo: invalid XML.</source>
        <translation>Das WOTemplateInfo Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOTicketAbstract</name>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="98"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="99"/>
        <source>Ordered</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="100"/>
        <source>Used</source>
        <translation>Benutzt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="101"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="102"/>
        <source>Refund</source>
        <translation>Geldrückgabe</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="103"/>
        <source>MaskBlock</source>
        <translation>Maske: blockiert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="104"/>
        <source>MaskPay</source>
        <translation>Maske: bezahlen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="105"/>
        <source>MaskUsable</source>
        <translation>Maske: benutzbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="106"/>
        <source>MaskReturnable</source>
        <translation>Maske: rückgebbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="107"/>
        <source>MaskChangeable</source>
        <translation>Maske: änderbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Ticket.php" line="329"/>
        <source>Unable to deserialize object of type WOTicket: invalid XML.</source>
        <translation>Das WOTicket Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOTicketAuditAbstract</name>
    <message>
        <location filename="../inc/wob/wo_TicketAudit.php" line="145"/>
        <source>Unable to deserialize object of type WOTicketAudit: invalid XML.</source>
        <translation>Das WOTicketAudit Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOTicketSaleInfo</name>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="85"/>
        <source>Reserved</source>
        <translation type="unfinished">Reserviert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="86"/>
        <source>Ordered</source>
        <translation type="unfinished">bestellt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="87"/>
        <source>Used</source>
        <translation type="unfinished">Benutzt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="88"/>
        <source>Cancelled</source>
        <translation type="unfinished">storniert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="89"/>
        <source>Refund</source>
        <translation type="unfinished">Geldrückgabe</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="90"/>
        <source>MaskBlock</source>
        <translation type="unfinished">Maske: blockiert</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="91"/>
        <source>MaskPay</source>
        <translation type="unfinished">Maske: bezahlen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="92"/>
        <source>MaskUsable</source>
        <translation type="unfinished">Maske: benutzbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="93"/>
        <source>MaskReturnable</source>
        <translation type="unfinished">Maske: rückgebbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="94"/>
        <source>MaskChangeable</source>
        <translation type="unfinished">Maske: änderbar</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketSaleInfo.php" line="277"/>
        <source>Unable to deserialize object of type WOTicketSaleInfo: invalid XML.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WOTicketUse</name>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="63"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="64"/>
        <source>NotFound</source>
        <translation>nicht gefunden</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="65"/>
        <source>WrongEvent</source>
        <translation>Falsche Veranstaltung</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="66"/>
        <source>AlreadyUsed</source>
        <translation>bereits benutzt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="67"/>
        <source>NotUsable</source>
        <translation>kann nicht benutzt werden</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="68"/>
        <source>Unpaid</source>
        <translation>noch nicht bezahlt</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="69"/>
        <source>InvalidEvent</source>
        <translation>ungültige Veranstaltung</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_TicketUse.php" line="215"/>
        <source>Unable to deserialize object of type WOTicketUse: invalid XML.</source>
        <translation>Das WOTicketUse Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOUser</name>
    <message>
        <location filename="../inc/wob/wo_User.php" line="92"/>
        <source>Unable to deserialize object of type WOUser: invalid XML.</source>
        <translation>Das WOUser Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOVoucherAbstract</name>
    <message>
        <location filename="../inc/wob/wo_Voucher.php" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Voucher.php" line="80"/>
        <source>InvalidValue</source>
        <translation>Gutscheinwert nicht zulässig</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Voucher.php" line="81"/>
        <source>InvalidPrice</source>
        <translation>Gutscheinpreis nicht zulässig</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Voucher.php" line="82"/>
        <source>InvalidTime</source>
        <translation>Gutschein abgelaufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/wo_Voucher.php" line="242"/>
        <source>Unable to deserialize object of type WOVoucher: invalid XML.</source>
        <translation>Das WOVoucher Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOVoucherAudit</name>
    <message>
        <location filename="../inc/wob/wo_VoucherAudit.php" line="132"/>
        <source>Unable to deserialize object of type WOVoucherAudit: invalid XML.</source>
        <translation>Das WOVoucherAudit Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOWebCartAbstract</name>
    <message>
        <location filename="../inc/wob/wo_WebCart.php" line="335"/>
        <source>Unable to deserialize object of type WOWebCart: invalid XML.</source>
        <translation>Das WOWebCart Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WOWebSessionAbstract</name>
    <message>
        <location filename="../inc/wob/wo_WebSession.php" line="111"/>
        <source>Unable to deserialize object of type WOWebSession: invalid XML.</source>
        <translation>Das WOWebSession Objekt kann nicht deserialisiert werden: ungültiges XML.</translation>
    </message>
</context>
<context>
    <name>WebSite</name>
    <message>
        <location filename="../index.php" line="147"/>
        <source>An error occured, contact the server admin for details.</source>
        <translation>Ein Fehler ist aufgetreten, bitte kontaktieren Sie den Server-Admin um Details zu erfahren.</translation>
    </message>
</context>
<context>
    <name>_PrivilegeNames</name>
    <message>
        <location filename="../inc/wob/transaction.php" line="297"/>
        <source>ChangeEvent:CancelEvent</source>
        <translation>Veranstaltung absagen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="298"/>
        <source>CreateOrder:AnyVoucherValue</source>
        <translation>Bestellung anlegen: beliebige Gutscheinwerte erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="299"/>
        <source>CreateOrder:DiffVoucherValuePrice</source>
        <translation>Bestellung anlegen: Gutscheinpreis darf von Gutscheinwert abweichen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="300"/>
        <source>CreateOrder:LateSale</source>
        <translation>Bestellung anlegen: bis zu Veranstaltungsbeginn erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="301"/>
        <source>CreateOrder:AfterTheFactSale</source>
        <translation>Bestellung anlegen: auch nach der Veranstaltung erlauben (Adminfunktion)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="302"/>
        <source>CreateOrder:CanOrder</source>
        <translation>Bestellung anlegen: Nutzer darf bestellen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="303"/>
        <source>CreateOrder:CanSell</source>
        <translation>Bestellung anlegen: Nutzer darf verkaufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="304"/>
        <source>CreateOrder:CanOrderTicket</source>
        <translation>Bestellung anlegen: Nutzer darf Tickets verkaufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="305"/>
        <source>CreateOrder:CanOrderVoucher</source>
        <translation>Bestellung anlegen: Nutzer darf Gutscheine verkaufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="306"/>
        <source>CreateOrder:CanOrderItem</source>
        <translation>Bestellung anlegen: Nutzer darf Waren verkaufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="307"/>
        <source>CreateOrder:CanPayVoucherWithVoucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="308"/>
        <source>CreateReservation:LateReserve</source>
        <translation>Reservierung anlegen: bis Veranstaltungsbeginn erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="309"/>
        <source>CancelOrder:CancelSentOrder</source>
        <translation>Bestellung stornieren: auch für bereits versandte Bestellung</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="310"/>
        <source>CancelOrder:CancelPastTickets</source>
        <translation>Bestellung stornieren: auch für Bestellung mit Karten vergangener Veranstaltungen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="311"/>
        <source>UseVoucher:CanPayVoucherWithVoucher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="312"/>
        <source>OrderChangeShipping:ChangePrice</source>
        <translation>Versandoption einer Bestellung ändern: beliebigen Preis erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="313"/>
        <source>OrderMarkShipped:SetTime</source>
        <translation>Bestellung als verschickt markieren: beliebigen Zeitpunkt erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="314"/>
        <source>ReturnTicketVoucher:ReturnPastTicket</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben: auch abgelaufene Karten erlauben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="315"/>
        <source>ChangeTicketPrice:ChangeUsedTicket</source>
        <translation>Ticketpreis ändern: auch bereits genutzte Karten</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="316"/>
        <source>ChangeTicketPrice:ChangePastTicket</source>
        <translation>Ticketpreis ändern: auch abgelaufene Karten</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="317"/>
        <source>ChangeTicketPriceCategory:ChangeUsedTicket</source>
        <translation>Kartenkategorie ändern: Nutzer darf benutzte Karte ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="318"/>
        <source>ChangeTicketPriceCategory:ChangePastTicket</source>
        <translation>Kartenkategorie ändern: Nutzer darf Karte für Veranstaltung ändern, die schon vorbei ist</translation>
    </message>
</context>
<context>
    <name>_TransactionNames</name>
    <message>
        <location filename="../inc/wob/transaction.php" line="165"/>
        <source>Backup</source>
        <translation>Sicherungskopie anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="161"/>
        <source>GetLanguage</source>
        <translation>Übersetzung für Servermeldungen holen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="162"/>
        <source>GetValidFlags</source>
        <translation>alle gültigen Flags abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="163"/>
        <source>SetFlag</source>
        <translation>Flag anlegen/ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="164"/>
        <source>DeleteFlag</source>
        <translation>Flag löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="166"/>
        <source>BackupExplore</source>
        <translation>Backup-Index erfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="167"/>
        <source>BackupTable</source>
        <translation>Backup von Tabelle</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="168"/>
        <source>RestoreBackup</source>
        <translation>Backup wieder-herstellen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="169"/>
        <source>ServerInfo</source>
        <translation>Serverinformationen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="170"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="171"/>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="172"/>
        <source>GetMyRoles</source>
        <translation>meine Rollen herausfinden</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="173"/>
        <source>GetMyRights</source>
        <translation>meine Rechte herausfinden</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="174"/>
        <source>ChangeMyPassword</source>
        <translation>Mein Passwort ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="175"/>
        <source>GetAllUsers</source>
        <translation>Nutzer abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="176"/>
        <source>GetUser</source>
        <translation>Nutzerdaten abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="177"/>
        <source>CreateUser</source>
        <translation>Nutzer anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="178"/>
        <source>ChangePassword</source>
        <translation>Passwort eines anderen Nutzers ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="179"/>
        <source>DeleteUser</source>
        <translation>Nutzer löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="180"/>
        <source>SetUserDescription</source>
        <translation>Nutzerkommentar setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="181"/>
        <source>GetUserRoles</source>
        <translation>erlaubte Hosts eines Nutzers abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="182"/>
        <source>SetUserRoles</source>
        <translation>erlaubte Hosts eines Nutzers abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="183"/>
        <source>GetUserHosts</source>
        <translation>erlaubte Hosts eines Nutzers abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="184"/>
        <source>SetUserHosts</source>
        <translation>erlaubte Hosts eines Nutzers setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="185"/>
        <source>SetUserFlags</source>
        <translation>erlaubte Hosts eines Nutzers setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="186"/>
        <source>GetAllRoles</source>
        <translation>meine Rollen herausfinden</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="187"/>
        <source>GetRole</source>
        <translation>spezifische Rolle abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="188"/>
        <source>CreateRole</source>
        <translation>Rolle anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="189"/>
        <source>SetRoleDescription</source>
        <translation>Rollenkommentar setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="190"/>
        <source>SetRoleRights</source>
        <translation>Rollenrechte setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="191"/>
        <source>DeleteRole</source>
        <translation>Rolle löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="192"/>
        <source>SetRoleFlags</source>
        <translation>Flags der Rolle setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="193"/>
        <source>GetAllRightNames</source>
        <translation>Namen aller Rechte abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="194"/>
        <source>GetAllHostNames</source>
        <translation>Namen aller Hosts abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="195"/>
        <source>GetAllHosts</source>
        <translation>Alle Hosts (incl. Keys) abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="196"/>
        <source>SetHost</source>
        <translation>Host ändern/anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="197"/>
        <source>DeleteHost</source>
        <translation>Host löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="198"/>
        <source>GetAllContactTypes</source>
        <translation>Kontaktinformationstypen abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="199"/>
        <source>CreateContactType</source>
        <translation>Kontaktinformationstypen anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="200"/>
        <source>GetCustomer</source>
        <translation>Kunden abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="201"/>
        <source>GetAllCustomerNames</source>
        <translation>Alle Kundennamen abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="202"/>
        <source>CreateCustomer</source>
        <translation>Kunden anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="203"/>
        <source>ChangeCustomer</source>
        <translation>Kunden ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="204"/>
        <source>ChangeCustomerMail</source>
        <translation>Kunden-E-Mail-Adresse ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="205"/>
        <source>DeleteCustomer</source>
        <translation>Kunden löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="206"/>
        <source>GetAddress</source>
        <translation>Addresse abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="207"/>
        <source>GetAllCountries</source>
        <translation>gespeicherte Länder abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="208"/>
        <source>CreateCountry</source>
        <translation>Land anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="209"/>
        <source>ResetCustomerPassword</source>
        <translation>Kundenpasswort zurücksetzen (sendet Mail)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="210"/>
        <source>GetCreateCustomerHints</source>
        <translation>Editierhilfen für Kunden-Wizard abholen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="211"/>
        <source>GetAllArtists</source>
        <translation>Künstler abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="212"/>
        <source>CreateArtist</source>
        <translation>Künstler anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="213"/>
        <source>GetAllPriceCategories</source>
        <translation>Preiskategorien abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="214"/>
        <source>CreatePriceCategory</source>
        <translation>Preiskategorie anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="215"/>
        <source>ChangePriceCategory</source>
        <translation>Preiskategorie anpassen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="216"/>
        <source>GetEvent</source>
        <translation>Veranstaltungsdetails abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="217"/>
        <source>GetAllEvents</source>
        <translation>Liste der Veranstaltungen abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="218"/>
        <source>GetEventList</source>
        <translation>Liste der Veranstaltungen abfragen (spezifische Liste)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="219"/>
        <source>GetEventSaleInfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="220"/>
        <source>CreateEvent</source>
        <translation>Veranstaltung anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="221"/>
        <source>ChangeEvent</source>
        <translation>Veranstaltung ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="222"/>
        <source>CancelEvent</source>
        <translation>Veranstaltung absagen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="223"/>
        <source>GetAllRooms</source>
        <translation>Liste aller Räume abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="224"/>
        <source>CreateRoom</source>
        <translation>Raum anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="225"/>
        <source>GetAllSeatPlans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="226"/>
        <source>CreateSeatPlan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="227"/>
        <source>UpdateSeatPlan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="228"/>
        <source>GetEventSummary</source>
        <translation>Veranstaltungübersicht</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="229"/>
        <source>GetTicket</source>
        <translation>Ticket abrufen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="230"/>
        <source>GetVoucher</source>
        <translation>Gutschein abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="235"/>
        <source>GetMyOrders</source>
        <translation>Eigene Bestellungen ansehen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="236"/>
        <source>GetOrdersByUser</source>
        <translation>Bestellungen eines anderen Nutzers ansehen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="238"/>
        <source>GetOrdersByCoupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="248"/>
        <source>ChangeVoucherValidity</source>
        <translation>Gutschein-Gültigkeit ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="256"/>
        <source>ChangeOrderAddress</source>
        <translation>Adresse einer Bestellung ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="264"/>
        <source>GetPaymentTypes</source>
        <translation>Bezahlarten abholen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="265"/>
        <source>SetPaymentType</source>
        <translation>Bezahlart anlegen/ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="266"/>
        <source>SetDefaultPaymentType</source>
        <translation>Standard-Bezahlart festlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="267"/>
        <source>DeletePaymentType</source>
        <translation>Bezahlart löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="268"/>
        <source>GetOrderDocumentNames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="269"/>
        <source>GetOrderDocument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="270"/>
        <source>SetOrderDocument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="271"/>
        <source>DeleteOrderDocument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="272"/>
        <source>SendCustomerMail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="273"/>
        <source>GetPrintAtHomeSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="274"/>
        <source>SetPrintAtHomeSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="275"/>
        <source>GetCoupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="276"/>
        <source>GetCouponList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="277"/>
        <source>CreateCoupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="278"/>
        <source>ChangeCoupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="279"/>
        <source>GetTicketAudit</source>
        <translation>Auditierung nach Eintrittskarte</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="280"/>
        <source>GetVoucherAudit</source>
        <translation>Logdaten zu Gutschein abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="281"/>
        <source>GetOrderAudit</source>
        <translation>Logdaten zu Bestellung abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="287"/>
        <source>WebCartAddCoupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="293"/>
        <source>SetTemplateFlags</source>
        <translation>Falgs für Template setzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="231"/>
        <source>GetOrder</source>
        <translation>Bestellung: Details abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="232"/>
        <source>GetOrderList</source>
        <translation>Liste der Bestellungen abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="233"/>
        <source>GetOrdersByEvents</source>
        <translation>Bestellungen finden, die Veranstaltung enthalten</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="234"/>
        <source>GetOrdersByCustomer</source>
        <translation>Bestellungen finden, die zu einem Kunden gehören</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="237"/>
        <source>GetOrderByBarcode</source>
        <translation>Bestellung finden, die Eintrittskarte oder Gutschein enthält</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="239"/>
        <source>CreateOrder</source>
        <translation>Bestellung anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="240"/>
        <source>CreateReservation</source>
        <translation>Reservierung anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="241"/>
        <source>ReservationToOrder</source>
        <translation>Reservierung in Bestellung wandeln</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="242"/>
        <source>CancelOrder</source>
        <translation>Bestellung stornieren</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="243"/>
        <source>OrderPay</source>
        <translation>Bestellung bezahlen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="244"/>
        <source>OrderRefund</source>
        <translation>Bestellung: Geld zurück geben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="245"/>
        <source>UseVoucher</source>
        <translation>Gutschein benutzen (damit bezahlen)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="246"/>
        <source>DeductVoucher</source>
        <translation>Gutschein für Waren außerhalb MagicSmoke benutzen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="247"/>
        <source>EmptyVoucher</source>
        <translation>Gutschein ungültig machen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="249"/>
        <source>OrderChangeShipping</source>
        <translation>Versandoption einer Bestellung ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="250"/>
        <source>OrderMarkShipped</source>
        <translation>Bestellung als verschickt markieren</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="251"/>
        <source>OrderAddComment</source>
        <translation>Bestellkommentar (in angelegter Bestellung) hinzufügen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="252"/>
        <source>OrderChangeComments</source>
        <translation>Bestellkommentar (in angelegter Bestellung) ändern (Adminfunktion)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="253"/>
        <source>ReturnTicketVoucher</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="254"/>
        <source>ChangeTicketPrice</source>
        <translation>Ticketpreis ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="255"/>
        <source>ChangeTicketPriceCategory</source>
        <translation>Preiskategorie einer Karte ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="257"/>
        <source>GetAllShipping</source>
        <translation>Versandoptionen holen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="258"/>
        <source>ChangeShipping</source>
        <translation>Versandoptionsdaten ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="259"/>
        <source>CreateShipping</source>
        <translation>Versandoption anlegen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="260"/>
        <source>DeleteShipping</source>
        <translation>Versandoption löschen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="261"/>
        <source>GetValidVoucherPrices</source>
        <translation>Gutscheinpreise abfragen (zB. für Bestellformular)</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="262"/>
        <source>UseTicket</source>
        <translation>Ticket entwerten</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="263"/>
        <source>GetEntranceEvents</source>
        <translation>Liste der Veranstaltungen abfragen, die am Einlass relevant sind</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="282"/>
        <source>GetUserAudit</source>
        <translation>Auditierung nach Nutzer</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="283"/>
        <source>WebCartAddTicket</source>
        <translation>Nur Web: Karte zum Warenkorb hinzufügen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="284"/>
        <source>WebCartRemoveTicket</source>
        <translation>Nur Web: Karte aus Warenkorb entfernen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="285"/>
        <source>WebCartAddVoucher</source>
        <translation>Nur Web: Gutschein zum Warenkorb hinzufügen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="286"/>
        <source>WebCartRemoveVoucher</source>
        <translation>Nur Web: Gutschein aus Warenkorb entfernen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="288"/>
        <source>GetTemplateList</source>
        <translation>Vorlagenliste abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="289"/>
        <source>GetTemplate</source>
        <translation>Vorlage abfragen</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="290"/>
        <source>SetTemplate</source>
        <translation>Vorlage anlegen oder ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="291"/>
        <source>SetTemplateDescription</source>
        <translation>Beschreibung einer Vorlage ändern</translation>
    </message>
    <message>
        <location filename="../inc/wob/transaction.php" line="292"/>
        <source>DeleteTemplate</source>
        <translation>Vorlage löschen</translation>
    </message>
</context>
</TS>
