<?php
// (c) Konrad Rosenbaum, 2007-2011
// protected under the GNU AGPL version 3 or at your option any newer
// see COPYING.AGPL

include('inc/loader.php');

if(!$db->canAdministrate())
	die("Administration is turned off. Sorry.");
if(!$db->checkAdmin()){
	header("HTTP/1.0 401 Unauthorized");
	header("WWW-Authenticate: Basic realm=\"Smoke Admin Login\"");
	echo "Need to login in order to administrate.";
	exit;
}
#phpinfo();
function form($m="GET")
{
	if($m=="FILE"){
		print("<form method=\"POST\" enctype=\"multipart/form-data\" action=\"".$_SERVER["SCRIPT_NAME"]."\">\n");
// 		print("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".???
	}else
		print("<form action=\"".$_SERVER["SCRIPT_NAME"]."\" method=\"".$m."\">\n");
}

include_once('./inc/classes/random.php');
?>
<h1>Magic Smoke Admin Utility</h1>



<h2>Checking DB</h2>

<?php
if(isset($_GET["CreateDB"])){
	print("Creating Database...<p>");
	$db->createDb();
}
if(isset($_GET["UpgradeDB"])){
	print("Upgrading Database...<p>");
	$db->upgradeDb();
}
?>

<?php
$canUseDb=$db->canUseDb();
if($canUseDb){
	//DB exists, is usable, no strange buttons
?>
Database exists and is usable.<p>

<?php form() ?>
<input type="submit" value="Show SQL Script to create DB." name="ShowCreateDB"/>
</form>

<?php
}else{
	print("Current Status:<p><ul><li>Connected to DB: ".($db->isConnected()?"yes":"no")." <li>can use/upgrade current DB Schema: ".($db->canUseDb(false)?"yes":"no"));
	print(" <li>Config Table exists: ".($db->hasTable("config")?"yes":"no")." <li>DB Schema version=".$db->getConfig(WobSchema::configVersionRow()));
	print(" <li>DB name: ".$db->dbName()." <li>config table Name: ".$db->tableName("config")."</ul><p>\n");
	$cversion=$db->getConfig(WobSchema::configVersionRow());
	$xversion=$db->needVersion();
	if($db->canUseDb(false) && substr($cversion,0,3)=="01."){
		//DB exists, but is wrong (sub-)version
?>
Database is wrong version (is <?php echo $cversion ?>, expecting <?php echo $xversion ?>). Attempt automatic upgrade?<br/>
(<font color="red">Warning: make a backup of the database before attempting this!</font>)<br/>
<?php form() ?>
<input type="submit" value="Yes, Upgrade DB now." name="UpgradeDB"/>
<input type="submit" value="Show SQL Script to upgrade DB." name="ShowUpgradeDB"/>
<input type="submit" value="Show SQL Script to create DB." name="ShowCreateDB"/>
</form>
<?php
	}else{
		//DB does not exist yet
?>
Database is not usable. Create?<br>
<?php form() ?>
<input type="submit" value="Yes, Create DB now." name="CreateDB"/>
<input type="submit" value="Show SQL Script to create DB." name="ShowCreateDB"/>
</form>
<?php
	}
}
?>

<?php
if(isset($_GET["ShowCreateDB"])){
	print("Creating Database...<p>");
	$db->showCreateDb();
}
if(isset($_GET["ShowUpgradeDB"])){
	print("Upgrading Database...<p>");
	$db->upgradeDb(false);
}

//the stuff below does not work if DB unusable
if(!$canUseDb)
	exit();
?>

<h2>Restore Backups</h2>

<?php form("FILE") ?>
Backup file to restore:
<input type="file" name="restore"/><br/>
<input type="checkbox" name="overwrite" value="ovr">Overwrite existing data<br>
<input type="submit" name="restorenow" value="Restore Data Now"/>
</form><p>
<font color="red"><b>Warning:</b></font> don't attempt to restore data unless you really lost it! Ideally restore should only be made on an empty database.<p>

<?php
if(isset($_POST["restorenow"])){
	echo "<h3>Restoring Data...</h3>\n";
	$ov=false;
	if(isset($_POST["overwrite"]))$ov=($_POST["overwrite"]=="ovr");
	if(!is_uploaded_file($_FILES["restore"]["tmp_name"]))
		die("Trying to work on non-uploaded file. Abort.");
	$db->restoreData($_FILES["restore"]["tmp_name"],$ov);
	unlink($_FILES["restore"]["tmp_name"]);
}
?>

<h2>Restore from Old Database (MagicSmoke 1.x)</h2>

<?php if(isset($olddb)){ ?>
<?php form() ?>
Enter "upgrade" here: <input type="text" name="olddbup"/>
<input type="submit" value="Upgrade DB Now"/>
</form><p>
<font color="red"><b>Warning:</b></font> don't attempt to upgrade data unless you really mean it! Upgrade deletes all data from the new database (MagicSmoke2) and overwrites it with the old data (MagicSmoke1). This re-uses the credentials from the configuration.<p>

<?php if(isset($_GET["olddbup"]) && $_GET["olddbup"]=="upgrade"){
	echo "<h3>Upgrading Database...</h3>\n";
	DBUpgrade::upgrade();
} ?>

<?php }else{ //olddb not set ?>
If you want to upgrade from an old database version, please uncomment the $olddb-configuration in config.php.<p>
<?php } ?>


<h2>Checking for Admin Users</h2>

<?php
do{
if(isset($_POST["adminuser"])&&isset($_POST["adminpwd1"])&&isset($_POST["adminpwd2"])){
	//do passwords match?
	if($_POST["adminpwd1"]==""){
		print("Error: Cannot create user with empty password!<p>");
		break;
	}
	if($_POST["adminpwd1"]!=$_POST["adminpwd2"]){
		print("Error: Passwords do not match.<p>");
		break;
	}
	//does user exist?
	$un=$_POST["adminuser"];
	$usr=$db->select("user","uname","uname=".$db->escapeString($un));
	if(count($usr)>0){
		print("Error: User already exists.<p>");
		break;
	}
	//create user
	$salt=getSalt();
	$pwd=$salt." ".sha1($salt.$_POST["adminpwd1"]);
	$db->insert("user",array("uname"=>$un,"passwd"=>$pwd,"flags"=>"admin"));
	//make it admin
	$db->insert("userrole",array("uname"=>$un,"role"=>"_admin"));
	//allow it on all hosts
	$db->insert("userhost",array("uname"=>$un,"host"=>"_any"));
}
}while(0);

if(isset($_GET["addanyhost"])){
	$db->insert("userhost",array("uname"=>$_GET["addanyhost"], "host"=>"_any"));
}
?>

List of Admins:
<ul>
<?php
$admlst=$db->select("userrole","uname","role='_admin'");
for($i=0;$i<count($admlst);$i++){
	print("<li>".$admlst[$i][0]);
	$hst=$db->select("userhost","uname","host='_any' AND uname=".$db->escapeString($admlst[$i][0]));
	if(count($hst)<1)
		print(" <a href=\"admin.php?addanyhost=".urlencode($admlst[$i][0])."\">Add _any host.</a>");
	print("</li>\n");
}
?>
</ul><p>

<b>Create new Admin User:</b><br>
<form action="admin.php" method="POST">
<table>
<tr><td>Username:</td><td><input type="text" name="adminuser"></td></tr>
<tr><td>Password:</td><td><input type="password" name="adminpwd1"></td></tr>
<tr><td>Repeat Password:</td><td><input type="password" name="adminpwd2"></td></tr>
</table>
<input type="submit" value="Create">
</form><p/>

<h2>Checking for Hosts</h2>

<?php
if(isset($_POST["updatehost"])){
	if(!is_uploaded_file($_FILES["host"]["tmp_name"]))
		die("Trying to work on non-uploaded file. Abort.");
	$host=file($_FILES["host"]["tmp_name"]);
// 	print_r($host);
	if(count($host)<3)
		die("Trying to work on non-host file (<3 lines). Abort.");
	$ishash=trim($host[0])=="MagicSmokeHostHash";
	if(!$ishash && trim($host[0])!="MagicSmokeHostKey")
		die("Trying to work on non-host file (header mismatch). Abort.");
	if($ishash)
		$key=trim($host[2]);
	else{
		$salt=getSalt();
		$key=$salt." ".sha1($salt.trim($host[2]));
	}
	$hname=$db->escapeString(trim($host[1]));
// 	print_r($key);
	$data=array("hostname" => trim($host[1]), "hostkey" => $key);
	$res=$db->select("host","hostname","hostname=".$hname);
	if(count($res)>0)
		$db->update("host",$data,"hostname=".$hname);
	else
		$db->insert("host",$data);
	unlink($_FILES["host"]["tmp_name"]);
	print("<font color=\"green\">Successfully updated ".$host[1].".</font><p>\n");
}
?>

List of Hosts:
<ul>
<?php
$hlst=$db->select("host","hostname","");
for($i=0;$i<count($hlst);$i++){
	print("<li>".$hlst[$i][0]."</li>\n");
}
?>
</ul><p>

<b>Import Host File:</b><br>
<?php form("FILE"); ?>
<input type="file" name="host"><br>
<input type="submit" name="updatehost" value="Upload">
</form>

</html>
<?php
//done, thwart some stupid bots
exit(0);
//end of file
?>
