# Libraries for Magic Smoke by (c) Konrad Rosenbaum, 2007-2013

# PACK library
LIBS += -lqwbase
INCLUDEPATH += $$PWD/../pack/qtbase/include

# Time Zone DB library
LIBS += -lQtTzData
INCLUDEPATH += $$PWD/../tzone/include

# ELAM library
include($$PWD/../taurus/elam.pri)

# Chester DPtr library
include($$PWD/../taurus/chester.pri)

#make sure the correct Qt DLLs are used
QT += xml network
