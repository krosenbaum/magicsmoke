#Include PRI for the interface library non-GUI parts

LIBS += -lmagicsmoke-iface
INCLUDEPATH += \
        $$PWD/../iface/wob \
        $$PWD/../iface/wext \
        $$PWD/../iface/misc \
        $$PWD/../iface/templates \
        $$PWD/../iface

include ($$PWD/libs.pri)
