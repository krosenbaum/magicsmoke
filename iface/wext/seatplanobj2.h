//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2017
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOSEATPLANOBJ2_H
#define MAGICSMOKE_MOSEATPLANOBJ2_H

#include "MOSeatPlanAbstract"
#include <QSize>

class MSIFACE_EXPORT MOSeatPlan:public MOSeatPlanAbstract
{
	Q_GADGET
	WOBJECT(MOSeatPlan)
	public:
		bool isGraphical()const{return !size().isNull() && size().data().trimmed()!="";}
		QSize qSize()const{QStringList sz=size().data().trimmed().split(' ');if(sz.size()==2)return QSize(sz[0].toInt(),sz[1].toInt());else return QSize();}
};

Q_DECLARE_METATYPE(MOSeatPlan)
Q_DECLARE_METATYPE(QList<MOSeatPlan>)
Q_DECLARE_METATYPE(Nullable<MOSeatPlan>)

#endif
