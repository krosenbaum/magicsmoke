//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOORDERI_H
#define MAGICSMOKE_MOORDERI_H

#include "MOOrderInfoAbstract"
#include "misc.h"

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


class MSIFACE_EXPORT MOOrderInfo:public MOOrderInfoAbstract
{
	Q_GADGET
	WOBJECT(MOOrderInfo)
	Q_PROPERTY(bool isReservation READ isReservation)
	Q_PROPERTY(bool isSent READ isSent)
	Q_PROPERTY(bool isCancelled READ isCancelled)
	Q_PROPERTY(bool needsPayment READ needsPayment)
	Q_PROPERTY(bool needsRefund READ needsRefund)
	public:
		QString totalPriceString()const{return cent2str(totalprice());}
		QString amountPaidString()const{return cent2str(amountpaid());}
		QString amountDueString()const{return cent2str(amountdue());}
		QString orderStatusString()const{return OrderState2locstr(status());}
		
		bool needsPayment()const{return amountpaid()<totalprice();}
		bool needsRefund()const{return amountpaid()>totalprice();}
		bool isSent()const{return status()==Sent;}
		bool isReservation()const{return status()==Reserved;}
		bool isCancelled()const{return status()==Cancelled;}
		bool isPlaced()const{return status()==Placed;}
};

Q_DECLARE_METATYPE(MOOrderInfo)
Q_DECLARE_METATYPE(QList<MOOrderInfo>)
Q_DECLARE_METATYPE(Nullable<MOOrderInfo>)

#endif
