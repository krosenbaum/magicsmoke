//
// C++ Interface: MTransaction
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2013
//
// Copyright: See COPYING.GPL file that comes with this distribution
//
//

#ifndef MSMOKE_MTRANSACTION_H
#define MSMOKE_MTRANSACTION_H

#include <WTransaction>
#include <functional>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

typedef std::function<void()> MTStartStop;

class MSIFACE_EXPORT MTransaction:public WTransaction{
        public:
                ///set actions to be executed at start/stop
                static void setStartStopActions(const MTStartStop&start,const MTStartStop&stop);
	protected:
		/**internal: construct the transaction*/
		MTransaction(QString iface=QString());
		/**internal: copy the transaction*/
		MTransaction(const WTransaction&);
		/**internal: copy the transaction*/
		MTransaction(const MTransaction&);
		/**internal: extend executeQuery to show a wait cursor*/
		QByteArray executeQuery(QString,QByteArray);
};

#endif
