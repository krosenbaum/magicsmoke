//
// C++ Interface: MOVoucher
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOVOUCHER_H
#define MAGICSMOKE_MOVOUCHER_H

#include "MOVoucherAbstract"
#include "misc.h"

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


class MSIFACE_EXPORT MOVoucher:public MOVoucherAbstract
{
	Q_GADGET
	WOBJECT(MOVoucher)
	Q_PROPERTY(QString valueString READ valueString)
	Q_PROPERTY(QString statusString READ statusString)
	Q_PROPERTY(QString priceString READ priceString)
	public:
		/**returns whether this is a valid voucher object (ie. it has a voucher ID)*/
		bool isValid()const{return !voucherid().isNull();}
		/**returns the current value as string*/
		QString valueString()const{return cent2str(value());}
		/**returns the status of the voucher as string*/
		QString statusString()const{return VoucherState2locstr(status());}
		/**returns the price as string*/
		QString priceString()const{return cent2str(price());}
		///returns the validity date
		QString validDate()const{if(validtime().isNull())return tr("unlimited"); else return unix2date(validtime());}
};

Q_DECLARE_METATYPE(MOVoucher)
Q_DECLARE_METATYPE(QList<MOVoucher>)
Q_DECLARE_METATYPE(Nullable<MOVoucher>)

#endif
