//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2016
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_KEYVALUE_H
#define MAGICSMOKE_KEYVALUE_H

#include "MOKeyValuePairAbstract"
#include "misc.h"

#include <TimeStamp>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


/**this class represents a key value pair or part of a map/dictionary.*/
class MSIFACE_EXPORT MOKeyValuePair:public MOKeyValuePairAbstract
{
	Q_GADGET
	WOBJECT(MOKeyValuePair)
public:
	MOKeyValuePair(QString k,QString v){setkey(k);setvalue(v);setisnull(false);}
	MOKeyValuePair(QString k,std::nullptr_t){setkey(k);setisnull(true);}
	
	inline operator QPair<QString,QString> ()
	{
		return QPair<QString,QString>(key(),value());
	}

};



Q_DECLARE_METATYPE(MOKeyValuePair)
Q_DECLARE_METATYPE(QList<MOKeyValuePair>)
Q_DECLARE_METATYPE(Nullable<MOKeyValuePair>)

#include <QPair>
#include <QMap>

inline
QMap<QString,QString>& mergeToMap(QMap<QString,QString>&map, const QList<MOKeyValuePair>pl)
{
	for(auto p:pl)
		if(p.isnull())
			map.remove(p.key());
		else
			map.insert(p.key(),p.value());
	return map;
}

inline
QMap<QString,QString> toMap(const QList<MOKeyValuePair>pl)
{
	QMap<QString,QString>map;
	return mergeToMap(map,pl);
}

#endif
