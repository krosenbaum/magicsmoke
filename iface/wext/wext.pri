INCLUDEPATH += ./wext

HEADERS += \
	wext/customerinfo.h \
	wext/order.h \
	wext/orderinfo.h \
	wext/ticket.h \
	wext/voucher.h \
	wext/address.h \
	wext/event.h \
	wext/customer.h \
	wext/transaction.h \
	wext/keyvalue.h \
	wext/seatplanobj.h \
	wext/seatplanobj2.h \
	wext/eventsaleinfo.h

SOURCES += \
	wext/customerinfo.cpp \
	wext/order.cpp \
	wext/ticket.cpp \
	wext/address.cpp \
	wext/event.cpp \
	wext/customer.cpp \
	wext/transaction.cpp \
	wext/seatplanobj.cpp \
	wext/eventsaleinfo.cpp
