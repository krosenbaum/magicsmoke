//
// C++ Implementation: customer
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOCustomer"
#include "MOCustomerInfo"
#include "msinterface.h"

#include "MTGetCustomer"
#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOCustomer>()+
	qRegisterMetaType<QList<MOCustomer> >()+
	qRegisterMetaType<Nullable<MOCustomer> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOCustomer>,MOCustomer>([](const Nullable<MOCustomer>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOCustomer>,QVariantList>([](const QList<MOCustomer>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});


MOCustomer::MOCustomer(qint64 i)
{
	MTGetCustomer gc=req->queryGetCustomer(i);
	if(gc.stage()==gc.Success)
		operator=(gc.getcustomer().value());
}

MOCustomer::MOCustomer(const MOCustomerInfo&ci)
{
	MTGetCustomer gc=req->queryGetCustomer(ci.id());
	if(gc.stage()==gc.Success)
		operator=(gc.getcustomer().value());
}

QString MOCustomer::address(int i)
{
	if(i<0)return "";
	QList<MOAddress>adrs=addresses();
	if(i>=adrs.size())return "";
	return adrs[i].fullAddress(fullName());
}

QString MOCustomer::fullName()const
{
	QString ret=title().value().trimmed();
	QString s=name().value().trimmed();
	if(s!=""){
		if(ret!="")ret+=" ";
		ret+=s;
	}
	s=firstname().value().trimmed();
	if(s!=""){
		if(ret!="")ret+=", ";
		ret+=s;
	}
	return ret;
}
