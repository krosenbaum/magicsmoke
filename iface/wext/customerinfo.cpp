//
// C++ Implementation: MOCustomerInfo
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOCustomerInfo"
#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOCustomerInfo>()+
	qRegisterMetaType<QList<MOCustomerInfo> >()+
	qRegisterMetaType<Nullable<MOCustomerInfo> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOCustomerInfo>,MOCustomerInfo>([](const Nullable<MOCustomerInfo>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOCustomerInfo>,QVariantList>([](const QList<MOCustomerInfo>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

QString MOCustomerInfo::fullName()const
{
	QString ret=title().value().trimmed();
	QString s=name().value().trimmed();
	if(s!=""){
		if(ret!="")ret+=" ";
		ret+=s;
	}
	s=firstname().value().trimmed();
	if(s!=""){
		if(ret!="")ret+=", ";
		ret+=s;
	}
	return ret;
}
