//
// C++ Interface: MOAddress
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MADDRESS_H
#define MAGICSMOKE_MADDRESS_H

#include "MOAddressAbstract"

#include <QMetaType>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

class MSIFACE_EXPORT MOAddress:public MOAddressAbstract
{
	Q_GADGET
	WOBJECT(MOAddress)
	public:
		/**returns the full address, if name is given and the address does not have its own name, it is included as top line*/
		QString fullAddress(QString name=QString())const;
		
		/**returns true if this is a valid address stored in the DB (ie. it has an address ID)*/
		bool isValid()const{return !addressid().isNull() && addressid().value()>=0;}
};

Q_DECLARE_METATYPE(MOAddress)
Q_DECLARE_METATYPE(QList<MOAddress>)
Q_DECLARE_METATYPE(Nullable<MOAddress>)

#endif
