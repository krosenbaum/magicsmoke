//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOCUSTOMERI_H
#define MAGICSMOKE_MOCUSTOMERI_H

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


#include "MOCustomerInfoAbstract"
class MSIFACE_EXPORT MOCustomerInfo:public MOCustomerInfoAbstract
{
	Q_GADGET
	WOBJECT(MOCustomerInfo)
	public:
		/**returns the full name (incl. title)*/
		QString fullName()const;
		
		/**alias for customerid()*/
		Nullable<qint64> id()const{return customerid();}
};

Q_DECLARE_METATYPE(MOCustomerInfo)
Q_DECLARE_METATYPE(QList<MOCustomerInfo>)
Q_DECLARE_METATYPE(Nullable<MOCustomerInfo>)

#endif
