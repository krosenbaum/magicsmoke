//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOEVENTSALEINFO_H
#define MAGICSMOKE_MOEVENTSALEINFO_H

#include "MOEventSaleInfoAbstract"
#include "MOSeatPlan"

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif



//class MOSeatPlan;
/**this class represents a seat group*/
class MSIFACE_EXPORT MOEventSaleInfo:public MOEventSaleInfoAbstract
{
	Q_GADGET
	WOBJECT(MOEventSaleInfo)
	Q_PROPERTY(bool isValid READ isValid)
	Q_PROPERTY(MOSeatPlan seatplan READ seatplan)
	public:
		/**returns whether the object is valid (it comes from the DB and it has been understood by the parser)*/
		bool isValid()const{return !eventid().isNull();}

		///returns the parsed seat plan without sale info worked in
		MOSeatPlan seatplan()const;

		///returns the parsed seat plan with all the ticket info merged in
		MOSeatPlan seatplanAdjusted()const;
};

Q_DECLARE_METATYPE(MOEventSaleInfo)
Q_DECLARE_METATYPE(QList<MOEventSaleInfo>)
Q_DECLARE_METATYPE(Nullable<MOEventSaleInfo>)

#endif
