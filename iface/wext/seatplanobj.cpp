//
// C++ Implementation: MOOrder
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "seatplanobj.h"

#include "MOEventSaleInfo"
#include "MOEventPrice"

#include <QVariant>

#include <limits>

static int mymeta=
	qRegisterMetaType<MOSeatPlanDefPrice>()+
	qRegisterMetaType<QList<MOSeatPlanDefPrice> >()+
	qRegisterMetaType<Nullable<MOSeatPlanDefPrice> >()+
	qRegisterMetaType<MOSeatPlanGroup>()+
	qRegisterMetaType<QList<MOSeatPlanGroup> >()+
	qRegisterMetaType<Nullable<MOSeatPlanGroup> >()+
	qRegisterMetaType<MOSeatPlanVGroup>()+
	qRegisterMetaType<QList<MOSeatPlanVGroup> >()+
	qRegisterMetaType<Nullable<MOSeatPlanVGroup> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOSeatPlanDefPrice>,MOSeatPlanDefPrice>([](const Nullable<MOSeatPlanDefPrice>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOSeatPlanDefPrice>,QVariantList>([](const QList<MOSeatPlanDefPrice>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<Nullable<MOSeatPlanGroup>,MOSeatPlanGroup>([](const Nullable<MOSeatPlanGroup>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOSeatPlanGroup>,QVariantList>([](const QList<MOSeatPlanGroup>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<Nullable<MOSeatPlanVGroup>,MOSeatPlanVGroup>([](const Nullable<MOSeatPlanVGroup>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOSeatPlanVGroup>,QVariantList>([](const QList<MOSeatPlanVGroup>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});


static inline
QList<MOEventPrice> getcategories(const MOEventSaleInfo&ei,QString spec)
{
	const QStringList specl=spec.split(' ',Qt::SkipEmptyParts);
	QList<MOEventPrice>prc;
	QList<int>ids;
	QStringList abbrs;
	for(QString sp:specl){
		bool isint=false;
		const int id=sp.toInt(&isint);
		if(isint)ids<<id;
		else abbrs<<sp;
	}
	for(const MOEventPrice&ep:ei.price())
		if(ids.contains(ep.pricecategoryid())||abbrs.contains(ep.pricecategory().value().abbreviation()))
			prc.append(ep);
	return prc;
}

void MOSeatPlanVGroup::adjust(const MOEventSaleInfo&ei)
{
	//retrieve categories
	int max=-1,min=std::numeric_limits<int>::max(),sum=0;
	QList<MOEventPrice>cats=getcategories(ei,price());
	if(cats.size()==0)max=min=0;
	for(const MOEventPrice&c:cats){
		if(c.maxavailable()>max)max=c.maxavailable();
		if(c.maxavailable()<min)min=c.maxavailable();
		sum+=c.maxavailable();
		mcategoryids<<c.pricecategoryid();
		mcategoryabbr<<c.pricecategory().value().abbreviation();
	}
	//adjust capacity
	bool isint=false;
	const QString cap=capacity().value();
	mcapacity=cap.toInt(&isint);
	if(!isint){
		if(cap=="maxcat")mcapacity=max;
		else if(cap=="mincat")mcapacity=min;
		else if(cap=="sumcat")mcapacity=sum;
		//ignore auto and other values
	}
	//count tickets
}

void MOSeatPlanGroup::adjust(const MOEventSaleInfo&ei)
{
}

static inline
QRect str2rect(QString s)
{
	QList<int>il;
	for(QString c:s.split(' ',Qt::SkipEmptyParts))
		il<<c.toInt();
	if(il.size()!=4)return QRect();
	return QRect(il[0],il[1],il[2],il[3]);
}

QRect MOSeatPlanGroup::geometry() const
{
	return str2rect(geo());
}

