//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2017
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOSEATPLANOBJ_H
#define MAGICSMOKE_MOSEATPLANOBJ_H

#include "MOSeatPlanDefPriceAbstract"
#include "MOSeatPlanGroupAbstract"
#include "MOSeatPlanVGroupAbstract"
#include "misc.h"

#include <QRect>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

class MOEventSaleInfo;

/**this class represents a default price category definition*/
class MSIFACE_EXPORT MOSeatPlanDefPrice:public MOSeatPlanDefPriceAbstract
{
	Q_GADGET
	WOBJECT(MOSeatPlanDefPrice)
	Q_PROPERTY(bool isValid READ isValid)
	public:
		/**returns whether the object is valid (it comes from the DB and it has been understood by the parser)*/
		bool isValid()const{return !capacity().isNull() && !price().isNull() && !cost().isNull();}
};

Q_DECLARE_METATYPE(MOSeatPlanDefPrice)
Q_DECLARE_METATYPE(QList<MOSeatPlanDefPrice>)
Q_DECLARE_METATYPE(Nullable<MOSeatPlanDefPrice>)


/**this class represents a seat group*/
class MSIFACE_EXPORT MOSeatPlanGroup:public MOSeatPlanGroupAbstract
{
	Q_GADGET
	WOBJECT(MOSeatPlanGroup)
	Q_PROPERTY(bool isValid READ isValid)
	Q_PROPERTY(int capacityNum READ capacityNum)
	Q_PROPERTY(QRect geometry READ geometry)
	Q_PROPERTY(QList<int> categoryIds READ categoryIds)
	Q_PROPERTY(QStringList categoryAbbreviations READ categoryAbbreviations)
	Q_PROPERTY(int numReservedTickets READ numReservedTickets)
	Q_PROPERTY(int numOrderedTickets READ numOrderedTickets)
	Q_PROPERTY(int numUsedTickets READ numUsedTickets)
	Q_PROPERTY(int numCancelledTickets READ numCancelledTickets)
	Q_PROPERTY(int numBlockedSeats READ numBlockedSeats)
	public:
		/**returns whether the object is valid (it comes from the DB and it has been understood by the parser)*/
		bool isValid()const{return !capacity().isNull() && !price().isNull() && !id().isNull();}

		///numeric capacity
		int capacityNum()const{return mcapacity;}

		///geometry as a rect
		QRect geometry()const;

		///IDs of categories in this group
		QList<int> categoryIds()const{return mcategoryids;}

		///Abbreviations of categories in this group
		QStringList categoryAbbreviations()const{return mcategoryabbr;}

		///number of reserved tickets
		int numReservedTickets()const{return mreserve;}

		///number of ordered (not yet used) tickets
		int numOrderedTickets()const{return mordered;}

		///number of used tickets
		int numUsedTickets()const{return mused;}

		///number of cancelled tickets
		int numCancelledTickets()const{return mcancelled;}

		///number of blocked seats (reserved, ordered, used tickets)
		int numBlockedSeats()const{return mblocked;}

	private:
		friend class MOEventSaleInfo;
		void adjust(const MOEventSaleInfo&);

		QList<int> mcategoryids;
		QStringList mcategoryabbr;
		int mreserve=0,mordered=0,mused=0,mcancelled=0,mblocked=0,mcapacity=0;
};

Q_DECLARE_METATYPE(MOSeatPlanGroup)
Q_DECLARE_METATYPE(QList<MOSeatPlanGroup>)
Q_DECLARE_METATYPE(Nullable<MOSeatPlanGroup>)



/**this class represents a seat group*/
class MSIFACE_EXPORT MOSeatPlanVGroup:public MOSeatPlanVGroupAbstract
{
	Q_GADGET
	WOBJECT(MOSeatPlanVGroup)
	Q_PROPERTY(bool isValid READ isValid)
	Q_PROPERTY(int capacityNum READ capacityNum)
	Q_PROPERTY(QList<int> categoryIds READ categoryIds)
	Q_PROPERTY(QStringList categoryAbbreviations READ categoryAbbreviations)
	Q_PROPERTY(int numReservedTickets READ numReservedTickets)
	Q_PROPERTY(int numOrderedTickets READ numOrderedTickets)
	Q_PROPERTY(int numUsedTickets READ numUsedTickets)
	Q_PROPERTY(int numCancelledTickets READ numCancelledTickets)
	Q_PROPERTY(int numBlockedSeats READ numBlockedSeats);
	public:
		/**returns whether the object is valid (it comes from the DB and it has been understood by the parser)*/
		bool isValid()const{return !capacity().isNull() && !price().isNull();}

		///numeric capacity
		int capacityNum()const{return mcapacity;}

		///IDs of categories in this group
		QList<int> categoryIds()const{return mcategoryids;}

		///Abbreviations of categories in this group
		QStringList categoryAbbreviations()const{return mcategoryabbr;}

		///number of reserved tickets
		int numReservedTickets()const{return mreserve;}

		///number of ordered (not yet used) tickets
		int numOrderedTickets()const{return mordered;}

		///number of used tickets
		int numUsedTickets()const{return mused;}

		///number of cancelled tickets
		int numCancelledTickets()const{return mcancelled;}

		///number of blocked seats (reserved, ordered, used tickets)
		int numBlockedSeats()const{return mblocked;}

	private:
		friend class MOEventSaleInfo;
		void adjust(const MOEventSaleInfo&);

		QList<int> mcategoryids;
		QStringList mcategoryabbr;
		int mreserve=0,mordered=0,mused=0,mcancelled=0,mblocked=0,mcapacity=0;
};

Q_DECLARE_METATYPE(MOSeatPlanVGroup)
Q_DECLARE_METATYPE(QList<MOSeatPlanVGroup>)
Q_DECLARE_METATYPE(Nullable<MOSeatPlanVGroup>)


#endif
