//
// C++ Implementation: event
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOEvent"

#include <QCoreApplication>
#include <QRegExp>
#include <QStringList>

#include "msinterface.h"
#include "MTGetEvent"

#include "misc.h"
#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOEvent>()+
	qRegisterMetaType<QList<MOEvent> >()+
	qRegisterMetaType<Nullable<MOEvent> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOEvent>,MOEvent>([](const Nullable<MOEvent>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOEvent>,QVariantList>([](const QList<MOEvent>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

MOEvent::MOEvent(qint64 i)
{
	MTGetEvent ge=MSInterface::instance()->queryGetEvent(i);
	if(ge.stage()==ge.Success)
		operator=(ge.getevent().value());
}

QRegularExpression MOEvent::priceRegExp()const
{
	return ::priceRegExp();
}

QString MOEvent::priceString()const
{
	qint64 dp=/*defaultprice()*/0;
	QString ret=QString::number(dp/100);
	ret+=QCoreApplication::translate("MOEvent",".","price decimal dot");
	ret+=QString::number((dp/10)%10);
	ret+=QString::number(dp%10);
	return ret;
}

QString MOEvent::startTimeString()const
{
	 return unix2dateTime(start());
}

QString MOEvent::startDateString()const
{
	 return unix2date(start());
}

QString MOEvent::endTimeString()const
{
	 return unix2dateTime(end());
}

QList< MOEventPrice > MOEvent::priceFiltered() const
{
	//get prices
	QList<MOEventPrice> ep=price();
	QList<MOEventPrice> ret;
	//filter for access
	MSInterface *ifc=MSInterface::instance();
	if(ifc!=0)
	for(MOEventPrice p:ep){
		if(ifc->checkFlags(p.flags()))
			ret<<p;
	}
	//sort by priority
	std::sort(ret.begin(),ret.end(),[](const MOEventPrice&a,const MOEventPrice&b){return a.prio()<b.prio();});
	return ret;
}
