//
// C++ Implementation: MOOrder
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOOrder"
#include "MOOrderInfo"
#include "MORole"

#include "MTGetOrder"

#include "msinterface.h"
#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOOrder>()+
	qRegisterMetaType<QList<MOOrder> >()+
	qRegisterMetaType<Nullable<MOOrder> >()+
	qRegisterMetaType<MOOrderInfo>()+
	qRegisterMetaType<QList<MOOrderInfo> >()+
	qRegisterMetaType<Nullable<MOOrderInfo> >()+
	qRegisterMetaType<MORole>()+
	qRegisterMetaType<QList<MORole> >()+
	qRegisterMetaType<Nullable<MORole> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOOrder>,MOOrder>([](const Nullable<MOOrder>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOOrder>,QVariantList>([](const QList<MOOrder>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<Nullable<MOOrderInfo>,MOOrderInfo>([](const Nullable<MOOrderInfo>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOOrderInfo>,QVariantList>([](const QList<MOOrderInfo>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<Nullable<MORole>,MORole>([](const Nullable<MORole>&n){return n.value();})|
	QMetaType::registerConverter<QList<MORole>,QVariantList>([](const QList<MORole>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

MOOrder::MOOrder(qint64 id)
{
	MTGetOrder go=req->queryGetOrder(id);
	if(go.stage()==go.Success)
		operator=(go.getorder().value());
}

bool MOOrder::needsPayment()const
{
	if(status()==Placed || status()==Sent)
		if(amountpaid()<totalprice())return true;
	return false;
}

bool MOOrder::needsRefund()const
{
	if(status()==Cancelled)
		return amountpaid()>0;
	if(status()==Placed || status()==Sent)
		return amountpaid()>totalprice();
	return false;
}

int MOOrder::amountToPay()const
{
	if(status()==Placed || status()==Sent)
		if(amountpaid()<totalprice())return totalprice()-amountpaid();
	return 0;
}

int MOOrder::amountToRefund()const
{
	if(status()==Cancelled)
		return amountpaid();
	if(status()==Placed || status()==Sent)
		if(amountpaid()>totalprice())
			return amountpaid()-totalprice();
	return 0;
}

bool MOOrder::isSent()const
{
	//only in placed mode there is a need for action, hence in all other modes we assume sent
	return status()!=Placed;
}

TimeStamp MOOrder::orderDateTime()
{
	if(ordertime().value()==0)return TimeStamp();
	return TimeStamp(ordertime());
}

QString MOOrder::orderDateTimeStr()
{
	if(ordertime().value()==0)return "";
	return unix2dateTime(ordertime());
}

QString MOOrder::orderDateStr()
{
	if(ordertime().value()==0)return "";
	return unix2date(ordertime());
}

TimeStamp MOOrder::sentDateTime()
{
	return TimeStamp(senttime());
}

QString MOOrder::sentDateTimeStr()
{
	if(senttime().value()==0)return "";
	return unix2dateTime(senttime());
}

QString MOOrder::sentDateStr()
{
	if(senttime().value()==0)return "";
	return unix2date(senttime());
}

QString MOOrder::fullInvoiceAddress(bool allowfallback)const
{
	if(!invoiceaddress().isNull() && invoiceaddress().value().isValid())
		return invoiceaddress().value().fullAddress(customer().value().fullName());
	//fall back
	if(!allowfallback)return "";
	if(!deliveryaddress().isNull() && deliveryaddress().value().isValid())
		return deliveryaddress().value().fullAddress(customer().value().fullName());
	//give up
	return "";
}

QString MOOrder::fullDeliveryAddress(bool allowfallback)const
{
	if(!deliveryaddress().isNull() && deliveryaddress().value().isValid())
		return deliveryaddress().value().fullAddress(customer().value().fullName());
	//fall back
	if(!allowfallback)return "";
	if(!invoiceaddress().isNull() && invoiceaddress().value().isValid())
		return invoiceaddress().value().fullAddress(customer().value().fullName());
	//give up
	return "";
}
