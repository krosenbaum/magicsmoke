//
// C++ Implementation: MOOrder
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "eventsaleinfo.h"

#include "MOSeatPlan"
#include "MOSeatPlanGroup"
#include "MOSeatPlanVGroup"

#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOEventSaleInfo>()+
	qRegisterMetaType<QList<MOEventSaleInfo> >()+
	qRegisterMetaType<Nullable<MOEventSaleInfo> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOEventSaleInfo>,MOEventSaleInfo>([](const Nullable<MOEventSaleInfo>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOEventSaleInfo>,QVariantList>([](const QList<MOEventSaleInfo>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

MOSeatPlan MOEventSaleInfo::seatplan()const
{
	return MOSeatPlan::fromString(seatplaninfo().value().plan());
}

MOSeatPlan MOEventSaleInfo::seatplanAdjusted()const
{
	MOSeatPlan plan=MOSeatPlan::fromString(seatplaninfo().value().plan());
	QList<MOSeatPlanGroup>grp;
	for(auto g:plan.Group()){
		g.adjust(*this);
		grp.append(g);
	}
	plan.setGroup(grp);
	QList<MOSeatPlanVGroup>vgrp;
	for(auto g:plan.VGroup()){
		g.adjust(*this);
		vgrp.append(g);
	}
	plan.setVGroup(vgrp);
	return plan;
}
