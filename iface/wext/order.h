//
// C++ Interface: unabstract
//
// Description: removes abstract flag from classes that only need to be abstract in PHP
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOORDER_H
#define MAGICSMOKE_MOORDER_H

#include "MOOrderAbstract"
#include "misc.h"

#include <TimeStamp>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


/**this class represents a complete order*/
class MSIFACE_EXPORT MOOrder:public MOOrderAbstract
{
	Q_GADGET
	WOBJECT(MOOrder)
	Q_PROPERTY(bool isValid READ isValid)
	Q_PROPERTY(bool isReservation READ isReservation)
	Q_PROPERTY(bool isSent READ isSent)
	Q_PROPERTY(QString orderStatusString READ orderStatusString)
	Q_PROPERTY(QString totalPriceString READ totalPriceString)
	Q_PROPERTY(bool needsPayment READ needsPayment)
	Q_PROPERTY(bool needsRefund READ needsRefund)
	Q_PROPERTY(int amountToPay READ amountToPay)
	Q_PROPERTY(QString amountToPayStr READ amountToPayStr)
	Q_PROPERTY(int amountToRefund READ amountToRefund)
	Q_PROPERTY(QString amountToRefundStr READ amountToRefundStr)
	Q_PROPERTY(QString orderDateTimeStr READ orderDateTimeStr)
	Q_PROPERTY(TimeStamp oderDateTime READ orderDateTime)
	Q_PROPERTY(TimeStamp sentDateTime READ sentDateTime)
	Q_PROPERTY(QString sentDateTimeStr READ sentDateTimeStr)
	Q_PROPERTY(QString orderDateStr READ orderDateStr)
	Q_PROPERTY(QString sentDateStr READ sentDateStr)
	Q_PROPERTY(QString fullInvoiceAddress READ fullInvoiceAddress)
	Q_PROPERTY(QString fullDeliveryAddress READ fullDeliveryAddress)
	public:
		/**create order by id, retrieves it automatically from the database*/
		MOOrder(qint64);

		/**returns whether the order is valid (it comes from the DB and it has been understood by the parser)*/
		bool isValid()const{return !orderid().isNull();}
		
		/**returns whether this order is a reservation*/
		bool isReservation()const{return status()==Reserved;}
		
		/**returns whether the tickets of this order have already been shipped*/
		bool isSent()const;
		
		/**returns the status of the order as localized string*/
		QString orderStatusString()const{return OrderState2locstr(status());}

		/**returns how much money needs to be paid in total for this order, in cents*/
		QString totalPriceString()const{return cent2str(totalprice());}
		
		/**returns how much money has already been paid for this order, in cents*/
		QString amountPaidString()const{return cent2str(amountpaid());}
		
		/**returns whether there is anything left to pay*/
		bool needsPayment()const;
		
		/**returns whether there is anything left to refund*/
		bool needsRefund()const;
		
		/**returns how much there is to be paid, in cents*/
		int amountToPay()const;
		
		/**returns how much there is to be paid, as localized string*/
		QString amountToPayStr()const{return cent2str(amountToPay());}
		
		/**returns how much there is to be refunded, in cents*/
		int amountToRefund()const;
		
		/**returns how much there is to be refunded, as localized string*/
		QString amountToRefundStr()const{return cent2str(amountToRefund());}
		
		/**return the order date+time*/
		TimeStamp orderDateTime();
		
		/**returns the order date+time as string*/
		QString orderDateTimeStr();
		
		/**returns the order date only as string*/
		QString orderDateStr();
		
		/**returns the shipping date+time*/
		TimeStamp sentDateTime();
		
		/**returns the shipping date+time as string*/
		QString sentDateTimeStr();
		
		/**returns the shipping date only as string*/
		QString sentDateStr();
		
		/**returns the full invoice address, or delivery address if no explicit invoice address was given and allowfallback==true*/
		QString fullInvoiceAddress(bool allowfallback=true)const;
		/**returns the full delivery address, or invoice address if no explicit delivery address was given and allowfallback==true*/
		QString fullDeliveryAddress(bool allowfallback=true)const;
};

Q_DECLARE_METATYPE(MOOrder)
Q_DECLARE_METATYPE(QList<MOOrder>)
Q_DECLARE_METATYPE(Nullable<MOOrder>)

#endif
