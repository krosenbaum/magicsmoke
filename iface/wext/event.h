//
// C++ Interface: event
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2007-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef EVENT_H
#define EVENT_H

#include <QString>
#include <MOEventAbstract>
#include <QRegularExpression>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


/**encapsulation of an event, this class wraps the auto-generated event class to provide some convenience methods*/
class MSIFACE_EXPORT MOEvent:public MOEventAbstract
{
	Q_GADGET
	WOBJECT(MOEvent)
	Q_PROPERTY(Nullable<qint64> eventid READ eventid)
	Q_PROPERTY(QString startTimeString READ startTimeString)
	Q_PROPERTY(QString startDateString READ startDateString)
	Q_PROPERTY(QString endTimeString READ endTimeString)
	Q_PROPERTY(QString priceString READ priceString)
	Q_PROPERTY(QRegularExpression priceRegExp READ priceRegExp)
	Q_PROPERTY(bool isValid READ isValid)
	Q_PROPERTY(QList<MOEventPrice> priceFiltered READ priceFiltered)
	public:
		/**get event directly from server*/
		MOEvent(qint64);

		/**alias for id()*/
		inline Nullable<qint64> eventid()const{return id();}

		/**returns the start time of the event as localized string*/
		QString startTimeString()const;
		/**returns the start date as localized string*/
		QString startDateString()const;
		/**returns the end time of the event as localized string*/
		QString endTimeString()const;

		/**returns the price as a localized string*/
		QString priceString()const;
		/**returns the local regular expression for prices*/
		QRegularExpression priceRegExp()const;

		/**returns whether the event is valid. an event can be invalid if it is uninitialized (negative ID) or the server request failed*/
		bool isValid()const{return !eventid().isNull() && eventid().value()>0;}

		///returns only the prices that this user can order for
		QList<MOEventPrice> priceFiltered()const;
};

Q_DECLARE_METATYPE(MOEvent)
Q_DECLARE_METATYPE(QList<MOEvent>)
Q_DECLARE_METATYPE(Nullable<MOEvent>)

#endif
