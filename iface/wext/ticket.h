//
// C++ Interface: MOTicket
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOTICKET_H
#define MAGICSMOKE_MOTICKET_H

#include "MOTicketAbstract"
#include "misc.h"
#include "MOEvent"

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


class MSIFACE_EXPORT MOTicket:public MOTicketAbstract
{
	Q_GADGET
	WOBJECT(MOTicket)
	Q_PROPERTY(int amountToPay READ amountToPay)
	Q_PROPERTY(QString priceString READ priceString)
	Q_PROPERTY(QString statusString READ statusString)
	Q_PROPERTY(QString priceCategoryName READ priceCategoryName)
	Q_PROPERTY(QString priceCategoryShort READ priceCategoryShort)
	public:
		/**returns the amount to be paid for this ticket; this may be 0 even if there is a price attached*/
		int amountToPay()const{if(status()&MaskPay)return price();else return 0;}
		
		/**returns the price as string*/
		QString priceString()const{return cent2str(price());}
		
		/**returns the ticket status as localized string*/
		QString statusString()const{return TicketState2locstr(status());}
		
		/**returns the full price category name*/
		QString priceCategoryName()const;
		/**returns the abbreviation for the price category*/
		QString priceCategoryShort()const;
		
		/**returns the event that this ticket belongs to - does a roundtrip to the database the first time this is called!*/
		MOEvent event()const;
		
		/**used by label editor: sets the simulated event*/
		void setEvent(const MOEvent&e);
	private:
		//this must be mutable, since event() is semantically and technically const
		mutable MOEvent m_event;
};

Q_DECLARE_METATYPE(MOTicket)
Q_DECLARE_METATYPE(QList<MOTicket>)
Q_DECLARE_METATYPE(Nullable<MOTicket>)

#endif
