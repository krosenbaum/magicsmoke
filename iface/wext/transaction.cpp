//
// C++ Implementation: MTransaction
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2013
//
// Copyright: See COPYING.GPL file that comes with this distribution
//
//

#include "MTransaction"

#include "cleanup.h"

static QList<MTStartStop>& startFunctions()
{
        static QList<MTStartStop>f;
        return f;
}

static QList<MTStartStop>& stopFunctions()
{
        static QList<MTStartStop>f;
        return f;
}

QByteArray MTransaction::executeQuery(QString hreq,QByteArray data)
{
	//show the user we are waiting
	for(MTStartStop f:startFunctions())f();
        MCleanup c([]{for(MTStartStop f:stopFunctions())f();});
	//call parent
	return WTransaction::executeQuery(hreq,data);
}

MTransaction::MTransaction(QString iface):WTransaction(iface){}
MTransaction::MTransaction(const WTransaction&t):WTransaction(t){}
MTransaction::MTransaction(const MTransaction&t):WTransaction(t){}

void MTransaction::setStartStopActions(const MTStartStop& start, const MTStartStop& stop)
{
        startFunctions().append(start);
        stopFunctions().append(stop);
}
