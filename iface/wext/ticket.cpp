//
// C++ Implementation: MOTicket
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOTicket"
#include "MOVoucher"
#include "msinterface.h"
#include "MTGetEvent"
#include <QVariant>

static int mymeta=
	qRegisterMetaType<MOTicket>()+
	qRegisterMetaType<QList<MOTicket> >()+
	qRegisterMetaType<Nullable<MOTicket> >()+
	qRegisterMetaType<MOVoucher>()+
	qRegisterMetaType<QList<MOVoucher> >()+
	qRegisterMetaType<Nullable<MOVoucher> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOTicket>,MOTicket>([](const Nullable<MOTicket>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOTicket>,QVariantList>([](const QList<MOTicket>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;})|
	QMetaType::registerConverter<Nullable<MOVoucher>,MOVoucher>([](const Nullable<MOVoucher>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOVoucher>,QVariantList>([](const QList<MOVoucher>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

MOEvent MOTicket::event()const
{
	//if I am valid...
	if(!ticketid().isNull() && !eventid().isNull())
		//...and the event is not...
		if(m_event.eventid().isNull()){
			//...then retrieve the event
			MTGetEvent ge=req->queryGetEvent(eventid());
			if(!ge.hasError())
			m_event=ge.getevent();
		}
	//return whatever we have by now
	return m_event;
}

void MOTicket::setEvent(const MOEvent& e)
{
	m_event=e;
	seteventid(e.eventid());
}

QString MOTicket::priceCategoryName()const
{
	return pricecategory().value().name();
}
QString MOTicket::priceCategoryShort()const
{
	return pricecategory().value().abbreviation();
}
