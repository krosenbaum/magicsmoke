//
// C++ Interface: MOCustomer
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MOCUSTOMER_H
#define MAGICSMOKE_MOCUSTOMER_H

#include "MOCustomerAbstract"

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

class MOCustomerInfo;

class MSIFACE_EXPORT MOCustomer:public MOCustomerAbstract
{
	Q_GADGET
	WOBJECT(MOCustomer)
	public:
		/**shortcut: gets the customer from the DB*/
		MOCustomer(qint64);
		/**shortcut: extracts the ID from the info and retrieves the customer from the DB*/
		MOCustomer(const MOCustomerInfo&);
		/**returns whether the customer is valid*/
		bool isValid(){return !id().isNull();}
		
		/**alias for id()*/
		Nullable<qint64> customerid()const{return id();}
		
		/**returns the address of the customer*/
		QString address(int i=0);
		
		/**returns the full name (incl. title)*/
		QString fullName()const;

};

Q_DECLARE_METATYPE(MOCustomer)
Q_DECLARE_METATYPE(QList<MOCustomer>)
Q_DECLARE_METATYPE(Nullable<MOCustomer>)

#endif
