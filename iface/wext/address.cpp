//
// C++ Implementation: MOAddress
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "MOAddress"

#include <QStringList>
#include <QVariant>


static int moaid=
	qRegisterMetaType<MOAddress>()+
	qRegisterMetaType<QList<MOAddress> >()+
	qRegisterMetaType<Nullable<MOAddress> >();
static bool mocv=
	QMetaType::registerConverter<Nullable<MOAddress>,MOAddress>([](const Nullable<MOAddress>&n){return n.value();})|
	QMetaType::registerConverter<QList<MOAddress>,QVariantList>([](const QList<MOAddress>&n){QVariantList r;for(auto v:n)r<<QVariant::fromValue(v);return r;});

QString MOAddress::fullAddress(QString nm)const
{
	//collect lines
	QStringList rl;
	if(name().isNull() || name()==""){
		if(nm!="")rl<<nm;
	}else rl<<name();
	if(!addr1().isNull() && addr1()!="")rl<<addr1();
	if(!addr2().isNull() && addr2()!="")rl<<addr2();
	if(!city().isNull() && city()!="")rl<<city();
	if(!state().isNull() && state()!="")rl<<state();
	if(!zipcode().isNull() && zipcode()!="")rl<<zipcode();
	if(!country().isNull())rl<<country().value().name();
	//assemble
	QString r;
	for(int i=0;i<rl.size();i++){
		if(i)r+="\n";
		r+=rl[i];
	}
	
	return r;
}
