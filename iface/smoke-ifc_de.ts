<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MInterface</name>
    <message>
        <location filename="wob/srcMInterface.cpp" line="581"/>
        <source>Backup</source>
        <translation>Sicherung</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="577"/>
        <source>GetLanguage</source>
        <translation>Übersetzung für Servermeldungen holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="585"/>
        <source>ServerInfo</source>
        <translation>Serverinformationen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="586"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="587"/>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="588"/>
        <source>GetMyRoles</source>
        <translation>meine Rollen herausfinden</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="589"/>
        <source>GetMyRights</source>
        <translation>meine Rechte herausfinden</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="590"/>
        <source>ChangeMyPassword</source>
        <translation>Mein Passwort ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="591"/>
        <source>GetAllUsers</source>
        <translation>Nutzer abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="593"/>
        <source>CreateUser</source>
        <translation>Nutzer anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="594"/>
        <source>ChangePassword</source>
        <translation>Passwort eines anderen Nutzers ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="595"/>
        <source>DeleteUser</source>
        <translation>Nutzer löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="596"/>
        <source>SetUserDescription</source>
        <translation>Nutzerkommentar setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="597"/>
        <source>GetUserRoles</source>
        <translation>Rollen eines anderen Nutzers herausfinden</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="598"/>
        <source>SetUserRoles</source>
        <translation>Rollen eines anderen Nutzers setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="602"/>
        <source>GetAllRoles</source>
        <translation>Alle Rollen abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="603"/>
        <source>GetRole</source>
        <translation>spezifische Rolle abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="604"/>
        <source>CreateRole</source>
        <translation>Rolle anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="605"/>
        <source>SetRoleDescription</source>
        <translation>Rollenkommentar setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="606"/>
        <source>SetRoleRights</source>
        <translation>Rollenrechte setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="607"/>
        <source>DeleteRole</source>
        <translation>Rolle löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="609"/>
        <source>GetAllRightNames</source>
        <translation>Namen aller Rechte abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="610"/>
        <source>GetAllHostNames</source>
        <translation>Namen aller Hosts abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="611"/>
        <source>GetAllHosts</source>
        <translation>Alle Hosts (incl. Keys) abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="612"/>
        <source>SetHost</source>
        <translation>Host ändern/anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="613"/>
        <source>DeleteHost</source>
        <translation>Host löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="599"/>
        <source>GetUserHosts</source>
        <translation>erlaubte Hosts eines Nutzers abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="578"/>
        <source>GetValidFlags</source>
        <translation>alle gültigen Flags abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="579"/>
        <source>SetFlag</source>
        <translation>Flag anlegen/ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="580"/>
        <source>DeleteFlag</source>
        <translation>Flag löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="592"/>
        <source>GetUser</source>
        <translation>Nutzerdaten abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="600"/>
        <source>SetUserHosts</source>
        <translation>erlaubte Hosts eines Nutzers abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="601"/>
        <source>SetUserFlags</source>
        <translation>Flags des Nutzers setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="608"/>
        <source>SetRoleFlags</source>
        <translation>Flags der Rolle setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="614"/>
        <source>GetAllContactTypes</source>
        <translation>Kontaktinformationstypen abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="615"/>
        <source>CreateContactType</source>
        <translation>Kontaktinformationstypen anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="616"/>
        <source>GetCustomer</source>
        <translation>Kunden abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="617"/>
        <source>GetAllCustomerNames</source>
        <translation>Alle Kundennamen abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="618"/>
        <source>CreateCustomer</source>
        <translation>Kunden anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="619"/>
        <source>ChangeCustomer</source>
        <translation>Kunden ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="620"/>
        <source>ChangeCustomerMail</source>
        <translation>Kunden-E-Mail-Adresse ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="621"/>
        <source>DeleteCustomer</source>
        <translation>Kunden löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="622"/>
        <source>GetAddress</source>
        <translation>Addresse abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="623"/>
        <source>GetAllCountries</source>
        <translation>gespeicherte Länder abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="624"/>
        <source>CreateCountry</source>
        <translation>Land anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="625"/>
        <source>ResetCustomerPassword</source>
        <translation>Kundenpasswort zurücksetzen (sendet Mail)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="627"/>
        <source>GetAllArtists</source>
        <translation>Künstler abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="628"/>
        <source>CreateArtist</source>
        <translation>Künstler anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="629"/>
        <source>GetAllPriceCategories</source>
        <translation>Preiskategorien abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="630"/>
        <source>CreatePriceCategory</source>
        <translation>Preiskategorie anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="631"/>
        <source>ChangePriceCategory</source>
        <translation>Preiskategorie anpassen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="632"/>
        <source>GetEvent</source>
        <translation>Veranstaltungsdetails abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="633"/>
        <source>GetAllEvents</source>
        <translation>Liste der Veranstaltungen abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="634"/>
        <source>GetEventList</source>
        <translation>Liste der Veranstaltungen abfragen (spezifische Liste)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="636"/>
        <source>CreateEvent</source>
        <translation>Veranstaltung anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="637"/>
        <source>ChangeEvent</source>
        <translation>Veranstaltung ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="638"/>
        <source>CancelEvent</source>
        <translation>Veranstaltung absagen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="639"/>
        <source>GetAllRooms</source>
        <translation>Liste aller Räume abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="640"/>
        <source>CreateRoom</source>
        <translation>Raum anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="644"/>
        <source>GetEventSummary</source>
        <translation>Veranstaltungübersicht</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="645"/>
        <source>GetTicket</source>
        <translation>Ticket abrufen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="646"/>
        <source>GetVoucher</source>
        <translation>Gutschein abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="696"/>
        <source>GetVoucherAudit</source>
        <translation>Logdaten zu Gutschein abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="697"/>
        <source>GetOrderAudit</source>
        <translation>Logdaten zu Bestellung abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="647"/>
        <source>GetOrder</source>
        <translation>Bestellung: Details abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="582"/>
        <source>BackupExplore</source>
        <translation>Backup-Index erfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="583"/>
        <source>BackupTable</source>
        <translation>Backup von Tabelle</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="584"/>
        <source>RestoreBackup</source>
        <translation>Backup wieder-herstellen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="626"/>
        <source>GetCreateCustomerHints</source>
        <translation>Editierhilfen für Kunden-Wizard abholen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="635"/>
        <source>GetEventSaleInfo</source>
        <translation>Informationen für Kartenverkauf holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="641"/>
        <source>GetAllSeatPlans</source>
        <translation>Alle Raumpläne holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="642"/>
        <source>CreateSeatPlan</source>
        <translation>Raumplan anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="643"/>
        <source>UpdateSeatPlan</source>
        <translation>Raumplan hochladen/erneuern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="648"/>
        <source>GetOrderList</source>
        <translation>Liste der Bestellungen abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="649"/>
        <source>GetOrdersByEvents</source>
        <translation>Bestellungen finden, die Veranstaltung enthalten</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="650"/>
        <source>GetOrdersByCustomer</source>
        <translation>Bestellungen finden, die zu einem Kunden gehören</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="651"/>
        <source>GetMyOrders</source>
        <translation>Eigene Bestellungen ansehen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="652"/>
        <source>GetOrdersByUser</source>
        <translation>Bestellungen eines anderen Nutzers ansehen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="653"/>
        <source>GetOrderByBarcode</source>
        <translation>Bestellung finden, die Eintrittskarte oder Gutschein enthält</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="654"/>
        <source>GetOrdersByCoupon</source>
        <translation>Bestellungen nach benutztem Rabattcode suchen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="655"/>
        <source>CreateOrder</source>
        <translation>Bestellung anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="656"/>
        <source>CreateReservation</source>
        <translation>Reservierung anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="657"/>
        <source>ReservationToOrder</source>
        <translation>Reservierung in Bestellung wandeln</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="658"/>
        <source>CancelOrder</source>
        <translation>Bestellung stornieren</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="659"/>
        <source>OrderPay</source>
        <translation>Bestellung bezahlen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="660"/>
        <source>OrderRefund</source>
        <translation>Bestellung: Geld zurück geben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="661"/>
        <source>UseVoucher</source>
        <translation>Gutschein benutzen (damit bezahlen)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="662"/>
        <source>DeductVoucher</source>
        <translation>Gutschein für Waren außerhalb MagicSmoke benutzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="663"/>
        <source>EmptyVoucher</source>
        <translation>Gutschein ungültig machen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="664"/>
        <source>ChangeVoucherValidity</source>
        <translation>Gutschein-Gültigkeit ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="665"/>
        <source>OrderChangeShipping</source>
        <translation>Versandoption einer Bestellung ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="666"/>
        <source>OrderMarkShipped</source>
        <translation>Bestellung als verschickt markieren</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="667"/>
        <source>OrderAddComment</source>
        <translation>Bestellkommentar (in angelegter Bestellung) hinzufügen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="668"/>
        <source>OrderChangeComments</source>
        <translation>Bestellkommentar (in angelegter Bestellung) ändern (Adminfunktion)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="669"/>
        <source>ReturnTicketVoucher</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="670"/>
        <source>ChangeTicketPrice</source>
        <translation>Ticketpreis ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="671"/>
        <source>ChangeTicketPriceCategory</source>
        <translation>Preiskategorie einer Karte ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="672"/>
        <source>ChangeOrderAddress</source>
        <translation>Adresse einer Bestellung ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="673"/>
        <source>GetAllShipping</source>
        <translation>Versandoptionen holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="674"/>
        <source>ChangeShipping</source>
        <translation>Versandoptionsdaten ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="675"/>
        <source>CreateShipping</source>
        <translation>Versandoption anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="676"/>
        <source>DeleteShipping</source>
        <translation>Versandoption löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="677"/>
        <source>GetValidVoucherPrices</source>
        <translation>Gutscheinpreise abfragen (zB. für Bestellformular)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="678"/>
        <source>UseTicket</source>
        <translation>Ticket entwerten</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="679"/>
        <source>GetEntranceEvents</source>
        <translation>Liste der Veranstaltungen abfragen, die am Einlass relevant sind</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="680"/>
        <source>GetPaymentTypes</source>
        <translation>Bezahlarten abholen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="681"/>
        <source>SetPaymentType</source>
        <translation>Bezahlart anlegen/ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="682"/>
        <source>SetDefaultPaymentType</source>
        <translation>Standard-Bezahlart festlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="683"/>
        <source>DeletePaymentType</source>
        <translation>Bezahlart löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="684"/>
        <source>GetOrderDocumentNames</source>
        <translation>Dokumentennamen einer Bestellung abholen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="685"/>
        <source>GetOrderDocument</source>
        <translation>Dokument einer Bestellung abholen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="686"/>
        <source>SetOrderDocument</source>
        <translation>Dokument zu einer Bestellung hochladen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="687"/>
        <source>DeleteOrderDocument</source>
        <translation>Dokument einer Bestellung löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="688"/>
        <source>SendCustomerMail</source>
        <translation>Mail an Kunden schicken</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="689"/>
        <source>GetPrintAtHomeSettings</source>
        <translation>Print@Home Einstellungen abholen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="690"/>
        <source>SetPrintAtHomeSettings</source>
        <translation>Print@Home Einstellungen hochladen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="691"/>
        <source>GetCoupon</source>
        <translation>Rabattcode-Daten holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="692"/>
        <source>GetCouponList</source>
        <translation>Liste der Rabattcodes holen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="693"/>
        <source>CreateCoupon</source>
        <translation>Rabattcode anlegen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="694"/>
        <source>ChangeCoupon</source>
        <translation>Rabattcode ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="695"/>
        <source>GetTicketAudit</source>
        <translation>Auditierung nach Eintrittskarte</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="698"/>
        <source>GetUserAudit</source>
        <translation>Auditierung nach Nutzer</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="699"/>
        <source>WebCartAddTicket</source>
        <translation>Nur Web: Karte zum Warenkorb hinzufügen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="700"/>
        <source>WebCartRemoveTicket</source>
        <translation>Nur Web: Karte aus Warenkorb entfernen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="701"/>
        <source>WebCartAddVoucher</source>
        <translation>Nur Web: Gutschein zum Warenkorb hinzufügen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="702"/>
        <source>WebCartRemoveVoucher</source>
        <translation>Nur Web: Gutschein aus Warenkorb entfernen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="703"/>
        <source>WebCartAddCoupon</source>
        <translation>Nur Web: Rabattcode zum Einkaufswagen hinzufügen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="704"/>
        <source>GetTemplateList</source>
        <translation>Vorlagenliste abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="705"/>
        <source>GetTemplate</source>
        <translation>Vorlage abfragen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="706"/>
        <source>SetTemplate</source>
        <translation>Vorlage anlegen oder ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="707"/>
        <source>SetTemplateDescription</source>
        <translation>Beschreibung einer Vorlage ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="708"/>
        <source>DeleteTemplate</source>
        <translation>Vorlage löschen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="709"/>
        <source>SetTemplateFlags</source>
        <translation>Falgs für Template setzen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="710"/>
        <source>ChangeEvent:CancelEvent</source>
        <translation>Veranstaltung absagen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="711"/>
        <source>CreateOrder:AnyVoucherValue</source>
        <translation>Bestellung anlegen: beliebige Gutscheinwerte erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="712"/>
        <source>CreateOrder:DiffVoucherValuePrice</source>
        <translation>Bestellung anlegen: Gutscheinpreis darf von Gutscheinwert abweichen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="713"/>
        <source>CreateOrder:LateSale</source>
        <translation>Bestellung anlegen: bis zu Veranstaltungsbeginn erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="714"/>
        <source>CreateOrder:AfterTheFactSale</source>
        <translation>Bestellung anlegen: auch nach der Veranstaltung erlauben (Adminfunktion)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="715"/>
        <source>CreateOrder:CanOrder</source>
        <translation>Bestellung anlegen: Nutzer darf bestellen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="716"/>
        <source>CreateOrder:CanSell</source>
        <translation>Bestellung anlegen: Nutzer darf verkaufen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="717"/>
        <source>CreateOrder:CanOrderTicket</source>
        <translation>Bestellung anlegen: Nutzer darf Tickets verkaufen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="718"/>
        <source>CreateOrder:CanOrderVoucher</source>
        <translation>Bestellung anlegen: Nutzer darf Gutscheine verkaufen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="719"/>
        <source>CreateOrder:CanOrderItem</source>
        <translation>Bestellung anlegen: Nutzer darf Waren verkaufen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="720"/>
        <source>CreateOrder:CanPayVoucherWithVoucher</source>
        <translation>Bestellung anlegen: Nutzer darf Gutschein mit anderem Gutscheinen bezahlen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="721"/>
        <source>CreateReservation:LateReserve</source>
        <translation>Reservierung anlegen: bis Veranstaltungsbeginn erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="722"/>
        <source>CancelOrder:CancelSentOrder</source>
        <translation>Bestellung stornieren: auch für bereits versandte Bestellung</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="723"/>
        <source>CancelOrder:CancelPastTickets</source>
        <translation>Bestellung stornieren: auch für Bestellung mit Karten vergangener Veranstaltungen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="724"/>
        <source>UseVoucher:CanPayVoucherWithVoucher</source>
        <translation>Gutschein einsetzen: Nutzer darf anderen neuen Gutschein mit altem Gutschein bezahlen</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="725"/>
        <source>OrderChangeShipping:ChangePrice</source>
        <translation>Versandoption einer Bestellung ändern: beliebigen Preis erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="726"/>
        <source>OrderMarkShipped:SetTime</source>
        <translation>Bestellung als verschickt markieren: beliebigen Zeitpunkt erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="727"/>
        <source>ReturnTicketVoucher:ReturnPastTicket</source>
        <translation>Eintrittskarte oder Gutschein zurückgeben: auch abgelaufene Karten erlauben</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="728"/>
        <source>ChangeTicketPrice:ChangeUsedTicket</source>
        <translation>Ticketpreis ändern: auch bereits genutzte Karten</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="729"/>
        <source>ChangeTicketPrice:ChangePastTicket</source>
        <translation>Ticketpreis ändern: auch abgelaufene Karten</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="730"/>
        <source>ChangeTicketPriceCategory:ChangeUsedTicket</source>
        <translation>Kartenkategorie ändern: Nutzer darf benutzte Karte ändern</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="731"/>
        <source>ChangeTicketPriceCategory:ChangePastTicket</source>
        <translation>Kartenkategorie ändern: Nutzer darf Karte für Veranstaltung ändern, die schon vorbei ist</translation>
    </message>
</context>
<context>
    <name>MLocalFormat</name>
    <message>
        <location filename="misc/misc.cpp" line="212"/>
        <source>Monday</source>
        <translation>Montag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="213"/>
        <source>Tuesday</source>
        <translation>Dienstag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="214"/>
        <source>Wednesday</source>
        <translation>Mittwoch</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="215"/>
        <source>Thursday</source>
        <translation>Donnerstag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="216"/>
        <source>Friday</source>
        <translation>Freitag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="217"/>
        <source>Saturday</source>
        <translation>Samstag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="211"/>
        <source>Sunday</source>
        <translation>Sonntag</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="228"/>
        <source>Mon</source>
        <comment>short weekday</comment>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="229"/>
        <source>Tue</source>
        <comment>short weekday</comment>
        <translation>Di</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="230"/>
        <source>Wed</source>
        <comment>short weekday</comment>
        <translation>Mi</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="231"/>
        <source>Thu</source>
        <comment>short weekday</comment>
        <translation>Do</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="232"/>
        <source>Fri</source>
        <comment>short weekday</comment>
        <translation>Fr</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="233"/>
        <source>Sat</source>
        <comment>short weekday</comment>
        <translation>Sa</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="227"/>
        <source>Sun</source>
        <comment>short weekday</comment>
        <translation>So</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="243"/>
        <source>January</source>
        <translation>Januar</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="244"/>
        <source>February</source>
        <translation>Februar</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="245"/>
        <source>March</source>
        <translation>März</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="246"/>
        <source>April</source>
        <translation>April</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="247"/>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="248"/>
        <source>June</source>
        <translation>Juni</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="249"/>
        <source>July</source>
        <translation>Juli</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="250"/>
        <source>August</source>
        <translation>August</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="251"/>
        <source>September</source>
        <translation>September</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="252"/>
        <source>October</source>
        <translation>Oktober</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="253"/>
        <source>November</source>
        <translation>November</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="254"/>
        <source>December</source>
        <translation>Dezember</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="264"/>
        <source>Jan</source>
        <comment>short month name</comment>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="265"/>
        <source>Feb</source>
        <comment>short month name</comment>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="266"/>
        <source>Mar</source>
        <comment>short month name</comment>
        <translation>Mär</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="267"/>
        <source>Apr</source>
        <comment>short month name</comment>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="268"/>
        <source>May</source>
        <comment>short month name</comment>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="269"/>
        <source>Jun</source>
        <comment>short month name</comment>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="270"/>
        <source>Jul</source>
        <comment>short month name</comment>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="271"/>
        <source>Aug</source>
        <comment>short month name</comment>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="272"/>
        <source>Sep</source>
        <comment>short month name</comment>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="273"/>
        <source>Oct</source>
        <comment>short month name</comment>
        <translation>Okt</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="274"/>
        <source>Nov</source>
        <comment>short month name</comment>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="275"/>
        <source>Dec</source>
        <comment>short month name</comment>
        <translation>Dez</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="312"/>
        <source>am</source>
        <comment>AM/PM time component</comment>
        <translation>vormittags</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="314"/>
        <source>pm</source>
        <comment>AM/PM time component</comment>
        <translation>nachmittags</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="322"/>
        <source>.</source>
        <comment>decimal dot</comment>
        <translation>,</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="325"/>
        <source>,</source>
        <comment>thousand division character</comment>
        <translation> </translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="328"/>
        <source>0</source>
        <comment>digits between thousand division chars, &lt;=0 means none</comment>
        <translation>3</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="284"/>
        <source>%Y-%M-%D</source>
        <comment>date format</comment>
        <translation>%w %d.%m.%Y</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="286"/>
        <source>%h:%I</source>
        <comment>time format</comment>
        <translation>%h:%I</translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="288"/>
        <source>%Y-%M-%D %h:%I</source>
        <comment>date and time format</comment>
        <translation>%w %d.%m.%Y %h:%I</translation>
    </message>
</context>
<context>
    <name>MOCartOrder</name>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="26"/>
        <location filename="wob/srcMOCartOrder.cpp" line="33"/>
        <source>Ok</source>
        <oldsource>ok</oldsource>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="27"/>
        <location filename="wob/srcMOCartOrder.cpp" line="34"/>
        <source>Invalid</source>
        <oldsource>invalid</oldsource>
        <translation>ungültig</translation>
    </message>
</context>
<context>
    <name>MOCartTicket</name>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="32"/>
        <location filename="wob/srcMOCartTicket.cpp" line="42"/>
        <source>Ok</source>
        <oldsource>ok</oldsource>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="33"/>
        <location filename="wob/srcMOCartTicket.cpp" line="43"/>
        <source>EventOver</source>
        <translation>Veranstaltung ist vorbei</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="34"/>
        <location filename="wob/srcMOCartTicket.cpp" line="44"/>
        <source>TooLate</source>
        <oldsource>toolate</oldsource>
        <translation>zu spät</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="35"/>
        <location filename="wob/srcMOCartTicket.cpp" line="45"/>
        <source>Exhausted</source>
        <oldsource>exhausted</oldsource>
        <translation>keine Karten mehr verfügbar</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="36"/>
        <location filename="wob/srcMOCartTicket.cpp" line="46"/>
        <source>Invalid</source>
        <translation>ungültig</translation>
    </message>
</context>
<context>
    <name>MOCartVoucher</name>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="28"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="36"/>
        <source>Ok</source>
        <oldsource>ok</oldsource>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="29"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="37"/>
        <source>InvalidValue</source>
        <oldsource>invalidvalue</oldsource>
        <translation>Gutscheinwert nicht zulässig</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="30"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="38"/>
        <source>InvalidPrice</source>
        <oldsource>invalidprice</oldsource>
        <translation>Gutscheinpreis nicht zulässig</translation>
    </message>
</context>
<context>
    <name>MOEvent</name>
    <message>
        <location filename="wext/event.cpp" line="49"/>
        <source>.</source>
        <comment>price decimal dot</comment>
        <translation>,</translation>
    </message>
</context>
<context>
    <name>MOOrderAbstract</name>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="34"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="45"/>
        <source>Placed</source>
        <oldsource>placed</oldsource>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="35"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="46"/>
        <source>Sent</source>
        <oldsource>sent</oldsource>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="36"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="47"/>
        <source>Sold</source>
        <oldsource>sold</oldsource>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="37"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="48"/>
        <source>Cancelled</source>
        <oldsource>cancelled</oldsource>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="38"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="49"/>
        <source>Reserved</source>
        <oldsource>reserved</oldsource>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="39"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="50"/>
        <source>Closed</source>
        <oldsource>closed</oldsource>
        <translation>geschlossen</translation>
    </message>
</context>
<context>
    <name>MOOrderAudit</name>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="34"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="45"/>
        <source>Placed</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="35"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="46"/>
        <source>Sent</source>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="36"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="47"/>
        <source>Sold</source>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="37"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="48"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="38"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="49"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="39"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="50"/>
        <source>Closed</source>
        <translation>geschlossen</translation>
    </message>
</context>
<context>
    <name>MOOrderInfoAbstract</name>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="34"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="45"/>
        <source>Placed</source>
        <oldsource>placed</oldsource>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="35"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="46"/>
        <source>Sent</source>
        <oldsource>sent</oldsource>
        <translation>versandt</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="36"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="47"/>
        <source>Sold</source>
        <oldsource>sold</oldsource>
        <translation>Verkauft</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="37"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="48"/>
        <source>Cancelled</source>
        <oldsource>cancelled</oldsource>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="38"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="49"/>
        <source>Reserved</source>
        <oldsource>reserved</oldsource>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="39"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="50"/>
        <source>Closed</source>
        <oldsource>closed</oldsource>
        <translation>geschlossen</translation>
    </message>
</context>
<context>
    <name>MOServerFormat</name>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="34"/>
        <location filename="wob/srcMOServerFormat.cpp" line="45"/>
        <source>NoSign</source>
        <translation>Kein Vorzeichen</translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="35"/>
        <location filename="wob/srcMOServerFormat.cpp" line="46"/>
        <source>SignBeforeNum</source>
        <translation>Vorzeichen vor der Zahl</translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="36"/>
        <location filename="wob/srcMOServerFormat.cpp" line="47"/>
        <source>SignAfterNum</source>
        <translation>Vorzeichen nach der Zahl</translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="37"/>
        <location filename="wob/srcMOServerFormat.cpp" line="48"/>
        <source>SignBeforeSym</source>
        <translation>Vorzeichen vor Währungszeichen</translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="38"/>
        <location filename="wob/srcMOServerFormat.cpp" line="49"/>
        <source>SignAfterSym</source>
        <translation>Vorzeichen nach Währungszeichen</translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="39"/>
        <location filename="wob/srcMOServerFormat.cpp" line="50"/>
        <source>SignParen</source>
        <translation>Klammern benutzen</translation>
    </message>
</context>
<context>
    <name>MOTicketAbstract</name>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="42"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="57"/>
        <source>Reserved</source>
        <oldsource>reserved</oldsource>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="43"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="58"/>
        <source>Ordered</source>
        <oldsource>ordered</oldsource>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="44"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="59"/>
        <source>Used</source>
        <oldsource>used</oldsource>
        <translation>Benutzt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="45"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="60"/>
        <source>Cancelled</source>
        <oldsource>cancelled</oldsource>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="46"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="61"/>
        <source>Refund</source>
        <oldsource>refund</oldsource>
        <translation>zurückgegeben</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="47"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="62"/>
        <source>MaskBlock</source>
        <oldsource>maskblock</oldsource>
        <translation>MaskBlock</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="48"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="63"/>
        <source>MaskPay</source>
        <oldsource>maskpay</oldsource>
        <translation>MaskPay</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="49"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="64"/>
        <source>MaskUsable</source>
        <oldsource>maskusable</oldsource>
        <translation>MaskUsable</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="50"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="65"/>
        <source>MaskReturnable</source>
        <oldsource>maskreturnable</oldsource>
        <translation>MaskReturnable</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="51"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="66"/>
        <source>MaskChangeable</source>
        <translation>MaskChangeable</translation>
    </message>
</context>
<context>
    <name>MOTicketSaleInfo</name>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="42"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="57"/>
        <source>Reserved</source>
        <translation>Reserviert</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="43"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="58"/>
        <source>Ordered</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="44"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="59"/>
        <source>Used</source>
        <translation>Benutzt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="45"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="60"/>
        <source>Cancelled</source>
        <translation>storniert</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="46"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="61"/>
        <source>Refund</source>
        <translation>zurückgegeben</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="47"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="62"/>
        <source>MaskBlock</source>
        <translation>MaskBlock</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="48"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="63"/>
        <source>MaskPay</source>
        <translation>MaskPay</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="49"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="64"/>
        <source>MaskUsable</source>
        <translation>MaskUsable</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="50"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="65"/>
        <source>MaskReturnable</source>
        <translation>MaskReturnable</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="51"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="66"/>
        <source>MaskChangeable</source>
        <translation>MaskChangeable</translation>
    </message>
</context>
<context>
    <name>MOTicketUse</name>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="36"/>
        <location filename="wob/srcMOTicketUse.cpp" line="48"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="37"/>
        <location filename="wob/srcMOTicketUse.cpp" line="49"/>
        <source>NotFound</source>
        <translation>Karte nicht gefunden.</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="38"/>
        <location filename="wob/srcMOTicketUse.cpp" line="50"/>
        <source>WrongEvent</source>
        <translation>Falsche Veranstaltung</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="39"/>
        <location filename="wob/srcMOTicketUse.cpp" line="51"/>
        <source>AlreadyUsed</source>
        <translation>bereits benutzt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="40"/>
        <location filename="wob/srcMOTicketUse.cpp" line="52"/>
        <source>NotUsable</source>
        <translation>kann nicht benutzt werden</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="41"/>
        <location filename="wob/srcMOTicketUse.cpp" line="53"/>
        <source>Unpaid</source>
        <translation>noch nicht bezahlt</translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="42"/>
        <location filename="wob/srcMOTicketUse.cpp" line="54"/>
        <source>InvalidEvent</source>
        <translation>ungültige Veranstaltung</translation>
    </message>
</context>
<context>
    <name>MOVoucher</name>
    <message>
        <location filename="wext/voucher.h" line="41"/>
        <source>unlimited</source>
        <translation>unbegrenzt</translation>
    </message>
</context>
<context>
    <name>MOVoucherAbstract</name>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="30"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="39"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="31"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="40"/>
        <source>InvalidValue</source>
        <translation>Gutscheinwert nicht zulässig</translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="32"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="41"/>
        <source>InvalidPrice</source>
        <translation>Gutscheinpreis nicht zulässig</translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="33"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="42"/>
        <source>InvalidTime</source>
        <translation>Gutschein abgelaufen</translation>
    </message>
</context>
<context>
    <name>MSInterface</name>
    <message>
        <location filename="msinterface.cpp" line="110"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="110"/>
        <source>Login failed: %1</source>
        <translation>Login fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="152"/>
        <location filename="msinterface.cpp" line="156"/>
        <location filename="msinterface.cpp" line="161"/>
        <location filename="msinterface.cpp" line="166"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="152"/>
        <source>Communication problem while talking to the server, see log for details.</source>
        <translation>Kommunikationsfehler zum Server, bitte schauen Sie ins Log, um Details zu sehen.</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="156"/>
        <source>Communication with server was not successful.</source>
        <translation>Kommunikation mit dem Server ist nicht möglich.</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="161"/>
        <source>The server implementation is too old for this client.</source>
        <translation>Der Server ist zu alt für diesen Client.</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="166"/>
        <source>This client is too old for the server, please upgrade.</source>
        <translation>Dieser Client ist zu alt für den Server.</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="271"/>
        <source>Connection Error</source>
        <translation>Verbindungsfehler</translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="271"/>
        <source>There were problems while authenticating the server. Aborting. Check your configuration.</source>
        <translation>Es gab Probelme bei der Authentifikation. Abbruch. Bitte prüfen Sie Ihre Konfiguration.</translation>
    </message>
</context>
<context>
    <name>MTemplateStore</name>
    <message>
        <location filename="templates/templates.cpp" line="147"/>
        <source>Retrieving templates from server.</source>
        <translation>Hole Vorlagen vom Server.</translation>
    </message>
</context>
<context>
    <name>WobTransaction</name>
    <message>
        <location filename="wob/srcMTBackup.cpp" line="94"/>
        <location filename="wob/srcMTBackupExplore.cpp" line="94"/>
        <location filename="wob/srcMTBackupTable.cpp" line="112"/>
        <location filename="wob/srcMTCancelEvent.cpp" line="101"/>
        <location filename="wob/srcMTCancelOrder.cpp" line="97"/>
        <location filename="wob/srcMTChangeCoupon.cpp" line="96"/>
        <location filename="wob/srcMTChangeCustomer.cpp" line="97"/>
        <location filename="wob/srcMTChangeCustomerMail.cpp" line="102"/>
        <location filename="wob/srcMTChangeEvent.cpp" line="97"/>
        <location filename="wob/srcMTChangeMyPassword.cpp" line="103"/>
        <location filename="wob/srcMTChangeOrderAddress.cpp" line="109"/>
        <location filename="wob/srcMTChangePassword.cpp" line="101"/>
        <location filename="wob/srcMTChangePriceCategory.cpp" line="97"/>
        <location filename="wob/srcMTChangeShipping.cpp" line="97"/>
        <location filename="wob/srcMTChangeTicketPrice.cpp" line="100"/>
        <location filename="wob/srcMTChangeTicketPriceCategory.cpp" line="100"/>
        <location filename="wob/srcMTChangeVoucherValidity.cpp" line="109"/>
        <location filename="wob/srcMTCreateArtist.cpp" line="109"/>
        <location filename="wob/srcMTCreateContactType.cpp" line="102"/>
        <location filename="wob/srcMTCreateCountry.cpp" line="100"/>
        <location filename="wob/srcMTCreateCoupon.cpp" line="97"/>
        <location filename="wob/srcMTCreateCustomer.cpp" line="97"/>
        <location filename="wob/srcMTCreateEvent.cpp" line="97"/>
        <location filename="wob/srcMTCreateOrder.cpp" line="110"/>
        <location filename="wob/srcMTCreatePriceCategory.cpp" line="97"/>
        <location filename="wob/srcMTCreateReservation.cpp" line="98"/>
        <location filename="wob/srcMTCreateRole.cpp" line="96"/>
        <location filename="wob/srcMTCreateRoom.cpp" line="107"/>
        <location filename="wob/srcMTCreateSeatPlan.cpp" line="97"/>
        <location filename="wob/srcMTCreateShipping.cpp" line="97"/>
        <location filename="wob/srcMTCreateUser.cpp" line="107"/>
        <location filename="wob/srcMTDeductVoucher.cpp" line="106"/>
        <location filename="wob/srcMTDeleteCustomer.cpp" line="100"/>
        <location filename="wob/srcMTDeleteFlag.cpp" line="96"/>
        <location filename="wob/srcMTDeleteHost.cpp" line="96"/>
        <location filename="wob/srcMTDeleteOrderDocument.cpp" line="101"/>
        <location filename="wob/srcMTDeletePaymentType.cpp" line="98"/>
        <location filename="wob/srcMTDeleteRole.cpp" line="96"/>
        <location filename="wob/srcMTDeleteShipping.cpp" line="96"/>
        <location filename="wob/srcMTDeleteTemplate.cpp" line="96"/>
        <location filename="wob/srcMTDeleteUser.cpp" line="99"/>
        <location filename="wob/srcMTEmptyVoucher.cpp" line="102"/>
        <location filename="wob/srcMTGetAddress.cpp" line="97"/>
        <location filename="wob/srcMTGetAllArtists.cpp" line="94"/>
        <location filename="wob/srcMTGetAllContactTypes.cpp" line="94"/>
        <location filename="wob/srcMTGetAllCountries.cpp" line="94"/>
        <location filename="wob/srcMTGetAllCustomerNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllEvents.cpp" line="94"/>
        <location filename="wob/srcMTGetAllHostNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllHosts.cpp" line="94"/>
        <location filename="wob/srcMTGetAllPriceCategories.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRightNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRoles.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRooms.cpp" line="94"/>
        <location filename="wob/srcMTGetAllSeatPlans.cpp" line="94"/>
        <location filename="wob/srcMTGetAllShipping.cpp" line="94"/>
        <location filename="wob/srcMTGetAllUsers.cpp" line="94"/>
        <location filename="wob/srcMTGetCoupon.cpp" line="97"/>
        <location filename="wob/srcMTGetCouponList.cpp" line="97"/>
        <location filename="wob/srcMTGetCreateCustomerHints.cpp" line="98"/>
        <location filename="wob/srcMTGetCustomer.cpp" line="97"/>
        <location filename="wob/srcMTGetEntranceEvents.cpp" line="100"/>
        <location filename="wob/srcMTGetEvent.cpp" line="97"/>
        <location filename="wob/srcMTGetEventList.cpp" line="101"/>
        <location filename="wob/srcMTGetEventSaleInfo.cpp" line="97"/>
        <location filename="wob/srcMTGetEventSummary.cpp" line="98"/>
        <location filename="wob/srcMTGetLanguage.cpp" line="102"/>
        <location filename="wob/srcMTGetMyOrders.cpp" line="101"/>
        <location filename="wob/srcMTGetMyRights.cpp" line="95"/>
        <location filename="wob/srcMTGetMyRoles.cpp" line="94"/>
        <location filename="wob/srcMTGetOrder.cpp" line="97"/>
        <location filename="wob/srcMTGetOrderAudit.cpp" line="99"/>
        <location filename="wob/srcMTGetOrderByBarcode.cpp" line="99"/>
        <location filename="wob/srcMTGetOrderDocument.cpp" line="102"/>
        <location filename="wob/srcMTGetOrderDocumentNames.cpp" line="97"/>
        <location filename="wob/srcMTGetOrderList.cpp" line="98"/>
        <location filename="wob/srcMTGetOrdersByCoupon.cpp" line="101"/>
        <location filename="wob/srcMTGetOrdersByCustomer.cpp" line="100"/>
        <location filename="wob/srcMTGetOrdersByEvents.cpp" line="105"/>
        <location filename="wob/srcMTGetOrdersByUser.cpp" line="104"/>
        <location filename="wob/srcMTGetPaymentTypes.cpp" line="95"/>
        <location filename="wob/srcMTGetPrintAtHomeSettings.cpp" line="94"/>
        <location filename="wob/srcMTGetRole.cpp" line="97"/>
        <location filename="wob/srcMTGetTemplate.cpp" line="97"/>
        <location filename="wob/srcMTGetTemplateList.cpp" line="94"/>
        <location filename="wob/srcMTGetTicket.cpp" line="97"/>
        <location filename="wob/srcMTGetTicketAudit.cpp" line="98"/>
        <location filename="wob/srcMTGetUser.cpp" line="97"/>
        <location filename="wob/srcMTGetUserAudit.cpp" line="104"/>
        <location filename="wob/srcMTGetUserHosts.cpp" line="97"/>
        <location filename="wob/srcMTGetUserRoles.cpp" line="97"/>
        <location filename="wob/srcMTGetValidFlags.cpp" line="94"/>
        <location filename="wob/srcMTGetValidVoucherPrices.cpp" line="94"/>
        <location filename="wob/srcMTGetVoucher.cpp" line="98"/>
        <location filename="wob/srcMTGetVoucherAudit.cpp" line="97"/>
        <location filename="wob/srcMTLogin.cpp" line="115"/>
        <location filename="wob/srcMTLogout.cpp" line="93"/>
        <location filename="wob/srcMTOrderAddComment.cpp" line="102"/>
        <location filename="wob/srcMTOrderChangeComments.cpp" line="102"/>
        <location filename="wob/srcMTOrderChangeShipping.cpp" line="103"/>
        <location filename="wob/srcMTOrderMarkShipped.cpp" line="100"/>
        <location filename="wob/srcMTOrderPay.cpp" line="111"/>
        <location filename="wob/srcMTOrderRefund.cpp" line="101"/>
        <location filename="wob/srcMTReservationToOrder.cpp" line="97"/>
        <location filename="wob/srcMTResetCustomerPassword.cpp" line="96"/>
        <location filename="wob/srcMTRestoreBackup.cpp" line="101"/>
        <location filename="wob/srcMTReturnTicketVoucher.cpp" line="97"/>
        <location filename="wob/srcMTSendCustomerMail.cpp" line="101"/>
        <location filename="wob/srcMTServerInfo.cpp" line="96"/>
        <location filename="wob/srcMTSetDefaultPaymentType.cpp" line="99"/>
        <location filename="wob/srcMTSetFlag.cpp" line="96"/>
        <location filename="wob/srcMTSetHost.cpp" line="101"/>
        <location filename="wob/srcMTSetOrderDocument.cpp" line="96"/>
        <location filename="wob/srcMTSetPaymentType.cpp" line="97"/>
        <location filename="wob/srcMTSetPrintAtHomeSettings.cpp" line="99"/>
        <location filename="wob/srcMTSetRoleDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetRoleFlags.cpp" line="103"/>
        <location filename="wob/srcMTSetRoleRights.cpp" line="103"/>
        <location filename="wob/srcMTSetTemplate.cpp" line="107"/>
        <location filename="wob/srcMTSetTemplateDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetTemplateFlags.cpp" line="99"/>
        <location filename="wob/srcMTSetUserDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetUserFlags.cpp" line="103"/>
        <location filename="wob/srcMTSetUserHosts.cpp" line="103"/>
        <location filename="wob/srcMTSetUserRoles.cpp" line="103"/>
        <location filename="wob/srcMTUpdateSeatPlan.cpp" line="96"/>
        <location filename="wob/srcMTUseTicket.cpp" line="100"/>
        <location filename="wob/srcMTUseVoucher.cpp" line="102"/>
        <location filename="wob/srcMTWebCartAddCoupon.cpp" line="99"/>
        <location filename="wob/srcMTWebCartAddTicket.cpp" line="105"/>
        <location filename="wob/srcMTWebCartAddVoucher.cpp" line="99"/>
        <location filename="wob/srcMTWebCartRemoveTicket.cpp" line="102"/>
        <location filename="wob/srcMTWebCartRemoveVoucher.cpp" line="99"/>
        <source>XML result parser error: empty response.</source>
        <translation>XML Fehler: leere Antwort.</translation>
    </message>
    <message>
        <location filename="wob/srcMTBackup.cpp" line="102"/>
        <location filename="wob/srcMTBackupExplore.cpp" line="102"/>
        <location filename="wob/srcMTBackupTable.cpp" line="120"/>
        <location filename="wob/srcMTCancelEvent.cpp" line="109"/>
        <location filename="wob/srcMTCancelOrder.cpp" line="105"/>
        <location filename="wob/srcMTChangeCoupon.cpp" line="104"/>
        <location filename="wob/srcMTChangeCustomer.cpp" line="105"/>
        <location filename="wob/srcMTChangeCustomerMail.cpp" line="110"/>
        <location filename="wob/srcMTChangeEvent.cpp" line="105"/>
        <location filename="wob/srcMTChangeMyPassword.cpp" line="111"/>
        <location filename="wob/srcMTChangeOrderAddress.cpp" line="117"/>
        <location filename="wob/srcMTChangePassword.cpp" line="109"/>
        <location filename="wob/srcMTChangePriceCategory.cpp" line="105"/>
        <location filename="wob/srcMTChangeShipping.cpp" line="105"/>
        <location filename="wob/srcMTChangeTicketPrice.cpp" line="108"/>
        <location filename="wob/srcMTChangeTicketPriceCategory.cpp" line="108"/>
        <location filename="wob/srcMTChangeVoucherValidity.cpp" line="117"/>
        <location filename="wob/srcMTCreateArtist.cpp" line="117"/>
        <location filename="wob/srcMTCreateContactType.cpp" line="110"/>
        <location filename="wob/srcMTCreateCountry.cpp" line="108"/>
        <location filename="wob/srcMTCreateCoupon.cpp" line="105"/>
        <location filename="wob/srcMTCreateCustomer.cpp" line="105"/>
        <location filename="wob/srcMTCreateEvent.cpp" line="105"/>
        <location filename="wob/srcMTCreateOrder.cpp" line="118"/>
        <location filename="wob/srcMTCreatePriceCategory.cpp" line="105"/>
        <location filename="wob/srcMTCreateReservation.cpp" line="106"/>
        <location filename="wob/srcMTCreateRole.cpp" line="104"/>
        <location filename="wob/srcMTCreateRoom.cpp" line="115"/>
        <location filename="wob/srcMTCreateSeatPlan.cpp" line="105"/>
        <location filename="wob/srcMTCreateShipping.cpp" line="105"/>
        <location filename="wob/srcMTCreateUser.cpp" line="115"/>
        <location filename="wob/srcMTDeductVoucher.cpp" line="114"/>
        <location filename="wob/srcMTDeleteCustomer.cpp" line="108"/>
        <location filename="wob/srcMTDeleteFlag.cpp" line="104"/>
        <location filename="wob/srcMTDeleteHost.cpp" line="104"/>
        <location filename="wob/srcMTDeleteOrderDocument.cpp" line="109"/>
        <location filename="wob/srcMTDeletePaymentType.cpp" line="106"/>
        <location filename="wob/srcMTDeleteRole.cpp" line="104"/>
        <location filename="wob/srcMTDeleteShipping.cpp" line="104"/>
        <location filename="wob/srcMTDeleteTemplate.cpp" line="104"/>
        <location filename="wob/srcMTDeleteUser.cpp" line="107"/>
        <location filename="wob/srcMTEmptyVoucher.cpp" line="110"/>
        <location filename="wob/srcMTGetAddress.cpp" line="105"/>
        <location filename="wob/srcMTGetAllArtists.cpp" line="102"/>
        <location filename="wob/srcMTGetAllContactTypes.cpp" line="102"/>
        <location filename="wob/srcMTGetAllCountries.cpp" line="102"/>
        <location filename="wob/srcMTGetAllCustomerNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllEvents.cpp" line="102"/>
        <location filename="wob/srcMTGetAllHostNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllHosts.cpp" line="102"/>
        <location filename="wob/srcMTGetAllPriceCategories.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRightNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRoles.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRooms.cpp" line="102"/>
        <location filename="wob/srcMTGetAllSeatPlans.cpp" line="102"/>
        <location filename="wob/srcMTGetAllShipping.cpp" line="102"/>
        <location filename="wob/srcMTGetAllUsers.cpp" line="102"/>
        <location filename="wob/srcMTGetCoupon.cpp" line="105"/>
        <location filename="wob/srcMTGetCouponList.cpp" line="105"/>
        <location filename="wob/srcMTGetCreateCustomerHints.cpp" line="106"/>
        <location filename="wob/srcMTGetCustomer.cpp" line="105"/>
        <location filename="wob/srcMTGetEntranceEvents.cpp" line="108"/>
        <location filename="wob/srcMTGetEvent.cpp" line="105"/>
        <location filename="wob/srcMTGetEventList.cpp" line="109"/>
        <location filename="wob/srcMTGetEventSaleInfo.cpp" line="105"/>
        <location filename="wob/srcMTGetEventSummary.cpp" line="106"/>
        <location filename="wob/srcMTGetLanguage.cpp" line="110"/>
        <location filename="wob/srcMTGetMyOrders.cpp" line="109"/>
        <location filename="wob/srcMTGetMyRights.cpp" line="103"/>
        <location filename="wob/srcMTGetMyRoles.cpp" line="102"/>
        <location filename="wob/srcMTGetOrder.cpp" line="105"/>
        <location filename="wob/srcMTGetOrderAudit.cpp" line="107"/>
        <location filename="wob/srcMTGetOrderByBarcode.cpp" line="107"/>
        <location filename="wob/srcMTGetOrderDocument.cpp" line="110"/>
        <location filename="wob/srcMTGetOrderDocumentNames.cpp" line="105"/>
        <location filename="wob/srcMTGetOrderList.cpp" line="106"/>
        <location filename="wob/srcMTGetOrdersByCoupon.cpp" line="109"/>
        <location filename="wob/srcMTGetOrdersByCustomer.cpp" line="108"/>
        <location filename="wob/srcMTGetOrdersByEvents.cpp" line="113"/>
        <location filename="wob/srcMTGetOrdersByUser.cpp" line="112"/>
        <location filename="wob/srcMTGetPaymentTypes.cpp" line="103"/>
        <location filename="wob/srcMTGetPrintAtHomeSettings.cpp" line="102"/>
        <location filename="wob/srcMTGetRole.cpp" line="105"/>
        <location filename="wob/srcMTGetTemplate.cpp" line="105"/>
        <location filename="wob/srcMTGetTemplateList.cpp" line="102"/>
        <location filename="wob/srcMTGetTicket.cpp" line="105"/>
        <location filename="wob/srcMTGetTicketAudit.cpp" line="106"/>
        <location filename="wob/srcMTGetUser.cpp" line="105"/>
        <location filename="wob/srcMTGetUserAudit.cpp" line="112"/>
        <location filename="wob/srcMTGetUserHosts.cpp" line="105"/>
        <location filename="wob/srcMTGetUserRoles.cpp" line="105"/>
        <location filename="wob/srcMTGetValidFlags.cpp" line="102"/>
        <location filename="wob/srcMTGetValidVoucherPrices.cpp" line="102"/>
        <location filename="wob/srcMTGetVoucher.cpp" line="106"/>
        <location filename="wob/srcMTGetVoucherAudit.cpp" line="105"/>
        <location filename="wob/srcMTLogin.cpp" line="123"/>
        <location filename="wob/srcMTLogout.cpp" line="101"/>
        <location filename="wob/srcMTOrderAddComment.cpp" line="110"/>
        <location filename="wob/srcMTOrderChangeComments.cpp" line="110"/>
        <location filename="wob/srcMTOrderChangeShipping.cpp" line="111"/>
        <location filename="wob/srcMTOrderMarkShipped.cpp" line="108"/>
        <location filename="wob/srcMTOrderPay.cpp" line="119"/>
        <location filename="wob/srcMTOrderRefund.cpp" line="109"/>
        <location filename="wob/srcMTReservationToOrder.cpp" line="105"/>
        <location filename="wob/srcMTResetCustomerPassword.cpp" line="104"/>
        <location filename="wob/srcMTRestoreBackup.cpp" line="109"/>
        <location filename="wob/srcMTReturnTicketVoucher.cpp" line="105"/>
        <location filename="wob/srcMTSendCustomerMail.cpp" line="109"/>
        <location filename="wob/srcMTServerInfo.cpp" line="104"/>
        <location filename="wob/srcMTSetDefaultPaymentType.cpp" line="107"/>
        <location filename="wob/srcMTSetFlag.cpp" line="104"/>
        <location filename="wob/srcMTSetHost.cpp" line="109"/>
        <location filename="wob/srcMTSetOrderDocument.cpp" line="104"/>
        <location filename="wob/srcMTSetPaymentType.cpp" line="105"/>
        <location filename="wob/srcMTSetPrintAtHomeSettings.cpp" line="107"/>
        <location filename="wob/srcMTSetRoleDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetRoleFlags.cpp" line="111"/>
        <location filename="wob/srcMTSetRoleRights.cpp" line="111"/>
        <location filename="wob/srcMTSetTemplate.cpp" line="115"/>
        <location filename="wob/srcMTSetTemplateDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetTemplateFlags.cpp" line="107"/>
        <location filename="wob/srcMTSetUserDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetUserFlags.cpp" line="111"/>
        <location filename="wob/srcMTSetUserHosts.cpp" line="111"/>
        <location filename="wob/srcMTSetUserRoles.cpp" line="111"/>
        <location filename="wob/srcMTUpdateSeatPlan.cpp" line="104"/>
        <location filename="wob/srcMTUseTicket.cpp" line="108"/>
        <location filename="wob/srcMTUseVoucher.cpp" line="110"/>
        <location filename="wob/srcMTWebCartAddCoupon.cpp" line="107"/>
        <location filename="wob/srcMTWebCartAddTicket.cpp" line="113"/>
        <location filename="wob/srcMTWebCartAddVoucher.cpp" line="107"/>
        <location filename="wob/srcMTWebCartRemoveTicket.cpp" line="110"/>
        <location filename="wob/srcMTWebCartRemoveVoucher.cpp" line="107"/>
        <source>XML result parser error line %1 col %2: %3</source>
        <translation>XML Fehler in Antwort Zeile %1 Spalte %2: %3</translation>
    </message>
    <message>
        <location filename="wob/srcMOAddressAbstract.cpp" line="114"/>
        <location filename="wob/srcMOAddressAbstract.cpp" line="120"/>
        <location filename="wob/srcMOAddressAbstract.cpp" line="126"/>
        <location filename="wob/srcMOArtist.cpp" line="83"/>
        <location filename="wob/srcMOCartOrder.cpp" line="129"/>
        <location filename="wob/srcMOCartOrder.cpp" line="141"/>
        <location filename="wob/srcMOCartOrder.cpp" line="147"/>
        <location filename="wob/srcMOCartOrder.cpp" line="153"/>
        <location filename="wob/srcMOCartOrder.cpp" line="163"/>
        <location filename="wob/srcMOCartTicket.cpp" line="149"/>
        <location filename="wob/srcMOCartTicket.cpp" line="155"/>
        <location filename="wob/srcMOCartTicket.cpp" line="161"/>
        <location filename="wob/srcMOCartTicket.cpp" line="167"/>
        <location filename="wob/srcMOCartTicket.cpp" line="173"/>
        <location filename="wob/srcMOCartTicket.cpp" line="179"/>
        <location filename="wob/srcMOCartTicket.cpp" line="194"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="106"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="112"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="118"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="124"/>
        <location filename="wob/srcMOContact.cpp" line="69"/>
        <location filename="wob/srcMOContact.cpp" line="75"/>
        <location filename="wob/srcMOContact.cpp" line="81"/>
        <location filename="wob/srcMOContactType.cpp" line="61"/>
        <location filename="wob/srcMOCoupon.cpp" line="93"/>
        <location filename="wob/srcMOCoupon.cpp" line="99"/>
        <location filename="wob/srcMOCoupon.cpp" line="105"/>
        <location filename="wob/srcMOCoupon.cpp" line="111"/>
        <location filename="wob/srcMOCouponInfo.cpp" line="78"/>
        <location filename="wob/srcMOCouponInfo.cpp" line="84"/>
        <location filename="wob/srcMOCouponRule.cpp" line="77"/>
        <location filename="wob/srcMOCouponRule.cpp" line="86"/>
        <location filename="wob/srcMOCouponRule.cpp" line="92"/>
        <location filename="wob/srcMOCouponRule.cpp" line="98"/>
        <location filename="wob/srcMOCouponRule.cpp" line="104"/>
        <location filename="wob/srcMOCouponRule.cpp" line="110"/>
        <location filename="wob/srcMOCustomerAbstract.cpp" line="106"/>
        <location filename="wob/srcMOCustomerInfoAbstract.cpp" line="68"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="166"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="172"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="178"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="184"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="200"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="214"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="231"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="237"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="243"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="257"/>
        <location filename="wob/srcMOEventPrice.cpp" line="101"/>
        <location filename="wob/srcMOEventPrice.cpp" line="107"/>
        <location filename="wob/srcMOEventPrice.cpp" line="117"/>
        <location filename="wob/srcMOEventPrice.cpp" line="123"/>
        <location filename="wob/srcMOEventPrice.cpp" line="129"/>
        <location filename="wob/srcMOEventPrice.cpp" line="146"/>
        <location filename="wob/srcMOEventPrice.cpp" line="152"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="89"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="98"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="114"/>
        <location filename="wob/srcMOItemAudit.cpp" line="57"/>
        <location filename="wob/srcMOItemInfo.cpp" line="74"/>
        <location filename="wob/srcMOItemInfo.cpp" line="80"/>
        <location filename="wob/srcMOItemInfo.cpp" line="90"/>
        <location filename="wob/srcMOItemInfo.cpp" line="96"/>
        <location filename="wob/srcMOItemInfo.cpp" line="102"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="214"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="220"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="230"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="240"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="277"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="283"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="289"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="295"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="301"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="311"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="317"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="157"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="163"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="178"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="184"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="190"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="196"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="202"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="81"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="87"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="97"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="103"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="74"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="80"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="90"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="96"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="163"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="169"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="178"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="184"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="190"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="202"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="208"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="214"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="220"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="226"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="232"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="238"/>
        <location filename="wob/srcMOPriceCategory.cpp" line="85"/>
        <location filename="wob/srcMORoom.cpp" line="65"/>
        <location filename="wob/srcMOSeatPlanAbstract.cpp" line="102"/>
        <location filename="wob/srcMOSeatPlanBackground.cpp" line="94"/>
        <location filename="wob/srcMOSeatPlanBackground.cpp" line="103"/>
        <location filename="wob/srcMOSeatPlanGroupAbstract.cpp" line="129"/>
        <location filename="wob/srcMOSeatPlanInfo.cpp" line="71"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="95"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="116"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="122"/>
        <location filename="wob/srcMOSeatPlanSeat.cpp" line="57"/>
        <location filename="wob/srcMOSeatPlanSeat.cpp" line="66"/>
        <location filename="wob/srcMOServerFormat.cpp" line="251"/>
        <location filename="wob/srcMOServerFormat.cpp" line="257"/>
        <location filename="wob/srcMOShipping.cpp" line="71"/>
        <location filename="wob/srcMOShipping.cpp" line="77"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="154"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="160"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="172"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="178"/>
        <location filename="wob/srcMOTicketAudit.cpp" line="71"/>
        <location filename="wob/srcMOTicketAudit.cpp" line="88"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="136"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="142"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="154"/>
        <location filename="wob/srcMOTicketUse.cpp" line="132"/>
        <location filename="wob/srcMOTicketUse.cpp" line="138"/>
        <location filename="wob/srcMOTicketUse.cpp" line="144"/>
        <location filename="wob/srcMOTicketUse.cpp" line="150"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="125"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="131"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="146"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="156"/>
        <location filename="wob/srcMOVoucherAudit.cpp" line="64"/>
        <location filename="wob/srcMOWebCart.cpp" line="127"/>
        <location filename="wob/srcMOWebCart.cpp" line="133"/>
        <location filename="wob/srcMOWebCart.cpp" line="139"/>
        <location filename="wob/srcMOWebCart.cpp" line="145"/>
        <location filename="wob/srcMOWebCart.cpp" line="155"/>
        <location filename="wob/srcMOWebSession.cpp" line="68"/>
        <location filename="wob/srcMOWebSession.cpp" line="78"/>
        <source>Class &apos;%1&apos; property &apos;%2&apos; is integer, but non-integer was found.</source>
        <translation>Klasse &apos;%1&apos; Wert &apos;%2&apos; ist eine Zahl, aber es wurde keine Zahl gefunden.</translation>
    </message>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="135"/>
        <location filename="wob/srcMOCartTicket.cpp" line="185"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="130"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="271"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="172"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="196"/>
        <location filename="wob/srcMOServerFormat.cpp" line="281"/>
        <location filename="wob/srcMOServerFormat.cpp" line="287"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="166"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="148"/>
        <location filename="wob/srcMOTicketUse.cpp" line="126"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="137"/>
        <source>Class &apos;%1&apos; property &apos;%2&apos; is enum, invalid value was found.</source>
        <translation>Klasse &apos;%1&apos; Wert &apos;%2&apos; ist &quot;enum&quot;, ungültiger Wert gefunden.</translation>
    </message>
</context>
</TS>
