//
// C++ Implementation: sslexception
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "sslexception.h"

#include <QDebug>
// #include <QDialog>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNodeList>
#include <QFile>

MSslExceptions::MSslExceptions(QString p)
{
	path=p;
	//load...
	QFile fd(p);
	qDebug()<<"loading SSL Exceptions"<<p;
	if(fd.open(QIODevice::ReadOnly)){
		QDomDocument doc;
		if(!doc.setContent(&fd))return;
		fd.close();
		QDomElement root=doc.documentElement();
		QDomNodeList nl=root.elementsByTagName("SSL-Exception");
		for(int i=0;i<nl.size();i++){
			QDomElement el=nl.at(i).toElement();
			int e=el.attribute("errorId",0).toInt();
			if(e<=0)continue;
			QSslCertificate c(el.text().toLatin1());
			if(c.isNull())continue;
			sslexcept.append(QPair<QSslCertificate,int>(c,e));
			qDebug()<<"Note: will ignore SSL exception of type"<<e<<"from certificate (hash)"<<c.digest(QCryptographicHash::Sha1).toHex().data();
		}
	}
}

void MSslExceptions::savesslexcept()
{
	QDomDocument doc;
	QDomElement root=doc.createElement("SSL-Exceptions");
	for(int i=0;i<sslexcept.size();i++){
		QDomElement el=doc.createElement("SSL-Exception");
		el.setAttribute("errorId",sslexcept[i].second);
		el.appendChild(doc.createTextNode(sslexcept[i].first.toPem()));
		root.appendChild(el);
	}
	doc.appendChild(root);
	QFile fd(path);
	if(fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
		qDebug()<<"writing SSL Exceptions"<<path<<sslexcept.size()<<"elements";
		fd.write(doc.toByteArray());
		fd.close();
	}
}

void MSslExceptions::clear()
{
	sslexcept.clear();
	sslrecord.clear();
}

void MSslExceptions::acceptRecorded()
{
	for(int i=0;i<sslrecord.size();i++){
		bool known=false;
		for(int j=0;j<sslexcept.size();j++){
			if(sslrecord[i] == sslexcept[j]){
				known=true;
				break;
			}
		}
		if(!known)sslexcept.append(sslrecord[i]);
	}
}

bool MSslExceptions::checksslexcept(const QList<QSslError>&errs)
{
	//stage 1: record unknown exceptions
	for(int i=0;i<errs.size();i++){
		if(errs[i].error()==QSslError::NoError)continue;
		QPair<QSslCertificate,int>p(errs[i].certificate(),errs[i].error());
		bool known=false;
		for(int j=0;j<sslrecord.size();j++){
			if(p==sslrecord[j]){
				known=true;
				break;
			}
		}
		if(!known)sslrecord.append(p);
	}
	//stage 2: compare to acceptable exceptions
	for(int i=0;i<errs.size();i++){
		if(errs[i].error()==QSslError::NoError)continue;
		QPair<QSslCertificate,int>p(errs[i].certificate(),errs[i].error());
		bool known=false;
		for(int j=0;j<sslexcept.size();j++){
			if(p==sslexcept[j]){
				known=true;
				break;
			}
		}
		if(!known){
			qDebug()<<"Ooops! Non-ignored SSL exception of type"<<(int)errs[i].error()<<"certificate\n"<<errs[i].certificate().toPem().data();
			return false;
		}
	}
	return true;
}
