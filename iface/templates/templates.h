//
// C++ Interface: templates
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_TEMPLATES_H
#define MAGICSMOKE_TEMPLATES_H

#include <QString>
#include <QList>
#include <functional>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

/**this class wraps a single template*/
class MSIFACE_EXPORT MTemplate
{
	public:
		/**creates an invalid template*/
		MTemplate();
		
		/**creates a temporary template by a physical file name*/
		MTemplate(QString fn);
		
		/**copies a template*/
		MTemplate(const MTemplate&)=default;
		
		///copies a template
		MTemplate& operator=(const MTemplate&)=default;
		
		///reinitializes the template from the given file name
		void setFile(const QString&);
		
		/**returns the name/path of the cache file, if it exists*/
		QString cacheFileName()const;
		
		/**returns the file name of the template (eg. mytemplate.odtt)*/
		QString fileName()const;
		
		/**returns the complete encoded name of the template (eg. mytemplate.odtt,v1)*/
		QString completeFileName()const;
		
		/**returns the base name of the template (eg. mytemplate for mytemplate.odtt)*/
		QString baseName()const;
		
		/**returns the extension of the template (eg. odtt for mytemplate.odtt)*/
		QString extension()const;
		
		/**returns the variant ID of the template (warning: it has no meaning outside the template storage system*/
		QString variantID()const;
		
		/**returns the checksum (calculated by the server)*/
		QString checksum()const;
		
		/**returns the description*/
		QString description()const;
		
		/**returns whether this is a valid template*/
		bool isValid()const;
		
		/**returns the extension of the target document if it is saved (empty if the template cannot be saved)*/
		QString targetExtension()const;
		
		/**returns whether this is an ODF template*/
		bool isOdf()const;
		
		/**returns whether this is a label template*/
		bool isLabel()const;

		///returns whether this is a ZipTemplate
		bool isZip()const;

		///returns whether this is a HTML template
		bool isHtml()const;
		
		///returns the flags of this template
		QString flags()const{return m_flags;}
		
		/**template type; currently only ODF and Labels are known*/
		enum Type{
			/**uninitialized or unknown label type*/
			UnknownTemplate=0,
			/**ODF Text template*/
			OdtTemplate=1,
			/**ODF SpreadSheet template*/
			OdsTemplate=2,
			/**ODF Presentation*/
			OdpTemplate=4,
			/**ODF Drawing*/
			OdgTemplate=8,
			/**ODF DataBase*/
			OdbTemplate=0x10,
			/**ODF template*/
			OdfTemplateMask=0xff,
			/**Label template*/
			LabelTemplate=0x100,
			/**Script-Zip template*/
			ZipTemplate=0x200,
			///HTML patterns
			HtmlTemplate=0x400,
			///MIME patterns
			MimeTemplate=0x800
		};
		Q_DECLARE_FLAGS(Types,Type)
		
		/**returns the template type*/
		Type type()const;
		
		/**returns the currently known (by the client) template base names*/
		static QStringList legalBaseNames();
		
		/**returns the legal file types (as understood by the client) for a template base name; returns UnknownTemplate if none is legal*/
		static Types legalTypes(QString);
		
		/**returns the legal template file suffixes (as understood by the client) for a template base name; returns empty if none is legal*/
		static QStringList legalSuffixes(QString);
                
                typedef std::function<MTemplate(const QList<MTemplate>&)>TemplateChoserFunction;
                
                ///set a function to handle multiple template choices,
                ///the default is a function that choses the first one
                static void setTemplateChoiceFunction(const TemplateChoserFunction&);

	protected:
		friend class MTemplateStore;
		friend class MTemplateChoice;
		/**creates a template wrapper from a cache file name; this constructor is used internally in the storage system
		\param fn physical file name of the template
		\param chk server side check sum of the template
		\param dsc description of the template
		\param flg flags of the template
		*/
		MTemplate(QString fn,QString chk,QString dsc,QString flg);
	private:
		QString m_fname,m_checksum,m_descr,m_flags;
};
Q_DECLARE_OPERATORS_FOR_FLAGS(MTemplate::Types)

/**this class implements the storage end of the template subsystem, its only instance exists in the webrequest*/
class MSIFACE_EXPORT MTemplateStore
{
	protected:
		friend class MSInterface;
		friend class MTemplateEditor;
		/**instantiates the template subsystem*/
		MTemplateStore(QString);
		
		/**stores a specific template*/
		bool setTemplate(QString templatename,QString localfile);
		
		/**stores a new description for a template*/
		bool setTemplateDescription(QString templatename,QString description);
		
		///stores a change of flags for a template
		bool setTemplateFlags(QString templatename,QString flags);
		
		/**deletes a template (requires full name), used by MTemplateEditor*/
		bool deleteTemplate(QString);
		
		/**updates the template directory, does not do anything if force==false and the last update was less than 5min ago*/
		void updateTemplates(bool force);
		
	private:
		/**unused, just here to remove it from accessability*/
		MTemplateStore()=delete;
		
		/**helper for updateTemplates: retrieves a single file from the server*/
		bool retrieveFile(QString,QString);
		
	public:
		/**returns a specific template by its base name, opens a template choice dialog if necessary
		\param base the base name of the template to retrieve (eg. "ticket" or "voucher")
		\param useFirst if true: use the first match regardless of whether there are more candidates
		*/
		MTemplate getTemplate(QString base,bool useFirst=false);
		/**returns a specific template by its full name, returns an invalid template if it does not exist
		\param full the full name of the template to retrieve (eg. "ticket.xtt,1")*/
		MTemplate getTemplateByFile(QString full);
		
		/**returns all templates (for MTemplateEditor)*/
		QList<MTemplate> allTemplates();

	private:
		QString profileid;
};

#endif
