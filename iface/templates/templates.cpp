//
// C++ Implementation: templates
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

/*****************************
 * Settings hierarchy for templates:
 *
 * /templates/$PROFILEID -> template settings for this profile
 *  .../$TEMPLATENAME -> the settings for a template, uses the complete name (eg. bill.odtt,2)
 *  ....../checksum -> key that stores the checksum (calculated by server!!)
 *  ....../description -> key that stores the description (as received from server)
 *
 *
 * Template buffer on disk:
 *
 * req->dataDir()/templates -> directory for template files;
 *   each file is stored with complete name, no meta-info
 ***************************/

#include "templates.h"
#include "msinterface.h"
#include "boxwrapper.h"

#include "MTGetTemplateList"
#include "MTGetTemplate"
#include "MTSetTemplate"
#include "MTDeleteTemplate"
#include "MTSetTemplateDescription"
#include "MTSetTemplateFlags"

#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QRegExp>
#include <QSettings>
#include <QStringList>

static MTemplate::TemplateChoserFunction templaceChoser(MTemplate::TemplateChoserFunction f=nullptr)
{
        static MTemplate::TemplateChoserFunction rf=nullptr;
        if(f)rf=f;
        if(rf)return rf;
        else return [](const QList<MTemplate>&tl){return tl.value(0);};
}


MTemplateStore::MTemplateStore(QString p)
{
	profileid=p;
}

MTemplate MTemplateStore::getTemplate(QString f,bool useFirst)
{
	//syntax check
	f=f.toLower();
	QRegExp fregexp("[a-z0-9_]+");
	if(!fregexp.exactMatch(f))return MTemplate();
	//update directory (no force)
	updateTemplates(false);
	//find files matching the pattern
	//basics
	QString dname=req->dataDir()+"/templates/";
	QSettings set;
	set.beginGroup("templates/"+profileid);
	//get directory
	QStringList dir=set.childGroups();
	QStringList tdir;
	MSInterface*ifc=MSInterface::instance();
	for(int i=0;i<dir.size();i++){
		//filter by base name
		if(dir[i].split(".")[0].startsWith(f,Qt::CaseInsensitive))
			//filter by flags
			if(ifc->checkFlags(set.value(dir[i]+"/flags").toString()))
				tdir<<dir[i];
	}
	//check dir size
	if(tdir.size()==0)return MTemplate();
	if(tdir.size()==1 || useFirst)
		return MTemplate(dname+"/"+tdir[0], set.value(tdir[0]+"/checksum").toString(), set.value(tdir[0]+"/description").toString(), set.value(tdir[0]+"/flags").toString());
	//hmm, more than one choice
        QList<MTemplate>templst;
        for(const QString&td:tdir)
                templst<<MTemplate(dname+"/"+td, set.value(td+"/checksum").toString(), set.value(td+"/description").toString(), set.value(td+"/flags").toString());
        MTemplate::TemplateChoserFunction tcf=templaceChoser();
        return tcf(templst);
}

void MTemplate::setTemplateChoiceFunction(const TemplateChoserFunction &f)
{
        templaceChoser(f);
}

MTemplate MTemplateStore::getTemplateByFile(QString f)
{
	//syntax check
	f=f.toLower();
	QRegExp fregexp("[a-z0-9_\\.,]+");
	if(!fregexp.exactMatch(f))return MTemplate();
	//update directory (no force)
	updateTemplates(false);
	//find files matching the pattern
	//basics
	QString dname=req->dataDir()+"/templates/";
	QSettings set;
	set.beginGroup("templates/"+profileid);
	//check directory
	if(!set.childGroups().contains(f))return MTemplate();
	//return if found
	return MTemplate(dname+"/"+f, set.value(f+"/checksum").toString(), set.value(f+"/description").toString(), set.value(f+"/flags").toString());
}

void MTemplateStore::updateTemplates(bool force)
{
	//basics
	QString dname=req->dataDir()+"/templates/";
	QSettings set;
	set.beginGroup("templates/"+profileid);
	//make sure directory exists
	QDir(req->dataDir()).mkpath("templates");
	//do we need an update yet?
	QDateTime last=QDateTime::fromSecsSinceEpoch(set.value("lastupdate",0).toInt()+300);
	if(last>=QDateTime::currentDateTime() && !force)return;

	//get local info
	QStringList files=set.childGroups();
	QMap<QString,MOTemplateInfo> fmap;
	for(int i=0;i<files.size();i++){
		MOTemplateInfo t;
		t.setfilename(files[i]);
		t.sethash(set.value(files[i]+"/checksum").toString()); t.setdescription(set.value(files[i]+"/description").toString());
		t.setflags(set.value(files[i]+"/flags").toString());
		fmap.insert(files[i],t);
	}

	//display progress dialog
	MProgressWrapper *progress=MProgressWrapper::create(QCoreApplication::translate("MTemplateStore","Retrieving templates from server."));
        progress->setRange(0,1);
	progress->setValue(0);
        QObject progparent;progress->setParent(&progparent);

	//get remote info, assume it is still valid if unable to retrieve it
	MTGetTemplateList gtl=req->queryGetTemplateList();
	if(gtl.hasError())return;
	//remember update time
	set.setValue("lastupdate",QDateTime::currentDateTime().toSecsSinceEpoch());
	//scan and create new list
	QList<MOTemplateInfo>nf=gtl.gettemplates();
	QMap<QString,MOTemplateInfo>nfmap;
	for(int i=0;i<nf.size();i++){
		nfmap.insert(nf[i].filename(),nf[i]);
	}

	//check old list for removed or changed entries; remove them
	for(int i=0;i<files.size();i++){
		if(!nfmap.contains(files[i]))
			fmap.remove(files[i]);
		else
		if(nfmap[files[i]].hash() != fmap[files[i]].hash())
			fmap.remove(files[i]);
	}
	files=fmap.keys();

	//retrieve new files from server
	QStringList nfiles=nfmap.keys();
	progress->setRange(0,nfiles.size());
	for(int i=0;i<nfiles.size();i++){
		progress->setValue(i);
		if(!files.contains(nfiles[i])){
			//file is not in current cache (or was outdated)
			if(retrieveFile(dname,nfiles[i]))
				fmap.insert(nfiles[i],nfmap[nfiles[i]]);
		}else{
			//file is already up to date, still copy the current description and flags
			fmap[nfiles[i]].setdescription(nfmap[nfiles[i]].description());
			fmap[nfiles[i]].setflags(nfmap[nfiles[i]].flags());
		}
	}
	files=fmap.keys();

	//clean up directory
	QDir dir(dname);
	QStringList dfiles=dir.entryList(QDir::Files);
	for(int i=0;i<dfiles.size();i++)
		if(!files.contains(dfiles[i]))
			dir.remove(dfiles[i]);

	//settings: prune old stuff
	set.remove("");
	//settings: create new entries
	for(int i=0;i<files.size();i++){
		MOTemplateInfo s=fmap[files[i]];
		set.setValue(s.filename()+"/checksum",s.hash().value());
		set.setValue(s.filename()+"/description",s.description().value());
		set.setValue(s.filename()+"/flags",s.flags().value());
	}

	progress->hide();
}

bool MTemplateStore::retrieveFile(QString dname,QString fn)
{
	//request
	MTGetTemplate gt=req->queryGetTemplate(fn);
	if(gt.hasError())return false;
	//store
	QFile f(dname+"/"+fn);
	if(!f.open(QIODevice::WriteOnly|QIODevice::Truncate))return false;
	f.write(gt.gettemplatefile().value().content());
	return true;
}

bool MTemplateStore::setTemplate(QString n,QString f)
{
	//very rough sanity check
	QRegExp fregexp("[a-z0-9_\\.,]+");
	if(!fregexp.exactMatch(n))return false;
	//get content
	QFile fl(f);
	if(!fl.open(QIODevice::ReadOnly))
		return false;
	QByteArray ba=fl.readAll();
	fl.close();
	//send to server
	MTSetTemplate st=req->querySetTemplate(n,ba,"");
	if(st.hasError())
		return false;
	//delete it from cache, so it is retrieved again; force retrieval
	//TODO: the server returns the hash (since dec08), use it and update the cache without retrieval
	QSettings set;
	set.remove("templates/"+profileid+"/"+n);
	set.setValue("templates/"+profileid+"/lastupdate",0);
	QFile(req->dataDir()+"/templates/"+n).remove();
	//return success
	return true;
}

bool MTemplateStore::deleteTemplate(QString n)
{
	//very rough sanity check
	QRegExp fregexp("[a-z0-9_\\.,]+");
	if(!fregexp.exactMatch(n))return false;
	//send to server
	MTDeleteTemplate dt=req->queryDeleteTemplate(n);
	if(dt.hasError())
		return false;
	//delete it from cache
	QSettings set;
	set.remove("templates/"+profileid+"/"+n);
	QFile(req->dataDir()+"/templates/"+n).remove();
	return true;
}

bool MTemplateStore::setTemplateDescription(QString n,QString d)
{
	//very rough sanity check
	QRegExp fregexp("[a-z0-9_\\.,]+");
	if(!fregexp.exactMatch(n))return false;
	//check that we have a real change
	QSettings set;
	set.beginGroup("templates/"+profileid+"/"+n);
	QString o=set.value("description").toString();
	qDebug("setting %s '%s' -> '%s'",n.toLatin1().data(),o.toLatin1().data(),d.toLatin1().data());
	if(o==d)return true;
	//send to server
	MTSetTemplateDescription std=req->querySetTemplateDescription(n,d);
	if(std.hasError())
		return false;
	//update internal description
	set.setValue("description",d);
	//return success
	return true;
}

bool MTemplateStore::setTemplateFlags(QString n, QString flags)
{
	//check that we have a real change
	QSettings set;
	set.beginGroup("templates/"+profileid+"/"+n);
	QString o=set.value("flags").toString();
	qDebug("setting %s '%s' -> '%s'",n.toLatin1().data(),o.toLatin1().data(),flags.toLatin1().data());
	if(o==flags)return true;
	//send to server
	MTSetTemplateFlags std=req->querySetTemplateFlags(n,flags);
	if(std.hasError())
		return false;
	//update internal description
	set.setValue("flags",flags);
	//return success
	return true;
}

QList<MTemplate> MTemplateStore::allTemplates()
{
	updateTemplates(false);
	QString dname=req->dataDir()+"/templates/";
	QSettings set;
	set.beginGroup("templates/"+profileid);
	QStringList names=set.childGroups();
	QList<MTemplate> ret;
	for(int i=0;i<names.size();i++)
		ret.append(MTemplate(dname+"/"+names[i], set.value(names[i]+"/checksum").toString(), set.value(names[i]+"/description").toString(), set.value(names[i]+"/flags").toString()));
	return ret;
}

/****************************************************************************/

MTemplate::MTemplate(){}
MTemplate::MTemplate(QString fn){m_fname=fn;}
MTemplate::MTemplate(QString fn,QString chk,QString dsc,QString flg)
{m_fname=fn;m_checksum=chk;m_descr=dsc;m_flags=flg;}

void MTemplate::setFile(const QString& s)
{
	m_fname=s;
	m_checksum=m_descr=m_flags=QString();
}

QString MTemplate::cacheFileName()const{return m_fname;}

QString MTemplate::fileName()const
{
	return QFileInfo(m_fname).fileName().split(",")[0];
}

QString MTemplate::completeFileName()const
{
	return QFileInfo(m_fname).fileName();
}

QString MTemplate::baseName()const
{
	return QFileInfo(m_fname).baseName();
}

QString MTemplate::extension()const
{
	return QFileInfo(m_fname).completeSuffix().split(",")[0];
}

QString MTemplate::variantID()const
{
	QStringList lst=QFileInfo(m_fname).fileName().split(",");
	if(lst.size()>1)return lst[1];
	return "";
}

QString MTemplate::checksum()const{return m_checksum;}

bool MTemplate::isValid()const{return m_fname!="";}

QString MTemplate::targetExtension()const
{
	QString x=extension();
	//ODF file?
	if(QRegExp("od.t").exactMatch(x))return x.left(3);
	//all else: not storable
	return "";
}

bool MTemplate::isOdf()const{return (type()&OdfTemplateMask)!=0;}
bool MTemplate::isLabel()const{return type()==LabelTemplate;}
bool MTemplate::isZip() const{return type()==ZipTemplate;}
bool MTemplate::isHtml() const{return type()==HtmlTemplate;}

MTemplate::Type MTemplate::type()const
{
	QString x=extension();
	//ODF file?
	if(x=="odtt")return OdtTemplate;
	if(x=="odst")return OdsTemplate;
	if(x=="odpt")return OdpTemplate;
	if(x=="odgt")return OdgTemplate;
	if(x=="odbt")return OdbTemplate;
	//label?
	if(x=="xtt")return LabelTemplate;
	//script?
	if(x=="zip")return ZipTemplate;
	//HTML?
	if(x=="html")return HtmlTemplate;
	//MIME?
	if(x=="mime")return MimeTemplate;
	//hmm, unknown one
	return UnknownTemplate;
}

QString MTemplate::description()const{return m_descr;}


static QStringList tempBaseNames;
//static
QStringList MTemplate::legalBaseNames()
{
	if(tempBaseNames.size()==0){
		tempBaseNames <<"ticket" <<"voucher" <<"bill" <<"eventsummary" <<"scripts" <<"eventview" <<"printathome";
	}
	return tempBaseNames;
}

//static
MTemplate::Types MTemplate::legalTypes(QString bn)
{
	if(bn=="ticket" || bn=="voucher")
		return LabelTemplate;
	if(bn=="bill" || bn=="eventsummary")
		return OdfTemplateMask;
	if(bn=="scripts")
		return ZipTemplate;
	if(bn=="eventview")
		return HtmlTemplate;
	if(bn=="printathome")
		return MimeTemplate;
	return UnknownTemplate;
}

//static
QStringList MTemplate::legalSuffixes(QString bn)
{
	switch(legalTypes(bn)){
		case LabelTemplate:
			return QStringList()<<"xtt";
		case OdfTemplateMask:
			return QStringList()<<"odtt"<<"odst"<<"odpt"<<"odgt"<<"odbt";
		case ZipTemplate:
			return QStringList()<<"zip";
		case HtmlTemplate:
			return QStringList()<<"html";
		case MimeTemplate:
			return QStringList()<<"mime";
		default:
			return QStringList();
	}
}
