<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MInterface</name>
    <message>
        <location filename="wob/srcMInterface.cpp" line="581"/>
        <source>Backup</source>
        <translation>make backups of the server database</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="577"/>
        <source>GetLanguage</source>
        <translation>retrieve language files</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="585"/>
        <source>ServerInfo</source>
        <translation>basic server information (implicitly granted)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="586"/>
        <source>Login</source>
        <translation>log into the server</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="587"/>
        <source>Logout</source>
        <translation>log out of a session</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="588"/>
        <source>GetMyRoles</source>
        <translation>retrieve the roles/privileges I have</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="589"/>
        <source>GetMyRights</source>
        <translation>retrieve the explicit privileges I have</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="590"/>
        <source>ChangeMyPassword</source>
        <translation>set my own password</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="591"/>
        <source>GetAllUsers</source>
        <translation>get a list of all (system) users existing at the system</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="593"/>
        <source>CreateUser</source>
        <translation>create a new (system) user account</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="594"/>
        <source>ChangePassword</source>
        <translation>set the password of any (system) user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="595"/>
        <source>DeleteUser</source>
        <translation>delete a (system) user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="596"/>
        <source>SetUserDescription</source>
        <translation>set description of a user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="597"/>
        <source>GetUserRoles</source>
        <translation>retrieve the roles of another (system) user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="598"/>
        <source>SetUserRoles</source>
        <translation>change the roles of another (system) user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="602"/>
        <source>GetAllRoles</source>
        <translation>get a list of the existing system roles</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="603"/>
        <source>GetRole</source>
        <translation>get detail data about a (system-user) role</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="604"/>
        <source>CreateRole</source>
        <translation>create a new role/group</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="605"/>
        <source>SetRoleDescription</source>
        <translation>set the description of a role/group</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="606"/>
        <source>SetRoleRights</source>
        <translation>assign rights to a specific role/group</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="607"/>
        <source>DeleteRole</source>
        <translation>delete a role/group</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="609"/>
        <source>GetAllRightNames</source>
        <translation>get the names of all rights/transaction types that exist at the server</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="610"/>
        <source>GetAllHostNames</source>
        <translation>get the names of all hosts (instances of clients) registered with the server</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="611"/>
        <source>GetAllHosts</source>
        <translation>get detailed data about registered hosts</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="612"/>
        <source>SetHost</source>
        <translation>set the data of a host (like key, description)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="613"/>
        <source>DeleteHost</source>
        <translation>delete/unregister a host from the system</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="599"/>
        <source>GetUserHosts</source>
        <translation>get the hosts that a specific user can login from</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="578"/>
        <source>GetValidFlags</source>
        <translation>get all flags that can be set (necessary for everybody who can create new events, payment types, ...)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="579"/>
        <source>SetFlag</source>
        <translation>add/change the definition of a flag</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="580"/>
        <source>DeleteFlag</source>
        <translation>delete a flag</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="592"/>
        <source>GetUser</source>
        <translation>get detailed info about a (system) user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="600"/>
        <source>SetUserHosts</source>
        <translation>set the hosts a user may connect from</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="601"/>
        <source>SetUserFlags</source>
        <translation>set the flags that a user carries (can be used to give access to or remove access to privileged actions)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="608"/>
        <source>SetRoleFlags</source>
        <translation>set the flags that a role/group carries (can be used to give access to or remove access to privileged actions)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="614"/>
        <source>GetAllContactTypes</source>
        <translation>get all the types of contacts that can exist for a customer (needed by almost every user)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="615"/>
        <source>CreateContactType</source>
        <translation>create a new contact type</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="616"/>
        <source>GetCustomer</source>
        <translation>get customer data (needed by almost everyone)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="617"/>
        <source>GetAllCustomerNames</source>
        <translation>get the basic data of all customers to show a list of customers (needed by almost everyone)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="618"/>
        <source>CreateCustomer</source>
        <translation>create a new customer (needed by everyone who sells/orders)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="619"/>
        <source>ChangeCustomer</source>
        <translation>change customer data (needed by everyone who sells/orders)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="621"/>
        <source>DeleteCustomer</source>
        <translation>delete a customer</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="622"/>
        <source>GetAddress</source>
        <translation>retrieve customer address (needed by everyone)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="623"/>
        <source>GetAllCountries</source>
        <translation>get country definitions (needed by everyone who sells/orders)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="624"/>
        <source>CreateCountry</source>
        <translation>create new country</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="625"/>
        <source>ResetCustomerPassword</source>
        <translation>reset the web-login of a customer (needed by customer support)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="627"/>
        <source>GetAllArtists</source>
        <translation>retrieve data of all known artists (needed for creating new events)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="628"/>
        <source>CreateArtist</source>
        <translation>create a new artist (needed for creating new events)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="629"/>
        <source>GetAllPriceCategories</source>
        <translation>retrieve existing (general) price categories (needed to create new events)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="630"/>
        <source>CreatePriceCategory</source>
        <translation>create new price category (rarely needed by people who are allowed to set price policy)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="631"/>
        <source>ChangePriceCategory</source>
        <translation>change existing price category (rarely needed by people who are allowed to set price policy)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="632"/>
        <source>GetEvent</source>
        <translation>retrieve specific event (needed by most people)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="633"/>
        <source>GetAllEvents</source>
        <translation>retrieve detailed data of all events (needed by everyone selling tickets)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="634"/>
        <source>GetEventList</source>
        <translation>get events by a list of event IDs (needed for order processing, showing orders)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="636"/>
        <source>CreateEvent</source>
        <translation>create a new event</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="637"/>
        <source>ChangeEvent</source>
        <translation>change details of an existing event</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="638"/>
        <source>CancelEvent</source>
        <translation>cancel an event</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="639"/>
        <source>GetAllRooms</source>
        <translation>get the names of all existing rooms/venues</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="640"/>
        <source>CreateRoom</source>
        <translation>create a new room/theater/venue</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="644"/>
        <source>GetEventSummary</source>
        <translation>retrieve summary data for an event</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="645"/>
        <source>GetTicket</source>
        <translation>get data about a specific ticket</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="646"/>
        <source>GetVoucher</source>
        <translation>get data about a specific voucher</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="696"/>
        <source>GetVoucherAudit</source>
        <translation>get audit data for a specific voucher (when bought, used, etc.; only for auditors)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="697"/>
        <source>GetOrderAudit</source>
        <translation>get audit data about an order (when ordered, sent, payed,...; auditors only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="647"/>
        <source>GetOrder</source>
        <translation>get detailed data for an order (usually allowed for everyone who sells/orders; necessary for processing orders)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="582"/>
        <source>BackupExplore</source>
        <translation>explore backup plan</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="583"/>
        <source>BackupTable</source>
        <translation>backup a table</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="584"/>
        <source>RestoreBackup</source>
        <translation>restore backup</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="620"/>
        <source>ChangeCustomerMail</source>
        <translation>change the mail address that a customer uses to log into the web-shop (needed by customer support)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="626"/>
        <source>GetCreateCustomerHints</source>
        <translation>get the hints shown in the &quot;create customer&quot; wizard (necessary for everyone who sells/orders using the wizard)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="635"/>
        <source>GetEventSaleInfo</source>
        <translation>get detailed info to aid sales with room plans</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="641"/>
        <source>GetAllSeatPlans</source>
        <translation>retrieve all seat plans</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="642"/>
        <source>CreateSeatPlan</source>
        <translation>create new seat plan</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="643"/>
        <source>UpdateSeatPlan</source>
        <translation>update seat plan definition</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="648"/>
        <source>GetOrderList</source>
        <translation>get a list of orders (overview)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="649"/>
        <source>GetOrdersByEvents</source>
        <translation>get a list of all orders that contain tickets for an event</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="650"/>
        <source>GetOrdersByCustomer</source>
        <translation>get all orders by a specific customer</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="651"/>
        <source>GetMyOrders</source>
        <translation>get all orders processed by the current user</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="652"/>
        <source>GetOrdersByUser</source>
        <translation>get all orders processed by another user (auditors; privileged order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="653"/>
        <source>GetOrderByBarcode</source>
        <translation>get the order that contains a specific ticket or voucher (order processing, everyone accepting vouchers for payment)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="654"/>
        <source>GetOrdersByCoupon</source>
        <translation>get orders that used a specific coupon</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="655"/>
        <source>CreateOrder</source>
        <translation>create a new order (everyone selling/ordering; specific sub-privileges must be set)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="656"/>
        <source>CreateReservation</source>
        <translation>create a reservation (order in &quot;reserved&quot; state)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="657"/>
        <source>ReservationToOrder</source>
        <translation>change a reservation into a regular order or sale (order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="658"/>
        <source>CancelOrder</source>
        <translation>cancel an order (order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="659"/>
        <source>OrderPay</source>
        <translation>pay for an order (order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="660"/>
        <source>OrderRefund</source>
        <translation>refund an order (order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="661"/>
        <source>UseVoucher</source>
        <translation>use a voucher to pay for an order (order processing, everyone accepting vouchers)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="662"/>
        <source>DeductVoucher</source>
        <translation>deduct money from a voucher (outside the system, not for an order)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="663"/>
        <source>EmptyVoucher</source>
        <translation>make a voucher invalid (remaining value goes to zero, price remains)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="664"/>
        <source>ChangeVoucherValidity</source>
        <translation>change validity period of voucher</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="665"/>
        <source>OrderChangeShipping</source>
        <translation>change the shipping method of an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="666"/>
        <source>OrderMarkShipped</source>
        <translation>mark an order as being shipped</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="667"/>
        <source>OrderAddComment</source>
        <translation>add a comment to an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="668"/>
        <source>OrderChangeComments</source>
        <translation>change the comment of an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="669"/>
        <source>ReturnTicketVoucher</source>
        <translation>return a ticket or voucher unused</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="670"/>
        <source>ChangeTicketPrice</source>
        <translation>change the price of a specific ticket (automatically changes price of the order)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="671"/>
        <source>ChangeTicketPriceCategory</source>
        <translation>change the price category of an existing ticket (privileged: customer support, order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="672"/>
        <source>ChangeOrderAddress</source>
        <translation>change the address on an order (customer support, order processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="673"/>
        <source>GetAllShipping</source>
        <translation>get all allowed shipping types (anyone creating orders that are to be shipped)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="674"/>
        <source>ChangeShipping</source>
        <translation>change a shipping method (privileged users)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="675"/>
        <source>CreateShipping</source>
        <translation>create a new shipping type (privileged users)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="676"/>
        <source>DeleteShipping</source>
        <translation>delete a shipping type (privileged users)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="677"/>
        <source>GetValidVoucherPrices</source>
        <translation>get the allowed voucher prices</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="678"/>
        <source>UseTicket</source>
        <translation>mark a ticket as used (checking tickets at the theater entrance)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="679"/>
        <source>GetEntranceEvents</source>
        <translation>retrieve all events that are playing now (for checking tickets at the theater entrance)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="680"/>
        <source>GetPaymentTypes</source>
        <translation>get all types of payment (order/payment processing)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="681"/>
        <source>SetPaymentType</source>
        <translation>create a new type of payment (privileged)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="682"/>
        <source>SetDefaultPaymentType</source>
        <translation>set the default payment type (privileged)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="683"/>
        <source>DeletePaymentType</source>
        <translation>delete a payment type (privileged)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="684"/>
        <source>GetOrderDocumentNames</source>
        <translation>get names of documents attached to an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="685"/>
        <source>GetOrderDocument</source>
        <translation>get document attached to an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="686"/>
        <source>SetOrderDocument</source>
        <translation>upload document and attach it to an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="687"/>
        <source>DeleteOrderDocument</source>
        <translation>delete a document attached to an order</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="688"/>
        <source>SendCustomerMail</source>
        <translation>send a mail to a customer</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="689"/>
        <source>GetPrintAtHomeSettings</source>
        <translation>get settings for the Print@Home app</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="690"/>
        <source>SetPrintAtHomeSettings</source>
        <translation>change settings for the Print@Home app</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="691"/>
        <source>GetCoupon</source>
        <translation>get all data for a specific coupon</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="692"/>
        <source>GetCouponList</source>
        <translation>get a list of all coupon codes</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="693"/>
        <source>CreateCoupon</source>
        <translation>create a new coupon</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="694"/>
        <source>ChangeCoupon</source>
        <translation>change a coupon</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="695"/>
        <source>GetTicketAudit</source>
        <translation>get audit data for a ticket (when bought, paid, used; auditors only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="698"/>
        <source>GetUserAudit</source>
        <translation>get audit data for a user (what orders were created/changed/...; auditors only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="699"/>
        <source>WebCartAddTicket</source>
        <translation>add a ticket to the web cart (web system only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="700"/>
        <source>WebCartRemoveTicket</source>
        <translation>remove a ticket from the web cart (web system only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="701"/>
        <source>WebCartAddVoucher</source>
        <translation>add a voucher to the web cart (web system only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="702"/>
        <source>WebCartRemoveVoucher</source>
        <translation>remove a voucher from the web cart (web system only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="703"/>
        <source>WebCartAddCoupon</source>
        <translation>attach a coupon to the cart (web system only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="704"/>
        <source>GetTemplateList</source>
        <translation>get the list of existing templates (anyone printing tickets, vouchers, summaries, or bills)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="705"/>
        <source>GetTemplate</source>
        <translation>retrieve a specific template (anyone printing; access to specific templates can be restricted with flags)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="706"/>
        <source>SetTemplate</source>
        <translation>create/change a template</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="707"/>
        <source>SetTemplateDescription</source>
        <translation>set the description of a template</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="708"/>
        <source>DeleteTemplate</source>
        <translation>delete a template</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="709"/>
        <source>SetTemplateFlags</source>
        <translation>set flags of a template (used to regulate access to templates)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="710"/>
        <source>ChangeEvent:CancelEvent</source>
        <translation>the user can cancel an event (see also the CancelEvent right)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="711"/>
        <source>CreateOrder:AnyVoucherValue</source>
        <translation>the user can create vouchers with arbitrary values (normally only specific values are allowed)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="712"/>
        <source>CreateOrder:DiffVoucherValuePrice</source>
        <translation>the user can create vouchers whose price differs from the value (privileged)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="713"/>
        <source>CreateOrder:LateSale</source>
        <translation>the user can create sales when the event has already started (but not after it is over; theater booth for late arrivals)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="714"/>
        <source>CreateOrder:AfterTheFactSale</source>
        <translation>the user can create sales for events that are already over (privileged)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="715"/>
        <source>CreateOrder:CanOrder</source>
        <translation>the user can create orders (anyone creating orders that are to be paid later)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="716"/>
        <source>CreateOrder:CanSell</source>
        <translation>the user can create sales (anyone selling directly to customers with immediate payment)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="717"/>
        <source>CreateOrder:CanOrderTicket</source>
        <translation>user can sell/order tickets</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="718"/>
        <source>CreateOrder:CanOrderVoucher</source>
        <translation>user can sell/order vouchers</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="719"/>
        <source>CreateOrder:CanOrderItem</source>
        <translation>the user can sell shop items (merchandising)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="720"/>
        <source>CreateOrder:CanPayVoucherWithVoucher</source>
        <translation>user can pay new vouchers with older vouchers (while selling/ordering only)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="721"/>
        <source>CreateReservation:LateReserve</source>
        <translation>the user can create a reservation right to the start of the event (normally reservation ends a configured amount of days earlier)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="722"/>
        <source>CancelOrder:CancelSentOrder</source>
        <translation>the user can also cancel orders that are already shipped out</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="723"/>
        <source>CancelOrder:CancelPastTickets</source>
        <translation>the user can cancel tickets for events that have already started or are over</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="724"/>
        <source>UseVoucher:CanPayVoucherWithVoucher</source>
        <translation>user can pay new voucher with older voucher (while using voucher on existing order)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="725"/>
        <source>OrderChangeShipping:ChangePrice</source>
        <translation>the user is allowed to change the shipping price arbitrarily (normally a fixed price per shipping type is used)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="726"/>
        <source>OrderMarkShipped:SetTime</source>
        <translation>the user can set a different time for when the order was shipped</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="727"/>
        <source>ReturnTicketVoucher:ReturnPastTicket</source>
        <translation>a ticket for an event that is already over can be returned</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="728"/>
        <source>ChangeTicketPrice:ChangeUsedTicket</source>
        <translation>the user can change the price of a ticket that is already in use (customer has entered the theater)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="729"/>
        <source>ChangeTicketPrice:ChangePastTicket</source>
        <translation>the user can change the price of a ticket for an event that is already over</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="730"/>
        <source>ChangeTicketPriceCategory:ChangeUsedTicket</source>
        <translation>the user can change the price-category of a ticket that is already in use (customer has entered the theater)</translation>
    </message>
    <message>
        <location filename="wob/srcMInterface.cpp" line="731"/>
        <source>ChangeTicketPriceCategory:ChangePastTicket</source>
        <translation>the user can change the price-category of a ticket for an event that is already over</translation>
    </message>
</context>
<context>
    <name>MLocalFormat</name>
    <message>
        <location filename="misc/misc.cpp" line="211"/>
        <source>Sunday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="212"/>
        <source>Monday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="213"/>
        <source>Tuesday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="214"/>
        <source>Wednesday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="215"/>
        <source>Thursday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="216"/>
        <source>Friday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="217"/>
        <source>Saturday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="227"/>
        <source>Sun</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="228"/>
        <source>Mon</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="229"/>
        <source>Tue</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="230"/>
        <source>Wed</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="231"/>
        <source>Thu</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="232"/>
        <source>Fri</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="233"/>
        <source>Sat</source>
        <comment>short weekday</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="243"/>
        <source>January</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="244"/>
        <source>February</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="245"/>
        <source>March</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="246"/>
        <source>April</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="247"/>
        <source>May</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="248"/>
        <source>June</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="249"/>
        <source>July</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="250"/>
        <source>August</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="251"/>
        <source>September</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="252"/>
        <source>October</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="253"/>
        <source>November</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="254"/>
        <source>December</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="264"/>
        <source>Jan</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="265"/>
        <source>Feb</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="266"/>
        <source>Mar</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="267"/>
        <source>Apr</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="268"/>
        <source>May</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="269"/>
        <source>Jun</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="270"/>
        <source>Jul</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="271"/>
        <source>Aug</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="272"/>
        <source>Sep</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="273"/>
        <source>Oct</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="274"/>
        <source>Nov</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="275"/>
        <source>Dec</source>
        <comment>short month name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="284"/>
        <source>%Y-%M-%D</source>
        <comment>date format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="286"/>
        <source>%h:%I</source>
        <comment>time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="288"/>
        <source>%Y-%M-%D %h:%I</source>
        <comment>date and time format</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="312"/>
        <source>am</source>
        <comment>AM/PM time component</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="314"/>
        <source>pm</source>
        <comment>AM/PM time component</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="322"/>
        <source>.</source>
        <comment>decimal dot</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="325"/>
        <source>,</source>
        <comment>thousand division character</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="misc/misc.cpp" line="328"/>
        <source>0</source>
        <comment>digits between thousand division chars, &lt;=0 means none</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOCartOrder</name>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="26"/>
        <location filename="wob/srcMOCartOrder.cpp" line="33"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="27"/>
        <location filename="wob/srcMOCartOrder.cpp" line="34"/>
        <source>Invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOCartTicket</name>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="32"/>
        <location filename="wob/srcMOCartTicket.cpp" line="42"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="33"/>
        <location filename="wob/srcMOCartTicket.cpp" line="43"/>
        <source>EventOver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="34"/>
        <location filename="wob/srcMOCartTicket.cpp" line="44"/>
        <source>TooLate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="35"/>
        <location filename="wob/srcMOCartTicket.cpp" line="45"/>
        <source>Exhausted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartTicket.cpp" line="36"/>
        <location filename="wob/srcMOCartTicket.cpp" line="46"/>
        <source>Invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOCartVoucher</name>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="28"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="36"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="29"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="37"/>
        <source>InvalidValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartVoucher.cpp" line="30"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="38"/>
        <source>InvalidPrice</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOEvent</name>
    <message>
        <location filename="wext/event.cpp" line="49"/>
        <source>.</source>
        <comment>price decimal dot</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOOrderAbstract</name>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="34"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="45"/>
        <source>Placed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="35"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="46"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="36"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="47"/>
        <source>Sold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="37"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="48"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="38"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="49"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAbstract.cpp" line="39"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="50"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOOrderAudit</name>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="34"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="45"/>
        <source>Placed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="35"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="46"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="36"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="47"/>
        <source>Sold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="37"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="48"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="38"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="49"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderAudit.cpp" line="39"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="50"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOOrderInfoAbstract</name>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="34"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="45"/>
        <source>Placed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="35"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="46"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="36"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="47"/>
        <source>Sold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="37"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="48"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="38"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="49"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="39"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="50"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOServerFormat</name>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="34"/>
        <location filename="wob/srcMOServerFormat.cpp" line="45"/>
        <source>NoSign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="35"/>
        <location filename="wob/srcMOServerFormat.cpp" line="46"/>
        <source>SignBeforeNum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="36"/>
        <location filename="wob/srcMOServerFormat.cpp" line="47"/>
        <source>SignAfterNum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="37"/>
        <location filename="wob/srcMOServerFormat.cpp" line="48"/>
        <source>SignBeforeSym</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="38"/>
        <location filename="wob/srcMOServerFormat.cpp" line="49"/>
        <source>SignAfterSym</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOServerFormat.cpp" line="39"/>
        <location filename="wob/srcMOServerFormat.cpp" line="50"/>
        <source>SignParen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOTicketAbstract</name>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="42"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="57"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="43"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="58"/>
        <source>Ordered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="44"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="59"/>
        <source>Used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="45"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="60"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="46"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="61"/>
        <source>Refund</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="47"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="62"/>
        <source>MaskBlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="48"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="63"/>
        <source>MaskPay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="49"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="64"/>
        <source>MaskUsable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="50"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="65"/>
        <source>MaskReturnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketAbstract.cpp" line="51"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="66"/>
        <source>MaskChangeable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOTicketSaleInfo</name>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="42"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="57"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="43"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="58"/>
        <source>Ordered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="44"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="59"/>
        <source>Used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="45"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="60"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="46"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="61"/>
        <source>Refund</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="47"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="62"/>
        <source>MaskBlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="48"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="63"/>
        <source>MaskPay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="49"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="64"/>
        <source>MaskUsable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="50"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="65"/>
        <source>MaskReturnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="51"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="66"/>
        <source>MaskChangeable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOTicketUse</name>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="36"/>
        <location filename="wob/srcMOTicketUse.cpp" line="48"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="37"/>
        <location filename="wob/srcMOTicketUse.cpp" line="49"/>
        <source>NotFound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="38"/>
        <location filename="wob/srcMOTicketUse.cpp" line="50"/>
        <source>WrongEvent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="39"/>
        <location filename="wob/srcMOTicketUse.cpp" line="51"/>
        <source>AlreadyUsed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="40"/>
        <location filename="wob/srcMOTicketUse.cpp" line="52"/>
        <source>NotUsable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="41"/>
        <location filename="wob/srcMOTicketUse.cpp" line="53"/>
        <source>Unpaid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOTicketUse.cpp" line="42"/>
        <location filename="wob/srcMOTicketUse.cpp" line="54"/>
        <source>InvalidEvent</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOVoucher</name>
    <message>
        <location filename="wext/voucher.h" line="41"/>
        <source>unlimited</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MOVoucherAbstract</name>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="30"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="39"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="31"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="40"/>
        <source>InvalidValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="32"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="41"/>
        <source>InvalidPrice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="33"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="42"/>
        <source>InvalidTime</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MSInterface</name>
    <message>
        <location filename="msinterface.cpp" line="110"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="110"/>
        <source>Login failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="152"/>
        <location filename="msinterface.cpp" line="156"/>
        <location filename="msinterface.cpp" line="161"/>
        <location filename="msinterface.cpp" line="166"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="152"/>
        <source>Communication problem while talking to the server, see log for details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="156"/>
        <source>Communication with server was not successful.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="161"/>
        <source>The server implementation is too old for this client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="166"/>
        <source>This client is too old for the server, please upgrade.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="271"/>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="msinterface.cpp" line="271"/>
        <source>There were problems while authenticating the server. Aborting. Check your configuration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MTemplateStore</name>
    <message>
        <location filename="templates/templates.cpp" line="147"/>
        <source>Retrieving templates from server.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WobTransaction</name>
    <message>
        <location filename="wob/srcMOAddressAbstract.cpp" line="114"/>
        <location filename="wob/srcMOAddressAbstract.cpp" line="120"/>
        <location filename="wob/srcMOAddressAbstract.cpp" line="126"/>
        <location filename="wob/srcMOArtist.cpp" line="83"/>
        <location filename="wob/srcMOCartOrder.cpp" line="129"/>
        <location filename="wob/srcMOCartOrder.cpp" line="141"/>
        <location filename="wob/srcMOCartOrder.cpp" line="147"/>
        <location filename="wob/srcMOCartOrder.cpp" line="153"/>
        <location filename="wob/srcMOCartOrder.cpp" line="163"/>
        <location filename="wob/srcMOCartTicket.cpp" line="149"/>
        <location filename="wob/srcMOCartTicket.cpp" line="155"/>
        <location filename="wob/srcMOCartTicket.cpp" line="161"/>
        <location filename="wob/srcMOCartTicket.cpp" line="167"/>
        <location filename="wob/srcMOCartTicket.cpp" line="173"/>
        <location filename="wob/srcMOCartTicket.cpp" line="179"/>
        <location filename="wob/srcMOCartTicket.cpp" line="194"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="106"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="112"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="118"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="124"/>
        <location filename="wob/srcMOContact.cpp" line="69"/>
        <location filename="wob/srcMOContact.cpp" line="75"/>
        <location filename="wob/srcMOContact.cpp" line="81"/>
        <location filename="wob/srcMOContactType.cpp" line="61"/>
        <location filename="wob/srcMOCoupon.cpp" line="93"/>
        <location filename="wob/srcMOCoupon.cpp" line="99"/>
        <location filename="wob/srcMOCoupon.cpp" line="105"/>
        <location filename="wob/srcMOCoupon.cpp" line="111"/>
        <location filename="wob/srcMOCouponInfo.cpp" line="78"/>
        <location filename="wob/srcMOCouponInfo.cpp" line="84"/>
        <location filename="wob/srcMOCouponRule.cpp" line="77"/>
        <location filename="wob/srcMOCouponRule.cpp" line="86"/>
        <location filename="wob/srcMOCouponRule.cpp" line="92"/>
        <location filename="wob/srcMOCouponRule.cpp" line="98"/>
        <location filename="wob/srcMOCouponRule.cpp" line="104"/>
        <location filename="wob/srcMOCouponRule.cpp" line="110"/>
        <location filename="wob/srcMOCustomerAbstract.cpp" line="106"/>
        <location filename="wob/srcMOCustomerInfoAbstract.cpp" line="68"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="166"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="172"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="178"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="184"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="200"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="214"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="231"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="237"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="243"/>
        <location filename="wob/srcMOEventAbstract.cpp" line="257"/>
        <location filename="wob/srcMOEventPrice.cpp" line="101"/>
        <location filename="wob/srcMOEventPrice.cpp" line="107"/>
        <location filename="wob/srcMOEventPrice.cpp" line="117"/>
        <location filename="wob/srcMOEventPrice.cpp" line="123"/>
        <location filename="wob/srcMOEventPrice.cpp" line="129"/>
        <location filename="wob/srcMOEventPrice.cpp" line="146"/>
        <location filename="wob/srcMOEventPrice.cpp" line="152"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="89"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="98"/>
        <location filename="wob/srcMOEventSaleInfoAbstract.cpp" line="114"/>
        <location filename="wob/srcMOItemAudit.cpp" line="57"/>
        <location filename="wob/srcMOItemInfo.cpp" line="74"/>
        <location filename="wob/srcMOItemInfo.cpp" line="80"/>
        <location filename="wob/srcMOItemInfo.cpp" line="90"/>
        <location filename="wob/srcMOItemInfo.cpp" line="96"/>
        <location filename="wob/srcMOItemInfo.cpp" line="102"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="214"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="220"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="230"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="240"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="277"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="283"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="289"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="295"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="301"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="311"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="317"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="157"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="163"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="178"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="184"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="190"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="196"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="202"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="81"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="87"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="97"/>
        <location filename="wob/srcMOOrderDocument.cpp" line="103"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="74"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="80"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="90"/>
        <location filename="wob/srcMOOrderDocumentInfo.cpp" line="96"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="163"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="169"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="178"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="184"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="190"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="202"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="208"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="214"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="220"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="226"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="232"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="238"/>
        <location filename="wob/srcMOPriceCategory.cpp" line="85"/>
        <location filename="wob/srcMORoom.cpp" line="65"/>
        <location filename="wob/srcMOSeatPlanAbstract.cpp" line="102"/>
        <location filename="wob/srcMOSeatPlanBackground.cpp" line="94"/>
        <location filename="wob/srcMOSeatPlanBackground.cpp" line="103"/>
        <location filename="wob/srcMOSeatPlanGroupAbstract.cpp" line="129"/>
        <location filename="wob/srcMOSeatPlanInfo.cpp" line="71"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="95"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="116"/>
        <location filename="wob/srcMOSeatPlanRow.cpp" line="122"/>
        <location filename="wob/srcMOSeatPlanSeat.cpp" line="57"/>
        <location filename="wob/srcMOSeatPlanSeat.cpp" line="66"/>
        <location filename="wob/srcMOServerFormat.cpp" line="251"/>
        <location filename="wob/srcMOServerFormat.cpp" line="257"/>
        <location filename="wob/srcMOShipping.cpp" line="71"/>
        <location filename="wob/srcMOShipping.cpp" line="77"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="154"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="160"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="172"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="178"/>
        <location filename="wob/srcMOTicketAudit.cpp" line="71"/>
        <location filename="wob/srcMOTicketAudit.cpp" line="88"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="136"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="142"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="154"/>
        <location filename="wob/srcMOTicketUse.cpp" line="132"/>
        <location filename="wob/srcMOTicketUse.cpp" line="138"/>
        <location filename="wob/srcMOTicketUse.cpp" line="144"/>
        <location filename="wob/srcMOTicketUse.cpp" line="150"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="125"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="131"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="146"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="156"/>
        <location filename="wob/srcMOVoucherAudit.cpp" line="64"/>
        <location filename="wob/srcMOWebCart.cpp" line="127"/>
        <location filename="wob/srcMOWebCart.cpp" line="133"/>
        <location filename="wob/srcMOWebCart.cpp" line="139"/>
        <location filename="wob/srcMOWebCart.cpp" line="145"/>
        <location filename="wob/srcMOWebCart.cpp" line="155"/>
        <location filename="wob/srcMOWebSession.cpp" line="68"/>
        <location filename="wob/srcMOWebSession.cpp" line="78"/>
        <source>Class &apos;%1&apos; property &apos;%2&apos; is integer, but non-integer was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMOCartOrder.cpp" line="135"/>
        <location filename="wob/srcMOCartTicket.cpp" line="185"/>
        <location filename="wob/srcMOCartVoucher.cpp" line="130"/>
        <location filename="wob/srcMOOrderAbstract.cpp" line="271"/>
        <location filename="wob/srcMOOrderAudit.cpp" line="172"/>
        <location filename="wob/srcMOOrderInfoAbstract.cpp" line="196"/>
        <location filename="wob/srcMOServerFormat.cpp" line="281"/>
        <location filename="wob/srcMOServerFormat.cpp" line="287"/>
        <location filename="wob/srcMOTicketAbstract.cpp" line="166"/>
        <location filename="wob/srcMOTicketSaleInfo.cpp" line="148"/>
        <location filename="wob/srcMOTicketUse.cpp" line="126"/>
        <location filename="wob/srcMOVoucherAbstract.cpp" line="137"/>
        <source>Class &apos;%1&apos; property &apos;%2&apos; is enum, invalid value was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMTBackup.cpp" line="94"/>
        <location filename="wob/srcMTBackupExplore.cpp" line="94"/>
        <location filename="wob/srcMTBackupTable.cpp" line="112"/>
        <location filename="wob/srcMTCancelEvent.cpp" line="101"/>
        <location filename="wob/srcMTCancelOrder.cpp" line="97"/>
        <location filename="wob/srcMTChangeCoupon.cpp" line="96"/>
        <location filename="wob/srcMTChangeCustomer.cpp" line="97"/>
        <location filename="wob/srcMTChangeCustomerMail.cpp" line="102"/>
        <location filename="wob/srcMTChangeEvent.cpp" line="97"/>
        <location filename="wob/srcMTChangeMyPassword.cpp" line="103"/>
        <location filename="wob/srcMTChangeOrderAddress.cpp" line="109"/>
        <location filename="wob/srcMTChangePassword.cpp" line="101"/>
        <location filename="wob/srcMTChangePriceCategory.cpp" line="97"/>
        <location filename="wob/srcMTChangeShipping.cpp" line="97"/>
        <location filename="wob/srcMTChangeTicketPrice.cpp" line="100"/>
        <location filename="wob/srcMTChangeTicketPriceCategory.cpp" line="100"/>
        <location filename="wob/srcMTChangeVoucherValidity.cpp" line="109"/>
        <location filename="wob/srcMTCreateArtist.cpp" line="109"/>
        <location filename="wob/srcMTCreateContactType.cpp" line="102"/>
        <location filename="wob/srcMTCreateCountry.cpp" line="100"/>
        <location filename="wob/srcMTCreateCoupon.cpp" line="97"/>
        <location filename="wob/srcMTCreateCustomer.cpp" line="97"/>
        <location filename="wob/srcMTCreateEvent.cpp" line="97"/>
        <location filename="wob/srcMTCreateOrder.cpp" line="110"/>
        <location filename="wob/srcMTCreatePriceCategory.cpp" line="97"/>
        <location filename="wob/srcMTCreateReservation.cpp" line="98"/>
        <location filename="wob/srcMTCreateRole.cpp" line="96"/>
        <location filename="wob/srcMTCreateRoom.cpp" line="107"/>
        <location filename="wob/srcMTCreateSeatPlan.cpp" line="97"/>
        <location filename="wob/srcMTCreateShipping.cpp" line="97"/>
        <location filename="wob/srcMTCreateUser.cpp" line="107"/>
        <location filename="wob/srcMTDeductVoucher.cpp" line="106"/>
        <location filename="wob/srcMTDeleteCustomer.cpp" line="100"/>
        <location filename="wob/srcMTDeleteFlag.cpp" line="96"/>
        <location filename="wob/srcMTDeleteHost.cpp" line="96"/>
        <location filename="wob/srcMTDeleteOrderDocument.cpp" line="101"/>
        <location filename="wob/srcMTDeletePaymentType.cpp" line="98"/>
        <location filename="wob/srcMTDeleteRole.cpp" line="96"/>
        <location filename="wob/srcMTDeleteShipping.cpp" line="96"/>
        <location filename="wob/srcMTDeleteTemplate.cpp" line="96"/>
        <location filename="wob/srcMTDeleteUser.cpp" line="99"/>
        <location filename="wob/srcMTEmptyVoucher.cpp" line="102"/>
        <location filename="wob/srcMTGetAddress.cpp" line="97"/>
        <location filename="wob/srcMTGetAllArtists.cpp" line="94"/>
        <location filename="wob/srcMTGetAllContactTypes.cpp" line="94"/>
        <location filename="wob/srcMTGetAllCountries.cpp" line="94"/>
        <location filename="wob/srcMTGetAllCustomerNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllEvents.cpp" line="94"/>
        <location filename="wob/srcMTGetAllHostNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllHosts.cpp" line="94"/>
        <location filename="wob/srcMTGetAllPriceCategories.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRightNames.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRoles.cpp" line="94"/>
        <location filename="wob/srcMTGetAllRooms.cpp" line="94"/>
        <location filename="wob/srcMTGetAllSeatPlans.cpp" line="94"/>
        <location filename="wob/srcMTGetAllShipping.cpp" line="94"/>
        <location filename="wob/srcMTGetAllUsers.cpp" line="94"/>
        <location filename="wob/srcMTGetCoupon.cpp" line="97"/>
        <location filename="wob/srcMTGetCouponList.cpp" line="97"/>
        <location filename="wob/srcMTGetCreateCustomerHints.cpp" line="98"/>
        <location filename="wob/srcMTGetCustomer.cpp" line="97"/>
        <location filename="wob/srcMTGetEntranceEvents.cpp" line="100"/>
        <location filename="wob/srcMTGetEvent.cpp" line="97"/>
        <location filename="wob/srcMTGetEventList.cpp" line="101"/>
        <location filename="wob/srcMTGetEventSaleInfo.cpp" line="97"/>
        <location filename="wob/srcMTGetEventSummary.cpp" line="98"/>
        <location filename="wob/srcMTGetLanguage.cpp" line="102"/>
        <location filename="wob/srcMTGetMyOrders.cpp" line="101"/>
        <location filename="wob/srcMTGetMyRights.cpp" line="95"/>
        <location filename="wob/srcMTGetMyRoles.cpp" line="94"/>
        <location filename="wob/srcMTGetOrder.cpp" line="97"/>
        <location filename="wob/srcMTGetOrderAudit.cpp" line="99"/>
        <location filename="wob/srcMTGetOrderByBarcode.cpp" line="99"/>
        <location filename="wob/srcMTGetOrderDocument.cpp" line="102"/>
        <location filename="wob/srcMTGetOrderDocumentNames.cpp" line="97"/>
        <location filename="wob/srcMTGetOrderList.cpp" line="98"/>
        <location filename="wob/srcMTGetOrdersByCoupon.cpp" line="101"/>
        <location filename="wob/srcMTGetOrdersByCustomer.cpp" line="100"/>
        <location filename="wob/srcMTGetOrdersByEvents.cpp" line="105"/>
        <location filename="wob/srcMTGetOrdersByUser.cpp" line="104"/>
        <location filename="wob/srcMTGetPaymentTypes.cpp" line="95"/>
        <location filename="wob/srcMTGetPrintAtHomeSettings.cpp" line="94"/>
        <location filename="wob/srcMTGetRole.cpp" line="97"/>
        <location filename="wob/srcMTGetTemplate.cpp" line="97"/>
        <location filename="wob/srcMTGetTemplateList.cpp" line="94"/>
        <location filename="wob/srcMTGetTicket.cpp" line="97"/>
        <location filename="wob/srcMTGetTicketAudit.cpp" line="98"/>
        <location filename="wob/srcMTGetUser.cpp" line="97"/>
        <location filename="wob/srcMTGetUserAudit.cpp" line="104"/>
        <location filename="wob/srcMTGetUserHosts.cpp" line="97"/>
        <location filename="wob/srcMTGetUserRoles.cpp" line="97"/>
        <location filename="wob/srcMTGetValidFlags.cpp" line="94"/>
        <location filename="wob/srcMTGetValidVoucherPrices.cpp" line="94"/>
        <location filename="wob/srcMTGetVoucher.cpp" line="98"/>
        <location filename="wob/srcMTGetVoucherAudit.cpp" line="97"/>
        <location filename="wob/srcMTLogin.cpp" line="115"/>
        <location filename="wob/srcMTLogout.cpp" line="93"/>
        <location filename="wob/srcMTOrderAddComment.cpp" line="102"/>
        <location filename="wob/srcMTOrderChangeComments.cpp" line="102"/>
        <location filename="wob/srcMTOrderChangeShipping.cpp" line="103"/>
        <location filename="wob/srcMTOrderMarkShipped.cpp" line="100"/>
        <location filename="wob/srcMTOrderPay.cpp" line="111"/>
        <location filename="wob/srcMTOrderRefund.cpp" line="101"/>
        <location filename="wob/srcMTReservationToOrder.cpp" line="97"/>
        <location filename="wob/srcMTResetCustomerPassword.cpp" line="96"/>
        <location filename="wob/srcMTRestoreBackup.cpp" line="101"/>
        <location filename="wob/srcMTReturnTicketVoucher.cpp" line="97"/>
        <location filename="wob/srcMTSendCustomerMail.cpp" line="101"/>
        <location filename="wob/srcMTServerInfo.cpp" line="96"/>
        <location filename="wob/srcMTSetDefaultPaymentType.cpp" line="99"/>
        <location filename="wob/srcMTSetFlag.cpp" line="96"/>
        <location filename="wob/srcMTSetHost.cpp" line="101"/>
        <location filename="wob/srcMTSetOrderDocument.cpp" line="96"/>
        <location filename="wob/srcMTSetPaymentType.cpp" line="97"/>
        <location filename="wob/srcMTSetPrintAtHomeSettings.cpp" line="99"/>
        <location filename="wob/srcMTSetRoleDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetRoleFlags.cpp" line="103"/>
        <location filename="wob/srcMTSetRoleRights.cpp" line="103"/>
        <location filename="wob/srcMTSetTemplate.cpp" line="107"/>
        <location filename="wob/srcMTSetTemplateDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetTemplateFlags.cpp" line="99"/>
        <location filename="wob/srcMTSetUserDescription.cpp" line="101"/>
        <location filename="wob/srcMTSetUserFlags.cpp" line="103"/>
        <location filename="wob/srcMTSetUserHosts.cpp" line="103"/>
        <location filename="wob/srcMTSetUserRoles.cpp" line="103"/>
        <location filename="wob/srcMTUpdateSeatPlan.cpp" line="96"/>
        <location filename="wob/srcMTUseTicket.cpp" line="100"/>
        <location filename="wob/srcMTUseVoucher.cpp" line="102"/>
        <location filename="wob/srcMTWebCartAddCoupon.cpp" line="99"/>
        <location filename="wob/srcMTWebCartAddTicket.cpp" line="105"/>
        <location filename="wob/srcMTWebCartAddVoucher.cpp" line="99"/>
        <location filename="wob/srcMTWebCartRemoveTicket.cpp" line="102"/>
        <location filename="wob/srcMTWebCartRemoveVoucher.cpp" line="99"/>
        <source>XML result parser error: empty response.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wob/srcMTBackup.cpp" line="102"/>
        <location filename="wob/srcMTBackupExplore.cpp" line="102"/>
        <location filename="wob/srcMTBackupTable.cpp" line="120"/>
        <location filename="wob/srcMTCancelEvent.cpp" line="109"/>
        <location filename="wob/srcMTCancelOrder.cpp" line="105"/>
        <location filename="wob/srcMTChangeCoupon.cpp" line="104"/>
        <location filename="wob/srcMTChangeCustomer.cpp" line="105"/>
        <location filename="wob/srcMTChangeCustomerMail.cpp" line="110"/>
        <location filename="wob/srcMTChangeEvent.cpp" line="105"/>
        <location filename="wob/srcMTChangeMyPassword.cpp" line="111"/>
        <location filename="wob/srcMTChangeOrderAddress.cpp" line="117"/>
        <location filename="wob/srcMTChangePassword.cpp" line="109"/>
        <location filename="wob/srcMTChangePriceCategory.cpp" line="105"/>
        <location filename="wob/srcMTChangeShipping.cpp" line="105"/>
        <location filename="wob/srcMTChangeTicketPrice.cpp" line="108"/>
        <location filename="wob/srcMTChangeTicketPriceCategory.cpp" line="108"/>
        <location filename="wob/srcMTChangeVoucherValidity.cpp" line="117"/>
        <location filename="wob/srcMTCreateArtist.cpp" line="117"/>
        <location filename="wob/srcMTCreateContactType.cpp" line="110"/>
        <location filename="wob/srcMTCreateCountry.cpp" line="108"/>
        <location filename="wob/srcMTCreateCoupon.cpp" line="105"/>
        <location filename="wob/srcMTCreateCustomer.cpp" line="105"/>
        <location filename="wob/srcMTCreateEvent.cpp" line="105"/>
        <location filename="wob/srcMTCreateOrder.cpp" line="118"/>
        <location filename="wob/srcMTCreatePriceCategory.cpp" line="105"/>
        <location filename="wob/srcMTCreateReservation.cpp" line="106"/>
        <location filename="wob/srcMTCreateRole.cpp" line="104"/>
        <location filename="wob/srcMTCreateRoom.cpp" line="115"/>
        <location filename="wob/srcMTCreateSeatPlan.cpp" line="105"/>
        <location filename="wob/srcMTCreateShipping.cpp" line="105"/>
        <location filename="wob/srcMTCreateUser.cpp" line="115"/>
        <location filename="wob/srcMTDeductVoucher.cpp" line="114"/>
        <location filename="wob/srcMTDeleteCustomer.cpp" line="108"/>
        <location filename="wob/srcMTDeleteFlag.cpp" line="104"/>
        <location filename="wob/srcMTDeleteHost.cpp" line="104"/>
        <location filename="wob/srcMTDeleteOrderDocument.cpp" line="109"/>
        <location filename="wob/srcMTDeletePaymentType.cpp" line="106"/>
        <location filename="wob/srcMTDeleteRole.cpp" line="104"/>
        <location filename="wob/srcMTDeleteShipping.cpp" line="104"/>
        <location filename="wob/srcMTDeleteTemplate.cpp" line="104"/>
        <location filename="wob/srcMTDeleteUser.cpp" line="107"/>
        <location filename="wob/srcMTEmptyVoucher.cpp" line="110"/>
        <location filename="wob/srcMTGetAddress.cpp" line="105"/>
        <location filename="wob/srcMTGetAllArtists.cpp" line="102"/>
        <location filename="wob/srcMTGetAllContactTypes.cpp" line="102"/>
        <location filename="wob/srcMTGetAllCountries.cpp" line="102"/>
        <location filename="wob/srcMTGetAllCustomerNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllEvents.cpp" line="102"/>
        <location filename="wob/srcMTGetAllHostNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllHosts.cpp" line="102"/>
        <location filename="wob/srcMTGetAllPriceCategories.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRightNames.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRoles.cpp" line="102"/>
        <location filename="wob/srcMTGetAllRooms.cpp" line="102"/>
        <location filename="wob/srcMTGetAllSeatPlans.cpp" line="102"/>
        <location filename="wob/srcMTGetAllShipping.cpp" line="102"/>
        <location filename="wob/srcMTGetAllUsers.cpp" line="102"/>
        <location filename="wob/srcMTGetCoupon.cpp" line="105"/>
        <location filename="wob/srcMTGetCouponList.cpp" line="105"/>
        <location filename="wob/srcMTGetCreateCustomerHints.cpp" line="106"/>
        <location filename="wob/srcMTGetCustomer.cpp" line="105"/>
        <location filename="wob/srcMTGetEntranceEvents.cpp" line="108"/>
        <location filename="wob/srcMTGetEvent.cpp" line="105"/>
        <location filename="wob/srcMTGetEventList.cpp" line="109"/>
        <location filename="wob/srcMTGetEventSaleInfo.cpp" line="105"/>
        <location filename="wob/srcMTGetEventSummary.cpp" line="106"/>
        <location filename="wob/srcMTGetLanguage.cpp" line="110"/>
        <location filename="wob/srcMTGetMyOrders.cpp" line="109"/>
        <location filename="wob/srcMTGetMyRights.cpp" line="103"/>
        <location filename="wob/srcMTGetMyRoles.cpp" line="102"/>
        <location filename="wob/srcMTGetOrder.cpp" line="105"/>
        <location filename="wob/srcMTGetOrderAudit.cpp" line="107"/>
        <location filename="wob/srcMTGetOrderByBarcode.cpp" line="107"/>
        <location filename="wob/srcMTGetOrderDocument.cpp" line="110"/>
        <location filename="wob/srcMTGetOrderDocumentNames.cpp" line="105"/>
        <location filename="wob/srcMTGetOrderList.cpp" line="106"/>
        <location filename="wob/srcMTGetOrdersByCoupon.cpp" line="109"/>
        <location filename="wob/srcMTGetOrdersByCustomer.cpp" line="108"/>
        <location filename="wob/srcMTGetOrdersByEvents.cpp" line="113"/>
        <location filename="wob/srcMTGetOrdersByUser.cpp" line="112"/>
        <location filename="wob/srcMTGetPaymentTypes.cpp" line="103"/>
        <location filename="wob/srcMTGetPrintAtHomeSettings.cpp" line="102"/>
        <location filename="wob/srcMTGetRole.cpp" line="105"/>
        <location filename="wob/srcMTGetTemplate.cpp" line="105"/>
        <location filename="wob/srcMTGetTemplateList.cpp" line="102"/>
        <location filename="wob/srcMTGetTicket.cpp" line="105"/>
        <location filename="wob/srcMTGetTicketAudit.cpp" line="106"/>
        <location filename="wob/srcMTGetUser.cpp" line="105"/>
        <location filename="wob/srcMTGetUserAudit.cpp" line="112"/>
        <location filename="wob/srcMTGetUserHosts.cpp" line="105"/>
        <location filename="wob/srcMTGetUserRoles.cpp" line="105"/>
        <location filename="wob/srcMTGetValidFlags.cpp" line="102"/>
        <location filename="wob/srcMTGetValidVoucherPrices.cpp" line="102"/>
        <location filename="wob/srcMTGetVoucher.cpp" line="106"/>
        <location filename="wob/srcMTGetVoucherAudit.cpp" line="105"/>
        <location filename="wob/srcMTLogin.cpp" line="123"/>
        <location filename="wob/srcMTLogout.cpp" line="101"/>
        <location filename="wob/srcMTOrderAddComment.cpp" line="110"/>
        <location filename="wob/srcMTOrderChangeComments.cpp" line="110"/>
        <location filename="wob/srcMTOrderChangeShipping.cpp" line="111"/>
        <location filename="wob/srcMTOrderMarkShipped.cpp" line="108"/>
        <location filename="wob/srcMTOrderPay.cpp" line="119"/>
        <location filename="wob/srcMTOrderRefund.cpp" line="109"/>
        <location filename="wob/srcMTReservationToOrder.cpp" line="105"/>
        <location filename="wob/srcMTResetCustomerPassword.cpp" line="104"/>
        <location filename="wob/srcMTRestoreBackup.cpp" line="109"/>
        <location filename="wob/srcMTReturnTicketVoucher.cpp" line="105"/>
        <location filename="wob/srcMTSendCustomerMail.cpp" line="109"/>
        <location filename="wob/srcMTServerInfo.cpp" line="104"/>
        <location filename="wob/srcMTSetDefaultPaymentType.cpp" line="107"/>
        <location filename="wob/srcMTSetFlag.cpp" line="104"/>
        <location filename="wob/srcMTSetHost.cpp" line="109"/>
        <location filename="wob/srcMTSetOrderDocument.cpp" line="104"/>
        <location filename="wob/srcMTSetPaymentType.cpp" line="105"/>
        <location filename="wob/srcMTSetPrintAtHomeSettings.cpp" line="107"/>
        <location filename="wob/srcMTSetRoleDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetRoleFlags.cpp" line="111"/>
        <location filename="wob/srcMTSetRoleRights.cpp" line="111"/>
        <location filename="wob/srcMTSetTemplate.cpp" line="115"/>
        <location filename="wob/srcMTSetTemplateDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetTemplateFlags.cpp" line="107"/>
        <location filename="wob/srcMTSetUserDescription.cpp" line="109"/>
        <location filename="wob/srcMTSetUserFlags.cpp" line="111"/>
        <location filename="wob/srcMTSetUserHosts.cpp" line="111"/>
        <location filename="wob/srcMTSetUserRoles.cpp" line="111"/>
        <location filename="wob/srcMTUpdateSeatPlan.cpp" line="104"/>
        <location filename="wob/srcMTUseTicket.cpp" line="108"/>
        <location filename="wob/srcMTUseVoucher.cpp" line="110"/>
        <location filename="wob/srcMTWebCartAddCoupon.cpp" line="107"/>
        <location filename="wob/srcMTWebCartAddTicket.cpp" line="113"/>
        <location filename="wob/srcMTWebCartAddVoucher.cpp" line="107"/>
        <location filename="wob/srcMTWebCartRemoveTicket.cpp" line="110"/>
        <location filename="wob/srcMTWebCartRemoveVoucher.cpp" line="107"/>
        <source>XML result parser error line %1 col %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
