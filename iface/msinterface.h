//
// C++ Interface: msinterface
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MSINTERFACE_H
#define MAGICSMOKE_MSINTERFACE_H

#include "MInterface"

#define req (MSInterface::instance())

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


class MSslExceptions;
class MTemplateStore;

/**the MagicSmoke specific interface class - enhances the basic interface by some functionality needed in the MagicSmoke context*/
class MSIFACE_EXPORT MSInterface:public MInterface
{
	Q_OBJECT
	Q_PROPERTY(QString currentUser READ currentUser)
	Q_PROPERTY(QString hostName READ hostName)
	Q_PROPERTY(QString dataDir READ dataDir)
	Q_PROPERTY(QString settingsGroup READ settingsGroup)
	Q_PROPERTY(QString sessionId READ sessionId)
	Q_PROPERTY(QString profileId READ profileId)
	public:
		/**creates the interface object, expects the profile ID as parameter*/
		MSInterface(QString);
		/**deletes the interface*/
		~MSInterface();
		
		/**returns the singleton instance of the interface*/
		static MSInterface* instance(){return qobject_cast<MSInterface*>(MInterface::instance());}
		
		/**returns the name of the current user*/
		Q_INVOKABLE QString currentUser()const{return m_uname;}
		
		/**returns the name used for the host in this session*/
		Q_INVOKABLE QString hostName()const{return m_host;}
		
		/**returns whether the user is part of this role*/
		Q_INVOKABLE bool hasRole(QString s)const{return userroles.contains(s);}
		
		/**returns whether the user has a particular right*/
		Q_INVOKABLE bool hasRight(Right)const;
		
		///returns whether the user has a particular flag
		Q_INVOKABLE bool hasFlag(QString f)const{return userflags.contains(f);}
		
		///checks the flags in the string list and returns true if all of them match
		Q_INVOKABLE bool checkFlags(const QStringList&)const;
		
		///checks the space separeted flags in the string and returns true if all of them match
		Q_INVOKABLE bool checkFlags(const QString&s)const{return checkFlags(s.split(" "));}
		
		/**returns the directory where to store data retrieved from the server*/
		Q_INVOKABLE QString dataDir()const;
		/**returns the group in which to find settings in QSettings, this group can be used by any class that accesses the profile*/
		Q_INVOKABLE QString settingsGroup()const;
		/**returns the group where central profile settings are stored, this group is read-only for anything but the configuration dialog*/
		Q_INVOKABLE QString configSettingsGroup()const;
		
		/**checks the server for compatibility*/
		bool checkServer();
		
		/**returns the current session ID*/
		Q_INVOKABLE QString sessionId()const{return m_sessid;}
		
		/**returns default headers, ie. session ID*/
		virtual QMap<QString,QString> headers(QString)const;
		
		/**initializes the interface, ie. retrieves language and scripts*/
		void initialize();
		
		/**returns a pointer to the template storage engine*/
		Q_INVOKABLE MTemplateStore* templateStore(){return temp;}
		
		/**returns the profile ID of this session*/
		QString profileId()const{return profileid;}

		///returns the human readable name of the profile
		QString profileName()const;
		
		///return all rights of the current user
		QList<Right> allRights()const{return userrights;}
		
		///return all roles of the current user
		QStringList allRoles()const{return userroles;}
		
		///return all flags of the current user
		QStringList allFlags()const{return userflags;}

		///return the parent directory of the base URL
		QUrl parentUrl()const;

		///returns whether this instance automatically logs out on closure
		bool autoLogout()const{return autologout;}

		///sets whether this instance automatically logs out on closure
		void setAutoLogout(bool a){autologout=a;}
		
		///return application main data directory
		static QString appDataDir();

                ///override the application main data directory
                static void setAppDataDir(QString);

                ///helper function to resolve directory names with patterns
                ///the string $BASE is resolved as the content of the environment variable Unix: $HOME or Windows: %APPDIR%
                ///the string $APP is resolved as the directory the application is installed in
                ///any other component starting with $ resolves to the environment variable of the same name
                static QString resolveDir(const QString&dir);

		///returns the status bar text
		QString statusBarText()const{return m_statusbartext;}
	
	public slots:
		/**logs into the server, returns true on success*/
		bool login(QString username,QString passwd);
		/**initializes with a known session ID, returns true on success*/
		bool loginSession(QString username,QString sessionid);
		/**logs out of the server*/
		void logout();
		/**refreshes the login*/
		bool relogin();
		/**sets the session id to be transmitted*/
		void setSessionId(QString sid){m_sessid=sid;}
		/**handles SSL errors*/
		virtual void sslErrors(QNetworkReply *,const QList<QSslError>&);
		/**force template store to update its templates*/
		void updateTemplates();
		
	signals:
		/// emitted when a re-login would be necessary
		void needRelogin();
	private:
		QString profileid,m_sessid,m_uname,m_passwd,m_host,m_hostkey,m_statusbartext;
		mutable QList<Right>userrights;
		mutable QStringList userroles,userflags;
		QByteArray servertranslation;
		MSslExceptions*sslexcept=nullptr;
		bool didsslerror=false,autologout=false;
		MTemplateStore *temp=nullptr;
};


#endif
