//
// C++ Interface: sslexception
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_SSLEXCEPTION_H
#define MAGICSMOKE_SSLEXCEPTION_H

#include <QByteArray>
#include <QList>
#include <QMap>
#include <QPair>
#include <QSslCertificate>
#include <QSslError>
#include <QString>

class QWidget;

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

/**Helper class: stores and compares SSL-Exceptions*/
class MSIFACE_EXPORT MSslExceptions
{
	public:
		/**create instance from file in path*/
		MSslExceptions(QString path);

		/**saves the exceptions to config file for next session*/
		void savesslexcept();
		/**checks errors against the exception list, records all exceptions*/
		bool checksslexcept(const QList<QSslError>&);
		
		/**returns the current list of acceptable exceptions*/
		QList<QPair<QSslCertificate,int> > nonFatalExceptions()const{return sslexcept;}
		
		/**returns the list of collected exceptions*/
		QList<QPair<QSslCertificate,int> > collectedExceptions()const{return sslrecord;}
		
		/**clears the internal lists of exceptions*/
		void clear();
		
		/**clears the list of recorded exceptions*/
		void clearRecorded(){sslrecord.clear();}
		
		/**accepts the recorded exceptions*/
		void acceptRecorded();
		
	private:
		QList<QPair<QSslCertificate,int> > sslexcept,sslrecord;
		QString path;
};

#endif
