//
// C++ Implementation: misc
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "misc.h"

#include <TimeStamp>

#include <math.h>

#include "MOServerFormat"

#include <QCoreApplication>
#include <QDateTime>
#include <QSettings>
#include <QFileInfo>
#include <QDir>

QString htmlize(QString str)
{
	QString out;
	for(int i=0;i<str.size();i++){
		QChar c=str[i];
		ushort ci=c.unicode();
		if(c=='<')out+="&lt;";else
		if(c=='>')out+="&gt;";else
		if(c=='&')out+="&amp;";else
		if(c=='\n')out+="<br/>\n";else
		if(c.isSpace()||(c.unicode()>=32&&c.unicode()<=0x7f))out+=c;
		else out+="&#"+QString::number(ci)+";";
	}
	return out;
}

QString xmlize(QString str,QString newline)
{
	QString out;
	for(int i=0;i<str.size();i++){
		QChar c=str[i];
		ushort ci=c.unicode();
		if(c=='<')out+="&lt;";else
		if(c=='>')out+="&gt;";else
		if(c=='&')out+="&amp;";else
		if(c=='\n')out+=newline;else
		if(c.isSpace()||(c.unicode()>=32&&c.unicode()<=0x7f))out+=c;
		else out+="&#"+QString::number(ci)+";";
	}
	return out;
}
QByteArray xmlize(QByteArray str,QString newline)
{
	QByteArray out;
	for(int i=0;i<str.size();i++){
		char c=str[i];
		if(c=='<')out+="&lt;";else
		if(c=='>')out+="&gt;";else
		if(c=='&')out+="&amp;";else
		if(c=='\n')out+=newline.toUtf8();else
		if(QChar(c).isSpace()||(c>=32 && static_cast<unsigned char>(c)<=0x7f))out+=c;
		else out+="&#"+QByteArray::number((unsigned char)c)+";";
	}
	return out;
}

QString cent2str(qint64 c,bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.formatMoney(c);
}

qint64 str2cent(QString s,bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.scanMoney(s);
}

QRegularExpression priceRegExp(bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.moneyRegExp(false,false);
}

QString unix2date(qint64 tm,bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.formatDate(tm);
}

QString unix2time(qint64 tm,bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.formatTime(tm);
}

QString unix2dateTime(qint64 tm,bool localize)
{
	MLocalFormat mf;
	if(!localize)mf.setNonLocalized();
	return mf.formatDateTime(tm);
}

// /////////////////////////////////////
// local formatting class
#include <SharedDPtr>
class MLocalFormat::Private:public SharedDPtr
{
	public:
		QStringList m_day,m_sday,m_month,m_smonth;
		QString m_am,m_pm,m_timezone,m_dateformat,m_timeformat,m_datetimeformat;
		QChar m_decimal,m_thousand;

		QString m_currency,m_moneyneg,m_moneypos;
		int m_moneydecimals,m_thousanddigits;
		bool m_moneysymbehind;
		MOServerFormat::MoneyPos m_moneynegpos,m_moneypospos;
};
DEFINE_SHARED_DPTR(MLocalFormat);

//static
MLocalFormat MLocalFormat::defaultformat((int)1);

MLocalFormat::MLocalFormat(int)
{
	d->m_moneydecimals=2;
	d->m_thousanddigits=3;
	d->m_timezone="UTC";
        d->m_am="AM";
        d->m_pm="PM";
	//set defaults
	setWeekDays();setShortWeekDays();
	setMonths();setShortMonths();
	setMoneyFormat();setNumberFormat();
	setDateTimeFormat();
}

MLocalFormat::MLocalFormat(const MOServerFormat& s)
{
	//init from current default
	operator=(defaultformat);
	//overwrite with new settings
	QStringList sl;
	sl=s.weekdays();if(sl.size()==7)setWeekDays(sl);else setWeekDays();
	sl=s.shortweekdays();if(sl.size()==7)setShortWeekDays(sl);else setShortWeekDays();
	sl=s.months();if(sl.size()==12)setMonths(sl);else setMonths();
	sl=s.shortmonths();if(sl.size()==12)setShortMonths(sl);else setShortMonths();
	setMoneyFormat(s.currencysymbol(),s.moneydecimals(),s.currencysymbolpos());
	setMoneySign(s.moneynegative(),s.moneypositive(),s.moneynegativepos(),s.moneypositivepos());
	setNumberFormat(s.decimaldot().value().at(0), s.thousandseparator().value().at(0), s.thousanddigits().value());
	setAP(s.amtext(),s.pmtext());
	setTimeZone(s.timezone());
	setDateTimeFormat(s.dateformat(),s.timeformat(),s.datetimeformat());
}

MLocalFormat::~MLocalFormat(){}

QStringList MLocalFormat::weekDayNames()const{return d->m_day;}
QStringList MLocalFormat::shortWeekDayNames()const{return d->m_sday;}
QStringList MLocalFormat:: monthNames()const{return d->m_month;}
QStringList MLocalFormat::shortMonthNames()const{return d->m_smonth;}
QString MLocalFormat::currency()const{return d->m_currency;}
QChar MLocalFormat::decimalDot()const{return d->m_decimal;}
QString MLocalFormat::thousandSeparator()const
{
	if(d->m_thousanddigits>0)
		return d->m_thousand;
	else return "";
}
int MLocalFormat::thousandDigits()const{return d->m_thousanddigits;}
int MLocalFormat::moneyDecimals()const{return d->m_moneydecimals;}
QString MLocalFormat::amText()const{return d->m_am;}
QString MLocalFormat::pmText()const{return d->m_pm;}
QString MLocalFormat::moneyNegativeSign()const{return d->m_moneyneg;}
QString MLocalFormat::moneyPositiveSign()const{return d->m_moneypos;}
QString MLocalFormat::dateFormat()const{return d->m_dateformat;}
QString MLocalFormat::timeFormat()const{return d->m_timeformat;}
QString MLocalFormat::dateTimeFormat()const{return d->m_datetimeformat;}
QString MLocalFormat::timeZone()const{return d->m_timezone;}

//static
void MLocalFormat::setDefaultFormat(const MLocalFormat& f)
{
	defaultformat=f;
	TimeStamp::setDefaultZone(f.d->m_timezone);
}


MLocalFormat::MLocalFormat()
{
	operator=(defaultformat);
}


void MLocalFormat::setWeekDays(const QStringList&w)
{
	d.decouple();
	if(w.size()==0){
		d->m_day=QStringList()
			<<QCoreApplication::translate("MLocalFormat","Sunday")
			<<QCoreApplication::translate("MLocalFormat","Monday")
			<<QCoreApplication::translate("MLocalFormat","Tuesday")
			<<QCoreApplication::translate("MLocalFormat","Wednesday")
			<<QCoreApplication::translate("MLocalFormat","Thursday")
			<<QCoreApplication::translate("MLocalFormat","Friday")
			<<QCoreApplication::translate("MLocalFormat","Saturday");
	}else
	if(w.size()==7)
		d->m_day=w;
}
void MLocalFormat::setShortWeekDays(const QStringList&w)
{
	d.decouple();
	if(w.size()==0){
		d->m_sday=QStringList()
			<<QCoreApplication::translate("MLocalFormat","Sun","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Mon","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Tue","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Wed","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Thu","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Fri","short weekday")
			<<QCoreApplication::translate("MLocalFormat","Sat","short weekday");
	}else
	if(w.size()==7)
		d->m_sday=w;
}
void MLocalFormat::setMonths(const QStringList&m)
{
	d.decouple();
	if(m.size()==0){
		d->m_month=QStringList()
			<<QCoreApplication::translate("MLocalFormat","January")
			<<QCoreApplication::translate("MLocalFormat","February")
			<<QCoreApplication::translate("MLocalFormat","March")
			<<QCoreApplication::translate("MLocalFormat","April")
			<<QCoreApplication::translate("MLocalFormat","May")
			<<QCoreApplication::translate("MLocalFormat","June")
			<<QCoreApplication::translate("MLocalFormat","July")
			<<QCoreApplication::translate("MLocalFormat","August")
			<<QCoreApplication::translate("MLocalFormat","September")
			<<QCoreApplication::translate("MLocalFormat","October")
			<<QCoreApplication::translate("MLocalFormat","November")
			<<QCoreApplication::translate("MLocalFormat","December");
	}else
	if(m.size()==12)
		d->m_month=m;
}
void MLocalFormat::setShortMonths(const QStringList&m)
{
	d.decouple();
	if(m.size()==0){
		d->m_smonth=QStringList()
			<<QCoreApplication::translate("MLocalFormat","Jan","short month name")
			<<QCoreApplication::translate("MLocalFormat","Feb","short month name")
			<<QCoreApplication::translate("MLocalFormat","Mar","short month name")
			<<QCoreApplication::translate("MLocalFormat","Apr","short month name")
			<<QCoreApplication::translate("MLocalFormat","May","short month name")
			<<QCoreApplication::translate("MLocalFormat","Jun","short month name")
			<<QCoreApplication::translate("MLocalFormat","Jul","short month name")
			<<QCoreApplication::translate("MLocalFormat","Aug","short month name")
			<<QCoreApplication::translate("MLocalFormat","Sep","short month name")
			<<QCoreApplication::translate("MLocalFormat","Oct","short month name")
			<<QCoreApplication::translate("MLocalFormat","Nov","short month name")
			<<QCoreApplication::translate("MLocalFormat","Dec","short month name");
	}else
	if(m.size()==12)
		d->m_smonth=m;
}

void MLocalFormat::setDateTimeFormat(QString dateformat, QString timeformat, QString datetimeformat)
{
	d.decouple();
	if(dateformat=="")d->m_dateformat=QCoreApplication::translate("MLocalFormat","%Y-%M-%D","date format");
	else d->m_dateformat=dateformat;
	if(timeformat=="")d->m_timeformat=QCoreApplication::translate("MLocalFormat","%h:%I","time format");
	else d->m_timeformat=timeformat;
	if(datetimeformat=="")d->m_datetimeformat=QCoreApplication::translate("MLocalFormat","%Y-%M-%D %h:%I","date and time format");
	else d->m_datetimeformat=datetimeformat;
}


void MLocalFormat::setMoneyFormat(QString c,int  n,bool s)
{
	d.decouple();
	d->m_currency=c;
	d->m_moneydecimals=n;
	d->m_moneysymbehind=s;
}

void MLocalFormat::setMoneySign(QString n,QString p, MOServerFormat::MoneyPos np, MOServerFormat::MoneyPos pp)
{
	d.decouple();
	d->m_moneyneg=n;d->m_moneypos=p;
	d->m_moneynegpos=np;d->m_moneypospos=pp;
}

void MLocalFormat::setAP(QString am,QString pm)
{
	d.decouple();

	if(am=="--")d->m_am=QCoreApplication::translate("MLocalFormat","am","AM/PM time component");
	else d->m_am=am;
	if(pm=="--")d->m_pm=QCoreApplication::translate("MLocalFormat","pm","AM/PM time component");
	else d->m_pm=pm;
}

void MLocalFormat::setNumberFormat(QChar dec,QChar thou,int dig)
{
	d.decouple();

	if(dec.isNull())d->m_decimal=QCoreApplication::translate("MLocalFormat",".","decimal dot")[0];
	else d->m_decimal=dec;

	if(thou.isNull())d->m_thousand=QCoreApplication::translate("MLocalFormat",",","thousand division character")[0];
	else d->m_thousand=thou;

	if(dig<0)d->m_thousanddigits=QCoreApplication::translate("MLocalFormat","0","digits between thousand division chars, <=0 means none").toInt();
	else d->m_thousanddigits=dig;
}

void MLocalFormat::setNonLocalized()
{
	d.decouple();
	//reset number format
	d->m_decimal='.';d->m_thousanddigits=0;
	//reset money signs
	setMoneyFormat();setMoneySign();
}


QString MLocalFormat::formatDate(const QDate&date,QString format)const
{
	return formatDate(TimeStamp(date,d->m_timezone),format);
}
QString MLocalFormat::formatDate(const TimeStamp& date, QString format) const
{
	if(format=="")format=d->m_dateformat;
	return formatDateTime(date,format);
}
QString MLocalFormat::formatDate(qint64 date,QString format)const
{
	return formatDate(TimeStamp(date,d->m_timezone),format);
}
QString MLocalFormat::formatTime(const TimeStamp& time, QString format) const
{
	if(format=="")format=d->m_timeformat;
	return formatDateTime(time,format);
}
QString MLocalFormat::formatTime(const QTime&time,QString format)const
{
	return formatTime(TimeStamp(time,d->m_timezone),format);
}
QString MLocalFormat::formatTime(qint64 time,QString format)const
{
	return formatTime(TimeStamp(time,d->m_timezone),format);
}
QString MLocalFormat::formatDateTime(const QDateTime&time,QString format)const
{
	return formatDateTime(TimeStamp(time,d->m_timezone),format);
}
QString MLocalFormat::formatDateTime(const TimeStamp& ts, QString format) const
{
	if(format=="")format=d->m_datetimeformat;
	//parse
	TimeStamp time=ts.toZone(d->m_timezone);
	QString out;
	bool inp=false;
	for(int i=0;i<format.size();i++){
		if(inp){
			switch(format[i].unicode()){
				//date format codes
				case 'Y':out+=QString::number(time.year());break;
				case 'y':{
					QString y=QString::number(time.year()%100);
					if(y.size()<2)y="0"+y;
					out+=y;
					break;
				}
				case 'm':out+=QString::number(time.month());break;
				case 'M':{
					QString m=QString::number(time.month());
					if(m.size()<2)m="0"+m;
					out+=m;
					break;
				}
				case 'n':out+=d->m_smonth.value(time.month()-1);break;
				case 'N':out+=d->m_month.value(time.month()-1);break;
				case 'd':out+=QString::number(time.day());break;
				case 'D':{
					QString m=QString::number(time.day());
					if(m.size()<2)m="0"+m;
					out+=m;
					break;
				}
				case 'w':out+=d->m_sday.value(time.weekDay());break;
				case 'W':out+=d->m_day.value(time.weekDay());break;
				//time formats
				case 'h':out+=QString::number(time.hour());break;
				case 'H':{
					QString t=QString::number(time.hour());
					if(t.size()<2)t="0"+t;
					out+=t;
					break;
				}
				case 'i':out+=QString::number(time.minute());break;
				case 'I':{
					QString t=QString::number(time.minute());
					if(t.size()<2)t="0"+t;
					out+=t;
					break;
				}
				case 's':out+=QString::number(time.second());break;
				case 'S':{
					QString t=QString::number(time.second());
					if(t.size()<2)t="0"+t;
					out+=t;
					break;
				}
				case 'z':out+=QString::number(time.msecs());break;
				case 'Z':{
					QString t=QString::number(time.msecs());
					while(t.size()<3)t="0"+t;
					out+=t;
					break;
				}
				case 'a':{
					int t=time.hour()%12;if(t==0)t=12;
					out+=QString::number(t);
					break;
				}
				case 'A':{
					int th=time.hour()%12;if(th==0)th=12;
					QString t=QString::number(th);
					if(t.size()<2)t="0"+t;
					out+=t;
					break;
				}
				case 'p':{
					int t=time.hour();
					if(t>=1 && t<=12)out+=d->m_am.toLower();
					else out+=d->m_pm.toLower();
					break;
				}
				case 'P':{
					int t=time.hour();
					if(t>=1 && t<=12)out+=d->m_am;
					else out+=d->m_pm;
					break;
				}
				case 't':
				case 'T':{
					//get diff from UTC
					int d=time.offsetFromUTC()/60;
					//west(-) or east(+)?
					QString t;
					if(d<0){t="-";d*=-1;}else t="+";
					//append
					out+=QString("%1%2:%3")
                                                .arg(t)
                                                .arg(int(d/60),2,10,QChar('0'))
                                                .arg(int(d%60),2,10,QChar('0'));
					break;
				}
				case 'o':out+=time.zoneAbbreviation();break;
				case 'O':out+=d->m_timezone;break;
				// % sign
				case '%':out+="%";break;
				//mistakes
				default:out+=QString("%")+format[i];break;
			}
			inp=false;
		}else{
			if(format[i]=='%')inp=true;
			else out+=format[i];
		}
	}
	//catch mistaken % at end
	if(inp)out+="%";
	//return result
	return out;
}

QString MLocalFormat::formatDateTime(qint64 time,QString format)const
{
	return formatDateTime(TimeStamp(time,d->m_timezone),format);
}

QString MLocalFormat::formatNumber(qint64 n)const
{
	//shortcut if we use standard format anyway
	if(d->m_thousanddigits<=0)return QString::number(n);
	//convert number to decimals
	bool neg=false;
	if(n<0){n*=-1;neg=true;}
	QString s=QString::number(n);
	//insert dividers
	QString r;
	int l=s.size();
	for(int i=0;i<s.size();i++){
		if(i>0 && ((l-i)%d->m_thousanddigits)==0)r+=d->m_thousand;
		r+=s[i];
	}
	if(neg)r="-"+r;
	return r;
}

QString MLocalFormat::formatNumber(double num,uint decimals)const
{
	//check negative
	bool neg=false;
	if(num<0.){num*=-1.;neg=true;}
	//before the dot
	qint64 be=floorl(num);
	QString r=formatNumber(be);
	//fractions
	if(decimals>0){
		r+=d->m_decimal;
		num-=be;
		for(int i=0;i<(int)decimals;i++){
			num*=10.;
			int d=floorl(num);
			r+=QString::number(d);
			num-=d;
		}
	}
	//add negative sign
	if(neg)r="-"+r;
	//result
	return r;
}

QString MLocalFormat::formatMoney(qint64 num,bool usethousand)const
{
	return formatMoney(num,(usethousand?UseThousand:0)|ShowCurrencySymbol);
}


QString MLocalFormat::formatMoney(qint64 num, MLocalFormat::MoneyFlags flags) const
{
	//check value
	bool neg=false;
	if(num<0){num*=-1;neg=true;}
	//split main and fraction
	qint64 mod=1;for(int i=0;i<d->m_moneydecimals;i++)mod*=10;
	qint64 frac=num%mod;
	num/=mod;
	//create main unit
	QString ms;
	if(flags&UseThousand)ms=formatNumber(num);
	else ms=QString::number(num);
	//create fractional unit
	QString fs=QString::number(frac);
	for(int i=fs.size();i<d->m_moneydecimals;i++)fs="0"+fs;
	//combine and get currency symbol
	QString r=ms+d->m_decimal+fs;
	QString curr;
	if(flags&ShowCurrencySymbol)curr=d->m_currency;
	//add sign
	QString sign;
	if(neg)sign=d->m_moneyneg;
	else sign=d->m_moneypos;
	switch(neg?d->m_moneynegpos:d->m_moneypospos){
		case MOServerFormat::NoSign:break;
		case MOServerFormat::SignParen:r="("+r+")";break;
		case MOServerFormat::SignAfterSym:curr+=sign;break;
		case MOServerFormat::SignBeforeSym:curr=sign+curr;break;
		case MOServerFormat::SignAfterNum:r+=sign;break;
		case MOServerFormat::SignBeforeNum:
		default:
			r=sign+r;
			break;
	}
	//add currency
	if(!curr.isEmpty()){
		if(d->m_moneysymbehind)r+=" "+curr;
		else r=curr+" "+r;
	}
	//return result
	return r;
}

qint64 MLocalFormat::scanInt(QString num)const
{
	num=num.trimmed();
	qint64 r=0;
	bool neg=false;
	for(int i=0;i<num.size();i++){
		QChar c=num[i];
		//negative sign
		if(c=='-')neg=true;else
		//digits
		if(c.isDigit()){
			r*=10;
			r+=c.digitValue();
		}else
		//ignore thousand divider
		if(c==d->m_thousand)continue;
		//non-numericals break
		else break;
	}
	if(neg)r*=-1;
	return r;
}
qint64 MLocalFormat::scanMoney(QString num)const
{
	//init
	num=num.trimmed();
	qint64 r=0,dd=0,nd=0;
	bool neg=false,frc=false;
	//scan string; r=main unit; d=fractions
	for(int i=0;i<num.size();i++){
		QChar c=num[i];
		//negative sign
		if(d->m_moneyneg.contains(c) || c=='-')neg=true;else
		//decimal dot
		if(c==d->m_decimal)frc=true;else
		//digits
		if(c.isDigit()){
			if(frc){
				if(nd>=d->m_moneydecimals)break;
				dd*=10;dd+=c.digitValue();
				nd++;
			}else{
				r*=10;r+=c.digitValue();
			}
		}else
		//ignore thousand divider
		if(c==d->m_thousand)continue;
		//non-numericals break
		else break;
	}
	//move main to left
	for(int i=0;i<d->m_moneydecimals;i++)r*=10;
	//add fractions
	for(;nd<d->m_moneydecimals;nd++)dd*=10;
	r+=dd;
	//negative?
	if(neg)r*=-1;
	//result
	return r;
}

double MLocalFormat::scanFloat(QString num)const
{
	//init
	num=num.trimmed();
	double r=0.;
	double ff=1.;
	bool neg=false,frc=false;
	//scan
	for(int i=0;i<num.size();i++){
		QChar c=num[i];
		//negative sign
		if(c=='-')neg=true;else
		//decimal dot
		if(c==d->m_decimal)frc=true;else
		//digits
		if(c.isDigit()){
			if(frc){
				ff/=10.;
				r+=c.digitValue()*ff;
			}else{
				r*=10.;
				r+=c.digitValue();
			}
		}else
		//ignore thousand divider
		if(c==d->m_thousand)continue;
		//non-numericals break
		else break;
	}
	//return
	if(neg)r*=-1.;
	return r;
}

QRegularExpression MLocalFormat::moneyRegExp(bool allownegative,bool allowcurrency)const
{
	//main unit
	QString r="[\\d"+QRegularExpression::escape(d->m_thousand)+"]+";
	//fractions
	if(d->m_moneydecimals>0){
		r+=QRegularExpression::escape(d->m_decimal);
		r+="\\d{"+QString::number(d->m_moneydecimals)+"}";
	}
	//negative
	if(allownegative){
		//front
		QString f="[-";
		if(d->m_moneyneg.size()>0)
			if(d->m_moneyneg[0]!='\n' && d->m_moneyneg[0]!='-')
				f+=QRegularExpression::escape(d->m_moneyneg[0]);
		f+="]?";
		r=f+r;
		//back
		if(d->m_moneyneg.size()>1)
			r+=QRegularExpression::escape(d->m_moneyneg[1])+"?";
	}
	//currency
	if(allowcurrency && d->m_currency!="")
		r+="("+QRegularExpression::escape(d->m_currency)+")?";
	//return
	return QRegularExpression(r);
}

void MLocalFormat::setTimeZone(QString olsonname)
{
	d.decouple();
	if(TimeStamp::loadZone(olsonname))
		d->m_timezone=olsonname;
}

QString currentDir()
{
	return QSettings().value("lastUsedDirectory",".").toString();
}

void setCurrentDir(QString fn)
{
	if(fn.isEmpty())return;
	QFileInfo fi(fn);
	//it is already a directory
	if(fi.isDir())QSettings().setValue("lastUsedDirectory",fi.absoluteFilePath());
	//it is probably a file
	else {
		//get parent directory name
		fn=fi.dir().absolutePath();
		if(QFileInfo(fn).isDir())
			QSettings().setValue("lastUsedDirectory",fn);
		//abort if not found by now, this was bogus
	}
}
