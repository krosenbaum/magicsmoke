//
// C++ Implementation: Message Box Wrapper
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "boxwrapper.h"
#include <QDebug>

MBoxWrapper::Wrapper MBoxWrapper::mwarn=nullptr;

void MBoxWrapper::warning(QString title, QString text)
{
        if(mwarn!=nullptr)
                mwarn(title,text);
        else
                qDebug()<<"Hidden Box Warning:"<<title<<text;
}

void MBoxWrapper::setWarning(Wrapper w)
{
        mwarn=w;
}

static MProgressWrapper::Factory prgfac=nullptr;
class MProgressWrapperDebug:public MProgressWrapper
{
        QString label;
        int min=0,max=0,val=0;
        void output()
        {
                const double cur=val-min;
                const double span=max-min;
                qDebug()<<"Progress of"<<label<<"at"<<val<<"of"<<max<<"("<<(cur*100./span)<<"%)";
        }
        public:
                MProgressWrapperDebug(QString l):label(l){}
                virtual void setLabelText(QString l){label=l;}
                virtual void setCancelButtonText(QString){}
                virtual void cancel(){}
                virtual void hide(){}
                virtual void setRange(int mi,int ma){
                        min=mi;max=ma;
                        if(min>max)min=max;
                        setValue(val);
                }
                virtual void setValue(int v)
                {
                        int old=val;
                        val=v;
                        if(val<min)val=min;
                        if(val>max)val=max;
                        if(val!=old)output();
                }
};


MProgressWrapper* MProgressWrapper::create(QString label, QString buttonlabel)
{
        if(prgfac==nullptr)return new MProgressWrapperDebug(label);
        else return prgfac(label,buttonlabel);
}

void MProgressWrapper::setFactory(Factory f)
{
        prgfac=f;
}
