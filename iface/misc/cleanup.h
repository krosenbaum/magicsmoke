//
// C++ Interface: misc
//
// Description: miscellaneous helper functions
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2012
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_LAMBDACLEAN_H
#define MAGICSMOKE_LAMBDACLEAN_H

#include <functional>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

///clean-up function: executes a lambda expression when it is destructed (i.e. when the instance leaves its scope
class MSIFACE_EXPORT MCleanup
{
	std::function<void()>m_ptr;
	public:
		///instantiates a new cleanup object and remembers the given function for later execution
		MCleanup(const std::function<void()>&f):m_ptr(f){}
		
		///sets a new cleanup function, removing the old one
		void setFunction(const std::function<void()>&f){m_ptr=f;}
		
		///destructs the object and executes the cleanup function
		~MCleanup(){if(m_ptr)m_ptr();}
};

#endif
