//
// C++ Interface: Message Box Wrapper
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MSINTERFACE_BOXWRAPPER_H
#define MSINTERFACE_BOXWRAPPER_H

#include <QString>
#include <QObject>
#include <functional>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif


class MSIFACE_EXPORT MBoxWrapper
{
        public:
                typedef std::function<void(QString,QString)>Wrapper;
                
                static void warning(QString title,QString text);
                static void setWarning(Wrapper);
        private:
                static Wrapper mwarn;
                MBoxWrapper()=default;
};

class MSIFACE_EXPORT MProgressWrapper:public QObject
{
        Q_OBJECT
        public:
                typedef std::function<MProgressWrapper*(QString,QString)>Factory;
                static void setFactory(Factory);
                
                static MProgressWrapper* create(QString label,QString buttonlabel=QString());
        public slots:
                virtual void setLabelText(QString)=0;
                virtual void setCancelButtonText(QString)=0;
                virtual void cancel()=0;
                virtual void setRange(int,int)=0;
                virtual void setValue(int)=0;
		virtual void hide()=0;
};

#endif
