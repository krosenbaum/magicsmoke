HEADERS += \
	$$PWD/misc.h \
	$$PWD/boxwrapper.h \
	$$PWD/formula.h

SOURCES += \
	$$PWD/misc.cpp \
	$$PWD/boxwrapper.cpp \
	$$PWD/formula.cpp

INCLUDEPATH += $$PWD
