//
// C++ Interface: magicsmoke ELAM adaptation
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_ELAM_H
#define MAGICSMOKE_ELAM_H

#include <ELAM/Engine>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

class MSIFACE_EXPORT MElamEngine:public ELAM::Engine
{
	public:
		MElamEngine(QObject*parent=0);
};

#endif
