//
// C++ Implementation: magicsmoke ELAM adaptation
//
// Description: 
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2010-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "formula.h"
#include "misc.h"

#include <QDebug>
#include <Nullable>

#include <ELAM/IntEngine>
#include <ELAM/BoolEngine>
#include <ELAM/FloatEngine>
#include <ELAM/StringEngine>

using namespace ELAM;

inline static QVariant toMoneyFunc(const QList<QVariant>&lf,bool local)
{
	if(lf.size()!=1)
		return Exception(Exception::ArgumentListError, "expecting exactly one argument");
	if(!lf[0].canConvert<qlonglong>())
		return Exception(Exception::TypeMismatchError,"cannot convert to int");
	return cent2str(lf[0].toLongLong(),local);
}
static QVariant toMoneyFuncLoc(const QList<QVariant>&lf,Engine&){return toMoneyFunc(lf,true);}
static QVariant toMoneyFuncNLoc(const QList<QVariant>&lf,Engine&){return toMoneyFunc(lf,false);}

inline static QVariant fromMoneyFunc(const QList<QVariant>&lf,bool local)
{
	if(lf.size()!=1)
		return Exception(Exception::ArgumentListError, "expecting exactly one argument");
	if(!lf[0].canConvert<QString>())
		return Exception(Exception::TypeMismatchError,"cannot convert to string");
	return str2cent(lf[0].toString(),local);
}
static QVariant fromMoneyFuncLoc(const QList<QVariant>&lf,Engine&){return fromMoneyFunc(lf,true);}
static QVariant fromMoneyFuncNLoc(const QList<QVariant>&lf,Engine&){return fromMoneyFunc(lf,false);}

inline static QVariant unix2DTFunc(const QList<QVariant>&args,int mode)
{
	if(args.size()<1 || args.size()>2)
		return ELAM::Exception(ELAM::Exception::ArgumentListError,"expecting 1 or 2 arguments");
	if(!args[0].canConvert<qlonglong>())
		return ELAM::Exception(ELAM::Exception::TypeMismatchError,"argument 1 must be int");
	qint64 tm=args[0].toLongLong();
	bool loc=true;
	if(args.size()>1){
		if(!args[1].canConvert<bool>())
			return ELAM::Exception(ELAM::Exception::TypeMismatchError,"argument 2 must be boolean");
		loc=args[1].toBool();
	}
	switch(mode){
		case 0:return unix2date(tm,loc);
		case 1:return unix2time(tm,loc);
		default:return unix2dateTime(tm,loc);
	}
}
static QVariant unix2DateFunc(const QList<QVariant>&args,Engine&){return unix2DTFunc(args,0);}
static QVariant unix2TimeFunc(const QList<QVariant>&args,Engine&){return unix2DTFunc(args,1);}
static QVariant unix2DateTimeFunc(const QList<QVariant>&args,Engine&){return unix2DTFunc(args,2);}

static QVariant nullintCast(const QVariant&orig,const Engine&)
{
	if(orig.userType()==qMetaTypeId<Nullable<qint32> >()){
		Nullable<qint32> ni=orig.value<Nullable<qint32> >();
		if(ni.isNull())return QVariant();
		else return (qlonglong)ni.value();
	}
	if(orig.userType()==qMetaTypeId<Nullable<quint32> >()){
		Nullable<quint32> ni=orig.value<Nullable<quint32> >();
		if(ni.isNull())return QVariant();
		else return (qlonglong)ni.value();
	}
	if(orig.userType()==qMetaTypeId<Nullable<qint64> >()){
		Nullable<qint64> ni=orig.value<Nullable<qint64> >();
		if(ni.isNull())return QVariant();
		else return (qlonglong)ni.value();
	}
	if(orig.userType()==qMetaTypeId<Nullable<quint64> >()){
		Nullable<quint64> ni=orig.value<Nullable<quint64> >();
		if(ni.isNull())return QVariant();
		else return (qlonglong)ni.value();
	}
	return orig;
}

static QVariant nullstrCast(const QVariant&orig,const Engine&)
{
	if(orig.userType()==qMetaTypeId<Nullable<QString> >()){
		Nullable<QString> ni=orig.value<Nullable<QString> >();
		if(ni.isNull())return QVariant();
		else return ni.value();
	}
	return orig;
}

static QVariant nullboolCast(const QVariant&orig,const Engine&)
{
	if(orig.userType()==qMetaTypeId<Nullable<bool> >()){
		Nullable<bool> ni=orig.value<Nullable<bool> >();
		if(ni.isNull())return QVariant();
		else return ni.value();
	}
	return orig;
}

MElamEngine::MElamEngine(QObject* parent): Engine(parent)
{
	//configure character classes
	QString alpha="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
	characterClasses().setNameClass(alpha,alpha+"0123456789.:");
	QString oc=characterClasses().operatorClass();
	characterClasses().setOperatorClass(oc.remove('.').remove(':'));
        characterClasses().setAssignmentChars('\0','=');
	if(!characterClasses().isConsistent())
		qDebug()<<"Warning: built-in calculator is inconsistent.";
	//load default engines
	IntEngine::configureIntEngine(*this);
	FloatEngine::configureFloatEngine(*this);
	BoolEngine::configureBoolEngine(*this);
	BoolEngine::configureLogicEngine(*this);
	StringEngine::configureStringEngine(*this);
        configureReflection(*this);
	//add money class
	setFunction("toMoney",toMoneyFuncLoc);
	setFunction("toMoneyLocal",toMoneyFuncLoc);
	setFunction("toMoneyNL",toMoneyFuncNLoc);
	setFunction("fromMoney",fromMoneyFuncLoc);
	setFunction("fromMoneyLocal",fromMoneyFuncLoc);
	setFunction("fromMoneyNL",fromMoneyFuncNLoc);
	//add datetime conversion
	setFunction("unix2date",unix2DateFunc);
	setFunction("unix2time",unix2TimeFunc);
	setFunction("unix2datetime",unix2DateTimeFunc);
	setFunction("unix2dateTime",unix2DateTimeFunc);
        //legacy functions for ODF renderer
        setFunction("cent2str",[](const QList<QVariant>&args,Engine&)->QVariant{
                if(args.size()!=1)
                        return Exception(Exception::ArgumentListError,"expected one argument to cent2str");
                else
                        return cent2str(args[0].toInt());
        });
        setFunction("str2cent",[](const QList<QVariant>&args,Engine&)->QVariant{
                if(args.size()!=1)
                        return Exception(Exception::ArgumentListError,"expected one argument to str2cent");
                else
                        return str2cent(args[0].toString());
        });
	//register casts for nullable
	setAutoCast(qMetaTypeId<qlonglong>(),
		QList<int>()
		<<qMetaTypeId<Nullable<qint32> >()
		<<qMetaTypeId<Nullable<quint32> >()
		<<qMetaTypeId<Nullable<qint64> >()
		<<qMetaTypeId<Nullable<quint64> >(), 
		nullintCast
	);
	setAutoCast(qMetaTypeId<QString>(),QList<int>()<<qMetaTypeId<Nullable<QString> >(), nullstrCast);
	setAutoCast(qMetaTypeId<bool>(),QList<int>()<<qMetaTypeId<Nullable<bool> >(), nullboolCast);
}
