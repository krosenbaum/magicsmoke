//
// C++ Interface: misc
//
// Description: miscellaneous helper functions
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2008-2012
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#ifndef MAGICSMOKE_MISC_H
#define MAGICSMOKE_MISC_H

#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QRegularExpression>

class TimeStamp;
#include <MOServerFormat>
#include <DPtrBase>

#ifndef MSIFACE_EXPORT
#define MSIFACE_EXPORT Q_DECL_IMPORT
#endif

/**converts special HTML characters into harmless &-codes, so the text can be included*/
QString MSIFACE_EXPORT htmlize(QString str);

/**converts special XML characters into harmless &-codes, so the text can be included*/
QString MSIFACE_EXPORT xmlize(QString str,QString newline="\n");
/**converts special XML characters into harmless &-codes, so the text can be included*/
QByteArray MSIFACE_EXPORT xmlize(QByteArray str,QString newline="\n");

/**converts a cent value into a (localized) string*/
QString MSIFACE_EXPORT cent2str(qint64 cent,bool localize=true);

/**converts a (localized) string back into a cent value (must not contain spaces or extra dots)*/
qint64 MSIFACE_EXPORT str2cent(QString s,bool fromlocal=true);

/**converts a unix timestamp into a date*/
QString MSIFACE_EXPORT unix2date(qint64,bool localize=true);

/**converts a unix timestamp into a time (ommitting the date)*/
QString MSIFACE_EXPORT unix2time(qint64,bool localize=true);

/**converts a unix timestamp into a date-time-string*/
QString MSIFACE_EXPORT unix2dateTime(qint64,bool localize=true);

/**return a (localized) regular expression that validates prices*/
QRegularExpression MSIFACE_EXPORT priceRegExp(bool localize=true);

///returns the current directory (the one last used in a file dialog)
QString MSIFACE_EXPORT currentDir();

///sets a new current directory (transparently handles files and directories)
void MSIFACE_EXPORT setCurrentDir(QString);

/**localized formatter class for timestamps, numbers and money;
per default it uses the local translation to format data, but can be overidden;

date values can be formatted with the following variables:
<table>
<tr><td>%%y<td>two-digit year (eg. 08)</tr>
<tr><td>%%Y<td>four-digit year (eg. 1908)</tr>
<tr><td>%%m<td>month as simple number (eg. 7)</tr>
<tr><td>%%M<td>month as two digit number (eg. 07)</tr>
<tr><td>%%n<td>short name of the month (eg. Jul)</tr>
<tr><td>%%N<td>long name of the month (eg. July)</tr>
<tr><td>%%d<td>day of the month as simple number (eg. 5)</tr>
<tr><td>%%D<td>day of the month as two digit number (eg. 05)</tr>
<tr><td>%%w<td>day of the week as short name (eg. Wed)</tr>
<tr><td>%%W<td>day of the week as long name (eg. Wednesday)</tr>
</table>

time values can be formatted with the following variables:
<table>
<tr><td>%%h<td>hour as simple number (eg. 7) in 24-hour format</tr>
<tr><td>%%H<td>hour as two digit number (eg. 07) in 24-hour format</tr>
<tr><td>%%a<td>hour as simple number (eg. 7) in 12-hour format</tr>
<tr><td>%%A<td>hour as two digit number (eg. 07) in 12-hour format</tr>
<tr><td>%%i<td>minute as simple number (eg. 7)</tr>
<tr><td>%%I<td>minute as two digit number (eg. 07)</tr>
<tr><td>%%s<td>second as simple number (eg. 7)</tr>
<tr><td>%%S<td>second as two digit number (eg. 07)</tr>
<tr><td>%%z<td>milli-seconds as simple number (eg. 7)</tr>
<tr><td>%%Z<td>milli-seconds as three digit number (eg. 007)</tr>
<tr><td>%%p<td>"am" or "pm" according to current time, this is the one set with setAP</tr>
<tr><td>%%P<td>"AM" or "PM" according to current time, this is toUpper executed on the one set with setAP</tr>
<tr><td>%%T<td>ISO timezone as +/-hhmm (eg. -0100 or +0100)</tr>
<tr><td>%%t<td>ISO timezone as +/-hh:mm (eg. -01:00 or +01:00)</tr>
<tr><td>%%o<td>timezone as abbreviation - the abbreviation comes from the background time zone DB and is not globally unique (eg. "CET")</tr>
<tr><td>%%O<td>timezone as Olson name (eg. "Europe/Berlin")</tr>
</table>

Any occurrence of "%%" will be translated to a literal "%" sign.

For example "%w the %d'th of %N in the year of the lord %Y at %a:%I %P" will be formatted as "Sat 19'th of June in the year of the lord 2010 at 8:29 AM".
*/
class MSIFACE_EXPORT MLocalFormat
{
	DECLARE_SHARED_DPTR(d);
	public:
		/**constructs a formatter object that corresponds to the default*/
		MLocalFormat();
		/**copies a formatter object inheriting its overrides*/
		MLocalFormat(const MLocalFormat&)=default;
		/**initializes the formatter object from a server format object*/
		MLocalFormat(const MOServerFormat&);
		/**deletes the formatter object*/
		virtual ~MLocalFormat();

		/**sets a new default format*/
		static void setDefaultFormat(const MLocalFormat&);

		/**copies a formatter object*/
		virtual MLocalFormat& operator=(const MLocalFormat&)=default;

		/**overrides the full names of week days, an empty list resets to the local translation, otherwise the list must be exactly seven entries long and starts with Sunday*/
		virtual void setWeekDays(const QStringList&l=QStringList());
		/**overrides the short week day names, an empty list resets to the local translation, otherwise the list must be exactly seven entries long and starts with Sunday (Sun)*/
		virtual void setShortWeekDays(const QStringList&l=QStringList());

		/**overrides the full names of months, an empty list resets to the local translation, otherwise the list must be exactly seven entries long and starts with January*/
		virtual void setMonths(const QStringList&l=QStringList());
		/**overrides the short names of months, an empty list resets to the local translation, otherwise the list must be exactly seven entries long and starts with January (Jan)*/
		virtual void setShortMonths(const QStringList&l=QStringList());

		/**overrides the formatting of money - the settings of numbers are re-used
		\param digitsCents defines how many digits are used for sub-amounts (cents), if zero no cents are used, normal are the values 0, 2 and 3; the default is 2
		\param currency defines the symbol of the currency used, the default is an empty string
		\param symbolBehind defines whether the currency symbol is shown before (false) or behind (true) the numeric value
		*/
		virtual void setMoneyFormat(QString currency=QString(),int digitsCents=2, bool symbolBehind=true);
		/**overrides the formatting of money - the settings of numbers are re-used
		\param negative defines the sign for negative values, the default is "-"
		\param positive defines the sign for positive values, the default is an empty string
		\param negativePos defines where the negative sign is printed
		\param positivePos defines where the positive sign is printed
		*/
		virtual void setMoneySign(QString negative="-",QString positive=QString(), MOServerFormat::MoneyPos negativePos=MOServerFormat::SignBeforeNum, MOServerFormat::MoneyPos positivePos=MOServerFormat::NoSign);

		/**overrides the formatting of numbers and money;
		\param decimal defines the character used to separate decimals from the main body, if QChar() is used it returns to the translation default (a dot "." in default localization)
		\param thousandDiv defines the character used to separate blocks of digits, if QChar() is used it returns to the translation default (comma "," in default)
		\param digitsDiv defines the amount of digits in a block, eg. the value 3 will format 1000 as 1,000 (none per default)*/
		virtual void setNumberFormat(QChar decimal=QChar(),QChar thousandDiv=QChar(),int digitsDiv=-1);

		/**overrides the formatting of the AM/PM part of time display, resets to the localization if none given*/
		virtual void setAP(QString am="--",QString pm="--");

		/**overrides the local time zone
		\param olsonname the name of the local timezone in Olson notation, eg. "Europe/Berlin"*/
		virtual void setTimeZone(QString olsonname);

		/**overrides the default formatting of date and time, if empty the translation default is used*/
		virtual void setDateTimeFormat(QString dateformat=QString(),QString timeformat=QString(),QString datetimeformat=QString());

		/**returns the currently set time zone of this formatter*/
		virtual QString timeZone()const;

		/**overrides the formatting to be non-localized, numbers use decimal dot and no thousand separator, no currency symbol and "-" as negative sign; otherwise things stay the same; this is a helper for the shortcut methods like cent2str*/
		virtual void setNonLocalized();

		/**formats a date according to the given format, if none is given, the translation default is used
		\param date a QDate used as input, it is assumed to be local
		\param format a format string */
		virtual QString formatDate(const QDate&date,QString format=QString())const;
		/**formats a date according to the given format, if none is given, the translation default is used
		\param date a unix timestamp, it is converted to the local time of this local format object
		\param format a format string */
		virtual QString formatDate(qint64 date,QString format=QString())const;
		/**formats a date according to the given format, if none is given, the translation default is used
		\param date a timestamp, it is converted to the local time of this local format object
		\param format a format string */
		virtual QString formatDate(const TimeStamp& date,QString format=QString())const;

		/**formats a time according to the given format, if none is given, the translation default is used
		\param time a QTime used as input, it is assumed to be local
		\param format a format string */
		virtual QString formatTime(const QTime&time,QString format=QString())const;
		/**formats a time according to the given format, if none is given, the translation default is used
		\param time a unix timestamp, it is converted to local time
		\param format a format string */
		virtual QString formatTime(qint64 time,QString format=QString())const;
		/**formats a time according to the given format, if none is given, the translation default is used
		\param time a timestamp, it is converted to the local time of this local format object
		\param format a format string */
		virtual QString formatTime(const TimeStamp& time,QString format=QString())const;

		/**formats a date and time according to the given format, if none is given, the translation default is used
		\param time a QDateTime used as input, it is assumed to be local
		\param format a format string as specified by QDateTime::toString */
		virtual QString formatDateTime(const QDateTime&time,QString format=QString())const;
		/**formats a date and time according to the given format, if none is given, the translation default is used
		\param time a unix timestamp, it is converted to local time
		\param format a format string as specified by QDateTime::toString */
		virtual QString formatDateTime(qint64 time,QString format=QString())const;
		/**formats a date and time according to the given format, if none is given, the translation default is used
		\param time a TimeStamp used as input, it is converted to the local time of this local format object
		\param format a format string as specified by QDateTime::toString */
		virtual QString formatDateTime(const TimeStamp&time,QString format=QString())const;


		/**formats an integer number*/
		virtual QString formatNumber(qint64)const;
		/**formats an integer number*/
		virtual QString formatNumber(int n)const{return formatNumber(qint64(n));}
		/**formats an integer number*/
		virtual QString formatNumber(uint n)const{return formatNumber(qint64(n));}

		/**formats a floating point number, this version does not use exponential display, but adheres to the settings of local decimal dot and thousands block division
		\param num the number to be formatted
		\param decimals the maximum number of digits after the decimal dot*/
		virtual QString formatNumber(double num,uint decimals=4)const;

		enum MoneyFlag {
			PlainMoneyFormat=0,
			UseThousand=1,
			ShowCurrencySymbol=2,

			DefaultFormat=UseThousand|ShowCurrencySymbol
		};
		Q_DECLARE_FLAGS(MoneyFlags,MoneyFlag);

		/**formats a money value
		\param num the amount in cents
		\param flags determines how the string will be formatted */
		virtual QString formatMoney(qint64 num,MoneyFlags flags=DefaultFormat)const;
		/**formats a money value
		\param num the amount in cents
		\param usethousand if true the thousand block division is inserted */
		virtual QString formatMoney(qint64 num,bool usethousand)const;

		/**scans an integer number and returns the value, non-numerical chars are ignored*/
		virtual qint64 scanInt(QString)const;

		/**scans a money value and returns it in cents, non-numerical chars are ignored*/
		virtual qint64 scanMoney(QString)const;

		/**scans a floating point number and returns it, non-numerical chars are ignored*/
		virtual double scanFloat(QString)const;

		/**returns the names used for week days*/
		QStringList weekDayNames()const;
		/**returns the abbreviations used for week days*/
		QStringList shortWeekDayNames()const;

		/**returns the names used for months*/
		QStringList monthNames()const;
		/**returns the names used for months*/
		QStringList shortMonthNames()const;

		/**returns the currency symbol*/
		QString currency()const;

		/**returns the decimal dot symbol*/
		QChar decimalDot()const;

		/**returns the thousand separator if used, otherwise an empty string*/
		QString thousandSeparator()const;

		/**returns the amount of digits between thousand separators*/
		int thousandDigits()const;

		/**returns the amount of decimals in a money value*/
		int moneyDecimals()const;

		/**returns the text for AM in 12-hour clock format*/
		QString amText()const;
		/**returns the text for PM in 12-hour clock format*/
		QString pmText()const;

		/**returns the negative sign for money values*/
		QString moneyNegativeSign()const;
		/**returns the positive sign for money values*/
		QString moneyPositiveSign()const;

		/**returns a regular expression matching money values
		\param allownegative if given the resulting RegExp allows to use negative values
		\param allowcurrency if given the resulting RegExp allows to use the currency symbol */
		QRegularExpression moneyRegExp(bool allownegative=false,bool allowcurrency=false)const;

		/**returns the default format for dates*/
		QString dateFormat()const;

		/**returns the default format for times*/
		QString timeFormat()const;

		/**returns the default format for date/time*/
		QString dateTimeFormat()const;
	protected:
		/** \internal constructs the default object from the translation*/
		MLocalFormat(int);
		/** \internal default format*/
		static MLocalFormat defaultformat;
};
Q_DECLARE_OPERATORS_FOR_FLAGS(MLocalFormat::MoneyFlags);

#endif
