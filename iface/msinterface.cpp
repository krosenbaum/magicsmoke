//
// C++ Implementation: msinterface
//
// Description:
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2009-2011
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
//

#include "msinterface.h"
#include "sslexception.h"
#include "templates.h"
#include "misc.h"
#include "boxwrapper.h"

#include "MTGetMyRights"
#include "MTGetMyRoles"
#include "MTServerInfo"
#include "MTLogout"
#include "MTGetLanguage"

#include <WException>

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QNetworkReply>
#include <QSettings>
#include <QTimer>
#include <QTranslator>
#include <MTLogin>

static QString dataDir;

#include <stdlib.h>

#ifdef Q_OS_WIN32
#define BASEDIRVAR "APPDATA"
#else
#define BASEDIRVAR "HOME"
#endif

QString MSInterface::resolveDir(const QString &dirspec)
{
        //resolve pattern
        const QStringList dirspecl=dirspec.split('/',Qt::KeepEmptyParts);
        bool notfirst=false;
        QString rdir;
        for(QString dcom:dirspecl){
                if(dcom=="$BASE"){
                        dcom=getenv(BASEDIRVAR);
                        if(dcom.isEmpty())
                                qFatal("Cannot determine base application data directory. While resolving %s",dirspec.toLatin1().data());
                }else if(dcom=="$APP")
                        dcom=qApp->applicationDirPath();
                else if(dcom.startsWith("$"))
                        dcom=getenv(dcom.mid(1).toLatin1().data());
                //append component
                if(notfirst)rdir+="/";
                else notfirst=true;
                rdir+=dcom;
        }
        //make sure it exists
        if(!QDir::current().mkpath(rdir))
                qFatal("Unable to create path '%s' for spec '%s'.",rdir.toLatin1().data(),dirspec.toLatin1().data());
        //return it
        return rdir;
}

MSInterface::MSInterface(QString pid)
	:MInterface()
{
	profileid=pid;
	QSettings set;
	set.beginGroup("profiles/"+pid);
	setUrl("https://"+set.value("serverurl","my.host.com/path/machine.php").toString());
	bool useproxy=set.value("useproxy",false).toBool();
	if(useproxy)
		setProxy(
			set.value("proxyname","proxy").toString(),
			set.value("proxyport",74).toInt(),
			set.value("proxyuser").toString(),
			set.value("proxypass").toString()
		);
	m_host=set.value("hostname").toString();
	m_hostkey=set.value("hostkey").toString();
	setLogLevel((LogLevel)set.value("webloglevel",LogOnError).toInt());
	setWebTimeout(set.value("webtimeout",30).toInt()*1000);
	sslexcept=new MSslExceptions(dataDir()+"/sslexceptions.xml");
	temp=new MTemplateStore(pid);
}

MSInterface::~MSInterface()
{
	if(autologout)logout();
	if(sslexcept)delete sslexcept;
	sslexcept=0;
	if(temp)delete temp;
	temp=0;
}

bool MSInterface::login(QString username,QString passwd)
{
	m_uname=username;m_passwd=passwd;
	MTLogin lg=MTLogin::query(username,passwd,m_host,m_hostkey);
	if(lg.stage()==lg.Error){
		MBoxWrapper::warning(tr("Warning"),tr("Login failed: %1").arg(tr(lg.errorString().toLatin1())));
		return false;
	}
	if(lg.stage()!=lg.Success)return false;
	//schedule re-login before session times out
	int msecs=QDateTime::currentDateTime().msecsTo(QDateTime::fromSecsSinceEpoch(lg.getvalidtill()));
	msecs-=120000;//do it 2 min before session runs out
	if(msecs>100000)//but only if it does not become annoying...
		QTimer::singleShot(msecs,this,SIGNAL(needRelogin()));
	// initialize
	return loginSession(m_uname,lg.getsessionid());
}

bool MSInterface::loginSession (QString username, QString sessionid )
{
	m_uname=username;
	setSessionId(sessionid);
	//get rights and roles
	MTGetMyRights mrt=MTGetMyRights::asyncQuery();
	MTGetMyRoles mrl=MTGetMyRoles::asyncQuery();
	WTransaction::WaitForAll()<<mrt<<mrl;
	QStringList rsl=mrt.getright();
	for(int i=0;i<rsl.size();i++){
// 		qDebug("have right %s",rsl[i].toLatin1().data());
		Right x=stringToRight(rsl[i]);
		if(x!=NoRight)userrights<<x;
	}
	userflags=mrt.getflag();
// 	qDebug()<<"have flags"<<userflags;
	userroles=mrl.getrole();
// 	for(int i=0;i<userroles.size();i++)qDebug("have role %s",userroles[i].toLatin1().data());

	return true;
}

bool MSInterface::checkServer()
{
	MTServerInfo si;
	try{si=MTServerInfo::query();}catch(WException e){
		qDebug("caught exception while getting server info (%s): %s",
		 e.component().toLatin1().data(),
		 e.error().toLatin1().data());
		MBoxWrapper::warning(tr("Error"),tr("Communication problem while talking to the server, see log for details."));
		return false;
	}
	if(si.stage()!=si.Success){
		MBoxWrapper::warning(tr("Error"),tr("Communication with server was not successful."));
		return false;
	}
	//is server new enough for me?
	if(si.getServerProtocolVersion().value()<needCommVersion()){
		MBoxWrapper::warning(tr("Error"),tr("The server implementation is too old for this client."));
		return false;
	}
	//am I new enough for server?
	if(si.getMinimumProtocolVersion().value()>commVersion()){
		MBoxWrapper::warning(tr("Error"),tr("This client is too old for the server, please upgrade."));
		return false;
	}
	//we are ok
	return true;
}

void MSInterface::logout()
{
	if(m_sessid!="")queryLogout();
	m_sessid="";
}
bool MSInterface::relogin()
{
	logout();
	return login(m_uname,m_passwd);
}

QMap<QString,QString> MSInterface::headers(QString s)const
{
	QMap<QString,QString> ret=WInterface::headers(s);
	ret.insert("Wob-SessionId",m_sessid);
	return ret;
}

QString MSInterface::dataDir()const
{
	QString dd="profile."+profileid;
	QDir dir(appDataDir());
	if(!dir.exists(dd))
		if(!dir.mkpath(dd))
			qDebug("Warning: oh dir! Can't create my data directory!");
	return appDataDir()+"/"+dd;
}

QString MSInterface::appDataDir()
{
        return ::dataDir;
}

void MSInterface::setAppDataDir(QString d)
{
        ::dataDir=resolveDir(d);
        qDebug()<<"Setting the data directory to"<<::dataDir<<"from"<<d;
}


QString MSInterface::settingsGroup()const
{
	return "profsettings/"+profileid;
}
QString MSInterface::configSettingsGroup()const
{
	return "profiles/"+profileid;
}

bool MSInterface::hasRight(Right r)const
{
	if(userroles.contains("_admin"))return true;
	return userrights.contains(r);
}

void MSInterface::initialize()
{
	//retrieve translation file
	if(servertranslation.size()==0){ //can be called only once, make sure it is so
		QString lang=QSettings().value("lang","--").toString();
		if(lang=="--"){
			qDebug("MSInterface: no local language is set, so not retrieving any from server.");
			return;
		}
		MTGetLanguage gl=MTGetLanguage::query(lang,"qm");
		if(gl.hasError()){
			qDebug("MSInterface: error while retrieving language %s from server: (%s) %s",
				lang.toLatin1().data(),
				gl.errorType().toLatin1().data(),
				gl.errorString().toLatin1().data());
			//go to fallback
			gl=MTGetLanguage::query("C","qm");
			if(gl.hasError()){
				qDebug("MSInterface: cannot retrieve language 'C', giving up on languages.");
				return;
			}
		}
		servertranslation=gl.getfile().value();
		QTranslator *trn=new QTranslator(this);
		trn->load((const uchar*)servertranslation.data(),servertranslation.size());
		qApp->installTranslator(trn);
		qDebug("MSInterface: successfully loaded server language %s",lang.toLatin1().data());
		MLocalFormat::setDefaultFormat(gl.getformats().value());
		m_statusbartext=gl.getstatusBarText();
	}
}

void MSInterface::sslErrors(QNetworkReply *src,const QList<QSslError>&errs)
{
	//get source of error
	if(!src)return;
	//check against known exceptions
	if(sslexcept->checksslexcept(errs)){
		src->ignoreSslErrors();
		return;
	}
	//message box
	if(!didsslerror){
		MBoxWrapper::warning(tr("Connection Error"),tr("There were problems while authenticating the server. Aborting. Check your configuration."));
		didsslerror=true;
	}
}

void MSInterface::updateTemplates()
{
	if(temp)temp->updateTemplates(true);
}

bool MSInterface::checkFlags(const QStringList& fl) const
{
	//shortcut for admins
	if(userflags.contains("_admin") || userroles.contains("_admin"))
		return true;
	//check actual flags
	foreach(QString f,fl){
		f=f.trimmed();
		if(f.isEmpty())continue;
		if(f[0]=='+'){
			if(!userflags.contains(f.mid(1)))return false;
		}else if(f[0]=='-'){
			if(userflags.contains(f.mid(1)))return false;
		}else //ooops! invalid flag spec
			return false;
	}
	//survived all tests
	return true;
}

QUrl MSInterface::parentUrl() const
{
	QUrl parent=url();
	parent.setFragment(QString());
	parent.setQuery(QString());
	QString path=parent.path();
	if(path==QString() || path.endsWith('/'))return parent;
	parent.setPath(path.left(path.lastIndexOf('/')+1));
	return parent;
}

QString MSInterface::profileName() const
{
	return QSettings().value("profiles/"+profileid+"/name").toString();
}
