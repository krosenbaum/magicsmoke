TEMPLATE = lib
TARGET = magicsmoke-iface
VERSION =
QT-=gui

#FIXME:
equals(QT_MAJOR_VERSION, 6): QT += core5compat

#make sure exports are ok
DEFINES += MSIFACE_EXPORT=Q_DECL_EXPORT

include (../basics.pri)
include (libs.pri)

include (wob/wob.pri)
include (wext/wext.pri)
include (misc/misc.pri)
include (templates/templates.pri)

HEADERS += \
	msinterface.h \
	sslexception.h

SOURCES += \
	msinterface.cpp \
	sslexception.cpp

INCLUDEPATH += $$PWD

#Localization
TRANSLATIONS = \
	smoke-ifc_de.ts \
	smoke-ifc_en.ts
